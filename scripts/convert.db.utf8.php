#!/usr/bin/env php
<?php

	/**************
	
	/usr/local/php53/bin/php convert.db.utf8.php
	
	***************/

$timestamp1	= microtime(TRUE);
$convertUtf8Chars	= TRUE;

date_default_timezone_set('America/New_York');
try {
	if (!defined('SCRIPTS_PATH')) {
		define('SCRIPTS_PATH', dirname(__FILE__));
	}
	require SCRIPTS_PATH . '/../public/env.php';
	require SCRIPTS_PATH . '/../app/configs/environment.php';
} catch (Exception $ex) {
	
}

$export_dir		= SCRIPTS_PATH .'/exported';
$convert_dir	= SCRIPTS_PATH .'/converted';

if (!isset ($application) || !($db = $application->getOption('db'))) {
	echo "Application configuration for database is not available!" . PHP_EOL;
}

if (!file_exists($export_dir)) {
	mkdir($export_dir);
	chmod($export_dir, 0777);
	echo "New directory \"". $export_dir ."\" has been created!". PHP_EOL;
}

if (!file_exists($convert_dir)) {
	mkdir($convert_dir);
	chmod($convert_dir, 0777);
	echo "New directory \"". $convert_dir ."\" has been created!". PHP_EOL;
}

$file_postfix	= date('Ymd-His', $timestamp1);
$export_sql		= $export_dir .'/'. $db['database'] .'-'. $file_postfix .'.sql';
$convert_sql	= $convert_dir .'/'. $db['database'] .'-'. $file_postfix .'.sql';
/**
 * exporting (also the backup)
 */

  
 
$exportCommand	= shell_exec('mysqldump --opt -h '. $db['host'] .' -u'. $db['username'] .' -p'. $db['password'] .' '. $db['database'] .' > '. $export_sql);
echo "A backup of Database \"". $db['database'] ."\" has been taken at \"". $export_sql ."\"!". PHP_EOL;

/**
 * convert existing
 */
$content	= file_get_contents($export_sql);
$converted	= preg_replace("/latin1_swedish_ci/", "utf8_general_ci", $content);
$converted	= preg_replace("/CHARSET\=latin1/", "CHARSET=utf8", $converted);
#$converted	= iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $content);
if ($convertUtf8Chars) {
	$converted	= iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $converted);
}

/**
 * put converted content into sql file
 */
file_put_contents($convert_sql, $converted);
chmod($convert_dir .'/'. $db['database'] .'-'. $file_postfix .'.sql', 0777);
echo "A converted sql file for Database \"". $db['database'] ."\" has been created at \"". $convert_sql ."\"!". PHP_EOL;
echo PHP_EOL ."Your next possible command would be (make sure that database is empty)". PHP_EOL .
	"\"mysql -u ". $db['username'] ." -p ". $db['database'] ." < ". $convert_sql . PHP_EOL;

/*
echo "\r\n(Database Password)\r\n";
$importTbl		= shell_exec('mysql -u -p --default_character_set utf8 cityblast2 < '. $workDir .'/cityblast-iconv2.sql');
echo $importTbl ."\r\n";
*/


?>
