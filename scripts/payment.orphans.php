#!/usr/bin/env php
<?php
	/**************
	
	/usr/local/php53/bin/php /home/abubic/cityblast.com/scripts/payment.orphans.php
	
	***************/
	
	require realpath(dirname(__FILE__) . '/../public/env.php');	
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	
	$members  = Member::all(array(
			"conditions"=> "(next_payment_date IS NULL OR next_payment_date='' OR next_payment_date < DATE(NOW()) OR price = 0) and payment_status = 'paid' and account_type = 'paid'"
	));

	ob_start();
?>

Members without current payments (<?php echo count($members) ?>)

<?php echo col('Member ID', 15, 'right') . col( 'Name', 30 ) . col('Next payment date', 20) ?>

----------------------------------------------------------------<?php foreach ($members as $member) : 

	$next_payment_date = null;
	if ($member->next_payment_date instanceOf DateTime) $next_payment_date = $member->next_payment_date->format('Y-m-d');

?>	
<?php echo col($member->id, 15, 'right') . col( $member->last_name . ', ' . $member->first_name, 30 ) . col($next_payment_date, 20) ?>
<?php endforeach; ?>
<?php if (count($members) == 0) : ?>

(No members found)<?php endif; ?>

----------------------------------------------------------------
Generated at <?php echo date('g:ia'); ?> on <?php echo date('F j, Y'); ?>

<?php	

	$email = ob_get_clean();
	
	$emailer = Zend_Registry::get('alertEmailer');
	$emailer->send("Members without current payments: " . count($members), $email, 'status-payment-orphans');


function col($str, $size, $position = 'left') {
	if ($str === null) $str = 'NULL';
	$str = (string) $str;
    $pos = 0;
    if ($position == 'left') {
    	$pos = STR_PAD_RIGHT;
    } else {
     	$str .= '   ';
   }
	return str_pad($str, $size, ' ', $pos);
}