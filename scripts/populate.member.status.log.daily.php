#!/usr/bin/env php
<?
include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');


Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

Logger::logTimeMarker();

// Date defaults to yesterday as cron should run after midnight
$start_date = !empty($argv[1]) ? $argv[1] : date('Y-m-d', strtotime('yesterday'));
$end_date = !empty($argv[2]) ? $argv[2] : $start_date;
MemberStatusLogDaily::populateDateRange($start_date, $end_date);
