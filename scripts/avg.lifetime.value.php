#!/usr/bin/env php
<?
    /**************
      /usr/local/php53/bin/php avg.lifetime.value.php
     ***************/
	
	
	    
	require realpath(dirname(__FILE__) . '/../public/env.php');
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');



	$query = "set @@wait_timeout=1200";
	Member::connection()->query($query);

	ini_set('memory_limit', '500M');
	
	$query = "
				SELECT SUM( amount ) AS total
				FROM payment
				JOIN member ON payment.member_id = member.id
				GROUP BY member.id
				HAVING total >0	
			";
	$paying_members =	Payment::find_by_sql($query);

	//print_r($paying_members);
	
	
	$running_total = 0;
	$count = 0;
	
	foreach($paying_members as $total_cash)
	{
		echo $total_cash->total . "\n";
		
		$running_total += $total_cash->total;
		$count++;
		
	}
	
	echo "\n\n";
	echo $running_total . "\n";
	echo $count . "\n";
	
	echo round($running_total / $count, 2) . "\n\n"; 
	
?>