#!/usr/bin/env php
<?

	/**************

	/usr/local/php53/bin/php balance.by.reach.php

	***************/
	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');



	ini_set('display_errors', 1);

	#define('ALLOW_NULL', TRUE);

	//Make sure the connection doesn't time out
	$query = "set @@wait_timeout=1200";
	Listing::connection()->query($query);
	echo "\n\n\n";


	#$cities = City::all();

	$inventoryList	= Member::find('all', array('select' => 'DISTINCT inventory_id', 'order' => 'inventory_id ASC'));

	$hopperList	= Member::find('all', array('select' => 'DISTINCT current_hoper', 'order' => 'current_hoper ASC'));


	#$cities = City::all();
	$select		= "*, (IFNULL(twitter_followers, 0) + IFNULL(linkedin_friends_count, 0) + IFNULL(facebook_friend_count, 0)) as reach";
	
	$conditions = "( (facebook_publish_flag > 0 and facebook_fails < 3) or
					(linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) or
					(twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) AND
					status='active'";

	$members	= Member::find('all', array('select' => $select, 'order' => 'reach DESC', 'conditions' => $conditions));



	$inventoriesReach	= $inventoriesMembers	= array();
	$hoppersReach		= $hoppersMembers		= array();


	foreach ($inventoryList as $inventory) {#if (is_null($inventory->inventory_id)); exit;

		$inventoriesReach[$inventory->inventory_id]		= 0;
		$inventoriesMembers[$inventory->inventory_id]	= 0;

		$hoppersReach[$inventory->inventory_id]	= $hoppersMembers[$inventory->inventory_id]	= array();

		foreach ($hopperList as $hopper) {

			$hoppersReach[$inventory->inventory_id][$hopper->current_hoper]		= 0;
			$hoppersMembers[$inventory->inventory_id][$hopper->current_hoper]	= 0;

		}
	}

	#pre($inventoriesReach, 0); pre($hoppersReach);

	echo 'Start processing:'. PHP_EOL;

	foreach($members as $member)
	{
		/*
		$memberReach	= 0;


		if ($member->twitter_followers) {

			$memberReach	+= $member->twitter_followers;

		}



		if ($member->linkedin_friends_count) {

			$memberReach	+= $member->linkedin_friends_count;
		
		}



		if ($member->facebook_friend_count) {

			$memberReach	+= $member->facebook_friend_count;
			
		}
		*/

		$memberReach		= $member->reach;
		/**
		 * find inventory & hopper of least reach
		 * and update their reach & members
		 */
		$least				= findLeastInventoryHopperReach($hoppersReach);

		$inventoriesReach[$least->inventory]	+= $memberReach;
		$inventoriesMembers[$least->inventory]++;

		$hoppersReach[$least->inventory][$least->hopper]	+= $memberReach;
		$hoppersMembers[$least->inventory][$least->hopper]++;

		/**
		 * update member's inventory and hopper
		 */
		$member->inventory_id	= $least->inventory;
		$member->current_hoper	= $least->hopper;

		//$member->save();
	}


	echo 'Process complete.'. PHP_EOL;
	echo 'Inventory vs Total Members:'. PHP_EOL;

	foreach ($inventoriesMembers as $id => $total) {

		echo $id .' --> '. $total . PHP_EOL;

	}


	echo PHP_EOL .'Inventory vs Total Reach:'. PHP_EOL;

	foreach ($inventoriesReach as $id => $total) {

		echo $id .' --> '. $total . PHP_EOL;

	}


	echo PHP_EOL .'Inventory vs Hopper vs Total Members:'. PHP_EOL;

	foreach ($hoppersMembers as $inventory => $hoppers) {
		foreach ($hoppers as $hopper => $total) {

			echo $inventory .' --> '. $hopper .' --> '. $total . PHP_EOL;

		}
	}


	echo PHP_EOL .'Inventory vs Hopper vs Total Reach:'. PHP_EOL;

	foreach ($hoppersReach as $inventory => $hoppers) {
		foreach ($hoppers as $hopper => $total) {

			echo $inventory .' --> '. $hopper .' --> '. $total . PHP_EOL;

		}
	}


	/*
	function findLeastInventoryReach(Array &$inventories)
	{
		$inventory	= null;
		$tmp		= null;
		reset($inventories);

		if (count($inventories)) {

			$inventory	= key($inventories);
			$tmp		= intval(current($inventories));
		}

		foreach ($inventories as $id => $value) {

			if ($tmp > intval($value)) {

				$tmp		= $value;
				$inventory	= $id;
			}
		}

		return $inventory;
	}
	*/


	function findLeastInventoryHopperReach(Array &$hoppers)
	{
		$hopper		= null;
		$inventory	= null;
		$tmp		= null;
		reset($hoppers);

		if (count($hoppers)) {

			$inventory	= key($hoppers);
			$hopper		= key($hoppers[$inventory]);
			$tmp		= intval($hoppers[$inventory][$hopper]);
		}

		/**
		 * loop for findout least reach
		 */
		foreach ($hoppers as $inventoryId => $inventoryHoppers) {
			foreach ($inventoryHoppers as $hopperId => $value) {

				if ($tmp > intval($value)) {

					$tmp		= intval($value);
					$inventory	= $inventoryId;
					$hopper		= $hopperId;
				}
			}
		}

		$return	= new stdClass();
		$return->inventory	= $inventory;
		$return->hopper		= $hopper;

		return $return;
	}

?>