#!/usr/bin/env php
<?php

/**************
/usr/local/php53/bin/php trialdrip.php 
***************/

include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');

$offset = 0;
$limit  = 100;
$count  = 0;
$total  = 0;

$campaign = new Blastit_Email_DripCampaign_TrialMyEventVoice;

do {
	$members = Member::all( array('conditions' => Blastit_Email_DripCampaign_Trial::getMemberConditions(), 'limit' => $limit, 'offset' => $offset) );

	foreach($members as $member)
	{
		$campaign->setMember($member);
		if ($campaign->drip()) 
		{
			echo 'Drip sent for member ' . $member->id . ' - ' . $member->first_name . ' ' . $member->last_name . PHP_EOL;
			$count++;
			if ($campaign->getDripNumber() == 6) {
				$member->payment_status = 'signup_expired';
				$member->save();
				
				$messageValues = array();
				$messageValues['Name'] = $member->first_name . ' ' . $member->last_name;
				if ($member->company_name) $messageValues['Company Name'] = $member->company_name;
				if ($member->email_override) $messageValues['Email Override'] = $member->email_override;
				$messageValues['Email'] = $member->email;
				if ($member->business_email) $messageValues['Email Business'] = $member->business_email;
				$messageValues['Phone'] = $member->phone;
				if ($member->gender) $messageValues['Gender'] = $member->gender;
				$messageValues['Edit URL'] = APP_URL . '/admin/member/' . $member->id;
				
				$message = '';
				foreach ($messageValues as $name => $value) {
					$message .= $name . ': ' . $value . "\n";
				}
				
				$notifier = Zend_Registry::get('alertEmailer');
				$notifier->send(
					'Trial Expiry: ' . $member->name(),
					$message,
					'trial-expiry',
					true
				);
			}
		}
		$total++;
	}
	
	$offset += $limit;
	
} while (count($members) == $limit);