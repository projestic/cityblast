#!/usr/bin/env php
<?

require realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

$query = "set @@wait_timeout=1200";
Member::connection()->query($query);

$offset = 0;
$limit  = 100;	
$total  = 0;

$mode = !empty($argv[1]) ? trim($argv[1]) : 'all';

switch ($mode) {
	case 'missing':
		$conditions = "
			twitter_access_token IS NOT NULL AND 
			twitter_access_token != '' AND (
				twitter_id IS NULL OR twitter_id = '' OR 
				twitter_screen_name IS NULL OR 
				twitter_screen_name = ''
			)
		";
	break;
	case 'all':
	default:
		$conditions = "twitter_access_token IS NOT NULL";
	break;
}

do {
	
	$members = Member::all( array('conditions' => $conditions, 'limit' => $limit, 'offset' => $offset) );

	foreach($members as $member)
	{
		processMember($member);
	}	
	
	$offset += $limit;
	
} while (count($members) == $limit);


	
	
function processMember($member) {

	$oauth = @unserialize($member->twitter_access_token);
	
	if ($oauth === false) {
		echo "{$member->id} {$member->first_name} {$member->last_name}: failed to unserialize access token"  . PHP_EOL . PHP_EOL;
		return false;
	}
	
	$twitter = new Blastit_Service_Twitter( array('username' => $member->twitter_name, 'oauth' => $oauth), $member );

	try {
		echo "{$member->id} {$member->first_name} {$member->last_name}: ";
		$credentials = $twitter->accountVerifyCredentials();
		if (isset($credentials->followers_count)) $member->twitter_followers = $credentials->followers_count;
		if (isset($credentials->name)) $member->twitter_name = $credentials->name;
		if (isset($credentials->screen_name)) $member->twitter_screen_name = $credentials->screen_name;
		if (isset($credentials->id)) $member->twitter_id = $credentials->id;
	} catch (Exception $e) {
		if ($e->getCode() == '88') {
			echo "Rate limited by Twitter, waiting 120 seconds." . PHP_EOL . PHP_EOL;
			sleep(120);
			processMember($member);
		} else {
			echo "  Error " . $e->getCode() . " - " . $e->getMessage() . PHP_EOL . PHP_EOL;
		}
		return;
	}

	$member->save();
} 
