#!/usr/bin/env php
<?php

/**
 * This example gets all campaigns. To add a campaign, run AddCampaign.php.
 *
 * PHP version 5
 *
 * Copyright 2009, Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    GoogleApiAdsAdWords
 * @subpackage v200909
 * @category   WebServices
 * @copyright  2009, Google Inc. All Rights Reserved.
 * @license    http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @author     Adam Rogal <api.arogal@gmail.com>
 * @author     Eric Koleda <api.ekoleda@gmail.com>
 * @link       http://code.google.com/apis/adwords/v2009/docs/reference/CampaignService.html
 */

error_reporting(E_STRICT | E_ALL);

// You can set the include path to src directory or reference
// AdWordsUser.php directly via require_once.
// $path = '/path/to/aw_api_php_lib/src';


//echo dirname(__FILE__) . "\n\n";
//exit();




 	
	/**************
	
	/usr/local/php53/bin/php business.post.php CITY_ID INVENTORY_ID HOPPER_ID
	
	***************/
	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');



	
	//Make sure the connection doesn't time out
	$query = "set @@wait_timeout=10000";
	Listing::connection()->query($query);
	echo "\n\n\n";





	$path = dirname(__FILE__) . '/../lib/adwords_api/src';
	set_include_path(get_include_path() . PATH_SEPARATOR . $path);
	
	require_once 'Google/Api/Ads/AdWords/Lib/AdWordsUser.php';






	
	
	try {
		
		$today =  date('Y-m-d');
		
		$start = mktime(0,0,0,date("m"),date("d")-1,date("Y"));
		$end = mktime(0,0,0,date("m"),date("d"),date("Y"));
		
		$start_date = date("Ymd", $start);
		$db_start_date = date("Y-m-d", $start);
		$end_date = date("Ymd", $end); 	
		
	
	echo "START:".$start_date . "\n";
	echo "END:".$end_date . "\n";
	
	
	
	
		
		// Get AdWordsUser from credentials in "/src/Google/Api/Ads/AdWords/auth.ini"
		// relative to the AdWordsUser.php file's directory.
		$user = new AdWordsUser();
		

		// Log SOAP XML request and response.
		$user->LogDefaults();
		
		// Get the CampaignService.
		$campaignService = $user->GetCampaignService('v200909');
		
		// Create selector.
		$selector = new CampaignSelector();
	
		
		// Create stats selector.
		$statsSelector = new StatsSelector();
		$dateRange = new DateRange();
		$dateRange->min = $start_date;
		$dateRange->max = $end_date;
		$statsSelector->dateRange = $dateRange;
		$selector->statsSelector = $statsSelector; 
	
	echo "<pre>";
	print_r($statsSelector);
	
	
	
	
	
		
		// Get all campaigns.
		$page = $campaignService->get($selector);
		
	
	
	//exit();  
	  
	 
	  	// Display campaigns.
	  	if (isset($page->entries)) 
	  	{
	    		foreach ($page->entries as $campaign) 
	    		{
	    						
				// Get campaigns with stats.
				$page = $campaignService->get($selector); 
	      
	      
			      if($campaign->stats->cost->microAmount)
			      {
			      		print 'Campaign with name "' . $campaign->name . '" and id "' . $campaign->id . "\" was found.\n";
			      		
	
						//echo "<pre>";
						//print_r($campaign->stats->cost->microAmount);
						
					     echo 'Campaign Clicks:' . $campaign->stats->clicks ."\n"; 
					     echo 'Campaign Impressions:' . $campaign->stats->impressions ."\n"; 
					      
					      
					     echo 'Campaign cost:' . ($campaign->stats->cost->microAmount / 1000000) ."\n"; 
					     $ctr = ($campaign->stats->clicks * 100) / $campaign->stats->impressions;
					     echo 'CTR:'.round($ctr, 2)."\n";
					     
					     
					     $query = "insert into adwords(date, contest_id, cost, clicks, impressions, campaign_id) ";
					     $query .= "VALUES ('".$db_start_date."', ".CURRENT_CONTEST.", '".($campaign->stats->cost->microAmount / 1000000)."', '".$campaign->stats->clicks."', '".$campaign->stats->impressions."', '".$campaign->id."')";
					     
					     echo "\n\n".$query."\n\n";
					     
					     $result = mysql_query($query);
	
	
						if (!$result) {
						    $message  = 'Invalid query: ' . mysql_error() . "\n";
						    $message .= 'Whole query: ' . $query;
						    die($message);
						}
					     
					     /****
					     $adword = new Adword();
					     $adword->date = $start_date;
					     $adword->cost = ($campaign->stats->cost->microAmount / 1000000);
					     $adword->clicks = $campaign->stats->clicks;
					     $adword->impressions = $campaign->stats->impressions;
					     
					     $adword->save();	
					     *******/			     				     				     
			      }
	      	
	      	
	    		}
	  	} else {
	    		print "No campaigns were found.\n";
	  	}
	  	
	} catch (Exception $e) {
	  print_r($e);
	}

?>