<?php
/**

	/usr/local/php53/bin/php process.treb.php


 * This script is intended to process CSV listing file
 *
 * allow access in comman-line with parameter like following
 *
 * cl$ sudo php process.treb.php time=20111015 mc=3 ipl=1
 *
 * "time" date of fetching listings
 * "mc" is the number of Maximum Connection at-a-time to download
 * "ipl" is the number images per listing to be downloaded
 *
 * @author nur<shahid9829@gmail.com>
 */
$timestamp1	= microtime(TRUE);

require realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

define('ALLOW_MLS_REPEATATION', 0);

ini_set('memory_limit', '300M');

include_once "process.treb.inc.php";

echo "Process started\r\n";

$TOTAL_LISTINGS=0;

/**
 * catch paramater passed through either URL or command line
 */
$time		= strtotime('now'); # date of fetch
$mc			= 1; # maximum connection for download

if (isset($argv) && $argc > 1) {
	array_shift($argv);
	$args	= parse_args($argv);

	if (!empty($args['time'])) {
		$time	= strtotime(urldecode($args['time']));
	}
	if (!empty($args['mc'])) {
		$mc	= $args['mc'];
	}
} else {
	if (!empty($_GET['time'])) {
		$time	= strtotime(urldecode($_GET['time']));
	}
	if (!empty($_GET['mc'])) {
		$mc		= urldecode($_GET['mc']);
	}
}

/**
 * Credential for FTP access
 */
$credentials= array(
	'username'	=> 't011cty@photos',
	'password'	=> 'T22cty',
);

/**
 * image directory
 */
$img_src_url	= "ftp://" . urlencode($credentials['username']) . ":" . urlencode($credentials['password']) . "@3pv.torontomls.net/mlsmultiphotos/";
$img_tmp_dir	= dirname(__FILE__) . "/../tmp/" . md5(microtime());

mkdir($img_tmp_dir);

$img_tmp_dir = realpath($img_tmp_dir);

/**
 * predefined image fields for DB
 */
$img_fields	= array(
	'image',
	'thumbnail'
);

/**
 * mapping array which helps to map csv data into db row
 *
 * db_field => csv_field
 */
$mapper	= array(
	'mls'					=> 'ml_num',
	'price'					=> 'lp_dol',
	'street'				=> array(
		'st_num',
		'st',
		'apt_num',
		'st_sfx'
	),
	'appartment'			=> 'apt_num',
	'message'				=> 'ad_text',
	'number_of_bedrooms'	=> 'br',
	'number_of_bathrooms'	=> 'bath_tot',
	'number_of_parking'		=> 'park_spcs',
	'maintenance_fee'		=> 'maint',
	'taxes'				=> 'taxes',
	'broker_of_record'		=> 'rltr',
	'postal_code'			=> 'zip',
	'city'				=> 'municipality',
	'area'				=> 'area',
	'community'			=> 'community',
	'list_agent'			=> 'list_agent',
	'list_agent_phone'		=> 'lagt_ph',
	'raw_property_type'		=> 'type_own1_out'
);

// db fields which have static value
$static_data= array(
	'approval'		=> 1,
	'publish'		=> 1,
	'published_on'	=> date('Y-m-d H:i:s', $time),
	'city_id'		=> 1,
	'status'		=> 1,
	'member_id'		=> '146',
	'blast_type'	=> Listing::TYPE_IDX,
);

// CSV directory
$csv_dir	= dirname(__FILE__) .'/treb-files/';

//start processing of available listing
$avail_file	= $csv_dir .'avail-'. date('Y-M-d', $time) .'.csv';
//$avail_file = $csv_dir ."avail-2012-Feb-17.csv";

if (file_exists($avail_file))
{
	echo "AVAILABLE FILE:".$avail_file . "\n\n";
}
else
{
	echo "CSV file for available listings not found at: ".$avail_file . PHP_EOL;
	echo "Hints: may be you need to execute command like \"php get.treb.php time=". date('Ymd', $time) ."\" to get the CSV ".
			"for available listings for that day". PHP_EOL;
	echo "processing Treb Listing aborted!". PHP_EOL;
	exit;
}

// get treb/mls listings
$treb_listings	= parse_csv($avail_file, $mapper);
// taking limited listings for test purpose
#$treb_listings	= array_slice(parse_csv($avail_file, $mapper), 0, 2);

// filtered listings
$listings		= array();
// stack for existing mls listing (listing_id => mls)
$existing_mls	= array();

// filtering for exiting mls listing
foreach ($treb_listings as $key => $row)
{
	$mls		= substr($row['mls'], 1);
	// checking whether the mls listing is exists or not
	$mls_listing= Listing::find_by_mls($mls);
	
	if ($mls_listing)
	{
		// stacked this existing listing
		$existing_mls[$mls_listing->id]	= $mls;

		if (!ALLOW_MLS_REPEATATION) {
			// skip image download
			echo PHP_EOL ."SKIPPING listing for existing MLS \"". $mls ."\" (Listing ID -> \"". $mls_listing->id ."\")". PHP_EOL . PHP_EOL;
			continue;
		}
	}

	$listings[]	= $row;
}



// lisitng filtered mls and download related images
if (is_array($listings) && count($listings)) 
{

	//create Extended Insert rows
	foreach ($listings as $key => $row) {

		$listing	= new Listing();
		
		// drop the initial character from MLS number
		$listing->mls					= empty($row['mls'])? "" : substr($row['mls'], 1);
		$listing->price					= empty($row['price'])? "" : $row['price'];

		$listing->street				= empty($row['street'])? "" : $row['street'];
		$listing->appartment			= empty($row['appartment'])? "" : $row['appartment'];
		$listing->message				= empty($row['message'])? "" : $row['message'];

		$listing->number_of_bedrooms	= empty($row['number_of_bedrooms'])? "" : $row['number_of_bedrooms'];
		$listing->number_of_bathrooms	= empty($row['number_of_bathrooms'])? "" : $row['number_of_bathrooms'];
		$listing->number_of_parking		= empty($row['number_of_parking'])? "" : $row['number_of_parking'];
		$listing->maintenance_fee		= empty($row['maintenance_fee'])? "" : $row['maintenance_fee'];

		$listing->taxes					= empty($row['taxes'])? "" : $row['taxes'];
		$listing->broker_of_record		= empty($row['broker_of_record'])? "" : $row['broker_of_record'];
		$listing->postal_code			= empty($row['postal_code'])? "" : $row['postal_code'];
		$listing->city					= empty($row['city'])? "" : $row['city'];

		$listing->area					= empty($row['area'])? "" : $row['area'];
		$listing->community				= empty($row['community'])? "" : $row['community'];
		$listing->list_agent			= empty($row['list_agent'])? "" : $row['list_agent'];
		$listing->list_agent_phone		= empty($row['list_agent_phone'])? "" : $row['list_agent_phone'];
		
		//echo $row['raw_property_type'] . "\n\n";


		$found = false;
		if (stristr($row['raw_property_type'],"Detached")) {
			$listing->property_type_id = 1;
			$found = true;	
		} elseif (stristr($row['raw_property_type'],"Semi-Detached")) {
			$listing->property_type_id = 2;
			$found = true;	
		} elseif (stristr($row['raw_property_type'],"Att/Row/Twnhouse")) {
			$listing->property_type_id = 3;
			$found = true;	
		} elseif (stristr($row['raw_property_type'],"Condo Townhouse")) {
			$listing->property_type_id = 4;
			$found = true;	
		} elseif (stristr($row['raw_property_type'],"Condo Apt")) {
			$listing->property_type_id = 5;
			$found = true;	
		} elseif (stristr($row['raw_property_type'],"Comm Element Condo")) {
			$listing->property_type_id = 6;
			$found = true;	
		} elseif (stristr($row['raw_property_type'],"Att/Twnhouse")) {
			$listing->property_type_id = 7;
			$found = true;	
		}
		
		// static part
		$listing->pre_approval			= 1;
		$listing->approval				= 1;
		$listing->publish				= 1;
		$listing->published_on			= date('Y-m-d H:i:s', $time);
		$listing->city_id				= 1;
		$listing->status				= 'active';
		$listing->member_id				= 146;
		$listing->blast_type			= Listing::TYPE_IDX;

		if ($found) {

			echo "I am a known type... save me!\n";
			
			$TOTAL_LISTINGS++;
			
			$listing->setLatitude();
			$listing->save();
			$mls = substr($row['mls'], 1);
			
			for ($i = 0; $i < 5; $i++) {
				$current = $i+1;
				if ($current == 1) {
					$image =  $img_src_url . ($current) ."/". substr($mls, -3) ."/". $row['mls'] .".jpg";
				} else {
					$image = $img_src_url . ($current) ."/". substr($mls, -3) ."/". $row['mls'] ."_".$current.".jpg";
				}
				$tmp_image = $img_tmp_dir . '/' . basename($image);
				try {
					if (copy($image, $tmp_image)) {
						if ($i == 0) {
							$listing->ingestImage($tmp_image);
							$listing->save();
						} else {
							$listingImage = new ListingImage();
							$listingImage->listing_id = $listing->id;
							$listingImage->save();
							$listingImage->ingestImage($tmp_image);
							$listingImage->save();
							@unlink($tmp_image);
						}
					}
				} catch (Exception $e) {
					echo "Failed to retrieve $image: " . $e->getMessage() . PHP_EOL;
				}
			}
	
		} else {
			echo "Unknown type found: " . $row['raw_property_type']. " do not save\n";	
			mail("alen@alpha-male.tv","unknown type: ".$row['raw_property_type'], "unknown type found");	
		} 
		

		
	}

	if ($listings) {
		echo $TOTAL_LISTINGS ." rows successfully inserted\r\n";
	}
	
	unlink($avail_file);
	echo "file \"". $avail_file ."\" deleted successfully\r\n";

	echo "Process ended successfully.\r\n";
	echo "takes ". round((microtime(TRUE) - $timestamp1), 3) ." sec\r\n";
	
	mail("alen@alpha-male.tv, shaun@shaunnilsson.com","ADDED SMLS: ".$TOTAL_LISTINGS,"new listings added");

} else {
	echo "No new listings found today...\n\n\n";
	//, shaun@shaunnilsson.com
	mail("alen@alpha-male.tv","NO SMLS FOUND TODAY","no new listings added");
}

unlink($img_tmp_dir);
