#!/usr/bin/env php
<?php
	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	
	ini_set('display_errors', 1);

	echo "Starting DB query....";
	
	$members = Member::find('all', array('conditions' => "payment_status IN ('signup','signup_expired','payment_failed','cancelled','extended_trial')", 
	    			'select' => "id, first_name, last_name, email"));

	echo "Done!\n";
	echo "Generating CSV....";
	
	$csv = "ID,First Name,Last Name,Email\n";
	
	foreach ($members as $m)
	{
		$csv .= "{$m->id},{$m->first_name},{$m->last_name},{$m->email}\n";
	}

	echo "Done!\n";
	echo "Writing CSV....";
	
	file_put_contents(dirname(__FILE__). '/unpaid_members.csv', print_r($csv, 1));
	
	echo "Done!\n\nCSV file location is ". dirname(__FILE__). "/unpaid_members.csv\n";
?>
