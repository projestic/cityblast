#!/usr/bin/env php
<?php

// Reports on payables recorded in the PREVIOUS month.
// For example, if the script is run on April 1 (or April 5, or April 15), it will report on payables recorded
// from March 1 to March 31.

require(__DIR__ . '/../public/env.php');
require(__DIR__ . '/../app/configs/environment.php');

$subject = "Affiliate Payout Report";
$csvfile = '/tmp/payables-' . date('Y-m-d') . '.csv';

$conditions = "created_at BETWEEN date_format(NOW() - INTERVAL 1 MONTH, '%Y-%m-01') AND last_day(NOW() - INTERVAL 1 MONTH)";
$payables = Payable::all(array(
	"conditions" => $conditions,
	"joins" => "JOIN affiliate ON payable.affiliate_id = affiliate.id",
	'order' => 'affiliate_id'
));

$csv = fopen($csvfile, 'w');

// Headers
fputcsv($csv, array(
	'Payable ID',
	'Payment date',
	'Payout amount',
	'Affiliate ID',
	'Affiliate',
	'Member ID',
	'Member Name'));

$total = 0;
foreach ($payables as $payable) {
	$affiliate_owner_name = '-';
	$affiliate_owner_id = '-';
	$member_name = '-';
	$member_id = '-';

	$total += $payable->amount;

	if ($payable->affiliate) {
		$affiliate_owner_name = $payable->affiliate->owner->name();
		$affiliate_owner_id = $payable->affiliate->owner->id;
	}

	if ($payable->member) {
		$member_name = $payable->member->name();
		$member_id = $payable->member->id;
	}

	fputcsv($csv, array(
		$payable->id,
		$payable->payment->created_at->format('Y-m-d'),
		'$' . number_format($payable->amount, 2),
		$affiliate_owner_id,
		$affiliate_owner_name,
		$member_id,
		$member_name));
}

fputcsv($csv, array("Total payouts: $" . $total));

// Email report
$emailer = Zend_Registry::get('alertEmailer');
if (file_exists($csvfile)) {
	$emailer->attachFile($csvfile);
	$body = 'Please see attached affiliate report';
} else {
	$body = 'No payables found';
}
$emailer->send($subject, $body, 'status-payment-batch');