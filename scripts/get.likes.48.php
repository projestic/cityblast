#!/usr/bin/env php
<?php

	/****
	
	/usr/local/php53/bin/php get.likes.24.php
	
	****/
	
	ini_set('display_errors', 1);

	// disabled, if we need to increase memory on this script, something's wrong.
	// ini_set('memory_limit', '500M');	

	include realpath(dirname(__FILE__) . '/../public/env.php');
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	require realpath(dirname(__FILE__) . '/get.likes.common.php');

	$query = "set @@wait_timeout=1200";
	Member::connection()->query($query);
	
	$start 	= 0;
	$limit 	= 100;		
	$i 		= 1;
	
	$processor = new PostsFeedbackProcessor;
	$processor->debug = true;
	
	do {

		unset($posts);
		unset($processed);

		//LAST 48 HRS...
		$query = "  SELECT *
					FROM `post`
					WHERE created_at <= ( NOW( ) - INTERVAL 2 DAY )
					AND created_at >= ( NOW( ) - INTERVAL 3 DAY )
					AND status != 'deleted'
					ORDER BY member_id ASC
					LIMIT $limit OFFSET $start";
		
		$posts = Post::find_by_sql($query);
		$processed = $processor->Process( $posts, true);

		echo "Completed round $i for a total of $processed requests with " . memory_get_peak_usage(true)/1024/1024 . "MB used" . PHP_EOL . PHP_EOL;
		
		$start += $limit;
		$i++;
				
	} while (count($posts) == $limit);

