#!/usr/bin/env php
<?
	/**************
	
	/usr/local/php53/bin/php /home/abubic/city-blast.com/scripts/accounts.payable.byaffiliate.php
	
	***************/
	
	// Reports on payables recorded in the PREVIOUS month. For example, if the script is run on April 1 (or April 5, or April 15), it will report on payables recorded from March 1 to March 31.

	require realpath(dirname(__FILE__) . '/../public/env.php');	
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

	ini_set('display_errors',1);




function create_report($affiliate_name, $affiliate_id)
{
	
	$conditions = "created_at BETWEEN date_format(NOW() - INTERVAL 1 MONTH, '%Y-%m-01') AND last_day(NOW() - INTERVAL 1 MONTH) AND affiliate.member_id = " . $affiliate_id;

	$emailer   = Zend_Registry::get('alertEmailer');

//echo "<pre>";
//print_r($emailer);
//exit();

	
	$payables  = Payable::all(array("conditions" => $conditions, "joins" => "JOIN affiliate ON payable.affiliate_id = affiliate.id", 'order' => 'affiliate_id'));
	$csvfile   = '/tmp/payables-' . date('Y-m-d') . '--'.$affiliate_id.'.csv';

	ob_start();

?>

Payouts recorded to <?=$affiliate_name;?> for the previous month: 

<?php if (count($payables)) : ?>
Payment date     Payout amount         Affiliate                    Member
-------------------------------------------------------------------------------------------
<?php	

	$fp = fopen($csvfile, 'w');

	fputcsv($fp, array('Payment date', 'Payout amount', 'Affiliate', 'Member' ));

	$total = 0;

	foreach ($payables as $payable) :

		$affiliate_owner_name = '--';
		$member_name = '--';
		$total += $payable->amount;
		
		if ($payable->affiliate) $affiliate_owner_name = $payable->affiliate->owner->name();
		if ($payable->member) $member_name = $payable->member->name(); 
		
		fputcsv($fp, array($payable->payment->created_at->format('Y-m-d'), '$' . number_format($payable->amount, 2), $affiliate_owner_name, $member_name ));
?><?= $payable->payment->created_at->format('Y-m-d') ?>    <?= str_pad('$' . number_format($payable->amount, 2), 16, " ", STR_PAD_LEFT) ?>         <?php echo str_pad($affiliate_owner_name, 28); ?> <?php echo str_pad($member_name, 24); ?> 
<?php 

	endforeach; 

	echo PHP_EOL . "Total payouts: \${$total}";	
	fputcsv($fp, array('Total', '$' . $total));
	fclose($fp);	

else :
	
?>No payouts found.<?php

	endif;

	
	$email = ob_get_clean();
	
	$subject = "Affiliate payout report - " . $affiliate_name;
	
	if (file_exists($csvfile)) $emailer->attachFile($csvfile);
	
	$emailer->send($subject, $email, 'status-payment-batch');
	
	echo $email;

}




//MAIN PROGRAM.......

$affiliate_array[1]['name'] = "Jim Ouellette";
$affiliate_array[1]['id'] = 1002741;

$affiliate_array[2]['name'] = "Hoss Pratt";
$affiliate_array[2]['id'] = 1002106;

$affiliate_array[3]['name'] = "Simon Giannini";
$affiliate_array[3]['id'] = 191;

$affiliate_array[4]['name'] = "Royal LePage";
$affiliate_array[4]['id'] = 1182;

$affiliate_array[5]['name'] = "Shawn Mahdavi";
$affiliate_array[5]['id'] = 1000531;


$affiliate_array[5]['name'] = "James Kupka";
$affiliate_array[5]['id'] = 1000452;

$affiliate_array[5]['name'] = "Leila Talbova";
$affiliate_array[5]['id'] = 55;

$affiliate_array[5]['name'] = "Scott Smith / CREW";
$affiliate_array[5]['id'] = 1002554;


$affiliate_array[6]['name'] = "William Ripley";
$affiliate_array[6]['id'] = 1003098;

$affiliate_array[7]['name'] = "Chris Leader";
$affiliate_array[7]['id'] = 1742;



//Create a report for each AFFILIATE
foreach($affiliate_array as $affiliate)
{
	//echo "<pre>";
	//print_r($affiliate);
	
	create_report($affiliate['name'], $affiliate['id']);		
}


?>