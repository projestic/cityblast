<?php
/* 

	/usr/local/php53/bin/php get.treb.php

 * This script is intended for download CSV for un/available listings
 *
 * allow parameters like
 *
 * cl$ sudo php get.treb.php time=200111215&debug=1
 */
$timestamp1	= microtime(TRUE);

date_default_timezone_set('America/New_York');


//download directory/folder to store CSV
$download_folder= dirname(__FILE__) .'/treb-files/';

//source URL of CSV
#$src_url		= 'http://trebdata.trebnet.com/';
$src_url		= 'http://3pv.torontomls.net/data3pv/DownLoad3PVAction.asp';

echo "Process started\r\n";


//catch paramater passed through either URL or command line
$time = strtotime('now');
if (!empty($_GET['time'])) 
{
	$time	= strtotime(urldecode($_GET['time']));
} elseif (isset($argc) && $argc > 1) 
{
	$args	= parse_args($argv[1]);
	$time	= strtotime($args['time']);
}


//form fields and their values for available listing
$form_avail		= array(
	'user_code'	=> 't011cty',
	'password'	=> 'T22cty',
	'sel_fields'=> '*',
	'dlDay'		=> date('d', $time),
	'dlMonth'	=> date('m', $time),
	'dlYear'	=> date('Y', $time),
	'order_by'	=> '',
	'au_both'	=> 'avail',
	'dl_type'	=> 'file',
	'use_table'	=> 'MLS',
	'incl_names'=> 'yes',
	'send_done'	=> 'no',
	'submit1'	=> 'Submit',
	'query_str'	=> "lud>='". date('Ymd', $time) ."'"
);


//form fields and their values for unavailable listings
$form_unavail	= array(
	'user_code'	=> 't011cty',
	'password'	=> 'T22cty',
	'sel_fields'=> '*',
	'dlDay'		=> date('d', $time),
	'dlMonth'	=> date('m', $time),
	'dlYear'	=> date('Y', $time),
	'order_by'	=> '',
	'au_both'	=> 'unavail',
	'dl_type'	=> 'file',
	'use_table'	=> 'MLS',
	'incl_names'=> 'yes',
	'send_done'	=> 'no',
	'submit1'	=> 'Submit',
	'query_str'	=> "lud>='". date('Ymd', $time) ."'"
);

//download available listings
$destination	= $download_folder .'avail-'. date('Y-M-d', $time) .'.csv';

$request		= new RequestCSV($src_url, 'POST', $form_avail, NULL, $destination);
$result		= $request->process();

if ($result) 
{
	echo "File for available listing has been downloaded successfully at \"". $destination ."\"\r\n";
	
	mail("alen@alpha-male.tv","Get TREB SUCCESS: ". filesize($destination), "File for available listing has been downloaded successfully at \"". $destination ."\"\r\n");
	#echo "File for available listing has been downloaded successfully at \"". $destination ."\"\r\n";
	
}
if (!empty($_REQUEST['debug'])) 
{
	print_r($request->get_request_status());
}


//download unavailable listings
$destination	= $download_folder .'unavail-'. date('Y-M-d', $time) .'.csv';

$request		= new RequestCSV($src_url, 'POST', $form_unavail, NULL, $destination);
$result			= $request->process();

if ($result) {
	echo "File for unavailable listing has been downloaded successfully at \"". $destination ."\"\r\n";
}
if (!empty($_REQUEST['debug'])) {
	print_r($request->get_request_status());
}

echo "Process ended successfully.\r\n";
echo "takes ". round((microtime(TRUE) - $timestamp1), 3) ." sec\r\n";


/**
 * class for download CSV file
 */
class RequestCSV
{
	private $is_curl		= FALSE;
	private $request_url	= NULL;
	private $url_data		= array();
	private $data			= array();
	private $data_str		= NULL;
	private $headers		= array();
	private $has_cookie		= FALSE;
	private $request_method	= 'POST';
	private $request_status	= NULL;
	private $destination	= NULL;
	private $file_handler	= NULL;

	public function  __construct($url, $method, $data = array(), $ext_headers = array(), $destination = NULL) {
		if (function_exists('curl_init')) {
			$this->is_curl	= TRUE;
		}
		#$this->is_curl	= FALSE;

		$this->request_url	= $url;
		$this->url_data		= parse_url($this->request_url);

		if ($data) {
			$this->set_data($data);
		}

		if ($ext_headers) {
			$this->set_headers($ext_headers);
		}

		if ($destination) {
			$this->set_destination($destination);
		}

		if ($method) {
			$this->set_method($method);
		}
	}

	public function set_data(Array $data) {
		$this->data	= $data;
		$str	= '';
		foreach ($this->data as $key => $value) {
			$str	.= $key .'='. urlencode($value) .'&';
		}
		$str	= rtrim($str, '&');
		$this->data_str	= $str;
		return $this;
	}

	public function get_data() {
		return $this->data;
	}

	public function set_headers(Array $headers) {
		$this->headers	= $headers;
		return $this;
	}

	public function get_headers() {
		return $this->headers;
	}

	public function set_destination($destination) {
		$this->destination	= $destination;
		return $this;
	}

	public function get_destination() {
		return $this->destination;
	}

	public function set_method($method) {
		$this->request_method	= strtoupper($method);
		return $this;
	}

	public function get_method() {
		return $this->request_method;
	}

	public function get_request_status() {
		return $this->request_status;
	}

	public function process() {
		if ($this->destination) {
			try {
				$this->file_handler	= fopen($this->destination , 'w');
			} catch (Exception $e) {
				$this->exception($e);
				return FALSE;
			}
		}

		try {
			if ($this->is_curl) {
				$result	= $this->process_curl();
			} else {
				$result	= $this->process_socket();
			}
		} catch (Exception $e) {
			$this->exception($e);
		}
		
		if ($this->destination) {
			fclose($this->file_handler);
			/*
			 * check if no records found
			 */
			$content	= file_get_contents($this->destination);
			if (strstr($content, 'No Records Found')) {
				try {
					$this->file_handler	= fopen($this->destination , 'w');
				} catch (Exception $e) {
					$this->exception($e);
					return FALSE;
				}
				fwrite($this->file_handler, 'No Records Found');
				fclose($this->file_handler);
			}
			
			chmod($this->destination, 0777);
			return TRUE;
		} else {
			return $result;
		}
	}

	private function process_curl() {

		$request	= curl_init();
		curl_setopt($request, CURLOPT_URL, $this->request_url);
		#curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);

		#curl_setopt($request, CURLOPT_HEADER, 1);
		curl_setopt($request, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($request, CURLOPT_REFERER, 'http://trebdata.trebnet.com/');

		if ($this->request_method == 'POST') {
			curl_setopt($request, CURLOPT_POST, count($this->data));
			curl_setopt($request, CURLOPT_POSTFIELDS, $this->data_str);
		}

		curl_setopt($request, CURLOPT_COOKIEJAR, '/tmp/cookies.txt');
		curl_setopt($request, CURLOPT_COOKIEFILE, '/tmp/cookies.txt');
		curl_setopt($request, CURLOPT_FOLLOWLOCATION, 1);

		if ($this->file_handler) {
			curl_setopt($request, CURLOPT_FILE, $this->file_handler);
		}
		curl_setopt($request, CURLOPT_TIMEOUT, 60);

		$result		= curl_exec($request);
		if ($result === false){
			throw new Exception('Curl error (' . curl_error($request) . ')' ."\r\n");
		}

		$this->request_status	= curl_getinfo($request);

		if ($this->request_status['http_code'] != 200) {
			// not a valid response from GA
			if ($this->request_status['http_code'] == 400) {
				throw new Exception('Bad request (' . $this->request_status['http_code'] . ') url: ' . $this->request_url ."\r\n");
			}
			if ($this->request_status['http_code'] == 403) {
				print_r($request);
				echo "\n";
				print_r($result);
				echo "\n";
				print_r($this->request_status);
				echo "\n";

				throw new Exception('Access denied (' . $this->request_status['http_code'] . ') url: ' . $this->request_url ."\r\n");
			}
			throw new Exception('Not a valid response (' . $this->request_status['http_code'] . ') url: ' . $this->request_url ."\r\n");
		}

		curl_close($request);
		return $result;
	}

	private function process_socket() {
		$fs		= @fsockopen($this->url_data['host'], 80, $errno, $errstr);
		$result	= '';
		if ($fs) {

			if ($this->request_method == 'POST') {
				$this->set_socket_headers_post();
			} elseif ($this->request_method == 'GET') {
				$this->set_socket_headers_get();
			}

			foreach ($this->headers as $hd) {
				fputs($fs, $hd);
			}
			stream_set_timeout($fs, 60);

			while (!feof($fs)) {
				$content	= fread($fs, 4096);
				$result	.= $content;
			}

			if ($result) {
				$hds	= explode("\r\n\r\n", $result, 2);
				if (is_array($hds) && count($hds) > 1) {
					$result	= $hds[1];
				} else {
					$hds	= explode("\r\r", $result, 2);
					if (is_array($hds) && count($hds) > 1) {
						$result	= $hds[1];
					}
				}
			}
			fwrite($this->file_handler, $result);

			if (!$this->has_cookie && preg_match("/Set\-Cookie\: /i", $content)) {echo 1234;
				$cookie	= explode("Set-Cookie: ", $content, 2);
				$cookie	= $cookie[1];
				$cookie = explode("\r",$cookie);
				$cookie	= $cookie[0];
				$cookie	= str_replace("path=/","",$cookie);
				fclose($fs);
				fclose($this->file_handler);
				$this->headers	= array();

				$cookies		= explode(';', $cookie, 2);
				$cookie_parts	= explode('=', $cookies[0], 2);
				setcookie($cookie_parts[0], $cookie_parts[1]);
				return $this->process();
			}
			#pre(stream_get_meta_data($fs));
			#pre($this->headers);

			fclose($fs);
			$this->request_status	= $errno .': '. $errstr;
		}
		return $result;
	}

	private function set_socket_headers_post() {
		$headers	= array();
		$headers[]	= "POST ". $this->url_data['path'] ." HTTP/1.1\r\n";
		$headers[]	= "Host: ". $this->url_data['host'] ."\r\n";

		$cookie_str	= '';
		foreach ($_COOKIE as $key => $value) {
			$cookie_str	.= $key .'='. $value .';';
		}
		if ($cookie_str) {
			$headers[]	= "Cookie: ". $cookie_str ."\r\n";
			$this->has_cookie	= TRUE;
		} else {
			$this->has_cookie	= FALSE;
		}

		$this->headers	= array_merge($headers, $this->headers);
		$this->headers[]= "Content-Type: application/x-www-form-urlencoded\r\n";
		$this->headers[]= "Content-Length: ". strlen($this->data_str) ."\r\n";
		$this->headers[]= "Connection: close\r\n\r\n";
		$this->headers[]= $this->data_str ."\r\n";
		return TRUE;
	}

	private function set_socket_headers_get() {
		$headers	= array();
		$headers[]	= "GET ". $this->url_data['path'] ."?". $this->data_str ." HTTP/1.1\r\n";
		$headers[]	= "Host: ". $this->url_data['host'] ."\r\n";

		$cookie_str		= '';
		foreach ($_COOKIE as $key => $value) {
			$cookie_str	.= $key .'='. $value .';';
		}
		if ($cookie_str) {
			$headers[]		= "Cookie: ". $cookie_str ."\r\n";
			$this->has_cookie	= TRUE;
		} else {
			$this->has_cookie	= FALSE;
		}

		$this->headers	= array_merge($headers, $this->headers);
		$this->headers[]= "Connection: close\r\n\r\n";
		return TRUE;
	}

	private function exception(Exception $e) {
		echo "Error at ". $e->getLine() ." of ". $e->getFile() ."\r\n". $e->getMessage() ."\r\n". $e->getTraceAsString() ."\r\n";
		return $this;
	}
}


function parse_args($arg_str) {
	$args	= array();
	$tmp	= explode('&', $arg_str);
	foreach ($tmp as $param) {
		if ($param) {
			$tmp2	= explode('=', $param, 2);
			if (count($tmp2) > 1) {
				$args[$tmp2[0]]	= $tmp2[1];
			} else {
				$args[]	= $tmp2[0];
			}
		}
	}
	return $args;
}

function pre($content) {
	echo '<pre>';
	print_r($content);
	echo '</pre>';
}

?>