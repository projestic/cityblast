#!/bin/bash -e


ENV=$1

if [ -z "$1" ]; then echo "Environment required."; exit; fi

APP="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

APP="$(dirname "$APP")"
APP="$(dirname "$APP")"

TMP='/tmp/cron.txt';

WARNING1="# WARNING: Commands between 'begin' and 'end' markers are provisioned automatically on deployment."
WARNING2="# Please do not alter markers or text between them manually unless you know exactly what you are doing."


(
	
	# Wait for lock
	flock -x -w 120 200 || exit 1

	crontab -l > $TMP

	# If the crontab is entirely empty, we need it to at least contain a CRLF for sed to attach the warning
	if [ ! -s $TMP ]; then echo '' >> $TMP; fi

	if ! grep -q "$WARNING1" "$TMP"; then
		sed -i "1s/^/$WARNING1\n$WARNING2\n\n/" $TMP
	fi

	# Update the appropriate crontab section

	CRON=$APP/setup/cron/$ENV.cron

	start="# Begin $ENV"
	end="# End $ENV"
	
	if [ -e "$CRON" ]; then
	
		if grep -q "$start" $TMP && ! grep -q "$end" $TMP; then
			echo "Crontab contains a start marker but not an end marker for $ENV. Cannot proceed. Please manually delete the entire '$ENV' section in the crontab and rerun this command."
			exit 1;
		fi

		sed -i "\:$start\$:,\:$end\$:d" $TMP

		echo "$start" >> $TMP
		echo '' >> $TMP
		cat $CRON >> $TMP
		echo '' >> $TMP
		echo "$end" >> $TMP
		echo '' >> $TMP
		
	fi

	crontab $TMP
	rm $TMP


) 200>/tmp/cronprovisioning.lock



