#!/bin/bash -e

# CityBlast post-deployment script


# Exit if any command fails.
set -e

APP="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

APP="$(dirname "$APP")"
APP="$(dirname "$APP")"

printf "Running post-deployment script for $APP ... "

sudo chown -R shp-admin:www $APP/log
sudo chmod -R 664 $APP/log
sudo chmod 774 $APP/log
sudo chown -R shp-admin:www $APP/cache
sudo chmod -R 774 $APP/cache
sudo chown shp-admin:www $APP/public/tmp
sudo chown -R shp-admin:www $APP/public/tmp
chmod -R g+rwx $APP/public/tmp
sudo chown -R shp-admin:www $APP/public/images/listings
sudo chmod -R g+rwx $APP/public/images/listings
sudo chown -R shp-admin:www $APP/public/images/agents
sudo chown -R shp-admin:www $APP/cache
sudo chmod -R g+rwx $APP/public/images/agents
sudo chown -R shp-admin:www $APP/public/images/listings

curl -s --header 'Host: www.cityblast.com' http://localhost/apc_clear.php > /dev/null

printf "done.\n"