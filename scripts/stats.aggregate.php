#!/usr/bin/env php
<?php

include realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors', 1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

Logger::logTimeMarker();

$start_date = !empty($argv[1]) ? trim($argv[1]) : null;
$start_date = ($start_date == 'all') ? date('Y-01-01', strtotime('-2years')) : $start_date;
$start_date = empty($start_date) ? date('Y-m-01', strtotime('-1month')) : date('Y-m-d', strtotime($start_date));
$end_date = !empty($argv[2]) ? trim($argv[2]) : null;
$end_date = empty($end_date) ? date('Y-m-d', strtotime('yesterday')) : date('Y-m-d', strtotime($end_date));

Logger::log('Start', Logger::LOG_TYPE_DEBUG);


ClickStatByDate::populateDateRange($start_date, $end_date);
ClickStatByDateListing::populateDateRange($start_date, $end_date);
ClickStatByDateMember::populateDateRange($start_date, $end_date);
ClickStatByMemberListing::populate();

FacebookStatByDate::populateDateRange($start_date, $end_date);
FacebookStatByDateListing::populateDateRange($start_date, $end_date);
FacebookStatByDateMember::populateDateRange($start_date, $end_date);

EngageByDate::populateDateRange($start_date, $end_date);
EngageByWeek::populateDateRange($start_date, $end_date);
EngageByMonth::populateDateRange($start_date, $end_date);
EngageByYear::populateDateRange($start_date, $end_date);

EngageByDateListing::populateDateRange($start_date, $end_date);
EngageByMonthListing::populateDateRange($start_date, $end_date);
EngageByWeekListing::populateDateRange($start_date, $end_date);
EngageByYearListing::populateDateRange($start_date, $end_date);

EngageByDateMember::populateDateRange($start_date, $end_date);
EngageByWeekMember::populateDateRange($start_date, $end_date);
EngageByMonthMember::populateDateRange($start_date, $end_date);
EngageByYearMember::populateDateRange($start_date, $end_date);

Logger::log('Finished', Logger::LOG_TYPE_DEBUG);