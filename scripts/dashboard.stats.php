#!/usr/bin/env php
<?
 	
	/**************
	
	/usr/local/php53/bin/php /home/abubic/cityblast.com/scripts/dashboard.stats.php
	
	/usr/local/php53/bin/php /home/abubic/alen.cityblast.com/scripts/dashboard.stats.php
	
	***************/
	
	//define('APPLICATION_ENV', 'jose');
	require realpath(dirname(__FILE__) . '/../public/env.php');	
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');


	ini_set('display_errors',1);



	//Make sure the connection doesn't time out
	$query = "set @@wait_timeout=1200";
	Listing::connection()->query($query);
	//echo "\n\n\n";
	

	$conditions  = "	( status = 'active' and ( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) ";
	$conditions .= "or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) and payment_status != 'cancelled' and payment_status != 'payment_failed' and (next_payment_date IS NULL or next_payment_date >= NOW()) )";
	$conditions .= " OR account_type='free' ";
			
	$conditions_array['conditions'] = $conditions;
	
	$members = Member::find("all", $conditions_array);
	
	$count = 1;
	$total_clicks 		= 0;
	$total_likes 		= 0;
	$total_comments	= 0;
	$total_shares		= 0;	
	$buzz_counter 		= 0;
	$buzz_total		= 0;
	
	$lowest 			= 1;
	$highest			= 0.0000000001;

	
	foreach($members as $member)
	{
		//echo $count++ . " " . $member->id . " " . $member->first_name . "\n";						
		//echo $count++ . "\n";
		
				
		//Let's figure out average clicks
		$clicks = $member->clicks_trailing_thirty();
		
		$total_clicks += $clicks;

		$return_array		= $member->likes_and_comments_trailing_thirty();
		$total_likes 		+= $return_array['likes'];
		$total_comments 	+= $return_array['comments'];

		$fb_share_count = $member->facebook_shares_trailing_thirty();
		$tw_share_count = $member->twitter_shares_trailing_thirty();
		$li_share_count = $member->linkedin_shares_trailing_thirty();
		$em_share_count = $member->email_shares_trailing_thirty();
		
		$ph_share_count = $member->phonecalls_trailing_thirty();
		$bk_share_count = $member->booking_trailing_thirty();
		
		$your_total_shares = $fb_share_count + $tw_share_count + $li_share_count + $em_share_count + $ph_share_count + $bk_share_count;	
		
		$total_shares += $your_total_shares;
		
		
		$total_touches = $clicks + $return_array['likes'] + $return_array['comments'] + $your_total_shares;
		
		$posts = $member->monthly_posts();
		$reach = $member->total_reach();
		
		if($posts && $reach)
		{	
			//TOTAL NUM TOUCHES / TOTAL NUMBER OF POSTS / REACH
			$raw_buzz = (($total_touches / $posts) / $reach);				
					
			//echo $raw_buzz . "\n";
			
			$buzz_counter++;
			$buzz_total += $raw_buzz;
			
			if($raw_buzz < $lowest)
			{ 
				$lowest = $raw_buzz;
				echo "LOWEST: ".$total_touches . " / " . $posts . " / " . $reach . " --- ".$member->id. " " . $member->first_name. " " . $member->last_name ."\n";
			}
			if($raw_buzz > $highest)
			{ 
				$highest = $raw_buzz;
				echo "HIGHEST: ".$total_touches . " / " . $posts . " / " . $reach . " --- ".$member->id. " " . $member->first_name. " " . $member->last_name ."\n";
			}
		}
		
		$count++;
	}

	echo "Total: " .$count."\n";

	echo "clicks:".$total_clicks . "\n";
	echo "likes:".$total_likes . "\n";
	echo "comments:".$total_comments . "\n";
	echo "shares:".$total_shares . "\n";
	echo "buzz:".$buzz_total."\n";
	
	if( $count ) echo "Averages CLICKS: " . ($total_clicks / $count) . "\n";
	if( $count ) echo "Averages LIKES: " . ($total_likes / $count) . "\n";
	if( $count ) echo "Averages COMMENTS: " . ($total_comments / $count) . "\n";
	if( $count ) echo "Averages SHARES: " . ($total_shares / $count) . "\n";
	
	if( $buzz_counter ) echo "Averages BUZZ: " . ($buzz_total / $buzz_counter)."\n";
	
	echo "Lowest:".$lowest."\n";
	echo "Highest:".$highest."\n";
	
	
	$dash = DashboardStats::find(1);
	//$dash =  new DashboardStats();
	
	if( $count ) 
	{
		$dash->avg_clicks = ($total_clicks / $count);
		$dash->avg_likes = ($total_likes / $count);
		$dash->avg_comments = ($total_comments / $count);
		$dash->avg_shares = ($total_shares / $count);
	}
	else
	{
		$dash->avg_clicks = 0;
		$dash->avg_likes = 0;
		$dash->avg_comments = 0;
		$dash->avg_shares = 0;		
	}
	
	if( $buzz_counter ) 
	{
		$dash->avg_buzz = ($buzz_total / $buzz_counter);	
	}
	else
	{
		$dash->avg_buzz = 0;
	}
		
	//$dash->id = 1;
	$dash->save();
	
	
/**
Clicks: 300.84485981308			= 300
Likes: 3.3355140186916		30	= 99.9	
Comments: 0.78785046728972	50	= 39.5
Shares: 0.29532710280374		75	= 21.75
**/
	
?>