#!/usr/bin/env php
<?php

	/****
	
	/usr/local/php53/bin/php get.likes.php
	
	****/

	ini_set('display_errors', 1);

	// disabled, if we need to increase memory on this script, something's wrong.
	// ini_set('memory_limit', '500M');	

	include realpath(dirname(__FILE__) . '/../public/env.php');
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	require realpath(dirname(__FILE__) . '/get.likes.common.php');

	$query = "set @@wait_timeout=1200";
	Member::connection()->query($query);
	
	$start 	= 0;
	$limit 	= 100;		
	$i 		= 1;
	
	$processor = new PostsFeedbackProcessor;
	$processor->debug = true;
	
	do {

		unset($posts);
		unset($processed);

		echo "select * from post limit $limit offset $start" . PHP_EOL . PHP_EOL;
	
		//$query = "select * from post where id=39";
		
		$posts = Post::find( 'all', array( 'limit' => $limit, 'offset' => $start) );
		$processed = $processor->Process( $posts, true);

		echo "Completed round $i for a total of $processed requests with " . memory_get_peak_usage(true)/1024/1024 . "MB used" . PHP_EOL . PHP_EOL;
		
		$start += $limit;
		$i++;
				
	} while (count($posts) == $limit);
	
