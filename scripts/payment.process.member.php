#!/usr/bin/env php
<?
require realpath(dirname(__FILE__) . '/../public/env.php');	
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors',1);

$alert_emailer = Zend_Registry::get('alertEmailer');

Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::log('Starting payment process member');

$member_id = !empty($argv[1]) ? $argv[1] : null;

Logger::startSection();
if (empty($member_id)) {
	Logger::log('No member id provided');
	exit;
} else {
	Logger::log('Selected member id: ' . $member_id);
}

$today	=  date("Y-m-d");

$conditions	=	"id = '" . addslashes($member_id) . "' ";	
$conditions	.= "and next_payment_date <= '" . addslashes($today) . "'  and account_type='paid' ";
$conditions .= "and payment_status != 'cancelled' and 
				payment_status != 'signup' and 
				payment_status != 'free' and 
				payment_status != 'signup_expired' and 
				payment_status != 'extended_trial' and 
				payment_status != 'payment_failed' ";

$members  = Member::all(array("conditions" => $conditions));

if (empty($members)) {
	Logger::log('The selected member is not in the correct state for payment');
	exit;
} 

$payment_counter = 0;
$counter = 0;
$msg = '';
foreach($members as $member) 
{
	Logger::log('Processing payment for: ' . $member->id . ' ' .$member->first_name . ' ' . $member->last_name);
	
	Logger::startSection();
	Logger::log('Loading previous payment details');
	
	$last_payment   =	Payment::find(array('conditions' => 'member_id='.$member->id.' and payment_type="clientfinder"', 'limit' => 1, 'order'=>'created_at desc, id desc'));
	if($last_payment)
	{
		
		$msg .= "Processing payment for: ". $member->id . " " .$member->first_name . " " . $member->last_name . " RESULT: ";

		Logger::log('Payment gateway is: ' . $member->payment_gateway);
		
		$counter++;
		
		if($member->processRecurringPayment())
		{
			Logger::log('Payment processed successfully');
			$msg .= $member->payment_gateway . " SUCCESS\n";
		}
		else
		{
			Logger::log('Payment processing failed');
			$msg .= $member->payment_gateway . " FAIL\n";
		}

	}
	else
	{
		Logger::log('Unable to find previous payment details', null, true);
		
		$alert_emailer->send(
			'Payment processing error - member_id = ' . $member->id,
			'Unable to find previous payment details for member_id ' . $member->id,
			'error-payment',
			true
		);
	}

	Logger::endSection();
}

$alert_emailer->send(
	'Payment processing batch completed - ' .  $payment_counter . ' of ' . $counter . ' payments',
	"FAILED: " . ($counter - $payment_counter) . "\n\n" . $msg,
	'status-payment-batch',
	true
);