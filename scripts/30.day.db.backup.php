<?php

	/****
	
	/usr/local/php53/bin/php /home/abubic/city-blast.com/scripts/30.day.db.backup.php
	
	****/
	
	
	$timestamp1	= microtime(TRUE);
	$convertUtf8Chars	= TRUE;
	
	date_default_timezone_set('America/New_York');
	try 
	{
		if (!defined('SCRIPTS_PATH')) 
		{
			define('SCRIPTS_PATH', dirname(__FILE__));
		}
		require SCRIPTS_PATH . '/../public/env.php';
		require SCRIPTS_PATH . '/../app/configs/environment.php';
	} 
	catch (Exception $ex) 
	{
		echo "There was a problem getting the required includes";
		exit();
	}
	
	$backup_dir		= SCRIPTS_PATH .'/../dbbackups';
	
	if (!isset ($application) || !($db = $application->getOption('db'))) 
	{
		echo "Application configuration for database is not available!" . PHP_EOL;
	}
	
	if (!file_exists($backup_dir)) 
	{
		mkdir($backup_dir);
		chmod($backup_dir, 0777);
		echo "directory \"". $backup_dir ."\" has been created!". PHP_EOL;
	}
	
	$file_postfix	= date('Y-m-d-His', $timestamp1);
	$backup_sql		= $backup_dir .'/'. $file_postfix .'-backup.sql';
	/**
	 * exporting (also the backup)
	 */
	$backup_command	= shell_exec('mysqldump --opt -h '. $db['host'] .' -u'. $db['username'] .' -p'. $db['password'] .' '. $db['database'] .' > '. $backup_sql);
	chmod($backup_sql, 0777);
	
	echo "A backup of Database \"". $db['database'] ."\" has been taken at \"". $backup_sql ."\"!". PHP_EOL;
	
	
	
	//This code is incorrect, it deletes ALL backups... we want to only delete backups that are older than 90 days
	
	/******
	$file_list	= scandir($backup_dir); #print_r($file_list);
	foreach ($file_list as $file) {
		$file_path	= $backup_dir ."/". $file;
		if (!is_dir($file_path) && $file_path != $backup_sql && unlink($file_path)) {
			echo "Older bakup \"". $file_path ."\" has been deleted!". PHP_EOL;
		}
	} *****/
	
	echo PHP_EOL ."It takes ". round((microtime(TRUE) - $timestamp1), 3) ." seconds". PHP_EOL;

?>
