#!/usr/bin/env php
<?
 	
	/**************
	
	/usr/local/php53/bin/php inventory.assignment.php
	
	***************/
	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	
	
	include_once "publish.post.inc.php";
	
	ini_set('display_errors', 1);


	
	//Make sure the connection doesn't time out
	$query = "set @@wait_timeout=1200";
	Listing::connection()->query($query);
	echo "\n\n\n";
	
	$cities = City::all();
	
	foreach($cities as $city)
	{
		echo "Working on: " .$city->name."\n";
		
		$conditions = "( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) AND status='active' and city_id=".$city->id;	
		$members = Member::find('all', array('order' => 'id ASC', 'conditions' => $conditions));
		
		
		$hoppers = array();
		$counter = 1;
		$total_inv = 0;
		
		for($i=1; $i <= $city->number_of_inventory_units;$i++)
		{
			$inventory_count_by_unit[$i] = 0;	
		}

		
		foreach($members as $member)
		{
			//print_r($member);
		
			$member_inv = 0;
			if($member->facebook_publish_flag && $member->facebook_fails < 3)
			{
				//echo "Adding FB: " .$member->facebook_friend_count . " to total inventory\n";
				$total_inv += $member->facebook_friend_count;
				$member_inv += $member->facebook_friend_count;
			}

			if($member->twitter_publish_flag && $member->twitter_fails < 3 && $member->twitter_access_token)
			{
				//echo "Adding TW: " .$member->twitter_followers . " to total inventory\n";
				$total_inv += $member->twitter_followers;
				$member_inv += $member->facebook_friend_count;
			}
			
			if($member->linkedin_publish_flag && $member->linkedin_fails < 3 && $member->linkedin_access_token)
			{
				//echo "Adding LN: " .$member->linkedin_friends_count . " to total inventory\n";
				$total_inv += $member->linkedin_friends_count;
				$member_inv += $member->facebook_friend_count;
			}			
			
			echo $member->email . " adds ".$member_inv . " of total inventory\n";


			
			asort($inventory_count_by_unit);
			
			foreach($inventory_count_by_unit as $key => $value)
			{
				$INVENTORY_UNIT_ID = $key;
				break;	
			}
		
		$member->inventory_id = 	$INVENTORY_UNIT_ID;
		$member->save();
		
		$inventory_count_by_unit[$INVENTORY_UNIT_ID]+=$member_inv;
			
			echo "INVENTORY UNIT ID=".$INVENTORY_UNIT_ID; 
			echo "\n";
			
		}
		
		echo "\n\n";
		echo "Your TOTAL inventory is:".$total_inv."\n\n\n";

print_r($inventory_count_by_unit);

		foreach($members as $member)
		{
			echo $member->email . " inventory_id: ".$member->inventory_id."\n";
		}

		//exit();

	}
	
?>