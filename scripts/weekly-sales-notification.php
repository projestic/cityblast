#!/usr/bin/env php
<?php

include realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

echo "Generating report\n";

// Calculate year, week number, from and to date for the previous week
$from = date('Y-m-d', strtotime('-7 days'));
$to = date('Y-m-d', strtotime('-1 days'));
$year = date('Y', strtotime('-1 days'));
$week_number = date('W', strtotime('-1 days'));

$sql = "
	SELECT 
		m.id, 
		m.first_name,
		m.last_name, 
		(
			SELECT 
				COUNT(mem.id)
			FROM member mem
			INNER JOIN payment p 
				ON p.member_id = mem.id AND p.amount > 0
				AND DATE(p.created_at) BETWEEN '" . $from . "' AND '" . $to . "'
			WHERE mem.referring_affiliate_id = a.id 
		) AS count,
		(
			SELECT 
				IFNULL(SUM(p.amount), 0)
			FROM member mem
			INNER JOIN payment p 
				ON p.member_id = mem.id AND p.amount > 0
				AND DATE(p.created_at) BETWEEN '" . $from . "' AND '" . $to . "'
			WHERE mem.referring_affiliate_id = a.id
		) AS revenue,
		(
			SELECT 
				IFNULL(SUM(pay.amount), 0)
			FROM member mem
			INNER JOIN payment p 
				ON p.member_id = mem.id AND p.amount > 0
				AND DATE(p.created_at) BETWEEN '" . $from . "' AND '" . $to . "'
			INNER JOIN payable pay 
				ON pay.member_payment_id = p.id
			WHERE mem.referring_affiliate_id = a.id
		) AS commission
	FROM member m
	WHERE 
		(m.first_name = 'Jeff' AND m.last_name = 'Woods') OR
		(m.first_name = 'Mike' AND m.last_name = 'Smith') OR
		(m.first_name = 'Mike' AND m.last_name = 'Shimmin')
";

$members = Member::find_by_sql($sql);

echo "Saving reports data in the database\n";

// Save reports data in the database
foreach ($members as $member) {
	$weeklysales = WeeklySales::find(array(
		'year' => $year, 
		'week_number' => $week_number, 
		'member_id' => $member->id));
	
	if (!$weeklysales) {
		$weeklysales = new WeeklySales();
		$weeklysales->year = $year;
		$weeklysales->week_number = $week_number;
		$weeklysales->member_id = $member->id;
	}

	$weeklysales->volume = $member->revenue;
	$weeklysales->commission = $member->commission;
	$weeklysales->number_of_sales = $member->count;
	$weeklysales->save();
}

echo "Sending email\n";

// Send report by email
$config = Zend_Registry::get('config');
$view = Zend_Registry::get('view');
$mandrill = new Mandrill('WEEKLY_SALES_REPORT');
$mandrill->setFrom(COMPANY_NAME, HELP_EMAIL);
foreach ($config->{'notification-emailer'}->notify->{'weekly-sales-report'} as $email) {
	$mandrill->addTo('', $email);
}
$mandrill->send(
	'Weekly Sales Report', 
	$view->partial('email/weekly-sales-report.html.php', array('members' => $members, 'from' => $from, 'to' => $to))
);
