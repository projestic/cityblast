#!/usr/bin/env php
<?

/**************

/usr/local/php53/bin/php publish.post.php

***************/
include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');


ini_set('display_errors', 1);

$alert_emailer = Zend_Registry::get('alertEmailer');

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();

$ids = Member::connection()->query("SELECT id FROM member WHERE status='active' AND payment_status IN ('paid','signup','free','extended_trial')")->fetchAll();

shuffle($ids);

$total_members = count($ids);
Logger::log('Found ' . $total_members . ' members');

// Start at 8am... and go until 4pm
$work_day_minutes = 8 * 60 * 60;

if ($total_members > 10) {		
	$sleep = round($work_day_minutes / $total_members) - 8;
	if ($sleep < 1) $sleep = 1;
} elseif ($total_members > 2 and $total_members < 10) {
	$sleep = 600;
} else {
	$sleep = 1;
}

$sleep_interval = $sleep;

if ($sleep_interval > 10000) {
	$sleep_interval = 9999;
}

//For testing purposes... make the sleep interval small no matter what
if(APPLICATION_ENV == "alen") $sleep_interval = 3;

Logger::log('Sleep interval: ' . $sleep_interval);

foreach($ids as $id)
{
	$logger_indent_level = Logger::getIndentLevel();
	
	$member = Member::find($id);
	
	if (!$member) {
		Logger::log('Member not found: ' . $id);
		continue;
	}
	
	Logger::log('PUBLISHING: ' . $member->id . ' ' .$member->first_name . ' ' . $member->last_name);
	Logger::startSection();
	Logger::log(date('Y-m-d H:i:s'));
	
	$member_blast = null;

	//Am I an ACTIVE MEMBER?
	if($member->publishing_active())
	{
		Logger::log('Member is active and paid up');
		
		
		//If I haven't PUBLISHED today, go ahead and BLAST
		if(!$member->published_today())
		{
			Logger::log('Member has not published anything today');
			
			if($member->frequency_id == 7)
			{						
				$bingo = 1;
			
				Logger::log('FREQUENCY IS 7, publishing');
			}
			else
			{
				$bingo = $member->should_i_publish($member);

				Logger::log('FREQUENCY IS LESS than 7: ' . $member->frequency_id);
			}

				
			if ($bingo) 
			{		
				Logger::log('Member will publish this time based on probability');
				
				try {
					//Let's find out the next listing this particular member should publish...	
					$member_blast = $member->find_blast();
			
					if ($member_blast) 
					{

						if(isset($member_blast->listing_id) && !empty($member_blast->listing_id)) 
						{	
							// echo "publishing...\n";
							$member->publishBlast($member_blast);
							$member->rotateCurrentContent();
							
							//Ok, sleepy time
							sleep($sleep_interval);	
						}
					} else {
						Logger::log('I couldnt find a piece of unique content for this member.');
		
						$message      = "Tried to publish content for this member, but unfortunately NONE of the LOCAL or NATIONAL CONTENT in the HOPPER was unique to this MEMBER";
						$member->saveListingPost(null, $message, 'NONE', 0);
			
					}
				} catch(Exception $e) {
					$message = get_class($e) . ': ' . $e->getMessage();
					Logger::log($message);
					
					$alert_emailer->send('Publisher Exception, ' . $message, $message);
					
					$member->saveListingPost(null, $message, 'NONE', 0);
				}
			}
			else
			{
				Logger::log('Member will not publish this time based on probility');

				$message      = "Dice was ROLLED but did not hit. Don't publish this time based on probility. Move on to next hopper.";
				$member->saveListingPost(null, $message, 'NONE', NULL);

			}
		}	
		else
		{
			Logger::log('Member already published today');	
		}
		
	}
	else
	{
		
		Logger::log('Member inactive');
		Logger::startSection();
		Logger::log('ACCOUNT TYPE: '.$member->account_type);
		Logger::log('PAYMENT STATUS: '.$member->payment_status);


		$reason = "UNKNOWN: INACTIVE (For some reason). ACCOUNT TYPE:".$member->account_type." PAYMENT STATUS:".$member->payment_status;
		
		if($member->payment_status == "signup")
		{
			Logger::log('Member was created on: '.$member->created_at->format('Y-m-d'));
			Logger::log('14 days ago it was: '.date('Y-m-d',strtotime("-14 days")));
			
			
			if (strtotime($member->created_at->format('Y-m-d H:i')) >= strtotime("-14 days"))
			{
				Logger::log('14 day FREE TRIAL has EXPIRED!');
				$reason = "14 day FREE TRIAL has EXPIRED!";
			}
		}
		else
		{			
			if($member->account_type == "paid")
			{
				if($member->payment_status != "paid")
				{
					Logger::log('Member HAS NOT PAID');
					$reason = "The member HAS NOT PAID";
				}
			}				
		}
		
		$message      = "PUBLISH FAIL: ".$reason;
		$member->saveListingPost(null, $message, 'FACEBOOK', 0);			
		
		Logger::endSection();
	}
	
	Logger::endSection();
	
	Logger::setIndentLevel($logger_indent_level);
}

Logger::log('Finished');
