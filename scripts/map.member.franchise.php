#!/usr/bin/env php
<?php

/*****
/usr/local/php53/bin/php map.member.franchise.php
*****/

include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors', 1);

$alert_emailer = Zend_Registry::get('alertEmailer');

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

Logger::logTimeMarker();
Logger::log('Starting franchise mapper');


$members = new Blastit_ActiveRecord_IteratorBuffered('Member', array('conditions' => array('status = ?', 'active')));
$members->callbackAfterReLoad(function(){ 
	sleep(1); 
});

foreach ($members as $member) {
	Logger::log('Checking member: ' . $member->id);
	$member->mapFranchise(null, false);
}

Logger::log('Finished');