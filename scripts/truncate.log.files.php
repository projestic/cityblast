#!/usr/bin/env php
<?
require realpath(dirname(__FILE__) . '/../public/env.php');	
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

$emailer = Zend_Registry::get('alertEmailer');

$base_path = dirname(dirname(__FILE__)) . '/log';

$log_file_paths[] = $base_path . '/cityblast.log';
$log_file_paths[] = $base_path . '/mortgagemingler.log';
$log_file_paths[] = $base_path . '/payment.process.log';
$log_file_paths[] = $base_path . '/linkedin.status.log';
$log_file_paths[] = $base_path . '/account.payables.lw-retech.log';
$log_file_paths[] = $base_path . '/sitemap.log';

$truncated = array();

foreach($log_file_paths as $log_file_path)
{
	if (is_file($log_file_path)) {
		file_put_contents($log_file_path, '');
		$truncated[] = $log_file_path;
	}
}

$message = "Truncated: \n" . implode("\n", $truncated) . "\n";

echo $message;

$emailer->send(
	'Log files truncated', 
	$message, 
	'notice-logs-truncate'
);

