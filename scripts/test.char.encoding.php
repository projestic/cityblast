<?php
include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors', 1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();


function hex_to_bin($str) 
{
	$sbin = "";
	$len = strlen($str);
	for ($i = 0; $i < $len; $i += 2) {
		$sbin .= pack("H*", substr( $str, $i, 2 ));
	}
	return $sbin;
}


$srcChars = array(
	hex_to_bin('80'),
	hex_to_bin('82'),
	hex_to_bin('83'),
	hex_to_bin('84'),
	hex_to_bin('85'),
	hex_to_bin('86'),
	hex_to_bin('87'),
	hex_to_bin('88'),
	hex_to_bin('89'),
	hex_to_bin('8A'),
	hex_to_bin('8B'),
	hex_to_bin('8C'),
	hex_to_bin('8E'),
	hex_to_bin('91'),
	hex_to_bin('92'),
	hex_to_bin('93'),
	hex_to_bin('94'),
	hex_to_bin('95'),
	hex_to_bin('96'),
	hex_to_bin('97'),
	hex_to_bin('98'),
	hex_to_bin('99'),
	hex_to_bin('9A'),
	hex_to_bin('9B'),
	hex_to_bin('9C'),
	hex_to_bin('9E'),
	hex_to_bin('9F'),
);

$encodings = array('utf-8', 'windows-1251', 'iso-8859-1');

foreach ($srcChars as $src) {
	$hex = bin2hex($src);
	$hex2bin = hex_to_bin($hex);
	Logger::log('Bin src: ' . $src);
	Logger::log('Detected encoding: ' . mb_detect_encoding($src, $encodings));
	Logger::log('Hex: ' . $hex);
	Logger::log('Hex to bin: ' . $hex2bin);
	$sanitized = Blastit_Text::sanitizeOutput($src);
	$sanitizedHex = bin2hex($sanitized);
	Logger::log('Sanitized ' . $sanitized);
	Logger::log('Sanitized detected encoding: ' . mb_detect_encoding($sanitized, $encodings));
	Logger::log('Sanitized Hex: ' . $sanitizedHex);
	Logger::log('----------------------------');
}


