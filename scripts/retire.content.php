#!/usr/bin/env php
<?
 	
	/**************

	/usr/local/php53/bin/php /home/abubic/city-blast.com/scripts/retire.content.php
	
	***************/
	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	
	//include_once "publish.post.inc.php";
	
	ini_set('display_errors', 1);

	//Make sure the connection doesn't time out
	$query = "set @@wait_timeout=10000";
	Listing::connection()->query($query);
	echo "\n\n\n";

	//Check the BLAST QUEUE for items that should be "retired" because they're over the count...
	$publishing_queue = PublishingQueue::find("all");
	
	$seven_days_ago	    = strtotime(date('Y-m-d').' -7days');				

	foreach($publishing_queue as $publish)
	{
		$listing = $publish->listing;
		
		if($listing)
		{
			// Retiring manual blasts
			if(!empty($publish->listing->expiry_date))
			{
				if(	
					strtotime($publish->listing->expiry_date->format('Y-m-d')) <= strtotime(date('Y-m-d')) && 
					$publish->listing->isContent()	)
				{
					$listing_city_name = ($publish->listing->list_city) ? $publish->listing->list_city->name : '(!!city not found!!)';
					
					echo "PAST THE EXPIRY DATE: ".$publish->listing->expiry_date->format('Y-m-d')." -- I should expire -- Retiring BLAST: " . $publish->id . " LISTING: " . $publish->listing_id ." " . $listing_city_name . "\n";	
		
					if($publish->listing->isContent())
					{
						echo "https://www.city-blast.com/listing/createmanualblast/". $publish->listing->id. "\n\n";
					}
					else
					{
						echo "https://www.city-blast.com/listing/update/". $publish->listing->id. "\n\n";	
					}
	
					if($listing = Listing::find($publish->listing_id))
					{						
						$listing->published = $listing->published + 1;
						$listing->published_on = date("Y-m-d H:i:s");
						$listing->save();
					}
					$publish->delete();
					
					echo "I just retired: ". $publish->id . " " . $listing->id . "\n\n";
				}
			}
			
			// Need to expire all non manual blasts if they are older than 15 days
			if(	
				strtotime($publish->listing->created_at->format('Y-m-d')) < $seven_days_ago && 
				!$publish->listing->isContent())
			{
				echo "OLDER THEN 7 days: ".$publish->listing->created_at->format('Y-m-d')." -- I should expire -- Retiring BLAST: " . $publish->id . " LISTING: " . $publish->listing_id ." " . $publish->listing->list_city->name . "\n";
				echo "https://www.city-blast.com/listing/update/". $publish->listing->id. "\n\n";	
				if($listing = Listing::find($publish->listing_id))
				{						
					$listing->published = $listing->published + 1;
					$listing->published_on = date("Y-m-d H:i:s");
					$listing->save();
				}
				$publish->delete();
				
				echo "I just retired: ". $publish->id . " " . $listing->id . "\n\n";
			}	
			
			if($publish->current_count >= $publish->max_inventory_threshold)
			{
				echo "CURRENT:".$publish->current_count." MAX: " . $publish->max_inventory_threshold . " Retiring BLAST: " . $publish->id . " LISTING: " . $publish->listing_id ." " . $publish->listing->list_city->name . "\n";	


				if($publish->listing->isContent())
				{
					echo "https://www.city-blast.com/listing/createmanualblast/". $publish->listing->id. "\n\n";
				}
				else
				{
					echo "https://www.city-blast.com/listing/update/". $publish->listing->id. "\n\n";	
				}
						
				
				if($listing	= Listing::find($publish->listing_id))
				{
					
					$listing->published = $listing->published + 1;
					$listing->published_on = date("Y-m-d H:i:s");
					$listing->save();
				}
	

				
				$publish->delete();
				
				echo "I just retired: ". $publish->id . " " . $listing->id . "\n\n";
				
			}
		}
		else
		{
			echo "MISSING LISTING: I should delete -- deleting BLAST: " . $publish->id . " BECAUSE THE LISTING is MISSING\n";	
			$publish->delete();
		}
	}
	
?>
