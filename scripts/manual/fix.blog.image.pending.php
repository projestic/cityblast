#!/usr/bin/env php
<?
require realpath(dirname(__FILE__) . '/../public/env.php');	
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors',1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();

Logger::log('Starting');

$conditions = array('conditions' => array());
$images = new Blastit_ActiveRecord_IteratorBuffered('BlogImagePending', $conditions);
foreach ($images as $image) {
	if (empty($image->width) || empty($image->height)) {
		Logger::log('Updating #' . $image->id . ' (' . $image->image . ')');
		Logger::startSection();
		list($width, $height) = getimagesize(CDN_ORIGIN_STORAGE . $image->image);
		if (!empty($width) && !empty($height)) {
			$image->width = $width;
			$image->height = $height;
			$image->save();
		} else {
			Logger::log('Unable to determine dimensions');
		}
		Logger::endSection();
	}
}
Logger::log('Finished');