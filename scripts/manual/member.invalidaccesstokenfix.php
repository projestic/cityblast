#!/usr/bin/env php
<?
 
	/**************
	
	/usr/local/php53/bin/php member.invalidaccesstokenfix.php
	
	***************/

	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	

	// Define tDEBUG_MODE to False if you want to update database records
 	define('DEBUG_MODE', FALSE);

	
	ini_set('display_errors', 1);
	

	$conditions = array();
	$members = Member::find('all', array('conditions' => $conditions, 'order' => 'id ASC'));


	echo "Starting to iterate ".count($members)." members\n\n";
	
	
	$total_members = count($members);
	$updated_members = 0;

	foreach($members as $member)
	{
		$token = $member->access_token;
		// IF there is a expires param
		if( FALSE !== strstr($token,'expires=') ) {
			
			$access_token_parts 	=	explode('&expires',$token);
			$member->access_token 	=	!empty($access_token_parts) ? $access_token_parts[0] : $token;

			$updated_members++;

			// DEBUG MODE no save is done
			if ( DEBUG_MODE ) {
				echo "Changing from ".$token." TO ".$member->access_token."\n";
				continue;
			}
			$member->save();
		}

	}
	echo "Updated ".$updated_members." of ".$total_members."\n\n";

	if(DEBUG_MODE)
		echo "\n***** NO UPDATE DONE, RAN IN DEBUG MODE *****\n";


?>
