#!/usr/bin/env php
<?
require realpath(dirname(__FILE__) . '/../public/env.php');	
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors',1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();

Logger::log('Starting');

$listing_id_start = !empty($argv[1]) ? $argv[1] : null;
$listing_id_end  = !empty($argv[2]) ? $argv[2] : null;
$listing_id_end = empty($listing_id_end) && !empty($listing_id_start) ? $listing_id_start : null;

$conditions = array();
if (!empty($listing_id_start) && !empty($listing_id_end)) {
	$conditions = array('id >= ? AND id <= ?', $listing_id_end, $listing_id_end);
} 

$conditions_all = array('conditions' => $conditions);
$listings = new Blastit_ActiveRecord_IteratorBuffered('Listing', $conditions_all);
foreach ($listings as $listing) {
	
	Logger::log('Processing Listing: ' . $listing->id);
	Logger::startSection();
	if ($listing->image) {
		Logger::startSection();
		Logger::log('Processing listing image: ' . $listing->image);
		$resizer = new Image();
		$logger_indent_level = Logger::getIndentLevel();
		try {
			$resizer->resizeListingImage(CDN_ORIGIN_STORAGE . $listing->image, true);
		} catch (Exception $e) {
			Logger::log(get_class($e) . ': ' . $e->getMessage());
		}
		Logger::setIndentLevel($logger_indent_level);
		Logger::endSection();
	}
	
	if ($listing->images) {
		foreach ($listing->images as $img) {
			Logger::startSection();
			Logger::log('Processing listing image: ' . $img->image);
			$resizer = new Image();
			Logger::setIndentLevel($logger_indent_level);
			try {
				$resizer->resizeListingImage(CDN_ORIGIN_STORAGE . $img->image, true);
			} catch (Exception $e) {
				Logger::log(get_class($e) . ': ' . $e->getMessage());
			}
			Logger::setIndentLevel($logger_indent_level);
			Logger::endSection();
		}
	}
	Logger::endSection();
}

Logger::log('Finished');