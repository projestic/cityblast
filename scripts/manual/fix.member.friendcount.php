#!/usr/bin/env php
<?
require realpath(dirname(__FILE__) . '/../../public/env.php');	
require realpath(dirname(__FILE__) . '/../../app/configs/environment.php');

ini_set('display_errors',1);

$members = Member::find('all', array('conditions' => 'uid IS NOT NULL', 'order' => 'id desc') );
					
echo count($members);

foreach ($members as $member) {
	
	$network_app_facebook = $member->network_app_facebook;
	
	$facebook = new Facebook(
			array(
				'appId'  => $network_app_facebook->consumer_id,
				'secret' => $network_app_facebook->consumer_secret,
				'cookie' => false
			)
		);


	try {
	
		echo "Checking member {$member->id} ... " . PHP_EOL;
	
		// don't just count the /friends connection from the Graph API here, not all users will show in that list due to privacy settings
	
		$data = $facebook->api(
			array(
				'method' => 'fql.query',
				'access_token' => $member->network_app_facebook->getFacebookAppAccessToken(),
				'query' => 'SELECT friend_count FROM user WHERE uid = ' . $member->uid
			)
		);
		
		$fb_friends = null;
		if (is_array($data) && isset($data[0])) $fb_friends	= $data[0]['friend_count'];

		$oldcount = $member->facebook_friend_count;

		if($fb_friends > 0)
		{
			$member->facebook_friend_count	=	$fb_friends;
			$member->save();
		}
		
		if ($oldcount != $member->facebook_friend_count) echo "Changed friend count for member {$member->id} from $oldcount to {$member->facebook_friend_count}" . PHP_EOL;
		
	}
	catch(Exception $e)
	{
		echo $e->getMessage() . PHP_EOL;
	}

}

