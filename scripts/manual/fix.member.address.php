#!/usr/bin/env php
<?
require realpath(dirname(__FILE__) . '/../public/env.php');	
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors',1);

$alert_emailer = Zend_Registry::get('alertEmailer');

Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

require_once(APPLICATION_PATH . '/../lib/stripe_lib/Stripe.php'); 
Stripe::setApiKey(STRIPE_SECRET);

Logger::setEcho(true);
Logger::setHtml(false);

Logger::logTimeMarker();
Logger::log('Starting member address fix');

$stats = array(
	'found' => 0,
	'fixed' => 0
);

$batch = 1;
$select_limit = 20;
$select_offset = 0;
while (true) {
	$batch_stats = array(
		'found' => 0,
		'fixed' => 0,
		'api_called' => false
	);
	
	Logger::log('Starting batch ' . $batch);
	
	Logger::startSection();
	
	Logger::log('Selecting batch of members without address');
	$members  = Member::all(array(
		"conditions" => 'country IS NULL OR state IS NULL',
		'limit' => $select_limit,
		'offset' => $select_offset,
		'order' => 'id ASC'
	));
	
	Logger::startSection();
	if (empty($members)) {
		Logger::log('No members without address found');
		Logger::endSection();
		Logger::endSection();
		break;
	} else {
		$batch_stats['found'] = count($members);
		Logger::log('Found ' . $batch_stats['found'] . ' members in this batch');
	}
	
	foreach ($members as $member) {
		if (!empty($member->country) && !empty($member->state)) {
			// Skipe this member
			continue;
		}
		
		if (
			in_array($member->payment_gateway, array('STRIPE', 'STRIPES'))
			&& !empty($member->stripe_customer_id)
		) 
		{
			Logger::log('Calling Stripe API');
			
			$customer = Stripe_Customer::retrieve($member->stripe_customer_id);
			if (isset($customer->active_card)) 
			{
				if (
					!empty($customer->active_card->address_country) &&
					!empty($customer->active_card->address_state) 
				) 
				{
					$member->address = $customer->active_card->address_line1;
					$member->address2 = $customer->active_card->address_line2;
					$member->city = $customer->active_card->address_city;
					$member->state = $customer->active_card->address_state;
					$member->country = $customer->active_card->address_country;
					$member->zip = $customer->active_card->address_zip;
					
					$member->save();
					
					$batch_stats['fixed']++;
				}
			}
			
			$batch_stats['api_called'] = true;
			
			Logger::log('Going to sleep for 2 seconds');
			sleep(2);
		}
	}
	Logger::endSection();
	
	Logger::log('Fixed ' . $batch_stats['fixed']);
	Logger::log('Skipped ' . ($batch_stats['found'] - $batch_stats['fixed']));
	Logger::endSection();
	
	$stats['found'] += $batch_stats['found'];
	$stats['fixed'] += $batch_stats['fixed'];
	
	$select_offset = $select_offset + $select_limit;
	$batch++;
	
	if ($batch_stats['api_called']) {
		Logger::log('Going to sleep for 5 seconds');
		sleep(5);
	}
}

Logger::log('Total members found ' . $stats['found']);
Logger::log('Total members fixed ' . $stats['fixed']);
Logger::log('Total members skipped ' . ($stats['found'] - $stats['fixed']));

Logger::log('Finished');
Logger::logTimeMarker();

