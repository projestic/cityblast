#!/usr/bin/env php
<?
require realpath(dirname(__FILE__) . '/../public/env.php');	
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors',1);

Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

require_once(APPLICATION_PATH . '/../lib/stripe_lib/Stripe.php'); 
Stripe::setApiKey(STRIPE_SECRET);

Logger::setEcho(true);
Logger::setHtml(false);

Logger::logTimeMarker();
Logger::log('Starting member price zero fix');

Logger::log('Selecting members with price zero');
$members  = Member::all(array(
	"conditions" => 'price <= 0 OR price IS NULL',
	'order' => 'id ASC'
));

$members_total = !empty($members) ? count($members) : 0;

Logger::log('Found ' . $members_total . ' with price of zero');


if (!empty($members)) {
	foreach ($members as $member) {
		Logger::log('Updating memeber id: ' . $member->id);
		Logger::startSection();
		
		$memberDataOld = array(
			'account_type' => $member->account_type,
			'price' => $member->price,
		);
		
		$member->account_type = 'free';
		
		$last_payment = Payment::find(array(
			'conditions' => "member_id='" . addslashes($member->id) . "' AND amount > 0 AND debit_cash_trans_id = 0", 
			'limit' => 1, 
			'order'=>'created_at desc, id desc'
		));
		
		if ($last_payment) {
			Logger::log('Updating member price from recent payment');
			$member->price = $last_payment->amount;
		} elseif ($member->referring_affiliate instanceOf Affiliate) {
			Logger::log('Updating member price from affiliate (' .  $member->referring_affiliate_id . ') price');
			$member->price = $member->referring_affiliate->getPrice();
		} else {
			Logger::log('Updating member price to default price');
			$member->price = PAYMENT_AMOUNT;
		}
		
		$member->save();
		
		$memberDataNew = $member->to_array();
		$memberDataNew = array(
			'account_type' => $member->account_type,
			'price' => $member->price,
		);
		
		Logger::log('Before:');
		Logger::log(json_encode($memberDataOld), null, true);
		Logger::log('After:');
		Logger::log(json_encode($memberDataNew), null, true);
		
		Logger::endSection();
	}
}

Logger::log('Finished');