#!/usr/bin/env php
<?
require realpath(dirname(__FILE__) . '/../../public/env.php');	
require realpath(dirname(__FILE__) . '/../../app/configs/environment.php');

ini_set('display_errors',1);


$i = 0;
$chunk = 10;

do {
	
	$members = Member::find('all', array('conditions' => '1=1', 'limit' => $chunk, 'offset' => $i));

	$now = new DateTime;

	foreach ($members as $member) {

		echo "Processing member {$member->id} . . . " . PHP_EOL;

		if ($member->token_expires > $now) {
			try {
				$member->extendFanpageTokens();
			} catch (Exception $e) {
				echo $member->id . ' failed because ' . $e->getCode() . ': ' . $e->getMessage() . PHP_EOL;
			}
		}
	}
	$i = $i+$chunk;
} while (count($members) == $chunk);
