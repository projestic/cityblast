#!/usr/bin/env php
<?
require realpath(dirname(__FILE__) . '/../public/env.php');	
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors',1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();

Logger::log('Starting');

$sql = "
	DATE(created_at) <= ? AND 
	payment_status = 'signup' AND 
	(
		stripe_customer_id IS NULL 
		OR stripe_customer_id = ''
	)
";

$conditions = array('conditions' => array($sql, date('Y-m-d', strtotime('-7days'))));
$members = new Blastit_ActiveRecord_IteratorBuffered('Member', $conditions);
foreach ($members as $member) {
	Logger::log('Processing Member: ' . $member->id);
	Logger::startSection();
	$member->payment_status = 'signup_expired';
	$member->save();
	Logger::endSection();
}

Logger::log('Finished');