#!/usr/bin/env php
<?php
require realpath(dirname(__FILE__) . '/../public/env.php');	
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors',1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();

Logger::log('Starting');


$conditions = array("
	(
		image IS NOT NULL 
		AND image != '' 
		AND image NOT REGEXP '^/images/listings/[0-9]{4}/[0-9]{2}/[0-9]+/.+'
	) 
	OR
	(
		thumbnail IS NOT NULL 
		AND thumbnail != '' 
		AND thumbnail NOT REGEXP '^/images/listings/[0-9]{4}/[0-9]{2}/[0-9]+/.+'
	)
");
$conditions_all = array('conditions' => $conditions);
$listings = new Blastit_ActiveRecord_IteratorBuffered('Listing', $conditions_all, 600);
foreach ($listings as $listing) {
	
	Logger::log('Processing Listing: ' . $listing->id);
	Logger::startSection();
	$listing->fixImageFilePaths();
	Logger::endSection();
}

Logger::log('Finished');