#!/usr/bin/env php
<?php

    // it should include production environment
    defined('APPLICATION_ENV')
        || define('APPLICATION_ENV', 'production');

    require realpath(dirname(__FILE__) . '/../public/env.php');
    require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

    $sql = 'SELECT * FROM ' . Member::$table . ' WHERE email = ""';
    $members = Member::find_by_sql($sql);

    echo "Starting\n\n";

    if ( empty($members) ) {
        echo "Members with a empty e-mail aren't found\n";
        return;
    }

    $countMembers = count($members);

    echo "Found {$countMembers} members with a empty e-mail\n";

	$network_app_facebook = Zend_Registry::get('networkAppFacebook');
	$facebook = new Facebook(
			array(
				'appId'  => $network_app_facebook->consumer_id,
				'secret' => $network_app_facebook->consumer_secret,
				'cookie' => false
			)
		);

    $fails = 0;

    foreach ( $members as $member ) {
        if ( !empty($member->access_token) ) {
            echo "FB  Member: {$member->first_name} {$member->last_name}\n";

            try {
    		    $fbResult = $facebook->api('/' . $member->uid, 'get', array('access_token' => $member->network_app_facebook->getFacebookAppAccessToken() ));

        		if ( !empty($fbResult['email']) ) {
        		    $member->email = $fbResult['email'];
        		    $member->redonly(false);
        		    $member->save();
        		} else {
        		    $fails++;
        		}
    	    } catch ( Exception $e ) {
    		    echo "ERROR: FBUID[{$member->uid}]: {$e->getMessage()}\n\n";
    		    $fails++;
    	    }
        }
    }

    echo "Hasn't been updated members {$fails} of {$countMembers}\n\nDone!\n";


