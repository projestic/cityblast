#!/usr/bin/env php
<?
include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');


Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

Logger::logTimeMarker();

$start_date = !empty($argv[1]) ? $argv[1] : null;
$end_date = !empty($argv[2]) ? $argv[2] : null;
// Date defaults to 29 days ago - we want to process the past 28 days but the cron should run after midnight
if (empty($start_date) && empty($end_date)) {
	$start_date = date('Y-m-d', strtotime('-29 days'));
	$end_date = date('Y-m-d');
} elseif (empty($end_date)) {
	// if no end date is given default to the same as start date
	$end_date = $argv[1];
}

MemberStatsSignup::populateDateRange($start_date, $end_date);

