#!/usr/bin/env php
<?
    
	require realpath(dirname(__FILE__) . '/../public/env.php');
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	

/**************

 	/usr/local/php53/bin/php /home/abubic/city-blast.com/scripts/resizeimage_cron.php 

***************/



	//echo phpinfo();die;
	ini_set("display_errors",1);
	
	
	echo "ENV:".APPLICATION_ENV."\n\n";
	//exit();
	
	
	$listings = Listing::find_by_sql(
		"SELECT * FROM listing ".
		"WHERE blast_type !='".Listing::TYPE_CONTENT."' ".
		"AND status != 'deleted' ".
		"AND id <= 171 ".
		"ORDER BY ID DESC"
	);
	
	$resizer	= new Image();
	
	
	function resize($listing_id, $image, $newsize)
	{
		$info = pathinfo($image);
		
		print_r($info);
		
		$base = "/home/abubic/city-blast.com/public";
		
		
		
		$original = $base . $image;
		echo $original . "\n";
		
		$newfile = $base . $info['dirname'] . "/" . $listing_id . "/".$info['filename'] . "_".$newsize.".".$info['extension'];
		echo $newfile . "\n";
		
		
		try {
		
			echo 'convert '. $original .' -resize '. $newsize .'^ \  -gravity center -extent '. $newsize. ' '. $newfile .' 2>&1';
			echo "\n";
			
			exec('convert '. $original .' -resize '. $newsize .'^ \  -gravity center -extent '. $newsize. ' '. $newfile .' 2>&1', $output, $status);

		} catch (Exception $e) {

			#throw new Exception('ImageMagick "convert" command failed');
			throw new Exception('ImageMagick "convert" command failed. Error "'. $e->getMessage() .'" at '. $e->getLine() .' Line of '. $e->getFile());
		}
		
		echo "Image created successfully!\n";
		
	
	}
	
	foreach($listings as $listing ){
		
		echo "Updating:".$listing->id."\n";




		if(isset($listing->image) && !empty($listing->image))
		{
			resize($listing->id, $listing->image, '605x239');
			resize($listing->id, $listing->image, '101x99');
			resize($listing->id, $listing->image, '280x250');
			resize($listing->id, $listing->image, '76x76');
		}

		if(isset($listing->thumbnail) && !empty($listing->thumbnail))
		{
			resize($listing->id, $listing->thumbnail, '605x239');
			resize($listing->id, $listing->thumbnail, '101x99');
			resize($listing->id, $listing->thumbnail, '280x250');
			resize($listing->id, $listing->thumbnail, '76x76');
		}
		
	}
?>
