#!/usr/bin/env php
<?php
/**************
/usr/local/php53/bin/php trial.activate.notify.php
***************/

include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors', 1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

Logger::logTimeMarker();

Logger::log('Starting');

$date_trial_end = date('Y-m-d', strtotime('-14days'));


Logger::log('14 Days ago was: ' . $date_trial_end);

//Make sure the connection doesn't time out
$query = "set @@wait_timeout=1200";
Member::connection()->query($query);

$query = "
	SELECT * 
	FROM member 
	WHERE 
		DATE(created_at) <= '" . $date_trial_end . "' AND 
		payment_status = 'signup' AND 
		activate_email = 0
";

$members = Member::find_by_sql($query);

foreach($members as $member)
{
	Logger::log('Processing member: ' . $member->id);
	Logger::startSection();
	$m = Member::find($member->id);	
	$m->activateNotify();
	Logger::endSection();
}

Logger::log('Finished');


