#!/usr/bin/env php
<?
include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors', 1);

$alert_emailer = Zend_Registry::get('alertEmailer');

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();


$check_time = time();
$check_date = date('Y-m-d');

$error_rate_threshold = .1;

Logger::startSection();

// Check if we've posted in the last hour

Logger::log('Checking publisher is publishing');

$posts_last_hour = Post::count(array(
	'conditions' => array("created_at >= DATE_SUB(NOW(), INTERVAL 1 HOUR) AND result IS NOT NULL")
));

Logger::log("There have been $posts_last_hour publish cycles in the last hour.");

if ($posts_last_hour > 50) {
	
	$attempts = Post::count(array(
		'conditions' => array("created_at >= DATE(NOW()) AND result IS NOT NULL")
	));

	$successes = Post::count(array(
		'conditions' => array("created_at >= DATE(NOW()) AND result = 1")
	));

	$error_rate = null;
	if ($attempts > 50) $error_rate = round(($attempts - $successes)/$attempts, 2);

	$facebook = getStatsFor('FACEBOOK');
	$fanpage  = getStatsFor('FANPAGE');
	$twitter  = getStatsFor('TWITTER');
	$linkedin = getStatsFor('LINKEDIN');
	
	Logger::log("Publish successes: " . $successes . '/' . $attempts );
	Logger::log("Publish successes for Facebook: " . $facebook['successes'] . '/' . $facebook['attempts']);
	Logger::log("Publish successes for fan pages: " . $fanpage['successes'] . '/' . $fanpage['attempts']);	
	Logger::log("Publish successes for Twitter: " . $twitter['successes'] . '/' . $twitter['attempts']);
	Logger::log("Publish successes for LinkedIn: " . $linkedin['successes'] . '/' . $linkedin['attempts']);
	
	if ($facebook['error_rate'] >= $error_rate_threshold || 
	    $fanpage['error_rate']  >= $error_rate_threshold || 
	    $twitter['error_rate']  >= $error_rate_threshold || 
	    $linkedin['error_rate'] >= $error_rate_threshold) {
	    
		Logger::log('Potential publishing issue. Sending email alert.');
		$message = "One or more networks has exceeded the error rate threshold of " . $error_rate_threshold * 100 . "%:\n\n" .
		"Post success counts so far today (" . $check_date . "):" . "\n" .
		"Facebook: {$facebook['successes']}/{$facebook['attempts']} (Error rate: " . ($facebook['error_rate'] ? ($facebook['error_rate'] * 100) . "%" : 'N/A') . ")\n" . 
		"Fanpages: {$fanpage['successes']}/{$fanpage['attempts']} (Error rate: " . ($fanpage['error_rate'] ? ($fanpage['error_rate'] * 100) . "%" : 'N/A') . ")\n" . 
		"Twitter:  {$twitter['successes']}/{$twitter['attempts']} (Error rate: " . ($twitter['error_rate'] ? ($twitter['error_rate'] * 100) . "%" : 'N/A') . ")\n" .
		"Linkedin: {$linkedin['successes']}/{$linkedin['attempts']} (Error rate: " . ($linkedin['error_rate'] ? ($linkedin['error_rate'] * 100) . "%" : 'N/A') . ")\n" .
		"Total:    $successes/$attempts (Error rate: " . $error_rate * 100 . "%)\n";
		
		$message .= "\n\n\nErrors for Facebook:\n\n";

		$message .= implode("\n", getErrorListFor('FACEBOOK'));
		
		$message .= "\n\n\nErrors for fan pages:\n\n";
		
		$message .= implode("\n", getErrorListFor('FANPAGE'));

		$message .= "\n\n\nErrors for Twitter:\n\n";

		$message .= implode("\n", getErrorListFor('TWITTER'));

		$message .= "\n\n\nErrors for LinkedIn:\n\n";

		$message .= implode("\n", getErrorListFor('LINKEDIN'));
		
		Logger::log($message);
		
		$alert_emailer->send('URGENT WARNING!!! - Possible Publisher Error!', $message, 'publish-monitor');
		
	}
}

Logger::endSection();

function getErrorListFor($type) {
	$error_messages = Post::all(array(
		'select'     => 'message',
		'conditions' => array("created_at >= DATE(NOW()) AND result = 0 AND type = ?", $type),
		'group'      => 'message'
	));
	$errors = array();
	foreach ($error_messages as $error) {
		$errors[] = $error->message;
	}
	if (!count($errors)) return array('(none)');
	return $errors;
}

function getStatsFor($type) {
	$successes = Post::count(array(
		'conditions' => array("created_at >= DATE(NOW()) AND type = ? AND result = 1", $type)
	));
	$attempts = Post::count(array(
		'conditions' => array("created_at >= DATE(NOW()) AND type = ? AND result IS NOT NULL", $type)
	));
	$error_rate = null;
	if ($attempts > 20) $error_rate = round(($attempts - $successes)/$attempts, 2);
	return array('successes' => $successes, 'attempts' => $attempts, 'error_rate' => $error_rate);
}
