#!/usr/bin/env php
<?

	require realpath(dirname(__FILE__) . '/../public/env.php');
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

	ini_set('display_errors', 1);
	echo date("Y-m-d H:i:s") . "\t Starting\n";
	
	//Make sure the connection doesn't time out
	Listing::connection()->query( "set @@wait_timeout=1200" );
	
	$offset = 0;
	$limit = 100;	

	do {
		
		$emails = EmailLog::find('all', array('conditions' => array("type = 'REFERRAL' AND created_at > DATE_SUB(NOW(), INTERVAL 28 HOUR)"), 'limit' => $limit, 'offset' => $offset));
	
		foreach ($emails as $email) {
			subscribe($email);
		}
		
		$offset += $limit;
		
	} while (count($emails) == $limit);
	
	echo "Processed all referrals in last 28 hours\n";	


function subscribe($obj) {
	try {
		if ($obj->addToConstantContact()) {
			echo "Synced " . $obj->email . PHP_EOL . PHP_EOL;
		}
	} catch (Exception $e) {
		if (stristr($e->getMessage(), 'mashery.not.authorized.over.qps')) {
			echo "Mashery throttled, retrying in 1s. ". PHP_EOL;
			sleep(1);
			subscribe($obj);
		} else {
			echo "Error " . $e->getCode() . ". Constant Contact said: " . $e->getMessage() . PHP_EOL;
		}
	}

}
