#!/usr/bin/env php
<?
 	
	/**************
	
	/usr/local/php53/bin/php reorder.hoppers.php
	
	***************/
	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	

	
	ini_set('display_errors', 1);


	
	//Make sure the connection doesn't time out
	$query = "set @@wait_timeout=1200";
	Listing::connection()->query($query);
	echo "\n\n\n";
	

	$cities = City::all();
	
	foreach($cities as $city)
	{
		echo "Ordering: " .$city->name."\n";
		
		$conditions = "( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) AND status='active' and city_id=".$city->id;	
		$members = Member::find('all', array('order' => 'id ASC', 'conditions' => $conditions));

		
		$hoppers = array();
		$counter = 1;
		foreach($members as $member)
		{			
			echo $member->id . " " . $member->last_name . " " . $member->current_hoper . "\n";	
			//$hoppers[$member->current_hoper]++;
			
			echo "NEW: " .$counter."\n";
			
			$member->current_hoper = $counter;
			$member->save();
			
			$counter++;
			
			if($counter > 3) $counter = 1;
		}
	}
	

	
	
?>