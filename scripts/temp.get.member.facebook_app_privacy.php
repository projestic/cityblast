#!/usr/bin/env php
<?php

include realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors', 1);

$offset = 0;
$limit  = 100;

do {

	$members = Member::all(array('conditions' => 'token_expires > NOW()', 'limit' => $limit, 'offset' => $offset));
	
	echo count($members);
	foreach ($members as $member) {
		try {
			$member->updateFacebookAppVisibility();
			$member->save();
			echo $member->id . " app privacy updated to " . $member->facebook_app_visibility . PHP_EOL;
		} catch (Exception $e) {
			echo $member->id . " failed. Error " . $e->getCode() . ": " . $e->getMessage();
		}
	}
	
	$offset += $limit∑

} while (count($members) == $limit);