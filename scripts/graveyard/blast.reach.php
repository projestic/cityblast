#!/usr/bin/env php
<?
    
	require realpath(dirname(__FILE__) . '/../public/env.php');
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

	// Update friend list only 1 day after last update
	define("FRIEND_LIST_VALID_UNTIL", date("Y-m-d H:i:s",strtotime("-1 day")));


	$query = "set @@wait_timeout=1200";
	Member::connection()->query($query);

	ini_set('memory_limit', '500M');
	
	$network_app_facebook = Zend_Registry::get('networkAppFacebook');
	
	$facebook = $network_app_facebook->getFacebookAPI();

	// Linkedin Client
	$linkedin = new LinkedIn();

	$blastQueue =	BlastReachQueue::find('all',array('conditions'=>'status=\'pending\''));
	
	$memberList	=   array();
	$memberFriends	=   array();

	// Updating friends list
	foreach($blastQueue as $blast)
	{
	    $post   =	Post::find($blast->post_id);

	    if (empty($memberList[$blast->member_id]))
		    $memberList[$blast->member_id]  = Member::find($blast->member_id);

	    $member	=   $memberList[$blast->member_id];
	    $lastFriend	=   Friends::find('one',array('conditions'=>'channel="'.$blast->channel.'" AND created_at >= "'.FRIEND_LIST_VALID_UNTIL.'"'));

	    $updateList	=   (count($lastFriend)==0);
	    
	    if( $updateList) updateFriends($blast->channel,$member, ${$blast->channel});

	    if( empty($memberFriends[$blast->channel][$blast->member_id]) )
		    $memberFriends[$blast->channel][$blast->member_id]	=   Friends::find('all',array('conditions'=>'channel="'.$blast->channel.'" AND member_id='.$blast->member_id));

	    foreach ($memberFriends[$blast->channel][$blast->member_id] as $friend) {

			$blastReach =	new BlastReach();
			$blastReach->member_id	=   $blast->member_id;
			$blastReach->post_id	=   $blast->post_id;
			$blastReach->listing_id	=   $post->listing_id;
			$blastReach->friend_id	=   $friend->id;
			$blastReach->channel	=   $blast->channel;
			$blastReach->save();

	    }
	    
	    $blast->status  =	'completed';
	    $blast->save();
	    
	}

	
	function updateFriends($channel, $member, $handler)
	{
	    switch($channel){
		case 'facebook':
		     try {
				$publish_array			=   array();
				$publish_array['access_token']	=   $member->network_app_facebook->getFacebookAppAccessToken();
				$publish_array['fields']    =	'id,first_name,last_name';
				
				$friends = $handler->api("/".$member->uid."/friends","get",$publish_array);

				foreach($friends['data'] as $friend)
				{
					$friendO	=   Friends::find_by_channel_and_social_profile_id('facebook',$friend['id']);
					if( is_null($friendO) )
					{
						$friendO    =	new Friends();
						$friendO->member_id =	$member->id;
						$friendO->name	    =	utf8_decode($friend['first_name'].' '.$friend['last_name']);
						$friendO->social_profile_id =	$friend['id'];
						$friendO->channel   =	'facebook';
						$friendO->save();
					} 

				}
		    } catch(Exception $e) {
				echo "Error: " . $e->getCode() . ": " . $e->getMessage();
		    }

		    break;
		    
		case 'twitter':

		    try{

				$twitter = new Blastit_Service_Twitter(null, $member);

				$friendApi			    =	$handler->getFollowersIdsByUserIdAndScreenName($member->twitter_id, $member->twitter_screen_name);

				$friends    =	array('data'=>array());
				$friendsIds =	array();

				foreach($friendApi as $friend)
				{
					$friends['data'][]	=   array('id'=>$friend);
					$friendO	=   Friends::find_by_channel_and_social_profile_id('twitter',$friend);
					if( is_null($friendO) )
					{
					$friendO	    =	new Friends();
					$friendO->member_id =	$member->id;
					$friendO->name	    =	'';
					$friendO->social_profile_id =	$friend;
					$friendO->channel   =	'twitter';
					$friendO->save();
					$friendsIds[]	=   $friend;

					}
				}

				// Need to get twitter extended info from friends, to that we can save names
				$friendsIds = array_unique($friendsIds);
				$manyOffsets	=   ceil(count($friendsIds)/100);
			
				for($i=0;$i<$manyOffsets;$i++)
				{
					$offset	=	$i*100; // 100 elements per request, for twitter api
					$list_of_ids    = array_slice($friendsIds, $offset, 100);
					$userLookup	=	$handler->getUserLoookup(implode(",",$list_of_ids));

					foreach($userLookup as $tuser)
					{
					$friendO	=   Friends::find_all_by_channel_and_social_profile_id('twitter',$tuser->id);

					foreach($friendO as $f)
					{
						$f->name	    =	utf8_decode($tuser->name);
						$f->save();
					}
					}
				}

			} catch(Exception $e) {
				echo "Error: " . $e->getCode() . ": " . $e->getMessage();
			}

		    break;
		    
		case 'linkedin':
		     try{

				$token	    =	unserialize($member->linkedin_access_token);
				$connections=   $handler->getConnections($token->oauth_token, $token->oauth_token_secret, $listing->message, $link='', $image='');

				$friends    =	array('data'=>array());
				foreach($connections as $friend)
				{
					$friends['data'][]	=   array('id'=>$friend['id']);
					$friendO	=   Friends::find_by_channel_and_social_profile_id('linkedin',$friend['id']);
					if( is_null($friendO) )
					{
					$friendO	    =	new Friends();
					$friendO->member_id =	$member->id;
					$friendO->name	    =	utf8_decode($friend['firstname'].' '.$friend['lastname']);
					$friendO->social_profile_id =	$friend['id'];
					$friendO->channel   =	'linkedin';
					$friendO->save();
					}
				}

			} catch(Exception $e) {
				echo "Error: " . $e->getCode() . ": " . $e->getMessage();
			}
		    break;
	    }

	    return $friends;

	}
echo 'DONE\n\n\n\n';
?>