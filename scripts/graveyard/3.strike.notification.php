#!/usr/bin/env php
<?php

/****

/usr/local/php53/bin/php 3.strike.notification.php

****/

include realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

$query = "set @@wait_timeout=1200";
Member::connection()->query($query);

$conditions = " ((facebook_fails >= 3 and facebook_publish_flag = 1) or 
				(linkedin_fails >= 3 and linkedin_access_token IS NOT NULL and linkedin_publish_flag = 1) or 
				(twitter_fails >= 3 and twitter_access_token IS NOT NULL and twitter_publish_flag = 1) ) AND status='active' ";
$members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

echo "START\n";

if($members)
{
	foreach($members as $member)
	{
		//fetch last  post of this member
		$conditions = "member_id = ".$member->id;
		$posts = Post::find('all',array('order' => 'id DESC', 'conditions' => $conditions, 'limit' => 15));
		
		//print_r($posts);

		$errorNetworks = array();

		foreach($posts as $post)
		{

			//Facebook Errors
			if(stristr($post->message, "ERROR:Error validating access token: The session has been invalidated because the user has changed the password."))
			{
				//Check to see if this MEMBER has the publishing turned off for this type...				
				if($member->facebook_publish_flag && $member->facebook_fails >= 3) $errorNetworks['facebook'] = 'facebook';
			}
			if(stristr($post->message, "has not authorized application 199255106788609."))
			{
				if($member->facebook_publish_flag && $member->facebook_fails >= 3) $errorNetworks['facebook'] = 'facebook';	
			}
			
			//Twitter Error			
			if(stristr($post->message, "Twitter post FAIL. ERROR:Could not authenticate with OAuth."))
			{
				if($member->twitter_publish_flag && $member->twitter_fails >= 3) $errorNetworks['twitter'] = 'twitter';
			}
			
			//Linked In Error
			if(stristr($post->message, "Linkedin post FAIL."))
			{
				if($member->linkedin_publish_flag && $member->linkedin_fails >= 3) $errorNetworks['linkedin'] = 'linkedin';
			}							
		}

		$today = new DateTime;
		$today->modify('-7 day');
		
		//Now check when the last time it was that we sent a notification email to this user... if it's more than 7 days, then send again
		$conditions = " type = 'AUTH_REMINDER' and member_id=".$member->id." and created_at <= '" . $today->format('Y-m-d H:i:00') . "' ";	
		$result = EmailLog::find('all', array('order' => 'id DESC', 'conditions' => $conditions));


		if(!$result)
		{
			//send email
			echo "Sending email to Member =".$member->id . "\n";
			$member->invalidTokenNotify($errorNetworks);
		}
	}
}
else
{
	echo "NO 3 STRIKE MEMBERS\n";
}