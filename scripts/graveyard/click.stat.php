#!/usr/bin/env php
<?
 /**************
 /usr/local/php53/bin/php click.stat.php 
 ***************/



//DEPRICATED...

/*********************************************

 	require realpath(dirname(__FILE__) . '/../public/env.php');
 	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');



 	ini_set('display_errors', 1);
 	//echo date("Y-m-d H:i:s") . "\t Starting\n";
 
 	$reg_pattern = '/\/listing\/view\//'; //used to find out listing_id
 	$filter_pattern = '^/listing/view/'; //Google Analytics request filter
	$filter = 'ga:pagePath%3D~'; //pagePath should match regular expression
	$filter = $filter . urlencode($filter_pattern);
		
	$dimensions = array('ga:pagePath', 'ga:date', 'ga:hour');
	$dimensions = implode(',', $dimensions);
	$metrics = array('ga:pageviews');
	$metrics = implode(',', $metrics);
		
	$start_date = date('Y-m-d', strtotime('-1 day')); //CHANGE THIS
	$end_date = date('Y-m-d');
?>

<html><head></head>
<body>
<?php
	function full_url() {
		$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
		$protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) . $s;
		$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
		return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
	}

	function get_session_token($onetimetoken) {
		$output = make_api_call("https://www.google.com/accounts/AuthSubSessionToken", $onetimetoken);
				
		if (preg_match("/Token=(.*)/", $output, $matches))
		{
			$sessiontoken = $matches[1];
		} else {
			echo "Error authenticating with Google.";
			exit;
		}		
		return $sessiontoken;
	}

	function make_api_call($url, $token)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$curlheader[0] = sprintf("Authorization: AuthSub token=\"%s\"/n", $token);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $curlheader);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}
	
	function parse_account_list($xml)
	{
		$doc = new DOMDocument();
		$doc->loadXML($xml);
		$entries = $doc->getElementsByTagName('entry');
		$i = 0;
		$profiles = array();
		foreach($entries as $entry)
		{
			$properties = $entry->getElementsByTagName('property');
			foreach($properties as $property)
			{
				if (strcmp($property->getAttribute('name'), 'ga:profileId') == 0 && strcmp($property->getAttribute('value'), PROFILE_ID))
					return $entry->getElementsByTagName('tableId')->item(0)->nodeValue;
			}
		}
		return null;
	}

	if ($_REQUEST['token'])
	{
		// Exchange token for session token
		$sessiontoken = get_session_token($_REQUEST['token']);
	
		// Get list of accounts
		$accountxml = make_api_call("https://www.google.com/analytics/feeds/accounts/default", $sessiontoken);
		
		$tableId = parse_account_list($accountxml);
		$requrl = "https://www.google.com/analytics/feeds/data?ids=" . $tableId . "&dimensions=" . $dimensions . "&metrics=" . $metrics . "&start-date=" .$start_date . "&end-date=" . $end_date . "&filters=" . $filter;
		$pagecountxml = make_api_call($requrl, $sessiontoken);

		
			foreach(reportObjectMapper($pagecountxml) as $result) {
				$date = $result->getDate();
				$hour = $result->getHour();
				$path = $result->getPagePath();
				$count = $result->getPageviews();
				
				
				$url = parse_url($path);
				$listing_id = intval(preg_replace($reg_pattern, '', $url['path']));
				if(isset($url['query'])) {
					parse_str($url['query'], $member_id);
					$member_id = isset($member_id['mid']) ? intval($member_id['mid']) : 0;
				} else {
					$member_id = 0;
				}
				
				$click = new Click();
				$click->date = date('Y-m-d H:i:s', mktime($hour, 0, 0, substr($date, 4, 2), substr($date, 6, 2), substr($date, 0, 4)));
				$click->url = $path;
				$click->listing_id = $listing_id;
				$click->member_id = $member_id;
				$click->count = $count;
				$click->save();
			}


	}
	else // haven't authenticated yet, show authentication link
	{
?>


<a href="https://www.google.com/accounts/AuthSubRequest?next=<?php echo full_url(); ?>&scope=https://www.google.com/analytics/feeds/&secure=0&session=1">Click here to authenticate through Google.</a>
<br />
<?php 	
	}
?>

</body>
</html>


<?php

 /**
 * Report Object Mapper to convert the XML to array of useful PHP objects
 *
 * @param String $xml_string
 * @return Array of gapiReportEntry objects
 *
 function reportObjectMapper($xml_string)
 {
 $xml = simplexml_load_string($xml_string);
 
 $results = array();
 
 $report_root_parameters = array();
 $report_aggregate_metrics = array();
 
 //Load root parameters
 
 $report_root_parameters['updated'] = strval($xml->updated);
 $report_root_parameters['generator'] = strval($xml->generator);
 $report_root_parameters['generatorVersion'] = strval($xml->generator->attributes());
 
 $open_search_results = $xml->children('http://a9.com/-/spec/opensearchrss/1.0/');
 
 foreach($open_search_results as $key => $open_search_result)
 {
 $report_root_parameters[$key] = intval($open_search_result);
 }
 
 $google_results = $xml->children('http://schemas.google.com/analytics/2009');

 foreach($google_results->dataSource->property as $property_attributes)
 {
 $report_root_parameters[str_replace('ga:','',$property_attributes->attributes()->name)] = strval($property_attributes->attributes()->value);
 }
 
 $report_root_parameters['startDate'] = strval($google_results->startDate);
 $report_root_parameters['endDate'] = strval($google_results->endDate);
 
 //Load result aggregate metrics
 
 foreach($google_results->aggregates->metric as $aggregate_metric)
 {
 $metric_value = strval($aggregate_metric->attributes()->value);
 
 //Check for float, or value with scientific notation
 if(preg_match('/^(\d+\.\d+)|(\d+E\d+)|(\d+.\d+E\d+)$/',$metric_value))
 {
 $report_aggregate_metrics[str_replace('ga:','',$aggregate_metric->attributes()->name)] = floatval($metric_value);
 }
 else
 {
 $report_aggregate_metrics[str_replace('ga:','',$aggregate_metric->attributes()->name)] = intval($metric_value);
 }
 }
 
 //Load result entries
 
 foreach($xml->entry as $entry)
 {
 $metrics = array();
 foreach($entry->children('http://schemas.google.com/analytics/2009')->metric as $metric)
 {
 $metric_value = strval($metric->attributes()->value);
 
 //Check for float, or value with scientific notation
 if(preg_match('/^(\d+\.\d+)|(\d+E\d+)|(\d+.\d+E\d+)$/',$metric_value))
 {
 $metrics[str_replace('ga:','',$metric->attributes()->name)] = floatval($metric_value);
 }
 else
 {
 $metrics[str_replace('ga:','',$metric->attributes()->name)] = intval($metric_value);
 }
 }
 
 $dimensions = array();
 foreach($entry->children('http://schemas.google.com/analytics/2009')->dimension as $dimension)
 {
 $dimensions[str_replace('ga:','',$dimension->attributes()->name)] = strval($dimension->attributes()->value);
 }
 
 $results[] = new gapiReportEntry($metrics,$dimensions);
 }
 
 return $results;
 }

***************************/

?>