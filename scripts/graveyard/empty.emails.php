#!/usr/bin/env php
<?
    /**************
      /usr/local/php53/bin/php empty.emails.php 
     ***************/
	
	
	require realpath(dirname(__FILE__) . '/../public/env.php');
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	
	
	ini_set('display_errors', 1);
	echo date("Y-m-d H:i:s") . "\t Starting\n";
	
	//Set up the FB Interface Class
	$network_app_facebook = Zend_Registry::get('networkAppFacebook');
	$facebook = $network_app_facebook->getFacebookAPI();
	
	// Grab only those members who've their email empty
	$conditions = "email=''";
	$members = Member::find('all', array('conditions' => $conditions));
	
	$counter=0;
	foreach($members as $member)
	{
		if(isset($member->access_token) && !empty($member->access_token) && !empty($member->uid))
		{
	
			if(empty($member->email))
			{
				//Set up the FACEBOOK VARIABLES...
				$publish_array['access_token'] = $member->network_app_facebook->getFacebookAppAccessToken();

				echo "\t".date("Y-m-d H:i:s")."\t FB  Member: ".$member->first_name . " " . $member->last_name . " is missing an email address\n";
		
				try {
		
					$fb_result = $facebook->api("/me/", "get", $publish_array);
		
					if( !empty($fb_result['email'])) 
					{
						echo "Found this email:".$fb_result['email']."\n";
						
						$member->email	=   $fb_result['email'];
						$member->save();
						
						echo "Saved\n";
						$counter++;
					}
				} 
				catch (Exception $e)
				{
					echo "ERROR: FBUID[".$member->uid."]: ".$e->getMessage()."\n\n";
				}
			}
		}
	}
	
	echo "Done\n\n\n";	
	echo "Recovered: " . $counter . " email addresses\n";
