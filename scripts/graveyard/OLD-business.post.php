#!/usr/bin/env php
<?
 	
	/**************
	
	/usr/local/php53/bin/php business.post.php CITY_ID INVENTORY_ID HOPPER_ID
	
	***************/
	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	
	
	include_once "publish.post.inc.php";
	
	ini_set('display_errors', 1);

	$FREQUENCY_ID = 3;
	
	
	function find_member_blast($member, $publishing_queue)
	{
		foreach($publishing_queue as $key => $publish)
		{
			
			//Let's start out with a sanity check... have we published this posting before?
			$post = Post::find_by_member_id_and_listing_id($member->id, $publish->listing_id);
		
			if(!$post)
			{ 
				//Let's grab the record one more time because it might have been updated by other running cron jobs
				$new_publish = PublishingQueue::find_by_id($publish->id);
				$publish 				= $new_publish;
				$publising_queue[$key] 	= $new_publish;
				
				//Make sure it needs more "inventory"								
				if($publish->current_count < $publish->max_inventory_threshold)
				{
					return($publish);	
				}
			}
		}			
	}
	
	
	//Make sure the connection doesn't time out
	$query = "set @@wait_timeout=10000";
	Listing::connection()->query($query);
	echo "\n\n\n";


	$city_id 		= (int)$argv[1];
	$inventory_id 	= (int)$argv[2];
	$hopper_id 	= (int)$argv[3];
	
	
	//Get all the members of this PUBLISHING FREQUENCY
	$members = Member::find_all_by_city_id_and_frequency_id_and_inventory_id_and_current_hoper($city_id, $FREQUENCY_ID, $inventory_id, $hopper_id);
	
	
	//Get the PUBLISHING QUEUE
	$publishing_queue = PublishingQueue::find_all_by_city_id_and_inventory_id_and_hopper_id($city_id, $inventory_id, $hopper_id, array('order' => 'priority ASC'));
	


	$total_members = count($members);

	
	//Start at 8am... and go until 4pm
	$work_day_minutes = 8 * 60 * 60;
	if($total_members > 10)
	{		
		$sleep = round($work_day_minutes / $total_members);	
	}
	elseif($total_members > 2 and $total_members < 10)
	{
		$sleep = 600;
	}
	else
	{
		$sleep = 1;
	}

	$sleep_interval = $sleep;
	
	if($sleep_interval > 10000)
	{
		$sleep_interval = 9999;
	}	

	echo "Sleep Interval:".$sleep_interval." seconds\n\n";	



	
	
	//Right here we're going to want an algorithm to randomize the order of when the members are published...
	if(is_array($members)) shuffle($members);	
	
	foreach($members as $member)
	{
		//Let's find out the next listing this particular member should publish...	
		$member_blast = find_member_blast($member, $publishing_queue);
		
		echo $member->id . " " . " should blast listing: " . $member_blast->listing_id . "\n";
		
		if($member->current_hoper_status == "pending")
		{
			publishMember($member, $inventory_id, $hopper_id, $member_blast->listing, $member_blast);


			//Ok, sleepy time
			sleep($sleep_interval);
		}
		
	}


		


	//Ok, now that we're done... rotate the members
	$city = City::find_by_id($city_id);
	rotateMembers($members, $city);	
	
	
	
	//Check the BLAST QUEUE for items that should be "retired" because they're over the count...
	$publishing_queue = PublishingQueue::find_all_by_city_id_and_inventory_id_and_hopper_id($city_id, $inventory_id, $hopper_id, array('order' => 'priority ASC'));
	
	foreach($publishing_queue as $publish)
	{
		
		if($publish->current_count >= $publish->max_inventory_threshold)
		{
			echo "Retiring BLAST: " . $publish->id . " LISTING: " . $publish->listing_id ."\n\n";	


			$listing	= Listing::find($publish->listing_id);
			$listing->published = $listing->published + 1;
			$listing->published_on = date("Y-m-d H:i:s");
			$listing->save();
			
			$publish->delete();
		}
	}
?>

