#!/usr/bin/env php
<?
	/**************
	/usr/local/php53/bin/php mailchimp.sync.php 
	***************/

	require realpath(dirname(__FILE__) . '/../public/env.php');
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

	ini_set('display_errors', 1);
	echo date("Y-m-d H:i:s") . "\t Starting\n";
	
	//Make sure the connection doesn't time out
	Listing::connection()->query( "set @@wait_timeout=1200" );
	
	$affiliates = Affiliate::find( 'all', array("conditions" => "first_payout > 0 || recurring_payout > 0"));
	
	$total = 0;
	
	foreach($affiliates as $a)
	{

		echo PHP_EOL . PHP_EOL . PHP_EOL . "Members referred by affiliate {$a->member_id} (" . $a->owner->name()  . "):";
		echo PHP_EOL . "- - - - - - - - - - - - - - - -";
		
		foreach ($a->referred_members as $referred) {
			if ($referred->account_type == 'free') continue;
			$first = true;
			echo PHP_EOL . "Member {$referred->id}:";
			$dates = array();
			$processed = false;
			foreach ($referred->payments as $payment) {
				if ( ($payment->amount > 0) === false ) continue;
				$processed = true;
				echo PHP_EOL . "Payment from " . $payment->created_at->format('Y-m-d') . " . . . ";
				if (in_array($payment->created_at->format('Y-m-d'), $dates)) {
					echo "is a duplicate transaction!";
					continue;
				}
				$dates[] = $payment->created_at->format('Y-m-d');
				$payable = Payable::find(array("conditions" => "member_id={$referred->id} AND payment_date = '" . $payment->created_at->format('Y-m-d') . "'"));
				if (!$payable) {
					if ($first) {
						echo  "needs a payout, recording first payout of \${$a->first_payout}";
						$total += $a->first_payout;
						$referred->doReferringAffiliatePayout($a->first_payout, $payment->created_at->format('Y-m-d'));
					} else {
						echo "needs a payout, recording recurring payout of \${$a->recurring_payout}";
						$total += $a->recurring_payout;
						$referred->doReferringAffiliatePayout($a->recurring_payout, $payment->created_at->format('Y-m-d'));
					}
				} else {
					if ($first && ($payable->amount != $a->first_payout)) {
						echo "has a payout of \${$payable->amount} but should be \${$a->first_payout}, updating";
						$total += ($payable->amount - $a->first_payout);
						$payable->amount = $a->first_payout;
						$payable->save();
					} elseif (!$first && ($payable->amount != $a->recurring_payout)) {
						$total += ($payable->amount - $a->recurring_payout);
						echo "has a payout of \${$payable->amount} but should be \${$a->recurring_payout}, updating";
						$payable->amount = $a->recurring_payout;
						$payable->save();
					} else {
						echo "has a payout.";
					}
				}
				$first = false;
			}
			if (!$processed) echo " no nonzero payments on file.";
		}

	}

	echo PHP_EOL . "Recorded \${$total} in payouts.\n";

