#!/usr/bin/env php
<?
 /**************
 /usr/local/php53/bin/php click.stat.cl.php 
 ***************/



//DEPRICATED...

/*********************************************

 	require realpath(dirname(__FILE__) . '/../public/env.php');
 	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

	$start_date = date('Y-m-d', strtotime('-100 days')); //CHANGE THIS
	$end_date   = date('Y-m-d'); 

	$reg_pattern    = '/\/listing\/view\//'; //used to find out listing_id
	$filter_pattern = '^/listing/view/'; //Google Analytics request filter
	$filter         = 'ga:pagePath%3D~'; //pagePath should match regular expression (e.g. $filter_pattern)
	$filter         = $filter . urlencode($filter_pattern);

	$dimensions = array('ga:pagePath', 'ga:date');
	$dimensions = implode(',', $dimensions);

	$metrics = array('ga:pageviews');
	$metrics = implode(',', $metrics);

try {
	$oAnalytics = new analytics(ANALYTICS_USERNAME, ANALYTICS_PASSWORD);
	$oAnalytics->setProfileById(ANALYTICS_PROFILE_ID);
	$oAnalytics->setDateRange($start_date, $end_date);

	$click_stats = $oAnalytics->getData(array('dimensions'  => $dimensions,
											 'metrics'	    => $metrics,
											 'filters'	    => $filter,
											 'max-results'  => 100));
	foreach($click_stats as $click_stat) {
		$date  = $click_stat['ga:date'];
		//$hour  = $click_stat['ga:hour'];
		$pagepath  = $click_stat['ga:pagePath'];
		$count = $click_stat['count'];
	
		$url        = parse_url($pagepath);
		$listing_id = intval(preg_replace($reg_pattern, '', $url['path']));

		if(isset($url['query'])) {
			parse_str($url['query'], $query);
			$source    = isset($query['src']) ? intval($query['src']) : '';
			$member_id = isset($query['mid']) ? intval($query['mid']) : 0;
		} else {
			$member_id = 0;
			$source    = '';
		}

		$path	=   !empty($url['path']) ? $url['path'] : $pagepath;

		$click = new Click();
		//$click->date       = date('Y-m-d H:i:s', mktime($hour, 0, 0, substr($date, 4, 2), substr($date, 6, 2), substr($date, 0, 4)));
		$click->date       = date('Y-m-d', strtotime($date));
		$click->url        = $path;
		$click->listing_id = $listing_id;
		$click->member_id  = $member_id;
		$click->count      = $count;
		$click->source     = $source;
		$click->save();
	}

} catch (Exception $e) { 
	  echo 'Caught exception: ' . $e->getMessage(); 
}

***************************/

?>