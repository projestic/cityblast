#!/usr/bin/env php
<?
 	
//DEPRECATED 	
 	
/*********************************************************************************
	
	/usr/local/php53/bin/php publish.post.php CITY_ID HOPPER_ID
	

	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	
	
	include_once "publish.post.inc.php";
	
	ini_set('display_errors', 1);


	
	//Make sure the connection doesn't time out
	$query = "set @@wait_timeout=1200";
	Listing::connection()->query($query);
	echo "\n\n\n";

	
	$city_id 		= (int)$argv[1];
	$hopper_id 	= (int)$argv[2];


echo "CITY:".$city_id."\t\tHOPPER:".$hopper_id."--------------------------------------------------------------------------------\n\n\n";



	
	//Dummy Check...
	if(isset($city_id) && !empty($city_id) && is_numeric($city_id) && isset($hopper_id) && !empty($hopper_id) && is_numeric($hopper_id))
	{
		$processedByMembers = 0;
		
		
		$conditions = "current_hoper=".$hopper_id." AND current_hoper_status='pending'";
		$total_members = Member::count('all', array('conditions' => $conditions));
		
		echo "TOTAL:".$total_members."\n\n";


		
		$work_day_minutes = 8 * 60;
		if($total_members > 1)
		{		
			$sleep = round($work_day_minutes / $total_members);	
		}
		else
		{
			$sleep = 1;
		}

		$sleep_interval = $sleep * 60;
		
		echo "Sleep Interval:".$sleep_interval."\n\n";		
		//exit();



		$timeout_sleep_interval = $sleep_interval+500;
		//Make sure the connection doesn't time out
		$query = "set @@wait_timeout=".$timeout_sleep_interval;
		Listing::connection()->query($query);
		echo "\n\n\n";
	
		$timeout_sleep_interval = $sleep_interval+500;
		//Make sure the connection doesn't time out
		$query = "set @@wait_timeout=".$timeout_sleep_interval;
		Listing::connection()->query($query);
		echo "\n\n\n";		



		//Find the listing that we need to publish for this particular HOPPER
		$sql	=   "SELECT
						listing.*,
						hopers_queue.hoper_id,
						hopers_queue.priority as hoper_priority
						FROM listing
						JOIN hopers_queue ON (hopers_queue.listing_id=listing.id) WHERE
						    listing.published=0 AND
						    listing.payment_id>0 AND
						    ( listing.approval=1 and listing.approved_at is not null)
						    AND listing.status='active'
						    AND (hopers_queue.priority != 0)
						    AND hopers_queue.hoper_id='".$hopper_id."'
						    AND city_id='".$city_id."' 
						    ORDER BY hopers_queue.priority ASC LIMIT 1";


		$listings = Listing::find_by_sql($sql);	
		
	//print_r($listings);
	//exit();	

		
		//We should have a listing for EACH CITY...
		foreach($listings as $listing)
		{				
			/*
			
				Once we actually start publishing to multiple cities, this code will have to change!
				
				We're going to have to call a background process whereby the hopper_id and listing_id is sent to the "publish.listing.php" script which will:
				 	-run for 8 hrs
				 	-randomize the order in which members are published
				 	-take care of updating the hopper_id
			
			
			
			    	
			echo date("Y-m-d H:i:s")."\t Processing ---------------------------------------------------LISTING ID: ".$listing->id." \n";
			
	
			//Grab only those members who've agreed to allow you to publish to their FB or Twitter Account or LinkedIn (SORTED BY CITY!)
			$conditions = "( current_hoper='".$hopper_id."' AND current_hoper_status='pending') AND (facebook_publish_flag=1 OR twitter_publish_flag=1 OR linkedin_publish_flag=1) and status='active'";
			$conditions.=" AND city_id=".intval($city_id);
			
			$members	=   Member::find('all', array('conditions' => $conditions));

	
		
		
			//Right here we're going to want an algorithm to randomize the order of when the members are published...
			if(is_array($members))
			    shuffle($members);
			
			//Ok, let's publish the listing for each member...
			$processedByMembers = 0;
			foreach($members as $member)
			{
				$processedByMembers++;
				try{
				    publishMember($hopper_id, $listing, $member, $sleep_interval);
				} catch (Exception $e)
				{
				    // Got an error, will report and follow on
				    mail("alen@alpha-male.tv","Exception Got on Publishing", $e->getMessage());

				}
			}

		}

		// Sending the notifications emails
		sendNotificationEmails($invalidTokenEmails);

		//Update the HOPPER COUNT
	    	if($processedByMembers>0) 
	    	{
			$hoper			=   Hoper::find_by_listing_id($listing->id);
			$hoper->counter	=   $hoper->counter+1;			
			$hoper->save();

	
		    	//Let's figure out if we're done with this HOPPER
		    	if($hoper->counter >= NUMBER_OF_HOPPERS)
		    	{
				
				echo "REMOVING:".$listing->id . " from the HOPPER since it is now FULLY PUBLISHED";
				
				$listingO	= Listing::find($listing->id);
				$listingO->published = $listingO->published + 1;
				$listingO->published_on = date("Y-m-d H:i:s");
				$listingO->save();
	
				// Removing from hoper list
				$hoper->delete();
				
				
				
				//Sending notification only after the listing has fully finished publishing
				if($sentOk > 0)
				{
					$listing->sendNotification($listing);
					//mail("alen@alpha-male.tv","1 Blast Sent", "Email Blast Was Successfully Sent");
				}				
		    	}	
	    	}
	    
	    	

	    	
	    	
	    if(isset($listing))	echo "PUBLISHING Complete for LISTING ID:".$listing->id."\n";	
	    else echo "NO LISTING FOUND\n";
	}


****************************************************************************/

?>
