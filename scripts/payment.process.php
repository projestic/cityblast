#!/usr/bin/env php
<?
/**************

/usr/local/php53/bin/php /home/abubic/cityblast.com/scripts/payment.process.php

***************/

require realpath(dirname(__FILE__) . '/../public/env.php');	
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

$emailer = Zend_Registry::get('alertEmailer');

ini_set('display_errors',1);

$today	=   date("Y-m-d");
echo "Processing payments ON ".$today."\n\n";

$conditions	 = "next_payment_date <= DATE(CURRENT_TIMESTAMP()) AND 
				(
					(
						account_type='paid' AND 
						payment_status NOT IN ('cancelled', 'signup', 'free', 'signup_expired', 'extended_trial', 'refund_issued', 'payment_failed') AND 
						(billing_attempt_fail IS NULL OR billing_attempt_fail < next_payment_date)
					)
						OR
					(
						payment_status = 'payment_failed' AND 
						DATE(billing_attempt_fail) >= next_payment_date AND
						billing_failures < 3 AND
						DATE(CURRENT_TIMESTAMP()) <= DATE_ADD(next_payment_date, INTERVAL 15 DAY) 
					)
				)"; // use 15-day check to avoid rebilling old payment failures

// Join to payment table to only find members with a previous payment

$members  = Member::all(array("conditions" => $conditions, "joins" => "JOIN payment ON payment.member_id = member.id AND payment_type=\"clientfinder\"", "group" => "member.id"));

$count = 0;
$success_count = 0;
$retry_count = 0;
$retry_success_count = 0;

$msg = '';

foreach($members as $member) 
{
	$msg .= "Processing payment for: ". $member->id . " " .$member->first_name . " " . $member->last_name . " RESULT: ";

	if (!$member->billing_failures) {
		$count++;
	} else {
		$retry_count++;
	}
	
	$gateway = $member->payment_gateway;
	
	if ($gateway) {
		if($gateway->processRecurringPayment($member))
		{
			if (!$member->billing_failures) {
				$success_count++;
			} else {
				$retry_success_count++;
			}
			$msg .= $gateway->code . " SUCCESS\n";
		}
		else
		{
			$msg .= $gateway->code . " FAIL (try #" . $member->billing_failures . ")\n";
		}
	}

	echo $msg . "\n\n";			
	echo "I'm about to got to sleep!\n";
	
	sleep(30);
}

$subject = $success_count . ' of ' . $count . ' (plus ' . $retry_success_count . ' of ' . $retry_count . ' retry) payments processed';
$email   = 
	"FAILED: " . ($count - $success_count) . "\n" . 
	"RETRY FAILED: " . ($retry_count - $retry_success_count) . "\n\n" . $msg;
$emailer->send($subject, $email, 'status-payment-batch');

