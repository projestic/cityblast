<?php
include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors', 1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();


Logger::log('Starting networks disabled remdiner process', Logger::LOG_TYPE_DEBUG);
Logger::startSection(Logger::LOG_TYPE_DEBUG);

$members = new Blastit_ActiveRecord_IteratorBuffered('Member', array(
	'conditions' => "status = 'active' AND payment_status = 'paid' AND " . 
					"(facebook_auto_disabled = 1 OR twitter_auto_disabled = 1 OR linkedin_auto_disabled = 1)"
));

foreach ($members as $member) {
	$member->notifyNetworksDisabled();
}

Logger::endSection(Logger::LOG_TYPE_DEBUG);
Logger::log('Finished', Logger::LOG_TYPE_DEBUG);
