#!/usr/bin/env php
<?php

/**************

 	/usr/local/php53/bin/php /home/abubic/city-blast.com/scripts/daily_summary.php 

***************/

require realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

// Number of new member signups
$totalSignups = Member::find_by_sql(
	'SELECT COUNT(*) AS total_signups FROM member WHERE DATE(created_at) = DATE(NOW()) LIMIT 1'
);
$totalSignups = (int)current($totalSignups)->total_signups;

// Number of payments
$totalPurchases = Payment::find_by_sql(
	'SELECT COUNT(*) AS total_purchases FROM payment WHERE DATE(created_at) = DATE(NOW()) and payment_type = "clientfinder" and amount > 0 LIMIT 1'
);
$totalPurchases = (int)current($totalPurchases)->total_purchases;


// Number of new PAID signups
$sql = "SELECT payment.member_id FROM payment JOIN member ON (member.id=payment.member_id)
				WHERE 
					payment.member_id IN ( 
						SELECT 
							DISTINCT(p2.member_id) 
						FROM payment p2 
						WHERE 
							DATE(p2.created_at) =  DATE( NOW() ) 
							AND p2.amount=0 
							AND p2.paypal_confirmation_number!='FREE_REFERRAL_CREDIT'
					) 
				GROUP BY 
					payment.member_id 
				HAVING 
					SUM(amount)=0";
$totalNewPurchases = 0;
$totalNewPurchasesQ = Payment::find_by_sql($sql);

foreach($totalNewPurchasesQ as $p) {
	$totalNewPurchases++;
}

// Revenue
$gross_rev = Payment::find_by_sql(
	'SELECT SUM(amount) AS total_purchases FROM payment WHERE DATE(created_at) = DATE(NOW()) and payment_type = "clientfinder" and amount > 0 LIMIT 1'
);

//print_r($gross_rev);
$gross_rev = $gross_rev[0]->total_purchases;



// Number of bookings
$totalBookings = Booking::find_by_sql(
	'SELECT COUNT(*) AS total_bookings FROM booking WHERE DATE(created_at) = DATE(NOW()) LIMIT 1'
);
$totalBookings = (int)current($totalBookings)->total_bookings;

// Number of comments
$totalComments = Comment::find_by_sql(
	'SELECT COUNT(*) AS total_comments FROM comments WHERE DATE(created_at) = DATE(NOW()) LIMIT 1'
);
$totalComments = (int)current($totalComments)->total_comments;

// Number of interactions
$totalInteractions = Interaction::find_by_sql(
    'SELECT COUNT(*) AS total_interactions FROM interaction WHERE DATE(created_at) = DATE(NOW()) LIMIT 1'
);
$totalInteractions = (int)current($totalInteractions)->total_interactions;

// Number of emails
$totalEmails = EmailLog::find_by_sql(
    'SELECT COUNT(*) AS total_emails FROM email_sent WHERE DATE(created_at) = DATE(NOW()) LIMIT 1'
);
$totalEmails = (int)current($totalEmails)->total_emails;

// NUmber of dropoffs
$query = "select count(*) as drops from member INNER JOIN payment ON (payment.member_id=member.id) where payment.amount>0 AND member.payment_status IN ('payment_failed','cancelled') AND  ( DATE(member.cancelled_date) = DATE(NOW() )  OR  DATE(member.billing_attempt_fail) = DATE( NOW( ) )  )";

$member_monthly_dropoffs 	=	Member::find_by_sql($query);
$totalDropoffs	=	(int)current($member_monthly_dropoffs)->drops;


// Total clicks
/***************************************************
$totalClicks = Click::find_by_sql(
	'SELECT SUM(count) AS total_clicks FROM click_stat WHERE DATE(date) = DATE(NOW()) LIMIT 1'
);
$totalClicks = (int)current($totalClicks)->total_clicks;
***************************************************/


$date = date('F d, Y');
$body = <<<HTML
<h3>{$date}</h3>
<table cellpadding="3" cellspacing="0">
	<tr>
		<th align="left">Number of new PAID signups:</th><td>{$totalNewPurchases}</td>
	</tr>

	<tr>
		<th align="left">Total number of new signups (paid/unpaid):</th><td>{$totalSignups}</td>
	</tr>
	<tr>
		<th align="left">Total number of dropoffs:</th><td>{$totalDropoffs}</td>
	</tr>
	<tr>
		<th align="left">Number of Clientfinder Payments:</th><td>{$totalPurchases}</td>
	</tr>
	<tr>
		<th align="left">Today's Gross Revenue:</th><td>&#36;{$gross_rev}</td>
	</tr>
	<tr>
		<th align="left">Number of bookings:</th><td>{$totalBookings}</td>
	</tr>
	<tr>
		<th align="left">Number of comments:</th><td>{$totalComments}</td>
	</tr>
	<tr>
		<th align="left">Number of interactions:</th><td>{$totalInteractions}</td>
	</tr>
	<tr>
		<th align="left">Number of emails:</th><td>{$totalEmails}</td>
	</tr>
</table>
HTML;

if(stristr(APPLICATION_ENV, "cityblast"))
{
	
	$mail = new Zend_Mail();
	$mail->addTo('alen@alpha-male.tv', 'Alen Bubich')
		->addTo('shaun@shaunnilsson.com', 'Shaun Nilsson')
		->addTo('nigel@cityblast.com', ' Nigel Young')
		->setSubject(COMPANY_NAME . ' - Daily Summary Email')
		->setBodyHtml($body)
		->send();
	    
	       echo "EMAIL SENT".PHP_EOL;
 
 }
 elseif(stristr(APPLICATION_ENV, "mortgagemingler"))
 {
 	$mail = new Zend_Mail();
	$mail->addTo('alen@alpha-male.tv', 'Alen Bubich')
		->addTo('shaun@shaunnilsson.com', 'Shaun Nilsson')
		->addTo('nigel@cityblast.com', 'Nigel Young')
		->addTo('ryan@retechulous.com', 'Ryan')
		->addTo('schoenly.josh@gmail.com', 'Josh')
		->setSubject(COMPANY_NAME . ' - Daily Summary Email')
		->setBodyHtml($body)
		->send();
	    
	       echo "EMAIL SENT".PHP_EOL;
 }
 elseif(stristr(APPLICATION_ENV, "insuranceposters"))
 {
 	$mail = new Zend_Mail();
	$mail->addTo('alen@alpha-male.tv', 'Alen Bubich')
		->addTo('shaun@shaunnilsson.com', 'Shaun Nilsson')
		->addTo('nigel@cityblast.com', 'Nigel Young')
		->addTo('nick@insuranceposters.com', 'Nick S')
		->setSubject(COMPANY_NAME . ' - Daily Summary Email')
		->setBodyHtml($body)
		->send();
	    
	       echo "EMAIL SENT".PHP_EOL;
 } 
    
?>