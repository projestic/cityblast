#!/usr/bin/env php
<?
 	
/**************

/usr/local/php53/bin/php sitemap.php

***************/
include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');


ini_set('display_errors', 1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

Logger::logTimeMarker();
Logger::log('Starting site map generation');

$site_map_generator =  new BlastIt_SiteMapGenerator();
$site_map_generator->setTempFilePath(__DIR__ . '/../sitemap.temp.xml');
$site_map_generator->addSiteMapFilePath(__DIR__ . '/../sitemap.xml');
$site_map_generator->addSiteMapFilePath(__DIR__ . '/../public/sitemap.xml');
$site_map_generator->generateSiteMap();

Logger::log('Completed site map generation');

class BlastIt_SiteMapGenerator 
{
	private $tempFilePath = null;
	private $tempFileHandle = null;
	private $siteMapFilePaths = array();
	
	public function setTempFilePath($filePath)
	{
		$this->tempFilePath = $filePath;
	}
	
	public function addSiteMapFilePath($filePath)
	{
		$this->siteMapFilePaths[] = $filePath;
	}
	
	public function generateSiteMap()
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$this->initTempFile();
		
		$data  = "<" . "?" . 'xml version="1.0" encoding="UTF-8"'.'?'.'>' . "\n";
		$data .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
		
		$data .= '<url><loc>'. APP_URL . '/member/index</loc></url>' . "\n";
		$data .= '<url><loc>'. APP_URL . '/index/terms</loc></url>' . "\n";
		$data .= '<url><loc>'. APP_URL . '/index/pricing</loc></url>' . "\n";
		$data .= '<url><loc>'. APP_URL . '/index/privacy</loc></url>' . "\n";

		$data .= '<url><loc>'. APP_URL . '/index/idx</loc></url>' . "\n";
		$data .= '<url><loc>'. APP_URL . '/index/faq</loc></url>' . "\n";		
		$data .= '<url><loc>'. APP_URL . '/index/api</loc></url>' . "\n";
		$data .= '<url><loc>'. APP_URL . '/index/content</loc></url>' . "\n";
		$data .= '<url><loc>'. APP_URL . '/index/training</loc></url>' . "\n";
		$data .= '<url><loc>'. APP_URL . '/index/ambassador</loc></url>' . "\n";
		$data .= '<url><loc>'. APP_URL . '/api/index</loc></url>' . "\n";
		$data .= '<url><loc>'. APP_URL . '/affiliate/signup</loc></url>' . "\n";
														
		
		$data .= '<url><loc>'. APP_URL . '/index/why-'.strtolower(COMPANY_NAME).'</loc></url>' . "\n";
		//$data .= '<url><loc>'. APP_URL . '/index/refer-an-agent</loc></url>' . "\n";

		
		if(stristr(APPLICATION_ENV, 'cityblast'))
		{ 
			$data .= '<url><loc>'. APP_URL . '/index/how-blasting-works</loc></url>' . "\n";
			$data .= '<url><loc>'. APP_URL . '/index/our-story</loc></url>' . "\n";				
			$data .= '<url><loc>'. APP_URL . '/listing/index</loc></url>' . "\n";	
			$data .= '<url><loc>'. APP_URL . '/blog/index</loc></url>' . "\n";	
		}
			
		
		//$data .= '<url><loc>'. APP_URL . '/index/about</loc></url>' . "\n";		
		//$data .= '<url><loc>'. APP_URL . '/index/guarantee</loc></url>' . "\n";
		//$data .= '<url><loc>'. APP_URL . '/member/how-'.strtolower(COMPANY_NAME).'-works</loc></url>' . "\n";
		//$data .= '<url><loc>'. APP_URL . '/member/join</loc></url>' . "\n";	
		
		
									
		$this->appendTempFile($data);
		
		
	
	
		$offset = 0;
		do 
		{
			unset($listings);
			$listings = Listing::all(array('select' => 'id', 'limit' => 100, 'offset' => $offset));
			
			$data = '';
			foreach ($listings as $listing)
			{
				$data .= '<url><loc>'.APP_URL . "/listing/view/".$listing->id.'</loc></url>' . "\n";
			}
			$this->appendTempFile($data);
			
			$offset += 100;
		} while (count($listings) == 100);





		//ADD BLOGS...
		if(stristr(APPLICATION_ENV, 'cityblast'))
		{ 		
			include_once (APPLICATION_PATH . "/../public/blog/wp-config.php");
			$wp_de_conn = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
			mysql_select_db(DB_NAME, $wp_de_conn);
			
			$query   = "SELECT ID, post_name, post_title, post_excerpt, post_date, guid ";
			$query  .= "FROM wp_dpvjuu_posts as P2 ";
			$query  .= "WHERE P2.post_status = 'publish' ";
			$query  .= "ORDER BY P2.post_date DESC ";
			
		
			$data = '';
			$result = mysql_query($query);
			if (!$result) 
			{
			    die('Invalid query: ' . mysql_error());
			}
	
	
			while ($row = mysql_fetch_assoc($result)) 
			{	
				//print_r($row);
				$data .= '<url><loc>'.APP_URL . "/blog/".$row['post_name'].'</loc></url>' . "\n";
				//echo APP_URL . "/blog/".$row['post_name'] . "\n";
			}
			$this->appendTempFile($data);
		}

	
	
		$data = '</urlset>';
		$this->appendTempFile($data);
		
		if ($this->tempFileHandle) 
		{
			fclose($this->tempFileHandle);
		}
		
		$this->duplicateTempFile();
		
		@unlink($this->tempFilePath);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	private function initTempFile()
	{
		if ($this->tempFilePath === null) 
		{
			throw new Exception('Temp file path not set');
		}
		
		if (!is_writable(dirname($this->tempFilePath))) 
		{
			throw new Exception('Temp file is not writable (' . $this->tempFilePath . ')');
		} 
		
		// In our example we're opening $filename in append mode.
		// The file pointer is at the bottom of the file hence
		// that's where $somecontent will go when we fwrite() it.
		if (!$this->tempFileHandle = fopen($this->tempFilePath, 'w')) 
		{
			throw new Exception('Cannot open temp file (' . $this->tempFilePath . ')');
		}
	}
	
	private function appendTempFile($data)
	{
		if (!$this->tempFileHandle) 
		{
			throw new Exception('Cannot write to temp file. File handle not initialised. (' . $this->tempFilePath . ')');
		}
		if (fwrite($this->tempFileHandle, $data) === FALSE) 
		{
			throw new Exception('Failed writing to temp file (' . $this->tempFilePath . ')');
		}
	}
	
	private function duplicateTempFile()
	{
		if (!is_array($this->siteMapFilePaths) || empty($this->siteMapFilePaths)) 
		{
			throw new Exception('No sitemap file paths added');
		}
		
		foreach ($this->siteMapFilePaths as $filePath) 
		{
			if (!copy($this->tempFilePath, $filePath)) 
			{
				throw new Exception('Fileding copying "' . $this->tempFilePath . '" to "' . $filePath . '"');
			}
		}
	}
}
