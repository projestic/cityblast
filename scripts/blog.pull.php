#!/usr/bin/env php
<?

/**************
/usr/local/php53/bin/php blog.pull.php 
***************/

include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');
$currDir = realpath(dirname(__FILE__));


ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_STRICT); // konstv

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();


Logger::log('Starting: blog.pull.php', Logger::LOG_TYPE_DEBUG);
Logger::startSection(Logger::LOG_TYPE_DEBUG);

$puller = new Blastit_BlogExternal_BlogPuller();
$puller->importFeeds($currDir);


Logger::endSection(Logger::LOG_TYPE_DEBUG);
Logger::log('Finished', Logger::LOG_TYPE_DEBUG);
