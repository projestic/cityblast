#!/usr/bin/env php
<?
	/**************
	
	/usr/local/php53/bin/php /home/abubic/city-blast.com/scripts/accounts.payable.php
	
	***************/
	
	//define('APPLICATION_ENV', 'jose');
	require realpath(dirname(__FILE__) . '/../public/env.php');	
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

	ini_set('display_errors',1);

	$emailer   = Zend_Registry::get('alertEmailer');
	$payables  = Payable::all(array("conditions" => 'created_at >= NOW() - INTERVAL 1 DAY', 'order' => 'affiliate_id'));

	$csvfile = '/tmp/payables-' . date('Y-m-d') . '.csv';

	ob_start();

?>

Payouts recorded in the last 24 hours: 

<?php if (count($payables)) : ?>
Payment date     Payout amount         Affiliate                    Member
-------------------------------------------------------------------------------------------
<?php	

	$fp = fopen($csvfile, 'w');

	fputcsv($fp, array('Payment date', 'Payout amount', 'Affiliate', 'Member' ));

	$total = 0;

	foreach ($payables as $payable) :

		$affiliate_owner_name = '--';
		$member_name = '--';
		$total += $payable->amount;

		if ($payable->affiliate) $affiliate_owner_name = $payable->affiliate->owner->name();
		if ($payable->member) $member_name = $payable->member->name(); 
			
		fputcsv($fp, array($payable->payment->created_at->format('Y-m-d'), '$' . number_format($payable->amount, 2), $affiliate_owner_name, $member_name ));
?><?= $payable->payment->created_at->format('Y-m-d') ?>    <?= str_pad('$' . number_format($payable->amount, 2), 16, " ", STR_PAD_LEFT) ?>         <?php echo str_pad($affiliate_owner_name, 28); ?> <?php echo str_pad($member_name, 24); ?> 
<?php 

	endforeach;
	
	echo PHP_EOL . "Total payouts: \${$total}";	
	fputcsv($fp, array('Total', '$' . $total));
	fclose($fp);	

else :
	
?>No payouts found.<?php

	endif;

	
	$email = ob_get_clean();
	
	$subject = "Affiliate payout report";
	
	if (file_exists($csvfile)) $emailer->attachFile($csvfile);
	
	$emailer->send($subject, $email, 'status-payment-batch');
	
	echo $email;
