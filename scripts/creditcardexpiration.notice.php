#!/usr/bin/env php
<?php

	error_reporting(E_ALL);
	ini_set("display_errors",1);
	/**************
	/usr/local/php53/bin/php /home/abubic/city-blast.com/scripts/creditcardexpiration.notice.php
	***************/
	
	require realpath(dirname(__FILE__) . '/../public/env.php');	
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');


	// making sure every payment has expiration dates
	require_once(APPLICATION_PATH . '/../lib/stripe_lib/Stripe.php'); 
	Stripe::setApiKey(STRIPE_SECRET);

	$sql = "SELECT p.* FROM payment p, payment_gateway pg WHERE (p.cc_expire_date IS NULL OR p.cc_expire_date ='' OR p.last_four='') and p.payment_gateway_id=pg.id and pg.code='STRIPE' ORDER BY id DESC LIMIT 100";
	$stripePayments  = Payment::find_by_sql($sql);

	foreach($stripePayments as $pay) {
		
		if( empty($pay->stripe_payment_id) ) {
			$pay->cc_expire_date    = "N/A";
			$pay->last_four    		= "N/A";
			$pay->save();
			echo "Payment #".$pay->id." has empty stripe_payment_id.\n";
			continue;
		}

		try {	
			$details = Stripe_Charge::retrieve($pay->stripe_payment_id);
		} catch(Exception $e) {
			var_dump($e);
		}

		if( is_object($details->card) && !is_null($details->card->exp_year) && !is_null($details->card->exp_month)) {
			$pay->cc_expire_date	=	str_pad($details->card->exp_month,2,"0",STR_PAD_LEFT).$details->card->exp_year;
			$pay->last_four			=	$details->card->last4;	
			$pay->save();
		} else {
		}

	}

	$sql     = "SELECT * FROM payment WHERE cc_expire_date='N/A' or cc_expire_date='00' or last_four='N/A' ORDER BY ID DESC";
	$stripePayments  = Payment::find_by_sql($sql);

	foreach($stripePayments as $pay) 
	{
		if( !empty($pay->member->stripe_customer_id) ) 
		{
			try {
				$details = Stripe_Customer::retrieve( $pay->member->stripe_customer_id );
				 if ( isset($details->active_card) &&  is_object($details->active_card) && !is_null($details->active_card->exp_year) && !is_null($details->active_card->exp_month)) {
					$pay->cc_expire_date    =       str_pad($details->active_card->exp_month,2,"0",STR_PAD_LEFT).$details->active_card->exp_year;
					$pay->last_four			=	$details->active_card->last4;	
					$pay->save();
				} else {
					$pay->cc_expire_date    = "Error";
					$pay->save();
				} 
			} catch (Exception $e) {
				echo $e->getMessage() . PHP_EOL;
				$pay->cc_expire_date = "Error";
				$pay->save();				
			}
		} else {
			$pay->cc_expire_date    = "Error";
            $pay->save();
		}
	}

	if( !empty($argv[1]) && strtoupper($argv[1])=='SYNCONLY' ) {
		die(" Updated payments CC expire date. Exiting now...\n\n");
	}

	// Check all members in paying conditions
	$conditions	= "account_type='paid' and 
	           	payment_status != 'cancelled' and 
	           	payment_status != 'signup' and 
	           	payment_status != 'free' and 
	           	payment_status != 'signup_expired' and 
	           	payment_status != 'extended_trial' and 
	           	payment_status != 'payment_failed' 
	           	";

	$batch  = 100;
	$offset = 0;

	do {
		$members  = Member::all(array("conditions" => $conditions, 'limit' => $batch, 'offset' => $offset));
		foreach($members as $member) 
		{
			if ($member->creditcardexpiringnextmonthNotify()) {
	 			echo "Notified {$member->email}" . PHP_EOL;
			}	
		}
		$offset += $batch;
	} while (count($members) == $batch);

