#!/usr/bin/env php
<?php
/**************
  /usr/local/php53/bin/php create.blog.from.top.content.php
***************/


require_once realpath(dirname(__FILE__) . '/../public/env.php');
require_once realpath(dirname(__FILE__) . '/../app/configs/environment.php');

require_once realpath(dirname(__FILE__) . '/../public/blog/wp-load.php');

//https://dev.twitter.com/docs/embedded-timelines
//https://developers.facebook.com/docs/plugins/embedded-posts/

$app_config = Zend_Registry::get('appConfig');
if ($app_config['wordpress']['enabled']) {
	include_once (APPLICATION_PATH . "/../public/blog/wp-config.php");
} else {
	throw new Exception('Wordpress not enabled (see application.ini: wordpress.enabled)');
}

ini_set('display_errors', 1);

$emailer = Zend_Registry::get('alertEmailer');

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
Logger::logTimeMarker();

$date = date('Y-m-d', strtotime('-1day'));
$top_contents = ListingTopByWeek::getContent($date);

if (empty($top_contents)) {
	$msg = 'Blog content top-content post generator was unable to find top content' . "\n\n";
	$emailer->send('Blog top-content post generator failed', $msg, 'blog-top-content');
	throw new Exception('Unable to find top content');
}

$member_id = !empty($app_config['sample_publish']['member_id']) ? $app_config['sample_publish']['member_id'] : null;
if (empty($member_id)) {
	$msg = 'Sample publishing member id not set' . "\n\n";
	$emailer->send('Blog top-content post generator failed', $msg, 'blog-top-content');
	throw new Exception('Sample publishing member id not set');
}
$member = Member::find($member_id);

$fbpost_failures = 0;
foreach($top_contents as $x => $top_content) {
	$publish_queue = PublishingQueue::find_by_listing_id($top_content->listing_id);
	if ($publish_queue) {
		$result = $member->publishFacebookFanpage($publish_queue, true);
		if ($result) {
			$posts_fb[$x]['listing_id'] = $top_content->listing_id;
			$posts_fb[$x]['id'] = $result['post_id'];
			$member->likePost($result['post_id']);
		} else {
			$fbpost_failures++;
			Logger::log('Failed posting listing ' . $listing->id . ' to sample site');
		}
	}
}

if ($fbpost_failures) {
	$msg = 'Failed to post ' . $fbpost_failures . ' listings to the sample site' . "\n\n";
	$emailer->send('Blog top-content post generator failed', $msg, 'blog-top-content');
}

$content = '';
if (!empty($posts_fb)) {
ob_start();
?>
<p>Looking for great ideas and great inbound marketing content to post to your wall? Well, you’ve come to the right place. Every week we compile the absolute best content we have into our weekly best of list. Feel free to “borrow” these great content ideas it will be our little secret!</p>

<p>Too busy to post this content to your Fanpage every week? No problem! We can take care of that for you. Our <i>Social Experts</i> are ready to take over the tedium of managing your Fanpage, Twitter and LinkedIn accounts. <a href=”/member/index”>Start your FREE 14 day trial</a> of <?=COMPANY_NAME;?> today.</p>

<center>
<?php foreach($posts_fb as $post): ?>
<div class="fb-post" data-href="https://www.facebook.com/FacebookDevelopers/posts/<?=$post['id']?>" data-width="568"></div>
<div style='margin-top: 25px;'></div>
<?php endforeach; ?>
</center>
<?php
	$content = ob_get_contents();
	$content = trim($content);
	ob_end_clean();
	
	$excerpt = "Ever wondered what it would be like to have your very own Social Expert? " .
				"Here's a sneak peek.";

	$post_data_wp = array(
		'post_title' => 'Our Best Content - ' . date('F jS, Y'),
		'post_content' => $content,
		'post_excerpt' => $excerpt,
		'post_status' => 'draft',
		'post_author' => 1
	);

	$post_wp_id = Blastit_WordPress::postCreate($post_data_wp);
	Logger::log('Word press post result: ' . print_r($post_wp_id, true), Logger::LOG_TYPE_DEBUG);

	if ($post_wp_id) {
		foreach ($posts_fb as $post_fb) {
			$listing = Listing::find_by_id($post_fb['listing_id']);
			Blastit_WordPress::postAttachListingImages($post_wp_id, $listing);
		}
		if (!empty($app_config['wordpress']['top_content']['thumbnail_id'])) {
			// Set post tumbnail
			Logger::log('Setting post thumbnail: ' . $app_config['wordpress']['top_content']['thumbnail_id'], Logger::LOG_TYPE_DEBUG);
			add_post_meta($post_wp_id, '_thumbnail_id', $app_config['wordpress']['top_content']['thumbnail_id'], true);
		}
	}
	
	$msg = 'Blog post successfully created' . "\n\n";
	$emailer->send('Blog top-content post generated', $msg, 'blog-top-content');
	
	Logger::log('Blog has been successfully created');
} else {
	$msg = 'No facebook posts generated' . "\n\n";
	$emailer->send('Blog top-content post generator failed', $msg, 'blog-top-content');
	
	Logger::log('No facebook posts generated');
}
