<?php
/**
 * This script is intended to update "frequency_id" of all active members
 *
 * /usr/local/php53/bin/php update.member.frequency.php
 *
 * @author nur<shahid9829@gmail.com>
 */
$timestamp1	= microtime(TRUE);

date_default_timezone_set('America/New_York');




try {
	if (!defined('SCRIPTS_PATH')) {
		define('SCRIPTS_PATH', dirname(__FILE__));
	}
	require SCRIPTS_PATH . '/../public/env.php';
	require SCRIPTS_PATH . '/../app/configs/environment.php';
} catch (Exception $ex) {
	throw new Exception("Error at line ". $ex->getLine() ." of ". $ex->getFile());
}



echo "Member's 'frequency_id' update process is started!". PHP_EOL;


$members	= Member::all(array( 'conditions' => 'status = "active"' ));
$number_of_updates	= 0;

foreach ($members as $member) {
	if ($member->frequency_id == 1) {
		$member->frequency_id	= 4;
	} elseif ($member->frequency_id == 2 || $member->frequency_id == 3) {
		$member->frequency_id	= 7;
	} else {
		continue;
	}
	$member->save();
	$number_of_updates++;
}

echo "Total ". $number_of_updates ." members' 'frequency_id' have been updated". PHP_EOL;
echo "It takes total ". round((microtime(TRUE) - $timestamp1), 3) ." sec". PHP_EOL;

echo PHP_EOL;


$members	= Member::all();

foreach($members as $member)
{
	echo $member->id . " " . " " . $member->first_name . " " . $member->last_name . " ". $member->city_id . " ";
	//echo $member->default_city->name . "<BR>";	
	
	if($member->city_id)
	{
		echo $member->default_city->name . " " .$member->default_city->country  . " ---> \t\t";


		//CANADA = 1102, UNITED STATES = 1103	
		if($member->city_id == 1)
		{
			$member->publish_city_id = 1;
		}
		elseif($member->city_id == 2)
		{		
			$member->publish_city_id = 2;
		}
		elseif(strtoupper($member->default_city->country) == "CANADA")
		{
			$member->publish_city_id = 1102;
		}
		elseif(strtoupper($member->default_city->country) == "UNITED STATES")
		{
			$member->publish_city_id = 1103;
		}	
		
		echo $member->publish_city_id . "\n";	
	}
	else
	{
		echo "Missing city\n";	
		$member->city_id = 1102;
	}
	
	$member->save();
}

echo "\n\n";
echo "Completed updating all Publish City ID's\n";

?>