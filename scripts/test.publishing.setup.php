#!/usr/bin/env php
<?

	/**************
	
	/usr/local/php53/bin/php test.publishing.setup.php 
	
	***************/
    
	require realpath(dirname(__FILE__) . '/../public/env.php');
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

	// Update friend list only 1 day after last update
	define("FRIEND_LIST_VALID_UNTIL", date("Y-m-d H:i:s",strtotime("-1 day")));


	$query = "set @@wait_timeout=1200";
	Member::connection()->query($query);
	
	
	
	
	$query = "delete from publishing_queue";
	Member::connection()->query($query);		
	

	
	$query = "update listing set approval=0, published=0";
	Member::connection()->query($query);		
	
	

//Add 3 LISTINGS to INVENTORY UNIT A


	//MANUAL BLAST
	$listing	= Listing::find(346);
	$listing->approval		=   1;
	$listing->approved_by_id	=   35;
	$listing->approved_at	=   date("Y-m-d H:i:s");
	
	$listing->save();

	$publish = new PublishingQueue();
	
	$publish->listing_id 	= $listing->id;
	$publish->city_id 		= $listing->city_id;
	$publish->inventory_id 	= 1;
	$publish->hopper_id 	= 1;
	$publish->priority 		= 10;
	$publish->current_count 	= 0;
	
	$publish->min_inventory_threshold 	= $listing->list_city->min_inventory_threshold;
	$publish->max_inventory_threshold 	= $listing->list_city->max_inventory_threshold;
	
	$publish->save();



	//Update the APPROVAL to YES
	$listing	= Listing::find(362);
	$listing->approval		=   1;
	$listing->approved_by_id	=   35;
	$listing->approved_at	=   date("Y-m-d H:i:s");
	
	$listing->save();

	$publish = new PublishingQueue();
	
	$publish->listing_id 	= $listing->id;
	$publish->city_id 		= $listing->city_id;
	$publish->inventory_id 	= 1;
	$publish->hopper_id 	= 2;
	$publish->priority 		= 10;
	$publish->current_count 	= 0;
	
	$publish->min_inventory_threshold 	= $listing->list_city->min_inventory_threshold;
	$publish->max_inventory_threshold 	= $listing->list_city->max_inventory_threshold;
	
	$publish->save();
	
	
	//Update the APPROVAL to YES
	$listing	= Listing::find(363);
	$listing->approval		=   1;
	$listing->approved_by_id	=   35;
	$listing->approved_at	=   date("Y-m-d H:i:s");
	
	$listing->save();

	$publish = new PublishingQueue();
	
	$publish->listing_id 	= $listing->id;
	$publish->city_id 		= $listing->city_id;
	$publish->inventory_id 	= 1;
	$publish->hopper_id 	= 3;
	$publish->priority 		= 10;
	$publish->current_count 	= 0;
	
	$publish->min_inventory_threshold 	= $listing->list_city->min_inventory_threshold;
	$publish->max_inventory_threshold 	= $listing->list_city->max_inventory_threshold;
	
	$publish->save();	





//Add 3 LISTINGS to INVENTORY UNIT B


	//Update the APPROVAL to YES
	$listing	= Listing::find(368);
	$listing->approval		=   1;
	$listing->approved_by_id	=   35;
	$listing->approved_at	=   date("Y-m-d H:i:s");
	
	$listing->save();

	$publish = new PublishingQueue();
	
	$publish->listing_id 	= $listing->id;
	$publish->city_id 		= $listing->city_id;
	$publish->inventory_id 	= 2;
	$publish->hopper_id 	= 1;
	$publish->priority 		= 10;
	$publish->current_count 	= 0;
	
	$publish->min_inventory_threshold 	= $listing->list_city->min_inventory_threshold;
	$publish->max_inventory_threshold 	= $listing->list_city->max_inventory_threshold;
	
	$publish->save();



	//Update the APPROVAL to YES
	$listing	= Listing::find(369);
	$listing->approval		=   1;
	$listing->approved_by_id	=   35;
	$listing->approved_at	=   date("Y-m-d H:i:s");
	
	$listing->save();

	$publish = new PublishingQueue();
	
	$publish->listing_id 	= $listing->id;
	$publish->city_id 		= $listing->city_id;
	$publish->inventory_id 	= 2;
	$publish->hopper_id 	= 2;
	$publish->priority 		= 10;
	$publish->current_count 	= 0;
	
	$publish->min_inventory_threshold 	= $listing->list_city->min_inventory_threshold;
	$publish->max_inventory_threshold 	= $listing->list_city->max_inventory_threshold;
	
	$publish->save();
	
	
	//Update the APPROVAL to YES
	$listing	= Listing::find(370);
	$listing->approval		=   1;
	$listing->approved_by_id	=   35;
	$listing->approved_at	=   date("Y-m-d H:i:s");
	
	$listing->save();

	$publish = new PublishingQueue();
	
	$publish->listing_id 	= $listing->id;
	$publish->city_id 		= $listing->city_id;
	$publish->inventory_id 	= 2;
	$publish->hopper_id 	= 3;
	$publish->priority 		= 10;
	$publish->current_count 	= 0;
	
	$publish->min_inventory_threshold 	= $listing->list_city->min_inventory_threshold;
	$publish->max_inventory_threshold 	= $listing->list_city->max_inventory_threshold;
	
	$publish->save();	
	
	
	
	//Set everyone's FB, TW and LN publishing to OFF
	$query = "update member set facebook_auto_disabled=1, twitter_auto_disabled=1, linkedin_auto_disabled=1, inventory_id=2, current_hoper=2 where id!=35";
	Member::connection()->query($query);


	//TURN ON FB, TW and LN for desired MEMBERS
	$query = "update member set facebook_auto_disabled=0, twitter_auto_disabled=0, linkedin_auto_disabled=0, inventory_id=1, current_hoper=1, current_hoper_status='pending', city_id=1, status='active', frequency_id=2 where id=35";
	Member::connection()->query($query);


	//DELETE the POSTS from the POST table so that the POST will go through (optional)...
	$query = "delete from post";
	Member::connection()->query($query);
	

/****************	
	
	//Publish TORONTO, INVENTORY UNIT A, HOPPER 1
	$command = "/usr/local/php53/bin/php personal.post.php 1 1 1";
	system($command);



	//Publish TORONTO, INVENTORY UNIT A, HOPPER 2
	$command = "/usr/local/php53/bin/php personal.post.php 1 1 2";
	system($command);	






echo "\n\n\n";


//Now let's test 2 people in ONE HOPPER

	//DELETE the POSTS from the POST table so that the POST will go through (optional)...
	$query = "delete from post";
	Member::connection()->query($query);
	

	//TURN ON FB, TW and LN for desired MEMBERS
	$query = "update member set facebook_auto_disabled=0, inventory_id=1, current_hoper=1, current_hoper_status='pending', city_id=1, status='active', frequency_id=7 where id IN(35,216)";
	Member::connection()->query($query);


	//Publish TORONTO, INVENTORY UNIT A, HOPPER 1
	$command = "/usr/local/php53/bin/php personal.post.php 1 1 1";
	system($command);



	//Publish TORONTO, INVENTORY UNIT A, HOPPER 2
	$command = "/usr/local/php53/bin/php personal.post.php 1 1 2";
	system($command);		


*****************/



//Now let's test 3 people in THREE SEPARATE HOPPERS

	//DELETE the POSTS from the POST table so that the POST will go through (optional)...
	$query = "delete from post";
	Member::connection()->query($query);
	

	//TURN ON FB, TW and LN for desired MEMBERS
	$query = "update member set facebook_auto_disabled=0, inventory_id=1, current_hoper=1, current_hoper_status='pending', city_id=1, status='active', frequency_id=7 where id=35";
	Member::connection()->query($query);

	//TURN ON FB, TW and LN for desired MEMBERS
	$query = "update member set facebook_auto_disabled=0, inventory_id=1, current_hoper=2, current_hoper_status='pending', city_id=1, status='active', frequency_id=7 where id=216";
	Member::connection()->query($query);
	
	//TURN ON FB, TW and LN for desired MEMBERS
	$query = "update member set facebook_auto_disabled=0, inventory_id=1, current_hoper=3, current_hoper_status='pending', city_id=1, status='active', frequency_id=7 where id=236";
	Member::connection()->query($query);	


	//Publish TORONTO, INVENTORY UNIT A, HOPPER 1
	$command = "/usr/local/php53/bin/php personal.post.php 1 1 1";
	system($command);



	//Publish TORONTO, INVENTORY UNIT A, HOPPER 2
	$command = "/usr/local/php53/bin/php personal.post.php 1 1 2";
	system($command);


	//Publish TORONTO, INVENTORY UNIT A, HOPPER 2
	$command = "/usr/local/php53/bin/php personal.post.php 1 1 3";
	system($command);
?>