#!/usr/bin/env php
<?php

/****

/usr/local/php53/bin/php sync.postswithreach.php

****/

include realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

$query = "set @@wait_timeout=1200";
Member::connection()->query($query);

ini_set('memory_limit', '500M');
ini_set('display_errors',1);

$start = $argv[1];
$query = "select * from post where ( reach=0 OR reach IS NULL) AND type IS NOT NULL AND id>".$start." AND result=1 AND status != 'deleted' order by id asc limit 2000";

$posts = (array)Post::find_by_sql($query);
echo "Processing from id ".$posts[0]->id." to ".$posts[count($posts)-1]->id."\n";
	
foreach($posts as $post)
{
	if($post->result!=1) continue;

	$message = strtolower($post->message);
	if( strtoupper($post->type) =='FACEBOOK' && preg_match("/(?:fanpage)/i",$message))
	{
		list($fanpageid,$user)	=	explode("_",$post->fb_post_id);
		$fanpage = FanPage::find_by_fanpage_id($fanpageid);			
		$post->reach = (int)$fanpage->likes;					
		$post->save();
	
	} elseif(strtoupper($post->type) =='FACEBOOK'  && !preg_match("/(?:fanpage)/i",$message)) {

		$post->reach = (int)$post->member->facebook_friend_count;					
		$post->save();
	} elseif (strtoupper($post->type) =='TWITTER') {
		$post->reach = (int)$post->member->twitter_followers;					
		$post->save();
	} elseif (strtoupper($post->type) =='LINKEDIN') {
		$post->reach = (int)$post->member->linkedin_friends_count;					
		$post->save();
	}

unset($post);
}

unset($posts);
	
	
?>
