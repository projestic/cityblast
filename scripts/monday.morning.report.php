#!/usr/bin/env php
<?php
/**************

/usr/local/php53/bin/php monday.morning.report.php

***************/

include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');



// Define DEBUG_MODE to False if you want to update database records
define('DEBUG_MODE', false);
ini_set('display_errors', 1);

// Get current date to use in emails body
$currentDate = date('F d, Y');





//RETECHULOUS
$query =
	'SELECT
		COUNT(`payment`.`id`) AS `count`
		, `member`.`payment_status`
		, `payment`.`created_at`
	FROM `payment`
	JOIN `member` ON `payment`.`member_id` = `member`.`id`
	WHERE
		`member`.`referring_affiliate_id` = 19
		AND `member`.`payment_status` = "paid"
	GROUP BY
		YEAR(`payment`.`created_at`)
		, MONTH(`payment`.`created_at`)
		, DAY(`payment`.`created_at`)
	ORDER BY
		`payment`.`created_at` DESC';

$payments = Payment::find_by_sql($query);

	$html = '<h3>
				<span>Retechulous Conversion By Day.</span><br />
				<small>'.$currentDate.'</small>
			</h3>
			<table cellpadding="3" cellspacing="0">
				<thead>
					<tr>
						<th>Created at</th>
						<th>Count</th>					
					</tr>
				</thead>
				<tbody>';


			$total = 0;
			foreach ($payments as $key => $payment) 
			{
				$date = (!is_null($payment->created_at)) ? $payment->created_at->format('M-d-y') : '';
				$html .= "\n\t" . '<tr><td>' . $date . '</td><td align="right">' . $payment->count . '</td></tr>';
				
				$total += $payment->count;
			}
			
			$html .= '<tr><td>Total:</td><td align="right">' . number_format($total) . '</td></tr>';
			
		$html .= '</tbody></table>';




// Send first email
$mail = new Zend_Mail();
$mail->addTo('schoenly.josh@gmail.com')
	->addTo('ryan@retechulous.com')
	->addCc('shaun@cityblast.com')
	->addCc('alen@cityblast.com')	
	->addTo('gwallace@lwolf.com')
	->addTo('mhall@lwolf.com')	
	->setSubject('Monday morning report. Conversions by Day.')
	->setBodyHtml($html)
	->send();


// Send first email
/*************************************
$mail = new Zend_Mail();
$mail->addTo('alen@alpha-male.tv')
	->setSubject('Weekly Report: Conversions by Day: '.$currentDate.' - CityBlast.com')
	->setBodyHtml($html)
	->send();
************************************/

echo "\n\n"."Retechulous Conversion email sent\n\n";




//LONEWOLF
$query =
	'SELECT
		COUNT(`payment`.`id`) AS `count`
		, `member`.`payment_status`
		, `payment`.`created_at`
	FROM `payment`
	JOIN `member` ON `payment`.`member_id` = `member`.`id`
	WHERE
		`member`.`referring_affiliate_id` = 18
		AND `member`.`payment_status` = "paid"
	GROUP BY
		YEAR(`payment`.`created_at`)
		, MONTH(`payment`.`created_at`)
		, DAY(`payment`.`created_at`)
	ORDER BY
		`payment`.`created_at` DESC';

$payments = Payment::find_by_sql($query);

	$html = '<h3>
				<span>LoneWolf Conversion By Day.</span><br />
				<small>'.$currentDate.'</small>
			</h3>
			<table cellpadding="3" cellspacing="0">
				<thead>
					<tr>
						<th>Created at</th>
						<th>Count</th>					
					</tr>
				</thead>
				<tbody>';


			$total = 0;
			foreach ($payments as $key => $payment) 
			{
				$date = (!is_null($payment->created_at)) ? $payment->created_at->format('M-d-y') : '';
				$html .= "\n\t" . '<tr><td>' . $date . '</td><td align="right">' . $payment->count . '</td></tr>';
				
				$total += $payment->count;
			}
			
			$html .= '<tr><td>Total:</td><td align="right">' . number_format($total) . '</td></tr>';
			
		$html .= '</tbody></table>';




// Send first email
$mail = new Zend_Mail();
$mail->addTo('gwallace@lwolf.com')
	->addTo('mhall@lwolf.com')
	->addCc('shaun@cityblast.com')
	->addCc('alen@cityblast.com')
	->addCc('grace@cityblast.com')	
	->setSubject('Monday morning report. Conversions by Day.')
	->setBodyHtml($html)
	->send();


// Send first email
/***************
$mail = new Zend_Mail();
$mail->addTo('alen@alpha-male.tv')
	->setSubject('Weekly Report: Conversions by Day: '.$currentDate.' - CityBlast.com')
	->setBodyHtml($html)
	->send();
******************/

echo "Lonewolf Conversion email sent\n\n";





//CANADIAN REAL ESTATE WEALTH
$query =
	'SELECT
		`click_stat`.`listing_id`
		, `url_mapping`.`url`
		, count(`click_stat`.`id`) AS `total_clicks`
		, `listing`.`created_at`
	FROM `click_stat`
		JOIN `url_mapping` ON `click_stat`.`listing_id` = `url_mapping`.`listing_id`
		JOIN `listing` ON `listing`.`id` = `url_mapping`.`listing_id`
	WHERE
		`url_mapping`.`url` LIKE "%canadianreal%"
	GROUP BY
		`click_stat`.`listing_id`
	ORDER BY
		`listing_id` DESC';

$clicks = Click::find_by_sql($query);


$html = null;
$html = '
		<h3>
			<span>Monday morning report. Clicks stats.</span><br />
			<small>'.$currentDate.'</small>
		</h3>
		<table cellpadding="3" cellspacing="0">
			<thead>
				<tr>
					<th>Listing ID</th>
					<th>URL</th>
					
					<th>Last click</th>
					
					<th>Total clicks</th>
				</tr>
			</thead>
			<tbody>';
			
			
			$total = 0;
			
			foreach ($clicks as $key => $click) 
			{
				$date = (isset($click->created_at) && !is_null($click->created_at) && !empty($click->created_at)) ? $click->created_at : '';
				$html .= "\n\t" . '<tr><td align="center">' . $click->listing_id . '</td><td>' . $click->url . '</td><td>' . $date . '</td><td align="right">' . number_format($click->total_clicks) . '</td></tr>';
				
				$total += $click->total_clicks;
			}
			
			$html .= '<tr><td colspan="3">Total:</td><td align="right">' . number_format($total) . '</td></tr>';
			
	$html .= '</tbody></table>';






// Send second email
$mail = new Zend_Mail();
$mail->addTo('julia.comitale@kmimedia.ca')
	->addCc('grace@cityblast.com')
	->addCc('alen@cityblast.com')
	->addCc('shaun@cityblast.com')
	->setSubject('Monday morning report. Clicks stats. CityBlast.com')
	->setBodyHtml($html)
	->send();
	

// Send second email
/******************
$mail = new Zend_Mail();
$mail->addTo('alen@alpha-male.tv')
	->setSubject('Weekly Report: Clicks Stats: '.$currentDate.' - CityBlast.com')
	->setBodyHtml($html)
	->send();	
**********************/

echo "Click stat email sent\n\n";


	
?>