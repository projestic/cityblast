#!/usr/bin/env php
<?

	/**************

	/usr/local/php53/bin/php trial.notify.php

	***************/
	include realpath(dirname(__FILE__) . '/../public/env.php');
	include realpath(dirname(__FILE__) . '/../app/configs/environment.php');

	ini_set('display_errors', 1);

    $members = Member::all( array('conditions' => array("created_at <= NOW() AND payment_status = 'signup' AND trial_email = 0;")) );
	
	$day = date("Y-m-d", strtotime("+7 days"));
	
	foreach($members as $member)
	{
		$member->trialNotify();
		
		$next = new DateTime();
		$next->setTimestamp($member->created_at->getTimestamp() + 7 * 60 * 60 * 24);
		$member->next_payment_date = $next;
		$member->save();	
	
	}
	
