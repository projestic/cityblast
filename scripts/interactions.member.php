#!/usr/bin/env php
<?

	/**************

	/usr/local/php53/bin/php interactions.member.php

	***************/
	include realpath(dirname(__FILE__) . '/../public/env.php');
	
	require realpath(dirname(__FILE__) . '/../app/configs/environment.php');
	
	ini_set('display_errors',1);
	
	$conditions = array();
	
	$conditions = "payment_status = 'paid' or payment_status='free' or payment_status='extended_trial' or payment_status='signup'";
	$members = Member::find('all', array('conditions' => $conditions, 'order' => 'id ASC'));
	
	$member_count = count($members);
	
	$running_total = 0;
	foreach($members as $member)
	{
		$interaction_score	= 0;
		$total_clicks 		= 0;
		$fb_share 		= 0;
		$tw_share 		= 0;
		$email_share 		= 0;
		$bookings 		= 0;
		$total_likes  		= 0;
		$total_comments 	= 0;
		 
		
		$email_share = $tw_share = $email_share = 0;
		if($member->interactions)
		{
			foreach($member->interactions as $interaction)
			{
				if($interaction->type == "twitter_share")
				{
					$tw_share++;
				}
				if($interaction->type == "facebook_share")
				{
					$fb_share++;
				}					
				if($interaction->type == "share_listing_email")
				{
					$email_share++;
				}					
			}
		}
		
		$query  = "select sum(click_stat.count) as total_clicks from click_stat ";
		$query .= "where member_id=".$member->id . " ";
//echo $query . "\n";
		
		$clicks = Click::find_by_sql($query);
		$total_clicks = $clicks[0]->total_clicks;
			
		$query = "select sum(facebook_likes) as total_likes from post where member_id=".$member->id;
//echo $query . "\n";

		$likes = Post::find_by_sql($query);
		$total_likes = $likes[0]->total_likes;
	
		$query = "select sum(facebook_comments) as total_comments from post where member_id=".$member->id;
//echo $query . "\n";

		$comments = Post::find_by_sql($query);
		$total_comments = $comments[0]->total_comments;
				
		$bookings = count($member->bookings);
		$interactions = $total_clicks + $fb_share + $tw_share + $email_share + $bookings + $total_likes + $total_comments;

	$params['conditions'] = null;
	$params['conditions'] = "member_id=".$member->id . " and result=1";
	$total_blasts = Post::count("all", $params);
	//echo $total_blasts;
		
		if($total_blasts) $interaction_score = round(($interactions / $total_blasts), 2);
		else $interaction_score = 0;
		
		
		$member->interaction_score = $interaction_score;
		$member->save();
		
		echo $member->id . " " . $member->first_name . " " . $member->last_name . " == " . $interaction_score . " was saved\n";
		$running_total+=$interaction_score;
		
	}
	
	echo "The average Interaction Score is:".round($running_total/$member_count,2)."\n\n";
	
	$site = new SiteSetting();
	$site->interaction_score_average = round($running_total/$member_count,2);
	$site->save();
	
?>