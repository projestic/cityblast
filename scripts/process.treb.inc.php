<?


/**
 * parsing CSV file with given mapper
 *
 * @param string $file	file path
 * @param array $mapper	array of mapping fields like db_field => csv_field
 * @return array
 */
function parse_csv($file, array $mapper) {

	$listings	= array();

	$counter = 0;
	
	try 
	{
		if(!$fh	= fopen($file, 'r'))
		{
			echo "COULD NOT OPEN FILE!\n";
		}	
		
		if ($fh) 
		{
			$csv_cols	= fgetcsv($fh);


			
			if ($csv_cols && $csv_cols[0] == 'No Records Found') 
			{
				echo "No Records Found at \"". $file ."\"\r\n";
				return $listings;
			}

			$csv_cols	= array_flip($csv_cols);

			while (($row = fgetcsv($fh)) !== FALSE) 
			{
	
				$list	= array();
				foreach ($mapper as $key => $map) 
				{
					if (is_array($map)) 
					{
						/*
						 * if the value of a db_field is a combination of the value of multiple csv_fields
						 * then combined them with pipe (|)
						 */
						$tmp	= array();
						foreach ($map as $fld) {
							$tmp[]	= $row[$csv_cols[$fld]];
						}

						if ($key == 'street') {
							$list[$key]	= $tmp[0] ." ". $tmp[1];
							if ($tmp[2]) {
								$list[$key] .= " Unit ". $tmp[2];
							}
							$list[$key]	.= " ". $tmp[3];
						} else {
							$list[$key]	= implode('|', $tmp);
						}
					} else {
						
						//echo "KEY: ".$key . " CSV COL: " . $csv_cols[$map] . " VALUE: " . $row[$csv_cols[$map]] . "\n";						
						$list[$key]	= $row[$csv_cols[$map]];
					}
				}
				if (!is_numeric(substr($list['mls'], 0, 1))) {
					#$list['mls']	= substr($list['mls'], 1);
				}
				$listings[]	= $list;
				
			}
			fclose($fh);
			#pre($listings);
		}
		
		
	} catch (Exception $e) {
		echo "Error at ". $e->getLine() ." of ". $e->getFile() ."\r\n". $e->getMessage() ."\r\n". $e->getTraceAsString() ."\r\n";
	}

//print_r($listings);
	
	return $listings;
}



/**
 * parsing command-line arguments
 *
 * @param string $arg_str
 * @return array
 */
function parse_args($argvs) {
	$args	= array();
	foreach ($argvs as $param) {
		if ($param) {
			$tmp	= explode('=', $param, 2);
			if (count($tmp) > 1) {
				$args[$tmp[0]]	= $tmp[1];
			} else {
				$args[]	= $tmp[0];
			}
		}
	}
	return $args;
}

/**
 * @param Exception $e
 */
function exception(Exception $e) {
	echo "Error at ". $e->getLine() ." of ". $e->getFile() ."\r\n". $e->getMessage() ."\r\n";
}

