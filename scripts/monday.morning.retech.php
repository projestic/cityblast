<?php

/****
/usr/local/php53/bin/php /home/abubic/city-blast.com/scripts/30.day.db.backup.php
****/
//C:/wamp/bin/mysql/mysql5.5.20/bin/

require realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors', 1);
echo date("Y-m-d H:i:s") . "\t Starting\n";

$emailer   = Zend_Registry::get('alertEmailer');

$members = Member::all( array('conditions' => 'referring_affiliate_id = 1000018', 'order' => 'created_at DESC') );

$report = new Blastit_Report;
$report->enableHTML();
$report->enableCSV();
$report->setColumnTitles( array( "First name", "Last name", "Email", "Payment status", "Brokerage", "Address", "Phone", "City", "State/Province") );

foreach ($members as $member) {
	$address = $member->address;
	if ($member->address2) $address .= ', ' . $member->address2;
	$city = $member->city;
	$state = $member->state;
	if (count($member->payments)) {
		$payment = $member->payments[0];
		if ($payment->address1 != 'undefined') {
			$address = $payment->address1;
			if ($payment->address2) $address .= ', ' . $payment->address2;
		}
		$city = $payment->city;
		$state = $payment->state;
	}
	$report->addRow( 
		array($member->first_name, $member->last_name, $member->email, ucfirst(str_replace('_', ' ', $member->payment_status)), $member->brokerage, $address, $member->phone, $city, $state)
	);
}

if (!count($members)) {
	$report->addRow(array('(No members found)'));
} 

$report->generate();

$email = '<html><body>' . $report->getHTML() . '</body></html>';

$outboundFile = '/tmp/Retechulous weekly report for ' . date('Y-m-d') . '.csv';

rename($report->getCSVFile(), $outboundFile);
$emailer->setConvertNewlines(false);
$emailer->attachFile($outboundFile);
$emailer->send("Weekly report: Retechulous", $email, 'retech-weekly-report');

echo $email;