#!/usr/bin/env php
<?

/**************

/usr/local/php53/bin/php publish.post.php LOWER_LIMIT UPPER_LIMIT
/usr/local/php53/bin/php /home/abubic/city-blast.com/scripts/publish.post.php 1001 1500

***************/
include realpath(dirname(__FILE__) . '/../public/env.php');
include realpath(dirname(__FILE__) . '/../app/configs/environment.php');


//include_once "publish.post.inc.php";

ini_set('display_errors', 1);


Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

Logger::logTimeMarker();


$input_is_valid = true;
$account_type = !empty($argv[1]) ? $argv[1] : null;
$member_ids_csv = !empty($argv[2]) ? $argv[2] : '';
$member_ids = array_map('trim', explode(',', $member_ids_csv));

$valid_account_types = array('paid', 'free');

if (!in_array($account_type, $valid_account_types)) {
	Logger::log('"' . $account_type . '" is not a valid account type');
	$input_is_valid = false;
}

if (empty($member_ids)) {
	Logger::log('You must provide one or more member ids');
	$input_is_valid = false;
}

if ($input_is_valid) {
	Logger::log('Updating member account types to "' . $account_type . '"');

	$members = Member::find($member_ids);

	$member_count = !empty($members) ? count($members) : 0;

	Logger::log('Found ' . $member_count . ' members');

	$updated_count = 0;
	if (!empty($members)) {
		foreach ($members as $member) {
			$member->account_type = $account_type;
			$member->save();
			$updated_count++;
			Logger::log('Updated member id: ' . $member->id);
		}
	}

	Logger::log('Updated ' . $updated_count . ' members');
}

