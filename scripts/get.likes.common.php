<?php

class PostsFeedbackProcessor {

	protected $curlMultiHandle;
	protected $curlHandles 	= array();
	protected $postQueue 	= array('likes' => array(), 'comments' => array());
	protected $queueSize	= 20;
	protected $urls 		= array('likes' => array(), 'comments' => array());
	protected $processedCount = 0;
	public    $debug 		= false;
	

	function Process( $posts ) {
		$this->processedCount = 0;
		foreach($posts as $post)
		{
			$this->queuePost($post);
		}
		
		// Trigger processing on the last batch even if queue isn't full
		if (count($this->postQueue['likes']) || count($this->postQueue['comments'])) $this->processQueue();

		return $this->processedCount;
	}

	public function queuePost($post) {
		if (!$post->member || !$post->member->network_app_facebook) return false;
		$this->processedCount++;
		$this->queuePostForType($post, 'likes');
		$this->queuePostForType($post, 'comments');
		$this->queuePostForType($post, 'shares');
		if ($this->isQueueFull()) $this->processQueue();		
	}
	
	public function isQueueFull() {
		return ($this->queueSize <= count($this->postQueue['likes']) || $this->queueSize <= count($this->postQueue['comments']));
	}

	public function queuePostForType($post, $type) {
		if (strlen($post->fb_post_id) == 0) return false;
		if ($fb_url = $this->getFacebookUrl($post, $type)) {
			$this->postQueue[$type][$post->id] = $post;
			$this->addCurlUrl($fb_url, $type, $post->id);
		}
	}
	
	public function addCurlUrl($url, $type, $post_id) {
		if (!$this->curlMultiHandle) $this->curlMultiHandle = curl_multi_init();
		$ch = $this->getCurlHandle($url);
		curl_multi_add_handle($this->curlMultiHandle, $ch);
		$this->curlHandles[$type][$post_id] = $ch;
		$this->urls[$type][$post_id] = $url;
	}

	protected function processQueue() { 
		
		if ($this->curlMultiHandle == null) return;
		
		do {
			$execReturnValue = curl_multi_exec($this->curlMultiHandle, $runningHandles);
		} while ($execReturnValue == CURLM_CALL_MULTI_PERFORM);

		while ($runningHandles && $execReturnValue == CURLM_OK) {
 			$ready = curl_multi_select($this->curlMultiHandle);
			if ($ready != -1) {
				// Pull in any new data, or at least handle timeouts
				do {
				  $execReturnValue = curl_multi_exec($this->curlMultiHandle, $runningHandles);
				  usleep(500);
				} while ($execReturnValue == CURLM_CALL_MULTI_PERFORM);
			}
		}

		if ($execReturnValue != CURLM_OK) {
			trigger_error("Curl error $execReturnValue\n", E_USER_WARNING);
		}

		$results = array( 'likes' => array(), 'comments' => array());

		foreach ($this->postQueue as $type => $posts) {
			foreach ($posts as $post_id => $post) {
			
				$handle = $this->curlHandles[$type][$post_id];
				$curl_error = curl_error($handle);
			
				if ($curl_error == "") {
			
					$result 	= curl_multi_getcontent($handle);
					$data 		= json_decode($result, true);

					if( !empty($data['error']['code']) && $data['error']['code'] == 100) {
						// retry request synchronously with raw FB post ID 
						$raw_fb_url = $this->getFacebookUrl($post, $type, true);
						$raw_ch 	= $this->getCurlHandle($raw_fb_url);
						$result 	= curl_exec($raw_ch);
						$data 		= json_decode($result, true);
						curl_close($raw_ch);
						unset($raw_ch);
					}
					
					$property 	= 'facebook_' . $type;
					

					$post->$property = 0;
					if (!isset($data['error'])) {
						switch ($type) {
							case 'likes':
							case 'comments':
								$post->$property = count($data['data']);
							break;
							case 'shares':
								$post->$property = !empty($data['shares']['count']) ? $data['shares']['count'] : 0;
							break;
						}
						$post->save();
						
						if ($post->$property) {
							// Populate PostStatFacebookByDate
							PostStatFacebookByDate::setTotalOnDate(
								$type, 
								date('Y-m-d'),
								$post->$property, 
								$post
							);
						}
						
						if ($type == 'comments' && !empty($data['data'])) {
							FacebookComment::createFromPostCommentsApiResp($post_id, $data['data']);
						}
					}
					
					
										
					$results[$type][$post->id] = $data;
										
				} elseif ($this->debug) {
					print "Curl error on post $post_id: $curl_error\n";
				}

				curl_multi_remove_handle($this->curlMultiHandle, $handle);
				curl_close($handle);
			
			}
		}

		if ( $this->debug ) {
			foreach ($this->postQueue as $type => $posts) {
				foreach ($posts as $post_id => $post) {
					$result = array();
					if (isset($results[$type][$post_id])) $result = $results[$type][$post_id];
					$this->debugPost($post, $result);
				}
			}
		}
				
		$this->clearQueue();
		
		return true;
		
	
	} 
	
	protected function debugPost($post, $result) {
		$post_id   = $this->getFbPostId($post);
		$member_id = $this->getFbMemberId($post);
		echo $this->urls['likes'][$post->id] . PHP_EOL;
		echo $post->fb_post_id . " " . $post->member->first_name . " " . $post->member->last_name . PHP_EOL;	
		echo "LIKES:" . $post->facebook_likes . " -- ";
		echo "COMMENTS:" . $post->facebook_comments . " -- ";
		echo "SHARES:" . $post->facebook_shares;		
		if (isset($result['error'])) echo "  ({$result['error']['message']})";
		echo PHP_EOL;
		echo "http://www.facebook.com/$member_id/posts/$post_id" . PHP_EOL  . PHP_EOL;
	}
	
	public function getCurlHandle($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		return $ch;
	}
	
	public function clearQueue() {
		curl_multi_close($this->curlMultiHandle);
		unset($this->curlMultiHandle);
		unset($this->curlHandles);
		$this->curlMultiHandle = null;
		$this->curlHandles = array();
		$this->postQueue   = array( 'likes' => array(), 'comments' => array() );
		$this->urls		   = array( 'likes' => array(), 'comments' => array() );
	}

	public function getFacebookUrl($post, $type, $raw = false) {
	
       switch ($type) {
            case 'likes':
            case 'comments':
                if ($raw) {
                    $fb_post_id = $post->fb_post_id;
                } else {
                    $fb_post_id = $this->getFbPostId($post);
                }
                if ($fb_post_id) {
                    return "https://graph.facebook.com/$fb_post_id/$type?limit=10000&access_token=" . $post->member->getFacebookUserOrAppAccessToken();
                }
            break;
            case 'shares':
                $fb_post_id = $post->fb_post_id;
                if ($fb_post_id) {
                    return "https://graph.facebook.com/$fb_post_id?access_token=" . $post->member->getFacebookUserOrAppAccessToken();
                }
            break;
        }
        
	}
	
	protected function getFbPostId($post) {
		if (strlen($post->fb_post_id) == 0) return false;
		$parts = explode("_", $post->fb_post_id);
		if (!isset($parts[1])) return false;
		return isset($parts[1]) ? $parts[1] : false;
	}

	protected function getFbMemberId($post) {
		if (strlen($post->fb_post_id) == 0) return false;
		$parts = explode("_", $post->fb_post_id);
		return isset($parts[0]) ? $parts[0] : false;
	}

	public function __destruct() {
		if ($this->curlMultiHandle != null) curl_multi_close($this->curlMultiHandle);
	}

}
