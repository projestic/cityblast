#!/usr/bin/env php
<?php

include realpath(dirname(__FILE__) . '/../public/env.php');
require realpath(dirname(__FILE__) . '/../app/configs/environment.php');

ini_set('display_errors', 1);

Logger::setEcho(true);
Logger::setHtml(false);
Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);

Logger::logTimeMarker();

TextFix::populate();


Logger::log('Finihsed');