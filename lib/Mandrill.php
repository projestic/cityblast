<?php
/**
 * A library for the Mandrill mail interface
 *
 * @author pedrocarmo
 */
class Mandrill
{
	private $apiUrl = "https://mandrillapp.com/api/1.0/";
	
	private $fromName   = COMPANY_NAME;
	private $fromEmail  = HELP_EMAIL;
	private $to         = array();
	private $bcc        = "";

	private $attachments = array();

	private $templateContent = array();
	
	/**
	 * Set the tags for the mandrill service
	 * 
	 * @param mixed $tags
	 */
	public function __construct($tags = array()) 
	{
		if (is_array($tags))
		{
			$this->tags = $tags;
		}
		else
		{
			$this->tags = array($tags);
		}
	}

	/**
	 * Makes a call to the mandrill server
	 * 
	 * @param string $endpoint
	 * @param array $params
	 * @return array
	 */
	private function _makeCall($endpoint, $params)
	{
		// add the key to all requests
		$params['key'] = MANDRILL_API_KEY;
		
		$ch = curl_init($this->apiUrl . $endpoint);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_HEADER, false);

		// value of CURLOPT_POSTFIELDS is an array.
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($params));

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLINFO_HEADER_OUT, true);

		curl_setopt ($ch, CURLOPT_BUFFERSIZE, 4096);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 30);

		$response = curl_exec ($ch);
		curl_close($ch);

		return json_decode($response, true);
	}

	/**
	 * Set the email sender details
	 * 
	 * @param string $name
	 * @param string $email
	 */
	public function setFrom($name, $email) {
		$this->fromName  = $name;
		$this->fromEmail = $email;
	}

	/**
	 * Add an email recipient
	 * 
	 * @param string $name
	 * @param string $email
	 */
	public function addTo($name, $email) {
		$this->to[] = array(
		    'name' => $name,
		    'email' => $email
		);
	}
	
	/**
	 * Set a blind carbon copy recipient
	 * 
	 * @param string $email
	 */
	public function setBcc($email) {
		$this->bcc = $email;
	}
	
	/**
	 * Add attachment
	 * 
	 * @param string $filePath
	 * @param string $fileName
	 */
	public function addAttachment($filePath, $fileName = '') {
		$attachment_encoded = base64_encode(file_get_contents($filePath));

		$mime_type = $this->detectMimeType($filePath);

		$this->attachments[] = array(
			'type' => $mime_type,
			'name' => empty($fileName) ? basename($filePath) : $fileName,
			'content' => $attachment_encoded
		);
	}
	
	protected function detectMimeType($file)
	{
		$result = null;
		if (class_exists('finfo', false)) {
            $const = defined('FILEINFO_MIME_TYPE') ? FILEINFO_MIME_TYPE : FILEINFO_MIME;
            $mime = @finfo_open($const);

            if (!empty($mime)) {
                $result = finfo_file($mime, $file);
            }

            unset($mime);
        }

        if (empty($result) && (function_exists('mime_content_type')
            && ini_get('mime_magic.magicfile'))) {
            $result = mime_content_type($file);
        }

        if (empty($result)) {
            $result = 'application/octet-stream';
        }
        
        return $result;
	}

	/**
	 * Add content to editable areas of the tempalte
	 * 
	 * @param string $name
	 * @param string $content
	 */
	public function addTemplateContent($name, $content) {
		$this->templateContent[] = array(
		    'name' => $name,
		    'content' => $content
		);
	}
	
	/**
	 * Send a regular html email
	 * 
	 * @param string $subject
	 * @param string $html
	 * @param string $plain
	 * @return mixed
	 */
	public function send($subject, $html, $plain = "")
	{
		$message = array();
		$message["html"]                = $html;
		$message["text"]                = $plain;
		$message["subject"]             = $subject;
		$message["from_email"]          = $this->fromEmail;
		$message["from_name"]           = $this->fromName;
		$message["to"]                  = $this->to;
		$message["auto_text"]           = true;
		$message["preserve_recipients"] = false;
		$message["bcc_address"]         = $this->bcc;
		// add the tags (if any) 
		$message['tags'] = $this->tags;
		$message['attachments'] = $this->attachments;

		$params = array();
		$params["message"] = $message;
		$params["async"] = true;

		$params = $this->_debugSanitizeParams($params);
		 
		return $this->_makeCall("/messages/send.json", $params);
	}
	
	/**
	 * Send an email using a template set in the mandrill website
	 * 
	 * @param string $subject
	 * @param string $templateName
	 * @param string $plain
	 * @return mixed
	 */
	public function sendTemplate($subject, $templateName, $plain = null)
	{
		$message = array();
		$message["text"]                = $plain;
		$message["subject"]             = $subject;
		$message["from_email"]          = $this->fromEmail;
		$message["from_name"]           = $this->fromName;
		$message["to"]                  = $this->to;
		$message["auto_text"]           = true;
		$message["preserve_recipients"] = false;
		$message["bcc_address"]         = $this->bcc;
		// add the tags (if any)
		$message['tags'] = $this->tags;

		$params = array();
		$params["template_name"] = $templateName;
		$params["template_content"] = $this->templateContent;
		$params["message"] = $message;
		$params["async"] = true;

		$params = $this->_debugSanitizeParams($params);
		 
		return $this->_makeCall("/messages/send-template.json", $params);
	}
	
	private function _debugSanitizeParams($params) {
		if (defined('MANDRILL_DEBUG_MODE') && MANDRILL_DEBUG_MODE) {
			if (!defined('MANDRILL_DEBUG_DESTINATION')) throw new Exception('MANDRILL_DEBUG_MODE set but MANDRILL_DEBUG_DESTINATION constant not set. Please set MANDRILL_DEBUG_DESTINATION to a valid email address.');
			foreach ( $params['message']["to"] as $i => $to ) {
				$to['email'] = MANDRILL_DEBUG_DESTINATION;
				$params['message']["to"][$i] = $to;
			}
		}
		return $params;
	}
	
}
