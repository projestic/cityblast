<?php

class Blastit_Affiliate_RouterPlugin extends Zend_Controller_Plugin_Abstract
{

	public function routeStartup(Zend_Controller_Request_Abstract $request) {
	
		$dispatcher = Zend_Controller_Front::getInstance()->getDispatcher();
		$request    = Zend_Controller_Front::getInstance()->getRequest();
		if ($dispatcher->isDispatchable($request)) return;
		
		$router 	= Zend_Controller_Front::getInstance()->getRouter();
		
		$cache = Zend_Registry::get('cache');
		
		if ( false === ($affiliates = $cache->load('affiliate_slugs')) ) {
			$affiliates = Affiliate::all();
			$cache->save( $affiliates, 'affiliate_slugs' );	
		}
				
		foreach ($affiliates as $affiliate) {
			if (strlen($affiliate->slug)) {
				$router->addRoute("affiliate_landing_page_" . $affiliate->id,
					new Zend_Controller_Router_Route($affiliate->slug,
						array("controller" => "affiliate", "action" => "landingpage", 'slug' => $affiliate->slug)
					)
				);
			}
		}

	}
	
}
