<?php

class Blastit_Affiliate_SlugValidator 
{

	protected $prohibited_slugs = array(
			'admin',
			'user',
			'login',
			'logout',
			'signin',
			'join',
			'manage',
			'signup',
			'cityblast',
			'city-blast',
			'free'
	);

	protected $message = '';

	public function validate($slug, $member = null) {
	
		if (strlen($slug) == 0) {
			$this->setMessage('Empty URL is not permitted.');
			return false;
		}
		
		if (in_array($slug, $this->prohibited_slugs)) {
			$this->setMessage('Sorry, that URL is not available.');
			return false;
		}
		
		$conditions = '';
		
		if ($member instanceOf Member && $member->affiliate_account) {
			$affiliate_conditions = ' AND id != ' . $member->affiliate_account->id;
			$promo_conditions = ' AND affiliate_id != ' . $member->affiliate_account->id;
		}
		
		if (Affiliate::count( array('conditions' => array('slug = ? ' . $affiliate_conditions, $slug) )) || Promo::count( array('conditions' => array('promo_code = ? ' . $promo_conditions, $slug) ))) {
			$this->setMessage('Sorry, that URL is already taken.');
			return false;
		}
		
		$slug  = preg_replace('/[^A-Za-z0-9\-]/', '', $slug);
		
		$dispatcher = Zend_Controller_Front::getInstance()->getDispatcher();
		$request    = new Zend_Controller_Request_Http();
		$request->setControllerName($slug);
		$request->setActionName('index');

		if ($dispatcher->isDispatchable($request)) {
			$this->setMessage('Sorry, that URL is not available.');
			return false;
		}
		
		$this->setMessage('Available!');
		
		return true;
		
	}
	
	protected function setMessage($message) {
		$this->message = $message;
	}

	public function getMessage() {
		return $this->message;
	}
	
}
