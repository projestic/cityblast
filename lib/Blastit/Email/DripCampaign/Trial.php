<?php

class Blastit_Email_DripCampaign_Trial extends Blastit_Email_DripCampaign_Abstract
{

	protected $intervals   			= array('PT1H', 'P1D', 'P2D', 'P3D', 'P4D', 'P5D', 'P6D');

	protected $time_of_day   		= array(null, 
										array('hour' => 14, 'minute' => 0), 
										array('hour' => 14, 'minute' => 0),
										array('hour' => 14, 'minute' => 0),
										array('hour' => 14, 'minute' => 0),
										array('hour' => 14, 'minute' => 0),
										array('hour' => 14, 'minute' => 0),
									);

	protected $age_value   			= 'created_at';
	protected $email_type  			= 'DRIP_TRIAL';
	protected $email_template_path  	= 'email/drip/trial';
	
	public function __construct ($member = null)
	{

	 	$this->subjects   			= array(
									'We\'ve temporarily started your trial - but still need your missing info!',
									'Reach Thousands Of NEW Potential Buyers In Your Area with  '.COMPANY_NAME,
									'STOP What You\'re Doing. A Lack of Consistency Is DESTROYING Your Real Estate Business.',
									'There\'s A Reason We\'re The BIGGEST In The World. The Most Content Choices. Guaranteed.',
									'Answer 3 Short Questions: Extend your FREE Trial!', 											
									'The Social Media Revolution Has Happened.  Are You Getting Left Behind?', 																				
									'Oh Dear - Your Trial Is Over.  Get Another 7 Days FREE!'
							   );		

		parent::__construct($member = null);
	}

	static public function getMemberConditions() 
	{
		return array("member.payment_status = 'signup' AND (stripe_customer_id IS NULL OR stripe_customer_id = '') AND member.created_at > NOW() - INTERVAL 9 DAY");

	}
	
}
