<?php

class Blastit_Email_DripCampaign_TrialMyEventVoice extends Blastit_Email_DripCampaign_Abstract
{
	protected $intervals			= array('PT1H', 'P1D', 'P2D', 'P3D', 'P6D');

	protected $time_of_day			= array(null,
										array('hour' => 14, 'minute' => 0),
										array('hour' => 14, 'minute' => 0),
										array('hour' => 14, 'minute' => 0),
										array('hour' => 14, 'minute' => 0),
									);

	protected $age_value			= 'created_at';
	protected $email_type			= 'DRIP_TRIAL';
	protected $email_template_path	= 'email/drip/trial';

	public function __construct ($member = null)
	{
		$this->subjects = array(
			'We\'ve Started Your Trial - But You Didn\'t Lock-In the Promotion!',
			'STOP What You\'re Doing. A Lack of Consistency Is DESTROYING Your Business.',
			'There\'s A Reason We\'re The BIGGEST. The Most Customized Content - Guaranteed.',
			'Answer 3 Short Questions: Extend your FREE Trial!',
			'Your Trial Is Over.  But, Have Another 7 Days On Us!',
		);

		parent::__construct($member = null);
	}

	static public function getMemberConditions()
	{
		return array("member.payment_status = 'signup' AND (stripe_customer_id IS NULL OR stripe_customer_id = '') AND member.created_at > NOW() - INTERVAL 9 DAY");
	}
}
