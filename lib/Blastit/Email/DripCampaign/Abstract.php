<?php

abstract class Blastit_Email_DripCampaign_Abstract
{

	protected $intervals;
	protected $subject;
	protected $drip;
	protected $member;
	
	public function __construct($member = null) {
		if ($member) $this->setMember($member);
	}
	
	public function reset() {
		$this->drip = null;
	}
	
	public function setMember($member) {
		$this->reset();
		$this->member = $member;
	}
	
	public function getDripNumber() {
	
		if ($this->drip) return $this->drip;
	
		$age_value = $this->age_value;
		$since     = $this->member->$age_value;
		$now	   = new DateTime();
		$drip	   = false;
		
		for ($i = count($this->intervals)-1; $i >= 0; $i--) {
			
			$int = new DateInterval($this->intervals[$i]);
			
			if (isset($this->time_of_day[$i])) {
				$date = new DateTime($since->format('Y-m-d'));
				$date->setTime( $this->time_of_day[$i]['hour'], $this->time_of_day[$i]['minute'] );
			} elseif ($int->format('%H') == 0 && $int->format('%i') == 0 && $int->format('%s') == 0) {
				// if interval does not include sub-day granularity, check date starting from 12am
				$date = new DateTime($since->format('Y-m-d'));
			} else {
				$date = new DateTime($since->format('Y-m-d H:i:s'));
			}
			
			$date->add($int);

			if ($date <= $now) {
				$drip = $i;
				break;
			}
			
		}
		
		$this->drip = $drip;
		
		return $drip;

	}
	
	protected function getDripEmailsSent() {
		return EmailLog::count( array('conditions' => array('member_id' => $this->member->id, 'type' => $this->email_type)) );
	}
	
	protected function getDripEmailTemplate($drip) {
		$drip++;
		return "{$this->email_template_path}/{$drip}.html.php";
	}

	protected function getDripEmailSubject($drip) {
		if (!isset($this->subjects[$drip])) throw new Zend_Exception("Email $drip has no subject defined");
		return $this->subjects[$drip];
	}

	public function drip() {
	
		if (!$this->member) throw new Zend_Exception('Member must be set first');
		
		$drip = $this->getDripNumber();
		
		if (false === ($drip = $this->getDripNumber())) {
			return false;
		}
		
		if ($drip != ($this->getDripEmailsSent()) ) {

			// we've already sent all the emails that should have been sent to date, OR we're on the wrong drip number, so don't send
			return false;
		}
		
		$subject  = $this->getDripEmailSubject($drip);
		$template = $this->getDripEmailTemplate($drip);
		
		// Use unique tag for Mandrill, but not internally
		
		$email = new Blastit_Email_MandrillLogger($this->member, $this->email_type, $this->email_type . '_' . $drip);
		$email->setSubject( $subject );
		$email->setHtmlTemplate( $template );
		return $email->send();
		
	}	

}
