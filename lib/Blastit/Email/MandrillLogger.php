<?php

class Blastit_Email_MandrillLogger
{

	protected $type;
	protected $subject;
	protected $html;
	protected $text;
	protected $to = array();
	protected $member;
	protected $mandrill;
	protected $params = array();

	public function  __construct($member, $type, $mandrill_type = null) {
		$this->type	  	= $type;
		$this->member 	= $member;
		$this->mandrill = new Mandrill(array($mandrill_type ? $mandrill_type : $this->type));
		$fromEmail = HELP_EMAIL;
		$fromName  = COMPANY_NAME;
		$this->setFrom($fromName, $fromEmail);
		$this->addTo($member->name(), (isset($member->email_override) && $member->email_override) ? $member->email_override : $member->email);
		$this->setTemplateParam('member', $member);
	}

	public function setSubject($subject) {
		$this->subject = $subject;
	}

	public function setHtml($html) {
		$this->html = $html;
	}
	
	public function setHtmlTemplate($tpl) {
		$this->template = $tpl;	
	}

	public function setTemplateParam($name, $value) {
		$this->params[$name] = $value;
	}

	public function setTemplateParams($params) {
		$this->params = $params;
		$this->setTemplateParam('member', $this->member);
	}

	public function setText($text) {
		$this->text = $text;
	}

	public function setFrom($name, $email) {
		$this->mandrill->setFrom($name, $email);
	}
	
	public function addTo($name, $email) {
		$this->to[] = array($name, $email);
		$this->mandrill->addTo($name, $email);
	}
	
	public function setBcc($name, $email) {
		$this->mandrill->setBcc($name, $email);
	}
	
	public function send() {
		
		if ($this->template) {
			$view = Zend_Registry::get('view');
			$this->setHtml( $view->partial($this->template, $this->params) );
		}
		
		if ($this->mandrill->send($this->subject, $this->html, $this->text)) {
			$this->log();
			return true;
		}
	
		return false;
	
	}
	
	private function log()
	{
	
		$emails = array();
	
		foreach ($this->to as $to) {
			$emails[] = $to[1];
		}
	
		$log			=   new EmailLog();
		$log->type		=   $this->type;
		$log->member_id	=   $this->member->id;
		$log->email		=   implode(",", $emails);
		$log->subject	=   $this->subject;
		$log->save();
		
	}

}
