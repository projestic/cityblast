<?php
/**
 * Produces paths to the storage directory.
 * @author konstv
 * @date: 2013nov11
 */
class Blastit_BlogExternal_BlogProcessor_StorageDirectory {
	/**
	 * A directory name for entity, stored under public images directory.
	 * Like:
	 * /images/listings/
	 * /images/blog/
	 *
	 * Example: 'listings'
	 * @var string
	 */
	protected $entityNameDirectory;

	public function __construct($entityNameDirectory) {
		$this->entityNameDirectory = $entityNameDirectory;
	}

	/**
	 * Prepares destination directory path, where to save file to.
	 *
	 * @param string $entityId Example: blog ID
	 * @param string $originalFilename Example: flower.jpg
	 * @return array
	 * full: /website/images/blog/2013/11/flower
	 * shot: /images/blog/2013/11/flower
	 */
	public function prepareDestinationDirectoryPathFromOrigFilename($entityId, $originalFilename) {
		$pathParts = pathinfo($originalFilename);
		$filenameOrigWithoutExtension = $pathParts['filename'];

		$destDirPathParts = $this->prepareDestinationDirectoryPath($entityId, $filenameOrigWithoutExtension);

		$destDirPathFull = $destDirPathParts['dirPathFull'];
		$destDirPathShort = $destDirPathParts['dirPathShort'];

		$filePathFull = $destDirPathFull . '/' . $originalFilename;
		$filePathShort = $destDirPathShort . '/' . $originalFilename;

		$result = $destDirPathParts;
		$result['filePathFull'] = $filePathFull;
		$result['filePathShort'] = $filePathShort;
		return $result;
	}

	/**
	 * Prepares destination directory path, where to save file to.
	 *
	 * @param string $entityId Example: blog ID
	 * @param string $originalFilenameWithoutExtension Example: flower
	 * @return array
	 * full: /website/images/blog/2013/11/flower
	 * shot: /images/blog/2013/11/flower
	 */
	public function prepareDestinationDirectoryPath($entityId, $originalFilenameWithoutExtension) {
		$entityNameDirectory = $this->entityNameDirectory;

		$pathBase = '';
		if (CDN_ORIGIN_STORAGE) {
			$pathBase = CDN_ORIGIN_STORAGE;
		}

		$pathFull = $pathBase;

		$pathShort = '';
		$pathShort .= '/';
		$pathShort .= 'images';
		$pathShort .= '/';
		$pathShort .= $entityNameDirectory;
		$pathShort .= '/';
		$pathShort .= date('Y') . "/" . date('m');
		$pathShort .= '/';
		$pathShort .= $entityId;

		$pathFull .= $pathShort;

		if (!file_exists($pathFull)) {
			mkdir($pathFull, 0755, true);
		}

		return array(
			'dirPathFull' => $pathFull,
			'dirPathShort' => $pathShort
		);
	}
}