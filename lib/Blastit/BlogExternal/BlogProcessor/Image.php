<?php
/**
 * Image processor
 */
class Blastit_BlogExternal_BlogProcessor_Image {
	/**
	 * Processes image XML/HTML node.
	 *
	 * @param int $blogEntryId Blog entry ID
	 * @param DOMElement $node HTML [img] node
	 * @return string|null Saved Image Path
	 */
	public function processImageNode($blogEntryId, $node)
	{
		Logger::log('Processing image node...', Logger::LOG_TYPE_DEBUG);

		$imageSrc = $this->extractSrcFromNode($node);

		$filesize = $this->remote_filesize($imageSrc);

		Logger::log('URL: ' . $imageSrc, Logger::LOG_TYPE_DEBUG);
		Logger::log('filesize: ' . $filesize . '(' . number_format($filesize/1024/1024, 2) . 'MB)', Logger::LOG_TYPE_DEBUG);

		if(!$this->isSizeParsable($filesize))
		{
			return null;
		}
		$saveResult = $this->downloadAndSave($blogEntryId, $imageSrc);
		if (!$saveResult)
		{
			return null;
		}
		$filepathDest = $saveResult['filePath'];
		$isSuccess = $saveResult['isSuccess'];
		if ($isSuccess && $filepathDest !== null)
		{
			return $filepathDest;
		}
		return null;
	}

	/**
	 * @param int $filesize
	 * @return bool True - suitable
	 */
	protected function isSizeSuitable($filesize)
	{
		//If the file exists and is less than 10M go ahead and save it locally
		if($filesize < 2048 || $filesize > 10485760) {
			Logger::log("File too big or too small: " . $filesize, Logger::LOG_TYPE_DEBUG);
			return false;
		}
		return true;
	}

	/**
	 * @param int $filesize
	 * @return bool True - ok, suitable
	 */
	protected function isSizeParsable($filesize) {
		if($filesize == "unknown") {
			return false;
		}
		if(!is_numeric($filesize)) {
			Logger::log("The file size was not numeric: " . $filesize, Logger::LOG_TYPE_DEBUG);
			return false;
		}
		return true;
	}

	/**
	 * @param DOMElement $node
	 * @return string
	 */
	protected function extractSrcFromNode($node) {
		return $node->getAttribute('src');
	}

	/**
	 * @param string $url
	 * @return int
	 */
	protected function remote_filesize($url)
	{
		$size = Blastit_BlogExternal_BlogProcessor_CUrl::remote_filesize($url);
		return trim($size);
	}

	protected function downloadAndSave($blogEntryId, $imageSrc) {
		Logger::log('We\'ve found a file to save...', Logger::LOG_TYPE_DEBUG);

		$downloadedArr = $this->fileDownloadLow($imageSrc);
		$fileRaw = $downloadedArr['data'];
		$error = $downloadedArr['error'];
		if ($fileRaw === null) {
			Logger::log('Error in download: ' . $error, Logger::LOG_TYPE_DEBUG);
			return null;
		}

		$filesizeStrlen = strlen($fileRaw);
		Logger::log('strlen of the raw file: ' . $filesizeStrlen, Logger::LOG_TYPE_DEBUG);
		if (!$filesizeStrlen) {
			return null;
		}
		if(!$this->isSizeSuitable($filesizeStrlen)) {
			return null;
		}
		return $this->fileSave($blogEntryId, $imageSrc, $fileRaw);
	}

	/**
	 * @param string $url
	 * @return array
	 */
	protected function fileDownloadLow($url)
	{
		return Blastit_BlogExternal_BlogProcessor_CUrl::downloadFile($url);
	}

	protected function fileSave($blogId, $filePathOrig, $fileRaw)
	{
		$filenameOrig = sha1(basename($filePathOrig));
		$targetDirectoryMan = new Blastit_BlogExternal_BlogProcessor_StorageDirectory('externalcontent');
		$destPathParts = $targetDirectoryMan->prepareDestinationDirectoryPathFromOrigFilename($blogId, $filenameOrig);

		$filePathShort = $destPathParts['filePathShort'];
		$filePathFull = $destPathParts['filePathFull'];
		Logger::log('[fileSaveLow] filepathDest: ' . $filePathFull, Logger::LOG_TYPE_DEBUG);

		$isSuccess = $this->fileSaveLow($filePathFull, $fileRaw);
		return array(
			'filePath' => $filePathShort,
			'isSuccess' => $isSuccess
		);
	}

	protected function fileSaveLow($filepathDest, $fileRaw) {
		//Open the file location
		$fhandle = fopen($filepathDest, "w");

		//Write the raw track to the location
		fwrite($fhandle, $fileRaw);

		//Close the file handle
		fclose($fhandle);

		//Test to see if the file exists (i.e. the write was successful)
		return file_exists($filepathDest);
	}
}
