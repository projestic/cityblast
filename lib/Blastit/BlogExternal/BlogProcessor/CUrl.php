<?php
/**
 * Manipulate remote files.
 */
class Blastit_BlogExternal_BlogProcessor_CUrl
{
	/**
	 * @param string $url
	 * @return array
	 */
	public static function downloadFile($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($ch);

		$resArr = array();
		$resArr['error'] = null;
		$resArr['data'] = $result;
		if(curl_errno($ch)) {
			$resArr['data'] = null;
			$resArr['error'] = curl_error($ch);
		}
		curl_close($ch);
		return $resArr;
	}

	/**
	 * Taken from Curl.php in audio_academy project.
	 *
	 * See also another method: http://stackoverflow.com/questions/2602612/php-remote-file-size-without-downloading-file
	 * @param string $url
	 * @return string
	 */
	public static function remote_filesize($url){
		ob_start();

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_NOBODY, 1);

		if(!empty($user) && !empty($pw)){
			$headers = array('Authorization: Basic ' .  base64_encode("$user:$pw"));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}

		$ok = curl_exec($ch);
		curl_close($ch);

		$head = ob_get_contents();
		ob_end_clean();

		$regex = '/Content-Length:\s([0-9].+?)\s/';
		$count = preg_match($regex, $head, $matches);

		return isset($matches[1]) ? $matches[1] : "unknown";
	}
}