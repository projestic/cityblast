<?php
/**
 * Copies blog into listing.
 * @author konstv
 * @date: 2013niv11
 */
class Blastit_BlogExternal_CopyToListing_BlogCopyToListing {
	/**
	 * List of errors happened during the procedure.
	 * @var string[]
	 */
	protected $errors = array();

	/**
	 * Returns errors happened during the procedure.
	 * @return string[]
	 */
	public function getErrors() {
		return $this->errors;
	}

	/**
	 * Copies the blog entry into a listing record
	 * @param BlogPending $blogEntry Source blog entry
	 * @param string $blastType
	 * @param null $imageIds
	 * @param $member_id
	 * @return Listing Produced listing record
	 */
	public function copy($blogEntry, $blastType = 'CONTENT', $imageIds = null, $member_id)
	{
		$listing = new Listing();
		$listing->created_at = $blogEntry->created_at;
		$listing->title = $blogEntry->title;
		$listing->description = $blogEntry->title;
		$message = strip_tags($blogEntry->description);
		if (mb_strlen($message) > 400) {
			$message = mb_substr($message, 0, 400);
			$message .= ' ...';
		}
		$listing->message = $message;
		$listing->content_link = $blogEntry->content_link;
		$listing->blast_type = $blastType;
		$listing->member_id = $member_id;

		$listing->save();
		
		$this->copyImages($blogEntry, $listing, $imageIds);

		return $listing;
	}

	protected function copyImages($blogEntry, $listing, $imageIds = null) 
	{
	
		$i = 0;
	
		foreach ($blogEntry->images as $blogImage) 
		{
			
			if (is_array($imageIds) && !in_array($blogImage->id, $imageIds)) {
				$blogImage->delete();
				continue;
			}
			
			if ($i == 0) {
				$listing->ingestImage(CDN_ORIGIN_STORAGE . $blogImage->image, false);
				$listing->ingestThumbnail(CDN_ORIGIN_STORAGE . $blogImage->image);
			} else {
				$listingImage	= new ListingImage();
				$listingImage->listing_id = $listing->id;
				$listingImage->ingestImage(CDN_ORIGIN_STORAGE . $blogImage->image);
				$listingImage->save();
			}

			$blogImage->delete();
			$i++;
		}
	}

	/**
	 * @param string $blogImagePathShort
	 * @param int $listingId
	 * @return string Destination file path short
	 * @throws Exception
	 */
/*	protected function moveImageFile($blogImagePathShort, $listingId)
	{
		$errors = array();
		$resArr = array();
		$resArr['errors'] = &$errors;
		$resArr['destFilePathShort'] = null;


		// @temporary 2013nov15. A temporary check for legacy file locations. Remove this check at 2013dec01.
		if (strpos($blogImagePathShort, 'blogexternal')) {
			$errors[] = 'Legacy file location detected: ' . $blogImagePathShort;
			return $resArr;
		}
		// <- temporary

		$blogImagePathShort = ltrim($blogImagePathShort, '/'); // strip first slash
		$srcImagePath = CDN_ORIGIN_STORAGE . '/' . $blogImagePathShort;
		$srcFileName = basename($srcImagePath);
		$destDirectoryMan = new Blastit_BlogExternal_BlogProcessor_StorageDirectory('listings');
		$destPathParts = $destDirectoryMan->prepareDestinationDirectoryPathFromOrigFilename($listingId, $srcFileName);

		$destFilePathShort = $destPathParts['filePathShort'];
		$destFilePathFull = $destPathParts['filePathFull'];

		if (!file_exists($srcImagePath)) {
			$errors[] = 'Source file doesnt exist: ' . $blogImagePathShort;
			return $resArr;
		}

		$res = rename($srcImagePath, $destFilePathFull);
		if (!$res) {
			throw new Exception('[BlogCopyToListing.moveImage] Couldnt move file from ' . $srcImagePath . ' to ' .$destFilePathFull );
		}

		$resArr['destFilePathShort'] = $destFilePathShort;
		return $resArr;
	} */
}