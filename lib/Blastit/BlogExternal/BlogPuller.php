<?php
/**
 * Pulls the blogs feeds.
 * @author konstv
 * @date: 2013nov09
 */
class Blastit_BlogExternal_BlogPuller 
{
	protected $currDir;

	/**
	 * Imports feeds from the blogs URLs listed in database.
	 * Populates db with the feed contents.
	 * @param string $currDir
	 */
	public function importFeeds($currDir) 
	{
		$this->currDir = $currDir;
		$blogs = Blog::all();
		foreach($blogs as $blog) 
		{
			try {
				$this->pullFromBlog($blog->id, (bool)$blog->seek_images_in_external_article, $blog->url);
			} catch (Exception $e) {
				Logger::log('Blog exception ' . get_class($e) . ': ' . $e->getMessage(), Logger::LOG_TYPE_DEBUG, true);
			}
		}
	}

	/**
	 * Pulls content from the feed.
	 * @param int $blogId Blog ID
	 * @param bool $seek_images_in_external_article
	 * @param string $url Blog Feed URL
	 */
	protected function pullFromBlog($blogId, $seek_images_in_external_article, $url)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		Logger::log('$blogId ' . $blogId, Logger::LOG_TYPE_DEBUG, true);

		$feed = new SimplePie();
		$feed->set_cache_location(APPLICATION_PATH . "/../cache/simplepie");
		
		//$feed->set_raw_data($this->tempGetContents());
		$feed->set_feed_url($url);

		$feed->force_feed(true);

		$feed->init();

		if ($feed->error()) {
			Logger::log('$feed->error(): ' . $feed->error(), Logger::LOG_TYPE_DEBUG, true);
		}

		$blogEntryProcessor =  new Blastit_BlogExternal_BlogProcessor();
		foreach($feed->get_items() as $item) 
		{
			try {
				$blogEntryProcessor->processItem($blogId, $seek_images_in_external_article, $item);
			} catch (Exception $e) {
				Logger::log('Blog item exception ' . get_class($e) . ': ' . $e->getMessage(), Logger::LOG_TYPE_DEBUG, true);
			}
		}
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}

	protected function tempGetContents() 
	{
		$dir = $this->currDir . '/..';
		//$res = file_get_contents($dir . '/' . 'blog.pull.input_01_bweek.txt');
		$res = file_get_contents($dir . '/' . 'blog.pull.input_02_engaget.txt ');
		return $res;
	}
}