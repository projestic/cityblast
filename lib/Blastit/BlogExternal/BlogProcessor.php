<?php
/**
 * Pulls the blogs feeds.
 * @author konstv
 * @date: 2013nov09
 */
class Blastit_BlogExternal_BlogProcessor
{
	/**
	 * @param int $blogId Blog ID
	 * @param bool $seek_images_in_external_article
	 * @param SimplePie_Item $item
	 */
	public function processItem($blogId, $seek_images_in_external_article, $item)
	{
		/**
		 * @var Zend_Config $config
		 */
		$config = Zend_Registry::get('config');
		$max_age = $config->externalblog->max_age;
		$min_blog_date = date('Ymd', strtotime($max_age . ' days ago'));
		$article_date = $item->get_date('Ymd');
		if ($article_date >= $min_blog_date) {
			$this->convertRssItemToBlogEntry($blogId, $seek_images_in_external_article, $item);
		}
	}

	/**
	 * @param int $blogId Blog ID
	 * @param bool $seek_images_in_external_article
	 * @param SimplePie_Item $item
	 * @return BlogPending
	 */
	protected function convertRssItemToBlogEntry($blogId, $seek_images_in_external_article, $item)
	{
		$dateformat = 'Y-m-d H:i:s';

		// such record already exists
		if (BlogImported::previouslyImported($blogId, $item)) {
			return;
		}
		
		try {
			// Check that URL is accessible
			// will through an exception on failure
			file_get_contents($item->get_permalink(), null, null, 0, 1);
		} catch (Exception $e) {
			// 404 - article not found
			return;
		}

		$b = new BlogPending();
		$b->blog_id = $blogId;
		$b->content_link = $item->get_permalink();
		$b->message = '';
		$b->description = $item->get_description();
		$b->title = $item->get_title();
		$b->created_at = date($dateformat);
		$b->article_date = $item->get_date($dateformat);
		//$item->get_author(); $item->get_category(); $item->get_content()

		$b->save();
		$blogEntryId = $b->id;
		
		BlogImported::registerImport($b);

		// blog images
		$contentSignificant = $this->getSignificantContent($item, $seek_images_in_external_article);
		$imagesPaths = $this->findImagesInsideContent($contentSignificant, $blogEntryId);

		foreach($imagesPaths as $imagePath) 
		{
			$image_sizes = getimagesize(CDN_ORIGIN_STORAGE . $imagePath);
			
			$width = !empty($image_sizes[0]) ? $image_sizes[0] : 0;
			$height = !empty($image_sizes[1]) ? $image_sizes[1] : 0;
			 
			$image = new BlogImagePending;
			$image->blog_entry_id = $blogEntryId;
			$image->image = $imagePath;
			$image->width = $width;
			$image->height = $height;
			$image->save();
		}

	}

	/**
	 * @param SimplePie_Item $item
	 * @param bool $seek_images_in_external_article
	 * @return string
	 */
	protected function getSignificantContent($item, $seek_images_in_external_article) {
		$contentOrig = $item->get_content();
		if (!$seek_images_in_external_article) {
			return $contentOrig;
		}
		$res = null;
		try {
			$res = file_get_contents($item->get_permalink());
		} catch (Exception $e) {
			// 404
		}
		return $res;
	}

	/**
	 * Looks for [img] tags inside HTML.
	 * @param string $content
	 * @param int $blogEntryId
	 * @return string[] Image Paths
	 */
	protected function findImagesInsideContent($content, $blogEntryId) 
	{
		$dom = new DOMDocument("1.0", "ISO-8859-15");
		try {
			$content = mb_convert_encoding($content, 'HTML-ENTITIES', "auto");
		} catch (Exception $e) {
			// Failed converting encoding - Unable to detect character encoding?
			Logger::log('No images found', Logger::LOG_TYPE_DEBUG);
			return array();
		}
		@$dom->loadHTML($content);

		$nodes = $dom->getElementsByTagName('img');

		if (!is_object($nodes)) 
		{
			Logger::log('No images found', Logger::LOG_TYPE_DEBUG);
			return array();
		}

		$resImagePathes = array();
		$imageNodeProcessor = new Blastit_BlogExternal_BlogProcessor_Image();
		foreach($nodes as $node) 
		{
			$savedImagedPath = $imageNodeProcessor->processImageNode($blogEntryId, $node);
			if ($savedImagedPath) {
				$resImagePathes[] = $savedImagedPath;
			}
		}

		return $resImagePathes;
	}
}

