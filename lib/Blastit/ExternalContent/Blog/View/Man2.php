<?php

class Blastit_ExternalContent_Blog_View_Man2
{
	public static function oddEvenClass(&$rowParityType) {
		if($rowParityType == true)
		{
			$rowParityType = false;
			return "class='odd' ";
		}
		else
		{
			$rowParityType = true;
			return "class='' ";
		}
	}

	public static function link($urlEditBase, $itemId, $title) {
		return '<a href="' . $urlEditBase . $itemId . '"' . '>' . $title . '</a>';
	}
}
