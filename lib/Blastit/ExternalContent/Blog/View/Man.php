<?php

class Blastit_ExternalContent_Blog_View_Man
{
	/**
	 * @param Blog $blogItem
	 * @return string
	 */
	public function row($blogItem) 
	{
		$res = '';
		$res .= $this->id($blogItem->id) . $this->minorDelimiter();
		$res .= $this->name($blogItem) . $this->minorDelimiter();
		$res .= $this->url($blogItem) . $this->minorDelimiter();
		$res .= $this->tier($blogItem) . $this->minorDelimiter();
		$res .= $this->created_at($blogItem) . $this->minorDelimiter();
		$res .= $this->actions($blogItem) . $this->minorDelimiter();
		return $res;
	}

	protected function id($id)
	{
		return "<td style='vertical-align: top;'><a href='/externalcontent/blogedit/" . $id . "' style='text-decoration: none;'>" . $id . "</a></td>";
	}

	/**
	 * @param Blog $blogItem
	 * @return string
	 */
	protected function name($blogItem)
	{
		return "<td style='vertical-align: top;'>" . $blogItem->name . "</td>";
	}

	/**
	 * @param Blog $blogItem
	 * @return string
	 */
	protected function url($blogItem)
	{
		return "<td style='vertical-align: top;'>" . $blogItem->url . "</td>";
	}

	/**
	 * @param Blog $blogItem
	 * @return string
	 */
	protected function tier($blogItem)
	{
		return "<td style='vertical-align: top;'>" . $blogItem->tier . "</td>";
	}

	/**
	 * @param Blog $blogItem
	 * @return string|null
	 */
	protected function created_at($blogItem) 
	{
		$createdAtFormatted = null;
		if(isset($blogItem->created_at) && !empty($blogItem->created_at)) {
			$createdAtFormatted = $blogItem->created_at->format('M-d-Y H:i');
		}
		return "<td style='vertical-align: top; color: #a2a2a2'>" . $createdAtFormatted . "</td>";
	}

	protected function actions($blogItem)
	{
		$res = '';
		$res .= "<td style='text-align: right;' nowrap>";
		$res .= "<a href='/externalcontent/blogedit/" . $blogItem->id . "'>edit</a>";
		$res .= ' | ';

		$deleteText = 'Are you sure to delete record ' . $blogItem->name . '?';
		$res .= '<a href="/externalcontent/blogdelete?id=' . $blogItem->id . '" onclick="return confirm(\'' . $deleteText . '\');">delete</a></td>';
		$res .= "</td>";
		return $res;
	}

	protected function minorDelimiter() 
	{
		return "\n";
	}
}

?>