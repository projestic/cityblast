<?php

class Blastit_ExternalContent_Blog_Edit_Man
{
	public static function inputText($title, $elName, $value, $width=500) {
?>
		<li class="fieldname"><?= $title ?> <span style="font-weight: bold; color: #990000">*</span></li>
		<li class="field" style="padding-top: 20px;">
			<input type="text" name="<?= $elName ?>" id="<?= $elName ?>" value="<?= $value ?>" class="uiInput" style="width: <?= $width ?>px" />
		</li>
<?php
	}
}