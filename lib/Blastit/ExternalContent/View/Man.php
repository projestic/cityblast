<?php

class Blastit_ExternalContent_View_Man 
{
	protected $sortField;

	public function __construct($sortField)	{
		$this->sortField = $sortField;
	}

	/**
	 * @param BlogPending $blogEntry
	 * @return string
	 */
	public function row($blogEntry) 
	{
		$res = '';
		$res .= $this->id($blogEntry->id) . $this->minorDelimiter();
		$res .= $this->article_date($blogEntry) . $this->minorDelimiter();
		$res .= $this->tier($blogEntry) . $this->minorDelimiter();
		$res .= $this->blog_name($blogEntry) . $this->minorDelimiter();
		$res .= $this->title($blogEntry) . $this->minorDelimiter();
		$res .= $this->delete_checkbox($blogEntry) . $this->minorDelimiter();

		return $res;
	}

	protected function id($id)
	{
		$link = "<a href='/externalcontent/edit/" . $id . "' style='text-decoration: none;'>" . $id . "</a>";
		return $this->td('id', $link);
	}

	/**
	 * @param BlogPending $blogEntry
	 * @return string|null
	 */
	protected function article_date($blogEntry)
	{
		if(!isset($blogEntry->article_date) || empty($blogEntry->article_date)) {
			return null;
		}
		return $this->td('article_date', $blogEntry->article_date->format('M-d-Y H:i'));
	}

	/**
	 * @param BlogPending $blogEntry
	 * @return string
	 */
	protected function delete_checkbox($blogEntry)
	{
		return $this->td(null, '<input type="checkbox" name="delete[]" value="' . $blogEntry->id . '" />');
	}

	/**
	 * @param BlogPending $blogEntry
	 * @return string
	 */
	protected function tier($blogEntry)
	{
		$tier = null;
		if ($blogEntry->blog) {
			$tier = $blogEntry->blog->tier;
		}
		return $this->td('tier', $tier);
	}

	protected function blog_name($blogEntry)
	{
		$blogName = null;
		if ($blogEntry->blog) {
			$blogName = $blogEntry->blog->name;
		}
		return $this->td('blog_name', $blogName);
	}

	/**
	 * @param BlogPending $blogEntry
	 * @return string
	 */
	protected function title($blogEntry) 
	{
		$message = stripslashes(strip_tags($blogEntry->title));
		return "<td style='vertical-align: top; width: 400px; font-size: 12px;'><a href='/externalcontent/edit/" . $blogEntry->id . "' style='text-decoration: none; color: #000000'>" . substr($message, 0, 50) . " ...</a></td>";
	}

	/**
	 * @param string $fieldName
	 * @param string $contents
	 * @return string
	 */
	protected function td($fieldName, $contents) {
		$class = null;
		if ($this->sortField == $fieldName) {
			$class = 'class="page_Td_Sorted"';
		}
		return "<td style='vertical-align: top;' $class>" . $contents . "</td>";
	}

	protected function minorDelimiter() 
	{
		return "\n";
	}
}
