<?php

class Blastit_ExternalContent_View_Link {

	protected $urlBase;
	protected $pageCurr;
	protected $sortFieldCurr;
	protected $sortOrderCurr;
	protected $filterTierCurr;

	public function __construct($urlBase, $page, $sortFieldCurr, $sortOrderCurr, $filterTier)
	{
		$this->urlBase = $urlBase;
		$this->pageCurr = $page;
		$this->sortFieldCurr = $sortFieldCurr;
		$this->sortOrderCurr = $sortOrderCurr;
		$this->filterTierCurr = $filterTier;
	}

	/**
	 * Draws a button that can sort the contents.
	 * @param string $title Human title
	 * @param string $sortField Filed name to be sorted
	 * @return string Button HTML
	 */
	public function linkButton($title, $sortField) {
		$sortOrder = 'desc';
		$isCurrentField = false;
		if ($this->sortFieldCurr == $sortField) {
			$isCurrentField = true;
		}
		if ($isCurrentField) {
			$sortOrder = $this->sortOrderInvert($this->sortOrderCurr);
		}

		$url = $this->urlFull($sortField, $sortOrder);

		$titleFull = $title . ' ' . $this->arrow($sortOrder);
		return $this->link($titleFull, $url);
	}

	protected function link($title, $url) {
		return "<a href='$url'>" . $title . "</a>";
	}

	protected function urlFull($sortField, $order) {
		$res = '';
		$res .= $this->urlBase .
		$this->uriSortField($sortField) .
		'/' .
		$this->uriSortOrder($order);
		if ($this->pageCurr) {
			$res .= '/';
			$res .= $this->uriPage($this->pageCurr);
		}
		if ($this->filterTierCurr) {
			$res .= '/';
			$res .= $this->uriFilterTier($this->filterTierCurr);
		}
		return $res;
	}

	protected function uriSortField($sortField) {
		return 'sort/' . $sortField;
	}

	protected function uriSortOrder($order) {
		return 'order/' . $order;
	}

	protected function uriPage($page) {
		return 'page/' . $page;
	}

	protected function uriFilterTier($tier) {
		return 'filterTier/' . $tier;
	}

	/**
	 * Inverts asc to desc and vice-versa
	 * @param string $order asc|desc
	 * @return string asc|desc
	 */
	protected function sortOrderInvert($order) {
		if (!$order) {
			return 'desc';
		}
		if ($order == 'desc') {
			return 'asc';
		}
		return 'desc';
	}

	/**
	 * Sort arrow
	 * @param string $sortOrder asc|desc
	 * @return string HTML
	 */
	protected function arrow($sortOrder) {
		$posX = -2;
		$posY = 0;
		if ($sortOrder == 'asc') {
			$posY = -33;
		} else {
			$posY = 0;
		}
		$imgSrc = '/blog/wp-admin/images/arrows.png';
		$styleDisplay = 'display: inline-block; width: 12px; height: 13px;';
		$styleBackground = "background: url($imgSrc); background-position: {$posX}px {$posY}px; background-repeat: no-repeat;";
		$style = "$styleBackground $styleDisplay";
		return "<span style='$style'></span>";
	}
}