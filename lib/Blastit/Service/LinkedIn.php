<?php

class Blastit_Service_LinkedIn 
{
	protected $oauth_options = null;
	protected $access_token = null;
	protected $oauth_client = null;
	protected $base_uri = 'https://api.linkedin.com/v1';
	protected $response = null;
	protected $response_data = null;
	
	
	public function __construct($oauth_options, Zend_Oauth_Token_Access $access_token)
	{
		$this->oauth_options = $oauth_options;
		$this->access_token = $access_token;
	}
	
	/**
	 * Get configured instance 
	 * 
	 * This clas should not really contain any application specific or global data references.
	 * This method should be removed and the data used within it should be passed into the 
	 * constructor by the calling code. Since the calling code needs allot of refactoring I decided 
	 * to create this method as a temporary measure. (KFoster 2013-07-18)
	 */
	public static function newInstance($member)
	{
		if (empty($member->linkedin_access_token)) {
			throw new Exception('No access token set');
		}
		
		$access_token = unserialize($member->linkedin_access_token);
		
		$ln_config = array(
			'callbackUrl' => APP_URL . '/linkedin/callback',
			'consumerKey' => $member->network_app_linkedin->consumer_id,
			'consumerSecret' => $member->network_app_linkedin->consumer_secret
		);
		
		return new Blastit_Service_LinkedIn($ln_config, $access_token);
	}
	
	public function getProfileData()
	{
		$client = $this->getOauthClient();
		$client->resetParameters()->setUri($this->getUri('/people/~:(id,first-name,last-name,num-connections)'));
		$client->setMethod(Zend_Http_Client::GET);
		$response = $client->request();
		
		$result = $this->processResponse($response);
		
		return $result;
	}
	
	public function getResponseData() {
		return $this->response_data;
	}
	
	
	public function getResponseStatus() {
		$status = $this->response ? $this->response->getStatus() :  null;
		return $status;
	}
	
	
	public function statusUpdate($description, $url = null, $title = null, $image_url = null)
	{
		$request_data = array(
			'content' => array('description' => mb_substr($description, 0, 256, 'utf8')),
			'visibility' => array('code' => 'anyone')
		);
		if (!empty($title)) {
			$request_data['content']['title'] = mb_substr($title, 0, 200, 'utf8');
		}
		if (!empty($url)) {
			$request_data['content']['submittedUrl'] = $url;
		}
		if (!empty($image_url)) {
			$request_data['content']['submittedImageUrl'] = $image_url;
		}
		
		$request_json = Zend_Json::encode($request_data);
		
		$client = $this->getOauthClient();
		$client->resetParameters()->setUri($this->getUri('/people/~/shares'));
		$client->setMethod(Zend_Http_Client::POST);
		$client->setRawData($request_json, 'text/html');
		
		$linkedin_log_file = dirname(__FILE__).'/../../../log/linkedin.status.log';
		
		try {
			$response = $client->request();
		} catch (Zend_Http_Client_Exception $e) {
			$response = null;
			$log= get_class($e) . ":\n" . $e->getMessage() .
				"\nText:\n" . $description .
				"\n\n";
			file_put_contents($linkedin_log_file, $log, FILE_APPEND);
		}
		
		$result = ($response) ? $this->processResponse($response) : null;
		
		if (!$result) {
			$log	= "Error Code:\n" . $this->getErrorCode() .
					"\nError Message:\n" . $this->getErrorMessage() .
					"\nText:\n" . $description .
					"\n\n";
			file_put_contents($linkedin_log_file, $log, FILE_APPEND);
		}
		
		return $result;
	}
	
	public function getErrorCode()
	{
		$code = (
			!$this->isSuccessful() && 
			$this->response && 
			$this->isJson($this->response->getBody()) && 
			!empty($this->response_data['errorCode'])
		) ? $this->response_data['errorCode'] : null;
		return $code;
	}
	
	public function getErrorMessage()
	{
		if (!$this->isSuccessful()) {
			if ($this->response && !$this->isJson($this->response->getBody())) {
				return $this->response->getBody();
			} elseif (!empty($this->response_data)) {
				return (!empty($this->response_data['message'])) ? $this->response_data['message'] : null;
			}
		}
		return null;
	}

	protected function isSuccessful()
	{
		$success = $this->response && $this->isJson($this->response->getBody()) && in_array($this->response->getStatus(), array(200, 201, 202));
		return $success;
	}
	
	protected function processResponse(Zend_Http_Response $response)
	{
		$result = false;
		$this->response = $response;
		if ($this->isJson($response->getBody())) {
			$this->response_data = Zend_Json::decode($response->getBody());
			if ($this->isSuccessful()) {
				$result = !empty($this->response_data) ? $this->response_data : true;
			} 
		}
	
		return $result;
	}
	
	protected function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
	
	protected function getOauthClient()
	{
		if ($this->oauth_client == null) {
			if ($this->access_token == null) {
				throw new Exception('Access token not set');
			}
			$this->oauth_client = $this->access_token->getHttpClient($this->oauth_options);
			$this->oauth_client->setHeaders('Content-Type', 'application/json'); // we are sending json request data
			$this->oauth_client->setHeaders('x-li-format', 'json'); // request json response
		}
		
		return $this->oauth_client;
	}
	
	protected function getUri($uri)
	{
		return $this->base_uri . $uri;
	}
}