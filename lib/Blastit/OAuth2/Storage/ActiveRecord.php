<?php

use OAuth2\Storage\Pdo;

class Blastit_OAuth2_Storage_ActiveRecord extends Pdo
{

    public function __construct($config = array())
    {
        $this->config = array_merge(array(
            'client_model' 			=> 'OAuthClient',
            'access_token_model' 	=> 'OAuthAccessToken',
            'refresh_token_model' 	=> 'OAuthRefreshToken',
            'code_model' 			=> 'OAuthAuthorizationCode',
            'user_model' 			=> 'OAuthUser',
            'jwt_model' 			=> 'OAuthJwt',
        ), $config);
    }

    /* OAuth2_Storage_ClientCredentialsInterface */
    public function checkClientCredentials($client_id, $client_secret = null)
    {
        $client = $this->find('client', 'client_id', $client_id);
        return $client->client_secret == $client_secret;
    }

    public function getClientDetails($client_id)
    {
        $client = $this->find('client', 'client_id', $client_id);
        return $this->modelToArray($client);
    }

    public function checkRestrictedGrantType($client_id, $grant_type)
    {
        $details = $this->getClientDetails($client_id);
        if (isset($details['grant_types'])) {
            return in_array($grant_type, (array) $details['grant_types']);
        }

        // if grant_types are not defined, then none are restricted
        return true;
    }

    /* OAuth2_Storage_AccessTokenInterface */
    public function getAccessToken($access_token)
    {
    	$token = $this->find('access_token', 'access_token', $access_token);
        return $this->modelToArray($token);
    }

    public function setAccessToken($access_token, $client_id, $user_id, $expires, $scope = null)
    {
        // convert expires to datestring
        $expires = date('Y-m-d H:i:s', $expires);

        if (false == ($token = $this->getAccessToken($access_token))) {
        	$model = $this->getModel('access_token');
        	$token = new $model;
        }
		$token->access_token = $access_token;
		$token->client_id 	 = $client_id;
		$token->member_id 	 = $user_id;
		$token->expires 	 = $expires;
		$token->scope 		 = $scope;
		return $token->save();

    }

    /* OAuth2_Storage_AuthorizationCodeInterface */
    public function getAuthorizationCode($code)
    {
        $code = $this->find('code', 'authorization_code', $code);
        return $this->modelToArray($code);
    }

    public function setAuthorizationCode($authorization_code, $client_id, $user_id, $redirect_uri, $expires, $scope = null)
    {
        // convert expires to datestring
        $expires = date('Y-m-d H:i:s', $expires);

        if (false == ($code = $this->getAuthorizationCode($authorization_code))) {
        	$model = $this->getModel('code');
        	$code = new $model;
        }
        $code->authorization_code 	= $authorization_code;
		$code->client_id 			= $client_id;
		$code->member_id 				= $user_id;
		$code->redirect_uri 		= $redirect_uri;
		$code->expires 				= $expires;
		$code->scope 				= $scope;
		return $code->save();
    }

    public function expireAuthorizationCode($authorization_code)
    {
    	$code = $this->find('code', 'authorization_code', $authorization_code);
		return $code->delete();
    }

    /* OAuth2_Storage_UserCredentialsInterface */
    public function checkUserCredentials($username, $password)
    {
        if ($user = $this->getUser($username)) {
            return $this->checkPassword($user, $password);
        }
        return false;
    }

    /* OAuth2_Storage_RefreshTokenInterface */
    public function getRefreshToken($refresh_token)
    {
		$token = $this->find('refresh_token', 'refresh_token', $refresh_token);
        return $this->modelToArray($token);
    }

    public function setRefreshToken($refresh_token, $client_id, $user_id, $expires_timestamp, $scope = null)
    {
        $expires = new \DateTime();
        $expires->setTimestamp($expires_timestamp);

    	$model = $this->getModel('refresh_token');

		$token = new $model;
		$token->refresh_token 	= $refresh_token;
		$token->client_id 		= $client_id;
		$token->member_id 		= $user_id;
		$token->expires 		= $expires;
		$token->scope 			= $scope;
		return $token->save();
		
    }

    public function unsetRefreshToken($refresh_token)
    {    
    	$model = $this->getModel('refresh_token');
    	return $model::delete( array('refresh_token' => $refresh_token) );
    }

    // plaintext passwords are bad!  Override this for your application
    protected function checkPassword($user, $password)
    {
        return $user->password == sha1($password);
    }

    public function getUser($username)
    {
    	throw new Exception('getUser not implemented');
    	// return $this->find('user', 'username', $username);
    }

    public function setUser($username, $password, $firstName = null, $lastName = null)
    {
		throw new Exception('setUser not implemented');
    }

    /* OAuth2_Storage_JWTBearerInterface */
    public function getClientKey($client_id, $subject)
    {
    	$model = $this->getModel('jwt');
    	$key = $model::find( array('client_id' => $client_id, 'subject' => $subject));
    	return $this->modelToArray($key);
    }

    
    protected function find($model, $property, $id) {
    	$model = $this->getModel($model);
    	$method = 'find_by_' . $property; 
    	return $model::$method($id);
    }
    
    protected function getModel($type) 
    {
    	if (isset($this->config[$type . '_model'])) return $this->config[$type . '_model'];
    }

	protected function modelToArray($model)
	{
		if (!is_object($model)) return false;
		$array = $model->to_array();
		if (isset($array['expires'])) {
			$array['expires'] = $model->expires->getTimestamp();
		}
		if (isset($array['member_id'])) {
			$array['user_id'] = $array['member_id'];
			unset($array['member_id']);
		}
		return $array;
	}

}
