<?php

class Blastit_Report
{

	protected $textFile 		= null;
	protected $textFileHandle 	= null;
	protected $csvFile 			= null;
	protected $csvFileHandle	= null;
	protected $htmlFile 		= null;
	protected $htmlFileHandle	= null;
	protected $columnTitles 	= array();
	protected $rowData			= array();
	protected $columnsRight		= array();
	protected $maxLengths		= array();
	protected $textRowPadding	= 4;
	protected $generated		= false;
	
	public function enableText() {
		$this->textFile = '/tmp/'  . md5(microtime()) . '.txt';
		$this->textFileHandle = fopen($this->textFile, 'w');
	}

	public function enableHTML() {
		$this->htmlFile = '/tmp/'  . md5(microtime()) . '.html';
		$this->htmlFileHandle = fopen($this->htmlFile, 'w');
	}

	public function enableCSV() {
		$this->csvFile       = '/tmp/'  . md5(microtime()) . '.csv';
		$this->csvFileHandle = fopen($this->csvFile, 'w');
	}
	
	public function setColumnTitles($titles) {
		$this->columnTitles = $titles;
		$this->updateMaxLengths($titles);
	}
	
	public function setColumnRight($i) {
		$this->columnsRight[$i] = true;
	}
	
	public function addRow($columns) {
		$this->rowData[] = $columns;
		$this->updateMaxLengths($columns);
	}
	
	protected function updateMaxLengths($columns) {
		$i = 0;
		foreach ($columns as $s) {
			$len = mb_strlen($s);
			if (!isset($this->maxLengths[$i]) || ($len > $this->maxLengths[$i])) {
				$this->maxLengths[$i] = $len;
			}
			$i++;
		}
	}
	
	public function generate() {
		if ($this->htmlFileHandle) {
			fwrite($this->htmlFileHandle, "<table>\n");
		}

		if (count($this->columnTitles)) {
			$this->generateRow($this->columnTitles, true);
			$width = array_sum($this->maxLengths) + ($this->textRowPadding * count($this->maxLengths)) - 1;
			$this->addTextRow( str_repeat('-', $width) );
		}

		foreach ($this->rowData as $columns) {
			 $this->generateRow($columns);
		}

		if ($this->htmlFileHandle) {
			fwrite($this->htmlFileHandle, "</table>\n");
		}

		if ($this->textFileHandle) fclose($this->textFileHandle);
		if ($this->htmlFileHandle) fclose($this->htmlFileHandle);
		if ($this->csvFileHandle)  fclose($this->csvFileHandle);
		$this->generated = true;
	}

	public function email($to, $from, $subject, $attachmentFileName) {
	
		$this->checkGenerated();
	
		$message = '<html><body>' . $this->getHTML() . '</body></html>';
		$outboundFile = '/tmp/' . $attachmentFileName . '.csv';

		copy($this->getCSVFile(), $outboundFile);
		
		$mail = new Zend_Mail();
		$mail->setFrom($from);

		$filedata = file_get_contents($outboundFile);
		$a = $mail->createAttachment($filedata);
		$a->type = 'text/csv';
		$a->filename = basename($outboundFile);

		$mail->addTo($to);
		$mail->setSubject("Your report" );
			
		$mail->setBodyHTML($message);
		return $mail->send();
	}
	
	protected function checkGenerated() {
		if (!$this->generated) throw new Zend_Exception('Method generate() must be called first');
	}
	
	public function getText() {
		$this->checkGenerated();
		return file_get_contents($this->textFile);
	}
	
	public function getHTML() {
		$this->checkGenerated();
		return file_get_contents($this->htmlFile);
	}
	
	public function getTextFile() {
		$this->checkGenerated();
		return $this->textFile;
	}
	
	public function getHTMLFile() {
		$this->checkGenerated();
		return $this->htmlFile;
	}

	public function getCSVFile() {
		$this->checkGenerated();
		return $this->csvFile;
	}
	
	protected function generateRow($columns, $isTitles = false) {
		$this->addTextRow($columns, $isTitles);
		$this->addHTMLRow($columns, $isTitles);
		$this->addCSVRow($columns, $isTitles);
	}
	
	protected function addTextRow($columns) {
		if (!$this->textFileHandle) return false;
		if (is_string($columns)) {
			$row = $columns;
		} else {
			$row = '';
			$i = 0;
			$padding = str_repeat(' ', $this->textRowPadding);
			foreach ($columns as $column) {
				$max  = $this->maxLengths[$i];
				$padType = STR_PAD_RIGHT;
				if (isset($this->columnsRight[$i])) {
					$padType = STR_PAD_LEFT;
				}
				$row .= str_pad($column, $max, ' ', $padType) . $padding;
				$i++;
			}
		}
		fwrite($this->textFileHandle, $row . "\n");
	}

	protected function addHTMLRow($columns, $isTitles) {
		if (!$this->htmlFileHandle) {
			return false;
		}

		$tag = 'td';
		if ($isTitles) $tag = 'th';
		$row = '<tr>';
		foreach ($columns as $c) {
			$row .= "<$tag>$c</$tag>";
		}
		$row .= '<tr>';
		fwrite($this->htmlFileHandle, $row . "\n");
	}
	
	protected function addCSVRow($columns) {
		if (!$this->csvFileHandle) return false;
		fputcsv($this->csvFileHandle, $columns);
	}

}
