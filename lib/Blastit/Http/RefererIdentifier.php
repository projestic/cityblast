<?php

class Blastit_Http_RefererIdentifier
{
	protected static $searchEngineFragments = array(
		'www.google.',
		'msn.',
		'bing.',
		'yahoo.',
		'ask.',
		'search.aol.',
		'mywebsearch.',
		'blekko.',
		'search.lycos.',
		'dogpile.',
		'webcrawler.',
		'www.info.com',
		'infospace.',
		'msxml.excite',
		'baidu.',
		'yandex.',
		'youdao.'
	);

	public static function isSearchEngine($httpReferer = null)
	{
		$defaultReferer = !empty($_SERVER['HTTP_REFERER']) ? strtolower($_SERVER['HTTP_REFERER']) : null;
		$httpReferer = ($httpReferer === null) ? $defaultReferer : strtolower($httpReferer);
		
		$isSearchEngine = false;
		if (!empty($httpReferer)) {
			foreach (self::$searchEngineFragments as $searchEngineFragment) {
				if (strpos($httpReferer, $searchEngineFragment) !== false) {
					$isSearchEngine = true;
					break;
				}
			}
		}
		
		return $isSearchEngine;
	}
}