<?php

class Blastit_Clareity_Service_BaseError
{
	public $faultCode;
	public $faultMessage;
	public $details;
	
	public function __construct($faultCode = null, $details = null)
	{
		$this->faultCode = $faultCode;
		$this->faultMessage = $this->getMessage($faultCode);
		$this->details = $details;
	}
	
	public function getSoapFault()
	{
		return new SoapFault($this->faultCode, $this->faultMessage, null, $this, 'fault');
	}
	
	private function getMessage($code)
	{
		$message = 'Unknown';
		switch ($code) 
		{
			case 'CS00010':
				$message = 'Store temporarily disabled';
			break;
			case 'CS00020':
				$message = 'Invalid Store Identifier';
			break;
			case 'CS00030':
				$message = 'Invalid Vendor Identifier';
			break;
			case 'CS00040':
				$message = 'Invalid Item Identifier';
			break;
			case 'CS00050':
				$message = 'Missing Transaction Identifier';
			break;
			case 'CS00060':
				$message = 'Missing API Key provided';
			break;
			// Message Operation Error
			case 'CS01010': 
				$message = 'Message error';
			break;
			// Report Operation Errors
			case 'CS02010':
				$message = 'Invalid date range';
			break;
			case 'CS02020':
				$message = 'Report creation fault, internal error';
			break;
			// Provision Server Operation Errors
			case 'PS03000':
				$message = 'Unknown error occurred - Internal error';
			break;
			case 'PS03010':
				$message = 'No parameters received';
			break;
			case 'PS03020':
				$message = 'Invalid storeId received';
			break;
			case 'PS03030':
				$message = 'No user found';
			break;
			case 'PS03040':
				$message = 'User account is disabled';
			break;
			case 'PS03050':
				$message = 'No item found';
			break;
			case 'PS03060':
				$message = 'The requested item is disabled for this store';
			break;
			case 'PS03070':
				$message = 'Store is disabled';
			break;
			case 'PS03080':
				$message = 'No user office found';
			break;
			case 'PS03090':
				$message = 'No user broker found';
			break;
			case 'PS03100':
				$message = 'No user association found';
			break;
			case 'PS03110':
				$message = 'The item is currently back ordered';
			break;
			case 'PS99000':
				$message = 'Invalid API Key';
			break;
			case 'PS99010':
				$message = 'Invalid Transaction ID';
			break;
		}
		
		return $message;
	}
}
