<?php

/**
 */
class Blastit_Clareity_Service_Provision
{
	public static $classmap = array(
		'AddressType' => 'Blastit_Clareity_Service_Provision_Address',
		'BuyerInfoType' => 'Blastit_Clareity_Service_Provision_BuyerInfo',
		'ProvisionResponseType' => 'Blastit_Clareity_Service_Provision_ProvisionResponse',
		'PollProvisionType' => 'Blastit_Clareity_Service_Provision_PollProvision',
		'RemainingSupplyType' => 'Blastit_Clareity_Service_Provision_RemainingSupply',
		'RemainingSupplyResponseType' => 'Blastit_Clareity_Service_Provision_RemainingSupplyResponse'
	);
	
	private $server = null;
	
	private static $debug = true;
	
	public function __construct(Zend_Soap_Server $server)
	{
		$this->server = $server;
	}
	
	public function ProvisionItem(Blastit_Clareity_Service_Provision_BuyerInfo $param)
	{
		$result = null;
		$continue = true;
		
		if (empty($param->apiKey)) {
			// Invalid API Key
			throw $this->fault('PS99000', 'No API key provided');
		}
		
		$apikey = null;
		try {
			$apikey = ClareityWsApiKey::find_by_apikey($param->apiKey);
		} catch (Exception $e) {
			// Internal error
			throw $this->fault('PS03000', $e->getMessage());
		}
		if (empty($apikey)) {
			// Invalid API Key
			throw $this->fault('PS99000', 'API key not found');
		}

		if (!$apikey->enabled) {
			// API key diabled
			throw $this->fault('PS99000', 'API key disabled');
		}
		
		$this->transValidate($param);

		$member = $this->memberCreate($param);
		
		$trans = $this->transSave($param, $member);
		$login = $this->samlLoginCreate($param, $member);
		
		$response = new Blastit_Clareity_Service_Provision_ProvisionResponse();
		$response->transactionId = $param->transactionId;
		$response->message = 'Provision successful';
		$response->provisionCode = 0; // Zero = no error
		$result = $response;
		
		$alert_emailer = Zend_Registry::get('alertEmailer');
		$alert_emailer->send(
			'New signup via Clareity (' . $member->id . ')',
			'New signup via Clareity: ' . $member->first_name . ' ' . $member->last_name . ' (' . $member->id . ')',
			'notice-clareity-provisioned-member'
		);
		
		return $result;
	}
	
	private function samlLoginCreate(Blastit_Clareity_Service_Provision_BuyerInfo $param, Member $member)
	{
		$storeName = mb_strtolower($param->StoreName);
		$login = SAMLLogin::find_by_username_and_idp($param->UserName, $storeName);
		$login = !empty($login) ? $login : new SAMLLogin();
		
		$login->member_id = $member->id;
		$login->username = $param->UserName;
		$login->idp = $storeName;
		
		try {
			$login->save();
		} catch (Exception $e) {
			if (static::$debug) {
				throw $this->fault('PS03000', 'Failed creating saml login: ' . $e->getMessage());
			} else {
				throw $this->fault('PS03000', 'Failed creating saml login');
			}
		}
	}
	
	private function memberCreate(Blastit_Clareity_Service_Provision_BuyerInfo $param)
	{
		$item = ClareityWsItem::find_by_store_and_name($param->StoreName, $param->ItemName);
		
		$paymentGateway = PaymentGateway::find_by_code('CLAREITY');
		
		$member = new Member();
		$member->type_id = $item->member_type->id;
		$member->email = $param->Email;
		$member->last_name = $param->LastName;
		$member->first_name = $param->FirstName;
		$member->address = $param->ShippingAddress->Street;
		$member->city = $param->ShippingAddress->City;
		$member->state = $param->ShippingAddress->State;
		$member->country = $param->ShippingAddress->Country;
		$member->zip = $param->ShippingAddress->Zip;
		$member->phone = $param->MobilePhone;
		$member->billing_cycle = 'month';
		$member->payment_gateway_id = $paymentGateway->id;
		$member->payment_status = 'paid';
		$member->account_type = 'paid';
		
		try {
			$member->save();
		} catch (Exception $e) {
			if (static::$debug) {
				throw $this->fault('PS03000', 'Failed creating member account: ' . $e->getMessage());
			} else {
				throw $this->fault('PS03000', 'Failed creating member account');
			}
		}
		
		return $member;
	}
	
	private function transValidate(Blastit_Clareity_Service_Provision_BuyerInfo $param)
	{
		$trans = ClareityWsProvisionTrans::find_by_transaction_id($param->transactionId);
		if (!empty($trans)) {
			// Invalid Transaction ID
			throw $this->fault('PS99010', 'Transaction id "' . $param->transactionId . '" previously processed');
		}
		
		$item = ClareityWsItem::find_by_store_and_name($param->StoreName, $param->ItemName);
		if (empty($item)) {
			// No item found
			throw $this->fault('PS03050', 'There is no item "' . $param->ItemName . '" for store "' . $param->StoreName . '"');
		}
	}
	
	private function transSave(Blastit_Clareity_Service_Provision_BuyerInfo $param, Member $member)
	{
		$trans = new ClareityWsProvisionTrans();
		$trans->transaction_id = $param->transactionId;
		$trans->member_id = $member->id;
		$trans->store_name = $param->StoreName;
		$trans->item_name = $param->ItemName;
		$trans->item_quantity = $param->ItemQuantity;
		$trans->user_name = $param->UserName;
		$trans->nrds_id = $param->NRDSId;
		$trans->association_id = $param->AssociationId;
		$trans->office_id = $param->OfficeId;
		$trans->broker_id = $param->BrokerId;
		$trans->email = $param->Email;
		$trans->first_name = $param->FirstName;
		$trans->last_name = $param->LastName;
		$trans->mobile_phone = $param->MobilePhone;
		$trans->adrs_ship_street = $param->ShippingAddress->Street;
		$trans->adrs_ship_city = $param->ShippingAddress->City;
		$trans->adrs_ship_state = $param->ShippingAddress->State;
		$trans->adrs_ship_zip = $param->ShippingAddress->Zip;
		$trans->adrs_ship_country = $param->ShippingAddress->Country;
		$trans->adrs_bill_street = $param->BillingAddress->Street;
		$trans->adrs_bill_city = $param->BillingAddress->City;
		$trans->adrs_bill_state = $param->BillingAddress->State;
		$trans->adrs_bill_zip = $param->BillingAddress->Zip;
		$trans->adrs_bill_country = $param->BillingAddress->Country;

		try {
			$trans->save();
		} catch (Exception $e) {
			if (static::$debug) {
				throw $this->fault('PS03000', 'Failed saving provision transaction data: ' . $e->getMessage());
			} else {
				throw $this->fault('PS03000', 'Failed saving provision transaction data');
			}
		}
		
		return $trans;
	}
	
	private function fault($code, $details = '')
	{
		$alert_emailer = Zend_Registry::get('alertEmailer');
		$alert_emailer->send(
			'Clareity provision fault',
			'Code: ' . $code . " \n\n" . $details,
			'error-clareity-provision-fault',
			true
		);
		
		$fault = new Blastit_Clareity_Service_BaseError($code, $details);
		return $fault->getSoapFault();
	}
}

class Blastit_Clareity_Service_Provision_Address
{
	public $Street;
	public $City;
	public $State;
	public $Zip;
	public $Country;
}

class Blastit_Clareity_Service_Provision_BuyerInfo
{
	public $apiKey;
	public $transactionId;
	public $StoreName;
	public $ItemName;
	public $ItemQuantity;
	public $UserName;
	public $NRDSId;
	public $AssociationId;
	public $OfficeId;
	public $BrokerId;
	public $Email;
	public $FirstName;
	public $LastName;
	public $MobilePhone;
	public $ShippingAddress;
	public $BillingAddress;
}

class Blastit_Clareity_Service_Provision_ProvisionResponse
{
	public $transactionId;
	public $message;
	public $provisionCode;
	public $RetryInterval;
}

class Blastit_Clareity_Service_Provision_PollProvision
{
	public $apiKey;
	public $transactionId;
}

class Blastit_Clareity_Service_Provision_RemainingSupply
{
	public $apiKey;
	public $StoreName;
	public $ItemName;
	public $UserName;
}

class Blastit_Clareity_Service_Provision_RemainingSupplyResponse
{
	public $StoreName;
	public $ItemName;
	public $UserName;
	public $UnitsRemaining;
	public $ExpiryDate;
	public $NextEventDate;
	public $NextEventLocation;
}