<?php


class Blastit_Oauth_Consumer_Twitter extends Zend_Oauth_Consumer
{

    public function __construct($options = null)
    {

        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }
        
        $network_app_twitter = Zend_Registry::get('networkAppTwitter');

		$twitterConfig = array(
							'callbackUrl' 	 => APP_URL . '/twitter/callback',
							'consumerKey' 	 => $network_app_twitter->consumer_id,
							'consumerSecret' => $network_app_twitter->consumer_secret
						);
		
		if (!is_array($options)) $options = array();
		
		$options = array_merge($twitterConfig, $options);

        $this->_config = new Zend_Oauth_Config;
        if (!is_null($options)) {
            if ($options instanceof Zend_Config) {
                $options = $options->toArray();
            }
            if (!isset($options['siteUrl'])) $options['siteUrl'] = 'https://api.twitter.com/oauth';
            $this->_config->setOptions($options);
        }
    }


}
