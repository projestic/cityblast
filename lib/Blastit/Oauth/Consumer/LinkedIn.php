<?php

class Blastit_Oauth_Consumer_LinkedIn extends Zend_Oauth_Consumer
{
    public function __construct($options = null)
    {

        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }
        
        $network_app_linkedin = Zend_Registry::get('networkAppLinkedIn');

		$config = array(
			'callbackUrl' => APP_URL . '/linkedin/callback',
			'consumerKey' => $network_app_linkedin->consumer_id,
			'consumerSecret' => $network_app_linkedin->consumer_secret
		);
		
		$options = (is_array($options)) ? $options : array();
		$options = array_merge($config, $options);

		if (!isset($options['siteUrl'])) {
			$options['siteUrl'] = 'https://www.linkedin.com/uas/oauth';
		}
		if (!isset($options['requestUrl'])) {
			$options['srequestUrl'] = 'https://api.linkedin.com/uas/oauth/requestToken';
		}
		if (!isset($options['requestTokenUrl'])) {
			$options['requestTokenUrl'] = 'https://api.linkedin.com/uas/oauth/requestToken';
		}
		if (!isset($options['accessTokenUrl'])) {
			$options['accessTokenUrl'] = 'https://api.linkedin.com/uas/oauth/accessToken';
		}
		if (!isset($options['accessTokenUrl'])) {
			$options['accessTokenUrl'] = 'https://api.linkedin.com/uas/oauth/accessToken';
		}
		
		$this->_config = new Zend_Oauth_Config();
		$this->_config->setOptions($options);
    }
}
