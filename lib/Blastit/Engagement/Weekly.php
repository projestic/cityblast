<?php 

class Blastit_Engagement_Weekly {

	/**
	 * @param string $runmode all|yesterday
	 */
	public function gatherTopEngagementScoreByWeek($runmode) {
		if ($runmode == 'yesterday') {
			$dt = new DateTime();
			$this->gatherWithinDate($dt);
			return;
		}
		if ($runmode == 'all') {
			$dateFrom = new DateTime('2008-01-01'); // some old date, to capture a big time frame
		  $dateTo = new DateTime();
			$this->gatherFromDateToDate($dateFrom, $dateTo);
			return;
		}
		// Example: 2013-10-01,2013-11-11
		$monthsArg = $runmode;
		$monthsArgExploded = explode(',', $monthsArg);
		$dateFrom = new DateTime($monthsArgExploded[0]);
		$dateTo = new DateTime($monthsArgExploded[1]);
		$this->gatherFromDateToDate($dateFrom, $dateTo);
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 */
	protected function gatherFromDateToDate($dateFrom, $dateTo) {
		while($dateTo >= $dateFrom) {
			$affectedRowsCount = $this->gatherWithinDate($dateTo);
			$isStop = $affectedRowsCount == 0;
			$dateTo->modify('-1 week');
			usleep(10);
		}
	}

	/**
	 * @param DateTime $dt
	 * @return int Affected rows count
	 */
	protected function gatherWithinDate($dt) {
		$weekTimeBorders = $this->findPreviousWeekTimeBorders($dt);
		$weekStart = $weekTimeBorders['weekStart'];
		$weekEnd = $weekTimeBorders['weekEnd'];
		return EngagementScoreAggregatorTopByWeek::inst()->aggrTopByWeek($weekStart, $weekEnd);
	}

	/**
	 * Example:
	 * 2013-oct
	 * Mon Tue Wed Thu Fri Sat Sun
	 * 14   15  16  17  18  19  20
	 * 21  22
	 *
	 * If today is 2013-oct-22 Tue, then the result would be:
	 * weekStart => 2013-oct-14 Mon
	 * weekEnd => 2013-oct-21 Mon
	 *
	 * @param DateTime $dtOrig
	 * @return string
	 */
	protected function findPreviousWeekTimeBorders($dtOrig) {
		$dt = clone $dtOrig;
//$dt->setDate(2013, 10, 21); // temporary test
		$this->rollBackToClosestMonday($dt, false);
		$weekEndStr = $dt->format('Y-m-d');
		$this->rollBackToClosestMonday($dt, true);
		$weekStartStr = $dt->format('Y-m-d');
		$weekNumber = $dt->format('W');
		return array(
			'weekStart' => $weekStartStr,
			'weekEnd' => $weekEndStr,
			'weekNumber' => $weekNumber
		);
	}

	/**
	 * Example.
	 * Provided date: 2013oct22 Tue.
	 * Result date: 2013oct21 Monday.
	 *
	 * Rolls back the date to the closest Monday
	 * @param DateTime $dt Date to be manipulated
	 * @param bool $isExcludeCurrentMonday If currently is already situated on a needed day (Monday) - exclude it and roll back further
	 */
	protected function rollBackToClosestMonday($dt, $isExcludeCurrentMonday) {
		$mondayIndex = 1;
		$weekdayNumber = $dt->format('N');
		if ($isExcludeCurrentMonday) {
			if ($weekdayNumber == $mondayIndex) {
				$dt->modify("-1 day");
			}
			$weekdayNumber = $dt->format('N');
		}
		while($weekdayNumber != $mondayIndex) {
			$dt->modify("-1 day");
			$weekdayNumber = $dt->format('N');
		}
		$dt->setTime(0, 0, 0);
	}
}