<?php

class Blastit_Access_Cityblast extends Blastit_Access_Abstract
{
	protected $callbacks = array();
	
	public function isAuthenticated()
	{
		return Member::isLoggedIn();
	}
	
	public function hasRole($rolenames)
	{
		if (!is_array($rolenames)) $rolenames = array($rolenames);
		$member = Member::getActive();
		foreach ($rolenames as $rolename) {
			if ($member->hasType($rolename, true)) return true;
		}
		return false;
	}
}
