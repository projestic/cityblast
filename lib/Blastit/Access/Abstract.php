<?php

abstract class Blastit_Access_Abstract
{
	abstract public function isAuthenticated();
	
	abstract public function hasRole($rolenames);
	
	public function isPermitted($action = null, $controller = null, $module = null)
	{
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$module = !empty($module) ? $module : $request->getModuleName();
		$controller = !empty($controller) ? $controller : $request->getControllerName();
		$action = !empty($action) ? $action : $request->getActionName();
		
		$acl = $this->getControllerAcl($controller, $action, $module);
		
		$result = null;
		
		if (isset($acl[$action])) {
			$permittedRoles = $acl[$action];
		} elseif (isset($acl['*'])) {
			$permittedRoles = $acl['*'];
		} else {
			$result = false;
		}

		if ($result === null && $permittedRoles == 'public') $result = true;
		if ($this->isAuthenticated()) {
			if ($result === null && $permittedRoles == '*') $result = true;
			if ($result === null && $this->hasRole($permittedRoles)) $result = true;
		}
		
		if ($result === null) $result = false;
		
		return $result;
	}
	
	protected function getControllerAcl($controller, $action, $module)
	{
		$controllerName = ucfirst($controller) . 'Controller';
		Zend_Controller_Front::getInstance()->getDispatcher()->loadClass($controllerName);
		$acl = method_exists($controllerName, 'getACL') ?  $controllerName::getACL() : array();
		
		return $acl;
	}
}
