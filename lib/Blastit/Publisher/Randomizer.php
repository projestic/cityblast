<?php

class Blastit_Publisher_Randomizer
{

	protected $frequency_days = 7; // number of days
	protected $weight = 2.2;
	public $random = 0;
	
	public function test($freq, $variation) {
		$random = $this->gaussianWeightedRandom( 1, $this->frequency_days, $variation * $this->weight );
		return (bool) ($random <= $freq);
	}
	
	public function isPublishingToday($member, $service) {

		$days = $this->frequency_days;
		
		$this->random = null;
		
		if ($member->frequency_id == $days) {						
			return true;
		}
				
		$since = date('Y-m-d', strtotime(-$days . ' days')) . '00:00:00';
		
		$attempts = Post::count(array('conditions' => array("member_id = ? AND type = 'NONE' AND created_at > '" . $since . "' AND result = 0", $member->id )));
		$posts    = Post::count(array('conditions' => array("member_id = ? AND type = ? AND created_at > '" . $since . "' AND result = 1", $member->id, strtoupper($service) )));

		// might need to use these instead in the future
		
		// $attempt_days = Post::find(array('select' => 'COUNT( DISTINCT(DATE(created_at)) ) as count', 'conditions' => array("member_id = ? AND created_at > '" . $since . "'", 35)));
		// $post_days    = Post::find(array('select' => 'COUNT( DISTINCT(DATE(created_at)) ) as count', 'conditions' => array("member_id = ? AND created_at > '" . $since . "' AND result = 1", 35 )));
		
		$member_age = $member->created_at->diff( new DateTime() );
		$variation_weight = 1;
		
		if ($member_age->format('%a') < $days) $variation_weight = $member_age->format('%a')/$days;

		$variation 	= $posts - $member->frequency_id * $variation_weight;

		Logger::log("Member age: " . $member_age->format('%a') . ' days');
		Logger::log("Posts: $posts");
		Logger::log("Frequency: {$member->frequency_id}");
		Logger::log("Variation: $variation");
		Logger::log("Variation weight: $variation_weight");

		// if $variation is more than 2 off, don't use random value, just adjust
		
		if ($variation > 2) return false;
		if ($variation < -2) return true;

		// more than 1.2 and the weighted random calculation just takes way too long.
		
		if ($variation > 1.2)  $variation = 1.2;
		if ($variation < -1.2) $variation = -1.2;

		// if no posts but services are enabled, no need to do the random calculation. Member should publish.
		
		if (($posts == 0) && $member->frequency_id) return true;

		$random = $this->gaussianWeightedRandom( 1, $days, $variation * $this->weight );

		$this->random = $random;
		
		Logger::log("Random: $random");
		
		return (bool) ($random <= $member->frequency_id);

	}

	protected function mkseed()
	{
		srand(hexdec(substr(md5(microtime()), -8)) & 0x7fffffff) ;
	}

	protected function random_0_1()
	{
		//  returns random number using mt_rand() with a flat distribution from 0 to 1 inclusive
		return (float) mt_rand() / (float) mt_getrandmax() ;
	}

	protected function random_PN()
	{
		//  returns random number using mt_rand() with a flat distribution from -1 to 1 inclusive
		return (2.0 * $this->random_0_1()) - 1.0 ;
	}

	protected function gauss()
	{
		static $useExists = false ;
		static $useValue ;

		if ($useExists) {
			//  Use value from a previous call to this function
			//
			$useExists = false ;
			return $useValue ;
		} else {
			//  Polar form of the Box-Muller transformation
			//
			$w = 2.0 ;
			while (($w >= 1.0) || ($w == 0.0)) {
				$x = $this->random_PN() ;
				$y = $this->random_PN() ;
				$w = ($x * $x) + ($y * $y) ;
			}
			$w = sqrt((-2.0 * log($w)) / $w) ;

			//  Set value for next call to this function
			//
			$useValue = $y * $w ;
			$useExists = true ;

			return $x * $w ;
		}
	}

	protected function gauss_ms( $mean,
					   $stddev )
	{
		//  Adjust our gaussian random to fit the mean and standard deviation
		//  The division by 4 is an arbitrary value to help fit the distribution
		//      within our required range, and gives a best fit for $stddev = 1.0
		return $this->gauss() * ($stddev/4) + $mean;
	}

	protected function gaussianWeightedRandom( $LowValue,
									 $maxRand,
									 $mean=0.0,
									 $stddev=2.0 )
	{
		//  Adjust a gaussian random value to fit within our specified range
		//      by 'trimming' the extreme values as the distribution curve
		//      approaches +/- infinity
		$rand_val = $LowValue + $maxRand ;
		while (($rand_val < $LowValue) || ($rand_val >= ($LowValue + $maxRand))) {
			$rand_val = floor($this->gauss_ms($mean,$stddev) * $maxRand) + $LowValue ;
			$rand_val = ($rand_val + $maxRand) / 2 ;
		}

		return $rand_val ;
	}



}
