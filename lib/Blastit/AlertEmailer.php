<?php
 
/**
 * Alert Emailer
 * 
 * Simplifies the process of notifying developers and administrators of events within an application.
 * 
 * Email addresses can be registered to recieve certain types of email from an application.
 */
class Blastit_AlertEmailer 
{
	protected $appName = 'Application';
	protected $recipients = array();
	protected $optouts = array();
	protected $fromEmail = '';
	protected $attachments = array();
	protected $messageType = 'html';
	protected $convertNewlines = true;
	
	public function configure($config)
	{
		if (!empty($config['app'])) {
			$this->setAppName($config['app']);
		}
		
		if (!empty($config['from'])) {
			$this->setFrom($config['from']);
		}
		
		if (!empty($config['notify'])) {
			foreach ($config['notify'] as $subjectId => $emails) {
				if (!empty($emails)) {
					foreach ($emails as $emailOrGroup) {
						if (strstr($emailOrGroup, '@')) {
							$this->addRecipient($emailOrGroup, $subjectId);
						} elseif (!empty($config['group']) && !empty($config['group'][$emailOrGroup])) {
							foreach ($config['group'][$emailOrGroup] as $groupMemberEmail) {
								$this->addRecipient($groupMemberEmail, $subjectId);
							}
						}
					}
				}
			}
		}
		
		if (!empty($config['optout'])) {
			foreach ($config['optout'] as $subjectId => $emails) {
				if (!empty($emails)) {
					foreach ($emails as $emailOrGroup) {
						if (strstr($emailOrGroup, '@')) {
							$this->addOptout($emailOrGroup, $subjectId);
						} elseif (!empty($config['group']) && !empty($config['group'][$emailOrGroup])) {
							foreach ($config['group'][$emailOrGroup] as $groupMemberEmail) {
								$this->addOptout($groupMemberEmail, $subjectId);
							}
						}
					}
				}
			}
		}
	}
	
	public function setAppName($app)
	{
		$this->appName = !empty($app) ? $app : 'Application';
	}
	
	public function setMessageType($type)
	{
		$this->messageType = $type;
	}
	
	public function setConvertNewlines($bool)
	{
		$this->convertNewlines = $bool;
	}
	
	public function setFrom($email)
	{
		$this->fromEmail = !empty($email) ? $email : 'root@localhost';
	}
	
	/**
	 * Add recipient
	 * 
	 * The subject id indentifies the type of email which should be sent to this email address.
	 * 
	 * The special subject 'default' indicates this email address should recieve emails that have a subject id that does 
	 * not have any email addresses explicitly subscribed.
	 * The special subject 'all' indicates that this email address will recieve all emails not matter what the subject
	 * id might be.
	 */
	public function addRecipient($email, $subjectId = 'default')
	{
		if (!isset($this->recipients[$subjectId])) {
			$this->recipients[$subjectId] = array();
		}
		$this->recipients[$subjectId][$email] = $email;
	}
	
	/**
	 * Add opt out
	 * 
	 * The subject id indentifies the type of email which should NOT be sent to this email address.
	 */
	public function addOptout($email, $subjectId)
	{
		if (!isset($this->optouts[$subjectId])) {
			$this->optouts[$subjectId] = array();
		}
		$this->optouts[$subjectId][$email] = $email;
	}
	
	public function attachFile($filePath, $mimeType = 'application/octet-stream') {
		if (!file_exists($filePath)) throw new Zend_Exception("File $filePath does not exist.");
		$this->attachments[] = array('file' => $filePath, 'mimetype' => $mimeType);
	}
	
	public function send($subjectText = null, $message = null, $subjectId = 'default', $incVarDump = false, $debugdata = array()) 
	{
	
		$message = $this->getMessage($subjectText, $message, $incVarDump, $debugdata);
		
		$from = !empty($this->fromEmail) ? $this->fromEmail : 'root@localhost';
		
		$mail = new Zend_Mail();
		$mail->setFrom($from);

		$recipients = $this->getRecipients($subjectId);

		foreach ($this->attachments as $attachment) {
			$filedata = file_get_contents($attachment['file']);
			$a = $mail->createAttachment($filedata);
			$a->type = $attachment['mimetype'];
			$a->filename = basename($attachment['file']);
		}
		
		if (!empty($recipients)) {
			foreach ($recipients as $email) {
				$mail->addTo($email);
			}
			$mail->setSubject($this->getSubject($subjectText));
			
			if ($this->messageType == 'html') {
				$mail->setBodyHTML($message);
			} else {
				$mail->setBodyText($message);
			}
			$mail->send();
		}
		
		$this->attachments = array();
		
	}
	
	private function getRecipients($subjectId)
	{
		$subjectId = !empty($subjectId) ? $subjectId : 'default'; 
	
		$recipients = !empty($this->recipients[$subjectId]) ? $this->recipients[$subjectId] : array();
		
		// If no recipients are configured for the given subject use the default recipients list
		$recipients = empty($recipients) && !empty($this->recipients['default']) ? $this->recipients['default'] : $recipients;
		
		// If the "all" recipients list is set append those recipients 
		$all = !empty($this->recipients['all']) ? $this->recipients['all'] : array();
		$recipients = array_unique(array_merge($recipients, $all));
		
		// Remove opted out
		$optouts = !empty($this->optouts[$subjectId]) ? $this->optouts[$subjectId] : array();
		foreach ($recipients as $x => $recipient) {
			if (in_array($recipient, $optouts)) {
				unset($recipients[$x]);
			}
		}
		
		return $recipients;
	}
	
	private function getSubject($subjectText)
	{
		return $this->appName . ' Notification - ' . $subjectText;
	}
	
	private function getMessage($subjectText = null, $message = null, $incVarDump = false, $debugdata = array())
	{
		$subjectText = !empty($subjectText) ?  ' - ' . $subjectText : '';
		$message = $message . "\n\n";
		
		
		$message .= "\n\n---------------------------------------------------------------------------------------------";
		if ($incVarDump) {
			$processIsCli = static::processIsCli();
			if (!empty($debugdata)) {
				$message .= "\n\n" . '<strong>$debugdata</strong> = ' . print_r($debugdata, true);
			}
			if ($processIsCli) {
				if (isset($argc)) {
					$message .= "\n" . '<strong>$argc</strong> = ' . print_r($argc, true);
				}
			} else {
				$message .= "\n" . '<strong>$_REQUEST</strong> = ' . print_r($_REQUEST, true);
				$message .= "\n" . '<strong>$_SERVER</strong> = ' . print_r($_SERVER, true);
			}
			$message .= "\n" . '<strong>$_ENV</strong> = ' . print_r($_ENV, true);
			if (!empty($_SESSION)) {
				$message .= "\n" . '<strong>$_SESSION</strong> = ' . print_r($_SESSION, true);
			}
			$message .= "\n\n---------------------------------------------------------------------------------------------";
		}
		$message .= "\n" . static::processType();
		
		if ($this->messageType == 'html' && $this->convertNewlines) {
			return nl2br($message);
		} else {
			return $message;
		}
	}
	
	private function processIsCli()
	{
		return defined('STDIN');
	}
	
	private function processType()
	{
		$result = static::processIsCli() ? 'CLI' : 'HTTP';
		return $result;
	}
}
  
