<?php

class Blastit_ActiveRecord_AggregationHelper
{
	public static function selectReplaceInto(\ActiveRecord\Table $table, $select_query)
	{
		$temp_file = '/tmp/' . $table->table . '_' . microtime(true) . '_' . mt_rand(1, 100000) . '.txt';
		$table->conn->query($select_query . " INTO OUTFILE '" . $temp_file ."'");
		$table->conn->query("LOAD DATA INFILE '" . $temp_file . "' REPLACE INTO TABLE " . $table->table);
	}
}