<?php

// This class makes allows iteration over an entire database table using simple foreach syntax
class Blastit_ActiveRecord_IteratorBuffered implements Iterator
{
	private $model_name = null;
	private $conditions = array();
	private $cache_size = 0;
	private $query_offset = 0;
	private $position = 0;
	private $after_reload_callback = null;
	private $cache = array(); 

	public function __construct($model_name, $conditions = array(), $cache_size = 100) 
	{
		$this->model_name = $model_name;
		$this->conditions = !empty($conditions) ? $conditions : array();
		$this->cache_size = ($cache_size <= 500) ? $cache_size : 500;
	}
	
	public function callbackAfterReLoad($callback)
	{
		$this->after_reload_callback = $callback;
	}

	public function rewind() 
	{
		$this->query_offset = 0;
		$this->position = 0;
		$this->populateCache();
	}

	public function current() 
	{
		return $this->cache[$this->position];
	}

	public function key() 
	{
		$key = isset($this->cache[$this->position]) && isset($this->cache[$this->position]['id']) 
				? $this->cache[$this->position]['id'] 
				: null;
		$key = empty($key) ? $this->query_offset + $this->position : $key;
		return $key;
	}

	public function next() 
	{
		++$this->position;
		if ($this->position == $this->cache_size) {
			$this->query_offset += $this->cache_size;
			$this->position = 0;
			$this->populateCache();
			if ($this->after_reload_callback) {
				$after_reload_callback = $this->after_reload_callback;
				$after_reload_callback();
			}
		}
	}

	public function valid() 
	{
		return isset($this->cache[$this->position]);
	}
	
	private function populateCache()
	{
		$conditions = $this->conditions;
		
		$conditions['limit'] = $this->cache_size;
		$conditions['offset'] = $this->query_offset;
		
		$model_name = $this->model_name;
		
		$this->cache = $model_name::all($conditions);
	}
}