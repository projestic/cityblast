<?php

/**
 * This class is designed to be used with a loaded wordpress enviroment.
 * It depends on global variables from word press.
 */
class Blastit_WordPress
{
	public static function postCreate($data)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		//$result = wp_insert_post($data);
		$result = static::wordpressInsertPost($data);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	private static function wordpressInsertPost($postarr = array(), $wp_error = false)
	{
		global $wpdb;
		$user_id = 1;

		$defaults = array('post_status' => 'draft', 'post_type' => 'post', 'post_author' => $user_id,
			'ping_status' => get_option('default_ping_status'), 'post_parent' => 0,
			'menu_order' => 0, 'to_ping' =>  '', 'pinged' => '', 'post_password' => '',
			'guid' => '', 'post_content_filtered' => '', 'post_excerpt' => '', 'import_id' => 0,
			'post_content' => '', 'post_title' => '');

		$postarr = wp_parse_args($postarr, $defaults);

		unset( $postarr[ 'filter' ] );

		// We dont want to want to strip "unsafe" html and javascript
		//$postarr = sanitize_post($postarr, 'db');

		// export array as variables
		extract($postarr, EXTR_SKIP);

		// Are we updating or creating?
		$post_ID = 0;
		$update = false;
		if ( ! empty( $ID ) ) {
			$update = true;

			// Get the post ID and GUID
			$post_ID = $ID;
			$post_before = get_post( $post_ID );
			if ( is_null( $post_before ) ) {
				if ( $wp_error )
					return new WP_Error( 'invalid_post', __( 'Invalid post ID.' ) );
				return 0;
			}

			$guid = get_post_field( 'guid', $post_ID );
			$previous_status = get_post_field('post_status', $ID);
		} else {
			$previous_status = 'new';
		}

		$maybe_empty = ! $post_content && ! $post_title && ! $post_excerpt && post_type_supports( $post_type, 'editor' )
			&& post_type_supports( $post_type, 'title' ) && post_type_supports( $post_type, 'excerpt' );

		if ( apply_filters( 'wp_insert_post_empty_content', $maybe_empty, $postarr ) ) {
			if ( $wp_error )
				return new WP_Error( 'empty_content', __( 'Content, title, and excerpt are empty.' ) );
			else
				return 0;
		}

		if ( empty($post_type) )
			$post_type = 'post';

		if ( empty($post_status) )
			$post_status = 'draft';

		if ( !empty($post_category) )
			$post_category = array_filter($post_category); // Filter out empty terms

		// Make sure we set a valid category.
		if ( empty($post_category) || 0 == count($post_category) || !is_array($post_category) ) {
			// 'post' requires at least one category.
			if ( 'post' == $post_type && 'auto-draft' != $post_status )
				$post_category = array( get_option('default_category') );
			else
				$post_category = array();
		}

		if ( empty($post_author) )
			$post_author = $user_id;

		// Don't allow contributors to set the post slug for pending review posts
		if ( 'pending' == $post_status && !current_user_can( 'publish_posts' ) )
			$post_name = '';

		// Create a valid post name. Drafts and pending posts are allowed to have an empty
		// post name.
		if ( empty($post_name) ) {
			if ( !in_array( $post_status, array( 'draft', 'pending', 'auto-draft' ) ) )
				$post_name = sanitize_title($post_title);
			else
				$post_name = '';
		} else {
			// On updates, we need to check to see if it's using the old, fixed sanitization context.
			$check_name = sanitize_title( $post_name, '', 'old-save' );
			if ( $update && strtolower( urlencode( $post_name ) ) == $check_name && get_post_field( 'post_name', $ID ) == $check_name )
				$post_name = $check_name;
			else // new post, or slug has changed.
				$post_name = sanitize_title($post_name);
		}

		// If the post date is empty (due to having been new or a draft) and status is not 'draft' or 'pending', set date to now
		if ( empty($post_date) || '0000-00-00 00:00:00' == $post_date )
			$post_date = current_time('mysql');

			// validate the date
			$mm = substr( $post_date, 5, 2 );
			$jj = substr( $post_date, 8, 2 );
			$aa = substr( $post_date, 0, 4 );
			$valid_date = wp_checkdate( $mm, $jj, $aa, $post_date );
			if ( !$valid_date ) {
				if ( $wp_error )
					return new WP_Error( 'invalid_date', __( 'Whoops, the provided date is invalid.' ) );
				else
					return 0;
			}

		if ( empty($post_date_gmt) || '0000-00-00 00:00:00' == $post_date_gmt ) {
			if ( !in_array( $post_status, array( 'draft', 'pending', 'auto-draft' ) ) )
				$post_date_gmt = get_gmt_from_date($post_date);
			else
				$post_date_gmt = '0000-00-00 00:00:00';
		}

		if ( $update || '0000-00-00 00:00:00' == $post_date ) {
			$post_modified     = current_time( 'mysql' );
			$post_modified_gmt = current_time( 'mysql', 1 );
		} else {
			$post_modified     = $post_date;
			$post_modified_gmt = $post_date_gmt;
		}

		if ( 'publish' == $post_status ) {
			$now = gmdate('Y-m-d H:i:59');
			if ( mysql2date('U', $post_date_gmt, false) > mysql2date('U', $now, false) )
				$post_status = 'future';
		} elseif( 'future' == $post_status ) {
			$now = gmdate('Y-m-d H:i:59');
			if ( mysql2date('U', $post_date_gmt, false) <= mysql2date('U', $now, false) )
				$post_status = 'publish';
		}

		if ( empty($comment_status) ) {
			if ( $update )
				$comment_status = 'closed';
			else
				$comment_status = get_option('default_comment_status');
		}
		if ( empty($ping_status) )
			$ping_status = get_option('default_ping_status');

		if ( isset($to_ping) )
			$to_ping = sanitize_trackback_urls( $to_ping );
		else
			$to_ping = '';

		if ( ! isset($pinged) )
			$pinged = '';

		if ( isset($post_parent) )
			$post_parent = (int) $post_parent;
		else
			$post_parent = 0;

		// Check the post_parent to see if it will cause a hierarchy loop
		$post_parent = apply_filters( 'wp_insert_post_parent', $post_parent, $post_ID, compact( array_keys( $postarr ) ), $postarr );

		if ( isset($menu_order) )
			$menu_order = (int) $menu_order;
		else
			$menu_order = 0;

		if ( !isset($post_password) || 'private' == $post_status )
			$post_password = '';

		$post_name = wp_unique_post_slug($post_name, $post_ID, $post_status, $post_type, $post_parent);

		// expected_slashed (everything!)
		$data = compact( array( 'post_author', 'post_date', 'post_date_gmt', 'post_content', 'post_content_filtered', 'post_title', 'post_excerpt', 'post_status', 'post_type', 'comment_status', 'ping_status', 'post_password', 'post_name', 'to_ping', 'pinged', 'post_modified', 'post_modified_gmt', 'post_parent', 'menu_order', 'guid' ) );
		//$data = apply_filters('wp_insert_post_data', $data, $postarr);
		$data = wp_unslash( $data );
		$where = array( 'ID' => $post_ID );

		if ( $update ) {
			do_action( 'pre_post_update', $post_ID, $data );
			if ( false === $wpdb->update( $wpdb->posts, $data, $where ) ) {
				if ( $wp_error )
					return new WP_Error('db_update_error', __('Could not update post in the database'), $wpdb->last_error);
				else
					return 0;
			}
		} else {
			if ( isset($post_mime_type) )
				$data['post_mime_type'] = wp_unslash( $post_mime_type ); // This isn't in the update
			// If there is a suggested ID, use it if not already present
			if ( !empty($import_id) ) {
				$import_id = (int) $import_id;
				if ( ! $wpdb->get_var( $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE ID = %d", $import_id) ) ) {
					$data['ID'] = $import_id;
				}
			}
			if ( false === $wpdb->insert( $wpdb->posts, $data ) ) {
				if ( $wp_error )
					return new WP_Error('db_insert_error', __('Could not insert post into the database'), $wpdb->last_error);
				else
					return 0;
			}
			$post_ID = (int) $wpdb->insert_id;

			// use the newly generated $post_ID
			$where = array( 'ID' => $post_ID );
		}

		if ( empty($data['post_name']) && !in_array( $data['post_status'], array( 'draft', 'pending', 'auto-draft' ) ) ) {
			$data['post_name'] = sanitize_title($data['post_title'], $post_ID);
			$wpdb->update( $wpdb->posts, array( 'post_name' => $data['post_name'] ), $where );
		}

		if ( is_object_in_taxonomy($post_type, 'category') )
			wp_set_post_categories( $post_ID, $post_category );

		if ( isset( $tags_input ) && is_object_in_taxonomy($post_type, 'post_tag') )
			wp_set_post_tags( $post_ID, $tags_input );

		// new-style support for all custom taxonomies
		if ( !empty($tax_input) ) {
			foreach ( $tax_input as $taxonomy => $tags ) {
				$taxonomy_obj = get_taxonomy($taxonomy);
				if ( is_array($tags) ) // array = hierarchical, string = non-hierarchical.
					$tags = array_filter($tags);
				if ( current_user_can($taxonomy_obj->cap->assign_terms) )
					wp_set_post_terms( $post_ID, $tags, $taxonomy );
			}
		}

		$current_guid = get_post_field( 'guid', $post_ID );

		// Set GUID
		if ( !$update && '' == $current_guid )
			$wpdb->update( $wpdb->posts, array( 'guid' => get_permalink( $post_ID ) ), $where );

		clean_post_cache( $post_ID );

		$post = get_post($post_ID);

		if ( !empty($page_template) && 'page' == $data['post_type'] ) {
			$post->page_template = $page_template;
			$page_templates = wp_get_theme()->get_page_templates();
			if ( 'default' != $page_template && ! isset( $page_templates[ $page_template ] ) ) {
				if ( $wp_error )
					return new WP_Error('invalid_page_template', __('The page template is invalid.'));
				else
					return 0;
			}
			update_post_meta($post_ID, '_wp_page_template',  $page_template);
		}

		wp_transition_post_status($data['post_status'], $previous_status, $post);

		if ( $update ) {
			do_action('edit_post', $post_ID, $post);
			$post_after = get_post($post_ID);
			do_action( 'post_updated', $post_ID, $post_after, $post_before);
		}

		do_action( "save_post_{$post->post_type}", $post_ID, $post, $update );
		do_action( 'save_post', $post_ID, $post, $update );
		do_action( 'wp_insert_post', $post_ID, $post, $update );

		return $post_ID;
	}
	
	public static function postAttachListingImages($post_id, $listing)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		//Logger::log('Listing: ' . print_r($listing, true), Logger::LOG_TYPE_DEBUG);
		
		$paths_possible = array();
		
		if ($listing->image) {
			$paths_possible[] = CDN_ORIGIN_STORAGE . $listing->image;
		}
		if ($listing->thumbnail) {
			$paths_possible[] = CDN_ORIGIN_STORAGE . $listing->thumbnail;
		}
		
		if ($listing->images) {
			foreach ($listing->images as $image) {
				if ($image->image) {
					$paths_possible[] = CDN_ORIGIN_STORAGE . $image->image;
				}
			}
		}
		//Logger::log('Possible image  paths: ' . print_r($paths_possible, true), Logger::LOG_TYPE_DEBUG);
		
		$paths = array();
		foreach ($paths_possible as $path) {
			if (is_file($path)) {
				$paths[] = $path;
			}
		}
		//Logger::log('Actual image paths: ' . print_r($paths, true), Logger::LOG_TYPE_DEBUG);
		
		if (empty($paths)) {
			Logger::log('No listing images found', Logger::LOG_TYPE_DEBUG);
		} else {
			foreach ($paths as $path) {
				static::postAttachImage($path, $post_id);
			}
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	public static function postAttachImage($image_path, $post_id)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		Logger::log('Attaching image "' . $image_path . '" to post id "' . $post_id . '"', Logger::LOG_TYPE_DEBUG);
		
		$wp_upload_dir = wp_upload_dir();
		//Logger::log('Word press upload directory: ' . print_r($wp_upload_dir, true), Logger::LOG_TYPE_DEBUG);
		
		$filename = basename($image_path);
		$filepath = $wp_upload_dir['path'] . '/' . $filename;
		
		if (!is_file($filepath)) {
			// first copy image to wordpress upload directory
			copy($image_path, $filepath);
		} else {
			Logger::log('File "' . $filepath . '" already exists', Logger::LOG_TYPE_DEBUG);
		}
			
		$wp_filetype = wp_check_filetype($filename, null);
		$attachment = array(
		 'guid' => $wp_upload_dir['url'] . '/' . $filename, 
		 'post_mime_type' => $wp_filetype['type'],
		 'post_title' => preg_replace( '/\.[^.]+$/', '', $filename),
		 'post_content' => '',
		 'post_status' => 'inherit'
		);
		$attach_id = wp_insert_attachment($attachment, $filepath, $post_id);
		// you must first include the image.php file
		// for the function wp_generate_attachment_metadata() to work
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		$attach_data = wp_generate_attachment_metadata($attach_id, $filename);
		wp_update_attachment_metadata($attach_id, $attach_data);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}