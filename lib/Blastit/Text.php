<?php

class Blastit_Text
{
	public static function sanitizeInput($string)
	{
		$result = static::utf8Fix($string);
		return $result;
	}
	
	public static function sanitizeOutput($string)
	{
		$result = static::utf8Fix($string);
		return $result;
	}
	
	public static function utf8Fix($string)
	{
		if (!static::isUtf8($string)) {
			$mappings = array_merge(
				static::getCharMappingHexLatin1ToUtf8(),
				static::getCharMappingHexAnsiToUtf8()
			);
			$max = strlen($string);
			for($i = 0; $i < $max; $i++){
				$charIsUtf8 = true;
				for ($byte = 3; $byte > 0; $byte--) {
					$char = substr($string , $i, $byte + 1);
					if (static::isUtf8($char)) {
						$charIsUtf8 = true;
						$i += $byte;
						break;
					} else {
						$charIsUtf8 = false;
					}
				}
				if (!$charIsUtf8) {
					$char = substr($string , $i, 1);
					$charHex = static::bin2Hex($char);
					if (!empty($mappings[$charHex])) {
						$newChar = static::hex2bin($mappings[$charHex]);
						$newCharLength = strlen($newChar);
						$string = substr($string, 0, $i) . $newChar . substr($string, $i + 1);
						$i += $newCharLength;
						$max = strlen($string);
					}
				}
			}
		}
		
		return $string;
	}
	
	public static function bin2Hex($string)
	{
		$result = strtoupper(bin2hex($string));
		return $result;
	}
	
	public static function hex2bin($string) 
	{
		$hex2BinWs = " \t\n\r";
		$pos = 0;
		$result = '';
		while ($pos < strlen($string)) {
			if (strpos($hex2BinWs, $string{$pos}) !== false) {
				$pos++;
			} else {
				$code = hexdec(substr($string, $pos, 2));
				$pos = $pos + 2;
				$result .= chr($code); 
			}
		}
		return $result;
	}
	
	public static function isUtf8($string)
	{
		$regexPatternIsUtf8 = '%^(?:
			  [\x0-\x7F]           				 # ASCII
			| [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
			| \xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs
			| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
			| \xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates
			| \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
			| [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
			| \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
		)*$%xs';
		
		return preg_match($regexPatternIsUtf8, $string);
	}
	
	private static function getCharMappingHexLatin1ToUtf8()
	{
		$hex = array(
			'A0' => 'C2A0', 'A1' => 'C2A1', 'A2' => 'C2A2', 'A3' => 'C2A3', 
			'A4' => 'C2A4', 'A5' => 'C2A5', 'A6' => 'C2A6', 'A7' => 'C2A7', 
			'A8' => 'C2A8', 'A9' => 'C2A9', 'AA' => 'C2AA', 'AB' => 'C2AB', 
			'AC' => 'C2AC', 'AD' => 'C2AD', 'AE' => 'C2AE', 'AF' => 'C2AF',
						
			'B0' => 'C2B0', 'B1' => 'C2B1', 'B2' => 'C2B2', 'B3' => 'C2B3',
			'B4' => 'C2B4', 'B5' => 'C2B5', 'B6' => 'C2B6', 'B7' => 'C2B7',
			'B8' => 'C2B8', 'B9' => 'C2B9', 'BA' => 'C2BA', 'BB' => 'C2BB',
			'BC' => 'C2BC', 'BD' => 'C2BD', 'BE' => 'C2BE', 'BF' => 'C2BF',
						
			'C0' => 'C380', 'C1' => 'C381', 'C2' => 'C382', 'C3' => 'C383',
			'C4' => 'C384', 'C5' => 'C385', 'C6' => 'C386', 'C7' => 'C387',
			'C8' => 'C388', 'C9' => 'C389', 'CA' => 'C38A', 'CB' => 'C38B',
			'CC' => 'C38C', 'CD' => 'C38D', 'CE' => 'C38E', 'CF' => 'C38F',
						
			'D0' => 'C390', 'D1' => 'C391', 'D2' => 'C392', 'D3' => 'C393',
			'D4' => 'C394', 'D5' => 'C395', 'D6' => 'C396', 'D7' => 'C397',
			'D8' => 'C398', 'D9' => 'C399', 'DA' => 'C39A', 'DB' => 'C39B',
			'DC' => 'C39C', 'DD' => 'C39D', 'DE' => 'C39E', 'DF' => 'C39F',
				
			'E0' => 'C3A0', 'E1' => 'C3A1', 'E2' => 'C3A2', 'E3' => 'C3A3',
			'E4' => 'C3A4', 'E5' => 'C3A5', 'E6' => 'C3A6', 'E7' => 'C3A7',
			'E8' => 'C3A8', 'E9' => 'C3A9', 'EA' => 'C3AA', 'EB' => 'C3AB',
			'EC' => 'C3AC', 'ED' => 'C3AD', 'EE' => 'C3AE', 'EF' => 'C3AF',
			
			'F0' => 'C3B0', 'F1' => 'C3B1', 'F2' => 'C3B2', 'F3' => 'C3B3',
			'F4' => 'C3B4', 'F5' => 'C3B5', 'F6' => 'C3B6', 'F7' => 'C3B7',
			'F8' => 'C3B8', 'F9' => 'C3B9', 'FA' => 'C3BA',
			'FB' => 'C3BB', 
			'FC' => 'C3BC', 
			'FD' => 'C3BD',
			'FE' => 'C3BE',
			'FF' => 'C3BF'
		);
		
		return $hex;
	}
    
    private static function getCharMappingHexAnsiToUtf8()
    {
		$hex = array(
			'80' => 'E282AC',
			'82' => 'E2809A',
			'83' => 'C692',
			'84' => 'E2809E',
			'85' => 'E280A6',
			'86' => 'E280A0',
			'87' => 'E280A1',
			'88' => 'CB86',
			'89' => 'E280B0',
			'8A' => 'C5A0',
			'8B' => 'E280B9',
			'8C' => 'C592',
			'8E' => 'C5BD',
			'91' => 'E28098',
			'92' => 'E28099',
			'93' => 'E2809C',
			'94' => 'E2809D',
			'95' => 'E280A2',
			'96' => 'E28093',
			'97' => 'E28094',
			'98' => 'CB9C',
			'99' => 'E284A2',
			'9A' => 'C5A1',
			'9B' => 'E280BA',
			'9C' => 'C593',
			'9E' => 'C5bE',
			'9F' => 'C5B8'
		);
		
		return $hex;
	}
}