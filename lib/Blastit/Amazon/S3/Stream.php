<?php

class Blastit_Amazon_S3_Stream extends Zend_Service_Amazon_S3_Stream {

	private $_bucketList = array();

    public function stream_open($path, $mode, $options, $opened_path)
    {
		try {
			return parent::stream_open($path, $mode, $options, $opened_path);
		} catch (Exception $e) {
			// retry once if previous attempt failed
			sleep(1);
			return parent::stream_open($path, $mode, $options, $opened_path);
		}
	}
    /**
     * Attempt to open a directory
     *
     * Improvement on Zend_Service_Amazon_S3_Stream::dir_opendir();
     * 
     * @param  string $path
     * @param  integer $options
     * @return boolean
     */

    public function dir_opendir($path, $options)
    {

		try {
			$cache = Zend_Registry::get( 'cachemanager' )->getCache('data');
		} catch (Exception $e) {
			$cache = false;
		}
		
		$cache_key = preg_replace('/[^a-zA-Z0-9_]/', '_', $path);

		if ($cache && $list = $cache->load($cache_key)) {
			$this->_bucketList = $list;
			return true;
		}
		
        if (preg_match('@^([a-z0-9+.]|-)+://$@', $path)) {
            $this->_bucketList = $this->_getS3Client($path)->getBuckets();
        }
        else {
            $host = parse_url($path, PHP_URL_HOST);
            // format $dir so it works with S3 bucket
            $dir  = trim(parse_url($path, PHP_URL_PATH), '/') . '/';
            $this->_bucketList = $this->_getS3Client($path)->getObjectsByBucket($host, array( 'delimiter' => '/', 'prefix' => $dir ));

            // use basenames only so that values look like you would normally expect with readdir()
            if (is_array($this->_bucketList)) {
				for ($i = 0; $i < count($this->_bucketList); $i++) {
					$this->_bucketList[$i] = basename($this->_bucketList[$i]);
				}
            }
        }
        
        if ($cache) $cache->save($this->_bucketList, $cache_key);

        return ($this->_bucketList !== false);
    }

    /**
     * Return the next filename in the directory
     *
     * No changes from inherited. Included because we can't call Zend_Amazon_S3_Stream::dir_readdir -- it would use
     * the Zend_Amazon_S3_Stream's private $_bucketList instead of ours
	 *
     * @return string
     */
    public function dir_readdir()
    {
        $object = current($this->_bucketList);
        if ($object !== false) {
            next($this->_bucketList);
        }
        return $object;
    }

    /**
     * Reset the directory pointer
     *
     * No changes from inherited. Included because we can't call Zend_Amazon_S3_Stream::dir_readdir -- it would use
     * the Zend_Amazon_S3_Stream's private $_bucketList instead of ours
     *
     * @return boolean True
     */
    public function dir_rewinddir()
    {
        reset($this->_bucketList);
        return true;
    }

    /**
     * Close a directory
     *
     * No changes from inherited. Included because we can't call Zend_Amazon_S3_Stream::dir_readdir -- it would use
     * the Zend_Amazon_S3_Stream's private $_bucketList instead of ours
     *
     * @return boolean True
     */
    public function dir_closedir()
    {
        $this->_bucketList = array();
        return true;
    }
    
    /**
     * Stat() wrapper
     *
     * Contains workaround for the fact that Zend_Service_Amazon_S3_Stream::url_stat() returns true even if file doesn't exist
     * if $info['mode'] is the only nonzero value, call $path nonexistent
     *
     * @return array or boolean false
     */

    public function url_stat($path, $flags) {
		$info = parent::url_stat($path, $flags);
		$has = array();
		foreach ($info as $key => $val) {
			if ($key != 'mode' && $val != 0) $has[] = $key;
		}
		if (!count($has)) return false;
		return $info;
	}
	
    /**
     * Create a new directory
     *
     * @param  string  $path
     * @param  integer $mode
     * @param  integer $options
     * @return boolean
     */
     
    public function mkdir($path, $mode, $options)
    {
    	$path = parse_url($path, PHP_URL_PATH);
    	if (strlen($path) > 1) return true;
        return $this->_getS3Client($path)->createBucket(parse_url($path, PHP_URL_HOST));
    }

    /**
     * Remove a directory
     *
     * @param  string  $path
     * @param  integer $options
     * @return boolean
     */
    public function rmdir($path, $options)
    {
    	$path = parse_url($path, PHP_URL_PATH);
    	if (strlen($path) > 1) return true;
        return $this->_getS3Client($path)->removeBucket(parse_url($path, PHP_URL_HOST));
    }

    /**
     * Attempt to rename the item
     *
     * @param  string  $path_from
     * @param  string  $path_to
     * @return boolean False
     */
    public function rename($path_from, $path_to)
    {
    	$path_from_path = parse_url($path_to, PHP_URL_HOST) . parse_url($path_from, PHP_URL_PATH);
    	$path_to_path   = parse_url($path_to, PHP_URL_HOST) . parse_url($path_to, PHP_URL_PATH);
		return $this->_getS3Client($path_from)->moveObject($path_from_path, $path_to_path);
    }


}
