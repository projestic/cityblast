<?php

class Blastit_Amazon_S3 extends Zend_Service_Amazon_S3 {

    public function putObject($object, $data, $meta=null) {
    	if ($meta == null) $meta = array(Zend_Service_Amazon_S3::S3_ACL_HEADER => Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ);
		try {
			$result = parent::putObject($object, $data, $meta);
		} catch (Exception $e) {
			// retry once if previous attempt failed
			sleep(1);
			$result = parent::putObject($object, $data, $meta);
		}
		return $result;
	}
	
    public function registerStreamWrapper($name='s3') {
        stream_register_wrapper($name, 'Blastit_Amazon_S3_Stream');
        $this->registerAsClient($name);
    }
    
    public function _makeRequest($method, $path='', $params=null, $headers=array(), $data=null)
    {
    	$path = str_replace(' ', '%20', $path);
    	return parent::_makeRequest($method, $path, $params, $headers, $data);
    }

}
