<?php

class Blastit_CityMatcher
{

	public $city;
	public $location;
	public $state;
	public $country;
	
	private $location_parts;
	private $location_string;

	public function fuzzyMatchLocation($location, $member = null) {
		
		$this->location_string = $location;
		$this->location_parts = preg_split('/[, ]/', $location);

		$this->location_parts = array_filter($this->location_parts, function ($var) { return (bool) strlen(trim($var)); });

		$search_parts = array();
	
		foreach ($this->location_parts as $part) {
			
			$search_parts[] = $part;
			
			if (count($search_parts) < 2 && count($this->location_parts) >= 2) continue;
			
			$city_parts = array();

			foreach ($search_parts as $part2) {
			
				$city_parts[] = $part2;
				$city_candidates = City::all( array('conditions' => array('LOWER(name) = ?', strtolower(implode(' ', $city_parts)) )) );
				
				if (count($city_candidates)) {
				
					// make state name out of parts that aren't part of found city
				
					$state = trim(str_replace(implode(' ', $city_parts), '', implode(' ', $search_parts)));
					
					if (strlen($state) == 0) {
						// must be searching by city only.
						return $city_candidates[0];
					}
					
					$state_candidates = State::all( array('conditions' => array('LOWER(name) = ? OR LOWER(code) = ?', $state, $state )) );
					
					if (count($state_candidates)) {
						
						// Found city and state at this point, match them up
						
						foreach ($city_candidates as $c) {
							foreach ($state_candidates as $s) {
								if ($c->state == $s->name) {
									return $c;
								}
							}
						}
						
					}
					
				}
				
			}

		}
	
		if ($member instanceOf Member && $member->default_city) {
			return $member->default_city;
		}
	
		return null;

	}

}
