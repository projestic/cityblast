<?
	class Validator
	{
		
		public $errors;
		public $fields = array();

		// constructor not implemented
		public function __construct(){}

		// Registration validation
		public function clientfinder($params)
		{
			array_walk($params, 'trim_value');


			if(!$this->isAlpha($params['fname'])){
				$this->errors .= "<li>First name is a required field</li>";
				array_push($this->fields, "fname");
			}
			
			if($this->isEmpty($params['lname'])){
				$this->errors .= "<li>Last name is a required field</li>";
				array_push($this->fields, "lname");
				
				echo "Im EMPTY!</br>";	
			}
			else
			{
				echo "Im ok!</br>";	
			}
			
			if($this->isEmpty($params['address'])){

				$this->errors .= "<li>Address is a required field</li>";
				array_push($this->fields, "address");
			}
			
			if($this->isEmpty($params['city'])){
				$this->errors .= "<li>City is a required field</li>";
				array_push($this->fields, "city");
			}
			
			if($this->isEmpty($params['state'])){
				$this->errors .= "<li>State is a required field</li>";
				array_push($this->fields, "state");
			}
			
			if($this->isEmpty($params['cc_country'])){
				$this->errors .= "<li>Country is a required field</li>";
				array_push($this->fields, "cc_country");
			}
			
			if($this->isEmpty($params['zip'])){
				$this->errors .= "<li>Zip/postal code is not valid</li>";
				array_push($this->fields, "zip");
			}
			else{
				if($params['cc_country']=="US"){
					if(!$this->isZip($params['zip'])){
						$this->errors .= "<li>Postal code is not valid</li>";
						array_push($this->fields, "zip");
					}
				}
				elseif($params['cc_country']=="CA"){
					if(!$this->isPostal($params['zip'])){
						$this->errors .= "<li>Postal code is not valid</li>";
						array_push($this->fields, "zip");
					}
				}
			}
			
			if($this->isEmpty($params['cc_type'])){
				$this->errors .= "<li>Card type is a required field</li>";
				array_push($this->fields, "cc_type");
			}
			
			if($this->isEmpty($params['cc_number'])){
				$this->errors .= "<li>Card number is a required field</li>";
				array_push($this->fields, "cc_number");
			}
			else{
				if(!$this->isCC($params['cc_number'])){
					$this->errors .= "<li>Card number is not valid</li>";
					array_push($this->fields, "cc_number");
				}
			}
			
			if($this->isEmpty($params['cv2'])){
				$this->errors .= "<li>CVV2 is a required field</li>";
				array_push($this->fields, "cv2");
			}
			
			if($this->isEmpty($params['exp_month'])){
				$this->errors .= "<li>Expiry month is a required field</li>";
				array_push($this->fields, "exp_month");
			}
			
			if($this->isEmpty($params['exp_year'])){
				$this->errors .= "<li>Expiry year is a required field</li>";
				array_push($this->fields, "exp_year");
			}
				
		}


		// Registration validation
		public function listing($params)
		{
			array_walk($params, 'trim_value');
			
			if(!$this->isAlpha($params['fname'])){
				$this->errors .= "<li>First name is a required field</li>";
				array_push($this->fields, "fname");
			}
			
			if($this->isEmpty($params['lname'])){
				$this->errors .= "<li>Last name is a required field</li>";
				array_push($this->fields, "lname");
			}
			
			if($this->isEmpty($params['address'])){

				$this->errors .= "<li>Address is a required field</li>";
				array_push($this->fields, "address");
			}
			
			if($this->isEmpty($params['city'])){
				$this->errors .= "<li>City is a required field</li>";
				array_push($this->fields, "city");
			}
			
			if($this->isEmpty($params['state'])){
				$this->errors .= "<li>State is a required field</li>";
				array_push($this->fields, "state");
			}
			
			if($this->isEmpty($params['cc_country'])){
				$this->errors .= "<li>Country is a required field</li>";
				array_push($this->fields, "cc_country");
			}
			
			if($this->isEmpty($params['zip'])){
				$this->errors .= "<li>Zip/postal code is not valid</li>";
				array_push($this->fields, "zip");
			}
			else{
				if($params['cc_country']=="US"){
					if(!$this->isZip($params['zip'])){
						$this->errors .= "<li>Postal code is not valid</li>";
						array_push($this->fields, "zip");
					}
				}
				elseif($params['cc_country']=="CA"){
					if(!$this->isPostal($params['zip'])){
						$this->errors .= "<li>Postal code is not valid</li>";
						array_push($this->fields, "zip");
					}
				}
			}
			
			if($this->isEmpty($params['cc_type'])){
				$this->errors .= "<li>Card type is a required field</li>";
				array_push($this->fields, "cc_type");
			}
			
			if($this->isEmpty($params['cc_number'])){
				$this->errors .= "<li>Card number is a required field</li>";
				array_push($this->fields, "cc_number");
			}
			else{
				if(!$this->isCC($params['cc_number'])){
					$this->errors .= "<li>Card number is not valid</li>";
					array_push($this->fields, "cc_number");
				}
			}
			
			if($this->isEmpty($params['cv2'])){
				$this->errors .= "<li>CVV2 is a required field</li>";
				array_push($this->fields, "cv2");
			}
			
			if($this->isEmpty($params['exp_month'])){
				$this->errors .= "<li>Expiry month is a required field</li>";
				array_push($this->fields, "exp_month");
			}
			
			if($this->isEmpty($params['exp_year'])){
				$this->errors .= "<li>Expiry year is a required field</li>";
				array_push($this->fields, "exp_year");
			}
				
		}

		// validate empties
		public function isEmpty($value)
		{
			return (strlen($value)==0);
		}

		// validate length
		public function isLength($value, $min)
		{
			return (strlen($value)>=$min);
		}

		// validate matching values
		public function isMatch($value1, $value2)
		{
			return ($value1===$value2);
		}

		// validate integer
		public function isInt($value, $min, $max)
		{
			return filter_var($value, FILTER_VALIDATE_INT, array('options' => array('min_range' => $min, 'max_range' => $max)));
		}

		// validate float number
		public function isFloat($value)
		{
			return filter_var($value, FILTER_VALIDATE_FLOAT);
		}
		
		// validate alphabetic value
		public function isAlpha($value)
		{
			return filter_var($value, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^[a-zA-Z]+$/")));
		}
		
		// validate alphanumeric value
		public function isAlphanum($value)
		{
			return filter_var($value, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^[a-zA-Z0-9 ]+$/")));
		}
		
		// validate phone number
		public function isPhone($value)
		{
			return filter_var($value, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}/")));
		}
		
		// validate zip code
		public function isZip($value)
		{
			return filter_var($value, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^([0-9]{5})(-[0-9]{4})?$/i")));
		}
		
		// validate postal code
		public function isPostal($value)
		{
			return filter_var($value, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^([ABCEGHJKLMNPRSTVXY][0-9][A-Z] ?[0-9][A-Z][0-9])*$/i")));
		}
		
		// validate URL
		public function isUrl($value)
		{
			return filter_var($value, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^((https?://)?([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.]*(\?\S+)?)?)*)/i")));
		}
		
		// contains URL
		public function containsUrl($value)
		{
			preg_match("@((https?://)?([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.]*(\?\S+)?)?)*)@", $value)?true:false;
		}
		
		// validate IP address
		public function isIp($value)
		{
			return filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
		} 
		
		// validate email address
		public function isEmail($value)
		{
			return filter_var($value, FILTER_VALIDATE_EMAIL);
		}
		
		// validate credit card number
		public function isCC($value)
		{
			// Is it formatted correctly
			return filter_var($value, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6011[0-9]{12}|622((12[6-9]|1[3-9][0-9])|([2-8][0-9][0-9])|(9(([0-1][0-9])|(2[0-5]))))[0-9]{10}|64[4-9][0-9]{13}|65[0-9]{14}|3(?:0[0-5]|[68][0-9])[0-9]{11}|3[47][0-9]{13})*$/")));
		}

	}
?> 