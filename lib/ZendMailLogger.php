<?php

class ZendMailLogger extends Zend_Mail {

	// Email type
	private $type   =	'';
	private $member_id	=   0;

	public function  __construct($type, $member_id=0) {
		parent::__construct('UTF-8');
		$this->type	=   $type;
		$this->member_id=   $member_id;
	}

	public function log()
	{
		$log				=   new EmailLog();
		$log->type		=   $this->type;
		$log->member_id	=   $this->member_id;
		$log->email		=   implode(",",$this->getRecipients());
		$log->subject		=   $this->getSubject();
		$log->save();
	}

	public function sendemail()
	{
		$this->log();
		return $this->send();
	}
}

?>