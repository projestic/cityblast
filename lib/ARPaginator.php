<?php
/**
 * Paginator Adapter
 *
 * pass "count_distinct_by" => "table_name.column_name" for avoid redundant count
 */

class ARPaginator implements Zend_Paginator_Adapter_Interface {

	protected $options			= array();
	private $count_distinct_by	= null;
	public $table_class			= null;
	public $error				= null;


	public function __construct($table, $options = array())
	{
		/**
		 * phpaciverecord has bug, if self::$options is not null for an many-2-many, then it produces error for Eager loading
		 * 
		 * we should avoid Eager loading for a set of recordset
		 */
		if(!is_array($options))
		{
			$this->conditions	= array( $options );

			$options	= null;
		}
		elseif (isset($options['conditions']))
		{
			$this->conditions	= is_array($options['conditions']) ? $options['conditions'] : array( $options['conditions'] );
			
			unset($options['conditions']);
		}
		else
		{
			$this->conditions	= $options;

			$options	= null;
		}

		if (is_array($options))
		{
			/**
			 * allow DISTINCT option for counting recordset
			 */
			if (isset($options['count_distinct_by']))
			{
				if ($options['count_distinct_by'])
				{
					$this->count_distinct_by = $options['count_distinct_by'];
				}

				unset($options['count_distinct_by']);
			}

			if(!$options)
			{
				$options	= null;
			}

		}

		$this->options	= $options;

		/**
		 * @abstract ActiveRecord\Model
		 */
		$this->table_class= $table;

		/**
		 * @var ActiveRecord\Model
		 */
		$this->table	= new $table;

	}

	public function getItems($offset, $itemCountPerPage)
	{
		$options	= $this->options;

		$options['limit']	= $itemCountPerPage;
		$options['offset']	= $offset;
		$options['conditions']	= $this->conditions;

		return $this->table->find('all', $options);
	}

	public function count()
	{
		$options = $this->options;

		$options['conditions']	= $this->conditions;

		if (isset($options['order']))
		{
			unset($options['order']);
		}

		if (isset($options['group']) && !isset($options['having']))
		{
			unset($options['group']);
		}

		if ($this->count_distinct_by)
		{
			$vars = get_class_vars($this->table_class);

			$table_name	= isset($vars['table_name']) ? $vars['table_name'] : null;
			$table_name	= isset($vars['table']) && !$table_name ? $vars['table'] : $table_name;

			$sql	= "Select COUNT(DISTINCT ". $this->count_distinct_by .") AS n
						FROM ". $table_name ." ";

			if (isset($options['joins']) && $options['joins'] && is_array($options['joins']))
			{
				$sql	.= implode(" ", $options['joins']);
			}

			if (isset($options['conditions']) && $options['conditions'])
			{
				$sql	.= " WHERE ". (is_array($options['conditions']) ? implode(" AND ", $options['conditions']) : $options['conditions'] );
			}

			if (isset($options['group']) && $options['group'])
			{
				$sql	.= " GROUP BY ". $options['group'];
			}

			if (isset($options['having']) && $options['having'])
			{
				$sql	.= " HAVING ". (is_array($options['having']) ? implode(" AND ", $options['having']) : $options['having'] );
			}

			$result	= $this->table->find_by_sql($sql);

			try
			{
				if ($result && is_array($result))
				{
					return $result[0]->n;
				}
				else
				{
					return 0;
				}
			}
			catch (Exception $e)
			{
				$this->error	= "Error at line ". $e->getLine() ." of file ". $e->getFile() ." <br />\r\n". $e->getTraceAsString();
			}
			return 0;
		}

		return $this->table->count($options);
	}
}

/*
class ARPaginator implements Zend_Paginator_Adapter_Interface {


	public function __construct($table,$conditions = array())
	{
		if(!is_array($conditions))

			$conditions = array( $conditions );

		$this->conditions = $conditions;

		$this->table	= new $table;

	}

	public function getItems($offset, $itemCountPerPage)
	{
		return $this->table->find('all', array('limit' => $itemCountPerPage, 'offset' => $offset, 'conditions' => $this->conditions));
	}

	public function count()
	{
		return $this->table->count(array('conditions' => $this->conditions));
	}
}
*/
?>
