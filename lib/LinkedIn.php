<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once dirname(__FILE__).'/../lib/OAuth.php';

class LinkedIn 
{
    private $config;
    private $signatureMethod;
    private $consumer;

    public	$profileFields = array(
							'id',
							'first-name',
							'last-name',
							'picture-url',
							'public-profile-url',
							'headline',
							'current-status',
							'location',
							'distance',
							'summary',
							'industry',
							'specialties',
							'positions',
							'educations',
							'num-connections'
			);
    
    
	public function __construct($member = null)
	{

//		define('BASE_API_URL', 'https://api.linkedin.com');
//		define('REQUEST_PATH', '/uas/oauth/requestToken?scope=r_fullprofile+r_emailaddress+rw_nus+r_network+r_contactinfo');
//		define('AUTH_PATH', '/uas/oauth/authorize');
//		define('ACC_PATH', '/uas/oauth/accessToken');
		
		// Constants disabled by Shane -- generating errors and not used anywhere.
		
		// define('BASE_API_URL', 'https://api.linkedin.com');
		// define('REQUEST_PATH', '/uas/oauth/requestToken?scope=r_fullprofile+r_emailaddress+rw_nus+r_network+r_contactinfo');
		// define('AUTH_PATH', '/uas/oauth/authorize');
		// define('ACC_PATH', '/uas/oauth/accessToken');
		
		$network_app_linkedin = Zend_Registry::get('networkAppLinkedIn');

		$configuration = array(
			'version' => '1.0',
			'localUrl' => APP_URL . '/linkedin',
			'requestTokenUrl' => 'https://api.linkedin.com/uas/oauth/requestToken',
			'userAuthorizationUrl' => 'https://api.linkedin.com/uas/oauth/authorize',
			'accessTokenUrl' => 'https://api.linkedin.com/uas/oauth/accessToken',
			'callbackUrl' => APP_URL . '/linkedin/callback',     //the callback url your app have registered with twitter
			'consumerKey' => $network_app_linkedin->consumer_id,   	// the consumer key provided from twitter
			'consumerSecret' => $network_app_linkedin->consumer_secret     // the secret key provided from twitter
		);	

		

		
		$this->config	=   $configuration;
		$this->signatureMethod = new OAuthSignatureMethod_HMAC_SHA1();
		$this->consumer = new Zend_Oauth_Consumer( $configuration );

		
	}

	public function getConsumer()
	{
		return $this->consumer;
	}

	private function get_curl_response($toHeader, $url, $method='POST', $data=null)
	{
		$ch = curl_init();
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
	
		switch($method) {
			case 'DELETE':
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
			break;
			case 'POST':
			case 'PUT':
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
			break;
		}
	
		// check if we are sending data to LinkedIn
		if(is_null($data)) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array($toHeader));
	
		} else {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array($toHeader, "Content-Type: text/xml;charset=utf-8"));
	
		}
	
	
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
	
		$status	    =	$info['http_code'];
		curl_close($ch);
	
		return array('status' => $status, 'response' => $output);
	}

    	public function getProfile($oauth_token, $oauth_secret)
	{

		$params = ":(".join(',', $this->profileFields).")";
		$url = "http://api.linkedin.com/v1/people/~".$params;

		$token = new OAuthConsumer($oauth_token, $oauth_secret, 1);

		$profileObj = OAuthRequest::from_consumer_and_token($this->consumer, $token, "GET", $url, array());
		$profileObj->sign_request($this->signatureMethod, $this->consumer, $token);
		$toHeader = $profileObj->to_header();
		$data	=   $this->get_curl_response($toHeader, $url, 'GET');
		return $this->parse_xml( $data['response']);
	}


	private function parse_xml($data) {
		$profileXML = simplexml_load_string($data);


		if(isset($profileXML->{'id'}))
			$profile['id'] = (string)$profileXML->{'id'};

		if(isset($profileXML->{'first-name'}))
			$profile['firstname'] = (string)$profileXML->{'first-name'};

		if(isset($profileXML->{'last-name'}))
			$profile['lastname'] = (string)$profileXML->{'last-name'};

		if(isset($profileXML->{'picture-url'}))
			$profile['pictureURL'] = (string)$profileXML->{'picture-url'};

		if(isset($profileXML->{'public-profile-url'}))
			$profile['publicURL'] = (string)$profileXML->{'public-profile-url'};

		if(isset($profileXML->headline))
			$profile['headline'] = (string)$profileXML->headline;

		if(isset($profileXML->{'current-status'}))
			$profile['currentStatus'] = (string)$profileXML->{'current-status'};

		if(isset($profileXML->location->name))
			$profile['locationName'] = (string)$profileXML->location->name;

		if(isset($profileXML->location->country->code))
			$profile['locationCountryCode'] = (string)$profileXML->location->country->code;

		if(isset($profileXML->distance))
			$profile['distance'] = (string)$profileXML->distance;

		if(isset($profileXML->{'summary'}))
			$profile['summary'] = (string)$profileXML->{'summary'};

		if(isset($profileXML->industry))
			$profile['industry'] = (string)$profileXML->industry;

		if(isset($profileXML->{'num-connections'}))
			$profile['num_connections'] = (int)$profileXML->{'num-connections'};

		return $profile;
	}

	public function getConnectionsCount($oauth_token, $oauth_secret)
	{
		$profile	=   $this->getProfile($oauth_token, $oauth_secret) ;
		return (int)$profile['num_connections'];
	}

	/**
	 * Method to get all connections list
	 * @param <type> $oauth_token
	 * @param <type> $oauth_secret
	 * @return <type>
	 */
	public function getConnections($oauth_token, $oauth_secret)
	{
	    $params = ":(first-name,last-name,id)";
	    $url = "http://api.linkedin.com/v1/people/~/connections".$params;

	    $token = new OAuthConsumer($oauth_token, $oauth_secret, 1);

	    $profileObj = OAuthRequest::from_consumer_and_token($this->consumer, $token, "GET", $url, array());
	    $profileObj->sign_request($this->signatureMethod, $this->consumer, $token);
	    $toHeader = $profileObj->to_header();
	    $data	=   $this->get_curl_response($toHeader, $url, 'GET');
	    return $this->parse_connections_xml($data['response']);
	}

	private function parse_connections_xml($data) {
		$profileXML = simplexml_load_string($data);

		$connections	=   array();
		foreach($profileXML->person as $person) {
		    $profile	=   array();
		    if(isset($person->{'id'}))
			    $profile['id'] = (string)$person->{'id'};

		    if(isset($person->{'first-name'}))
			    $profile['firstname'] = (string)$person->{'first-name'};

		    if(isset($person->{'last-name'}))
			    $profile['lastname'] = (string)$person->{'last-name'};

			    $connections[]  =	$profile;
		}
		return $connections;
	}
	/**
	 * XML string structure to perform a status update on LinkedIn
	 * @param String $status_text
	 * @param String $status_link
	 * @param String $status_image
	 * @return String
	 */
	private function getUpdateStatusXml($status_text, $link_title, $status_link='', $status_image='')
	{
	    $status_message =	'<?xml version="1.0" encoding="UTF-8"?>
<share>
  <comment>'.$status_text.'</comment>
  <content>
     <title>'.$link_title.'</title>
     <submitted-url>'.$status_link.'</submitted-url>
  	<submitted-image-url>'.$status_image.'</submitted-image-url>     
  </content>

  <visibility>
     <code>anyone</code>
  </visibility>
</share>';

	return ($status_message);
	}

	/**
	 * Method to send a status update to Linkedin
	 * @param String $oauth_token
	 * @param String $oauth_secret
	 * @param String $text
	 * @param String $link
	 * @param String $image
	 * @return Boolean $status
	 */
	public function statusUpdate($oauth_token, $oauth_secret, $text, $link='', $link_title, $image='')
	{
		$status_url	=   "http://api.linkedin.com/v1/people/~/shares";
		$message	=   $this->getUpdateStatusXml($text, $link_title, $link, $image);
	
		$token = new OAuthConsumer($oauth_token, $oauth_secret, 1);
		$profileObj = OAuthRequest::from_consumer_and_token($this->consumer, $token, "POST", $status_url, array());
		$profileObj->sign_request($this->signatureMethod, $this->consumer, $token);
		$toHeader = $profileObj->to_header();


	
		$log	    =   date("Y-m-d")."\n";
		try
		{
			$update = $this->get_curl_response($toHeader, $status_url, 'POST', $message);
		} 
		catch(Exception $e)
		{
			$log	   .=	$e->getMessage()."\n";
			$log	   .=	"---------------------\n\n";
		}
	
		if($update['status'] != 201)
		{
			$log	.= $update['response']."\nText:\n".$text."\n\n";
		}
	
		file_put_contents(dirname(__FILE__)."/../log/linkedin.status.log", $log, FILE_APPEND);

	
		return $update['status'] == 201;
	}
}
?>
