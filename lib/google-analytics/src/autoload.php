<?php

spl_autoload_register(function($className) {
	if($className[0] == '\\') {
		$className = substr($className, 1);
	}
	
	// Leave if class should not be handled by this autoloader
	if(strpos($className, 'UnitedPrototype\\GoogleAnalytics') !== 0) return;
	
	$classPath = strtr(substr($className, strlen('UnitedPrototype')), '\\', '/') . '.php';


	$tmp_path = APPLICATION_PATH . "/../lib/google-analytics/src";

	echo __DIR__ . $classPath;
	echo "<BR><BR>";
	
	
	//echo __DIR__ . "<BR>";
	
	//exit();
	
	require($tmp_path . $classPath);
});

?>
