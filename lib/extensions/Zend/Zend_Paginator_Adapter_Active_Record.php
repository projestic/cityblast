<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Paginator
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Array.php 10013 2008-07-09 21:08:06Z norm2782 $
 */

/**
 * @see Zend_Paginator_Adapter_Interface
 */
require_once 'Zend/Paginator/Adapter/Interface.php';

/**
 * @category   Zend
 * @package    Zend_Paginator
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Zend_Paginator_Adapter_Active_Record implements Zend_Paginator_Adapter_Interface
{
  /**
   * String
   * 
   * @var ActiveRecord\Model
   */
  protected $_model = null;

  /**
   * Finder Options
   *
   * @var array
   */
  protected $_options = null;
  
  /**
   * Item count
   *
   * @var integer
   */
  protected $_count = null;

  /**
   * Constructor.
   * 
   * @param String $model Model to paginate
   * @param Array $options Model finder options (without limit)
   */
  public function __construct($model, $options)
  {
    $this->_model = $model;
    $this->_count = $model::count($options);
    $this->_options = $options;
  }

  /**
   * Returns a collection of items for a page.
   *
   * @param  integer $offset Page offset
   * @param  integer $itemCountPerPage Number of items per page
   * @return ActiveRecord\Model collection
   */
  public function getItems($offset, $itemCountPerPage)
  {
    return call_user_func(array($this->_model, 'all'), 
      $this->_options + array('limit' => $itemCountPerPage, 'offset' => $offset)
    );
  }

  /**
   * Returns the total number of rows in the array.
   *
   * @return integer
   */
  public function count()
  {
    return $this->_count;
  }
  
  /**
   * Returns the page number an object would lie in on a set paginator.
   * Depends on TRACK_PAGE_COUNT as the page is set on the paginator not the adapter so we don't have it
   *
   * Depends on order by being timestamp desc, id desc
   *
   * @return integer
   */
  public function getPageFromID($object)
  {
    $connection = call_user_func(array($this->_model, 'connection'));
    $table = call_user_func(array($this->_model, 'table_name'));
    
    $vars = array(db_date($object->timestamp));
    
    #
    # We might be able to cheat and just do timestamp > ? and id > object->id
    # but there may be a case where the id's ore really out of sync, and we are only
    # using the id ordering as a timestamp tie break.
    #
    $count_ahead = $connection->query_and_fetch_one(
      "
  	    SELECT 
  	      count(*)
  	    FROM
  	      {$table}
  	    WHERE
  	      timestamp > ?
  	  ",
  	  $vars
	  );
	  
	  $count_equal = $connection->query_and_fetch_one(
      "
  	    SELECT 
  	      count(*)
  	    FROM
  	      {$table}
  	    WHERE
  	      timestamp = ?
  	  ",
  	  $vars
	  );
	  
	  if($count_equal > 1)
	  {
	    $vars = array(db_date($object->timestamp), $object->id);
	    $count_ahead += $connection->query_and_fetch_one(
        "
    	    SELECT 
    	      count(*)
    	    FROM
    	      {$table}
    	    WHERE
    	      timestamp = ?
    	      AND id > ?
    	  ",
    	  $vars
  	  );
	  }
	  
	  return (int)($count_ahead / TRACK_PAGE_COUNT) + 1;
	  
  }
}