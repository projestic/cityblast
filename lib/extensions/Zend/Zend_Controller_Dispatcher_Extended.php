<?

class Zend_Controller_Dispatcher_Extended extends Zend_Controller_Dispatcher_Standard
{
  public function formatActionName($unformatted)
  {  
    return preg_replace('/[^a-z0-9_]/', '', strtolower($unformatted));
  }
}


?>