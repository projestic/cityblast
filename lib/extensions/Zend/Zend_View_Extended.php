<?php

  class Zend_View_Extended extends Zend_View
  {
    
    /**
     * Determines if the current view is being rendered in a collection.
     */
    protected $_rendering_in_collection = false;
    
    /**
     * Processes a view script and returns the output.
     *
     * @param string $name The script script name to process.
     * @return string The script output.
     */
    public function render($name, $options = array())
    {
      if(isset($options['collection'])) 
        return $this->render_collection($name, $options['collection'], $options);
      
      return parent::render($name);
    }
    
    public function content_for($name, $content)
    {
      $content_property = $this->content_for_name($name);
      
      if(!isset($this->$content_property) || !is_array($this->$content_property))
        $this->$content_property = array();
      
      $this->{$content_property}[] = $content;
    }
    
    public function render_content_for($name)
    {
      $content_property = $this->content_for_name($name);
      
      if(!isset($this->$content_property) || !is_array($this->$content_property))
        return NULL;
      
      $self = $this; // so dumb
      
      return implode('', 
        array_map(function($chunk) use($self) { return ($chunk instanceOf Closure) ? $chunk($self) : $chunk; }, $this->$content_property)
      );
    }
    
     /**
      * Determines if the current view is being rendered in a collection.
      * @return bool
      */
    public function rendering_in_collection()
    {
      return $this->_rendering_in_collection;
    }
    
    private function render_collection($name, $collection, $options = array())
    {
      $content = array();
      
      /* 
        Just having spacer instead of spacer_template, if you need
        a spacer template, just supply the result of a render
      */
      $spacer = isset($options['spacer']) ? $options['spacer'] : '';
      
      if(!preg_match('/^(.*\/)?([^\.]+)\.(.*)$/', $name, $matches))
        throw new Exception("Could not introspect view partial name from '{$name}'");
      
      $variable_name = $matches[2];
      $this->counter = 0;
      $this->_rendering_in_collection = true;

      foreach($collection as $item) {
        $this->counter++;
        $this->$variable_name = $item;
        $content[] = parent::render($name);
      }
      
      // cleanup
      unset($this->$variable_name);
      unset($this->counter);
      $this->_rendering_in_collection = false;
      
      return implode($spacer, $content);
    }
    
    private function content_for_name($name)
    {
      return "content_for_$name";
    }
  }
?>