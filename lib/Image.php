<?php

class Image
{

	protected $streamMode = false;
	protected $tmpFile    = null;
	
	public function setStreamMode($bool) {
		$this->streamMode = $bool;
	}

	public function resizeListingImage($image, $onlyIfMissing = false) 
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		if (stristr($image, '://')) {
			$this->setStreamMode(true);
		}
	
		$files = array();
	
		try {
			$files[] = $this->createSizedVersion($image, 658, 0, false, $onlyIfMissing);
			$files[] = $this->createSizedVersion($image, 280, 250, true, $onlyIfMissing);
			$files[] = $this->createSizedVersion($image, 276, 182, true, $onlyIfMissing);
			$files[] = $this->createSizedVersion($image, 76, 76, true, $onlyIfMissing);
		} catch (Exception $e) {
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			$this->cleanup();
			throw $e;
		}
		
		$this->cleanup();
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $files;
	}

	public function resizeAgentImage($image, $onlyIfMissing = false) 
	{
		$files = array();
	
		try {
			$files[] = $this->createSizedVersion($image, 200, 200, true, $onlyIfMissing);
			$files[] = $this->createSizedVersion($image, 50, 50, true, $onlyIfMissing);
		} catch (Exception $e) {
			$this->cleanup();
			throw $e;
		}
		
		$this->cleanup();
		
		return $files;
	}
	
	private function createSizedVersion($image, $width, $height, $cropToAspect = true, $onlyIfMissing = false) {
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		$pathinfo = pathinfo($image);
		$ext = !empty($pathinfo['extension']) ? '.' . $pathinfo['extension'] : '';
		$outputFile = $pathinfo['dirname'] . "/" . $pathinfo['filename'] . "_{$width}x{$height}" . $ext;
		if ($onlyIfMissing && file_exists($outputFile)) {
			Logger::log('Size ' . $width . 'x'  . $height . ' already exists - skipping');
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return;
		}
		$resizeResult = null;
		$destFile = null;
		if ($this->streamMode) {
			if (!$this->tmpFile) {
				$this->tmpFile = '/tmp/_resize_' . md5(microtime()) . '/' . basename($image);
				mkdir(dirname($this->tmpFile));
				copy($image, $this->tmpFile);
				Logger::log('Created temp file: ' . $this->tmpFile, Logger::LOG_TYPE_DEBUG);
			}
			$imageOrig = $image;
			$image = $this->tmpFile;
			$pathinfo = pathinfo($image);
			$outputFile = $pathinfo['dirname'] . "/" . $pathinfo['filename'] . "_{$width}x{$height}" . $ext;
			$destFile = dirname($imageOrig) . '/' . basename($outputFile);
		}
		if ($cropToAspect) {
			$resizeResult = $this->resizeAndCrop($width, $height, $image, $outputFile);
		} else {
			$resizeResult = $this->resize($width, $height, $image, $outputFile);
		}
		Logger::log('Generated new file: ' . $outputFile, Logger::LOG_TYPE_DEBUG);
		$result = null;
		if ($resizeResult) {
			$result = $outputFile;
			if ($this->streamMode) {
				copy($outputFile, $destFile);
				Logger::log('Copied file to destination: ' . $destFile, Logger::LOG_TYPE_DEBUG);
				unlink($outputFile);
				// Don't unlink $this->tmpFile here, it will get removed in cleanup()
				// unlink($this->tmpFile);
				$result = $destFile;
			}
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		return $result;
	}
	
	/**
	 * Resize image cropping image to aspect ratio
	 */
	private function resizeAndCrop($width, $height, $srcImage, $destination) 
	{
		try {

			if (!file_exists(dirname($destination))) mkdir(dirname($destination), 0777, true);
			if (class_exists('Imagick')) {
				$im = new Imagick( $srcImage );
				$im->setImageResolution(72,72);
				$im->cropThumbnailImage($width, $height);
				$im->writeImage($destination);
				$im->destroy();
			} else {
				$resizeParameter = "{$width}x{$height}";
				if (PHP_OS != 'Darwin')
				{
					exec('convert '. $srcImage .' -resize '. $resizeParameter .'^ \  -gravity center -extent '. $resizeParameter. ' '. $destination .' 2>&1', $output, $status);
				}
				else
				{
					exec('/usr/local/bin/convert '. $srcImage .' -resize '. $resizeParameter .' '. $destination .' 2>&1', $output, $status);
				}
			}

		} catch (Exception $e) {

			throw new Exception('ImageMagick "convert" command failed. Error "'. $e->getMessage() .'" at '. $e->getLine() .' Line of '. $e->getFile());

		}

		return $this;
	}
	
	/**
	 * Resize image 
	 * 
	 * Passing zero for width or height will cause the correct width or height to used in order to maintain aspect ratio
	 */
	private function resize($width, $height, $srcPath, $destPath)
	{
		if (class_exists('Imagick')) {
			try {
				$im = new Imagick($srcPath);
				$im->setImageResolution(72, 72);
				$im->scaleImage($width, $height);
				$im->writeImage($destPath);
				$im->destroy();
			} catch (Exception $e) {
				throw new Exception('ImageMagick "convert" command failed. Error "'. $e->getMessage() .'" at '. $e->getLine() .' Line of '. $e->getFile());
			}
		} else {
			// Fall back to crop method
			$this->resizeAndCrop($width, $height, $srcPath, $srcPath);	
		}
		
		return $this;
	}

	private function cleanup() 
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		if ($this->tmpFile) {
			@unlink($this->tmpFile);
			@rmdir(dirname($this->tmpFile));
			$this->tmpFile = null;
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}
