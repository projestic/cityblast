<?php

class PaypalRequest
{
	public function init()
	{
		parent::init();
		//$this->setFacebookUser();
	} 


	function HTTP_Authorize($amount, $currency, $desc, $data) 
	{
		$version = urlencode('59.0');
		
		// Lets authorize 1 usd only
		$authorize_amount	=	1;

		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, API_URL);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
	
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
	
		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=DoDirectPayment&VERSION=".$version."&PWD=".API_PASSWORD."&USER=".API_USERNAME."&SIGNATURE=".API_SIGNATURE;


		$nvpreq = "SIGNATURE=".API_SIGNATURE."&USER=".API_USERNAME."&PWD=".API_PASSWORD."&METHOD=DoDirectPayment&VERSION=59.0&PAYMENTACTION=Authorization";

		
		if(isset($_SERVER) && isset($_SERVER['REMOTE_ADDR'])) $ip_address = $_SERVER['REMOTE_ADDR'];
		else $ip_address = IP_ADDRESS;

		$params = array(
			"IPADDRESS" => $ip_address,
			"AMT" => $authorize_amount,
			"CREDITCARDTYPE" => $data['cc_type'],
			"ACCT" => $data['cc_number'],
			"EXPDATE" => $data['exp_month'].$data['exp_year'],
			"CVV2" => $data['cv2'],			
			"FIRSTNAME" => $data['fname'],
			"LASTNAME" => $data['lname'],			
			"STREET" => $data['address'],
			"STREET2" => $data['address2'],
			"CITY" => $data['city'],
			"STATE" => $data['state'],
			"ZIP" => $data['zip'],
			"COUNTRYCODE" => $data['cc_country'],
			"CURRENCYCODE" => $currency,
			"EMAIL" => $data['email'],
			"PHONENUM" => $data['phone'],
			"ISSUENUMBER" => $data['issuenumber'],
			"STARTDATE" => $data['start_month'].$data['start_year'],
			"DESC" => $desc,
		);



		$nvpreq .= "&".http_build_query($params);

		
		info("PAYPAL CURL REQUEST");
		info($nvpreq);
	

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);


		// Get response from the server.
		$httpResponse = curl_exec($ch);
	
			
		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
	
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);
	
		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
	
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}


		if ( "Success" === $httpParsedResponseAr['ACK'] )
		{

			$transactionId	=	$httpParsedResponseAr['TRANSACTIONID'];
			return $this->doVoidAuthorization($transactionId); 
			
		}
		return $httpParsedResponseAr;
	}

	public function doVoidAuthorization($authorizationId)
	{
		$version = urlencode('59.0');
		
		// Lets authorize 1 usd only
		$authorize_amount	=	1;

		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, API_URL);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
	
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
	
		// Set the API operation, version, and API signature in the request.
		$nvpreq = "SIGNATURE=".API_SIGNATURE."&USER=".API_USERNAME."&PWD=".API_PASSWORD."&METHOD=DoVoid&VERSION=59.0&AuthorizationID=".$authorizationId;
		
		info("PAYPAL CURL REQUEST");
		info($nvpreq);
	
		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);


		// Get response from the server.
		$httpResponse = curl_exec($ch);
	
			
		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
	
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);
	
		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
	
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}

		$httpParsedResponseAr['TRANSACTIONID']	=	$authorizationId;

		return $httpParsedResponseAr;
	}

	public function DoMassPay($email,$amount,$currency)
	{

		$version = urlencode('51.0');
		
		$currency = urlencode($currency);
		// Set request-specific fields.
		$emailSubject =urlencode('example_email_subject');
		$receiverType = urlencode('EmailAddress');

		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, API_URL);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
	
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
	
		// Set the API operation, version, and API signature in the request.
		$nvpreq = "SIGNATURE=".API_SIGNATURE."&USER=".API_USERNAME."&PWD=".API_PASSWORD."&METHOD=MassPay&VERSION=51.0";
		$nvpreq .= "&EMAILSUBJECT=".$emailSubject."&RECEIVERTYPE=".$receiverType."&CURRENCYCODE=".$currency;

		$receiverEmail = urlencode($email);
		$amount = urlencode($amount);
		$uniqueID = uniqid();
		$note = '';
		$nvpreq .= "&L_EMAIL$i=$receiverEmail&L_Amt$i=$amount&L_UNIQUEID$i=$uniqueID&L_NOTE$i=$note";


		info("PAYPAL CURL REQUEST");
		info($nvpreq);

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);


		// Get response from the server.
		$httpResponse = curl_exec($ch);

			
		if(!$httpResponse) 
		{
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
	
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);
	
		$httpParsedResponseAr = array();		
		foreach ($httpResponseAr as $i => $value) 
		{
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
	
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) 
		{
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}

		return $httpParsedResponseAr;
	}

	/**
	 * Send PayPal HTTP POST Request
	 *
	 * @param	string	The API method name
	 * @param	string	The POST Message fields in &name=value pair format
	 * @return	array	Parsed HTTP Response body
	 */
	function HTTP_Post($amount, $currency, $desc, $data) 
	{
		
		$version = urlencode('59.0');
	
		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, API_URL);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
	
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
	
		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=DoDirectPayment&VERSION=".$version."&PWD=".API_PASSWORD."&USER=".API_USERNAME."&SIGNATURE=".API_SIGNATURE;


		$nvpreq = "SIGNATURE=".API_SIGNATURE."&USER=".API_USERNAME."&PWD=".API_PASSWORD."&METHOD=DoDirectPayment&VERSION=59.0&PAYMENTACTION=Sale";

		
		if(isset($_SERVER) && isset($_SERVER['REMOTE_ADDR'])) $ip_address = $_SERVER['REMOTE_ADDR'];
		else $ip_address = IP_ADDRESS;

		$params = array(
			"IPADDRESS" => $ip_address,
			"AMT" => $amount,
			"CREDITCARDTYPE" => $data['cc_type'],
			"ACCT" => $data['cc_number'],
			"EXPDATE" => $data['exp_month'].$data['exp_year'],
			"CVV2" => $data['cv2'],			
			"FIRSTNAME" => $data['fname'],
			"LASTNAME" => $data['lname'],			
			"STREET" => $data['address'],
			"STREET2" => $data['address2'],
			"CITY" => $data['city'],
			"STATE" => $data['state'],
			"ZIP" => $data['zip'],
			"COUNTRYCODE" => $data['cc_country'],
			"CURRENCYCODE" => $currency,
			"EMAIL" => $data['email'],
			"PHONENUM" => $data['phone'],
			"ISSUENUMBER" => $data['issuenumber'],
			"STARTDATE" => $data['start_month'].$data['start_year'],
			"DESC" => $desc,
		);



		$nvpreq .= "&".http_build_query($params);

		
		info("PAYPAL CURL REQUEST");
		info($nvpreq);
	

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);


		// Get response from the server.
		$httpResponse = curl_exec($ch);
	
			
		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
	
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);
	
		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
	
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}
	
		return $httpParsedResponseAr;
	}
	
	function RecurringPayment($amount, $currency, $desc, $data) 
	{
		
		$version = urlencode('59.0');
		$token = urlencode("token_from_setExpressCheckout");
		$currency = urlencode($currency);
		$startDate = urlencode(gmdate("Y-m-d\TH:i:s\Z"));
		$billingPeriod = urlencode("Month");
		$billingFreq = urlencode("1");
	
		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, API_URL);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
	
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
	
		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=CreateRecurringPaymentsProfile&VERSION=".$version."&PWD=".API_PASSWORD."&USER=".API_USERNAME."&SIGNATURE=".API_SIGNATURE;
		$nvpreq .= "&AMT=".$amount."&CURRENCYCODE=".$currency."&PROFILESTARTDATE=".$startDate."&BILLINGPERIOD=".$billingPeriod."&BILLINGFREQUENCY=".$billingFreq;


		$params = array(
			"IPADDRESS" => $_SERVER['REMOTE_ADDR'],
			"AMT" => $amount,
			"CREDITCARDTYPE" => $data['cc_type'],
			"ACCT" => $data['cc_number'],
			"EXPDATE" => $data['exp_month'].$data['exp_year'],
			"CVV2" => $data['cv2'],			
			"FIRSTNAME" => $data['fname'],
			"LASTNAME" => $data['lname'],			
			"STREET" => $data['address'],
			"STREET2" => $data['address2'],
			"CITY" => $data['city'],
			"STATE" => $data['state'],
			"ZIP" => $data['zip'],
			"COUNTRYCODE" => $data['cc_country'],
			"CURRENCYCODE" => $currency,
			"EMAIL" => $data['email'],
			"PHONENUM" => $data['phone'],
			"ISSUENUMBER" => $data['issuenumber'],
			"STARTDATE" => $data['start_month'].$data['start_year'],
			"DESC" => $desc,
		);


		$nvpreq .= "&".http_build_query($params);

		
		info("PAYPAL CURL REQUEST");
		info($nvpreq);
	

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);


		// Get response from the server.
		$httpResponse = curl_exec($ch);
	
			
		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
	
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);
	
		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
	
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}
	
		return $httpParsedResponseAr;
	}

	public function DoReferenceTransaction($payment, $amount, $currency, $itemName)
	{
	    $version = urlencode('59.0');

	    // Set the curl parameters.
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, API_URL);
	    curl_setopt($ch, CURLOPT_VERBOSE, 1);

	    // Turn off the server and peer verification (TrustManager Concept).
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_POST, 1);

	    // Set the API operation, version, and API signature in the request.

	    $nvpreq = "SIGNATURE=".API_SIGNATURE."&USER=".API_USERNAME."&PWD=".API_PASSWORD."&METHOD=DoReferenceTransaction&VERSION=59.0&PAYMENTACTION=Sale";

		$ip_address = null;
		if (isset($_SERVER['REMOTE_ADDR'])) $ip_address = $_SERVER['REMOTE_ADDR'];

	    $params = array(
		    "IPADDRESS" => $ip_address,
		    "AMT" => $amount,
		    "CURRENCYCODE" => $currency,
		    "AUTHORIZATIONID" => $payment->paypal_confirmation_number,
		    "REFERENCEID" => $payment->paypal_confirmation_number,
		    "EXPDATE" => $payment->cc_expire_date,
		    "FIRSTNAME" => $payment->first_name,
		    "LASTNAME" => $payment->last_name,
		    "DESC" => $itemName
	    );




	    $nvpreq .= "&".http_build_query($params);

	    info("PAYPAL CURL REQUEST");
	    info($nvpreq);


	    // Set the request as a POST FIELD for curl.
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);


	    // Get response from the server.
	    $httpResponse = curl_exec($ch);


	    if(!$httpResponse) {
		    exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
	    }

	    // Extract the response details.
	    $httpResponseAr = explode("&", $httpResponse);

	    $httpParsedResponseAr = array();
	    foreach ($httpResponseAr as $i => $value) {
		    $tmpAr = explode("=", $value);
		    if(sizeof($tmpAr) > 1) {
			    $httpParsedResponseAr[$tmpAr[0]] = urldecode($tmpAr[1]);
		    }
	    }

	    if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		    exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	    }

	    return $httpParsedResponseAr;

	}




}



?>