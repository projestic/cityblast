<?php

class Logger
{
	const LOG_TYPE_DEBUG = 'debug';
	
	private static $echo = false;
	private static $html = false;
	private static $filePath = null;
	private static $firstLogSeconds = null;
	private static $previousLogSeconds = null;
	private static $indentChar = '-';
	private static $logTypes = null;
	private static $indentLevel = 0;
	private static $cacheId = null;
	private static $levelCacheEnabled = false;
	
		
	/**
	 * Start a new identation section
	 */
	public static function startSection($type = null)
	{
		if (!self::isActiveType($type)) {
			return;
		}
		self::$indentLevel++;
	}
	
	/**
	 * End current indentation section
	 */
	public static function endSection($type = null)
	{
		if (!self::isActiveType($type)) {
			return;
		}
		@unlink(self::getSectionCacheFile(self::$indentLevel));
		self::$indentLevel--;
		self::$indentLevel = (self::$indentLevel < 0) ? 0: self::$indentLevel;
	}
	
	private static function cacheSectionLine($line) {
		if (self::$levelCacheEnabled == false) return false;
		// not caching top level, can check entire file or output for that
		if (self::$indentLevel == 0) return false;
		for ($i = self::$indentLevel; $i > 0; $i--) {
			file_put_contents(self::getSectionCacheFile($i), $line, FILE_APPEND);
		}
	}
	
	private static function getSectionCacheFile($level) {
		if (self::$levelCacheEnabled == false) return false;
		if (self::$cacheId == null) self::$cacheId = substr(md5(microtime()), 0, 10);
		return APPLICATION_PATH . '/../cache/' . self::$cacheId . '-' . "-level-$level.txt";
	}
	
	public static function getCurrentSectionLines() {
		if (self::$levelCacheEnabled == false) return false;
		// not caching top level, can check entire file or output for that
		if (self::$indentLevel == 0) return false;
		return file_get_contents(self::getSectionCacheFile(self::$indentLevel));
	}

	public static function setLevelCacheEnabled($boolean)
	{
		self::$levelCacheEnabled = (bool) $boolean;
		for ($i = self::$indentLevel; $i > 0; $i--) {
			if ($file = self::getSectionCacheFile($i)) @unlink($file);
		}
	}

	/**
	 * End current indentation section
	 */
	public static function isActiveType($type)
	{
		$result = true;
		if (
			!empty($type) &&
			(
				empty(self::$logTypes) ||
				!in_array($type, self::$logTypes)
			)
		) {
			$result = false;
		}
		return $result;
	}

	/**
	 * Log message
	 *
	 * @param string $message
	 * @param string $type The type of log message
	 * @param boolean $singleSection Set to true if this log message is both start and end of a section
	 */
	public static function log($message, $type = null, $singleSection = false)
	{
		if (!self::isActiveType($type)) {
			return;
		}
		if (self::$firstLogSeconds === null) {
			self::logTimeMarker();
		}
		if ($singleSection) {
			self::startSection($type);
		}
		$line = self::getLine($message, self::$indentLevel);
		self::writeLog($line);
		if ($singleSection) {
			self::endSection($type);
		}
		if (self::$firstLogSeconds === null) {
			self::$firstLogSeconds = microtime(true);
		}
		self::$previousLogSeconds = microtime(true);
	}

	/**
	 * Log time marker
	 *
	 * @return void
	 */
	public static function logTimeMarker()
	{
		$line = str_repeat('~', 25) . ' ' . date('c') . ' ' . str_repeat('~', 25) . "\n";
		self::writeLog($line);
	}

	/**
	 * Write a line to the log
	 *
	 * @param string $line
	 * @return void
	 */
	private static function writeLog($line)
	{
		if (self::$html) {
			$line = str_replace("\n", "<br/>\n", $line);
		}
		if (self::$echo) {
			echo $line;
		}
		if (self::$filePath) {
			file_put_contents(self::$filePath, $line, FILE_APPEND);
		}
		self::cacheSectionLine($line);
	}

	/**
	 * Set log types that will be logged
	 *
	 * Log messages that dont specify a log type will always be logged.
	 *
	 * @types string|array $types
	 * @return void
	 */
	public static function setLogTypes($types = null)
	{
		$types = is_array($types) ? $types : array($types);
		self::$logTypes = !empty($types) ? $types : null;
	}

	/**
	 * Echo log
	 *
	 * @param boolean $boolean
	 * @return void
	 */
	public static function setEcho($boolean)
	{
		self::$echo = (bool) $boolean;
	}

	/**
	 * Output HTML
	 *
	 * @param boolean $boolean
	 * @return void
	 */
	public static function setHtml($boolean)
	{
		self::$html = (bool) $boolean;
	}

	/**
	 * Set log file path
	 *
	 * @param string $filePath
	 * @return void
	 */
	public static function setFilePath($filePath)
	{
		self::$filePath = $filePath;
	}
	
	/**
	 * Set indent level
	 *
	 * @param int $level
	 * @return void
	 */
	public static function setIndentLevel($level)
	{
		$level = (int) $level;
		static::$indentLevel = ($level < 0) ? 0: $level;
	}
	
	/**
	 * Get indent level
	 *
	 * @return int
	 */
	public static function getIndentLevel()
	{
		return static::$indentLevel;
	}

	/**
	 * Get log line
	 *
	 * @param string $message
	 * @param int $indent The number of indent spaces
	 * @return string
	 */
	private static function getLine($message, $indent = 0)
	{
		$indent_chars = ($indent) ? str_repeat(self::$indentChar, $indent) . ' ' : '';
		$message = '['. self::getSecondsSinceFirstLog() .']['. self::getSecondsSincePreviousLog() .'] '
					. $indent_chars . $message . "\n";
		return $message;
	}

	/**
	 * Get number of seconds since previous log
	 *
	 * @return float
	 */
	private static function getSecondsSincePreviousLog()
	{
		$now = microtime(true);
		$seconds = empty(self::$previousLogSeconds) ? 0 : $now - self::$previousLogSeconds;
		return self::formatLogSeconds($seconds);
	}
	
	/**
	 * Get number of seconds since first log
	 *
	 * @return float
	 */
	private static function getSecondsSinceFirstLog()
	{
		$now = microtime(true);
		$seconds = empty(self::$firstLogSeconds) ? 0 : $now - self::$firstLogSeconds;
		return self::formatLogSeconds($seconds);
	}

	/**
	 * Format log seconds
	 *
	 * @param $seconds float
	 * @return float
	 */
	private static function formatLogSeconds($seconds)
	{
		return sprintf('%.4F', $seconds);
	}


}