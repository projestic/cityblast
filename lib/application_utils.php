<?
  function trace($trace = null)
  {
    if(!$trace) $trace = debug_backtrace();
    $return = '';
    foreach($trace as $line)
      $return .= "{$line['function']} {$line['file']}:{$line['line']}\n";
      
    return $return;
  }
  
  function print_rf($var)
  {
    f(print_r($var, true));
  }
  
  function f($text)
  {
    echo "<pre>";
    echo $text;
    echo "</pre>";
    ob_flush();
  }
  
  // Trim whitespace from all array values
  function trim_value(&$value) 
  { 
    $value = trim($value); 
  }
  
  function db_date($datetime)
  {
    return $datetime->format('Y-m-d H:i:s');
  }
  
  function absolute_url($url, $base_domain)
  {
    if(strpos($url, 'http://') === 0)
      return $url;
      
    return $base_domain . $url;
  }
  
  function debug($message)
  {
    zend_log($message, Zend_Log::DEBUG);
  }
  
  function info($message)
  {
    zend_log($message, Zend_Log::INFO);
  }
  
  function error($message)
  {
    zend_log($message, Zend_Log::ERR);
  }
  
  function zend_log($message, $level=Zend_Log::DEBUG)
  {
    $logger = Zend_Registry::get('logger');
    $logger->log($message, $level);
  }
  
  function array_to_sentence($array)
  {
    $last_word = array_pop($array);
    $sentence = implode(', ', $array);
    return "{$sentence} and {$last_word}";
  }
  
  function errorHandler($errno, $errstr, $errfile, $errline) 
  {
  
    if( error_reporting() != 0)
      throw new ErrorException($errstr,0,$errno,$errfile,$errline);
  }
  
  set_error_handler("errorHandler", E_ALL & ~E_NOTICE & ~E_DEPRECATED);
  
	function numberToRoman($num) 
	{
		// Make sure that we only use the integer portion of the value
		$n = intval($num);
		$result = '';
		
		// Declare a lookup array that we will use to traverse the number:
		$lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
		'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
		'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
		
		foreach ($lookup as $roman => $value) 
		{
			// Determine the number of matches
			$matches = intval($n / $value);
			
			// Store that many characters
			$result .= str_repeat($roman, $matches);
			
			// Substract that from the number
			$n = $n % $value;
		}
		
		// The Roman numeral should be built, return it
		return $result;
	}
	
	function strip_html_tags( $text )
	{
	    $text = preg_replace(
		   array(
			// Remove invisible content
			  '@<head[^>]*?>.*?</head>@siu',
			  '@<style[^>]*?>.*?</style>@siu',
			  '@<script[^>]*?.*?</script>@siu',
			  '@<object[^>]*?.*?</object>@siu',
			  '@<embed[^>]*?.*?</embed>@siu',
			  '@<applet[^>]*?.*?</applet>@siu',
			  '@<noframes[^>]*?.*?</noframes>@siu',
			  '@<noscript[^>]*?.*?</noscript>@siu',
			  '@<noembed[^>]*?.*?</noembed>@siu',
			// Add line breaks before and after blocks
			  '@</?((address)|(blockquote)|(center)|(del))@iu',
			  '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
			  '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
			  '@</?((table)|(th)|(td)|(caption))@iu',
			  '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
			  '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
			  '@</?((frameset)|(frame)|(iframe))@iu',
		   ),
		   array(
			  ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			  "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
			  "\n\$0", "\n\$0",
		   ),
		   $text );
	    return strip_tags( $text );
	}
	
	function ordinal_suffix($value, $sup = 0, $suffix_only = 0)
	{
		// Function written by Marcus L. Griswold (vujsa)
		// Can be found at http://www.handyphp.com
		// Do not remove this header!
		
		if(!$value)
			return;
	
	    is_numeric($value) or trigger_error("<b>\"$value\"</b> is not a number!, The value must be a number in the function <b>ordinal_suffix()</b>", E_USER_ERROR);
	    if(substr($value, -2, 2) == 11 || substr($value, -2, 2) == 12 || substr($value, -2, 2) == 13){
	        $suffix = "th";
	    }
	    else if (substr($value, -1, 1) == 1){
	        $suffix = "st";
	    }
	    else if (substr($value, -1, 1) == 2){
	        $suffix = "nd";
	    }
	    else if (substr($value, -1, 1) == 3){
	        $suffix = "rd";
	    }
	    else {
	        $suffix = "th";
	    }
	    if($sup){
	        $suffix = "<sup>" . $suffix . "</sup>";
	    }
	    if($suffix_only)
		    return $suffix;
	    else
		    return $value . $suffix;
	}
	
	function TimeAgo($datefrom,$dateto=-1)
	{
		// Defaults and assume if 0 is passed in that
		// its an error rather than the epoch
		
		if($datefrom<=0) { return "A long time ago"; }
		if($dateto==-1) { $dateto = time(); }
		
		// Calculate the difference in seconds betweeen
		// the two timestamps
		
		$difference = $dateto - $datefrom;
		
		// If difference is less than 60 seconds,
		// seconds is a good interval of choice
		
		if($difference < 60)
		{
			$interval = "s";
		}
		
		// If difference is between 60 seconds and
		// 60 minutes, minutes is a good interval
		elseif($difference >= 60 && $difference<60*60)
		{
			$interval = "n";
		}
		
		// If difference is between 1 hour and 24 hours
		// hours is a good interval
		elseif($difference >= 60*60 && $difference<60*60*24)
		{
			$interval = "h";
		}
		
		// If difference is between 1 day and 7 days
		// days is a good interval
		elseif($difference >= 60*60*24 && $difference<60*60*24*7)
		{
			$interval = "d";
		}
		
		// If difference is between 1 week and 30 days
		// weeks is a good interval
		elseif($difference >= 60*60*24*7 && $difference < 60*60*24*30)
		{
			$interval = "ww";
		}
		
		// If difference is between 30 days and 365 days
		// months is a good interval, again, the same thing
		// applies, if the 29th February happens to exist
		// between your 2 dates, the function will return
		// the 'incorrect' value for a day
		elseif($difference >= 60*60*24*30 && $difference < 60*60*24*365)
		{
			$interval = "m";
		}
		
		// If difference is greater than or equal to 365
		// days, return year. This will be incorrect if
		// for example, you call the function on the 28th April
		// 2008 passing in 29th April 2007. It will return
		// 1 year ago when in actual fact (yawn!) not quite
		// a year has gone by
		elseif($difference >= 60*60*24*365)
		{
			$interval = "y";
		}
		
		// Based on the interval, determine the
		// number of units between the two dates
		// From this point on, you would be hard
		// pushed telling the difference between
		// this function and DateDiff. If the $datediff
		// returned is 1, be sure to return the singular
		// of the unit, e.g. 'day' rather 'days'
		
		switch($interval)
		{
			case "m":
				$months_difference = floor($difference / 60 / 60 / 24 / 29);
				while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto)
				{
					$months_difference++;
				}
				$datediff = $months_difference;
		
				// We need this in here because it is possible
				// to have an 'm' interval and a months
				// difference of 12 because we are using 29 days
				// in a month
				
				if($datediff==12)
				{
					$datediff--;
				}
				
				$res = ($datediff==1) ? "$datediff month ago" : "$datediff months ago";
				break;
		
			case "y":
				$datediff = floor($difference / 60 / 60 / 24 / 365);
				$res = ($datediff==1) ? "$datediff year ago" : "$datediff years ago";
				break;
			
			case "d":
				$datediff = floor($difference / 60 / 60 / 24);
				$res = ($datediff==1) ? "$datediff day ago" : "$datediff days ago";
				break;
			
			case "ww":
				$datediff = floor($difference / 60 / 60 / 24 / 7);
				$res = ($datediff==1) ? "$datediff week ago" : "$datediff weeks ago";
				break;
			
			case "h":
				$datediff = floor($difference / 60 / 60);
				$res = ($datediff==1) ? "$datediff hour ago" : "$datediff hours ago";
				break;
			
			case "n":
				$datediff = floor($difference / 60);
				$res = ($datediff==1) ? "$datediff minute ago" : "$datediff minutes ago";
				break;
			
			case "s":
				$datediff = $difference;
				$res = ($datediff==1) ? "$datediff second ago" : "$datediff seconds ago";
				break;
		}
		return $res;
	}

	function nl2p($string, $class='') {
		$class_attr = ($class!='') ? ' class="'.$class.'"' : '';
		$newstr = '<p'.$class_attr.'>'.preg_replace('#(<br\s*?/?>\s*?){2,}#', '</p>'."\n".'<p'.$class_attr.'>', nl2br($string)).'</p>';
		return $newstr;
	} 

?>