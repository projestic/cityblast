<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<? if (isset($this->title) && $this->title) : ?><title><?= $this->title; ?></title><? endif; ?>

		<? if (isset($this->description) && $this->description) : ?><meta name="description" content="<?= $this->description; ?>" /><? endif; ?>
		<? if (isset($this->keyword) && $this->keyword) : ?><meta name="keywords" content="<?= $this->keyword; ?>" /><? endif; ?>
		<? if (isset($this->catonical) && $this->catonical) : ?><link rel="canonical" href="<?= $this->catonical; ?>" /><? endif; ?>

		<!-- STYLESHEETS -->

		<? /*<link href="/css/styles.css" rel="stylesheet" type="text/css" /> */ ?>
		<? /*<link href="/css/main.css" rel="stylesheet" type="text/css" /> */ ?>

		
		<? /* OLD STYLE SHEET  --  you need to delete this at some point
		<link href="/css/styles.css" rel="stylesheet" type="text/css" />*/ ?> 
		
		
		<? /* NEW STYLE SHEET -- these are missing images, etc. */ ?>
		<link href="/css/admin/forms.css" rel="stylesheet" type="text/css" />
		<link href="/css/admin/reset.css" rel="stylesheet" type="text/css" />
		<link href="/css/admin/grid.css" rel="stylesheet" type="text/css" />
		<link href="/css/admin/main.css" rel="stylesheet" type="text/css" />
		<link href="/css/admin/ui.css" rel="stylesheet" type="text/css" />



		<link href="/css/facebox.css" rel="stylesheet" type="text/css" />
		<link href="/css/uploadify.css" type="text/css" rel="stylesheet" />
		<link href="/css/slide.css" type="text/css" rel="stylesheet" />

		<link href="/js/colorbox/colorbox.css" type="text/css" rel="stylesheet" />

		<link type="text/css" rel="stylesheet" href="<?php echo $this->documentRoot;?>/css/jquery-ui-1.8.16.custom.css" />


		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


		<!-- SLIDING EFFECT -->
		<script src="/js/slide.js" type="text/javascript"></script>
		<script src="/js/facebox.js" type="text/javascript"></script>

   		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

		<script type="text/javascript">
		<?= $this->render_content_for('js'); ?>
		</script>

		<? /* FIREBUG LITE FOR DEBUGGING IN NON-FIREFOX BROWSER
		      <script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite.js'></script>
		      <script type="text/javascript">
		      firebug.env.height = 120;
		      </script>
		    */ ?>

		<link media="screen" rel="stylesheet" href="/js/colorbox/colorbox.css" />
		<script type="text/javascript" src="/js/colorbox/jquery.colorbox-min.js"></script>

	<style type="text/css">

	.clearfix:after {
		content: " ";
		display: block;
		height: 0;
		clear: both;
		visibility: hidden;
	}

	</style>

		<!--[if IE]>
			<style type="text/css">
				.clearfix {
					zoom: 1;
				}
			</style>
		<![endif]-->
		<!--[if lte IE 7]>
			<style type="text/css">
				.fieldname {
					text-indent: 0px;
				}
			</style>
		<![endif]-->
		<!--[if IE 7]>
			<style type="text/css">
				#daily_trials .trial_label, #daily_trials .trial_count{
					display: inline;
				}
			</style>
		<![endif]-->
		<!--[if lt IE 8]>
			<style type="text/css">
				/* IE6 */
				*html .uiButton.IE input{
					overflow: visible; /* remove padding from left/right */
					width:0; /*remove the remaining space in IE6*/
				}

				/* IE7 */
				*:first-child+html .uiButton.IE input{
					#overflow: visible; /* remove padding from left/right */
					#width:auto;
				}
			</style>
		<![endif]-->

		
		<? /***************
		<script type="text/javascript">
		  (function() {
		    window.fireflyAPI = {};
		    fireflyAPI.token = "506206504ef500670a002913";
		    var script = document.createElement("script");
		    script.type = "text/javascript";
		    script.src = "https://firefly-071591.s3.amazonaws.com/scripts/loaders/loader.js";
		    script.async = true;
		    var firstScript = document.getElementsByTagName("script")[0];
		    firstScript.parentNode.insertBefore(script, firstScript);
		  })();
		</script>
		*******************/
		?>
		
	</head>

<? $member = $_SESSION['member']; ?>


	<body id="wrapper" class="logged">




		<!--<div id="header" class="png_bg">-->
		<div id="toppanel" class="png_bg">


			<div id="panel">
				<div class="container_12" id="hud_content">

					<div class="grid_8">
						<h1>Welcome to <?=COMPANY_NAME;?></h1>
						<h2><?=COMPANY_NAME;?> + Facebook Connect = Match Made in Heaven</h2>
						<p>At <?=COMPANY_NAME;?> we've decided to use Facebook Connect for our login system. What does this mean for you? Well it means you don't have to
						remember yet another password and you don't have to fill in another pesky registration form. More importantly it means 100% privacy because we never get access to your Facebook
						password (we promise... just ask the good people at <a href="http://www.facebook.com">Facebook</a>).
						</p>
						<h2>Facebook is a virtual passport that saves you time</h2>
						<p>Still have concerns? Why not take a quick tour in order to find out about  <a href="http://developers.facebook.com/blog/post/108/">what Facebook Connect is</a> and how your privacy is protected.</p>
					</div>

					<div class="grid_4">

						<h1>Register</h1>

						<p>For all you first time users there's just one simple step to connecting your <?=COMPANY_NAME;?> Account with your Facebook Account. Simply click the Register button below and follow
						the instructions</p>

						<div style="float:right;"><a href="/member/signup/">register</a></div>
						<div class="clearfix"></div>


						<h1>Login</h1>

						<p>Already a member? Thanks for visiting us again. Simply login now.</p>

						<div style="float:right;"><a href="/member/login/">login</a></div>
						<div class="clearfix"></div>

					</div>

				</div>
			</div> <!-- /login -->



			<div id="top_cap"></div>
			<div id="header_wrapper">

				<div id="header" class="container_12 clearfix">

					<div class="grid_3">
						<a href="/" id="logo"><h1><?=COMPANY_NAME;?></h1></a>
					</div>
		
					<? if (empty($this->daily_stats) && !empty($_SESSION['daily_stats']))
						$this->daily_stats = $_SESSION['daily_stats'];
					?>
					<div class="grid_6">
						<div id="daily_trials">

							<div class="trial_label">Paid: </div>
							<div class="trial_count"><?=isset($this->daily_stats->paid) ? $this->daily_stats->paid : "--"?></div>

							<div class="trial_label">Trials: </div>
							<div class="trial_count"><?=isset($this->daily_stats->trials) ? $this->daily_stats->trials : "--"?></div>

							<div class="trial_label">Dropoffs: </div>
							<div class="trial_count"><?=isset($this->daily_stats->dropoffs) ? $this->daily_stats->dropoffs : "--"?></div>
						</div>
					</div>
					
					<div class="grid_3">
						<div id="user_nav">
							
							<? if($_SESSION['member']) : ?>

								<div id="top_menu">
									
									<div class="user_photo"><img width="50" height="50" src="<?php echo empty($_SESSION['member']->photo) ? "https://graph.facebook.com/". $_SESSION['member']->uid ."/picture" : CDN_URL . $_SESSION['member']->photo ?>" /></div>
									<span class="name"><a href="/member/settings"><?= $member->first_name . " " . $member->last_name; ?></a></span>
									<span class="logout"><a href="/member/logout">Logout</a></span>
								</div>

							<? else: ?>

								<div id="top_login" style="float: right;">

									<span id="open_login"><a href="/member/login" class="name">Login</a> or <a href="#" class="name" id="open">Register</a></span>
									<span id="close_login" style="display: none;"><a id="close" class="close" href="#">Close Panel</a></span>

								</div>

								<div class="clearfix"></div>
								<div id="followers" style="float: right; padding-top: 4px;"><?=number_format($this->total_count);?> followers and counting</div>

							<? endif; ?>


						</div>
					</div>


				</div>
			</div>
			<div id="navigation_wrapper">
				<div id="navigation" class="container_12 clearfix">

					<!-- start navigation -->
					<div class="grid_12 new_navigation">

						<? 

		                    if ( $member && $member->hasType('superadmin') ) : ?>
		
								<ul>
		
									<?= $this->AdminMenu(
										array(
											'/admin' => array(
												'id'   => 'publisher', 
												'name' => 'Publisher', 
												'children' => array(
													'/admin/index'    	=> array('name' => 'Publisher'),
													'/admin/listings' 	=> array('name' => 'Content'),
													'/admin/pending'    => array('name' => 'Pending approval'),
													'/admin/allposts' 	=> array('name' => 'Publishing Log'),
													'/admin/list_tags' 	=> array('name' => 'Tags'),
													'/listing/tags' 	=> array('name' => 'Untagged'),
													'/networkapp/index'	=> array('name' => 'Network Apps'),
												)
											)
										)
									); ?>

									<?= $this->AdminMenu(
										array(
											'/admin/payments' => array(
												'id'   => 'payment', 
												'name' => 'Payments', 
												'children' => array(
													'/admin/payments'   		=> array('name' => 'Payments'),
													'/admin/paymentsbyday' 		=> array('name' => 'Payments by Day'),
													'/admin/growth' 			=> array('name' => 'Growth'),
													'/admin/revenue' 			=> array('name' => 'Revenue'),
													'/admin/failedpayment' 		=> array('name' => 'Failed Payments'),
													'/admin/referralcredits' 	=> array('name' => 'Referral Credits'),
													'/admin/failedpayment' 		=> array('name' => 'Failed Payments'),
													'/admin/payable' 			=> array('name' => 'Accounts Payable'),
													'/admin/memberstatsdaily' 	=> array('name' => 'Member Stats'),
													'/stats/financiallatest' 	=> array('name' => 'Financial Stats'),
												)
											)
										)
									); ?>


									<?= $this->AdminMenu(
										array(
											'/admin/cities' => array(
												'id'   => 'cities', 
												'name' => 'Cities', 
												'children' => array(
													'/admin/cities'   			=> array('name' => 'Cities'),
													'/admin/reach-by-city' 		=> array('name' => 'Reach By City'),
													'/admin/groups' 			=> array('name' => 'Groups'),
												)
											)
										)
									); ?>
		

									<?= $this->AdminMenu(
										array(
											'/franchise' => array(
												'id'   => 'franchises', 
												'name' => 'Franchise', 
												'children' => array(
													'/franchise/index' 		=> array('name' => 'Franchises'),
													'/franchise/alias' 		=> array('name' => 'Franchise Aliases'),
													'/franchise/email' 		=> array('name' => 'Root Email Addresses'),
													'/franchise/unknown' 	=> array('name' => 'Unknown Members'),
												)
											)
										)
									); ?>


									<?= $this->AdminMenu(
										array(
											'/admin/allmembers' => array(
												'id'   => 'members', 
												'name' => 'Members', 
												'children' => array(
													'/admin/allmembers' 	=> array('name' => 'All'),
													'/admin/paid' 			=> array('name' => 'Paid'),
													'/admin/free' 			=> array('name' => 'Free'),
													'/admin/cityassignment' 	=> array('name' => 'Missing City'),
													'/admin/members' 		=> array('name' => 'Active'),
													'/admin/inactivemembers'=> array('name' => 'Inactive'),
													'/admin/expiredtoken'   => array('name' => 'Expire Token'),
													'/admin/threestrikes'   => array('name' => '3 Strikes'),
													'/fanpage'   			=> array('name' => 'Fanpages'),
													'/publishsettings'   	=> array('name' => 'Settings changes'),
													'/admin/fbpword'   		=> array('name' => 'FB Pword'),
													'/admin/twpword'   		=> array('name' => 'TW Pword'),
													'/admin/deletedmembers' => array('name' => 'Deleted'),
													'/admin/cancelrequests' => array('name' => 'Cancel requests'),
													'/membertype'   		=> array('name' => 'Types'),
												)
											)
										)
									); ?>


									<?= $this->AdminMenu(
										array(
											'/admin/affiliate' => array(
												'id'   => 'affiliates', 
												'name' => 'Affiliate Sales', 
												'children' => array(
													'/admin/affiliatepayoutreport' 	=> array('name' => 'Payout report'),
													'/admin/staffsalesleaders'		=> array('name' => 'Staff Leaders Board'),
												)
											)
										)
									); ?>

									<?= $this->AdminMenu(
										array(
											'/admin/interactions' => array(
												'id'   => 'interactions', 
												'name' => 'Statistics', 
												'children' => array(
													'/engagement'				=> array('name' => 'Engagement'),
													'/admin/demographics' 		=> array('name' => 'Demographics'),
													'/admin/interactions' 		=> array('name' => 'Interactions'),
													'/admin/bookings' 			=> array('name' => 'Bookings'),
													'/admin/comments' 			=> array('name' => 'Comments'),
													'/admin/emails' 			=> array('name' => 'Email Log'),
													'/stats/content' 			=> array('name' => 'Content'),
													'/stats/content?content_new_start=last%20tuesday&content_new_end=last%20tuesday%20+6days' 			=> array('name' => 'Content, Tue-Tue'),
												)
											)
										)
									); ?>
		
									<?= $this->AdminMenu(
										array(
											'/promo' => array(
												'id'   => 'promo', 
												'name' => 'Promo Codes', 
											)
										)
									); ?>

									<?= $this->AdminMenu(
										array(
											'/admin/newsletter' => array(
												'id'   => 'newsletter', 
												'name' => 'Newsletter', 
											)
										)
									); ?>

									<?= $this->AdminMenu(
										array(
											'/administrator' => array(
												'id'   => 'admins', 
												'name' => 'Admins', 
											)
										)
									); ?>

								</ul>
								
		                    <?php elseif ( $member && $member->hasType('salesadmin') ) : ?>

		                        	<ul>
										<li><a id="payments" href="/admin/payments/" <? if($this->params['controller']=="admin" && $this->params['action']=="payments") echo 'class="active"'; ?>>Payments</a></li>
										<li><a id="payables" href="/admin/payable/" <? if($this->params['controller']=="admin" && $this->params['action']=="payable") echo 'class="active"'; ?>>Payables</a></li>
										<li><a id="members" href="/admin/allmembers/" <? if($this->params['controller']=="admin" && in_array($this->params['action'], array("expiredtoken", "publishing", "fbpword", "twpword", "members", "threestrikesmembers", "inactivemembers", "allmembers", "deletedmembers", "growth", "reach-by-city", "cancelrequests"))) echo 'class="active"'; ?>>Members</a></li>
										<li><a id="fanpage" href="/fanpage/index/" <? if($this->params['controller']=="fanpage" && $this->params['action']=="index") echo 'class="active"'; ?>>Fanpages</a></li>
										<li><a id="stats" href="/admin/stats/" <? if($this->params['controller']=="admin" && $this->params['action']=="stats") echo 'class="active"'; ?>>Stats</a></li>
										<li><a id="interactions" href="/admin/interactions" <? if($this->params['controller']=="admin" && in_array($this->params['action'], array("interactions", "bookings", "emails", "comments"))) echo 'class="active"'; ?>>Interactions</a></li><li><a id="cities" href="/admin/cities/" <? if($this->params['controller']=="admin" && $this->params['action']=="cities") echo 'class="active"'; ?>>Cities</a></li>
	                        		</ul>
	                        		
		                    <?php elseif ( $member && ($member->hasType('odesk') || $member->hasType('assistanteditor')) ) : ?>
	                        		
		                    <?php endif; ?>
					</div>
					<!-- end navigation -->

				</div>


			</div>
			<!-- end headwarp  -->





		</div>
		<!-- end header -->



		<!-- start subnavs -->

		<? /*
			<div id="interactions_sub_nav" 

				<? if($this->params['controller']=="admin" && 
					($this->params['action']=="cities" or $this->params['action']=="groups"))
						 echo ' style="display:block;"';
					else echo ' style="display:none;" ';
				
				?>				
				>

				<div id="subnav_wrap" class="container_12">

					<!-- start sub nav list -->
					<div id="subnav" class="grid_12">

						<ul>
							<li><a href="/admin/cities" <? if($this->params['controller']=="admin" && $this->params['action']=="cities") echo 'class="active"'; ?>>Cities</a></li>
							<li><a href="/admin/groups" <? if($this->params['controller']=="admin" && $this->params['action']=="groups") echo 'class="active sub_nav_last"'; else echo 'class="sub_nav_last"'; ?>>Groups</a></li>														
						</ul>

					</div>
					<!-- end subnavigation list -->
				</div>
			</div>
			

			<div id="interactions_sub_nav" 

				<? if($this->params['controller']=="admin" && 
					($this->params['action']=="bookings" or $this->params['action']=="comments"  or $this->params['action']=="emails"  or $this->params['action']=="interactions"))
						 echo ' style="display:block;"';
					else echo ' style="display:none;" ';
				
				?>				
				>

				<div id="subnav_wrap" class="container_12">

					<!-- start sub nav list -->
					<div id="subnav" class="grid_12">

						<ul>
							<li><a href="/admin/interactions" <? if($this->params['controller']=="admin" && $this->params['action']=="interactions") echo 'class="active sub_nav_last"'; else echo 'class="sub_nav_last"'; ?>>Interactions</a></li>
							<li><a href="/admin/bookings" <? if($this->params['controller']=="admin" && $this->params['action']=="bookings") echo 'class="active"'; ?>>Bookings</a></li>
							<li><a href="/admin/comments/" <? if($this->params['controller']=="admin" && $this->params['action']=="comments") echo 'class="active"'; ?>>Comments</a></li>
							<li><a href="/admin/emails" <? if($this->params['controller']=="admin" && $this->params['action']=="emails") echo 'class="active"'; ?>>Emails</a></li>
						</ul>

					</div>
					<!-- end subnavigation list -->
				</div>
			</div>
			<!-- end sub_nav -->
			
			<div id="payment_sub_nav" 
				<?php
				$style = 'background-color: #F2F2F2; border-bottom-color: #CCCCCC; border-bottom-style: solid; border-bottom-width: 1px; overflow-x: hidden; overflow-y: hidden; padding-bottom: 15px; padding-top: 15px;';
				if($this->params['controller']=="admin" && ($this->params['action']=="payments" or $this->params['action']=="paymentsbyday")){
					echo " style='display:block;$style' ";
				}
				else{
					echo " style='display:none;$style' ";
				}
				?>
				>
				<div id="subnav_wrap" class="container_12">

					<!-- start sub nav list -->
					<div id="subnav" class="grid_12">
						<ul>
							<li><a href="/admin/payments/" <? if($this->params['controller']=="admin" && $this->params['action']=="payments") echo 'class="active"'; ?>>Payments</a></li>
							<li><a href="/admin/paymentsbyday/" <? if($this->params['controller']=="admin" && $this->params['action']=="paymentsbyday") echo 'class="active"'; ?>>Payments By Day</a></li>
							<li><a href="/admin/growth/" <? if($this->params['controller']=="admin" && $this->params['action']=="growth") echo 'class="active "'; else echo 'class=""'; ?>>Growth</a></li>
							<li><a href="/admin/referralcredits/" <? if($this->params['controller']=="admin" && $this->params['action']=="referralcredits") echo 'class="active sub_nav_last"'; else echo 'class="sub_nav_last"'; ?>>Referral Credits</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div id="member_sub_nav" 
				<? if($this->params['controller']=="admin" && 
					($this->params['action']=="allmembers" or $this->params['action']=="members"  or $this->params['action']=="inactivemembers"  or $this->params['action']=="expiredtoken"  
					or $this->params['action']=="threestrikesmembers"  or $this->params['action']=="publishing"   or $this->params['action']=="fbpword" 
					or $this->params['action']=="reach_by_city"  or $this->params['action']=="deletedmembers"  or $this->params['action']=="growth"  or $this->params['action']=="cancelrequests"
					or $this->params['action']=="free" or $this->params['action']=="paid" or $this->params['action']=="failedpayment" or $this->params['action']=="affiliate"
					or $this->params['action']=="canceled" or $this->params['action']=="paidcanceled")) echo ' style="display:block;"';
					
					else echo ' style="display:none;" ';
				
				?>
				>
				<div id="subnav_wrap" class="container_12">

					<!-- start sub nav list -->
					<div id="subnav" class="grid_12">

						<ul>
							<li><a href="/admin/allmembers/" <? if($this->params['controller']=="admin" && $this->params['action']=="allmembers") echo 'class="active"'; ?>>All</a></li>
							<li><a href="/admin/paid/" <? if($this->params['controller']=="admin" && $this->params['action']=="paid") echo 'class="active"'; ?>>Paid</a></li>
							<li><a href="/admin/free/" <? if($this->params['controller']=="free" && $this->params['action']=="free") echo 'class="active"'; ?>>Free</a></li>
							<li><a href="/admin/failedpayment/" <? if($this->params['controller']=="admin" && $this->params['action']=="failedpayment") echo 'class="active"'; ?>>Failed Payment</a></li>
							<li><a href="/admin/members/" <? if($this->params['controller']=="admin" && $this->params['action']=="members") echo 'class="active"'; ?>>Active</a></li>
							<li><a href="/admin/inactivemembers/" <? if($this->params['controller']=="admin" && $this->params['action']=="inactivemembers") echo 'class="active"'; ?>>Inactive</a></li>
							<li><a href="/admin/expiredtoken/" <? if($this->params['controller']=="admin" && $this->params['action']=="expiredtoken") echo 'class="active"'; ?>>Expired Token</a></li>
							<li><a href="/admin/threestrikesmembers/" <? if($this->params['controller']=="admin" && $this->params['action']=="threestrikesmembers") echo 'class="active"'; ?>>3 Strikes</a></li>
							<!-- <li><a href="/admin/publishing/" <? if($this->params['controller']=="admin" && $this->params['action']=="publishing") echo 'class="active"'; ?>>Pub Assign</a></li> -->
							<li><a href="/publishsettings/index/" <? if($this->params['controller']=="publishsettings" && $this->params['action']=="index") echo 'class="active"'; ?>>Pub Settings</a></li>
							<li><a href="/admin/fbpword/" <? if($this->params['controller']=="admin" && $this->params['action']=="fbpword") echo 'class="active"'; ?>>FB Pword</a></li>
							<li><a href="/admin/twpword/" <? if($this->params['controller']=="admin" && $this->params['action']=="twpword") echo 'class="active"'; ?>>TW Pword</a></li>
							<li><a href="/admin/reach-by-city/" <? if($this->params['controller']=="admin" && $this->params['action']=="reach_by_city") echo 'class="active"'; ?>>Reach By City</a></li>
							<li><a href="/admin/deletedmembers/" <? if($this->params['controller']=="admin" && $this->params['action']=="deletedmembers") echo 'class="active"'; ?>>Deleted</a></li>
							<li><a href="/admin/growth/" <? if($this->params['controller']=="admin" && $this->params['action']=="growth")  echo 'class="active sub_nav_last"'; else echo 'class="sub_nav_last"'; ?>>Growth</a></li>
							<li><a href="/admin/cancelrequests/" <? if($this->params['controller']=="admin" && $this->params['action']=="cancelrequests")  echo 'class="active sub_nav_last"'; else echo 'class="sub_nav"'; ?>>Cancel Request</a></li>
							<li><a href="/admin/affiliate/" <? if($this->params['controller']=="admin" && $this->params['action']=="affiliate")  echo 'class="active sub_nav_last"'; else echo 'class="sub_nav_last"'; ?>>Affilate IDs</a></li>
							<!-- <li><a href="/admin/inactivemembers/" <? if($this->params['controller']=="admin" && $this->params['action']=="inactivemembers") echo 'class="active sub_nav_last"'; else echo 'class="sub_nav_last"'; ?>>Inactive</a></li> -->
						</ul>

					</div>
					<!-- end subnavigation list -->
				</div>
			</div>
			<!-- end sub_nav -->
			*/ ?>






		





		<!-- MAIN CONTENT -->
		<div id="content_wrapper" class="<?=($this->controller_name=="index")?'home ':''?>">


			<? if($this->params['action'] != "gethopper") : ?>

				<form action="/city/changecity/" method="POST" name="change_city" id="change_city">
				<div class="container_12">
					<div class="grid_12" style="text-align: right;">
					<? if($_SESSION['member'] && count($this->cities)>0) : ?>
	
						<div id="cityselection" style="float: right;">
							<select name="admin_city" id="admin_city" style="margin-top: 25px;width: 200px; font-size: 30px; font-weight: bold; width: 300px;">
							<option value="*" <?php echo (empty($this->admin_selectedCity) || $this->admin_selectedCity == "*") ? "selected" : "";?>>All Cities</option>
	
							<?php foreach($this->cities as $city): ?>
								<option value="<?php echo $city->id;?>" <?php echo (!empty($this->admin_selectedCity) && $this->admin_selectedCity == $city->id) ? "selected" : "";?>><?php echo $city->name . ", " . $city->state;?></option>
							<?php endforeach; ?>
						</select>
						</div>
	
						<?php endif ;?>
						&nbsp;
					</div>
				</div>
				<div class="clearfix"></div>
				</form>
			
			<? endif; ?>


			<?=$this->layout()->content; ?>

		</div>


		<!-- END MAIN CONTENT -->
		<div id="footer_wrapper">

			<!-- START FOOTER -->
			<div id="footer" class="container_12 clearfix">
				<div class="grid_12">
					<p>&copy; Copyright 2010 <a href="/index"><?=COMPANY_WEBSITE;?></a></p>
				</div>
			</div>
			<!-- END FOOTER -->

		</div>


		<div id="popup" class="popup_container"></div>


		<script type="text/javascript">
		$(document).ready(function()
		{
			$("#inventory_unit").change(function (eventObject)
			{
				window.location.href = "/admin/index/"+$("#inventory_unit option:selected").val();
			});

			$("#admin_city").change(function(e)
			{
				$("#change_city").submit();
			});


			$("#publisher").click(function(e)
			{
				e.preventDefault();
				$("#member_sub_nav").hide();
				$("#interactions_sub_nav").hide();
				$("#payment_sub_nav").hide();
				
				window.location = $('#publisher').attr('href');
			});


			$("#listings").click(function(e)
			{
				e.preventDefault();
				$("#member_sub_nav").hide();
				$("#interactions_sub_nav").hide();
				$("#payment_sub_nav").hide();
				
				window.location = $('#listings').attr('href');
			});

			$("#posts").click(function(e)
			{
				e.preventDefault();
				$("#member_sub_nav").hide();
				$("#interactions_sub_nav").hide();
				$("#payment_sub_nav").hide();
				window.location = $('#posts').attr('href');
			});

			$("#payments").click(function(e)
			{
				e.preventDefault();
				$("#member_sub_nav").hide();
				$("#interactions_sub_nav").hide();
				$("#payment_sub_nav").show();
				
				//window.location = $('#payments').attr('href');
			});

			$("#cities").click(function(e)
			{
				e.preventDefault();
				$("#member_sub_nav").hide();
				$("#interactions_sub_nav").hide();
				$("#payment_sub_nav").hide();
				
				window.location = $('#cities').attr('href');
			});

			$("#members").click(function(e)
			{
				e.preventDefault();
				
				$("#interactions_sub_nav").hide();
				$("#member_sub_nav").show();
				$("#payment_sub_nav").hide();
				
				window.location = $('#members').attr('href');
			});

			$("#fanpage").click(function(e)
			{
				e.preventDefault();
				$("#member_sub_nav").hide();
				$("#interactions_sub_nav").hide();
				$("#payment_sub_nav").hide();
				
				window.location = $('#fanpage').attr('href');
			});

			$("#stats").click(function(e)
			{
				e.preventDefault();
				$("#member_sub_nav").hide();
				$("#interactions_sub_nav").hide();
				$("#payment_sub_nav").hide();
				
				window.location = $('#stats').attr('href');
			});

			$("#interactions").click(function(e)
			{ 
				e.preventDefault();
				$("#member_sub_nav").hide();
				$("#interactions_sub_nav").show();
				$("#payment_sub_nav").hide();
				
				window.location = $('#interactions').attr('href');
			});

		});
		</script>

	</body>

</html>