<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<?=APP_URL;?>/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">

	<head>
		<?=$this->partial('head.html.php')?>
		<?=$this->headMeta()?>
		
		<? if (isset($this->title) && $this->title) : ?><title><?= $this->title; ?></title><? endif; ?>
		<? if (isset($this->description) && $this->description) : ?><meta name="description" content="<?= $this->description; ?>" /><? endif; ?>
		<? if (isset($this->keyword) && $this->keyword) : ?><meta name="keywords" content="<?= $this->keyword; ?>" /><? endif; ?>
		<? if (isset($this->catonical) && $this->catonical) : ?><link rel="canonical" href="<?= $this->catonical; ?>" /><? endif; ?>
		
		<!-- STYLESHEETS -->
			<? /* <link rel="stylesheet" type="text/css" href="/css/facebox.css" /> */ ?>
			<link type="text/css" rel="stylesheet" href="/css/uploadify.css" />
			<link type="text/css" rel="stylesheet" href="/css/slide.css" />
			<link type="text/css" rel="stylesheet" href="/js/colorbox/colorbox.css" />
			<link type="text/css" rel="stylesheet" href="/css/reset.css" />
			<link type="text/css" rel="stylesheet" href="/css/grid.css" />
			
			<?/*<link type="text/css" rel="stylesheet" href="/css/main.css" />*/?>
			<link type="text/css" rel="stylesheet" href="/css/forms.css" />

			<? /*<link type="text/css" rel="stylesheet" href="/css/autocomplete/jquery.autocomplete.css" />*/?>
			<link type="text/css" rel="stylesheet" href="/css/jquery-ui-1.8.16.custom.css" />


		<link type="text/css" rel="stylesheet" href="/css/common.css" />			
		<link type="text/css" rel="stylesheet" href="/css/mortgagemingler.css" />	


		<!-- END STYLESHEETS -->

		<!-- JAVASCRIPTS -->
			<!-- TYPEKIT -->
			<script type="text/javascript" src="//use.typekit.net/abb8fhm.js"></script>
			<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
			

			<!-- AUTO COMPLETE -->
			<!--<script type="text/javascript" src="/js/autocomplete/jquery.bgiframe.min.js"></script>--><!-- commented as its throwing 404 -->
			<!--<script type="text/javascript" src="/js/autocomplete/jquery.autocomplete.min.js"></script>-->

			<script type="text/javascript">
				var is_facebook_init	= false;
				<?= $this->render_content_for('js'); ?>
			</script>

			<? /* FIREBUG LITE FOR DEBUGGING IN NON-FIREFOX BROWSER
			<script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite.js'></script>
			<script type="text/javascript">
				firebug.env.height = 120;
			</script>
			*/ ?>


		<!-- END JAVASCRIPTS -->

		<!--[if IE 7]>
			<style type="text/css">
				#navigation .refer_agent{
					white-space: nowrap
				}

				.uiSelectWrapper{
					padding-top: 3px;
					padding-bottom: 7px;
				}
				.uiSelectWrapper .uiSelect{
					height: 24px;
					background-color: #eeeeee;
				}

				#navigation_autocomplete .ui-menu .ui-menu-item{
					width: 244px;
				}

				#top_menu .my_account_down_arrow{
					margin-top: 8px;
				}
			</style>
		<![endif]-->

		<? /**************************
		<script type="text/javascript">
		(function() {
		window.fireflyAPI = {};
		    fireflyAPI.token = "506206504ef500670a002913";

		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "https://firefly-071591.s3.amazonaws.com/scripts/loaders/loader.js";
		script.async = true;
		var firstScript = document.getElementsByTagName("script")[0];
		firstScript.parentNode.insertBefore(script, firstScript);
		})();
		</script>
		******************************/ ?>

		
<? /* FRESH DESK */ ?>
<script type="text/javascript" src="http://assets.freshdesk.com/widget/freshwidget.js"></script>
<style type="text/css" media="screen, projection">
	@import url(http://assets.freshdesk.com/widget/freshwidget.css); 
</style> 
<script type="text/javascript">
	FreshWidget.init("", {"queryString": "&formTitle=MortgageMingler+Support&submitThanks=Great.+Our+support+team+will+handle+your+request+quickly.+Typically+within+a+few+hours+(even+on+weekends!+%3A)+", "buttonText": "Questions? Click Here For Quick Help....", "buttonColor": "white", "buttonBg": "#CC0011", "alignment": "1", "offset": "435px", "url": "http://mortgagemingler.freshdesk.com", "assetUrl": "http://assets.freshdesk.com/widget"} );
</script>

		
		

	</head>


	<body class="<?=($this->controller_name=="index" && $this->action_name=="index")?'home':''?>">

		<div id="fb-root"></div>
		<!-- this is reserved for navigation autocomplete -->
		<div id="navigation_autocomplete"></div>

		<div class="container_12" id="header_wrapper">

			<div id="header" class="grid_12 clearfix">

				<a href="/" id="logo"></a>

				<div class="followers_wrapper">

					<span class="followers">
						<? if($this->controller_name=="index" && $this->action_name=="clientfinder"): ?>
							<div class="followers_message">"<?=COMPANY_NAME;?> is made for mortgage brokers; by mortgage brokers."</div>
						<? else: ?>
							<span class="followers_message">Our members mingled with</span>
							<span class="followers_number"><?=number_format($this->total_count);?></span>
							<span class="followers_message">potential clients today.</span>
						<? endif; ?>
					</span>

				</div>

				<div id="user_nav">

					<? if (isset($_SESSION['member']) && $_SESSION['member']) : ?>

						<div id="top_menu">

							<? $member = $_SESSION['member'];?>
							<?php
								if(empty($_SESSION['member']->photo)){
									$src = "https://graph.facebook.com/". $_SESSION['member']->uid ."/picture" ;
								}
								else {
									$src = $_SESSION['member']->photo ;
								}
							?>
							<a href="#" class="my_account_text">My Account<img src="/images/my-account-arrow.png" border="0" /></a>
							<a href="#" class="my_account_user_img"><img width="24" height="24" src="<?php echo $src;?>" border="0" /></a>
							<a href="#" class="my_account_down_arrow">
								<img src="/images/expand-arrow.png" border="0" />
							</a>

							<div class="my_account_popup">
								<img src="/images/my-account-on-arrow.png" border="0" />
								<div class="user_info">
									<span class="name">
										<? if(isset($member->first_name)) echo substr ($member->first_name, 0, 1) . ".";  if(isset($member->last_name)) echo " " . $member->last_name; ?>
									</span>
									<span class="email">
										<? if(isset($member->email_override) && ($member->email_override)) $member->email_override; else echo $member->email; ?>
									</span>
									<span class="number">
										<?=$member->phone;?>
									</span>
								</div>
								<div class="user_account">
									<a href="/member/settings"><img width="50" height="50" src="<?php echo $src;?>" border="0" /></a>
									<ul>

										<? if($this->controller_name == "member" && $this->action_name == "settings"): ?>

											<li><a href="javascript:void(0);" onclick="$('#dashboardtab').trigger('click'); $('#top_menu').removeClass('on');">Dashboard</a></li>

											<li><a href="javascript:void(0);" onclick="$('#referagenttab').trigger('click'); $('#top_menu').removeClass('on');">Refer An Agent</a></li>
											<li><a href="javascript:void(0);" onclick="$('#affiliatestab').trigger('click'); $('#top_menu').removeClass('on');">Referrals</a></li>

											<li><a href="javascript:void(0);" onclick="$('#settingtab').trigger('click'); $('#top_menu').removeClass('on');">Settings</a></li>
											<li><a href="javascript:void(0);" onclick="$('#listingstab').trigger('click'); $('#top_menu').removeClass('on');">Listings</a></li>
											<li><a href="javascript:void(0);" onclick="$('#mybookingtab').trigger('click'); $('#top_menu').removeClass('on');">Bookings</a></li>
											<li><a href="javascript:void(0);" onclick="$('#invoicestab').trigger('click'); $('#top_menu').removeClass('on');">Invoices</a></li>

											<li><a href="javascript:void(0);" onclick="$('#myaccounttab').trigger('click'); $('#top_menu').removeClass('on');">My Account</a></li>

										<? else: ?>

											<li><a href="/member/settings#dashboardtab">Dashboard</a></li>

											<li><a href="/member/settings#referagenttab">Refer An Agent</a></li>
											<li><a href="/member/settings#affiliates">Referrals</a></li>

											<li><a href="/member/settings#settingtab">Settings</a></li>
											<li><a href="/member/settings#listingstab">Listings</a></li>
											<li><a href="/member/settings#mybookingtab">Bookings</a></li>
											<li><a href="/member/settings#invoices">Invoices</a></li>

											<li><a href="/member/settings#myaccounttab">My Account</a></li>

										<? endif; ?>

									</ul>
									<div class="clr"></div>
								</div>
								<div class="logout">
									<a href="/member/logout" id="facebook-logout">Log Out</a>
								</div>
							</div>
						</div>

					<? else: ?>
						<div id="top_login">

							<!-- id="open" -->
							<span id="open_login">
								<a href="#" id="facebook-login" class="name facebook-login">Login</a> or <a href="/member/join" class="name">Register</a>
							</span>

						</div>

					<? endif; ?>

				</div>

				<?
					if($this->admin)
					{
						echo "<div style='width: 168px; float: right; padding-top: 4px;'><a href='/admin'>Admin</a></div>";
					}
				?>

			</div>


			<div id="navigation" class="grid_12">


			</div>

		</div>


		<div class="container_12" id="content">

			<?=$this->layout()->content; ?>

		</div>




		<div id="popup" class="popup_container"></div>

				<script>

					$(document).ready(function() {

						//autocomplete
						$('#cityselect').autocomplete({
							appendTo: '#navigation_autocomplete',
							source: "/city/autocomplete",
							minLength: 2,
							//open: function(){
							//	if($.browser.msie && $.browser.version < 8){
							//		$('css3-container', '#navigation_autocomplete').css('left', $('#navigation_autocomplete .ui-autocomplete').css('left'));
							//		$('css3-container', '#navigation_autocomplete').css('top', $('#navigation_autocomplete .ui-autocomplete').css('top'));
							//	}
							//},
							select: function( event, ui ) {
								if(ui.item){
									$('#city').val(ui.item.id);
									var url	    	= "/member/setcity/";
									var pdata    	= "city_id="+ui.item.id;
									$.post(url, pdata, function(data){
										if(data.error==false)
											window.location.reload();
										<? /*else $.colorbox({href:false,innerWidth:300,initialWidth:330,initialHeight:45,html:"<h3>Not possible to change city.</h3>"});*/?>
									},'json')
								}
							},
							change:function(event, ui)
							{
								if(ui.item){
								}else{
        							$('#city').val('');
        							$(this).val('');
									$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oooops.</h3><p>You must select a known city from the list available in the dropdown!</p>"});
								}

							}

						}).focus(function() {
							if ($(this).val().search(/Select Your City/) != -1) {
								$(this).val('');
							}
						}).blur(function() {
							if ($(this).val() == '') {
								$(this).val('Select Your City');
							}
						});

						$('#top_menu').children('a').click(function(e){
							$(this).parent().toggleClass('on');
						});

					});
				</script>


		<script type="text/javascript"  language="javascript">
			//to apply stlye from main .box :last-child for IE7, 8
			$(document).ready(function() {
				if($.browser.msie && $.browser.version < 9){
					$('.box :last-child').css('margin-bottom', '0px');
				}
			});
		</script>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?=GOOGLE_ANALYTICS;?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>



		<!-- Correct fb_xd_fragment Bug Start -->
		<script type="text/javascript">
		document.getElementsByTagName('html')[0].style.display='block';
		</script>
		<!-- Correct fb_xd_fragment Bug End -->


		<?=$this->partial('bodyend.html.php')?>
	</body>

</html>
