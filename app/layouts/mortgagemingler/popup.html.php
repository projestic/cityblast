<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link type="text/css" rel="stylesheet" href="/stylesheets/popup.css" />
	<script type="text/javascript" src="<?php echo $this->documentRoot;?>/javascripts/mootools-1.2.4-core-yc.js.php"></script>
	<script type="text/javascript" src="<?php echo $this->documentRoot;?>/javascripts/mootools-1.2.4.2-more.js.php"></script>
	<script type="text/javascript" src="<?php echo $this->documentRoot;?>/javascripts/popup.js.php"></script>
	<script type="text/javascript" src="<?php echo $this->documentRoot;?>/javascripts/main.js"></script>
</head>
<body id="wrapper">
	<!-- MAIN CONTENT -->
	<?= $this->layout()->content ?>
	<!-- END MAIN CONTENT -->
	<script src="https://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php" type="text/javascript"></script>
	<script>
	FB_RequireFeatures(['XFBML'], function(){ FB.Facebook.init('<?= API_KEY ?>', '/xd_receiver.htm'); }); 
	</script>


<script src="https://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php" type="text/javascript"></script>
<? if(APPLICATION_ENV == 'production'): ?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("<?=GOOGLE_ANALYTICS;?>");
pageTracker._trackPageview();
} catch(err) {}</script>
<? endif; ?>
	
</body>
</html>
