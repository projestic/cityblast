<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<?=APP_URL;?>/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
	
	<head>
		<?=$this->partial('head.html.php')?>
		<?=$this->headMeta()?>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<? if (isset($this->title) && $this->title) : ?><title><?= $this->title; ?></title><? endif; ?>
		<? if (isset($this->description) && $this->description) : ?><meta name="description" content="<?= $this->description; ?>" /><? endif; ?>
		<? if (isset($this->keyword) && $this->keyword) : ?><meta name="keywords" content="<?= $this->keyword; ?>" /><? endif; ?>
		<? if (isset($this->catonical) && $this->catonical) : ?><link rel="canonical" href="<?= $this->catonical; ?>" /><? endif; ?>
		
		<!-- STYLESHEETS -->
			<? /* <link rel="stylesheet" type="text/css" href="<?php echo $this->documentRoot;?>/css/facebox.css" /> */ ?>
			<link type="text/css" rel="stylesheet" href="/css/slide.css" />
			<link type="text/css" rel="stylesheet" href="<?php echo trim($this->documentRoot);?>/js/colorbox/colorbox.css" />
		
			
			<? /*<link type="text/css" rel="stylesheet" href="<?php echo $this->documentRoot;?>/css/autocomplete/jquery.autocomplete.css" />*/?>			
			<link type="text/css" rel="stylesheet" href="<?php echo $this->documentRoot;?>/css/jquery-ui-1.8.16.custom.css" />
			

			<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css" />
			<link type="text/css" rel="stylesheet" href="/css/responsive.min.css" />
			
			<link type="text/css" rel="stylesheet" href="/css/common.css" />
			<link type="text/css" rel="stylesheet" href="/css/main.css" />
			<link type="text/css" rel="stylesheet" href="/css/forms.css" />


		    <!--[if lt IE 8]><div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div><![endif]-->
		  	<!--[if lt IE 9]>
			   	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			    <link href="/css/ie.css" rel="stylesheet" type="text/css">
		    <![endif]-->
    
			
		<?php echo $this->headStyle() ?>
		<?php echo $this->headLink() ?>
	
			
		<!-- END STYLESHEETS -->
	
	
	
		<!-- JAVASCRIPTS -->

			<!-- FACEBOOK -->
			<script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>
			

			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
			<script type="text/javascript" src="/js/main.js"></script>
			
			<!-- SLIDING EFFECT -->
			<script type="text/javascript" src="/js/slide.js"></script>
			
			<!-- COLOR BOX -->
			<script type="text/javascript" src="/js/colorbox/jquery.colorbox-min.js"></script>
	
			<script src="/js/ui/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
			
	
			<!-- TYPEKIT -->
			<script type="text/javascript" src="https://use.typekit.com/<?=TYPEKIT;?>.js"></script>
			<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


		
			<script type="text/javascript">

				var is_facebook_init	= false;

				<?= $this->render_content_for('js'); ?>
			</script>
			
			<? /* FIREBUG LITE FOR DEBUGGING IN NON-FIREFOX BROWSER
			<script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite.js'></script>
			<script type="text/javascript">
				firebug.env.height = 120;
			</script>
			*/ ?>

		    <script type="text/javascript">
		    var _kmq = _kmq || [];
		    var _kmk = _kmk || '162b3d5e9e4e1a42c4a23865f817e2e2f0631ab6';
		    function _kms(u){
		    setTimeout(function(){
		    var d = document, f = d.getElementsByTagName('script')[0],
		    s = d.createElement('script');
		    s.type = 'text/javascript'; s.async = true; s.src = u;
		    f.parentNode.insertBefore(s, f);
		    }, 1);
		    }
		    _kms('//i.kissmetrics.com/i.js');
		    _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
		    </script>
			
		<!-- END JAVASCRIPTS -->
		
		<? /* THESE SHOULDN'T BE NECESSARY, BUT JUST IN CASE <style type="text/css">
	
			.clearfix:after {
				content: " ";
				display: block;
				height: 0;
				clear: both;
				visibility: hidden;
			}
		
		</style>
		
		<!--[if IE]>
			<style type="text/css">
				.clearfix {
					zoom: 1;
				}
			</style>
		<![endif]-->
		<!--[if lte IE 7]>
			<style type="text/css">
				.fieldname {
					text-indent: 0px;
				}
			</style>
		<![endif]-->

		<!--[if lt IE 8]>
			<style type="text/css">
				// IE6
				*html .uiButton.IE input{
					// remove padding from left/right
					overflow: visible; 
					// remove the remaining space in IE6
					width:0; 
				}
				
				// IE7
				*:first-child+html .uiButton.IE input{
					//remove padding from left/right
					#overflow: visible;
					#width:auto;
				}
			</style>
		<![endif]-->
		
		
		*/
		
		
		if(isset($this->has_comments) && $this->has_comments)
		{
		
			//Comments ADMINS are Alen, Shaun and Grace...
			$fbadmins[] = "734450163";
			$fbadmins[] = "507956900";
			$fbadmins[] = "1387631581";
			$fbadmins[] = "100004323637534";
			$fbadmins[] = "100004112281200";
			$fbadmins[] = "100002817208869";
			$fbadmins[] = "514203248";
			$fbadmins[] = "510154680";
			$fbadmins[] = "100005394605090";
			$fbadmins[] = "172005537";

			$fbadmins[] = "626558428";	//Ryan
			$fbadmins[] = "1033141267";	//Josh
						
		
			echo '<meta property="fb:admins" content="';
			echo (is_array($fbadmins)) ? implode(",",$fbadmins) :  $fbadmins;
			echo '"/>';			
		}

		if(!empty($this->additional_meta)) 
		{
		    echo $this->additional_meta;
		}
		    
		?>
		
		<!--[if IE 7]>
			<style type="text/css">
				#navigation .refer_agent{
					white-space: nowrap
				}
				
				.uiSelectWrapper{
					padding-top: 3px;
					padding-bottom: 7px;
				}
				.uiSelectWrapper .uiSelect{
					height: 24px;
					background-color: #eeeeee;
				}
				
				#navigation_autocomplete .ui-menu .ui-menu-item{
					width: 244px;
				}
			</style>
		<![endif]-->
		
		<?php echo $this->headScript() ?>


		
<? /* FRESH DESK */ ?>
<script type="text/javascript" src="http://assets.freshdesk.com/widget/freshwidget.js"></script>
<style type="text/css" media="screen, projection">
	@import url(http://assets.freshdesk.com/widget/freshwidget.css); 
</style> 
<script type="text/javascript">
	FreshWidget.init("", {"queryString": "&formTitle=MortgageMingler+Support&submitThanks=Great.+Our+support+team+will+handle+your+request+quickly.+Typically+within+a+few+hours+(even+on+weekends!+%3A)+", "buttonText": "Questions? Click Here For Quick Help....", "buttonColor": "white", "buttonBg": "#CC0011", "alignment": "1", "offset": "435px", "url": "http://mortgagemingler.freshdesk.com", "assetUrl": "http://assets.freshdesk.com/widget"} );
</script>

		


	</head>

	
	<body class="Landing">

		<div id="fb-root"></div>
		<!-- this is reserved for navigation autocomplete -->
		<div id="navigation_autocomplete"></div>

		<div class="container" id="header_wrapper">

			<div id="header" class="span_12 clearfix">

				<a href="/" id="logo"></a>

				<div class="followers_wrapper">

					<span class="followers">
						<? if($this->controller_name=="index" && $this->action_name=="cityblast"): ?>
							<div class="followers_message">"<?=COMPANY_NAME;?> is made for real estate agents; by real estate agents."</div>
						<? else: ?>
							<span class="followers_message"><?=COMPANY_NAME;?> agents reached</span>
							<span class="followers_number"><?=number_format($this->total_count);?></span>
							<span class="followers_message">potential home buyers today.</span>
						<? endif; ?>
					</span>

				</div>

				<div id="user_nav">

					<? if(isset($_SESSION['member']) && $_SESSION['member']) : ?>

						<div id="top_menu">

							<? $member = $_SESSION['member'];?>
							<?php
								if(empty($_SESSION['member']->photo)){
									$src = "https://graph.facebook.com/". $_SESSION['member']->uid ."/picture" ;
								}
								else {
									$src = CDN_URL . $_SESSION['member']->photo ;
								}
							?>
							<a href="#" class="my_account_text">My Account<img src="/images/my-account-arrow.png" border="0" /></a>
							<a href="#" class="my_account_user_img"><img width="24" height="24" src="<?php echo $src;?>" border="0" /></a>
							<a href="#" class="my_account_down_arrow">
								<img src="/images/expand-arrow.png" border="0" />
							</a>

							<div class="my_account_popup">
								<img src="/images/my-account-on-arrow.png" border="0" />
								<div class="user_info">
									<span class="name">
										<? if(isset($member->first_name)) echo substr ($member->first_name, 0, 1) . ".";  if(isset($member->last_name)) echo " " . $member->last_name; ?>
									</span>
									<span class="email">
										<? if(isset($member->email_override) && ($member->email_override)) $member->email_override; else echo $member->email; ?>
									</span>
									<span class="number">
										<?=$member->phone;?>
									</span>
								</div>
								<div class="user_account">
									<a href="/member/settings"><img width="50" height="50" src="<?php echo $src;?>" border="0" /></a>
									<ul>

										<? if($this->controller_name == "member" && $this->action_name == "settings"): ?>

											<li><a href="javascript:void(0);" onclick="$('#dashboardtab').trigger('click'); $('#top_menu').removeClass('on');">Dashboard</a></li>

											<li><a href="javascript:void(0);" onclick="$('#referagenttab').trigger('click'); $('#top_menu').removeClass('on');">Refer An Agent</a></li>
											<li><a href="javascript:void(0);" onclick="$('#affiliatestab').trigger('click'); $('#top_menu').removeClass('on');">Referrals</a></li>

											<li><a href="javascript:void(0);" onclick="$('#settingtab').trigger('click'); $('#top_menu').removeClass('on');">Settings</a></li>
											<li><a href="javascript:void(0);" onclick="$('#listingstab').trigger('click'); $('#top_menu').removeClass('on');">Listings</a></li>
											<li><a href="javascript:void(0);" onclick="$('#mybookingtab').trigger('click'); $('#top_menu').removeClass('on');">Bookings</a></li>
											<li><a href="javascript:void(0);" onclick="$('#invoicestab').trigger('click'); $('#top_menu').removeClass('on');">Invoices</a></li>

											<li><a href="javascript:void(0);" onclick="$('#myaccounttab').trigger('click'); $('#top_menu').removeClass('on');">My Account</a></li>

										<? else: ?>

											<li><a href="/member/settings#dashboardtab">Dashboard</a></li>

											<li><a href="/member/settings#referagenttab">Refer An Agent</a></li>
											<li><a href="/member/settings#affiliates">Referrals</a></li>

											<li><a href="/member/settings#settingtab">Settings</a></li>
											<li><a href="/member/settings#listingstab">Listings</a></li>
											<li><a href="/member/settings#mybookingtab">Bookings</a></li>
											<li><a href="/member/settings#invoices">Invoices</a></li>

											<li><a href="/member/settings#myaccounttab">My Account</a></li>

										<? endif; ?>

									</ul>
									<div class="clr"></div>
								</div>
								<div class="logout">
									<a href="/member/logout" id="facebook-logout">Log Out</a>
								</div>
							</div>
						</div>

					<? else: ?>
						<div id="top_login">

							<!-- id="open" -->
							<span id="open_login">
								<a href="#" id="facebook-login" class="name facebook-login">Login</a> or <a href="/member/join" class="name">Register</a>
							</span>

						</div>

					<? endif; ?>

				</div>

				<?
					if($this->admin)
					{
						echo "<div style='width: 168px; float: right; padding-top: 4px;'><a href='/admin'>Admin</a></div>";
					}
				?>

			</div>


			<div id="navigation" class="span_12">


			</div>

		</div>
		
		
		<div class="container" id="content" style="margin-top: 20px;">
			
			<?=$this->layout()->content; ?>
			
		</div>
		
		<div class="container">
			
			<div class="span12" id="footer" style="margin-left: 0px !important; margin-right: 0px !important;">
			
				<div class="box noheading <?=($this->controller_name=="index" && $this->action_name=="index")?'black60':'black40'?>">

					<a href="/member/index">Register</a>
					<a href="/index/how-blasting-works">How Blasting Works</a>					
					<a href="/member/how-clientfinder-works">How MortgageMingler Works</a>
					<a href="/index/guarantee">Our Guarantee</a>
					<a href="/index/refer-an-agent">Refer an Agent</a>																			
					<? /*<a href="/index/feedback">Feedback</a> */?>
					<a href="/index/privacy">Privacy Policy</a>
					<a href="/index/terms">Terms of Use</a>
					<a href="/index/about">About <?=COMPANY_NAME;?></a>
					<a href="/index/contact-us">Contact Us</a>
					
				</div>
			

				
			</div>
			
		</div>
		
		<div id="popup" class="popup_container"></div>
		
				<script>
					
					$(document).ready(function() {
																			
						//autocomplete
						$('#cityselect').autocomplete({
							appendTo: '#navigation_autocomplete',
							source: "/city/autocomplete",
							minLength: 2,
							//open: function(){
							//	if($.browser.msie && $.browser.version < 8){
							//		$('css3-container', '#navigation_autocomplete').css('left', $('#navigation_autocomplete .ui-autocomplete').css('left'));
							//		$('css3-container', '#navigation_autocomplete').css('top', $('#navigation_autocomplete .ui-autocomplete').css('top'));
							//	}
							//},
							select: function( event, ui ) {
								if(ui.item){
									$('#city').val(ui.item.id);
									var url	    	= "/member/setcity/";
									var pdata    	= "city_id="+ui.item.id;
									$.post(url, pdata, function(data){
										if(data.error==false)
											window.location.reload();
										<? /*else $.colorbox({href:false,innerWidth:300,initialWidth:330,initialHeight:45,html:"<h3>Not possible to change city.</h3>"});*/?>
									},'json')
								}
							},
							change:function(event, ui)
							{
								if(ui.item){
								}else{
        							$('#city').val('');
        							$(this).val('');
									$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oooops.</h3><p>You must select a known city from the list available in the dropdown!</p>"});
								}
				
							}							

						}).focus(function() {
							if ($(this).val().search(/Select Your City/) != -1) {
								$(this).val('');
							}
						}).blur(function() {
							if ($(this).val() == '') {
								$(this).val('Select Your City');
							}
						});
																		
					});
				</script>

		
		<script type="text/javascript"  language="javascript">
			//to apply stlye from main .box :last-child for IE7, 8
			$(document).ready(function() {
				if($.browser.msie && $.browser.version < 9){
					$('.box :last-child').css('margin-bottom', '0px');
				}
			});
		</script>


		<script type="text/javascript">
			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
			document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script> 
		
		<script type="text/javascript">
			try {
				var pageTracker = _gat._getTracker("<?=GOOGLE_ANALYTICS;?>");
				pageTracker._trackPageview();
			} catch(err) {}
		</script>
	
		
		<!-- Correct fb_xd_fragment Bug Start -->
		<script type="text/javascript">
		document.getElementsByTagName('html')[0].style.display='block';
		</script>
		<!-- Correct fb_xd_fragment Bug End -->


		<?=$this->partial('bodyend.html.php')?>
	</body>
	
</html>