<div class="container footer">
	<div class="row-fluid">
		<div class="span7">
			<div class="leftContent">
				<ul class="nav nav-pills">
					<? if(strtolower(COMPANY_NAME) == "cityblast") : ?><li><a href="/listing/index">Search.</a></li><?php endif; ?>
					
					<? if(strtolower(COMPANY_NAME) == "cityblast") : ?><li><a href="/blog">Blog.</a></li><? endif; ?>
					
					<li><a href="/index/api">Developer API.</a></li>
					<? if(strtolower(COMPANY_NAME) == "cityblast") : ?><li><a href="/index/how-blasting-works">Blasting.</a></li><?php endif; ?>
					
					<li><a href="/index/terms">Terms.</a></li>
					<li><a href="/index/privacy">Privacy.</a></li>
					
					<li><a href="/help">Help.</a></li>
				</ul>
				<p>&copy; <?=date('Y');?> <?=COMPANY_WEBSITE;?>. All rights reserved.</p>
			</div>
		</div>
		<div class="span5">
			<div class="rightContent">
				<p>Our Social Experts are your Social Media Assistants.</p>
				<p>Tell us how you want your Facebook, Twitter & LinkedIn to look.</p>
				<a href="/index" class="quote">We Do the Rest. Find Out More.</a>
			</div>
		</div>
	</div>
</div>
