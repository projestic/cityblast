<div class="container footerMap">
	<div class="contact clearfix">
		<div class="contactTopBorder"></div>
		<div class="pull-left"><span class="phone"><?=TOLL_FREE;?></span> / 9 - 5 EDT</div>
		<div class="pull-right"><span>642 King Street West &#183; Toronto, ON, M5V 1M7 &#183; <a href="mailto:info@<?=strtolower(COMPANY_NAME);?>.com">info@<?=strtolower(COMPANY_NAME);?>.com</a></span></div>
	</div>
	<div class="map">
		<a href="https://maps.google.ca/maps?q=642+king+street+west+toronto&oe=utf-8&client=firefox-a&channel=fflb&hnear=642+King+St+W,+Toronto,+Ontario+M5V+1M7&gl=ca&t=m&z=16" target="_blank"><img alt="map" src="/images/new-images/map.png" class="img-responsive"/></a>
	</div>
</div>


<? $this->stats = LifetimeStats::find(1); ?>



<div class="container">
	<div class="footer-stats">
		<div class="topBorder"></div>
		<div class="row-fluid">
			<div class="span4 stat">
				<strong><?=number_format($this->member_count);?></strong> Members
			</div>
			<div class="span4 stat">
				<strong><?=number_format(	($this->stats->total_interactions + $this->stats->total_bookings + $this->stats->total_likes + $this->stats->total_comments) );?></strong> Leads
			</div>
			<div class="span4 stat">
				<strong><?=number_format($this->stats->total_posts);?></strong> Posts

			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="footer-menus">
		<div class="row-fluid">
			<div class="span5th footer-menu clearfix">
				<img src="/images/blank.gif" class="footer-menu-icon cityblast" />
				<h5><?= COMPANY_NAME ?>.</h5>
				
				<? if(strtolower(COMPANY_NAME) != "myeventvoice") : ?>
					<a href="/index/why-us">Why <?= COMPANY_NAME ?></a>
				<? endif; ?>
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?><a href="/index/how-blasting-works">Blasting</a><?php endif; ?>
				<a href="/index/pricing">Pricing</a>
				<?/*<a href="/index/why-cityblast">How it Works</a>*/?>
				
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<a href="/blog">Blog</a>
				<? endif; ?>
				
			</div>
			<div class="span5th footer-menu clearfix">
				<img src="/images/blank.gif" class="footer-menu-icon about" />
				<h5>About.</h5>
				
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<a href="/index/our-story">Our Story</a>
				<? endif; ?>
				
				<a href="/index/faq">FAQ</a>
				<?/*<a href="/index/press">Press Releases</a>*/?>
				<a href="/index/privacy">Privacy Policy</a>
				<a href="/index/terms">Terms and Conditions</a>
				
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<a href="/press/index">Press</a>
				<? endif; ?>
				
				<a href="/jobs/index">We're Hiring <span style="color: #790000;">*</span></a>
				
			</div>
			
			

			<div class="span5th footer-menu clearfix">
				<img src="/images/blank.gif" class="footer-menu-icon help" />
				<h5>Help.</h5>
				<a href="/help/index">Help</a>
				
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<a href="/listing/index">Search</a>
				<? endif; ?>
				
				
				<?/*<a href="xxx">Sitemap</a>*/?>
			</div>
		
			
			<div class="span5th footer-menu clearfix">
				<img src="/images/blank.gif" class="footer-menu-icon developers" />
				<h5>Developers.</h5>
				<a href="/index/api">REST API</a>
				<?/*<a href="/index/idx">IDX and Data Feeds</a>*/?>
				<a href="/api/index">Documentation</a>
			</div>
			
			
			<div class="span5th footer-menu clearfix">
				<img src="/images/blank.gif" class="footer-menu-icon approved" />
				<h5>Get Approved.</h5>
				<a href="/affiliate/signup">Marketing Partner</a>
				<a href="/index/training">Training Affiliate</a>
				<a href="/index/content">Content Provider</a>
				
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<a href="/index/idx">Franchise Partner</a>
				<? endif; ?>
					
				<a href="/index/ambassador">Brand Advocate</a>
				
			</div>
			
		</div>
	</div>
</div>

<div class="container">
	<div class="footer-social">
		<div class="topBorder"></div>
		<div class="row-fluid clearfix">
			<div class="span8">
				<h3><span>We Do the Rest.</span><br /> <span>Find Out More.</span></h3>
				<p>Our Social Experts are your Social Media Assistants.</p>
				<p>Tell us how you want your Facebook, Twitter &amp; LinkedIn to look.</p>
			</div>
			<div class="span4 footer-social-icons">

				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>

					<a href="//www.facebook.com/CityBlast" class="footer-social-icon" target="_new">
						<i class="icon icon-facebook"></i>
					</a>
					<a href="//twitter.com/<?=TWITTER_USERNAME;?>" class="footer-social-icon" target="_new">
						<i class="icon icon-twitter"></i>
					</a>
				
					<a href="//www.youtube.com/user/CityBlastMedia" class="footer-social-icon" target="_new">
						<i class="icon icon-youtube"></i>
					</a>
				<?php elseif (COMPANY_NAME == 'MortgageMingler') : ?>
					<a href="//www.facebook.com/mortgagemingler" class="footer-social-icon" target="_new">
						<i class="icon icon-facebook"></i>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="wePostContainer clearfix">
		<ul class="list-unstyled pull-left wePost">
			<li><a href="http://www.cityblast.com">CityBlast</a></li>
			<li><a href="http://www.mortgagemingler.com">MortgageMingler</a></li>
			<li><a href="http://www.myeventvoice.com">MyEventVoice</a></li>
			<?/*<li><a href="http://www.insuranceposters.com">InsurancePosters</a></li>*/?>
			<?/*<li><a href="http://www.socialcapit.al">SocialCapital</a></li>*/?>
		</ul>
		<a class="pull-right wePostWeb" href="http://www.wepostsocial.com">WePostSocial.com</a>
	</div>
</div>
<div class="container">
	<div class="footer-copyright">
		<div class="row-fluid">
			<div class="span8">
				&copy; <?php echo strftime( '%Y' ); ?> <?=COMPANY_NAME;?>.com. All rights reserved.
			</div>
			<div class="span4">
				<img src="/images/to-top.png" class="to-top pull-right" />
				<span class="pull-right">To the top</span>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		$('.to-top').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	});
</script>

</body>
</html>