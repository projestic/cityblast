
		<?php if ($this->flash_success || $this->flash_error) : ?>
		<div class="container flash-message">
			<?php foreach ($this->flash_error as $message) : ?> 
			<div class="alert alert-danger"><?= $message ?></div>
			<?php endforeach; ?>
			<?php foreach ($this->flash_success as $message) : ?> 
			<div class="alert alert-success"><?= $message ?></div>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
