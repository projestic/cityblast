<?php

echo $this->partial('copperegg.html.php');

$app_config = Zend_Registry::get('appConfig');
$network_app_facebook = Zend_Registry::get('networkAppFacebook');
$network_app_linkedin = Zend_Registry::get('networkAppLinkedIn');
?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?=APP_URL;?>/favicon-<?= strtolower(COMPANY_NAME) ?>.ico" type="image/x-icon" />
<?php
if(isset($this->has_comments) && $this->has_comments) {		
	//Comments ADMINS are Alen, Shaun and Grace...
	$fbadmins[] = "734450163";
	$fbadmins[] = "507956900";
	$fbadmins[] = "1387631581";
	$fbadmins[] = "100004323637534";
	$fbadmins[] = "100004112281200";
	$fbadmins[] = "100002817208869";
	$fbadmins[] = "514203248";
	$fbadmins[] = "510154680";
	$fbadmins[] = "100005394605090";
	$fbadmins[] = "172005537";
	

	echo '<meta property="fb:admins" content="';
	echo (is_array($fbadmins)) ? implode(",",$fbadmins) :  $fbadmins;
	echo '"/>';			
}
if(!empty($this->additional_meta)) 
{
	echo $this->additional_meta;
}
?>

<? if ($app_config['visualWebsiteOptimizer']['enabled'] && $app_config['visualWebsiteOptimizer']['accountId']): ?>
<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=<?=$app_config['visualWebsiteOptimizer']['accountId']?>,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
<? endif; ?>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery-ui-1.8.23.custom.min.js" ></script> <? /* PLEASE NOTE THIS IS JQUERY UI --> NOT JQUERY... THIS LIB IS NEEDED */ ?>
<script type="text/javascript" async src="/js/slide.js"></script>
<script type="text/javascript" src="/js/colorbox/jquery.colorbox-min.js"></script>
<?/*<script type="text/javascript" async src="/js/jquery.knob.js" ></script>*/ ?>
<script type="text/javascript" async src="/js/jquery.validate.min.js"></script>
			
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/new-script.js"></script>
<script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>


<script src="//maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script src="/js/jquery.geocomplete.min.js"></script>
<script type="text/javascript" src="//www.google.com/jsapi"></script>

<script>
	$(document).ready(function()
	{
		
		<? if(isset($network_app_linkedin->consumer_id) && !empty($network_app_linkedin->consumer_id)): ?>
		$.getScript("https://platform.linkedin.com/in.js?async=true", function success() {
			IN.init({
				api_key: '<?=$network_app_linkedin->consumer_id;?>',
				authorize: true
			});
		}); 
		<? endif; ?>
		
		var USE_FB_JS_SDK	=   true;
		if(USE_FB_JS_SDK) {
			// avoid multiple initialization
			if (typeof is_facebook_init == 'undefined' || !is_facebook_init)
			{
				FB.init({
					appId  : '<?=$network_app_facebook->consumer_id;?>',
					status : true, // check login status
					cookie : true, // enable cookies to allow the server to access the session
					xfbml  : true, // parse XFBML
					oauth  : true, // enable OAuth 2.0
					channelUrl: 'http://<?php echo $_SERVER['HTTP_HOST'] ?>/channel.php' // channeling JS SDK
				});
				is_facebook_init = true;
			}

			$('#facebook-logout').click(function(event){
				event.preventDefault();
				FB.getLoginStatus(handleSessionResponse);
			});

			// Function to notify backend about the logout
			// When user logs out we logut from Linkedin as well
			function doLogout() {
				$.ajax({			
					type: "POST",
					url: "/member/fblogout",
					data: "",
					dataType: "json",

					success: function(msg)
					{
						var LkdnExists	=	false;
						if(typeof IN != 'undefined' && typeof IN.User != 'undefined') {
							IN.User.refresh();
							LkdnExists	=	true;
						}
						/*
						if( LkdnExists && IN.User.isAuthorized()) {
							IN.User.logout(function(){
								window.location.replace("/");
							});
						} else {
							window.location.replace("/");
						}
						*/
						window.location = '/';
					}
				});
			}

			function handleSessionResponse(response) 
			{
				//if we dont have a session (which means the user has already logged out of FB)
				if (response.status!="connected") 
				{
					doLogout();
				} else {
					FB.logout(function(response)
					{
						doLogout();
					});
				}
			}
		}
		
		$('.facebook-login').click(function(event){
			event.preventDefault();
			FB.login(function(response) {
				if (response.authResponse)
				{
					$.ajax({
						type: "POST",
						url: "/member/fblogin",
						data: "access_token="+response.authResponse.accessToken,
						dataType: "json",
						success: function(msg)
						{
							window.location = msg.redirectBackTo;
						}
					});
				}
			}, {scope: '<?php echo FB_SCOPE;?>'});
		});

	});
</script>
