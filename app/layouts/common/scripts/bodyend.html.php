<?
$app_config = Zend_Registry::get('appConfig');
?>

<? if ($app_config['googleRemarketing']['enabled'] && $app_config['googleRemarketing']['conversionId']): ?>
<!-- Google Code for Remarketing tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = <?=$app_config['googleRemarketing']['conversionId']?>;
var google_conversion_label = "<?=$app_config['googleRemarketing']['conversionLabel']?>";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/<?=urlencode($app_config['googleRemarketing']['conversionId'])?>/?value=0&amp;label=<?=urlencode($app_config['googleRemarketing']['conversionLabel'])?>&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<? endif; ?>