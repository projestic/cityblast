<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<?=APP_URL;?>/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
	
	<head>
		<?=$this->partial('head.html.php')?>
		<?=$this->headMeta()?>
		<meta property="og:image" content="<?=APP_URL;?>/images/new-images/city-blast_logo.png" />
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<? if (isset($this->title) && $this->title) : ?><title><?= $this->title; ?></title><? endif; ?>
		<? if (isset($this->description) && $this->description) : ?><meta name="description" content="<?= $this->description; ?>" /><? endif; ?>
		<? if (isset($this->keyword) && $this->keyword) : ?><meta name="keywords" content="<?= $this->keyword; ?>" /><? endif; ?>
		<? if (isset($this->catonical) && $this->catonical) : ?><link rel="canonical" href="<?= $this->catonical; ?>" /><? endif; ?>
		
		<!-- STYLESHEETS -->
			<? /* <link rel="stylesheet" type="text/css" href="<?php echo $this->documentRoot;?>/css/facebox.css" /> */ ?>
			<link type="text/css" rel="stylesheet" href="/css/slide.css" />
			<link type="text/css" rel="stylesheet" href="<?php echo trim($this->documentRoot);?>/js/colorbox/colorbox.css" />
		
			
			<? /*<link type="text/css" rel="stylesheet" href="<?php echo $this->documentRoot;?>/css/autocomplete/jquery.autocomplete.css" />*/?>			
			<link type="text/css" rel="stylesheet" href="<?php echo $this->documentRoot;?>/css/jquery-ui-1.8.16.custom.css" />
			

			<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css" />
			<link type="text/css" rel="stylesheet" href="/css/responsive.min.css" />
			
			<link type="text/css" rel="stylesheet" href="/css/common.css" />
			<link type="text/css" rel="stylesheet" href="/css/main.css" />
			<link type="text/css" rel="stylesheet" href="/css/forms.css" />


		    <!--[if lt IE 8]><div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div><![endif]-->
		  	<!--[if lt IE 9]>
			   	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
			    <link href="/css/ie.css" rel="stylesheet" type="text/css">
		    <![endif]-->
    
			
		<?php echo $this->headStyle() ?>
		<?php echo $this->headLink() ?>
	
			
		<!-- END STYLESHEETS -->
	
	
	
		<!-- JAVASCRIPTS -->
			
	
			<!-- TYPEKIT -->
			<script type="text/javascript" src="https://use.typekit.com/<?=TYPEKIT;?>.js"></script>
			<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


		
			<script type="text/javascript">

				var is_facebook_init	= false;

				<?= $this->render_content_for('js'); ?>
			</script>
			
			<? /* FIREBUG LITE FOR DEBUGGING IN NON-FIREFOX BROWSER
			<script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite.js'></script>
			<script type="text/javascript">
				firebug.env.height = 120;
			</script>
			*/ ?>

		    <script type="text/javascript">
		    var _kmq = _kmq || [];
		    var _kmk = _kmk || '162b3d5e9e4e1a42c4a23865f817e2e2f0631ab6';
		    function _kms(u){
		    setTimeout(function(){
		    var d = document, f = d.getElementsByTagName('script')[0],
		    s = d.createElement('script');
		    s.type = 'text/javascript'; s.async = true; s.src = u;
		    f.parentNode.insertBefore(s, f);
		    }, 1);
		    }
		    _kms('//i.kissmetrics.com/i.js');
		    _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
		    </script>
			
		<!-- END JAVASCRIPTS -->
		
		<!--[if IE 7]>
			<style type="text/css">
				#navigation .refer_agent{
					white-space: nowrap
				}
				
				.uiSelectWrapper{
					padding-top: 3px;
					padding-bottom: 7px;
				}
				.uiSelectWrapper .uiSelect{
					height: 24px;
					background-color: #eeeeee;
				}
				
				#navigation_autocomplete .ui-menu .ui-menu-item{
					width: 244px;
				}
			</style>
		<![endif]-->
		
		<?php echo $this->headScript() ?>
	</head>

	
	<body class="Landing">

		<div id="fb-root"></div>
		<!-- this is reserved for navigation autocomplete -->
		<div id="navigation_autocomplete"></div>
		

		
		<div class="container" id="content" style="margin-top: 20px;">
			
			<?=$this->layout()->content; ?>
			
		</div>
		
		<div class="container">
			
			<div class="span12" id="footer" style="margin-left: 0px !important; margin-right: 0px !important;">
			
				<div class="box noheading <?=($this->controller_name=="index" && $this->action_name=="index")?'black60':'black40'?>">
					
					<a href="/member/index">Register</a>
					<a href="/member/how-cityblast-works">How CityBlast Works</a>
					<a href="/index/how-blasting-works">How Blasting Works</a>					
					<a href="/index/guarantee">Our Guarantee</a>
					<a href="/index/refer-an-agent">Refer an Agent</a>																			
					<? /*<a href="/index/feedback">Feedback</a> */?>
					<a href="/index/privacy">Privacy Policy</a>
					<a href="/index/terms">Terms of Use</a>
					<a href="/index/about">About <?=COMPANY_NAME;?></a>
					<a href="/index/contact-us">Contact Us</a>
					
				</div>
			

				
			</div>
			
		</div>
		
		<div id="popup" class="popup_container"></div>
		
				<script>
					
					$(document).ready(function() {
																			
						//autocomplete
						$('#cityselect').autocomplete({
							appendTo: '#navigation_autocomplete',
							source: "/city/autocomplete",
							minLength: 2,
							//open: function(){
							//	if($.browser.msie && $.browser.version < 8){
							//		$('css3-container', '#navigation_autocomplete').css('left', $('#navigation_autocomplete .ui-autocomplete').css('left'));
							//		$('css3-container', '#navigation_autocomplete').css('top', $('#navigation_autocomplete .ui-autocomplete').css('top'));
							//	}
							//},
							select: function( event, ui ) {
								if(ui.item){
									$('#city').val(ui.item.id);
									var url	    	= "/member/setcity/";
									var pdata    	= "city_id="+ui.item.id;
									$.post(url, pdata, function(data){
										if(data.error==false)
											window.location.reload();
										<? /*else $.colorbox({href:false,innerWidth:300,initialWidth:330,initialHeight:45,html:"<h3>Not possible to change city.</h3>"});*/?>
									},'json')
								}
							},
							change:function(event, ui)
							{
								if(ui.item){
								}else{
        							$('#city').val('');
        							$(this).val('');
									$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oooops.</h3><p>You must select a known city from the list available in the dropdown!</p>"});
								}
				
							}							

						}).focus(function() {
							if ($(this).val().search(/Select Your City/) != -1) {
								$(this).val('');
							}
						}).blur(function() {
							if ($(this).val() == '') {
								$(this).val('Select Your City');
							}
						});
																		
					});
				</script>

		<script type="text/javascript"  language="javascript">
			//to apply stlye from main .box :last-child for IE7, 8
			$(document).ready(function() {
				if($.browser.msie && $.browser.version < 9){
					$('.box :last-child').css('margin-bottom', '0px');
				}
			});
		</script>


		<script type="text/javascript">
			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
			document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script> 
		
		<script type="text/javascript">
			try {
				var pageTracker = _gat._getTracker("<?=GOOGLE_ANALYTICS;?>");
				pageTracker._trackPageview();
			} catch(err) {}
		</script>
	
		
		<!-- Correct fb_xd_fragment Bug Start -->
		<script type="text/javascript">
		document.getElementsByTagName('html')[0].style.display='block';
		</script>
		<!-- Correct fb_xd_fragment Bug End -->

<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/0184.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>


<script type="text/javascript">                                                                                                            
  (function() {                                                                                                                            
    window._pa = window._pa || {};                                                                                                         
    // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions                                          
    // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions                                                   
    // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads                                             
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;                                               
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/5371216b43fa865d0b000006.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);                                                  
  })();                                                                                                                                    
</script>    

		<?=$this->partial('bodyend.html.php')?>

		
	</body>
	
</html><