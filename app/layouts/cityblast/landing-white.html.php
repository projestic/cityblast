<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
	
	<head>
		<?=$this->partial('head.html.php')?>
		<?=$this->headMeta()?>
		
		<? if (isset($this->title) && $this->title) : ?><title><?= $this->title; ?></title><? endif; ?>
		<? if (isset($this->description) && $this->description) : ?><meta name="description" content="<?= $this->description; ?>" /><? endif; ?>
		<? if (isset($this->keyword) && $this->keyword) : ?><meta name="keywords" content="<?= $this->keyword; ?>" /><? endif; ?>
		<? if (isset($this->catonical) && $this->catonical) : ?><link rel="canonical" href="<?= $this->catonical; ?>" /><? endif; ?>

		<META NAME="WT.z_Connect_Company" CONTENT="<?=COMPANY_NAME;?>">
		<META NAME="WT.z_Connect_Site_Type" CONTENT="3RD Party">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<meta property="og:image" content="<?=APP_URL;?>/images/new-images/city-blast_logo.png" />
		
		<!-- STYLESHEETS -->
			<? /* <link rel="stylesheet" type="text/css" href="/css/facebox.css" /> */ ?>
			<link type="text/css" rel="stylesheet" href="/css/uploadify.css" />
			<link type="text/css" rel="stylesheet" href="/css/slide.css" />
			<link type="text/css" rel="stylesheet" href="/js/colorbox/colorbox.css" />
			<link type="text/css" rel="stylesheet" href="/css/reset.css" />
			<?/*<link type="text/css" rel="stylesheet" href="/css/grid.css" />*/?>
			<?/*<link type="text/css" rel="stylesheet" href="/css/main.css" />*/?>
			<?/*<link type="text/css" rel="stylesheet" href="/css/forms.css" />*/ ?>
			<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css" />
			<link type="text/css" rel="stylesheet" href="/css/bootstrap-responsive.min.css" />
			
			<? /*<link type="text/css" rel="stylesheet" href="/css/autocomplete/jquery.autocomplete.css" />*/?>			
			<link type="text/css" rel="stylesheet" href="/css/jquery-ui-1.8.16.custom.css" />
			<link type="text/css" rel="stylesheet" href="/css/dashboard-common.css" />

			
			<link type="text/css" rel="stylesheet" href="/css/new-style.css" />

			<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
			<!--[if IE 7]>
				<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome-ie7.css" rel="stylesheet">
			<![endif]-->
			
		<!-- END STYLESHEETS -->
	
		<!-- JAVASCRIPTS -->
	
			<!-- TYPEKIT -->
			<script type="text/javascript" src="https://use.typekit.com/<?=TYPEKIT;?>.js"></script>
			<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


			<!-- AUTO COMPLETE -->
			<!--<script type="text/javascript" src="/js/autocomplete/jquery.bgiframe.min.js"></script>--><!-- commented as its throwing 404 -->
			<!--<script type="text/javascript" src="/js/autocomplete/jquery.autocomplete.min.js"></script>-->


			<!-- WEBTRENDS -->
			<script type="text/javascript" src="/js/webtrends.js"></script>
					
			<script type="text/javascript">
				var is_facebook_init	= false;
				<?= $this->render_content_for('js'); ?>
			</script>
			
			<? /* FIREBUG LITE FOR DEBUGGING IN NON-FIREFOX BROWSER
			<script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite.js'></script>
			<script type="text/javascript">
				firebug.env.height = 120;
			</script>


		    <script type="text/javascript">
		    var _kmq = _kmq || [];
		    var _kmk = _kmk || '<?=KISSMETRICS_API_FEY;?>';
		    function _kms(u){
		    setTimeout(function(){
		    var d = document, f = d.getElementsByTagName('script')[0],
		    s = d.createElement('script');
		    s.type = 'text/javascript'; s.async = true; s.src = u;
		    f.parentNode.insertBefore(s, f);
		    }, 1);
		    }
		    _kms('//i.kissmetrics.com/i.js');
		    _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
		    </script>
			*/ ?>
			
						
		<!-- END JAVASCRIPTS -->
		
		<!--[if IE 7]>
			<style type="text/css">
				#navigation .refer_agent{
					white-space: nowrap
				}
				
				.uiSelectWrapper{
					padding-top: 3px;
					padding-bottom: 7px;
				}
				.uiSelectWrapper .uiSelect{
					height: 24px;
					background-color: #eeeeee;
				}
				
				#navigation_autocomplete .ui-menu .ui-menu-item{
					width: 244px;
				}
				
				#top_menu .my_account_down_arrow{
					margin-top: 8px;
				}
			</style>
		<![endif]-->


<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');$.src='//v2.zopim.com/?1XdfJljVrgbgDQuHbwWxia3I8JSy29cd';z.t=+new Date;
$.type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->



	</head>

	
	<body class="<?=($this->controller_name=="index" && $this->action_name=="index")?'home':''?>" style=" background-image: none; background-color: #FFFFFF;">

		<div id="fb-root"></div>
		<!-- this is reserved for navigation autocomplete -->
		<div id="navigation_autocomplete" style="z-index: 10; position: relative;"></div>
		
		<div class="container" id="header_wrapper">

			<div id="header" class="clearfix">
				
				<a href="/" id="logo" style="display: inline-block; margin: 10px 0px 10px -10px; width: 150px !important;"><img src="/images/new-images/city-blast_logo.png" alt="City Blast" /></a>

<? $this->stats = LifetimeStats::find(1); ?>
<span class="tk-open-sans" style="font-size: 12px; font-weight: bold; margin-top: 24px; display:inline-block !important; margin-left: 10px;">
<?=number_format($this->stats->total_posts);?></strong> Posts, 
<?=number_format($this->member_count);?></strong> Members, 
<?=number_format(	($this->stats->total_interactions + $this->stats->total_bookings + $this->stats->total_likes + $this->stats->total_comments) );?> Leads (<i>and counting</i>)
 </span>
				
				<span class="pull-right"><span class="phone"><?=TOLL_FREE;?></span> / 9 - 5 EDT</span>
			
			</div>

		</div>
		
		<div  style="border-bottom: 1px solid #d3d3d3; width: 100% !important; margin-top: 10px;"></div>
		
		
		<div class="container content" id="content" style="margin-top: 5px;">
			
			<?=$this->layout()->content; ?>
			
		</div>
		
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<p style="text-align: center; color: #999;">&copy; 2014 CityBlast.com. All rights reserved.</p>
				</div>
			</div>
		</div>

		<div id="popup" class="popup_container"></div>
		
		<script type="text/javascript">
		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '<?=GOOGLE_ANALYTICS;?>']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>
		
		
		<!-- START OF SmartSource Data Collector TAG -->
		<!-- Copyright (c) 1996-2011 WebTrends Inc.  All rights reserved. -->
		<!-- Version: 9.3.0 -->
		<!-- Tag Builder Version: 3.1  -->
		<!-- Created: 3/22/2011 3:12:41 PM -->
		
		
		<!-- ----------------------------------------------------------------------------------- -->
		<!-- Warning: The two script blocks below must remain inline. Moving them to an external -->
		<!-- JavaScript include file can cause serious problems with cross-domain tracking.      -->
		<!-- ----------------------------------------------------------------------------------- -->
		<script type="text/javascript">
			//<![CDATA[
			var trendVar = "www.cityblast.com";
			var dcsidVar = "dcs6p84o900000w0yf68kq216_9x3g";
			var _tag=new WebTrends();
			_tag.dcsGetId();
			//]]>
		</script>
		<script type="text/javascript">
			//<![CDATA[
			_tag.dcsCustom=function(){
			// Add custom parameters here.
			//_tag.DCSext.param_name=param_value;
			}
			_tag.dcsCollect();
			//]]>
		</script>
		<noscript>
			<div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="//statse.webtrendslive.com/dcs6p84o900000w0yf68kq216_9x3g/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=9.3.0&amp;WT.dcssip=www.city-blast.com"/></div>
		</noscript>
		<!-- END OF SmartSource Data Collector TAG -->

		<!-- Correct fb_xd_fragment Bug Start -->
		<script type="text/javascript">
		document.getElementsByTagName('html')[0].style.display='block';
		</script>
		<!-- Correct fb_xd_fragment Bug End -->


<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/0184.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

<script type="text/javascript">                                                                                                            
  (function() {                                                                                                                            
    window._pa = window._pa || {};                                                                                                         
    // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions                                          
    // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions                                                   
    // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads                                             
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;                                               
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/5371216b43fa865d0b000006.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);                                                  
  })();                                                                                                                                    
</script>    	
	
		<?=$this->partial('bodyend.html.php')?>
                                                                                                                              
	</body>
	
</html>