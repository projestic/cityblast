<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
	
	<head>
		<?=$this->partial('head.html.php')?>
		<?=$this->headMeta()?>
		
		<? if (isset($this->title) && $this->title) : ?><title><?= $this->title; ?></title><? endif; ?>
		<? if (isset($this->description) && $this->description) : ?><meta name="description" content="<?= $this->description; ?>" /><? endif; ?>
		<? if (isset($this->keyword) && $this->keyword) : ?><meta name="keywords" content="<?= $this->keyword; ?>" /><? endif; ?>
		<? if (isset($this->catonical) && $this->catonical) : ?><link rel="canonical" href="<?= $this->catonical; ?>" /><? endif; ?>

		<META NAME="WT.z_Connect_Company" CONTENT="<?=COMPANY_NAME;?>">
		<META NAME="WT.z_Connect_Site_Type" CONTENT="3RD Party">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<meta property="og:image" content="<?=APP_URL;?>/images/socialcapital-logo.png" />
		
		
		<!-- STYLESHEETS -->
			<? /* <link rel="stylesheet" type="text/css" href="/css/facebox.css" /> */ ?>
			<link type="text/css" rel="stylesheet" href="/css/uploadify.css" />
			<link type="text/css" rel="stylesheet" href="/css/slide.css" />
			<link type="text/css" rel="stylesheet" href="/js/colorbox/colorbox.css" />
			<link type="text/css" rel="stylesheet" href="/css/reset.css" />
			<?/*<link type="text/css" rel="stylesheet" href="/css/grid.css" />*/?>
			<?/*<link type="text/css" rel="stylesheet" href="/css/main.css" />*/?>
			<?/*<link type="text/css" rel="stylesheet" href="/css/forms.css" />*/ ?>
			<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css" />
			<link type="text/css" rel="stylesheet" href="/css/bootstrap-responsive.min.css" />
			
			<? /*<link type="text/css" rel="stylesheet" href="/css/autocomplete/jquery.autocomplete.css" />*/?>			
			<link type="text/css" rel="stylesheet" href="/css/jquery-ui-1.8.16.custom.css" />
			<link type="text/css" rel="stylesheet" href="/css/dashboard-common.css" />

			
			<link type="text/css" rel="stylesheet" href="/css/new-style.css" />
			
			<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
			<!--[if IE 7]>
				<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome-ie7.css" rel="stylesheet">
			<![endif]-->
			
		<!-- END STYLESHEETS -->
	
		<!-- JAVASCRIPTS -->

	
			<!-- TYPEKIT -->
			<script type="text/javascript" src="https://use.typekit.com/<?=TYPEKIT;?>.js"></script>
			<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


			<!-- AUTO COMPLETE -->
			<!--<script type="text/javascript" src="/js/autocomplete/jquery.bgiframe.min.js"></script>--><!-- commented as its throwing 404 -->
			<!--<script type="text/javascript" src="/js/autocomplete/jquery.autocomplete.min.js"></script>-->


			<!-- WEBTRENDS -->
			<script type="text/javascript" src="/js/webtrends.js"></script>
					
			<script type="text/javascript">
				var is_facebook_init	= false;
				<?= $this->render_content_for('js'); ?>
			</script>
			
			<? /* FIREBUG LITE FOR DEBUGGING IN NON-FIREFOX BROWSER
			<script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite.js'></script>
			<script type="text/javascript">
				firebug.env.height = 120;
			</script>
			*/ ?>

		    <script type="text/javascript">
		    var _kmq = _kmq || [];
		    var _kmk = _kmk || '<?= (defined('KISSMETRICS_API_KEY') ? KISSMETRICS_API_KEY : '') ?>';
		    function _kms(u){
		    setTimeout(function(){
		    var d = document, f = d.getElementsByTagName('script')[0],
		    s = d.createElement('script');
		    s.type = 'text/javascript'; s.async = true; s.src = u;
		    f.parentNode.insertBefore(s, f);
		    }, 1);
		    }
		    _kms('//i.kissmetrics.com/i.js');
		    _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
		    </script>
			
		<!-- END JAVASCRIPTS -->
		
		<!--[if IE 7]>
			<style type="text/css">
				#navigation .refer_agent{
					white-space: nowrap
				}
				
				.uiSelectWrapper{
					padding-top: 3px;
					padding-bottom: 7px;
				}
				.uiSelectWrapper .uiSelect{
					height: 24px;
					background-color: #eeeeee;
				}
				
				#navigation_autocomplete .ui-menu .ui-menu-item{
					width: 244px;
				}
				
				#top_menu .my_account_down_arrow{
					margin-top: 8px;
				}
			</style>
		<![endif]-->


<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?1XdfJljVrgbgDQuHbwWxia3I8JSy29cd';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->



<?/**********************
 <script type="text/javascript">
(function() {
window.fireflyAPI = {};
    fireflyAPI.token = "506206504ef500670a002913";

var script = document.createElement("script");
script.type = "text/javascript";
script.src = "https://firefly-071591.s3.amazonaws.com/scripts/loaders/loader.js";
script.async = true;
var firstScript = document.getElementsByTagName("script")[0];
firstScript.parentNode.insertBefore(script, firstScript);
})();
</script>
*********************************/ 
?>



	</head>

	
	<body class="<?=($this->controller_name=="index" && $this->action_name=="index")?'home':''?>">

		<div id="fb-root"></div>
		<!-- this is reserved for navigation autocomplete -->
		<div id="navigation_autocomplete" style="z-index: 10; position: relative;"></div>
		
		<div class="container" id="header_wrapper">
			<div class="pageTop clearfix">
				<ul class="pull-left sign">
					<li><a href="#" id="facebook-login" class="name facebook-login">Sign In</a></li>
					<li><a href="/member">Sign Up</a></li>
				</ul>
				<a href="javascript:void(0);" class="pull-right">
					<img src="/images/new-images/mobile-toggle-menu.png" />
				</a>
			</div>
			<div class="logoHeader">
				<div id="header" class="clearfix">
					
					<a href="/" id="logo"><img src="/images/socialcapital-logo.png" alt="Social Capital" /></a>
					
					<?/*
					<div class="followers_wrapper">
					
						<span class="followers">
							<? if($this->controller_name=="index" && $this->action_name=="cityblast"): ?>
								<div class="followers_message">"<?=COMPANY_NAME;?> is made for real estate agents; by real estate agents."</div>
							<? else: ?>
								<span class="followers_message"><?=COMPANY_NAME;?> agents reached</span>
								<span class="followers_number"><?=number_format($this->total_count);?></span>
								<span class="followers_message">potential home buyers today.</span>
							<? endif; ?>
						</span>
					
					</div>
					*/?>
					<div id="user_nav" class="pull-right">
						
						<? if(isset($_SESSION['member']) && $_SESSION['member']) : 
							$member = $_SESSION['member']; ?>
						
							<div id="top_menu">					
											
								<a href="#" class="my_account_text">My Account<img src="/images/my-account-arrow.png" border="0" /></a>
								<a href="#" class="my_account_user_img"><img width="24" height="24" src="<?= $this->profilePic() ?>" border="0" /></a>
								<a href="#" class="my_account_down_arrow">
									<?/*<img src="/images/expand-arrow.png" border="0" />*/?>
									<span class="caret"></span>
								</a>
								
								<div class="my_account_popup">
									<img src="/images/new-images/my-account-on-arrow.png" border="0" />
									<div class="user_info">
										<span class="name">
											<? if(isset($member->first_name)) echo substr ($member->first_name, 0, 1) . ".";  if(isset($member->last_name)) echo " " . $member->last_name; ?>
										</span>
										<span class="email">
											<? if(isset($member->email_override) && ($member->email_override)) $member->email_override; else echo $member->email; ?>
										</span>
										<span class="number">
											<?=$member->phone;?>
										</span>
									</div>
									<div class="user_account">
										<a href="/member/settings"><img width="50" height="50" src="<?php echo $src;?>" border="0" /></a>
										<ul>

											<? if($this->controller_name == "member" && $this->action_name == "settings"): ?>
											
												<li><a href="javascript:void(0);" onclick="$('#dashboardtab').trigger('click'); $('#top_menu').removeClass('on');">Dashboard</a></li>
												
												<li><a href="javascript:void(0);" onclick="$('#referagenttab').trigger('click'); $('#top_menu').removeClass('on');">Refer An Agent</a></li>
												<?/*<li><a href="javascript:void(0);" onclick="$('#affiliatestab').trigger('click'); $('#top_menu').removeClass('on');">Referrals</a></li>*/?>
												
												<li><a href="javascript:void(0);" onclick="$('#settingtab').trigger('click'); $('#top_menu').removeClass('on');">Settings</a></li>
												<li><a href="javascript:void(0);" onclick="$('#listingstab').trigger('click'); $('#top_menu').removeClass('on');">Listings</a></li>
												<li><a href="javascript:void(0);" onclick="$('#mybookingtab').trigger('click'); $('#top_menu').removeClass('on');">Bookings</a></li>
												<li><a href="javascript:void(0);" onclick="$('#invoicestab').trigger('click'); $('#top_menu').removeClass('on');">Invoices</a></li>
												
												<li><a href="javascript:void(0);" onclick="$('#myaccounttab').trigger('click'); $('#top_menu').removeClass('on');">My Account</a></li>
											
											<? else: ?>
											
												<li><a href="/member/settings#dashboardtab">Dashboard</a></li>
												
												<li><a href="/member/settings#referagenttab">Refer An Agent</a></li>
												<?/*<li><a href="/member/settings#affiliates">Referrals</a></li>*/?>
												
												<li><a href="/member/settings#settingtab">Settings</a></li>
												<li><a href="/member/settings#listingstab">Listings</a></li>
												<li><a href="/member/settings#mybookingtab">Bookings</a></li>
												<li><a href="/member/settings#invoices">Invoices</a></li>
												
												<li><a href="/member/settings#myaccounttab">My Account</a></li>
												
											<? endif; ?>

										</ul>
										<div class="clr"></div>
									</div>
									<div class="logout">
										<a href="/member/logout" id="facebook-logout">Log Out</a>
									</div>
								</div>
							</div>
							
						<? else: ?>	
							<div id="top_login">

								<!-- id="open" -->
								<span id="open_login">
									<a href="#" id="facebook-login" class="name facebook-login">Sign In.</a><a href="/member/index" class="name">Sign Up.</a>
								</span>
								
							</div>
							
						<? endif; ?>
						
					</div>
					
					<?
						if($this->canAccess('/admin'))
						{		
							echo "<div style='width: 68px; float: right; padding-top: 4px;'><a href='/admin'>Admin</a></div>";
						} elseif($this->canAccess('/admin/pending')) {		
							echo "<div style='width: 68px; float: right; padding-top: 4px;'><a href='/admin/pending'>Admin</a></div>";
						}
					?>
				
				</div>
			</div>


			<div id="navigation" class="row-fluid navigation">
				<div class="span12 clearfix">
				
					<? /* if($this->controller_name=="listing") : ?>
					
						<div style="float: right;">
							
							<span class="uiSelectWrapper" style="border: none;">
							
								<span class="uiSelectInner">
									<input type="text" name="cityselect" id="cityselect" class="uiSelect" style="width: 200px;" value="<?=$this->selectedCityName;?>" />
									<input type="hidden" name="city" id="city" value="<?=$this->selectedCity;?>" />
								</span>
								
							</span>
							
						</div>
						
					<? endif; */ ?>
					<span class="pull-right"><span class="phone"><?=TOLL_FREE;?></span> / 9 - 5 EDT</span>
					<? /*<input type="hidden" name="city" value="1">*/ ?>
					<? /*$this->controller_name.'/'.$this->action_name;*/ ?>
					<ul class="pull-left nav nav-pills hidden-phone">
						<li class="<?=($this->controller_name=="member" && $this->action_name=="index")?'active':'highlite'?>">
							<a href="/member/index">Try Us Now!</a>
							<img src="/images/new-images/header-menu-bottom-arrow.png" class="downArrow" />
						</li>
						
						<?/*
						<li class="<?=($this->controller_name=="member" && $this->action_name=="what_is_cityblast")?'active':''?>">
							<a href="/member/what-is-cityblast">What's <?=COMPANY_NAME;?>?</a>
							<img src="/images/new-images/header-menu-bottom-arrow.png" class="downArrow" />
						</li>
						*/ ?>
						
						<li class="<?=($this->controller_name=="index" && $this->action_name=="why-cityblast")?'active':''?>">
							<a href="/index/why-cityblast">Why it Works.</a>
							<img src="/images/new-images/header-menu-bottom-arrow.png" class="downArrow" />
						</li>
						<li class="<?=($this->controller_name=="index" && $this->action_name=="pricing")?'active':''?>">
							<a href="/index/pricing">Pricing.</a>
							<img src="/images/new-images/header-menu-bottom-arrow.png" class="downArrow" />
						</li>
						<li class="refer_agent <?=($this->controller_name=="index" && $this->action_name=="faq")?'active':''?>">
							<a href="/index/faq">FAQs.</a>
							<img src="/images/new-images/header-menu-bottom-arrow.png" class="downArrow" />
						</li>
						
						<?/***
						<li class="<?=($this->controller_name=="index" && $this->action_name=="our_story")?'active':''?>">
							<a href="/index/our-story">Our Story.</a>
							<img src="/images/new-images/header-menu-bottom-arrow.png" class="downArrow" />
						</li>
						***/?>
					</ul>

					<div class="mobileNavigation visible-phone">
						<div class="toggleMenu"></div>
						<ul class="unstyled">
							<li  class="<?=($this->controller_name=="member" && $this->action_name=="index")?'active':'highlite'?>">
								<a href="/member/index">Try Us Now!</a>
							</li>
							<li class="<?=($this->controller_name=="member" && $this->action_name=="what_is_cityblast")?'active':''?>">
								<a href="/member/what-is-cityblast">Whta's CityBlast?</a>
							</li>
							<li class="<?=($this->controller_name=="index" && $this->action_name=="why-cityblast")?'active':''?>">
								<a href="/index/why-cityblast">Why it Works.</a>
							</li>
							<li class="<?=($this->controller_name=="index" && $this->action_name=="pricing")?'active':''?>">
								<a href="/index/pricing">Pricing.</a>
							</li>
							<li class="refer_agent <?=($this->controller_name=="index" && $this->action_name=="faq")?'active':''?>">
								<a href="/index/faq">FAQs.</a>
							</li>
							<li class="<?=($this->controller_name=="index" && $this->action_name=="our_story")?'active':''?>">
								<a href="/index/our-story">Our Story.</a>
							</li>
							
						</ul>
					</div>
				</div>
			</div>

		</div>
		
		<?= $this->partial('flash-layout.html.php', array('flash_success' => $this->flash_success, 'flash_error' => $this->flash_error)); ?>

		<div class="container content" id="content">
			
			<?=$this->layout()->content; ?>
			
		</div>

		<?php
		// Select the kind of footer to be displayed: simple or fancy.
		$footerType = $this->footerType;
		if (empty($footerType)) {
			$footerType = 'simple';
		}
		switch ($footerType) {
			case 'simple': echo $this->render('footer/footer_simple.html.php'); break;
			case 'fancy': echo $this->render('footer/footer_fancy.html.php'); break;
		}
		?>

		<div id="popup" class="popup_container"></div>
		
				<script type="text/javascript">
					
					$(document).ready(function() {

						//autocomplete
						$('#cityselect').autocomplete({
							appendTo: "#navigation_autocomplete",
							source: "/city/autocomplete",
							minLength: 2,
							//open: function(){
							//	if($.browser.msie && $.browser.version < 8){
							//		$('css3-container', '#navigation_autocomplete').css('left', $('#navigation_autocomplete .ui-autocomplete').css('left'));
							//		$('css3-container', '#navigation_autocomplete').css('top', $('#navigation_autocomplete .ui-autocomplete').css('top'));
							//	}
							//},
							select: function( event, ui ) {
								if(ui.item){
									$('#city').val(ui.item.id);
									var url	    	= "/member/setcity/";
									var pdata    	= "city_id="+ui.item.id;
									$.post(url, pdata, function(data){
										if(data.error==false)
											window.location.reload();
										<? /*else $.colorbox({href:false,innerWidth:300,initialWidth:330,initialHeight:45,html:"<h3>Not possible to change city.</h3>"});*/?>
									},'json')
								}
							},
							change:function(event, ui)
							{
								if(ui.item){
								}else{
        							$('#city').val('');
        							$(this).val('');
									$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oooops.</h3><p>You must select a known city from the list available in the dropdown!</p>"});
								}
				
							}							

						}).focus(function() {
							if ($(this).val().search(/Select Your City/) != -1) {
								$(this).val('');
							}
						}).blur(function() {
							if ($(this).val() == '') {
								$(this).val('Select Your City');
							}
						});
						
						$('#top_menu').children('a').click(function(e){
							$(this).parent().toggleClass('on');
						});
						
					});
				</script>




		<script type="text/javascript"  language="javascript">
			//to apply stlye from main .box :last-child for IE7, 8
			$(document).ready(function() 
			{
				if($.browser.msie && $.browser.version < 9)
				{
					$('.box :last-child').css('margin-bottom', '0px');
				}
			});
		</script>


		
		
		<script type="text/javascript">
		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '<?=GOOGLE_ANALYTICS;?>']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>
		
		
		<!-- START OF SmartSource Data Collector TAG -->
		<!-- Copyright (c) 1996-2011 WebTrends Inc.  All rights reserved. -->
		<!-- Version: 9.3.0 -->
		<!-- Tag Builder Version: 3.1  -->
		<!-- Created: 3/22/2011 3:12:41 PM -->
		
		
		<!-- ----------------------------------------------------------------------------------- -->
		<!-- Warning: The two script blocks below must remain inline. Moving them to an external -->
		<!-- JavaScript include file can cause serious problems with cross-domain tracking.      -->
		<!-- ----------------------------------------------------------------------------------- -->
		<script type="text/javascript">
			//<![CDATA[
			var trendVar = "www.cityblast.com";
			var dcsidVar = "dcs6p84o900000w0yf68kq216_9x3g";
			var _tag=new WebTrends();
			_tag.dcsGetId();
			//]]>
		</script>
		<script type="text/javascript">
			//<![CDATA[
			_tag.dcsCustom=function(){
			// Add custom parameters here.
			//_tag.DCSext.param_name=param_value;
			}
			_tag.dcsCollect();
			//]]>
		</script>
		<noscript>
			<div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="//statse.webtrendslive.com/dcs6p84o900000w0yf68kq216_9x3g/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=9.3.0&amp;WT.dcssip=www.city-blast.com"/></div>
		</noscript>
		<!-- END OF SmartSource Data Collector TAG -->








		<!-- Correct fb_xd_fragment Bug Start -->
		<script type="text/javascript">
		document.getElementsByTagName('html')[0].style.display='block';
		</script>
		<!-- Correct fb_xd_fragment Bug End -->
	
		<?=$this->partial('bodyend.html.php')?>
		<?=$this->partial('flash-colorbox.html.php', array( 'flash_colorbox' => $this->flash_colorbox ))?>
	
	</body>
	
</html>