<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
	
	<head>
		<?=$this->partial('head.html.php')?>
		<?=$this->headMeta()?>
		
		<? if (isset($this->title) && $this->title) : ?><title><?= $this->title; ?></title><? endif; ?>
		<? if (isset($this->description) && $this->description) : ?><meta name="description" content="<?= $this->description; ?>" /><? endif; ?>
		<? if (isset($this->keyword) && $this->keyword) : ?><meta name="keywords" content="<?= $this->keyword; ?>" /><? endif; ?>
		<? if (isset($this->catonical) && $this->catonical) : ?><link rel="canonical" href="<?= $this->catonical; ?>" /><? endif; ?>

		<META NAME="WT.z_Connect_Company" CONTENT="<?=COMPANY_NAME;?>">
		<META NAME="WT.z_Connect_Site_Type" CONTENT="3RD Party">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<!-- STYLESHEETS -->
			<? /* <link rel="stylesheet" type="text/css" href="/css/facebox.css" /> */ ?>
			<link type="text/css" rel="stylesheet" href="/css/uploadify.css" />
			<link type="text/css" rel="stylesheet" href="/css/slide.css" />
			<link type="text/css" rel="stylesheet" href="/js/colorbox/colorbox.css" />
			<link type="text/css" rel="stylesheet" href="/css/reset.css" />
			<?/*<link type="text/css" rel="stylesheet" href="/css/grid.css" />*/?>
			<?/*<link type="text/css" rel="stylesheet" href="/css/main.css" />*/?>
			<?/*<link type="text/css" rel="stylesheet" href="/css/forms.css" />*/ ?>
			<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css" />
			<link type="text/css" rel="stylesheet" href="/css/bootstrap-responsive.min.css" />
			
			<? if(isset($this->member) && $this->member->franchise && $this->member->franchise->is_enabled) : ?>
				<? $brand = str_replace(' ', '-', strtolower( $this->member->franchise->name ) ); ?>
				<link type="text/css" rel="stylesheet" href="/branding/<?=$brand?>/styles.css" />
			<? endif; ?>
			
			<? /*<link type="text/css" rel="stylesheet" href="/css/autocomplete/jquery.autocomplete.css" />*/?>			
			<link type="text/css" rel="stylesheet" href="/css/jquery-ui-1.8.16.custom.css" />
			<link type="text/css" rel="stylesheet" href="/css/dashboard-common.css" />

			
			<link type="text/css" rel="stylesheet" href="/css/new-style.css" />
			
			<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
			<!--[if IE 7]>
				<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome-ie7.css" rel="stylesheet">
			<![endif]-->
			
		<!-- END STYLESHEETS -->
	
		<!-- JAVASCRIPTS -->
	
			<!-- TYPEKIT -->
			<script type="text/javascript" src="https://use.typekit.com/<?=TYPEKIT;?>.js"></script>
			<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


			<!-- AUTO COMPLETE -->
			<!--<script type="text/javascript" src="/js/autocomplete/jquery.bgiframe.min.js"></script>--><!-- commented as its throwing 404 -->
			<!--<script type="text/javascript" src="/js/autocomplete/jquery.autocomplete.min.js"></script>-->

			<!-- WEBTRENDS -->
			<script type="text/javascript" src="/js/webtrends.js"></script>
					
			<script type="text/javascript">
				var is_facebook_init	= false;
				<?= $this->render_content_for('js'); ?>
			</script>
			
			<? /* FIREBUG LITE FOR DEBUGGING IN NON-FIREFOX BROWSER
			<script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite.js'></script>
			<script type="text/javascript">
				firebug.env.height = 120;
			</script>
			*/ ?>

		    <script type="text/javascript">
		    var _kmq = _kmq || [];
		    var _kmk = _kmk || '162b3d5e9e4e1a42c4a23865f817e2e2f0631ab6';
		    function _kms(u){
		    setTimeout(function(){
		    var d = document, f = d.getElementsByTagName('script')[0],
		    s = d.createElement('script');
		    s.type = 'text/javascript'; s.async = true; s.src = u;
		    f.parentNode.insertBefore(s, f);
		    }, 1);
		    }
		    _kms('//i.kissmetrics.com/i.js');
		    _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
		    </script>
			
		<!-- END JAVASCRIPTS -->
		
		<? /* THESE SHOULDN'T BE NECESSARY, BUT JUST IN CASE <style type="text/css">
	
			.clearfix:after {
				content: " ";
				display: block;
				height: 0;
				clear: both;
				visibility: hidden;
			}
		
		</style>
		
		<!--[if IE]>
			<style type="text/css">
				.clearfix {
					zoom: 1;
				}
			</style>
		<![endif]-->
		<!--[if lte IE 7]>
			<style type="text/css">
				.fieldname {
					text-indent: 0px;
				}
			</style>
		<![endif]-->

		<!--[if lt IE 8]>
			<style type="text/css">
				// IE6
				*html .uiButton.IE input{
					// remove padding from left/right
					overflow: visible; 
					// remove the remaining space in IE6
					width:0; 
				}
				
				// IE7
				*:first-child+html .uiButton.IE input{
					//remove padding from left/right
					#overflow: visible;
					#width:auto;
				}
			</style>
		<![endif]-->
		
		
		*/
		
		
		if(isset($this->has_comments) && $this->has_comments)
		{
		
			//Comments ADMINS are Alen, Shaun and Grace...
			$fbadmins[] = "734450163";
			$fbadmins[] = "507956900";
			$fbadmins[] = "1387631581";
			$fbadmins[] = "100004323637534";
			$fbadmins[] = "100004112281200";
			$fbadmins[] = "100002817208869";
			$fbadmins[] = "514203248";
			$fbadmins[] = "510154680";
			$fbadmins[] = "100005394605090";
			$fbadmins[] = "172005537";
			
		
			echo '<meta property="fb:admins" content="';
			echo (is_array($fbadmins)) ? implode(",",$fbadmins) :  $fbadmins;
			echo '"/>';
		}

		if(!empty($this->additional_meta)) 
		{
		    echo $this->additional_meta;
		}
		    
		?>
		
		<!--[if IE 7]>
			<style type="text/css">
				#navigation .refer_agent{
					white-space: nowrap
				}
				
				.uiSelectWrapper{
					padding-top: 3px;
					padding-bottom: 7px;
				}
				.uiSelectWrapper .uiSelect{
					height: 24px;
					background-color: #eeeeee;
				}
				
				#navigation_autocomplete .ui-menu .ui-menu-item{
					width: 244px;
				}
				
				#top_menu .my_account_down_arrow{
					margin-top: 8px;
				}
			</style>
		<![endif]-->

<?/**********
		 <script type="text/javascript">
		(function() {
		window.fireflyAPI = {};
		    fireflyAPI.token = "506206504ef500670a002913";
		
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "https://firefly-071591.s3.amazonaws.com/scripts/loaders/loader.js";
		script.async = true;
		var firstScript = document.getElementsByTagName("script")[0];
		firstScript.parentNode.insertBefore(script, firstScript);
		})();
		</script>
**********/ ?>

	</head>

	
	<body class="<?=($this->controller_name=="index" && $this->action_name=="index")?'home':''?> <?= isset( $brand ) ? $brand : '' ?>">

		<div id="fb-root"></div>

		<div class="container" id="header_wrapper">
			<div class="pageTop clearfix">
				<!--<ul class="pull-left sign">
					<li><a href="">Sign In</a></li>
					<li><a href="">Sign Up</a></li>
				</ul>-->
				<a href="javascript:void(0);" class="pull-right">
					<img src="/images/new-images/mobile-toggle-menu.png" />
				</a>
			</div>
			<div class="logoHeader">
				<div id="header" class="clearfix">
					
					<? if( isset($this->member) && !empty($this->member->franchise_id) && !empty($this->member->franchise->logo)	) : ?>
						<a href="/" id="logo"><img src="/branding/<?=$brand;?>/logo.png" alt="SocialCapital + <?=$this->member->franchise->name;?>" style="padding-top: 3px;"/></a>
					<? else : ?>
						<a href="/" id="logo"><img src="/images/socialcapital-logo.png" alt="SocialCapital" style="padding-top: 3px;"/></a>
					<? endif; ?>
			
			
					<div class="pull-right" style="margin-right: 20px;">
	
						<div class="media">
							<a class="pull-left" href="#" onclick="openBookBox('BOOK_SHOWING')">
		
								<?php 
								
									if(empty($this->booking_agent->photo))
									{
										$src = "https://graph.facebook.com/". $this->booking_agent->uid ."/picture" ;
									}
									else {
										$src = $this->booking_agent->getSizedPhoto('50','50');
									}
								?>
		
								<img src="<?=$src;?>" class="media-object" height="50" width="50" style="width: 50px;" />
							</a>
							<div class="media-body pull-left">
													
								<?/*<img width="50" height="50" src="<?php echo $src;?>" border="0" style="margin-right: 10px;" />*/?>
								
								<div class="pull-right">
									
									<div style="font-size: 18px; font-weight: bold;"><?=$this->booking_agent->first_name;?> <?=$this->booking_agent->last_name;?></div>
									<div><?=$this->booking_agent->brokerage;?></div>
									<div class="clearfix"><?=$this->booking_agent->broker_address;?></div>
								</div>
							</div>
						</div>
						
	
				
					</div>
			
				</div>
				<div class="phone">
					<span>1-888-712-7888</span> / 9 - 5 EDT
				</div>
			</div>
						
		</div>
		
		<div class="container content" id="content">
			<? //print_rf( $this->member ); ?>
			<?=$this->layout()->content; ?>
			
		</div>
		<!-- footer -->
		<!-- old logo
		<a href="/" id="logo"><img src="/images/new-images/city-blast_logo.png" alt="City Blast" class="pull-right" style="margin-right: 10px;" /></a>
		-->
		<?php
		echo $this->render('footer/footer_fancy.html.php');
		?>

		<div id="popup" class="popup_container"></div>
		
				<script type="text/javascript">
					
					$(document).ready(function() {

						//autocomplete
						$('#cityselect').autocomplete({
							appendTo: "#navigation_autocomplete",
							source: "/city/autocomplete",
							minLength: 2,
							//open: function(){
							//	if($.browser.msie && $.browser.version < 8){
							//		$('css3-container', '#navigation_autocomplete').css('left', $('#navigation_autocomplete .ui-autocomplete').css('left'));
							//		$('css3-container', '#navigation_autocomplete').css('top', $('#navigation_autocomplete .ui-autocomplete').css('top'));
							//	}
							//},
							select: function( event, ui ) {
								if(ui.item){
									$('#city').val(ui.item.id);
									var url	    	= "/member/setcity/";
									var pdata    	= "city_id="+ui.item.id;
									$.post(url, pdata, function(data){
										if(data.error==false)
											window.location.reload();
										<? /*else $.colorbox({href:false,innerWidth:300,initialWidth:330,initialHeight:45,html:"<h3>Not possible to change city.</h3>"});*/?>
									},'json')
								}
							},
							change:function(event, ui)
							{
								if(ui.item){
								}else{
        							$('#city').val('');
        							$(this).val('');
									$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oooops.</h3><p>You must select a known city from the list available in the dropdown!</p>"});
								}
				
							}							

						}).focus(function() {
							if ($(this).val().search(/Select Your City/) != -1) {
								$(this).val('');
							}
						}).blur(function() {
							if ($(this).val() == '') {
								$(this).val('Select Your City');
							}
						});
						
						$('#top_menu').children('a').click(function(e){
							$(this).parent().toggleClass('on');
						});
						
					});
				</script>

		
		<script type="text/javascript"  language="javascript">
			//to apply stlye from main .box :last-child for IE7, 8
			$(document).ready(function() {
				if($.browser.msie && $.browser.version < 9){
					$('.box :last-child').css('margin-bottom', '0px');
				}
			});
		</script>



		
		
		<script type="text/javascript">
		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '<?=GOOGLE_ANALYTICS;?>']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>
		
		
		<!-- START OF SmartSource Data Collector TAG -->
		<!-- Copyright (c) 1996-2011 WebTrends Inc.  All rights reserved. -->
		<!-- Version: 9.3.0 -->
		<!-- Tag Builder Version: 3.1  -->
		<!-- Created: 3/22/2011 3:12:41 PM -->
		
		
		<!-- ----------------------------------------------------------------------------------- -->
		<!-- Warning: The two script blocks below must remain inline. Moving them to an external -->
		<!-- JavaScript include file can cause serious problems with cross-domain tracking.      -->
		<!-- ----------------------------------------------------------------------------------- -->
		<script type="text/javascript">
			//<![CDATA[
			var trendVar = "www.cityblast.com";
			var dcsidVar = "dcs6p84o900000w0yf68kq216_9x3g";
			var _tag=new WebTrends();
			_tag.dcsGetId();
			//]]>
		</script>
		<script type="text/javascript">
			//<![CDATA[
			_tag.dcsCustom=function(){
			// Add custom parameters here.
			//_tag.DCSext.param_name=param_value;
			}
			_tag.dcsCollect();
			//]]>
		</script>
		<noscript>
			<div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://statse.webtrendslive.com/dcs6p84o900000w0yf68kq216_9x3g/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=9.3.0&amp;WT.dcssip=www.city-blast.com"/></div>
		</noscript>
		<!-- END OF SmartSource Data Collector TAG -->




		<!-- Correct fb_xd_fragment Bug Start -->
		<script type="text/javascript">
		document.getElementsByTagName('html')[0].style.display='block';
		</script>
		<!-- Correct fb_xd_fragment Bug End -->
	
		<?=$this->partial('bodyend.html.php')?>
	
	</body>
	
</html>