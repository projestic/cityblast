<?php
/**
 * InteractionController
 *
 * @author nur
 */
class InteractionController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		parent::init();

		$this->renderNothing();
	}

	public function email()
	{
		$listing_id	= $this->_getParam('listing_id', null);
		$booking_agent_id 	= $this->_getParam('booking_agent_id', null);

		if ($listing_id && is_numeric($listing_id))
		{
			$listing	= Listing::find($listing_id);

			if ( false === strpos(@$_SERVER['HTTP_USER_AGENT'], 'Googlebot') ) {
			    Interaction::entry($listing_id, $booking_agent_id, 'share_listing_email');
			}
			$this->_redirect("/listing/email/". $listing_id);
		}
		else
		{
			$this->_redirect("/listing");
		}

	}

	public function facebook()
	{
		$listing_id		= $this->_getParam('listing_id', null);
		$booking_agent_id 	= $this->_getParam('booking_agent_id', null);



		if ($listing_id && is_numeric($listing_id))
		{
			$listing	= Listing::find($listing_id);


			//Take note of the Interaction
			if ( false === strpos(@$_SERVER['HTTP_USER_AGENT'], 'Googlebot') ) {
			    Interaction::entry($listing_id, $booking_agent_id, 'facebook_share');
			}


			if ($listing->message && $listing->image)
			{
				$imgUrl	= CDN_URL . $listing->image;
			}
			else
			{
				$imgUrl	= CDN_URL .'/images/fb_default_share.jpg';
			}


			$_SESSION['fb_share_processing']	= $listing->id;

			$fb_share_url	= 'http://www.facebook.com/sharer.php?s=100&p[title]='. urlencode(COMPANY_NAME . " Listing: ".$listing->street) .
							'&p[summary]='. urlencode($listing->message) .
							'&p[images][0]='. urlencode($imgUrl) .
							'&p[url]='. urlencode(APP_URL . "/listing/view/" . $listing->id);


			$this->_redirect($fb_share_url);
		}
		else
		{
			$this->_redirect("/listing");
		}
	}

	public function twitter()
	{
		$listing_id	= $this->_getParam('listing_id', null);
		$booking_agent_id 	= $this->_getParam('booking_agent_id', null);

		if ($listing_id && is_numeric($listing_id))
		{
			$listing	= Listing::find($listing_id);


			//Take note of the Interaction
			if ( false === strpos(@$_SERVER['HTTP_USER_AGENT'], 'Googlebot') ) {
			    Interaction::entry($listing_id, $booking_agent_id, 'twitter_share');
			}


			$tw_share_url	= 'http://twitter.com/share?url='. urlencode(APP_URL . "/listing/view/" . $listing->id).
							'&text='. urlencode(COMPANY_NAME . " Listing: ".$listing->street);

							//'&p[summary]='. urlencode($listing->message) .
							//'&p[images][0]='. urlencode($imgUrl);


			$this->_redirect($tw_share_url);
		}
		else
		{
			$this->_redirect("/listing");
		}
	}

	public function google()
	{
		$listing_id	= $this->_getParam('listing_id', null);
		$booking_agent_id 	= $this->_getParam('booking_agent_id', null);

		if ($listing_id && is_numeric($listing_id))
		{
			$listing	= Listing::find($listing_id);
		}
		else
		{
			$listing	= null;
		}

		if ($listing)
		{#pre($this->_getAllParams(), 0);pre($_SESSION);

		    if ( false === strpos(@$_SERVER['HTTP_USER_AGENT'], 'Googlebot') ) {
			    Interaction::entry($listing_id, $booking_agent_id, 'google_plus_share');
		    }

			/**
			 * @todo we may need to upgrade the googleplus share forwarding (or an ajax call for popup-share-box)
			 */
			$this->_redirect("/listing/email/". $listing_id);
		}
		else
		{
			$this->_redirect("/listing");
		}
	}
}
?>
