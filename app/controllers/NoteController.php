<?php

class NoteController extends ApplicationController
{
	
	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
	}
	
	public function index()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}

		$request    =  $this->getRequest();
		$member_id  =  $request->getParam('id');
		$member     = Member::find($member_id);
		
		if(!empty($member->id))
		{
			$this->view->member = $member;
			
			$conditions = array(
				"select" => "note.*, 
					m.first_name as mfirst, 
					m.last_name as mlast, 
					ma.first_name as afirst, 
					ma.last_name as alast",
				"conditions" => "note.member_id = '{$member->id}'",
				"joins" => "LEFT JOIN member m ON m.id = note.member_id
					LEFT JOIN member ma ON ma.id = note.admin_id",
				"order" => "id DESC");

			$paginator = new Zend_Paginator(new ARPaginator('Note', $conditions));
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;
		}

	}	
	
	public function add()
	{
		$params = $this->getRequest()->getParams();
		$params = array_map('trim', $params);
		
		$error_messages = array();
		
		if($this->_request->isPost()) {
			
			if (empty($params['member_id'])) {
				$error_messages[] = 'Member not specified';
			}
			else
			{
				$member = Member::find($params['member_id']);
				if (empty($member))
				{
					$error_messages[] = "Member doesn't exist!";
				}
			}
			
			if (empty($params['note'])) {
				$error_messages[] = 'You must provide a note';
			}

			if (empty($error_messages)) {
				$note = new Note();
				$note->member_id = $member->id;
				$note->admin_id = ($_SESSION['member']->id) ? $_SESSION['member']->id : 0;
				$note->note = htmlentities($params['note']);
				$note->created_at = date('Y-m-d H:i:s');
				$note->save();
				
				$this->_redirect("/admin/member/{$params['member_id']}#notes");
			}
		} 

		if (!empty($params['id']))
		{
			$this->view->member = Member::find($params['id']);
		} else
		{
			$error_messages[] = "Member id is required!";
		}
			
		$this->view->error_messages = $error_messages;
	}

	public function delete()
	{
		$this->renderNothing();

		$id =  $this->getRequest()->getParams('id');

		if (!empty($id))
		{
			$note = Note::find_by_id($id);
			$note->delete();

			$this->_redirect("/admin/member/{$note->member_id}#notes");
		}

		$this->redirect("/");
	}
}