<?php

class TextfixController extends ApplicationController
{

	protected static $acl = array(
		'index' => array('odesk', 'assistanteditor', 'salesadmin'),
		'manualsave' => array('odesk', 'assistanteditor', 'salesadmin'),
		'fixapply' => array('assistanteditor', 'salesadmin'),
		'*' => 'superadmin'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
		$this->view->admin_selectedCity = $this->getSelectedAdminCity();
	}
	
	public function index()
	{		
		$sql_conditions = array();
		$sql_values = array();
		
		
		$field = $this->_getParam('field');
		if (!empty($field)) {
			list($table_name, $field_name) = explode('.', $field);
			$sql_conditions[] = '(table_name = ? AND field_name = ?)';
			$sql_values[] = $table_name;
			$sql_values[] = $field_name;
		}
		
		$status = $this->_getParam('status', 'new');
		if ($status == 'new') {
			$sql_conditions[] = 'fixed = 0';
		} else {
			$sql_conditions[] = 'fixed = 1';
		}

		
		$this->view->field = $field;
		$this->view->status = $status;
		
		$sql_conditions = array_merge(array(implode(' AND ', $sql_conditions)), $sql_values);
		
		$options = array(
			'conditions' => $sql_conditions, 
			'order' => 'listing_id DESC'
		);
		$paginator = new Zend_Paginator(new ARPaginator('TextFix', $options));
		$paginator->setItemCountPerPage(1);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$this->view->paginator = $paginator;
	}
	
	public function manualsave()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$request = $this->getRequest();
		
		$result = false;
		if ($request->isPost()){
			$record = TextFix::find_by_table_name_and_row_id_and_field_name(
				$request->getParam('table_name'),
				$request->getParam('row_id'),
				$request->getParam('field_name')
			);
			$text = $request->getParam('text');
			$record->manual = $request->getParam('text');
			$record->save();
			
			$result = true;
	    }

		$this->_helper->json(array('result' => $result));
	}
	
	public function fixapply()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$request = $this->getRequest();
		
		$result = false;
		if ($request->isPost()){
			$type = $request->getParam('type');
			
			$record = TextFix::find_by_table_name_and_row_id_and_field_name(
				$request->getParam('table_name'),
				$request->getParam('row_id'),
				$request->getParam('field_name')
			);
			
			$text = $request->getParam('text');
			if (!empty($text) && $type == 'manual') {
				$record->manual = $text;
				$record->save();
			}

			$record->fix($type);
			
			$result = true;
	    }

		$this->_helper->json(array('result' => $result));
	}
}