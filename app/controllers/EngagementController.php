<?php

class EngagementController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
		$this->view->admin_selectedCity = $this->getSelectedAdminCity();
	}
	
	public function index()
	{
		$this->_redirect('/engagement/general');
	}
	
	public function general()
	{
		$date_from = $this->getParam('date_from', date('Y-01-01', strtotime('-3years')));
		$date_from_unixtime = strtotime($date_from);
		$date_to = $this->getParam('date_to', date('Y-m-d'));
		$date_to_unixtime = strtotime($date_to);
		
		$group_by = $this->getParam('group_by', 'week');
		switch ($group_by) {
			case 'date':
				$model = 'EngageByDate';
			break;
			case 'week':
				$model = 'EngageByWeek';
				// Snap to first day and last day of the week
				$date_from = (date('N', $date_from_unixtime) != 1) ? date('Y-m-d', strtotime('last monday', $date_from_unixtime)) : $date_from;
				$date_to = (date('N', $date_to_unixtime) != 7) ? date('Y-m-d', strtotime('next sunday', $date_to_unixtime)) : $date_to;
			break;
			case 'month':
				$model = 'EngageByMonth';
				// Snap to first day and last day of the month
				$date_from = date('Y-m-01', $date_from_unixtime);
				$date_to = date('Y-m-t', $date_to_unixtime);
			break;
			case 'year':
				$model = 'EngageByYear';
				// Snap to first day and last day of the month
				$date_from = date('Y-01-01', $date_from_unixtime);
				$date_to = date('Y-12-t', $date_to_unixtime);
			break;
		}
		
		$this->view->sort_by = $sort_by = $this->getParam('sort_by', 'date');
		$this->view->sort_dir = $sort_dir = $this->getParam('sort_dir', 'DESC');
		
		$this->view->group_by = $group_by;
		$this->view->date_from = $date_from;
		$this->view->date_to = $date_to;
		
		$conditions = array(
			'conditions' => array(
				"(date >= ? AND date <= ?)",
				$date_from, 
				$date_to
			), 
			'order' => $sort_by . ' ' . $sort_dir
		);
		$paginator = new Zend_Paginator(new ARPaginator($model, $conditions));
		$paginator->setItemCountPerPage(30);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$this->view->paginator = $paginator;
	}
	
	public function listing()
	{
		$date_from = $this->getParam('date_from', date('Y-01-01', strtotime('-3years')));
		$date_from_unixtime = strtotime($date_from);
		$date_to = $this->getParam('date_to', date('Y-m-d'));
		$date_to_unixtime = strtotime($date_to);
		$group_by = $this->getParam('group_by', 'week');
		switch ($group_by) {
			case 'date':
				$model = 'EngageByDateListing';
			break;
			case 'week':
				$model = 'EngageByWeekListing';
				// Snap to first day and last day of the week
				$date_from = (date('N', $date_from_unixtime) != 1) ? date('Y-m-d', strtotime('last monday', $date_from_unixtime)) : $date_from;
				$date_to = (date('N', $date_to_unixtime) != 7) ? date('Y-m-d', strtotime('next sunday', $date_to_unixtime)) : $date_to;
			break;
			case 'month':
				$model = 'EngageByMonthListing';
				// Snap to first day and last day of the month
				$date_from = date('Y-m-01', $date_from_unixtime);
				$date_to = date('Y-m-t', $date_to_unixtime);
			break;
			case 'year':
				$model = 'EngageByYearListing';
				// Snap to first day and last day of the month
				$date_from = date('Y-01-01', $date_from_unixtime);
				$date_to = date('Y-12-t', $date_to_unixtime);
			break;
		}

        $table = $model::$table;

		$where = array(
			'conditions' => array(),
			'values' => array()
		);
		
		$where['conditions'][] = 'date >= ? AND date <= ?';
		$where['values'][] = $date_from;
		$where['values'][] = $date_to;
		
		$blast_type = $this->getParam('blast_type');
		if (!empty($blast_type)) {
			$where['conditions'][] = 'listing.blast_type = ?';
			$where['values'][] = $blast_type;
		}

        $tag_type = $this->getParam('tag_type');
        if (!empty($tag_type)){
            $where['conditions'][] = 'tag_cloud.tag_id = ?';
            $where['values'][] = $tag_type;
            $where['joins'][] = 'INNER JOIN tag_cloud ON ' . $table . '.listing_id = tag_cloud.listing_id';
        }

		$this->view->blast_type = $blast_type;
		$this->view->group_by = $group_by;
		$this->view->date_from = $date_from;
		$this->view->date_to = $date_to;
		$this->view->sort_by = $sort_by = $this->getParam('sort_by', 'score_perpost');
		$this->view->sort_dir = $sort_dir = $this->getParam('sort_dir', 'DESC');
		$this->view->tag_selected = $tag_type;
		
		$condition = '(' . implode(') AND (', $where['conditions']) . ')';
		$conditions = $where['values'];
		array_unshift($conditions, $condition);

        $joins = 'INNER JOIN listing ON ' . $table . '.listing_id = listing.id';
        if (!empty($where['joins'])) {
            $joins = $joins . ' ' .  implode(' ', $where['joins']);
        }

		$conditions = array(
			'select' => $table . '.*, listing.blast_type',
			'conditions' => $conditions, 
			'joins' => $joins,
			'order' => $sort_by . ' ' . $sort_dir
		);
		$paginator = new Zend_Paginator(new ARPaginator($model, $conditions));
		$paginator->setItemCountPerPage(30);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$this->view->paginator = $paginator;

        // Get tags for view filter
        $tags = Tag::all(array(
            'order' => 'name'
        ));
        $this->view->tags = $tags;
	}
	
	public function member()
	{
		$date_from = $this->getParam('date_from', date('Y-01-01', strtotime('-3years')));
		$date_from_unixtime = strtotime($date_from);
		$date_to = $this->getParam('date_to', date('Y-m-d'));
		$date_to_unixtime = strtotime($date_to);
		$group_by = $this->getParam('group_by', 'week');
		switch ($group_by) {
			case 'date':
				$model = 'EngageByDateMember';
			break;
			case 'week':
				$model = 'EngageByWeekMember';
				// Snap to first day and last day of the week
				$date_from = (date('N', $date_from_unixtime) != 1) ? date('Y-m-d', strtotime('last monday', $date_from_unixtime)) : $date_from;
				$date_to = (date('N', $date_to_unixtime) != 7) ? date('Y-m-d', strtotime('next sunday', $date_to_unixtime)) : $date_to;
			break;
			case 'month':
				$model = 'EngageByMonthMember';
				// Snap to first day and last day of the month
				$date_from = date('Y-m-01', $date_from_unixtime);
				$date_to = date('Y-m-t', $date_to_unixtime);
			break;
			case 'year':
				$model = 'EngageByYearMember';
				// Snap to first day and last day of the month
				$date_from = date('Y-01-01', $date_from_unixtime);
				$date_to = date('Y-12-t', $date_to_unixtime);
			break;
		}
		
		$where = array(
			'conditions' => array(),
			'values' => array()
		);
		
		$where['conditions'][] = 'date >= ? AND date <= ?';
		$where['values'][] = $date_from;
		$where['values'][] = $date_to;
		
		$type_id = $this->getParam('type_id');
		if (!empty($type_id)) {
			$where['conditions'][] = 'member_type.id = ?';
			$where['values'][] = $type_id;
		}
		
		$this->view->type_id = $type_id;
		$this->view->group_by = $group_by;
		$this->view->date_from = $date_from;
		$this->view->date_to = $date_to;
		$this->view->sort_by = $sort_by = $this->getParam('sort_by', 'score_perpost');
		$this->view->sort_dir = $sort_dir = $this->getParam('sort_dir', 'DESC');
		
		$table = $model::$table;
		
		$condition = '(' . implode(') AND (', $where['conditions']) . ')';
		$conditions = $where['values'];
		array_unshift($conditions, $condition);
		
		$conditions = array(
			'select' => $table . '.*, 
						member_type.name AS type_name, 
						member.first_name,
						member.last_name',
			'conditions' => $conditions, 
			'joins' => 'INNER JOIN member ON ' . $table . '.member_id = member.id
						INNER JOIN member_type ON member.type_id = member_type.id',
			'order' => $sort_by . ' ' . $sort_dir
		);
		$paginator = new Zend_Paginator(new ARPaginator($model, $conditions));
		$paginator->setItemCountPerPage(30);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$this->view->paginator = $paginator;
	}
}