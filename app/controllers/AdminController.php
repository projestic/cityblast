 <?php

class AdminController extends ApplicationController
{
	CONST TAXES		=   "0.13"; // 13%
	CONST CURRENCY		=   "CAD";

	protected static $acl = array(
		'pending' => array('odesk'),
		'approved' => array('odesk'),
		'listingtype' => array('odesk'),
		'*' => 'superadmin'
	);

	public function init()
	{		
		//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');		

		parent::init();

		$this->view->admin_selectedCity    = $this->getSelectedAdminCity();
		$this->view->daily_stats = $this->todaysTrials();
		
	}

	public function test()
	{
		$member = Member::find(1001925);		
		$BINGO = $member->should_i_publish();
		
		echo "<h1>BINGO: " .$BINGO."</h1>";		
		exit();			
	}

	public function listingshowcase()
	{
		$request = $this->getRequest();
	

		$listing = Listing::find($request->getParam('listing_id'));		
		
		//echo "<pre>";
		//print_r($listing);
		
		
		$listing->createShowCase();
		

		$this->redirect("/admin/listings");
		exit();
		
	}

	private function getcityarray()
	{
		$cities = City::find('all', array('conditions' => 'UPPER(name) != "CANADA" and UPPER(name) != "UNITED STATES"', 'order' => 'name'));
		foreach($cities as $city)
		{
			$city_array[$city->id] = substr($city->name,0,15) . ", " . substr($city->state,0,2);
		}
		return $city_array;
	}

	public function pending() {
		$this->preApprovalView('pending');
	}

	public function approved() {
		$this->preApprovalView('approved');
		$this->render('pending');
	}

	private function preApprovalView($type = 'pending') {
		$listing_type = $this->view->type = !empty($_SESSION['listing_type']) ? $_SESSION['listing_type'] : null;
		
		$conditions = array();
		$conditions[] = "status = 'active'";
		$conditions[] = "approval = 0";
		
		if ($type == 'pending') {
			if ($this->memberIs('odesk')) {
				$conditions[] = "pre_approval = 0";
			} elseif ($this->memberIs('assistanteditor')) {
				$conditions[] = "assistant_approval = 0";
			} else {
				$conditions[] = "pre_approval = 1";
				$conditions[] = "(assistant_approval = 1 OR assistant_approval IS NULL)";
			}
		} else {
			if ($this->memberIs('odesk')) {
				$conditions[] = "pre_approval = 1";
			} elseif ($this->memberIs('assistanteditor')) {
				$conditions[] = "assistant_approval = 1";
			}
		}
		
		if (!empty($listing_type) && 
			in_array($listing_type, array(Listing::TYPE_LISTING, Listing::TYPE_CONTENT, Listing::TYPE_IDX))) {
			$conditions[] = "blast_type='".$listing_type."'";
		}

		$conditions = array('conditions' => implode(' AND ', $conditions), 'order' => 'id DESC');
		
		$paginator = new Zend_Paginator(new ARPaginator('Listing', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
		$this->view->view_type = $type;
	}

	public function newsletter()
	{
		include_once (APPLICATION_PATH . "/../public/blog/wp-config.php");
		$wp_de_conn = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
		mysql_select_db(DB_NAME, $wp_de_conn);
		

		$query  = "select ID, post_name, post_title, post_excerpt, post_date ";
		$query .= "from wp_dpvjuu_posts join wp_dpvjuu_term_relationships on wp_dpvjuu_posts.ID=wp_dpvjuu_term_relationships.object_id ";
		$query .= "where post_status='publish' ";
		$query .= "order by post_date desc";				
		
		//echo $query . "<BR>";
		
		$result = mysql_query($query);
		if (!$result) 
		{
		    die('Invalid query: ' . mysql_error());
		}

		$this->view->result = $result;
	}


	public function affiliatepayoutreport() {
	
		$request = $this->getRequest();
	
		$from = new DateTime('1970-01-01');
		$to   = new DateTime();
		$affiliate = Affiliate::find($request->getParam('affiliate_id'));
	
		$this->view->from 		= $from;
		$this->view->to   		= $to;
		$this->view->affiliate 	= $affiliate;
		$this->view->display_report = false;
		$this->view->affiliate_id = $request->getParam('affiliate_id');

		$columns = array( 
							'payable.amount' 		=> "Payout amount",
							'percentage'			=> "Payout percentage",
							'member.billing_cycle'  => "Billing cycle", 
							'promo.promo_code' 		=> "Promo code",
							'payable->created_at' 	=> "Payout date", 
							'member.first_name' 	=> "First name", 
							'member.last_name' 		=> "Last name", 
							'member.email' 			=> "Email", 
							'member.payment_status' => "Payment status", 
							'member.brokerage' 		=> "Brokerage", 
							'address' 				=> "Address", 
							'member.phone' 			=> "Phone", 
							'member.city' 			=> "City", 
							'member.state' 			=> "State/Province",
						);
		
		if ($request->isPost() && ($request->getParam('download') || $request->getParam('email'))) {
			
			if ($request->getParam('from')) $from = new DateTime($request->getParam('from'));
			if ($request->getParam('to'))   $to   = new DateTime($request->getParam('to'));			
			
			$report   = new Blastit_Report;
			$payables = Payable::all( array('conditions' => array('affiliate_id = ? AND created_at > ? AND created_at < ?', $request->getParam('affiliate_id'), $from->format('Y-m-d'), $to->format('Y-m-d') . ' 23:59:59'), 'order' => 'created_at ASC') );

			$report->enableHTML();
			$report->enableCSV();
			$report->setColumnTitles( array_values($columns) );

			foreach ($payables as $payable) {
				$member = $payable->member;
				$address = $member->address;
				if ($member->address2) $address .= ', ' . $member->address2;
				$city = $member->city;
				$state = $member->state;
				if (count($member->payments)) {
					$payment = $member->payments[0];
					if ($payment->address1 != 'undefined') {
						$address = $payment->address1;
						if ($payment->address2) $address .= ', ' . $payment->address2;
					}
					$city = $payment->city;
					$state = $payment->state;
				}
				
				$report->addRow(array(
							$payable->amount, 
							round($payable->amount/$payable->payment->amount*100), 
							$member->billing_cycle, 
							$payable->created_at->format('Y-m-d'), 
							$member->first_name, 
							$member->last_name, 
							$member->email, 
							ucfirst(str_replace('_', ' ', $member->payment_status)), 
							$member->brokerage, 
							$address, 
							$member->phone, 
							$city, 
							$state
						));
				
				
			}
			
			$filename = "Payout report for " . $affiliate->owner->first_name . " " . $affiliate->owner->last_name . " " . $from->format('Y-m-d') . " - " . $to->format('Y-m-d');

			$report->generate();
			
			if ($request->getParam('download')) {
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$fsize = filesize($report->getCSVFile());
				header("Pragma: public");
				header("Expires: 0"); 
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
				header("Cache-Control: private", false);
				header("Content-Transfer-Encoding: binary");
				header("Content-type: text/csv");
				header("Content-Disposition: attachment; filename=\"$filename\"");            
				header("Content-length: $fsize");
				fpassthru(fopen($report->getCSVFile(), 'r'));
			}
			
			if ($request->getParam('email')) {
				$report->email($_SESSION['member']->email, HELP_EMAIL, 'Your affiliate payout report', $filename);
				$message = "Your report has been emailed to " . $_SESSION['member']->email;
			}
		} else if($request->isPost() && $request->getParam('display')) {
			if ($request->getParam('from')) $from = new DateTime($request->getParam('from'));
			if ($request->getParam('to'))   $to   = new DateTime($request->getParam('to'));

			$payables = Payable::all( array('conditions' => array(
				'affiliate_id = ? AND created_at > ? AND created_at < ?',
				$request->getParam('affiliate_id'),
				$from->format('Y-m-d'),
				$to->format('Y-m-d') . ' 23:59:59'),
				'order' => 'created_at ASC')
			);

			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($payables));
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

			$this->view->paginator = $paginator;
			$this->view->display_report = true;
		}
	}

	public function newsletterpreview()
	{
		include_once (APPLICATION_PATH . "/../public/blog/wp-config.php");
		$wp_de_conn = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
		mysql_select_db(DB_NAME, $wp_de_conn);
		


		
		$request 		= $this->getRequest();	
		$params  		= $request->getParams();
		
		$this->view->params = $params;


		$query  = "select ID, guid, post_name, post_title, post_excerpt, post_date ";
		$query .= "from wp_dpvjuu_posts join wp_dpvjuu_term_relationships on wp_dpvjuu_posts.ID=wp_dpvjuu_term_relationships.object_id ";
		$query .= "where post_status='publish' and ID IN(".implode(",",  $params['blog_id']).")";
		$query .= "order by post_date desc";				
		
		//echo $query . "<BR>";
		
		$result = mysql_query($query);
		if (!$result) 
		{
		    die('Invalid query: ' . mysql_error());
		}

		$this->view->result = $result;

	}



	public function newsletterpost()
	{
		include_once (APPLICATION_PATH . "/../public/blog/wp-config.php");
		$wp_de_conn = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
		mysql_select_db(DB_NAME, $wp_de_conn);
		


		
		$request 		= $this->getRequest();	
		$params  		= $request->getParams();
		
		$this->view->params = $params;


		$query  = "select ID, guid, post_name, post_title, post_excerpt, post_date ";
		$query .= "from wp_dpvjuu_posts join wp_dpvjuu_term_relationships on wp_dpvjuu_posts.ID=wp_dpvjuu_term_relationships.object_id ";
		$query .= "where post_status='publish' and ID IN(".implode(",",  $params['blog_id']).")";
		$query .= "order by post_date desc";				
		
		//echo $query . "<BR>";
		
		$result = mysql_query($query);
		if (!$result) 
		{
		    die('Invalid query: ' . mysql_error());
		}

		$this->view->result = $result;


		//foreach (member)
		//{
		
			$member = Member::find(35);
			
		


			$return_array		= $member->likes_and_comments_trailing_thirty();
			$total_likes 		= $return_array['likes'];
			$total_comments 	= $return_array['comments'];	

			$fb_share_count 	= $member->email_shares_trailing_thirty();
			$tw_share_count 	= $member->twitter_shares_trailing_thirty();
			$li_share_count 	= $member->linkedin_shares_trailing_thirty();
			$em_share_count 	= $member->email_shares_trailing_thirty();
			
			$ph_share_count 	= $member->phonecalls_trailing_thirty();
			$bk_share_count 	= $member->booking_trailing_thirty();
			
			$your_total_shares = $fb_share_count + $tw_share_count + $li_share_count + $em_share_count;

		//$this->view->total_clicks = $member->clicks_trailing_thirty();			
	$your_clicks = $member->clicks_trailing_thirty();	
				
			$total_touches = $your_clicks + $total_likes + $total_comments + $fb_share_count + $tw_share_count + $li_share_count + $em_share_count + $ph_share_count + $bk_share_count;
		
		
		
			
			$posts = $member->monthly_posts();
			$reach = $member->total_reach();
			
			
			
			// Send welcome email
			$params	=   array(
				'member'	=>  $member,
				'total_count' => $total_touches,
				'total_clicks' => $your_clicks,
				'total_likes' => $total_likes,
				'total_comments' => $total_comments,
				'total_shares' => $your_total_shares,
				'result' => $result,
				'total_touches' => $total_touches,
				'posts' => $posts,
				'reach' => $reach
				
			);
			
			
			
			$fromEmail	=   defined('APP_EMAIL') ? HELP_EMAIL : 'noreply@'.APPLICATION_ENV.'.com';
			$fromName		=   defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
	        

			$htmlBody = $this->view->partial('email/newsletter.html.php', $params);

//$htmlBody = "hello world<BR>";			
echo $htmlBody;


$subject = COMPANY_NAME .' '.date('M') . ' Newsletter';
echo $subject . "<br/>";
			
	    		$member_email = !empty($member->email_override) ? $member->email_override : $member->email;


			$mandrill = new Mandrill(array('NEWSLETTER'));
			
			$mandrill->addTo($member->first_name . " " . $member->last_name, $member_email);
			$mandrill->setFrom($fromName, $fromEmail);
			
			$result = $mandrill->send($subject, $htmlBody);

			
			//Keep track of the Email in our local LOG
			$mail = new ZendMailLogger('NEWSLETTER', $member->id);
			$mail->addTo ( $member_email, $member_email );
			$mail->setSubject($subject);
			$mail->log();
			
		//}
		
		exit();
	}
	


	
	// method to handle comments listing
	public function comments()
	{

		//$this->view->comments = $all = Comment::all(array("conditions"=>"","order"=>"created_at DESC"));
		$conditions = array("conditions" => "1=1", "order" => "id DESC");
	
		$paginator = new Zend_Paginator(new ARPaginator('Comment', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
			
		
	}


	public function gethopper()
	{
		$this->_helper->layout->setLayout('admin');


		$listing_id = $this->getRequest()->getParam('listing_id');
		$listing = Listing::find($listing_id);


		if(!isset($listing->city_id) || empty($listing->city_id))
		{
			echo "<h1>Error: You must set the PUBLISHING CITY for this BLAST</h1>";
			echo "<p><a href='/listing/update/".$listing->id."'>Update this BLAST</a> before you publish.</p>";
			exit();
		}

		$this->view->listing 	= $listing;

	}

	public function pubassignment()
	{
		$this->_helper->layout->setLayout('admin');


		$listing_id = $this->getRequest()->getParam('listing_id');
		$listing = Listing::find($listing_id);
		$this->view->listing 	= $listing;
	
		$publish_list = PublishingQueue::find_all_by_listing_id($listing_id);			

		$pub_city_list = array();
		if($publish_list)
		{
			foreach($publish_list as $pub)
			{
				$pub_city_list[] 					= $pub->city_id;					
				$this->view->hopper_id 				= $pub->hopper_id;
				$this->view->max_inventory_threshold 	= $pub->max_inventory_threshold;
			}	
		}
		
	
		$this->view->group_list 	= Group::find('all', array('order' => 'name ASC'));
		$this->view->pub_city_list = $pub_city_list;	
	}

	public function publish()
	{
		$this->renderNothing();
						
		$request 		= $this->getRequest();	
		$params  		= $request->getParams();
		$listing_id  	= $request->getParam('listing_id');				


		//Update the APPROVAL to YES
		$listing	= Listing::find($listing_id);
		
		$listing->approval		= 1;
		$listing->approved_by_id	= $_SESSION['member']->id;
		$listing->approved_at	= date("Y-m-d H:i:s");
		$listing->save();



		//Build the EXISTING array of PUBLISHING CITIES for this bit of CONTENT
		$publish_list = PublishingQueue::find_all_by_listing_id($listing_id);			

		$current_pub_city_list = array();
		if($publish_list)
		{
			foreach($publish_list as $pub)
			{
				$current_pub_city_list[$pub->id] = $pub->city_id;					
			}	
		}



		//Figure out what CITIES this content belongs to and then PUBLISH
		$citylist = array();
		
		
		//Get all the CITIES from the selected GROUPS
		if(isset($params['grouplist']) && !empty($params['grouplist']))
		{
			foreach($params['grouplist'] as $group_id)
			{
				$group = Group::find($group_id);
				
				foreach($group->cities as $group_city)
				{
					// If the city exists, add it
					if ($group_city->city) {
						$citylist[$group_city->id] = $group_city->city_id;
					}
				}
			}
		}
		
		if(isset($params['citylist']) && !empty($params['citylist']))
		{
			foreach($params['citylist'] as $city_id)
			{			
				$citylist[$city_id] = $city_id;
			}
		}		

	
		//STEP 1, DELETE all of the CITIES that have been REMOVED by the UPDATE
		$delete_pub_queue = array();
		foreach($current_pub_city_list as $pubqueue_id => $city_id)
		{
			if(!in_array($city_id, $citylist))
			{
				$delete_pub_queue[] = $pubqueue_id;
			} 
		}
	
		if(count($delete_pub_queue))
		{
			$query = "delete from publishing_queue where id in(".implode(",",$delete_pub_queue).")";
			PublishingQueue::connection()->query($query);	
		}



		//STEP 2, ADD OR UPDATE all of the REMAINING CITIES
		$add_city 	= array();
		$update_city 	= array();
		foreach($citylist as $new_city_id)
		{
			if(!in_array($new_city_id, $current_pub_city_list))
			{
				$add_city[] = $new_city_id;
			} 
			else
			{	
				$update_city[] = $new_city_id;
			}
		}





		foreach($citylist as $city_id)
		{
			//Ok, now let's insert the item into the Publishing Queue
			if(in_array($city_id, $add_city))
			{
				$publish = new PublishingQueue();
			}
			else
			{
				$publish = PublishingQueue::find_by_city_id_and_listing_id($city_id, $listing->id);
			}



			$publish->listing_id 	= $listing->id;
			$publish->city_id 		= $city_id;
			$publish->hopper_id 	= $params['hopper_id'];
			$publish->current_count 	= 0;

			$list_city = City::find($city_id);
			
			
			if(empty($list_city->max_inventory_threshold))
			{
				$max = 10000;
			}
			else
			{
				$max = $list_city->max_inventory_threshold;
			}

			if(isset($params['max_inventory_threshold']) && !empty($params['max_inventory_threshold']))
			{
				$max = $params['max_inventory_threshold'];
			}

			//$publish->min_inventory_threshold 	= $listing->list_city->min_inventory_threshold;
			$publish->max_inventory_threshold 	= $max;

			$publish->save();



			$message = new Zend_Session_Namespace('messages');
			if (empty($message->data)) $message->data = new stdClass;
			$message->data->msg = "Listing #".$listing->id. " '".$listing->message."' was approved and published!";
		}

		$this->redirect("/admin/index/");
	}


	// method to handle emails listing
	public function emails()
	{
		$filterbymember =    $this->getRequest()->getParam('memberid');
		$filterbytype   =    $this->getRequest()->getParam('type');


		$addCondition    =   '';
		if(!empty($filterbymember))
		{
			$addCondition   =    "AND member_id=".intval($filterbymember);
			$member =    Member::find($filterbymember);
			$this->view->filterbymember    =   $member;
		}

		if(!empty($filterbytype))
		{
			$addCondition   =    "AND type='".$filterbytype."'";
			$this->view->filterbytype    =   $filterbytype;
		}

		$addCondition .= " order by id DESC ";


		$paginator = new Zend_Paginator(new ARPaginator('EmailLog','1=1 '.$addCondition));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		//$this->view->emails    =   $all    =    EmailLog::all(array("conditions"=>'1=1 '.$addCondition,"order"=>"created_at DESC"));

	

		// Forcing dropdown to hide
		$this->view->cities    =   array();
	}



	// method to handle emails listing
	public function memberemails()
	{

		$filterbymember =    $this->getRequest()->getParam('memberid');

		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}

		$member_id =    $this->getRequest()->getParam('id');


		$addCondition   =    "AND member_id=".intval($member_id);
		$member =    Member::find($filterbymember);
		$this->view->filterbymember    =   $member;
			
		$addCondition .= " order by id DESC ";

		$paginator = new Zend_Paginator(new ARPaginator('EmailLog','1=1 '.$addCondition));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		//$this->view->emails    =   $all    =    EmailLog::all(array("conditions"=>'1=1 '.$addCondition,"order"=>"created_at DESC"));


		// Forcing dropdown to hide
		$this->view->cities    =   array();

		$this->render('emails');
	}


	public function bookings()
	{

		$this->view->cities = City::find('all', array('order' => 'name ASC'));
					
		$request = $this->getRequest();

		$cities        	= new City();
		$select        	= intval($this->getSelectedAdminCity());
		$cityConditions    	=    "";

		$conditions    	= "1=1";

		if(!empty($select))
		{
			$selected      = $cities->find($select);
			$conditions    = "city_id=".intval($selected->id);
			$this->view->selectedCityObject    =   $selected;
		
			//$this->view->bookings = Booking::find('all', array('conditions' => $conditions, 'order' => 'id DESC') );
		}

		
		$params = array('conditions' => $conditions, 'order' => 'id DESC');

		$paginator = new Zend_Paginator(new ARPaginator('Booking', $params));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		
	}

	public function interactions()
	{
		$limit = null;
		$offset = null;
		
		$request = $this->getRequest();

		$cities        	= new City();
		$select        	= intval($this->getSelectedAdminCity());
		$cityConditions    	= "";

		//$limit = self::ITEMS_PER_PAGE;
		//$offset = ($this->_getParam('page', 1)-1)*self::ITEMS_PER_PAGE;


		$conditions = null;
		if(!empty($select))
		{
			$selected        = $cities->find($select);
			$conditions      = "city_id=".intval($selected->id);
			$this->view->selectedCityObject    =   $selected;

			//$this->view->interactions = Interaction::find('all', array('conditions' => $conditions, 'order' => 'id DESC', 'limit' => $limit, 'offset' => $offset) );
		}
		else
		{
			//$this->view->interactions = Interaction::find('all', array('order' => 'id DESC', 'limit' => $limit, 'offset' => $offset));
		}
		$this->view->cities = City::find('all', array('limit' => $limit, 'offset' => $offset));


		$count_conditions = "created_at >= '".date('Y-m-d 00:00:00') . "' and created_at <= '".date('Y-m-d 23:59:59')."' ";
		$this->view->interaction_total = Interaction::count('all', array('conditions' => $count_conditions) );


		$params = array('conditions' => '1=1', 'order' => 'id DESC');


		$paginator = new Zend_Paginator(new ARPaginator('Interaction', $params));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;


	}


	
	public function contentfill()
	{

		//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');
		
		$request = $this->getRequest();
		
		$this->view->member = $member = Member::find($request->getParam('member_id'));

		if ($request->isPost() )
		{
			$params = $request->getParams();
			
			//echo "<pre>";
			//print_r($params);
			
			
			$query = "select distinct listing.id from publishing_queue join listing on publishing_queue.listing_id=listing.id ";
			if(isset($params['content_type']) && $params['content_type'] == "BIG_PHOTO") $query .= " where listing.big_image = 1 ";
			elseif(isset($params['content_type']) && $params['content_type'] == "SMALL_PHOTO") $query .= " where listing.big_image = 0 ";
			
			$query .= "ORDER BY RAND() ";
			$query .= "limit ".$params['number_of_posts'];
			
			//echo $query . "<BR>";
			
			$listing_array = FanPage::find_by_sql($query);


			$facebook = $member->network_app_facebook->getFacebookAPI();

			
			foreach($listing_array as $listing_detail)
			{
				//print_r($listing_detail);	
				$listing = Listing::find($listing_detail->id);
				
				//print_r($listing);

				//Source is fp = fanpage
				$publish_array = $listing->getFacebookPayload($member, "fb");
				
				$result = $member->publishDirectToFacebook($facebook, $listing, $publish_array);
				

				
				print_r($result);
				echo "<br>";
				echo "\n\n";
			}
			
			
			echo "<br><br>";
			echo "FINISHED!<BR><BR>";
			exit();
			
		}
				
	}


	public function stats()
	{
	}

	public function engagweekly() {
		// Top performing listings for a given week
	}

	public function engagmonthly() {
		// Top performing listings for a given month
	}

	public function engagweekly_thru() {
		// Top performing listing for each week
	}

	public function engagmonthly_thru() {
		// Top performing listing for each month
	}

	public function demographics()
	{
		$age      = Member::find_by_sql("SELECT AVG( year(curdate())-year(birthday) - (right(curdate(),5) < right(birthday,5))) as avg_age from member WHERE birthday > '1920-01-01'");
		$gender_m = Member::find_by_sql("SELECT (SELECT count(id) FROM member WHERE gender = 'male') / (SELECT count(id) FROM member where gender is not null and gender != '') as male_pct");
		$regions  = Member::find_by_sql("SELECT current_state, count(id) as members FROM member GROUP BY current_state order by members DESC");

		$sql = "
				SELECT
				  COUNT(*) as total,
				  CASE
				    WHEN age >= 18 AND age <= 24 THEN '18-24'
				    WHEN age >=25 AND age <=30 THEN '25-30'
				    WHEN age >=31 AND age <=35 THEN '31-35'
				    WHEN age >=36 AND age <= 40 THEN '36-40'
				    WHEN age >=41 AND age <=45 THEN '41-45'
				    WHEN age >=46 AND age <=50 THEN '46-50'
				    WHEN age >=51 AND age <=55 THEN '51-55'
				    WHEN age >=56 AND age <=60 THEN '56-60'
				    WHEN age >=61 THEN '61+'
				  END AS ageband
				FROM
				  (
				    select DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(birthday, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(birthday, '00-%m-%d')) AS age from member WHERE  birthday > '1920-01-01'
				  ) as tbl
				GROUP BY ageband ";

		$this->view->age_bands = Member::find_by_sql($sql);
		$this->view->member_count = Member::count();
		
		$this->view->avg_age = $age[0]->avg_age;
		$this->view->gender_m = $gender_m[0]->male_pct * 100;
		$this->view->gender_f = 100  - $this->view->gender_m;
		$this->view->regions  = $regions;
	}
	
	public function index()
	{
		$this->view->publishing = true;

		$cities    = City::find('all', array('order' => 'name ASC'));

		foreach($cities as $city)
		{
			$cities_array[$city->id] = $city->name;
		}
		$this->view->cities_array = $cities_array;
		$this->view->cities = $cities;

		$request    =   $this->getRequest();
		
		$selected_city  = intval($this->getSelectedAdminCity());
		if($selected_city)
		{
			$selectedCityObject = City::find_by_id($selected_city);
			$this->view->selectedCityObject = $selectedCityObject;
			$this->view->all_cities = false;
		}
		
		
		$selectedHopper = $this->_getParam('hopper');
		$currenthopper = 1;
		if($selectedHopper) {
			$currenthopper = $selectedHopper;
		}
		$this->view->currenthopper = $currenthopper;

		$params = $request->getParams();
		
		if (count($params) <= 4) {
			// specify hopper ID if not currently searching
			$params['hopper_id'] = $currenthopper;
		}

		$this->view->tags_all = Tag::all(array('conditions' => array('publishable = 1')));
		$this->view->selectedTags = !empty($params['tags']) ? $params['tags'] : array();
		
		// search result without paginator
		$results = PublishingQueue::search($params, self::ITEMS_PER_PAGE, $this->_getParam('page', 1));
		$this->view->publish_queue =  $results;

		foreach ($params as $key => $value)
		{
			$this->view->$key    = $value;
		}


		$message = new Zend_Session_Namespace('messages');
		if(isset($message->data->msg)) $this->view->message = $message->data->msg;
		else $this->view->message = null;

		$message->unsetAll();

	}

	public function listingstatus()
	{
		$_SESSION['listing_status'] = $this->_getParam('status');
		$this->_redirect('/admin/listings');
	}

	public function listingtype()
	{
		$_SESSION['listing_type'] = strtoupper($this->_getParam('type'));
		$referer = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/admin/listings';
		$this->_redirect($referer);
	}

	public function listings()
	{
		$listing_status = null;
		$listing_type = null;

		if(isset($_SESSION['listing_status'])) $listing_status    = $this->view->status    = $_SESSION['listing_status'];
		if(isset($_SESSION['listing_type'])) $listing_type    = $this->view->type    = $_SESSION['listing_type'];

	
		//$this->view->members = Member::find('all', array('order' => 'first_name'));

		$request    = $this->getRequest();

		if ($request->isPost())
		{
			$params        = $request->getParams();

			$params['id'] = $params['listing_id'];
			$params['message'] = $params['listing_message'];


			// search result without paginator
			$results    = Listing::search($params, self::ITEMS_PER_PAGE, $this->_getParam('page', 1));
			$this->view->paginator =  $results;

			unset($params['id']);
			unset($params['message']);


			foreach ($params as $key => $value)
			{
				$this->view->$key    = $value;
			}
		}
		else
		{
			//$cities    = City::all();
			foreach($this->view->cities as $city)
			{
				$cities_array[$city->id] = $city->name;
			}
			$this->view->cities_array = $cities_array;
			$this->view->cities = $this->view->cities;

			$select = intval($this->getSelectedAdminCity());

			$selectedCity   =    "";

			$joins      = array();
			$conditions = array();

			if(!empty($select))
			{
				$selected      = City::find_by_id($select);
				$conditions[]  = "city_id=".intval($selected->id);
				$this->view->selectedCityObject    =   $selected;
			}

			$published  =    0;
			if( $listing_status == 'published')
			{
				$conditions[] = 'published = 1 and approval = 1';
			}
			elseif ($listing_status == 'deleted' )
			{
				$conditions[] = "status = 'deleted'";
			}
			elseif ($listing_status == 'publishing' )
			{
				$conditions[] = 'published = 0 and approval = 1';
				$joins[] = 'JOIN publishing_queue ON listing_id = listing.id';
			}
			elseif( $listing_status == 'unpublished')
			{
				$conditions[] = 'published = 0 and (approval=0 or approval=1) AND publishing_queue.id IS NULL';
				$joins[] = 'LEFT JOIN publishing_queue ON listing_id = listing.id';
			}
			elseif ($listing_status == 'inaccessible' )
			{
				$conditions[] = 'published = 0 and approval = 1';
				$joins[] = "JOIN url_mapping ON url_mapping.listing_id = listing.id AND url_inaccessible_since IS NOT NULL";
			}
			elseif ($listing_status == 'retired-inaccessible' )
			{
				$conditions[] = 'published > 0 and published_on > 0';
				$joins[] = "JOIN url_mapping ON url_mapping.listing_id = listing.id AND url_inaccessible_since IS NOT NULL";
			}

			if( $listing_type == Listing::TYPE_LISTING)
			{
				$conditions[] = "blast_type='".Listing::TYPE_LISTING."'";
			}
			elseif ($listing_type == Listing::TYPE_CONTENT )
			{
				$conditions[] = "blast_type='".Listing::TYPE_CONTENT."'";
			}
			elseif( $listing_type == Listing::TYPE_IDX)
			{
				$conditions[] = "blast_type='".Listing::TYPE_IDX."'";
			}
			else
			{
				$conditions[] = "(blast_type='".Listing::TYPE_CONTENT."' or blast_type='".Listing::TYPE_LISTING."') ";
			}

			//echo "<h1>".$conditions."</h1>";

			$conditions = array("conditions" => implode(' AND ', $conditions), 'joins' => $joins, "order" => "id DESC");
			
			$paginator = new Zend_Paginator(new ARPaginator('Listing', $conditions));
			//$paginator->setCurrentPageNumber($page);
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;

			$message = new Zend_Session_Namespace('messages');
			if(isset($message->data->msg)) {
				$this->view->message = $message->data->msg;
			} else {
				$this->view->message = null;
			}

			$message->unsetAll();

			if ($listing_type == 'SMLS') {
				$this->render('smls-listings');
			}
		}


	}

	public function deleteconfirm() 
	{
		$this->view->target = $this->getRequest()->getParam('target');
		$this->view->type   = $this->getRequest()->getParam('type');
		$this->_helper->layout->disableLayout();
	}

	public function allposts()
	{
		//ini_set('display_errors','On');


		$request        =  $this->getRequest();
		$selected_day   =  $request->getParam('for', null);
		
		if ($request->isPost()) 
		{
			if ($delete = $request->getParam('delete', null)) 
			{
				if ($post = Post::find($delete)) 
				{
					$post->delete();
					$action = new DeleteAction;
					$action->id = $_SESSION['member']->id;
					$action->post_id = $post->id;
					$action->reason = $_POST['reason'];
					$action->save();
				}
				
				$message    =	new Zend_Session_Namespace('messages');			
				if (empty($message->data)) $message->data = new stdClass;
				$message->data->msg   =	"Post $delete has been deleted.";
			}
			
		} elseif ($undelete = $request->getParam('undelete', null)) {
			
			if ($post = Post::find_deleted($undelete)) 
			{
				$post->undelete();
			}
			
			$message    =	new Zend_Session_Namespace('messages');			
			if (empty($message->data)) $message->data = new stdClass;
			$message->data->msg   =	"Post $undelete has been undeleted.";
		}
		
		if ($selected_day)
		{
			// to prevent invalid day
			$selected_day    = date('Y-m-d', strtotime($selected_day));
		}
		else
		{
			//Default to "today"
			$selected_day = date('Y-m-d');
		}

		$conditions        = '1';

		if($selected_day)
		{
			$conditions .= " AND created_at BETWEEN DATE( '". $selected_day ."' ) - INTERVAL 0 SECOND AND
			DATE( '". $selected_day ."' ) + INTERVAL 1 DAY - INTERVAL 1 SECOND";

		}
		
		$this->view->deleted = 0;
		
		if ($request->getParam('deleted')) {
			$this->view->deleted = 1;
			$conditions .= " AND status = 'deleted'";
		}

		$paginator = new Zend_Paginator(new ARPaginator('Post', array('conditions' => $conditions, 'order' => 'id DESC', 'include' => array('member'))));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		#$this->view->posts = Post::find('all', array('conditions' => $conditions, 'order' => 'id DESC'  ));
		//$query = "select count( distinct member_id ) as total from post where listing_id=".$listing_id;
		//echo "<h1>".$query."</h1>";

		//$total = Post::find_by_sql($query);

		#$this->view->total_unique = count($this->view->posts);
		$this->view->selected_day    = $selected_day;
		$this->view->total_posts    = $paginator->getTotalItemCount();
		$this->view->need_page        = (self::ITEMS_PER_PAGE < $this->view->total_posts);

	}

	public function posts()
	{
		$this->view->view_type = "LISTING";
		//ini_set('display_errors','On');

		$request    =   $this->getRequest();
		$this->view->listing_id    = $listing_id  =  $request->getParam('listing_id');
		$this->view->blast_id  =  $request->getParam('blast_id');

		$this->view->paginator = array();
		if($listing_id){
			$conditions = "listing_id=".$listing_id;
			//$this->view->posts = Post::find('all', array('conditions' => $conditions, 'order' => 'id DESC'  ));

			$paginator = new Zend_Paginator(new ARPaginator('Post', array(
				'select'=> "p.*, (SELECT count(`count`) FROM click_stat_by_member_listing WHERE member_id = p.member_id AND listing_id = p.listing_id) AS clicks", 
				'from' => 'post AS p',
				'conditions' => $conditions, 
				'order' => 'id DESC'
			)));
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;
		}

		$query = "select count( distinct member_id ) as total from post where status != 'deleted' AND listing_id=".$listing_id;
		//echo "<h1>".$query."</h1>";

		$total = Post::find_by_sql($query);

		$this->view->total_unique = $total[0]->total;
		$this->view->total_posts    = $paginator->getTotalItemCount();

	}

	public function memberlistings()
	{
		$this->view->view_type = "MEMBER";

		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}


		$request    =   $this->getRequest();
		$member_id  =  $request->getParam('member');

		$this->view->member = Member::find_by_id($member_id);

		if($member_id)
		{
			$this->view->member = Member::find_by_id($member_id);
			$conditions = array("conditions" => array('member_id = ?', $member_id), "order" => "id DESC");
			
			$paginator = new Zend_Paginator(new ARPaginator('Listing', $conditions));
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;
		}

	}

	public function memberposts()
	{
		$this->view->view_type = "MEMBER";

		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}

		$request    =   $this->getRequest();
		$member_id  =  $request->getParam('member');
		$this->view->member = Member::find_by_id($member_id);



		if($member_id)
		{
			$conditions = array("member_id = ?", $member_id);

			$options = array(
				'select'=> "p.*, (SELECT count(`count`) FROM click_stat_by_member_listing WHERE member_id = p.member_id AND listing_id = p.listing_id) AS clicks", 
				'from' => 'post AS p',
				'conditions' => $conditions, 
				'order' => 'id DESC'
			);
			$paginator = new Zend_Paginator(new ARPaginator('Post', $options));

			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;

			//$paginator->getPages()->ajaxmethod = "loadPost";

		}

		$this->render("posts");
		
	}

	public function setmemberstatus()
	{
		$this->view->selected_status  =  $this->getRequest()->getParam('member_status');


		switch ($this->view->selected_status)
		{
			case 'active':
				$this->_redirect("/admin/members");
				break;
			case 'deleted':
				$this->_redirect("/admin/deletedmembers");
				break;
			case 'threestrikes':
				$this->_redirect("/admin/threestrikesmembers");
				break;
			case 'inactive':
				$this->_redirect("/admin/inactivemembers");
				break;
			case 'publishing':
				$this->_redirect("/admin/publishing");
				break;
			case 'publishsettings':
				$this->_redirect("/publishsettings/index");
				break;
			case 'fbpword':
				$this->_redirect("/admin/fbpword");
				break;
			case 'twpword':
				$this->_redirect("/admin/twpword");
				break;
			case 'reach-by-city':
				$this->_redirect("/admin/reach-by-city");
				break;
			case 'growth':
				$this->_redirect("/admin/growth");
				break;
			default:
				$this->_redirect("/admin/allmembers");
		}
	}

	public function fbpword()
	{

		$this->view->display_type = "INACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		
		$conditions = null;

		$this->view->selected_status = 'fbpword';
		$query  = 'SELECT DISTINCT member_id ';
		$query .= 'FROM post ';
		$query .= 'WHERE message LIKE "%Error validating access token: The session has been invalidated because the user has changed the password.%" order by member_id DESC';

		$found_members =  Member::find_by_sql($query);
		$members_array = array();
		if($found_members){
			foreach($found_members as $fmember){
				//Make sure this isn't an old error
				$query = 'select id as last_post, member_id, fb_post_id from post where member_id='.$fmember->member_id . ' and (message like "%FB%" or message like "%factbook%" or message like "%facebook%") order by id DESC limit 1';
				$last_post =  Member::find_by_sql($query);
				if($last_post[0]->fb_post_id == "" or strtoupper($last_post[0]->fb_post_id) == "EMPTY"){
					//echo "<h1>I am adding...</h1>";
					//$member = Member::find_by_id($fmember->member_id);
					$members_array[] = $fmember->member_id;
					//if($member) $members_array[] = $member;
				}
			}
		}
		
		$this->view->paginator = array();
		
		if(count($members_array)>0)
		{
			$conditions .= " id IN(" .implode(',',$members_array). ")";
			$conditions = array("conditions" => $conditions, "order" => "id DESC");

			$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;
		}

		$this->render("members");

	}

	public function twpword()
	{

		$this->view->display_type = "INACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		
		$this->view->selected_status = 'twpword';
		$query  = 'SELECT DISTINCT member_id ';
		$query .= 'FROM post ';
		$query .= 'WHERE message LIKE "%Twitter post FAIL. ERROR:Could not authenticate with OAuth%" order by member_id DESC';

		$found_members =  Member::find_by_sql($query);

		$members_array = array();
		if($found_members)
		{				
			foreach($found_members as $fmember)
			{
				//Make sure this isn't an old error
				$query = 'select id as last_post, twitter_post_id from post where member_id='.$fmember->member_id . ' and (message like "%TWITTER%" or message like "%Twitter%") order by id DESC limit 1';
				$last_post =  Member::find_by_sql($query);
				 
				if($last_post[0]->twitter_post_id == "")
				{
					//echo "<h1>I am adding...</h1>";
					$members_array[] = $fmember->member_id;
					//$member = Member::find_by_id($fmember->member_id);
					//if($member) $members_array[] = $member;
				}
			}
		}
		
		$this->view->paginator = array();			
		if(count($members_array)>0)
		{
			$conditions .= " id IN(" .implode(',',$members_array). ")";
			$conditions = array("conditions" => $conditions, "order" => "id DESC");

			$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;
		}

		$this->render("members");
		//exit();

	}

	public function newcreditcard()
	{
		$request = $this->getRequest();
		$params = $request->getParams();

		$id = $this->_getParam('id', null);
		$this->view->member = $member = Member::find($id);

		$this->view->rebill = 0;
		if(isset($_REQUEST['rebill']) && $_REQUEST['rebill']) $this->view->rebill = $_REQUEST['rebill'];

		if(isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1183) mail("alen@alpha-male.tv","FB Click - Payment Started","");
		if(isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1184) mail("alen@alpha-male.tv","Google Click - Payment Started","");
	
		//Set to STRIPES
		// Set payment processing to default
		$default_payment_gateway = PaymentGateway::find_by_is_default(1);
		$member->payment_gateway_id = $default_payment_gateway->id;
		
		$member->save();


		$request = $this->getRequest();

		$states = State::all(array('order' => 'name ASC'));
		$this->view->states = $states;

		if($request->isPost())
		{
			$payment_gateway = $member->payment_gateway;
			
			if(
				$payment_gateway->newStripeCard($member, $params) && 
				$payment_gateway->saveAuthorization($member,$params)
			)
			{
				$member->update_account_status(false);
				$this->_redirect("/admin/member/".$member->id);										
				//$this->render("makepaymentquestion");
			}
			else
			{
				//There was a problem, show the error
				$this->view->payment_error = $member->payment_error;
			}
		}
	}
	
	public function applypromo() {
		
		$request = $this->getRequest();
		
		$member = Member::find($request->getParam('id'));
		
		if ($request->isPost()) {
			$promo = Promo::find($request->getParam('promo_id'));
			$member->applyPromo($promo);
			$member->save();
			$this->Redirect("/admin/member/" . $member->id);
		}
		
		$conditions = array();
		
		$affiliate_promos = array();
		if ($member->referring_affiliate) { 
			$affiliate_promos = Promo::find_all_by_affiliate_id($member->referring_affiliate_id);
		} 
		
		$this->view->promos = !empty($affiliate_promos) ? $affiliate_promos : Promo::all();
		
		$this->view->member = $member;
		
	}
	
	function memberpayment()
	{
		Logger::setEcho(true);
		Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
		Logger::logTimeMarker();
		echo "<h1>Actual payment to happen...</h1>";
		//exit();

		$member_id    = $this->_getParam('id', null);
		$member = Member::find($member_id);

		echo "<h1>Attempt " . $member->payment_gateway->name . " payment</h1>";
				
		if($member->payment_gateway->processRecurringPayment($member, false))
		{
			echo "<h1>Success!</h1>";
		}
		else
		{
			echo "<h1>Error:".$member->payment_error."</h1>";
		}

		echo "Forced ".$member->payment_gateway->name." Payment Made<BR><BR>";
		echo "Finished.<BR>";
		exit();
	}


	public function members()
	{
		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();

		$this->view->selected_status = 'active';
		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	 = " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$conditions  = "	( status = 'active' and ( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) ";
		$conditions .= "or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) and payment_status != 'cancelled' and payment_status != 'payment_failed' and (next_payment_date IS NULL or next_payment_date >= NOW()) )";
		$conditions .= " OR account_type='free' ".$cityConditions;


		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));


		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
		$this->view->city_array = $this->getcityarray();

	}

	public function linkedin_members()
	{
		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();

		$this->view->selected_status = 'linkedin_active';
		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$conditions  = "	( status = 'active' and (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) ";
		$conditions .= " and payment_status != 'cancelled' and payment_status != 'payment_failed' and (next_payment_date IS NULL or next_payment_date >= NOW()) )";
		$conditions .= " OR account_type='free' ".$cityConditions;

		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
		$this->view->city_array = $this->getcityarray();

	}

	public function expiredtoken()
	{
		$this->view->display_type = "INACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		
		$this->view->selected_status = 'expiredtoken';
		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$conditions = " token_expires IS NULL or token_expires <= NOW() ".$cityConditions;
		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator  = new Zend_Paginator(new ARPaginator('Member', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
		$this->render("members");

	}


	public function deletedmembers()
	{
		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		
		$this->view->selected_status = 'deleted';

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$conditions = "status = 'deleted' ".$cityConditions;

		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
			

		$this->render("members");
	}

	public function publishing()
	{
		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		
		$this->view->selected_status = 'publishing';
		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		if($select)
		{
			$selected        = $cities->find($select);
			$cityConditions= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$conditions = "( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and
						linkedin_access_token IS NOT NULL) or (twitter_publish_flag > 0 and twitter_fails < 3 and
						twitter_access_token IS NOT NULL) ) AND status='active' ".$cityConditions;

		$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

	}


	public function hidden()
	{
		$members = Member::find('all', array('order' => 'first_name ASC'));
		$payments = Payment::find('all', array('conditions' => 'payment_type="clientfinder"'));


		echo "<h1>".count($members)."</h1>";

		foreach($payments as $payment)
		{
			$confirmed[$payment->member_id] = $payment->member_id;
		}



		echo "<table border='1'>";

		echo "<tr>";
		echo "<th>ID</th>";
		echo "<th>First Name</th>";
		echo "<th>Last Name</th>";
		echo "<th>FB Email</th>";
		echo "<th>Primary Email</th>";
		echo "<th>Phone</th>";
		echo "<th>Brokerage</th>";
		echo "</tr>";


		$phone_count = 0;
		foreach($members as $member)
		{
			if(!in_array($member->id, $confirmed))
			{
				echo "<tr>";
				echo "<td>".$member->id."&nbsp;</td>";
				echo "<td>".$member->first_name."&nbsp;</td><td>".$member->last_name."&nbsp;</td>";
				echo "<td>".$member->email."&nbsp;</td><td>".$member->email_override."&nbsp;</td>";
				echo "<td>".$member->phone."&nbsp;</td><td>".$member->brokerage."&nbsp;</td>";

				if($member->phone) $phone_count++;

				echo "</tr>";
			}

		}

		echo "</table>";

		echo "<h1>Phone Count: ".$phone_count."</h1>";

		exit();
	}
	
	public function memberstatsdaily()
	{
		$year = $this->getRequest()->getParam('y', date('Y'));
		$month = $this->getRequest()->getParam('m', date('n', strtotime('-1days')));
		$date_start = $year . '-' . $month . '-01';
		$date_end = date('Y-m-t', strtotime($date_start));
		$days_in_month = date('t', strtotime($date_start));
		$month_name = date('F', strtotime($date_start));
		
		$stats = MemberStatsDaily::all(array(
			'conditions' => array('date >= ? AND date <= ?', $date_start, $date_end)
		));
		
		$stats_by_day_of_month = array();
		if (!empty($stats)) {
			foreach ($stats as $stat) {
				$stats_by_day_of_month['day' . $stat->date->format('j')] = $stat;
			}
		}
		
		$this->view->year = $year;
		$this->view->month = $month;
		$this->view->month_name = $month_name; 
		$this->view->days_in_month = $days_in_month;
		$this->view->stats_by_day_of_month = $stats_by_day_of_month;
	}

	public function growth()
	{
		$this->view->display_type 	= "ACTIVE";
		$this->view->affiliate_array 	= $this->set_affiliates();		
		$this->view->total_signups 	= Member::count();



		$query = "select count(distinct(member_id)) as total_members from payment join member on payment.member_id=member.id where payment_type = 'clientfinder'";
		$all_cc_members = Payment::find_by_sql($query);
		$this->view->all_cc_members = $all_cc_members[0]->total_members;
		unset($all_cc_members);
		
		
		$member_stats_daily = MemberStatsDaily::last();

		$stats = array();

		$stats['projected_revenue'] 			= MemberStatsFinancial::getProjectedMonthlyRevenue();
		$stats['projected_revenue_30'] 			= MemberStatsFinancial::getProjectedRevenue30day();
		$stats['projected_revenue_paid_member'] = MemberStatsFinancial::getProjectedMonthlyRevenuePaidMember();

		$stats['expected_revenue_month'] = MemberStatsFinancial::getExpectedRevenueCurrentMonth('month');
		$stats['expected_revenue_biannual'] = MemberStatsFinancial::getExpectedRevenueCurrentMonth('biannual');
		$stats['expected_revenue_annual'] = MemberStatsFinancial::getExpectedRevenueCurrentMonth('annual');

		$stats['renewals_month'] = MemberStatsFinancial::getExpectedRevenueCurrentMonthPaidMember('month');
		$stats['renewals_biannual'] = MemberStatsFinancial::getExpectedRevenueCurrentMonthPaidMember('biannual');
		$stats['renewals_annual'] = MemberStatsFinancial::getExpectedRevenueCurrentMonthPaidMember('annual');

		$stats['actual_revenue_current_month'] = MemberStatsFinancial::getActualRevenueCurrentMonth();
			
		$stats['projected_monthly_average_revenue_month'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenue('month');
			
				
		// Member counts
		//$stats['paid_member_count_month'] 		= MemberStatsFinancial::getPaidMemberCount('month');
		//$stats['paid_member_count_biannual'] 	= MemberStatsFinancial::getPaidMemberCount('biannual');
		//$stats['paid_member_count_annual'] 	= MemberStatsFinancial::getPaidMemberCount('annual');
		$stats['paid_member_count'] 			= MemberStatsFinancial::getPaidMemberCount();
		//$stats['cctrial_member_count_month'] 	= MemberStatsFinancial::getCcTrialMemberCount('month');
		//$stats['cctrial_member_count_biannual'] = MemberStatsFinancial::getCcTrialMemberCount('biannual');
		//$stats['cctrial_member_count_annual'] 	= MemberStatsFinancial::getCcTrialMemberCount('annual');
		$stats['cctrial_member_count'] 		= MemberStatsFinancial::getCcTrialMemberCount();
		//$stats['member_count_month'] 			= MemberStatsFinancial::getMemberCount('month');
		//$stats['member_count_biannual'] 		= MemberStatsFinancial::getMemberCount('biannual');
		//$stats['member_count_annual'] 		= MemberStatsFinancial::getMemberCount('annual');
		$stats['member_count'] 				= MemberStatsFinancial::getMemberCount();
		
		
		$stats['agent_count'] 				= MemberStatsFinancial::getPaidMemberCount(null, 1);
		$stats['broker_count'] 				= MemberStatsFinancial::getPaidMemberCount(null, 2);
		
		
		
		$stats['projected_monthly_average_price'] = MemberStatsFinancial::getProjectedMonthlyAveragePrice();	
		
		$stats['projected_monthly_average_price_paid_member'] = MemberStatsFinancial::getProjectedMonthlyAveragePricePaidMember();
		
		$stats['average_lifetime_value'] = !empty($member_stats_daily) ? $member_stats_daily->avrg_lifetime_value : 0;
			
		
		$this->view->stats = $stats;
		
		
		
		// Update todays member signup stats
		MemberStatsSignup::populateDay(date('Y-m-d'));
		
		$sql = "SELECT * FROM member_stats_signup WHERE date >= ? ORDER BY date DESC";
		$member_signups = Member::find_by_sql($sql, array(date('Y-m-d', strtotime('-30 days'))));
		$this->view->member_signups = $member_signups ;
		
		$member_signup_totals = array(
			'ccnew' => 0,
			'cc_paid_cancel' => 0,
			'total' => 0,
			'cc' => 0,
			'nocc' => 0,
			'cc_paid' => 0,
			'cc_cancel' => 0,
			'cc_failed_payment' => 0,
			'nocc_paid' => 0,
			'nocc_cancel' => 0,
			'nocc_failed_payment' => 0,
			'revenue_projected_max' => 0,
			'revenue_projected' => 0,
			'revenue_actual' => 0,
			'ad_spend' => 0,
			'profit' => 0
		);
		if (!empty($member_signups)) {
			foreach($member_signups as $member_signup)
			{
				$member_signup_totals['ccnew'] += $member_signup->ccnew;
				$member_signup_totals['cc_paid_cancel'] += $member_signup->cc_paid_cancel;
				$member_signup_totals['total'] += $member_signup->total;
				$member_signup_totals['cc'] += $member_signup->cc;
				$member_signup_totals['nocc'] += $member_signup->nocc;
				$member_signup_totals['cc_paid'] += $member_signup->cc_paid;
				$member_signup_totals['cc_cancel'] += $member_signup->cc_cancel;
				$member_signup_totals['cc_failed_payment'] += $member_signup->cc_failed_payment;
				$member_signup_totals['nocc_paid'] += $member_signup->nocc_paid;
				$member_signup_totals['nocc_cancel'] += $member_signup->nocc_cancel;
				$member_signup_totals['nocc_failed_payment'] += $member_signup->nocc_failed_payment;
				$member_signup_totals['revenue_projected_max'] += $member_signup->revenue_projected_max;
				$member_signup_totals['revenue_projected'] += $member_signup->revenue_projected;
				$member_signup_totals['revenue_actual'] += $member_signup->revenue_actual;
				$member_signup_totals['ad_spend'] += $member_signup->ad_spend;
				$member_signup_totals['profit'] += $member_signup->profit;
			}
		}
		$this->view->member_signup_totals = $member_signup_totals;


		//CANCEL MEMBERS
		$query = "select count(distinct(member.id)) as total_members from payment join member on payment.member_id=member.id where payment_type = 'clientfinder' and member.payment_status='cancelled'";
		$dropoff_members = Payment::find_by_sql($query);
		$this->view->dropoff_members = $dropoff_members[0]->total_members;
		unset($dropoff_members);

		//PAID CANCEL MEMBERS
		$query  = "select count(distinct(member.id)) as total_dropoffs from payment join member on payment.member_id=member.id where payment_type = 'clientfinder' and member.payment_status='cancelled' and payment.amount > 0";
		$paid_dropoff_members = Payment::find_by_sql($query);
		$this->view->paid_dropoff = $paid_dropoff_members[0]->total_dropoffs;
		unset($paid_dropoff_members);

		//14 DAY TRIAL
		$query  = "select distinct(member.id) as member_id, count(payment.id) as payment_count from member left outer join payment on member.id=payment.member_id ";
		$query .= "where member.created_at > DATE( NOW( ) ) - INTERVAL 7 DAY - INTERVAL 1 SECOND group by member.id";
		$trial = Payment::find_by_sql($query);
		$trial_member 				= 0;
		//$trial_14day_signup_members 	= array();
		if($trial)
		{
			foreach($trial as $member)
			{
				if($member->payment_count == 0)
				{
					$trial_member++;	
					//$trial_14day_signup_members[]	=	$member->member_id;
				}
			}
		}
		$this->view->trial_member = $trial_member;
		unset($trial);
		
		
		
		$start_date = date('Y-m-01', strtotime('-32 months'));
		$conditions = array("conditions" => array('date >= ?', $start_date), "order" => "date DESC");
		$member_stats_monthly = MemberStatsMonthly::all($conditions);
		$member_stats_monthly = $member_stats_monthly;
	
		$monthly_stats = array();
		$monthly_stats_totals = array(
			'total_signups' => 0,
			'paid_signups' => 0,
			'paid_cancels' => 0,
			'new_signup_percent' => 0,
			'paid_signup_percent' => 0,
			'paid_daily_average' => 0
		);
		$monthly_stats_avrgs = array(
			'total_signups' => 0,
			'paid_signups' => 0,
			'paid_cancels' => 0,
			'new_signup_percent' => 0,
			'paid_signup_percent' => 0,
			'paid_daily_average' => 0
		);
		if (!empty($member_stats_monthly)) {
			$latest_stats = $member_stats_monthly[0];
			$signup_alltime = $latest_stats->signup_alltime;
			$signup_alltime_paid = $latest_stats->cc_trial_alltime;
			
			$months = count($member_stats_monthly);
			
			foreach ($member_stats_monthly as $stat) {
				$signup_new = number_format($stat->signup_new);
				$cc_trial_new = number_format($stat->cc_trial_new);
				$paid_cancel = number_format($stat->paid_cancel);
				$new_signup_percent = ($signup_alltime) ? number_format($stat->signup_new / $signup_alltime, 2) : 100.00;
				$paid_signup_percent = ($signup_alltime_paid) ? number_format($cc_trial_new / $signup_alltime_paid, 2) : 100.00;
				$paid_daily_avrg = number_format($cc_trial_new / date('t'), 2);
				
				$monthly_stats[] = array(
					'month_string' => $stat->date->format('F Y'), 
					'total_signups' => $signup_new,
					'paid_signups' => $cc_trial_new,
					'paid_cancels' => $paid_cancel,
					'new_signup_percent' => $new_signup_percent,
					'paid_signup_percent' => $paid_signup_percent,
					'paid_daily_average' => $paid_daily_avrg
				);
			}
			
			foreach ($monthly_stats as $monthly_stat) {
				$monthly_stats_totals['total_signups'] += $monthly_stat['total_signups'];
				$monthly_stats_totals['paid_signups'] += $monthly_stat['paid_signups'];
				$monthly_stats_totals['paid_cancels'] += $monthly_stat['paid_cancels'];
				$monthly_stats_totals['new_signup_percent'] += $monthly_stat['new_signup_percent'];
				$monthly_stats_totals['paid_signup_percent'] += $monthly_stat['paid_signup_percent'];
				$monthly_stats_totals['paid_daily_average'] += $monthly_stat['paid_daily_average'];
			}
			
			foreach ($monthly_stats_totals as $key => $value) {
				$monthly_stats_avrgs[$key] = number_format($value / $months, 2);
			}
		}
		
		$this->view->member_stats_monthly = $monthly_stats;
		$this->view->member_stats_monthly_totals = $monthly_stats_totals;
		$this->view->member_stats_monthly_avrgs = $monthly_stats_avrgs;
	}
	
	public function membersignupstatsedit()
	{
		$request = $this->getRequest();
		$date = $request->getParam('date', null);
		$this->view->date = !empty($date) ? $date : '';
		

		if (empty($date)) {
			$this->view->message = 'You must specify a date';
		}
		
		$stats = MemberStatsSignup::find_by_date($date);
		$this->view->stats = $stats;
		
		if ($request->isPost()) {
			
			if (empty($stats)) {
				$this->view->message = 'No stats record exists for that date yet';
			} else {
				$ad_spend = (float) $request->getParam('ad_spend', 0);
				$profit = $stats->revenue_actual - $ad_spend;
				
				$stats->ad_spend = $ad_spend;
				$stats->profit = $profit;
				$stats->save();
				$this->redirect('/admin/growth');
			}
		}
	}
	
	private function _getFirstPaymentDate() {
			$payments = Payment::find_by_sql("SELECT MIN(payment.created_at) AS created_at FROM payment JOIN member ON payment.member_id = member.id WHERE account_type='paid' AND payment_type='clientfinder' AND payment.amount > 0");
			return $payments[0]->created_at->format('Y-m-d h:i:s');
	}

	private function _getPaymentCount() {
		$conn = Payment::connection();
		$sql = "SELECT COUNT(payment.id) as payments FROM payment JOIN member ON payment_type='clientfinder' AND payment.member_id = member.id AND  amount > 0 WHERE account_type='paid';";
		$query = $conn->query($sql);
		$result = $query->fetch();
		return  $result['payments'];
	}

	private function _getMemberCount($mode = 1) {
		$conn = Payment::connection();
		switch($mode) {
			case 1:
				// for everyone who has EVER GIVEN US A CREDIT CARD
				$sql = "SELECT COUNT(member.id) as members FROM member 
							WHERE EXISTS (SELECT id FROM payment WHERE payment_type='clientfinder' AND payment.member_id = member.id);";
				break;
			case 2:
				// for everyone who has successfully paid at least once
				$sql = "SELECT COUNT(member.id) as members FROM member 
							WHERE EXISTS (SELECT id FROM payment WHERE payment_type='clientfinder' AND payment.member_id = member.id AND amount > 0) AND 
								  account_type='paid';";
				break;
			case 3:
				// for everyone who DID NOT GIVE US A CREDIT CARD AT FIRST
				// ?? not yet implemented
				break;
		}
		$query = $conn->query($sql);
		$result = $query->fetch();
		return  $result['members'];
	}

	private function _getAverageLifetime($mode = 1) 
	{		
		$conn  = Member::connection();
		$join = "JOIN";
		$account_type = "";
		switch($mode) 
		{
			case 1:
				// LTV for everyone who has EVER GIVEN US A CREDIT CARD
				$join = "LEFT JOIN";
				break;
			case 2:
				// LTV for everyone who has successfully paid at least once
				$account_type = "AND account_type = 'paid'";
				break;
			case 3:
				// LTV for everyone who DID NOT GIVE US A CREDIT CARD AT FIRST
				// ?? not yet implemented
				break;
		}
		$sql = "SELECT AVG(count) as lifetime 
				  FROM (
							SELECT member.id, count(payment.id) as count 
							  FROM member $join payment ON member.id = payment.member_id $account_type AND payment_type = 'clientfinder' AND payment.amount > 0
							  JOIN member_type ON member.type_id = member_type.id
							  WHERE EXISTS (SELECT id FROM member_type WHERE member.type_id = member_type.id AND member_type.is_customer = 1)
							  		AND EXISTS (SELECT id FROM payment WHERE member.id = payment.member_id AND payment_type = 'clientfinder')
							  GROUP BY member.id
					   ) AS tmptable;";
		//echo $sql;
	
		$query = $conn->query($sql);
		$result = $query->fetch();
		return $result['lifetime'];
//		return (1-$churn)/$churn;
	}

	private function _getAverageRevenue($mode = 1) {
		$conn  = Member::connection();
		$join = "JOIN";
		$having = "";
		switch($mode) {
			case 1:
				// LTV for everyone who has EVER GIVEN US A CREDIT CARD
				$join = "LEFT JOIN";
				break;
			case 2:
				// LTV for everyone who has successfully paid at least once
				$having = "HAVING member_revenue > 0";
				break;
			case 3:
				// LTV for everyone who DID NOT GIVE US A CREDIT CARD AT FIRST
				// ?? not yet implemented
				break;
		}
		$sql = "SELECT SUM(payment.amount) as amount, COUNT(payment.id) as count FROM payment JOIN member ON payment_type='clientfinder' AND payment.member_id = member.id AND account_type='paid' WHERE payment.amount > 0;";
		$query = $conn->query($sql);
		$result = $query->fetch();

		return ($result['count']) ? $result['amount']/$result['count'] : 0;
	}

	private function _getSignups($from, $to, $include_trials) {
		$conn = Member::connection();
		if ($include_trials) {
			$query = $conn->query("SELECT count(id) as count FROM member
										   WHERE EXISTS (SELECT id FROM member_type WHERE member.type_id = member_type.id AND member_type.is_customer = 1) AND
										   		 created_at > '$from' AND
												 created_at <= '$to';");
		} else {
			$query = $conn->query("SELECT count(member.id) as count FROM member
										   WHERE EXISTS (SELECT id FROM member_type WHERE member.type_id = member_type.id AND member_type.is_customer = 1) AND
										   		 EXISTS (
										   					SELECT id FROM payment 
										   						     WHERE member.id = payment.member_id AND 
										   						     	   payment_type='clientfinder' AND 
										   						     	   payment.created_at > '$from' AND
												 						   payment.created_at <= '$to'
												 		);");
		}
		$query->execute();
		$result = $query->fetch();
		return $result['count'];
	} 

	private function _getCancellations($from, $to, $include_trials) {
	
		$conn = Member::connection();
		$status = "'payment_failed', 'cancelled'";
		
		if ($include_trials) {
			$status .= ", 'signup_expired'";
		}
		
		$query = $conn->query("SELECT count(member.id) as count FROM member
									   WHERE EXISTS (SELECT id FROM payment WHERE member.id = payment.member_id AND payment_type='clientfinder') AND
									   		 EXISTS (SELECT id FROM member_type WHERE member.type_id = member_type.id AND member_type.is_customer = 1) AND
									   		 next_payment_date > '$from' AND 
									   		 next_payment_date <= '$to' AND
									   		 payment_status IN ($status);");
		$query->execute();
		$result = $query->fetch();
		return $result['count'];
		
	} 

	public function set_affiliates()
	{
		$affiliate_list = Member::find_by_sql("select member.id, first_name, last_name from member join affiliate ON affiliate.member_id = member.id;");
		$affiliate_array = array();
		
		foreach($affiliate_list as $affiliate)
		{
			$affiliate_array[$affiliate->id] = $affiliate->first_name . " " . $affiliate->last_name;	
		}	
		
		return($affiliate_array);
	}
		
	public function allmembers()
	{
	
		$affiliate_list = Member::find_by_sql("select member.id, first_name, last_name from member join affiliate ON affiliate.member_id = member.id;");
		$affiliate_array = array();
		
		foreach($affiliate_list as $affiliate)
		{
			$affiliate_array[$affiliate->id] = $affiliate->first_name . " " .$affiliate->last_name;	
		}	
	
		$this->view->affiliate_array = $affiliate_array;
		$this->view->promo = Promo::find("all");
		
		$this->view->franchise = Franchise::find('all');

		$this->view->cid_list = Member::find_by_sql("select distinct(creative_id) from member where creative_id IS NOT NULL");


			
		if ($this->getRequest()->isPost())
		{
			$expiredtoken    = $this->_getParam('expiredtoken', null);
				
			if(isset($expiredtoken) && $expiredtoken)
			{
				//SEND 3 STRIKE MAIL TO SELECTED MEMBERS
				$strike3s    = $this->_getParam('3strikes', null);


				if (is_array($strike3s) && count($strike3s))
				{
					$member3strikes    = Member::find_all_by_id($strike3s);

					foreach ($member3strikes as $receiver)
					{
						$receiver->expiredTokenNotify();
					}

					$this->view->expires    = count($strike3s);
				}

			}
			else
			{

				//SEND 3 STRIKE MAIL TO SELECTED MEMBERS
				$strike3s    = $this->_getParam('3strikes', null);

				if (is_array($strike3s) && count($strike3s))
				{
					$member3strikes    = Member::find_all_by_id($strike3s);

					foreach ($member3strikes as $receiver)
					{
						$receiver->invalidTokenNotify('facebook');
					}

					$this->view->strikes    = count($strike3s);
				}
			}
		}

		
		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		
		$this->view->selected_status = 'all';

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		$conditions    = "status='active' ";
		
		$request = $this->getRequest();
		$params  = $request->getParams();





		$date_from 	= 	!empty($params['from']) ? $params['from'] : null;
		$date_to	=	!empty($params['to']) ? $params['to'] : null;
			
		if( !empty($params['month_from']) && !empty($params['day_from']) && !empty($params['year_from']) ) 
		{
			$date_from = 	$params['year_from'].'-'.$params['month_from'].'-'.$params['day_from'];
		}

		if( !empty($params['month_to']) && !empty($params['day_to']) && !empty($params['year_to']) ) 
		{
			$date_to = 	$params['year_to'].'-'.$params['month_to'].'-'.$params['day_to'];
		}

		$this->view->from_date = $date_from ;
		$this->view->to_date = $date_to ;

		
		$dropoffs = $request->getParam('dropoffs', null);

		$filter_param	=	($dropoffs == '1') ? 	'member.cancelled_date' : 'member.created_at';		
		if( !empty($date_from) && !empty($date_to) ) 
		{
			$conditions .= " AND ".$filter_param." BETWEEN DATE( '". $date_from ."' ) AND DATE( '". $date_to ."' )";
		}



		//if(!empty($status)) $conditions .= " AND status='".$status."' ";			

		

		if($select)
		{
			$selected = $cities->find($select);
			$conditions .= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}
			

		$searchFiled = $this->getRequest()->getParams();
		foreach($searchFiled as $k=>$val)
		$searchFiled[$k] = addslashes($val);
			
		if(!empty($searchFiled['member_id']) && trim($searchFiled['member_id'])!='')
		{
			$conditions .= " AND id='" .trim($searchFiled['member_id']). "'";
			$this->view->member_id = trim($searchFiled['member_id']);
		}

		if(!empty($searchFiled['first_name']) && trim($searchFiled['first_name'])!='')
		{
			$conditions .= " AND first_name LIKE '%" .trim($searchFiled['first_name']). "%'";
			$this->view->first_name = trim($searchFiled['first_name']);
		}

		if(!empty($searchFiled['last_name']) && trim($searchFiled['last_name'])!='')
		{
			$conditions .= " AND last_name LIKE '%" .trim($searchFiled['last_name']). "%'";
			$this->view->last_name = trim($searchFiled['last_name']);
		}

		if(!empty($searchFiled['email']) && trim($searchFiled['email'])!='' )
		{
			$conditions .= " AND email='" .trim($searchFiled['email']). "'";
			$this->view->email = trim($searchFiled['email']);
		}

		if(!empty($params['payment_status']))
		{  	
			$conditions .= " AND payment_status='".$params['payment_status']."' ";
			$this->view->payment_status = $params['payment_status'];
		}
		
		if(!empty($params['affiliate_id']))
		{  
			$conditions .= " AND referring_affiliate_id='".$params['affiliate_id']."' ";
			$this->view->affiliate_id = $params['affiliate_id'];
		}
		
		if(!empty($params['billing_cycle']))
		{  
			$conditions .= " AND billing_cycle='".$params['billing_cycle']."' ";
			$this->view->billing_cycle = $params['billing_cycle'];
		}
		
		if(!empty($params['membership_type']))
		{  
			$conditions .= " AND type_id='".$params['membership_type']."' ";
			$this->view->membership_type = $params['membership_type'];			
		}
		
		if(!empty($params['franchise_id']))
		{  
			$conditions .= " AND franchise_id='".$params['franchise_id']."' ";
			$this->view->franchise_id = $params['franchise_id'];
		}
		
		if(!empty($params['promo_id']))
		{  
			$conditions .= " AND promo_id='".$params['promo_id']."' ";
			$this->view->promo_id = $params['promo_id'];
		}

		if(!empty($params['creative_id']))
		{  
			$conditions .= " AND creative_id='".$params['creative_id']."' ";
			$this->view->creative_id = $params['creative_id'];
		}
		
		if(!empty($params['cc_provided_by']))
		{  
			$conditions .= "AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = member.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= '" . date('Y-m-d', strtotime($params['cc_provided_by'])) . "'
					LIMIT 1
				) IS NOT NULL
			";
		}
		
		if(!empty($params['cc_not_provided_by']))
		{  
			$conditions .= "AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = member.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= '" . date('Y-m-d', strtotime($params['cc_not_provided_by'])) . "'
					LIMIT 1
				) IS NULL
			";
		}

		if (!empty($params['payments_exist']))
		{
			$conditions .= "AND
				(
					SELECT id
					FROM payment p 
					WHERE member_id = mem.id 
					AND amount > 0
					LIMIT 1
				) IS NOT NULL
			";
		}

		$joins = array();

		if (!empty($params['pay_date_from']) && !empty($params['pay_date_to']))
		{
			$joins[] = "INNER JOIN payment p ON p.member_id = member.id AND p.amount > 0 AND (DATE(p.created_at) >= '" . $params['pay_date_from'] . "' AND DATE(p.created_at) <= '" . $params['pay_date_to'] . "')";
		}

		if (!empty($params['cancelled_date']))
		{
			$joins[] = "INNER JOIN member_type mt ON member.type_id = mt.id AND mt.is_customer = 1";
			$conditions = "
			# Member does not have payment_status or account type 'free'
				(payment_status != 'free' AND account_type != 'free')
				# Member had made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = member.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= '".$params['cancelled_date']."'
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
				# Member had payment status 'cancelled' on given date
				AND
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE member.id = msld.member_id 
					AND msld.date = '".$params['cancelled_date']."'
					AND msld.payment_status = 'cancelled'
					ORDER BY msld.date DESC
					LIMIT 1
				) IS NOT NULL 
				# Member did not have had payment status 'cancelled' on day before given date
				AND
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE member.id = msld.member_id 
					AND msld.date = DATE_ADD('".$params['cancelled_date']."', INTERVAL -1 DAY)
					AND msld.payment_status != 'cancelled'
					ORDER BY msld.date DESC
					LIMIT 1
				) IS NOT NULL 
 			";
		}
		
		$this->getRequest()->setParams(array('from' => $date_from,'to' => $date_to));
		$conditions = array("joins" => $joins, "conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);

		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
		$this->view->show_revenue_recur_monthly = !empty($params['show_revenue_recur_monthly']) && $params['show_revenue_recur_monthly'] == 1;

		$this->render("members");

	}

	public function threestrikesmembers()
	{
		$this->view->display_type = "INACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
	
		$this->view->selected_status = 'threestrikes';

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";


		if($select)
		{
			$selected           = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$conditions  = "status = 'active' and ( (access_token IS NOT NULL and facebook_fails >= 3) or (linkedin_fails >= 3 and linkedin_access_token IS NOT NULL) or ";
		$conditions .= "(twitter_fails >= 3 and twitter_access_token IS NOT NULL) ) AND status='active' ".$cityConditions;
		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		$this->render("members");

	}

	public function cityassignment()
	{

		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		$this->view->promo = Promo::find("all");
		$this->view->selected_status = 'cityassignment';
		$this->view->cid_list = Member::find_by_sql("select distinct(creative_id) from member where creative_id IS NOT NULL");

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());


		//$conditions  = "publish_city_id IS NULL or publish_city_id = '' ";
		$conditions  = "city_id IS NULL or city_id = '' ";


		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$this->view->franchise = Franchise::find('all');

		$this->render("members");

	}
	

	public function free()
	{

		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		$this->view->promo = Promo::find("all");
		$this->view->selected_status = 'free';
		$this->view->cid_list = Member::find_by_sql("select distinct(creative_id) from member where creative_id IS NOT NULL");

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";


		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$conditions  = "account_type='free' ".$cityConditions;


		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$this->view->franchise = Franchise::find('all');

		$this->render("members");

	}


	public function paid()
	{

		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		$this->view->promo = Promo::find("all");
		$this->view->selected_status = 'paid';
		$this->view->cid_list = Member::find_by_sql("select distinct(creative_id) from member where creative_id IS NOT NULL");

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";


		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$conditions  = "account_type='paid' and payment_status='paid' and status='active' ".$cityConditions;

		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		$this->render("members");

	}

	public function trial()
	{
		$conditions = array("conditions" => '', "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('TrialMember', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
	}

	public function failedpayment()
	{

		$this->view->display_type = "INACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();

		$this->view->selected_status = 'failedpayment';

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";


		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		//$conditions  = "( (account_type='paid' and (next_payment_date < NOW())  ) OR payment_status='payment_failed'  ) and payment_status != 'cancelled' and payment_status != 'signup' and payment_status !='signup_expired' ".$cityConditions;
		$conditions  = " payment_status='payment_failed' ".$cityConditions;
		
		$request = $this->getRequest();
		$date_from = $request->getParam('from',null);
		$date_to = $request->getParam('to',null);
		
		if($date_from && $date_to)
		{
			$conditions .= " AND next_payment_date BETWEEN DATE( '". $date_from ."' ) AND DATE( '". $date_to ."' ) ";
		}

		$conditions = array("conditions" => $conditions, "order" => "next_payment_date DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		$this->render("members");

	}
		



	public function inactivemembers()
	{
		$this->view->display_type = "INACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		
		$conditions = "status = 'active' and (facebook_publish_flag=0 and facebook_fails < 3 and access_token IS NOT NULL) ";

		// and (linkedin_publish_flag = 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) and (twitter_publish_flag = 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL)";
		//echo $conditions . "<BR>";
		$members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		foreach($members as $member)
		{
			$ids[] = $member->id;
		}

		$conditions  = "status = 'active' and ( ((twitter_followers > 0 and twitter_fails < 3  and twitter_access_token IS NOT NULL and twitter_publish_flag = 0) or ";
		$conditions .= " (twitter_access_token IS NULL)) and id in (".implode(",", $ids).") ) AND status='active' or payment_status = 'cancelled' ";
		//echo $conditions . "<BR>";
		$members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$ids = null;
		foreach($members as $member)
		{
			$ids[] = $member->id;
		}

		$conditions = "status = 'active' and ((linkedin_friends_count > 0 and linkedin_fails < 3  and linkedin_access_token IS NOT NULL and linkedin_publish_flag = 0) or (linkedin_access_token IS NULL)) and id in (".implode(",", $ids).")";
		//echo $conditions . "<BR>";

		$this->view->selected_status = 'inactive';

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}
		$conditions = $conditions . $cityConditions;

		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;


		$this->render("members");

	}


	public function frequency()
	{
		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();

		$this->view->frequency_id = $frequency_id = $this->getRequest()->getParam('frequency_id');

		if(empty($frequency_id))
		{
			$frequency = "IS NULL";
			$this->view->frequency_id = "MISSING FREQENCY";
				
		}
		else
		{
			$frequency = "=" . $frequency_id;
		}

		$conditions = "status = 'active' and frequency_id ".$frequency." and ";
		$conditions .= "( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) AND status='active' ";

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;

			$conditions .= " ".$cityConditions;
		}

		//echo $conditions . "<BR>";

		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		$this->render("members");

	}

	public function hopper()
	{
		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();

		$this->view->hopper_id = $hopper_id = $this->getRequest()->getParam('hopper_id');

		if(empty($hopper_id))
		{
			$hopper = "IS NULL";
			$this->view->hopper_id = "MISSING HOPPER";
				
		}
		else
		{
			$hopper = "=" . $hopper_id;
		}

		$conditions = "status = 'active' and current_hoper ".$hopper." and ";
		$conditions .= "( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) AND status='active' ";

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;

			$conditions .= " ".$cityConditions;
		}

		//echo $conditions . "<BR>";

		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		$this->render("members");


	}

	public function inventory()
	{
		$this->view->display_type = "ACTIVE";
		$this->view->affiliate_array = $this->set_affiliates();
		
		$this->view->inventory_id = $this->getRequest()->getParam('id');


		$conditions = "status = 'active' and (inventory_id = ".$this->getRequest()->getParam('id').") and ";
		$conditions .= "( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) AND status='active' ";

		$cities        = new City();
		$select        = intval($this->getSelectedAdminCity());
		$cityConditions    =    "";

		if($select)
		{
			$selected            = $cities->find($select);
			$cityConditions	= " and (publish_city_id=".intval($selected->id) . " or city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;

			$conditions .= " ".$cityConditions;
		}


		//$this->view->members = Member::find('all', array('order' => 'id DESC', 'conditions' => $conditions));

		$conditions = array("conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		//$paginator->setCurrentPageNumber($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		$this->render("members");

	}


	public function paymentsbyday()
	{

		/**************
		$query = "select id, first_name, last_name from member";
		$members = Member::find_by_sql($query);
		
		foreach($members as $member)
		{

			if(isset($member->first_name) && !empty($member->first_name))
			{
				$final[$member->id] = $member->first_name;
			}
			else
			{
				$final[$member->id] = $member->id;
			}
		} 
			
		$this->view->members = $final;
		**************/


		//$yesterday = date('Y-m-d', strtotime('-1 day'));
		//$next_month = date('Y-m-d', strtotime('+2 months'));
		
		$today = date('Y-m-d');
		$end_month = date('Y-m-t');


		

		$query  = "select DATE(member.next_payment_date) as payment_day, count(distinct(member.id)) cnt from member ";
		$query .= "where member.payment_status='paid' and account_type='paid' and status='active' ";
		$query .= "AND member.next_payment_date >= '".$today."' AND member.next_payment_date <= '".$end_month."' ";
		$query .= "group by DATE(member.next_payment_date)";
		$query .= "order by member.next_payment_date asc ";

		$payments = Payment::find_by_sql($query);


		$final_payments = array();

		foreach($payments as $p) 
		{
			$final_payments[$p->payment_day] = $p->cnt;
		}


		
		$query  = "select distinct(member.id), DATE(member.next_payment_date) as payment_day, SUM(member.price) as total from member ";
		$query .= "where member.payment_status='paid' and account_type='paid' and status='active' ";
		$query .= "AND member.next_payment_date >= '".$today."' AND member.next_payment_date <= '".$end_month."' ";
		$query .= "group by DATE(member.next_payment_date) ";
		

		
		$daily_revenues = Payment::find_by_sql($query);
		$final_rev = array();


		foreach($daily_revenues as $rev)
		{
			//$value = (isset($final_rev[$rev->payment_day])) ? $final_rev[$rev->payment_day] : 0;
			$final_rev[$rev->payment_day] = $rev->total;
		}
			


		ksort($final_payments);
		ksort($final_rev);
		
		$this->view->payments 	=	$final_payments;
		$this->view->revenue 	= 	$final_rev;


	}


	
	public function revenue()
	{
		$query = "
			SELECT 
				COUNT(*) AS count_purchases, 
				SUM(payment.taxes) AS running_tax, 
				SUM(payment.amount) AS running_total, 
				DATE_FORMAT(created_at, '%M %Y') AS month_str, 
				DATE_FORMAT(created_at, '%Y-%m') AS month
			FROM payment 
			WHERE created_at > DATE(NOW()) - INTERVAL (DAY(NOW()) - 1) DAY - INTERVAL 48 MONTH - INTERVAL 1 SECOND 
			GROUP BY month 
			ORDER BY month DESC 
			LIMIT 90 
		";
		
		$this->view->monthly_revenues = Payment::find_by_sql($query);
	}



	public function payments()
	{

		$display    = $this->view->display    = $this->_getParam('display', 'all');
		$sort       = $this->view->sort       = $this->_getParam('sort', 'count');
		$by         = $this->_getParam('by', 'desc');

		$status       = $this->view->status       = $this->_getParam('status', 'count');




		$sortField    = 'count_purchases';
		$sortOrder    = 'DESC';

		if (strtolower($sort) == 'amount')
		{
			$sortField    = 't_amount';
		}

		if (strtolower($by) == 'asc')
		{
			$sortOrder    = 'ASC';
		}

		$this->view->by    = $sortOrder == 'DESC' ? 'asc' : 'desc';

		//print_r($_POST);
		$request        	= $this->getRequest();
			
		$start_date    	= $request->getParam('from', null);
		$end_date    		= $request->getParam('to', null);

		// Validate bad date format mm-dd-YYYY
		$date_parts 	=	explode("-",$start_date);
		if(count($date_parts) == 3 && strlen($date_parts[2])==4) 
		{
			$start_date 	= $date_parts[2].'-'.$date_parts[0].'-'.$date_parts[1];
			$todate_parts	= explode("-",$end_date);
			$end_date 	= $todate_parts[2].'-'.$todate_parts[0].'-'.$todate_parts[1];
		}
			
		$conditions        	= '1';
			
		if($start_date && $end_date)
		{
			$this->view->from_date = $start_date;
			$this->view->to_date = $end_date;

			$conditions = "payment.created_at >=  '". $start_date ." 00:00:00' AND payment.created_at <=  '". $end_date ." 23:59:59' AND paypal_confirmation_number!='FREE_REFERRAL_CREDIT'";
		}
		else
		{
			$this->view->from_date = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
			$this->view->to_date = date('Y-m-d',strtotime('-1 second',strtotime('+1 month',strtotime(date('m').'/01/'.date('Y').' 00:00:00'))));
			$conditions 	= "payment.created_at >= '". $this->view->from_date ." 00:00:00' AND payment.created_at <= '". $this->view->to_date ." 23:59:59' AND paypal_confirmation_number!='FREE_REFERRAL_CREDIT'";
		}
		
		$state = $request->getParam('state', null);
		$this->view->payment_state = $state;
		
		if (!empty($state)) {
			$country_state = explode('|', $state);
			$country = !empty($country_state[0]) ? $country_state[0] : null;
			$state = !empty($country_state[1]) ? $country_state[1] : null;
			if (!empty($country)) {
				$conditions .= " AND country = '".$country."' ";
			}
			if (!empty($state)) {
				$conditions .= " AND state = '".$state."' ";
			}
		}
		
		$member_signup_from = $request->getParam('member_signup_from');
		if (!empty($member_signup_from)) {
			$conditions .= " AND DATE(member.created_at) >= '" . $member_signup_from . "' ";
		}
		$member_signup_to = $request->getParam('member_signup_to');
		if (!empty($member_signup_to)) {
			$conditions .= " AND DATE(member.created_at) <= '" . $member_signup_to . "' ";
		}
		$affiliate_id = $request->getParam('affiliate_id');
		if (intval($affiliate_id) > 0) {
			$conditions .= ' AND member.referring_affiliate_id = '.intval($affiliate_id);
		}
		$billing_cycle = $request->getParam('billing_cycle');
		if (!empty($billing_cycle)) {
			$conditions .= " AND member.billing_cycle = '" . $billing_cycle . "'";
		}

		if($status == "signup")
		{
			$conditions .= " AND amount = 0 ";
		}
		
		$cond = array();
		$cond['conditions'] = $conditions;
		$cond['joins'] = "LEFT JOIN member ON payment.member_id = member.id";
		$cond['order'] = "payment.id DESC";

		// Aggregation query
		if ($display == 'all')
		{

			// sub-totals
			$temp_cond = $cond;
			$temp_cond['select'] = "payment.*, COUNT(*) as count_purchases, SUM(payment.taxes) as running_tax, SUM(payment.amount) as running_total";
			$aggregation = Payment::find('one', $temp_cond);


			$this->view->running_tax 	= floatval($aggregation->running_tax);
			$this->view->running_total 	= floatval($aggregation->running_total);
			$this->view->count_purchases 	= floatval($aggregation->count_purchases);


			// shaun alen
			$temp_cond = $cond;
			$temp_cond['conditions'] = $conditions . ' AND member_id IN (40,35)';
			$temp_cond['select'] = "payment.*, SUM(payment.taxes) as taxes, SUM(payment.amount) as amount";
			$aggregation = Payment::find('one', $temp_cond);

			$this->view->alen_tax = floatval($aggregation->taxes);
			$this->view->alen_total = floatval($aggregation->amount);
		}
		else
		{
			$select    = "payment.*, COUNT( * ) AS `count_purchases`, SUM(IFNULL(`amount`, 0)) AS `t_amount`, SUM(IFNULL(`taxes`, 0)) AS `t_taxes` ";
			$group    = "`member_id`";
			$order    = "`". $sortField ."` ". $sortOrder;
		}

		$csvquery = new Zend_Session_Namespace('csvquery');
		$csvquery->data = new stdClass;
		$csvquery->data->condition = serialize($cond);
		$csvquery->data->from_date = $this->view->from_date;
		$csvquery->data->to_date = $this->view->to_date;
		$csvquery->data->display = $display;


		
		$paginator = new Zend_Paginator(new ARPaginator('Payment', $cond));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->payments = $paginator;

		//ASSIGN TO THE VIEW
		$payments = $this->view->payments;

		//TOTAL GROSS REVENUE...
		$query = "select sum(amount) as total_net, sum(taxes) as total_tax, (sum(amount) + sum(taxes)) as total_gross from payment";
		$result = Payment::find_by_sql($query);
		$this->view->total_net = (float)$result[0]->total_net;
		$this->view->total_tax = (float)$result[0]->total_tax;
		$this->view->total_gross = (float)$result[0]->total_gross;

		//TOTAL GROSS CLIENTFINDER REVENUE
		$query = "select sum(amount) as total_gross from payment where payment_type = 'clientfinder'";
		$result = Payment::find_by_sql($query);
		$this->view->gross_clientfinder_total = (float)$result[0]->total_gross;

		//TOTAL REVENUE FOR THE PERIOD IN QUESTION...
		$temp_cond = $cond;
		$temp_cond['select'] = "payment.*, sum(amount) as total_net, sum(taxes) as total_tax, (sum(amount) + sum(taxes)) as total_gross";
		$result = Payment::all($temp_cond);
		$this->view->monthly_net = (float)$result[0]->total_net;
		$this->view->monthly_tax = (float)$result[0]->total_tax;
		$this->view->monthly_gross = (float)$result[0]->total_gross;
		
		
		$query = "SELECT country, state FROM payment GROUP BY country, state";
		$result = Payment::find_by_sql($query);
		$this->view->payment_states = !empty($result) ? $result : array();
		
	}
	
	
	public function exporttocsv()
	{
		$query = "select id, first_name, last_name from member";
		$result = Member::find_by_sql($query);
		
		//echo "<pre>";
		//print_r($result);

		$final = array();
		foreach($result as $member)
		{
			if(isset($member->first_name) && !empty($member->first_name))
			{
				$final[$member->id] = $member->first_name;
			}
			else
			{
				$final[$member->id] = $member->id;
			}
		}
		$members = $final;		


		
		/************
		$members = Member::all();
		
		$members = Member::all();
		$final = array();
		foreach($members as $member)
		{
			if(isset($member->first_name) && !empty($member->first_name))
			{
				$final[$member->id] = $member->first_name;
			}
			else
			{
				$final[$member->id] = $member->id;
			}
		}
		$members = $final;
		************/



		$csvquery = new Zend_Session_Namespace('csvquery');
		$condition = unserialize($csvquery->data->condition);
		$from_date = $csvquery->data->from_date;
		$to_date = $csvquery->data->to_date;
		$display = $csvquery->data->display;
		
		$payments = Payment::find('all', $condition);
	
		$data = array();
		$data[] = array("PID","MID","Member","Type","Transaction ID","Date/Time","City","State","Aff", "Tax", "Amount");
		foreach($payments as $payment){
			$name = $payment->member->first_name . " " . $payment->member->last_name;
			if($payment->payment_gateway && $payment->payment_gateway->code == "PAYPAL") 
				$confNo = $payment->paypal_confirmation_number;
			else 
				$confNo = $payment->stripe_payment_id;
			
			$created_at = $payment->created_at->format('Y-m-d H:i:s');
	
			if(isset($payment->member->referring_affiliate_id) && !empty($payment->member->referring_affiliate_id)){
				$aff = @$members[$payment->member->referring_affiliate_id];
			}
			else{
				$aff = '';
			}
						
			$tax = money_format('%!i', ($display == 'bymember' ? $payment->t_taxes : $payment->taxes));
			
			$paymentAmt = money_format('%!i', ($display == 'bymember' ? $payment->t_amount : $payment->amount));
			
			$data[] = array($payment->id,$payment->member_id,$name,$payment->payment_type,$confNo,$created_at,$payment->city,$payment->state,$aff,$tax,$paymentAmt);
		}
	
		$sheetName = "Payment-From-".$from_date."-To-".$to_date;


		$this->_helper->getHelper('Csv')->Csv($data,$sheetName);
	}

	function dateDiff($start, $end)
	{

		$start_ts = strtotime($start);

		$end_ts = strtotime($end);

		$diff = $end_ts - $start_ts;

		return round($diff / 86400);
	}



	public function mpayments()
	{

		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}

		$this->view->member = $member = Member::find($this->_request->getParam('id'));

		$display    = $this->view->display    = $this->_getParam('display', 'all');
		$sort       = $this->view->sort        = $this->_getParam('sort', 'count');
		$by         = $this->_getParam('by', 'desc');


		$sortField    = 'count_purchases';
		$sortOrder    = 'DESC';

		if (strtolower($sort) == 'amount')
		{
			$sortField    = 't_amount';
		}

		if (strtolower($by) == 'asc')
		{
			$sortOrder    = 'ASC';
		}

		$this->view->by    = $sortOrder == 'DESC' ? 'asc' : 'desc';

		if ($display == 'all')
		{
			$this->view->payments = Payment::find('all', array('order' => 'id DESC', 'conditions' => 'member_id='.$member->id));
		}
		else
		{
			$select    = "*, COUNT( * ) AS `count_purchases`, SUM(IFNULL(`amount`, 0)) AS `t_amount`, SUM(IFNULL(`taxes`, 0)) AS `t_taxes` ";
			$group    = "`member_id`";
			$order    = "`". $sortField ."` ". $sortOrder;

			$this->view->payments = Payment::find('all', array('select' => $select, 'group' => $group, 'order' => $order, 'conditions' => 'member_id='.$member->id));
		}


		$this->view->payment_count = Payment::count('all', array('conditions' => "payment_type = 'clientfinder'", 'conditions' => 'member_id='.$member->id));

	}



	public function payment()
	{
		$payment_id    = $this->_getParam('id', 0);
		if ($payment_id)
		{
			$this->view->payment    = Payment::find_by_id($payment_id);
		}
	}

	public function payable()
	{

		$this->view->affiliate_dropdown = Member::find_by_sql("SELECT * FROM member JOIN affiliate ON affiliate.member_id = member.id ORDER BY first_name");
		
		
		$display    = $this->view->display    = $this->_getParam('display', 'all');
		$sort       = $this->view->sort        = $this->_getParam('sort', 'count');
		$by         = $this->_getParam('by', 'desc');


		$sortField    = 'amount';
		$sortOrder    = 'DESC';

		if (strtolower($sort) == 'amount')
		{
			$sortField    = 'amount';
		}

		if (strtolower($by) == 'asc')
		{
			$sortOrder    = 'ASC';
		}

		$this->view->by    = $sortOrder == 'DESC' ? 'asc' : 'desc';				
		$this->view->num_days = $this->dateDiff("2012-06-19", date('Y')."-".date('m')."-".date('d'));

		$paginator = new Zend_Paginator(new ARPaginator('Payable', array('conditions' => '', 'order' => 'id ' . $sortOrder)));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

		$this->view->paginator = $paginator;
		
		$this->view->payables = $paginator;
		
	}

	public function cities()
	{

		switch ($this->getRequest()->getParam('sort')) {
			case 'aasc':
				$sort = " ORDER BY current_agents ASC";
				break;
			case 'adesc':
				$sort = " ORDER BY current_agents DESC";
				break;
			case 'iaasc':
				$sort = " ORDER BY inactive_agents ASC";
				break;
			case 'iadesc':
				$sort = " ORDER BY inactive_agents DESC";
				break;
			case 'ndesc':
				$sort = " ORDER BY `c`.`name` DESC";
				break;
			case 'nasc':
				$sort = " ORDER BY `c`.`name` ASC";
				break;
			default:
				$sort = " ORDER BY current_agents DESC";
				break;
		}

		$sql = "SELECT
					c.`id`,
					c.`name`,
					c.`state`,
					c.`country`,
					c.`number_of_hoppers`,
					current_agents,
					all_agents,
					(all_agents - current_agents) as inactive_agents,
					reach
				FROM (
					SELECT
						c.`id`,
						c.`name`,
						c.`state`,
						c.`country`,
						c.`number_of_hoppers`,
						COUNT( m_current.id ) AS current_agents,
						(
							SELECT COUNT(m_all.id)
							FROM member m_all
							WHERE m_all.city_id = c.id
						) AS all_agents,
						(
							COALESCE(SUM( m_current.facebook_friend_count ),0) +
							COALESCE(SUM( m_current.twitter_followers ),0) +
							COALESCE(SUM( m_current.linkedin_friends_count ),0)
						) AS reach
					FROM `city` AS c
					LEFT JOIN member AS m_current ON
						m_current.city_id = c.id AND
						m_current.status =  'active'
						AND (
							(
								m_current.facebook_publish_flag >0
								AND m_current.facebook_fails <3
							)
							OR (
								m_current.linkedin_publish_flag >0
								AND m_current.linkedin_fails <3
								AND m_current.linkedin_access_token IS NOT NULL
							)
							OR (
								m_current.twitter_publish_flag >0
								AND m_current.twitter_fails <3
								AND m_current.twitter_access_token IS NOT NULL
							)
						)
						AND m_current.payment_status !=  'cancelled'
						AND m_current.payment_status !=  'payment_failed'
						AND (
							m_current.next_payment_date IS NULL
							OR m_current.next_payment_date >= DATE(NOW())
						)
					GROUP BY c.id
				";

		$sql .= $sort;
		$sql .= ') as c';

		$this->view->cities = Member::find_by_sql($sql);

		$this->view->unassigned = Member::find_by_sql("SELECT count(*) as total FROM member WHERE city_id IS NULL");
		
		$message = new Zend_Session_Namespace('messages');

		if(!is_object($message))
			$message =	new stdClass;
		if(!is_object($message->data))
			$message->data =	new stdClass;
			
		if(isset($message->data) && isset($message->data->message)) $this->view->message = $message->data->message;
		$message->data->message = '';

	}


	public function groups()
	{
		//$this->view->groups = Group::find('all', array('order' => 'name ASC'));

		// maybe from /admin/city/delete/

		$message = new Zend_Session_Namespace('messages');
		if(false == isset($message->data)) $message->data = new StdClass;
		if(isset($message->data->message)) $this->view->message = $message->data->message;
		$message->data->message = '';
		
		$paginator = new Zend_Paginator(new ARPaginator('Group', array('conditions' => array(), 'order' => 'name ASC')));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
	}

	public function lifetime()
	{
		$sort = $this->view->sort = $this->_getParam('sort', 'id');
		$by = $this->_getParam('by', 'desc');

		switch(strtolower($sort)) {
			case 'total_payment':
				$sortField = 'amount';
				break;
			case 'days_as_member':
				$sortField = 'as_member';
				break;
			case 'days_as_customer':
				$sortField = 'as_customer';
				break;
			default:
				$sortField = 'amount';
				break;
		}

		if (strtolower($by) == 'asc') {
			$sortOrder = 'ASC';
		} else {
			$sortOrder = 'DESC';
		}

		$this->view->by = $sortOrder == 'DESC' ? 'asc' : 'desc';

		$message = new Zend_Session_Namespace('messages');
		if(isset($message->data) && isset($message->data->message)) $this->view->message = $message->data->message;
		$message->data->message = '';
		$conditions = '';

		$avg_days = Member::find_by_sql("
			SELECT AVG( DATEDIFF( NOW( ), member.created_at ) ) AS avg_days FROM  `member`
		");
		$avg_amount = Payment::find_by_sql("
			SELECT  AVG( payment.amount ) AS avg_amount FROM `payment` WHERE payment.amount IS NOT NULL AND payment.amount <> 0.00
		");
		/*
		 * SELECT AVG( DATEDIFF( NOW( ), member.created_at ) ) AS avg_days, (SELECT  AVG( payment.amount ) AS avg_amount
FROM `payment` WHERE payment.amount IS NOT NULL AND payment.amount <> 0.00 AND payment.member_id = member.id) as avg_amount
FROM  `member`

		 * */

		$cities        = new City();
		$select = intval($this->getSelectedAdminCity());
		if($select)
		{
			$selected = $cities->find($select);
			$conditions .= " AND (member.publish_city_id=".intval($selected->id) . " OR member.city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$query = "
			SELECT
				member.id,
				member.first_name,
				member.last_name,
				member.email,
				COALESCE(SUM(payment.amount), 0.00) as amount,
				DATEDIFF(NOW(),member.created_at) as as_member,
				COALESCE(DATEDIFF(NOW(),MIN(payment.created_at)), 0) as as_customer
			FROM
				`member`
				LEFT JOIN `payment` ON member.id = payment.member_id
			WHERE 1 $conditions
			GROUP BY member.id
			ORDER BY $sortField $sortOrder
		";

		$lifetime = Member::find_by_sql($query);

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($lifetime));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

		$this->view->paginator = $paginator;
		$this->view->avg_days = round($avg_days[0]->avg_days);
		$this->view->avg_amount = round($avg_amount[0]->avg_amount, 2);
	}

	public function referrals()
	{
		$sort = $this->view->sort = $this->_getParam('sort', 'referrals');

		switch(strtolower($sort)) {
			case 'id':
				$sortField = 'member.id';
				break;
			case 'blast':
				$sortField = 'member.referral_blast_count';
				break;
			case 'email':
				$sortField = 'member.referral_email_count';
				break;
			case 'referral_credits':
				$sortField = 'total_referral_credits';
				break;
			case 'referrals':
			default:
				$sortField = 'total_referrals';
				break;
		}

		$by = strtoupper($this->_getParam('by', 'DESC'));

		$this->view->by = ($by == 'ASC' || $by == 'DESC') ? $by : 'DESC';

		$message = new Zend_Session_Namespace('messages');
		if(isset($message->data) && isset($message->data->message)) $this->view->message = $message->data->message;
		if (!isset($message->data)) $message->data = new stdClass;
		$message->data->message = '';
		$conditions = '';

		$cities        = new City();
		$select = intval($this->getSelectedAdminCity());
		if($select)
		{
			$selected = $cities->find($select);
			$conditions .= " AND (member.publish_city_id=".intval($selected->id) . " OR member.city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$query = "
			SELECT
				member.id,
				member.first_name,
				member.last_name,
				member.referral_blast_count,
				member.referral_email_count,
				(member.referral_blast_count + member.referral_email_count) as total_referrals,
				(SELECT count(*) FROM referral_credit WHERE member_id = member.id) as total_referral_credits
			FROM
				`member`
			WHERE 1 $conditions AND (member.referral_blast_count + member.referral_email_count) > 0
			ORDER BY $sortField $by
		";

		$lifetime = Member::find_by_sql($query);

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($lifetime));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

		$this->view->paginator = $paginator;
	}


	public function groupadd()
	{
		$id		=   $this->_request->getParam('id');
		$cities 	=   $this->_request->getParam('city');
		
	    	if($this->_request->isPost())
	    	{
	    		if(!empty($id) && $id != 0)
	    			$group = Group::find($id);
	    		else
	    			$group = new Group();

	    		$group->name 	= $this->_request->getParam('name');
	    		$group->save();


			$query = "delete from group_city where group_id=".$group->id;
	 		GroupCity::connection()->query($query);

	
			foreach($cities as $city_id)
			{
				$group_city = new GroupCity;
				$group_city->group_id = $group->id;
				$group_city->city_id  = $city_id;	
				$group_city->save();
			}

	    		$this->_redirect('/admin/groups');

	    	}


		$this->view->cities = City::find('all', array('order' => 'name ASC'));


		$this->view->group = null;
	    	if(!empty($id))
	    	{
	    		$this->view->group = Group::find($id);
	    	}

		$final_group_city_list = array();
		$group_city_list = $this->view->group->cities;	
		
		if($group_city_list)
		{	
			foreach($group_city_list as $group_city)
			{
				$final_group_city_list[] = $group_city->city_id;		
			}	
		}									
		
		$this->view->group_cities = $final_group_city_list;
	
	}

	public function create_states()
	{
		/***************************************
		$cities = City::find('all', array('order' => 'name ASC'));
		
		foreach($cities as $city)
		{
			if(!empty($city->state)) $states[$city->state] = $city->state;
		}
		
		foreach($states as $state)
		{
		
			$group = new Group;
			$group->name = $state;
			$group->save();
			
			
			foreach($cities as $city)
			{			
				if($city->state == $state)
				{
					echo "Add ".$city->name." to ".$state."<BR>";
					
					$group_city = new GroupCity;
					
					$group_city->city_id  = $city->id;
					$group_city->group_id = $group->id;
					
					$group_city->save();
					
				}
			
			}
		}
		
		****************************/
		exit();
	}


	public function groupdelete()
	{
		$this->renderNothing();
	
		$group_id	= (int)$this->getRequest()->getParam('id');
	
		if ( $group_id > 0 ) 
		{
			$message = new Zend_Session_Namespace('messages');
			if (!isset($message->data)) $message->data = new stdClass;
			try 
			{
				$group = Group::find($group_id);
				$group->delete();
				
				$message->data->message = 'The Group "' . $group->name . '" is successfully deleted.';
				
			} catch ( Exception $e ) {
				$message->data->message = $e->getMessage();
			}
	
			$this->_redirect('/admin/groups/');
		}
	
		$this->_redirect($this->getRequest()->getServer('HTTP_REFERER', '/'));
	}



	public function reach_by_city()
	{
		$city_id    = is_numeric($this->getSelectedAdminCity()) ? intval($this->getSelectedAdminCity()) : 0;
		$cities        = Member::get_reach_by_city($city_id);
		$this->view->city_reach    = $cities;
		#$this->render("city_reach");
	}



	public function cancelaccount()
	{
		$request = $this->getRequest();
		$member_id = $request->getParam('id', null);


		//echo "<h1>MEMBER ID:".$member_id."</h1>";

		if(!empty($member_id))
		{
			$member = Member::find($member_id);
			$member->payment_status = "cancelled";
				
			$member->facebook_publish_flag = 0;
			$member->twitter_publish_flag = 0;
			$member->linkedin_publish_flag = 0;
				
			$member->cancelled_by_id = $_SESSION['member']->id;
			$member->cancelled_date = date("Y-m-d H:i:s");
				
			$member->save();
				
				
				
			$cancel_account = CancelAccount::find_by_member_id($member_id);
				
			if(!$cancel_account)
			{
				$cancel_account = new CancelAccount();
				$cancel_account->member_id = $member_id;
			}

				
			$cancel_account->status = "complete";
			$cancel_account->admin_id = $_SESSION['member']->id;
				
			$cancel_account->save();
				
				
		}

		$this->_redirect("/admin/member/".$member_id);

	}

	public function cancelrequests()
	{
		//$this->view->cancel_requests = CancelAccount::all();

		$conditions = array("conditions" => "1=1", "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('CancelAccount', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
	
		$this->view->paginator = $paginator;	
	}

	public function affiliate()
	{
		$this->view->affiliate_dropdown = Member::find_by_sql("SELECT * FROM member JOIN affiliate ON affiliate.member_id = member.id ORDER BY first_name");
		$this->view->referring_member_dropdown = Member::find_by_sql("SELECT referring_member.* FROM member JOIN member AS referring_member ON member.referring_member_id = referring_member.id GROUP BY referring_member.id ORDER BY first_name ");

		$request = $this->getRequest();
		
		$affiliate_session = new Zend_Session_Namespace('affiliate_id');

		$affiliate_id = $request->getParam('affiliate_id');
		$referring_member_id = $request->getParam('referring_member_id');

		$start_date = $request->getParam('from_date', date('Y-m-01', strtotime('now')) );
		$end_date   = $request->getParam('to_date', date('Y-m-d', strtotime('now')) );

		$affiliate_session->data = new stdClass;

		$affiliate_session->data->affiliate_id        = $affiliate_id;
		$affiliate_session->data->referring_member_id = $referring_member_id;
		
		if ($request->getParam('from_date')) {
			$affiliate_session->data->from_date = $start_date;
		} elseif (isset($affiliate_session->data->from_date)) {
			$start_date = $affiliate_session->data->from_date;
		}
		
		if ($request->getParam('to_date')) {
			$affiliate_session->data->to_date = $end_date;
		} elseif (isset($affiliate_session->data->to_date)) {
			$end_date = $affiliate_session->data->to_date;
		}
		
		$this->view->selected_affiliate_id        = $affiliate_session->data->affiliate_id;
		$this->view->selected_referring_member_id = $affiliate_session->data->referring_member_id;

		$this->view->from_date = $start_date;
		$this->view->to_date = $end_date;

		$conditions = array();
			
		$conditions[] = "member.created_at >=  '". $start_date ." 00:00:00'";
		$conditions[] = "member.created_at <=  '". $end_date ." 23:59:59'";
			
		$conditions[] = "(referring_affiliate_id IS NOT NULL OR referring_member_id IS NOT NULL)";
		
		$conversion = array();
		if($affiliate_session->data->affiliate_id != '' && $affiliate_session->data->affiliate_id !='*')
		{
			$conditions[] = "referring_affiliate_id =". $affiliate_session->data->affiliate_id; 	
			$conversionCounts = Member::find_by_sql("SELECT COUNT(*) as total, payment_status FROM member WHERE referring_affiliate_id = {$affiliate_session->data->affiliate_id} GROUP BY payment_status");
			foreach ($conversionCounts as $c)
			{
				$conversion[$c->payment_status] = $c->total;
			}
		} 
		elseif($affiliate_session->data->referring_member_id != '' && $affiliate_session->data->referring_member_id !='*')
		{
			$conditions[] = "referring_member_id =". $affiliate_session->data->referring_member_id; 	
			$conversionCounts = Member::find_by_sql("SELECT COUNT(*) as total, payment_status FROM member WHERE referring_member_id = {$affiliate_session->data->referring_member_id} GROUP BY payment_status");
			foreach ($conversionCounts as $c)
			{
				$conversion[$c->payment_status] = $c->total;
			}
		}
		else
		{
			$conversionCounts = Member::find_by_sql("SELECT COUNT(*) as total, payment_status FROM member WHERE referring_affiliate_id IS NOT NULL OR referring_member_id IS NOT NULL GROUP BY payment_status");
			foreach ($conversionCounts as $c)
			{
				$conversion[$c->payment_status] = $c->total;
			}
		}
		
		$cond = array("conditions" => implode(' AND ', $conditions), "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $cond));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

		$this->view->conversionRate = $conversion;
		$this->view->paginator = $paginator;
	}

	public function staffsalesleaders()
	{
		$request = $this->getRequest();
		$params = $request->getParams();
		$conditions = '';

		$cities        = new City();
		$select = intval($this->getSelectedAdminCity());
		if($select)
		{
			$selected = $cities->find($select);
			$conditions .= " AND (m.publish_city_id=".intval($selected->id) . " OR m.city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}
		
		$date_from = !empty($params['date_from']) ? date('Y-m-d', strtotime($params['date_from'])) : date('Y-m-01');
		$this->view->date_from = $date_from;
		$date_to = !empty($params['date_to']) ? date('Y-m-d', strtotime($params['date_to'])) : date('Y-m-t');
		$this->view->date_to = $date_to;

		if(!empty($params['sales_person_id'])) {
			$conditions .= " AND m.id = ".(int)$params['sales_person_id'];
			$this->view->sales_person_id = (int)$params['sales_person_id'];
		}
		
		$sql = "
			SELECT 
				m.id, 
				m.first_name,
				m.last_name, 
				a.id AS affiliate_id,
				(
					SELECT 
						COUNT(mem.id) AS total
					FROM member mem
					INNER JOIN payment p 
						ON p.member_id = mem.id 
						AND p.amount > 0
						AND (DATE(p.created_at) >= '" . $date_from . "' AND DATE(p.created_at) <= '" . $date_to . "')
					WHERE mem.referring_affiliate_id = a.id 
					LIMIT 1
				) AS count,
				(
					SELECT 
						COUNT(mem.id) AS total
					FROM member mem
					WHERE mem.referring_affiliate_id = a.id  
						AND mem.payment_status = 'signup' 
						AND mem.status = 'active'
					LIMIT 1
				) AS trial_current,
				(
					SELECT 
						COUNT(mem.id) AS total
					FROM member mem
					WHERE mem.referring_affiliate_id = a.id  
						AND mem.payment_status = 'paid' 
						AND mem.status = 'active'
					LIMIT 1
				) AS paid_current,
				(
					SELECT 
						COUNT(mem.id) AS total
					FROM member mem
					INNER JOIN payment p 
						ON p.member_id = mem.id 
						AND p.amount > 0
						AND (DATE(p.created_at) >= '" . $date_from . "' AND DATE(p.created_at) <= '" . $date_to . "')
					INNER JOIN member_type mt ON mem.type_id = mt.id AND mt.code = 'AGENT'
					WHERE mem.referring_affiliate_id = a.id  
					LIMIT 1
				) AS agent,
				(
					SELECT 
						COUNT(mem.id) AS total
					FROM member mem
					INNER JOIN payment p 
						ON p.member_id = mem.id 
						AND p.amount > 0
						AND (DATE(p.created_at) >= '" . $date_from . "' AND DATE(p.created_at) <= '" . $date_to . "')
					INNER JOIN member_type mt ON mem.type_id = mt.id AND mt.code = 'BROKER'
					WHERE mem.referring_affiliate_id = a.id  
					LIMIT 1
				) AS broker,
				(
					SELECT 
						SUM(mem.price) AS total
					FROM member mem
					WHERE mem.billing_cycle = 'month' 
					AND mem.referring_affiliate_id = a.id
					AND payment_status = 'paid'
					AND status = 'active'
					AND  (
						# Member has made an actual payment
						SELECT id 
						FROM payment p 
						WHERE member_id = mem.id 
						AND amount > 0
						LIMIT 1
					) IS NOT NULL
				) AS revenue_recur_monthly,
				(
					SELECT 
						SUM(p.amount) AS total
					FROM member mem
					INNER JOIN payment p 
						ON p.member_id = mem.id 
						AND p.amount > 0
						AND (DATE(p.created_at) >= '" . $date_from . "' AND DATE(p.created_at) <= '" . $date_to . "')
					WHERE mem.referring_affiliate_id = a.id
					LIMIT 1
				) AS revenue,
				(
					SELECT 
						SUM(pay.amount) AS total
					FROM member mem
					INNER JOIN payment p 
						ON p.member_id = mem.id 
						AND p.amount > 0
						AND (DATE(p.created_at) >= '" . $date_from . "' AND DATE(p.created_at) <= '" . $date_to . "')
					INNER JOIN payable pay 
						ON pay.member_payment_id = p.id
					WHERE mem.referring_affiliate_id = a.id
					LIMIT 1
				) AS commission
			FROM member m
			INNER JOIN affiliate a ON a.member_id = m.id 
			INNER JOIN member_type mt ON m.type_id = mt.id AND (mt.code = 'SALESADMIN' OR mt.code = 'SUPERADMIN')
			WHERE 1 $conditions
		";


//echo $sql . "<BR>";

		$members = Member::find_by_sql($sql);
		
		if (!empty($members)) {
			uasort($members, function($a, $b){
				if ($a->revenue == $b->revenue) {
					return 0;
				}
				return ($a->revenue < $b->revenue) ? -1 : 1;
			});
		}
		$members = array_reverse($members);
		
		$this->view->members = $members;
		$members_list = Member::find_by_sql("
			SELECT
				m.id,
				m.first_name,
				m.last_name
			FROM
				member as m
				INNER JOIN affiliate a ON a.member_id = m.id
			ORDER BY m.first_name, m.last_name
		");
		$this->view->list_members = $members_list;
	}

	public function referralcredits() 
	{
		
		$conditions = array("conditions" => '', "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('ReferralCredit', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		$this->view->cities    =   array();

	}

	private function todaysTrials()
	{
		/* THIS CACHE IS COMMENTED FOR NOW -- if performance becomes an issue, just uncomment this
		
		if (!empty($_SESSION['daily_stats'])){
			$dailyStats = $_SESSION['daily_stats'];
			if ($dailyStats->timestamp > date('Y-m-d H:i:s', strtotime("-1 hour")))
			{
				return $_SESSION['daily_stats'];
			}
		}
		
		*/

		//Last 45 days...
		$query  = "select DATE(member.cancelled_date) as day, count(distinct(member.id)) as drops from member ";
		$query .= "INNER JOIN payment ON (payment.member_id=member.id) where payment.amount>0 AND member.payment_status = 'cancelled' ";
		$query .= "AND DATE(member.cancelled_date) = DATE(NOW()) ";
		$query .= "GROUP BY day order by day desc LIMIT 1";			

		$dropoffs = Member::find_by_sql($query);


		//Last day SIGNUPS...
		$select        = "DATE(created_at) AS join_date, COUNT(id) AS all_trials";
		$conditions    = "DATE(created_at) = DATE(NOW()) AND payment_status='signup' ";
		
		$group        = "join_date";
		$order        = "join_date DESC";
		$limit        = 1;

		$trials    = Member::find('all', array(
				'select'		=> $select,
				'conditions'   => $conditions,
				'group' 		=> $group,
				'order' 		=> $order,
				'limit' 		=> $limit,
			)
		);



		$sql = "
		SELECT 
			distinct(P1.member_id) as member_id, 
			P1.id,
			P1.amount,
			DATE_FORMAT(P1.created_at, '%Y-%m-%d') AS day_str,
			P1.created_at, 
			member.first_name, 
			member.last_name, 
			P1.created_at,
			(select SUM(P2.amount)from payment P2 where P2.member_id = P1.member_id AND P2.paypal_confirmation_number!='FREE_REFERRAL_CREDIT') as total_revenue, 
			(select count(P3.id) as total_payments from payment P3 where P3.member_id = P1.member_id AND P3.paypal_confirmation_number!='FREE_REFERRAL_CREDIT') as total_payments 
		FROM payment P1 
		JOIN member ON P1.member_id=member.id 
		WHERE 
			P1.payment_type = 'clientfinder' and member.payment_status='paid' 
			AND (DATE(P1.created_at) = DATE(NOW())) 
			AND P1.paypal_confirmation_number!='FREE_REFERRAL_CREDIT'
		GROUP by P1.member_id 
		HAVING total_revenue = 0
		ORDER BY P1.id DESC 
		";
		
		//echo $sql . "<BR>";
		//exit();

			
		$paid = Payment::find_by_sql($sql);

		$dailyStats = new stdClass();
		$dailyStats->trials = !empty($trials[0]) ? $trials[0]->all_trials : 0;
		$dailyStats->paid = count($paid);
		$dailyStats->dropoffs = !empty($dropoffs[0]) ? $dropoffs[0]->drops : 0;
		$dailyStats->timestamp = date('Y-m-d H:i:s');
		
		$_SESSION['daily_stats'] = $dailyStats;
		return $dailyStats;
	}
	
	public function canceled() 
	{
		$selectedStatus = 'all';
		
		$query = '
			SELECT
			`member`.*
			FROM `payment`
			JOIN `member` ON `payment`.`member_id` = `member`.`id`
			WHERE
			`payment`.`payment_type` = "clientfinder"
			AND `member`.`payment_status` = "cancelled"
			GROUP BY `member`.`id`
			ORDER BY `member`.`id` DESC';
		
		$members = Payment::find_by_sql($query);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($members));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

		$this->view->paginator = $paginator;
		$this->view->selected_status = $selectedStatus;
		$this->view->pageHeader = count($members) . ' members who\'ve cancelled';
		$this->view->city_array = $this->getcityarray();
	}
	
	public function paidcanceled() 
	{
		$selectedStatus = 'all';
		
		$query = '
			SELECT
			`member`.*
			, SUM(`payment`.`amount`) AS `total_revenue`
			FROM `payment`
			JOIN `member` ON `payment`.`member_id` = `member`.`id`
			WHERE
			`payment`.`payment_type` = "clientfinder"
			AND `member`.`payment_status` = "cancelled"
			GROUP BY `payment`.`member_id`
			HAVING `total_revenue` > 0';
		
		$members = Payment::find_by_sql($query);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($members));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

		$this->view->totalMembers = count($members);
		$this->view->paginator = $paginator;
		$this->view->selected_status = $selectedStatus;
		$this->view->pageHeader = count($members) . ' paid cancels';
		$this->render('canceled');
	}

	public function member()
	{
	    	//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');
		$this->update_common();
	}
	
	public function update_common()
	{

		$member = Member::find($this->_request->getParam('id'));

		$this->view->member   =	$member;

		if (!$member->isOAuthClient()) {
			$keys = OAuthClient::generateCredentials();
			$this->view->tmp_api_key    = $keys['client_id'];
			$this->view->tmp_api_secret = $keys['client_secret'];
		}

		//$this->view->cities = City::find('all', array('order' => 'name'));
		//$this->view->selectedCityObject = City::find_by_id(intval($this->getSelectedCity()));
		//$this->view->selectedAdminCityObject = City::find_by_id(intval($this->getSelectedAdminCity()));

		$this->view->affiliates = Affiliate::find('all', array('joins' => array('JOIN member ON member_id = member.id'), 'order' => 'last_name ASC, first_name ASC'));
		
		$this->view->franchises = Franchise::find('all');
		

		$query  = "select DATEDIFF(NOW(), created_at) AS number_of_days from member ";
		$query .= "where id=".$member->id;
		$this->view->days_on_site = Member::find_by_sql($query);
		


		$query  = "select sum(amount) as total_amount from payment ";
		$query .= "where member_id=".$member->id . " ";
		$this->view->total_payments = Payment::find_by_sql($query);

		$referralSatellites = $this->member_referrals_findSatellites($member->id, null);
		$this->view->referralSatellitesCount = count($referralSatellites);
	}


	public function update_member_details()
	{
		$flash_messenger = $this->_helper->getHelper('FlashMessenger');
		$this->view->messages = $flash_messenger->getMessages();
		//Disable the layout
		$this->_helper->layout()->disableLayout();

		//add content preference data
		$member = Member::find($this->_request->getParam('id'));
		$content_preference = ContentPref::findAllForMemberAsTag($member->id);
		$this->view->content_preference = $content_preference;




		$this->update_common();
	}

	public function member_referrals()
	{
		//Set the LAYOUT
		$this->_helper->layout()->disableLayout();

		$memberId = $this->_request->getParam('id');

		$affiliate = Affiliate::find_by_member_id($memberId);
		$affiliateId = null;
		if ($affiliate) {
			$affiliateId = $affiliate->id;
		}

		$satellitesByMember = $this->member_referrals_findSatellites($memberId, null);
		$satellitesByMemberPaginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($satellitesByMember));
		$satellitesByMemberPaginator->setCurrentPageNumber($this->_getParam('page', 1));
		$satellitesByMemberPaginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->satellitesByMemberPaginator = $satellitesByMemberPaginator;

		if ($affiliateId) {
			$satellitesByAffiliate = $this->member_referrals_findSatellites(null, $affiliateId);
			$satellitesByAffiliatePaginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($satellitesByAffiliate));
			$satellitesByAffiliatePaginator->setCurrentPageNumber($this->_getParam('page', 1));
			$satellitesByAffiliatePaginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->satellitesByAffiliatePaginator = $satellitesByAffiliatePaginator;
		}

		$referrerTotals = $this->member_referrals_ReferrerTotals($memberId);
		$this->view->referrerTotals = $referrerTotals;
	}

	protected function member_referrals_findSatellites($memberId, $affiliateId) {
		$sql = "
SELECT
m.id,
m.first_name,
m.last_name,
(SELECT COUNT(*) FROM referral_credit credit WHERE credit.referred_member_id = m.id GROUP BY credit.referred_member_id) AS total_referral_credits
FROM member m
WHERE ";
		$where = null;
		$args = array();
		if ($memberId) {
			$where = "m.referring_member_id=?";
			$args[] = $memberId;
		} else if ($affiliateId) {
			$where = "m.referring_affiliate_id=?";
			$args[] = $affiliateId;
		}
		$sql .= $where;
		$res = Member::find_by_sql($sql, $args);
		return $res;
	}

	protected function member_referrals_ReferrerTotals($memberId) {
		$sql = "
SELECT
m.referral_blast_count,
m.referral_email_count,
m.referral_blast_count + m.referral_email_count AS total_referrals,
COUNT(referral_credit_a.id) AS total_referral_credits,
SUM(payment_a.amount) AS total_referral_payment_amount
FROM member m
LEFT JOIN referral_credit referral_credit_a ON referral_credit_a.member_id = m.id
LEFT JOIN payment payment_a ON payment_a.id = referral_credit_a.payment_id
WHERE m.id=?
GROUP BY m.id;";
		$res = Member::find_by_sql($sql, array($memberId));
		if (count($res)) {
			return $res[0];
		}
		else {
			return null;
		}
	}
	
	public function add_manual_referral()
	{
		$this->view->all_users = Member::all(array('select' => 'member.id, first_name, last_name', 'conditions' => 'status = "active" AND affiliate.id IS NULL', 'order' => 'first_name, last_name', 'joins' => 'LEFT JOIN affiliate ON affiliate.member_id = member.id'));
		if($this->_request->isPost())
		{
			$referring_member = Member::find($this->params['referring_member_id']);
			$referred_member  = Member::find($this->params['referred_member_id']);
			
			if ($referring_member->affiliate_account instanceOf Affiliate) {
				$referred_member->referring_affiliate_id = $referring_member->affiliate_account->id;
			} else {
				$referred_member->referring_member_id = $referring_member->id;
			}
			$referred_member->save();
			
			$referred_member->creditAffiliate(1);
			$this->_redirect('/admin/referralcredits');
		}
	}
	
	public function list_tags()
	{
		$query = 'SELECT
				t.*,
				count(tc.id) as content_count
			FROM
				tag AS t
			LEFT JOIN tag_cloud as tc ON
				t.id = tc.tag_id
			GROUP BY
				t.id';
		
		$tags = Tag::find_by_sql($query);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($tags));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

		$this->view->paginator = $paginator;
	}
	
	public function add_tag()
	{
		$this->view->tag_groups = TagGroup::all(array('order' => 'id ASC'));
		
		$member_types = MemberType::all();
		$this->view->member_types = $member_types;
	}
	
	function update_tag()
	{
		$id = $this->getRequest()->getParam('id');
		
		$this->view->tag_groups = TagGroup::all(array('order' => 'id ASC'));
		
		$member_types = MemberType::all();
		$this->view->member_types = $member_types;
		

		$member_type_settings = TagMemberTypeSetting::find_all_by_tag_id($id);
		$member_type_settings_temp = array();
		if (!empty($member_type_settings)) {
			foreach ($member_type_settings as $type_settings) {
				$member_type_settings_temp[$type_settings->member_type_id] = $type_settings;
			}
			$member_type_settings = $member_type_settings_temp;
		}
		$this->view->member_type_settings = $member_type_settings;
		
		$tag = Tag::find($id);
		$this->view->tag = $tag;

		$this->render("add-tag");
	}

	public function save_tag()
	{
	    if($this->_request->isPost())
	    {
			$params = $this->getRequest()->getParams();

			if(!empty($params['id']))
			{
				$tag = Tag::find($params['id']);
			}
			else
			{
				$tag = new Tag();
			}
			
			$tag->name = $params['name'];
			$tag->description = $params['description'];
			$tag->tag_group_id = $params['tag_group_id'];
			$tag->save();
			
			
			$member_type_settings = TagMemberTypeSetting::find_all_by_tag_id($tag->id);
			$member_type_settings_temp = array();
			if (!empty($member_type_settings)) {
				foreach ($member_type_settings as $type_settings) {
					$member_type_settings_temp[$type_settings->member_type_id] = $type_settings;
				}
				$member_type_settings = $member_type_settings_temp;
			}
		
			if (!empty($params['member_type_settings'])) {
				foreach ($params['member_type_settings'] as $member_type_id => $settings) {
					if (isset($member_type_settings[$member_type_id])) {
						$setting = &$member_type_settings[$member_type_id];
					} else {
						$setting = new TagMemberTypeSetting();
					}
					$setting->tag_id = $tag->id;
					$setting->member_type_id = $member_type_id;
					$setting->available = !empty($settings['available']) ? 1 : 0;
					$setting->default_value = !empty($settings['default_value']) ? 1 : 0;
					$setting->save();
				}
			}
		}
		$this->_helper->viewRenderer->setNoRender(true);
		$this->redirect("/admin/list_tags/");
	}
	
	public function delete_tag()
	{
		$params = $this->getRequest()->getParams();
		
	    	if(!empty($params['id']))
	    	{
			$tag = Tag::find($params['id']);
		}

		$tag->delete();
		
		$this->redirect("/admin/list_tags/");
	}

	public function group_city_download()
	{
		$this->renderNothing();

		$filename = 'Group_City.csv';
		$columns = array(
			'Group ID',
			'Group Name',
			'City ID',
			'City Name'
		);

		$report   = new Blastit_Report;
		$report->enableCSV();
		$report->setColumnTitles($columns);

		$data = GroupCity::find_by_sql('select gc.group_id, g.`name` as group_name, gc.city_id, c.`name` as city_name from group_city gc, `group` g, city c
where gc.group_id=g.id and gc.city_id = c.id');

		foreach ($data as $row) {
			$report->addRow(
				array(
					$row->group_id,
					$row->group_name,
					$row->city_id,
					$row->city_name
				)
			);
		}

		$report->generate();

		$fsize = filesize($report->getCSVFile());
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false);
		header("Content-Transfer-Encoding: binary");
		header("Content-Type: application/force-download");
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-length: $fsize");
		fpassthru(fopen($report->getCSVFile(), 'r'));
	}
}
