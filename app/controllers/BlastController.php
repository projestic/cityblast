<?
class BlastController extends ApplicationController
{
	#	TEST VISA ACCOUNT
	#
	#	4178444934510973
	#	5/2016

	CONST AMOUNT		=   "49.00";
	CONST TAXES		=   "0.13"; // 13%
	CONST CURRENCY		=   "CAD";
	CONST PRIORITY_STEPS	=   10;

	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		parent::init();

 		//and blasting="ON"
		$this->view->blast_cities	= City::find('all', array('conditions' => 'UPPER(name) != "CANADA" and UPPER(name) != "UNITED STATES"', 'order' => 'name'));
	}
	public function membersonly(){}


	public function index()
	{
		if(!isset($_SESSION['member']) || empty($_SESSION['member']))
		{
			//After you sign-up or log in, redirect to the listing details
			$_SESSION['REDIRECT_AFTER_LOGIN'] = $_SESSION['next'] = "/blast/index";

			// instead of redirect we are showing popup with colorbox
			//$this->_redirect("/member/authenticate");
			$this->view->popup	= 1;
		}


		if(!isset($_SESSION['member']))
		{
			$this->_redirect("/member/authenticate");
		}


		$member = Member::find_by_id($_SESSION['member']->id);
		if($member->payment_status != "paid" and $member->payment_status != "free" and $member->payment_status != "signup" and $member->account_type != "free")
		{
			$this->_redirect("/member/authenticate");
		}

		$id	= $this->_getParam('id', 0);

		if ($id)
		{
			//Grab the LISTING from the DB
			$listing = Listing::find_by_id($id);


			$this->view->listing = $listing;

			$this->view->member = null;
			if($listing->member_id)
			{
				$this->view->member = $member = Member::find_by_id($listing->member_id);
			}

		} 
		else if ( $this->getRequest()->isPost() ) 
		{
			$params = $this->getRequest()->getPost();




			//Set the form data
			$listing = new Listing();
			$listing->image 			= $params['fileUploaded'];
			//$listing->thumbnail 		= $params['thumbUploaded'];
			$listing->blast_type 		= $params['blast_type'];
			$listing->city_id 			= $params['city_id'];
			$listing->message 			= $params['message'];
			$listing->price 			= $params['price'];
			$listing->property_type_id 	= $params['property_type_id'];
			$listing->number_of_bedrooms 	= $params['number_of_bedrooms'];
			$listing->number_of_extra_bedrooms 	= $params['number_of_extra_bedrooms'];
			$listing->number_of_bathrooms = $params['number_of_bathrooms'];
			$listing->number_of_parking 	= $params['number_of_parking'];
			$listing->maintenance_fee 	= $params['maintenance_fee'];
			$listing->street 			= $params['street_number'] . ' ' . $params['street'];
			$listing->appartment 		= $params['appartment'];
			$listing->city 			= $params['city'];
			$listing->postal_code 		= $params['postal'];
			$listing->square_footage 	= $params['square_footage'];
			$listing->mls 				= $params['mls'];
			$listing->showcase 			= $params['showcase'];
			
			$this->view->listing = $listing;

		}

		$_SESSION['UPLOAD_PID']	=   uniqid();

		if (isset($_SESSION['member']))
		{
			$this->view->member = $_SESSION['member'];
			if ( $this->getRequest()->isPost() ) 
			{
				$params = $this->getRequest()->getPost();
				$this->view->member->brokerage = $params['brokerage'];
				$this->view->member->broker_address = $params['broker_address'];
				$this->view->member->phone = $params['phone'];
			}
		}

		if(!isset($this->view->listing)) 
			$this->view->listing = new stdClass;

		$this->view->select_city = empty($_SESSION['member']->city_id)
			? intval($this->getSelectedCity()) : $_SESSION['member']->city_id;
	}

	public function preview()
	{
		$params = $this->getRequest()->getParams();

	
		$blast_type = $params['blast_type'];
		$this->view->blast_type = $blast_type;
		
		if ( empty($_SESSION['member']) ) 
		{
			//After you sign-up or log in, redirect to the listing details
			$_SESSION['REDIRECT_AFTER_LOGIN'] = $_SESSION['next'] = "/blast/index";

			$this->_redirect("/member/authenticate");
		}

	
		if ( !$this->getRequest()->isPost() ) 
		{
			$this->_redirect('/blast/index');
		}


		$this->view->member  = $_SESSION['member'];
		$this->view->params  = $params = $this->getRequest()->getPost();
	}

	public function login()
	{
		if(!isset($_SESSION['member']) || empty($_SESSION['member']))
		{
			//After you sign-up or log in, redirect to the listing details
			$_SESSION['REDIRECT_AFTER_LOGIN'] = $_SESSION['next'] = "/blast/index";

			$this->_redirect("/member/authenticate");
		}

		if(isset($_SESSION['member'])) $member = $this->view->member = $_SESSION['member'];

		$request	=   $this->getRequest();
		$payment_type   =   $request->getParam('payment_type');


		//Handling existing paypal transactions to use for next payment
		if(strtolower($payment_type) != "newcard"	)
		{
			$options	=	array(
				'conditions' => array( "member_id=? AND paypal_transaction_expire_date > ?
						AND paypal_confirmation_number IS NOT NULL",
						intval($member->id),
						date("Y-m-d H:i:s")),
				'order' => 'created_at desc',
				'limit' => 1);

			$payment	=	Payment::find('all',$options);
			if(count($payment) > 0)
			{
				$this->view->payment = $payment[0];
				$this->render('referencetransactionpayment');
				return;
			}
		}

	}
	
	/**
	 * 
	 * Validate listing for duplicacy
	 * @param array $params
	 */
	private function _validateListing(array $params) {
		$result = true;

		$street = addslashes($params['street_number']." ".$params['street']);
		$appartment = addslashes($params['appartment']);
		$postal_code = addslashes($params['postal']);
		$city = addslashes($params['city']);
		
		$listing = Listing::find( array('conditions' => array('street=? AND postal_code=? AND appartment=? AND city=? AND status != ?', $street, $postal_code, $appartment, $city, 'deleted') ) );		
		
		if ($listing) {
			$created_at = strtotime($listing->created_at->format('Y-m-d H:i:s'));
			$time = time();
			$diffDay = (($time - $created_at)/(3600 * 24));			
			if ($diffDay <= 7) $result = false;
		}
		
		return $result;
	}
	
	public function blastsave()
	{
		$params =	$this->getRequest()->getParams();
		$member = $_SESSION['member'];

		// Validate listing for duplicacy
		if(!$this->_validateListing($params))
		{
			$message = 'A listing with following details already exists in the database. \n';
			$message .= "* Street: " . $params['street_number']." ".$params['street'] . '\n';
			$message .= "* Apartment: " . $params['appartment'] . '\n';
			$message .= "* Apartment: " . $params['appartment'] . '\n';
			$message .= "* PostalCode: " . $params['postal'] . '\n';
			$message .= "* City: " . $params['city'] . '\n';
			echo "<script>alert('$message'); window.location='/blast/index';</script>";
			exit;
		}


		//Save the listing...
		$listing = new Listing();
		$listing->raw_message 	= $params['message'];

//		$message = $this->sentence_case($params['message']);
		$message = $params['message'];

		$listing->message 		= $message;
		$listing->member_id 	= $member->id;
		$listing->street 		= $params['street_number']." ".$params['street'];
		$listing->appartment	= $params['appartment'];
		$listing->city_id		= $params['city_id'];
		$listing->postal_code 	= $params['postal'];
		$listing->price 		= $params['price'];
		$listing->mls 			= $params['mls'];
		$listing->city   		= $params['city'];
		$listing->square_footage = $params['square_footage'];

		$listing->blast_type   	= $params['blast_type'];
		$listing->pre_approval 	= 0;
		$listing->approval 		= 0;


		//Images verification
		$listing->number_of_bedrooms	= intval($params['number_of_bedrooms']);
		$listing->number_of_extra_bedrooms	= intval($params['number_of_extra_bedrooms']);
		$listing->number_of_bathrooms	= intval($params['number_of_bathrooms']);
		$listing->number_of_parking		= intval($params['number_of_parking']);
		$listing->maintenance_fee		= $params['maintenance_fee'];
		$listing->property_type_id		= $params['property_type_id'];
		$listing->expiry_date			= date('Y-m-d H:i:s', strtotime('+30 days'));
		
		$listing->showcase		= $params['showcase'];

		//Save the listing...
		$listing->save(false);

		
		$image      = stristr($params['fileUploaded'], TMP_ASSET_URL) ? $params['fileUploaded'] : TMP_ASSET_URL.$params['fileUploaded'];
		//$thumbnail  = stristr($params['thumbUploaded'], TMP_ASSET_URL) ? $params['thumbUploaded'] : TMP_ASSET_URL.$params['thumbUploaded'];
		
		$imageFile = str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $image);
				
		if (file_exists($imageFile) && is_file($imageFile)) {
			// Make big image listing if image is big enough
			// !! TEMP DISABLE BIG IMAGE LISTINGS
			//list($width, $height) = getimagesize($imageFile);
			//if ($width > 400) {
			//	$listing->big_image = 1;
			//}

			$listing->ingestImage($imageFile);

		}
		
		$_SESSION['order_number'] = $listing->id;

		$i = 1;
		$endCycle = false;
		do {
			$uploadField = 'fileUploaded' . $i;

			if(!isset($params[$uploadField]))
			{
				$endCycle = true;
			} 
			elseif (!empty($params[$uploadField]))
			{
				$path = stristr($params[$uploadField], TMP_ASSET_URL) ? $params[$uploadField] : TMP_ASSET_URL . $params[$uploadField];
				$tmpFile = str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $path);
				$altImage = new ListingImage();
				$altImage->listing_id = $listing->id;
				$altImage->save();
				$altImage->ingestImage($tmpFile);
			}
			$i++;
		} while (!$endCycle);

		//Set the LATTITUDE for mapping purposes
		$listing->setLatitude();

		//$_SESSION['order_number'] = $listing->id;
		$listing->save();

		//Save the member's brokerage and broker address
		$member = $_SESSION['member'];

		if(!$member->city_id) $member->city_id = $listing->city_id;

		$member->brokerage = $params['brokerage'];
		$member->broker_address = $params['broker_address'];
		$member->save();


		$member->publishOneOff($listing);


		if ($listing->blast_type == Listing::TYPE_IDX) {
			$this->redirect("/listing/view/".$listing->id);
		} else {
			$_SESSION['order_number'] = $listing->id;
			$this->_redirect("/blast/thankyou");
		}
	
	}

	public function uploadimage() 
	{ 

		$tmp_subdir = date('Y-m-d-H');

		if (!empty($_FILES))
		{

			$tempFile = $_FILES['Filedata']['tmp_name'];
			
			$fileParts	= explode(".", $_FILES['Filedata']['name']);
			$extension  = end($fileParts);
			$filename   = str_replace(".".$extension,"",$_FILES['Filedata']['name']);

			$filename   = str_replace("-","_",$filename);
			$finalName  = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $filename);
			$finalName  = trim($finalName, '-');
			$finalName  = preg_replace("/[\/_|+ -]+/", '_', $finalName) . ".$extension";

			$targetFile = TMP_ASSET_STORAGE . "/" . $tmp_subdir . "/". $finalName;

			if(	!file_exists(TMP_ASSET_STORAGE . "/" . $tmp_subdir)	)
			{
				mkdir(TMP_ASSET_STORAGE . "/" . $tmp_subdir, 0777, true);
			}
			
			if (file_exists($targetFile)) 
			{
				$targetFileInfo = pathinfo($targetFile);
				$targetFile = $targetFileInfo['dirname'] . '/' . $targetFileInfo['filename'] . '_' . strtotime('now') . '.' . $targetFileInfo['extension'];
			}

			if(move_uploaded_file($tempFile, $targetFile))
			{
				echo TMP_ASSET_URL . '/' . $tmp_subdir . "/". basename($targetFile);
			}
			else
			{
				$msg = null;

				switch ($_FILES['Filedata']['error'])
				{
					case 0:
						$msg = "No Error"; // comment this out if you don't want a message to appear on success.
						break;
					case 1:
						$msg = "The file is bigger than this PHP installation allows";
						break;
					case 2:
						$msg = "The file is bigger than this form allows";
						break;
					case 3:
						$msg = "Only part of the file was uploaded";
						break;
					case 4:
						$msg = "No file was uploaded";
						break;
					case 6:
						$msg = "Missing a temporary folder";
						break;
					case 7:
						$msg = "Failed to write file to disk";
						break;
					case 8:
						$msg = "File upload stopped by extension";
						break;
					default:
						$msg = "unknown error ".$_FILES['Filedata']['error'];
						break;
				}

				//echo $_FILES['Filedata']['error'];

				echo $msg;
			}


		}
		else
		{
			echo "0";
		}
		$this->cleanupTmpDir();
		$this->renderNothing();
	}

	protected function cleanupTmpDir() 
	{
		foreach (scandir(TMP_ASSET_STORAGE) as $file) 
		{
			$file = TMP_ASSET_STORAGE . '/' . $file;
			if (is_file($file) && $file != '..' && $file != '.' && !stristr($file, '.svn') && (filemtime($file) < time() - 14400)) {
				@unlink($file);
			} elseif (is_dir($file)) {
				@rmdir($file);
			}
		}
	}

	public function newcard(){}


	public function referencetransactionpayment()
	{
		/*********************************************
		if(!$_SESSION['member'])
		{
			echo "ERROR: something went wrong.<BR>";
			exit();
		}

		$member = $this->view->member = $_SESSION['member'];
		$paymentO = null;
		if(isset($_POST['pid'])) $paymentO = Payment::find($_POST['pid']);

		if( !empty($paymentO) && $paymentO->member_id != $member->id)
		{
			die("INVALID OPERATION FOUND");
		}

		//Submit the payment to PayPal
		$paypal = new PaypalRequest();


		//Get the listing...
		$listing = Listing::find_by_id($_SESSION['order_number']);

		if($listing->list_city->price > 0) $PAYMENT_AMOUNT = $listing->list_city->price;
		else $PAYMENT_AMOUNT = self::AMOUNT;


		$taxes  =   round($PAYMENT_AMOUNT * self::TAXES , 2);
		$amount =  $PAYMENT_AMOUNT + $taxes;

		$response = $paypal->DoReferenceTransaction($paymentO, $amount, self::CURRENCY, self::ITEM_NAME);

		if("SUCCESS" == strtoupper($response["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($response["ACK"]))
		{
			$payment = new Payment();

			$payment->paypal_confirmation_number 		= $response['TRANSACTIONID'];
			$payment->paypal_transaction_expire_date		=	date("Y-m-d H:i:s", strtotime("+1 year"));
			$payment->cc_expire_date					=	$paymentO->cc_expire_date;

			$payment->last_four	=	$paymentO->last_four;

			$payment->first_name 	= $paymentO->first_name;
			$payment->last_name 	= $paymentO->last_name;
			$payment->member_id 	= $member->id;

			$payment->address1 		= $paymentO->address1;
			$payment->city 		= $paymentO->city;
			$payment->state 		= $paymentO->state;
			$payment->country 		= $paymentO->country;
			$payment->zip 			= $paymentO->zip;
			//$payment->phone 		= $paymentO->phone;
			$payment->amount 		= floatVal($PAYMENT_AMOUNT);
			$payment->taxes 		= floatVal($taxes);
			$payment->save();

			$_SESSION['payment_id'] = $payment->id;

			$message = $listing->message;
			$message = $this->sentence_case($message);


			//UPDATE the POST info
			$listing->payment_id = $payment->id;
			$listing->approval = 0;
			$listing->priority = $listing->id*self::PRIORITY_STEPS;
			$listing->save();

			$_SESSION['order_number']   =   $listing->id;


			//Send them to a member sign-up page...
			$this->redirect('/blast/thankyou');

		}
		else
		{
			info("WE FAILED TO SEND PROCESS THIS CC INFORMATION:");
			//info(print_r($e, true));
			info(print_r($response, true));
			info(print_r($_REQUEST, true));



			//If something goes wrong here... we're going to need to ask them for all their CC information again!



			$tmp_error_array = array();



			$errors = null;
			for($i=0; $i<=5; $i++)
			{

				$index = 'L_LONGMESSAGE'.$i;
				if(isset($response[$index]) && !empty($response[$index])	)
				{
					if(!in_array(urldecode($response[$index]), $tmp_error_array))
					{

						$tmp_error_array[] = urldecode($response[$index]);

						$message = urldecode($response[$index]);
						$message = str_replace("This transaction cannot be processed. ", "", $message);

						$errors[] = "<li>" . $message . "</li>";
					}
				}
			}

			$this->view->footer_off = TRUE;
			$this->view->errors = $errors;

			//Return them to the /blast/login and display errors...
			$this->render("index");
		}
		******************************/
	}




	function publish_facebook($member, $listing)
	{

		global $invalidTokenEmails, $sentOk;


		//Set up the FB Interface Class
		$facebook = new Facebook(
							array(
								'appId'  => $member->network_app_facebook->consumer_id,
								'secret' => $member->network_app_facebook->consumer_secret,
								'cookie' => false
							)
						);

		// Get the facebook payload object
		$publish_array 	=	$listing->getFacebookPayload($member);

		try {

			$fb_result = $facebook->api("/".$member->uid."/feed", "post", $publish_array);

			$message = "Successful post to FB: " .$fb_result['id'];
			$member->saveListingPost($listing, $message, 'FACEBOOK', 1, $fb_result['id'], $member->facebook_friend_count);

		} catch(Exception $e) {

			//echo "ERROR: ".$e->getMessage()."\n";

			// Handling invalid token
			if( strstr($e->getMessage(), "Error validating") )
			{
				if(isset($member->email) && !empty($member->email))
				{
					$invalidTokenEmails[$member->email]['object'] =   $member;
						$invalidTokenEmails[$member->email]['facebook'] =   1;
					}
			}

			$message	  = "Factbook post FAIL. ERROR:".$e->getMessage();
			$member->saveListingPost($listing, $message, 'FACEBOOK', 0);

		}

	}




	function publish_twitter($member, $listing)
	{

		//Set up the TWITTER Interface Class
		//echo "\t".date("Y-m-d H:i:s")."\t TWITTER Publishing Member: ".$member->first_name . " " . $member->last_name . "\n";

		$my_twitter = new Blastit_Service_Twitter(null, $member);

		$twitter_suffix = "... " . APP_URL . "/listing/view/".$listing->id."?mid=".$member->id;
		$current_length = mb_strlen($listing->message);
		$suffix_length = mb_strlen($twitter_suffix);


		if(($current_length + $suffix_length) > 140)
		{
			$total_available = 140 - $suffix_length;			
			$prefix = mb_substr($listing->message, 0, $total_available);
			$twitter_msg = $prefix . $twitter_suffix;
		}
		else
		{
			$twitter_msg = $listing->message . $twitter_suffix;
		}


		//Ok, let's post...

		// Second param means that a twitter object will be returned
		// Response will be Json format

		$response = null;
		try
		{

			$response = $my_twitter->statusUpdate($twitter_msg);
		}
		catch(Exception $e)
		{

			// Handling invalid token
			/****************************
			if( !strstr($e->getMessage(), "Error validating") )
			{
				mail("alen@alpha-male.tv","Exception Publishing To Twitter", $e->getMessage());
			} **************************/
		}

		if($response)
		{
			if( !empty($response->error) )
			{
				$post = new Post();

				$post->listing_id = $listing->id;
				$post->member_id = $member->id;
				$post->message = "Twitter post FAIL. ERROR:".print_r($response->error, true);
				$post->twitter_post_id = $response->id_str;
				$post->result	  = 0;
				$post->save();

				//echo $member->email . " -> FAILED to post to Twitter: ".$response->error."\n";
			}
			else
			{

				//THIS IS A TWITTER POST!
				$post = new Post();

				$post->listing_id = $listing->id;
				$post->member_id = $member->id;
				$post->message = "Successful post to Twitter: " .$response->id_str;
				$post->twitter_post_id = $response->id_str;
				$post->result	  = 1;
				$post->save();
			}
		}
	}



	function publish_linkedin($member, $listing)
	{

		$linkedin = new LinkedIn();

		//echo "\t".date("Y-m-d H:i:s")."\t Linkedin Publishing Member: ".$member->first_name . " " . $member->last_name . "\n";

		//Randomize the message when possible
		$random_message = $listing->message;
		$random_image = CDN_URL . $listing->image;




		$link = APP_URL . "/listing/view/".$listing->id."?mid=".$member->id;

		$desc = null;
		$desc_array = null;
		
		/***********************
		$price_components = explode(".", $listing->price);
		$price_components[0] = preg_replace("/[^a-zA-Z0-9\s]/", "", $price_components[0]);
		$listing->price = $price_components[0];
		//$listing->price = preg_replace("/[^a-zA-Z0-9\s]/", "", $listing->price);


		$desc = null;
		$desc_array = null;
		if(isset($listing->price) && !empty($listing->price))
		{
			if(is_numeric($listing->price)) $desc .= "List Price: $".number_format($listing->price). " ";
			else $desc .= "List Price: ".$listing->price. " ";
		} ************/


		if(isset($listing->street) && !empty($listing->street)) $desc_array[] = $listing->street;
		if(isset($listing->city) && !empty($listing->city)) $desc_array[] = $listing->city;
		if(isset($listing->postal_code) && !empty($listing->postal_code)) $desc_array[] = $listing->postal_code;



		if(is_array($desc_array) && count($desc_array)) $desc .= implode(", ", $desc_array);

		$link_title = $desc;



		$token	  =	unserialize($member->linkedin_access_token);
		$response   =  $linkedin->statusUpdate($token->oauth_token, $token->oauth_token_secret, $random_message, $link, $link_title, $random_image);



		if($response)
		{

			$message = "Successful post to Linkedin";
			$member->saveListingPost($listing, $message, 'LINKEDIN', 1);

			//Reset the FAIL to zero again, because we've succeeded!
			$member->linkedin_fails = 0;
			$member->save();
		}
	}



	//Thank you page for payment
	public function thankyou()
	{
		$member = $this->view->member = $_SESSION['member'];

		if(isset($_SESSION['order_number']))
		{
			$this->view->order_number = $_SESSION['order_number'];
			$listing = Listing::find_by_id($_SESSION['order_number']);
			$this->view->listing	=   $listing;
		}

		if(APPLICATION_ENV == "production")
		{

			//Let's start out with a sanity check... have we published this posting before?
			$post = Post::find_by_member_id_and_listing_id($member->id, $listing->id);


			if(!$post)
			{
				//As per Shaun's instructions, the publish should be the "RAW" message
				$listing->message = $listing->raw_message;

				//Ok, let's publish the post for this member right now at "game time"
				$this->publish_facebook($member, $listing);

				//Publish to Twitter
				if($member->twitter_access_token) $this->publish_twitter($member, $listing);

				//Publish to LinkedIn
				if($member->linkedin_access_token) $this->publish_linkedin($member, $listing);
			}

		}

		/***********************************************************************
		if(isset($_SESSION['order_number']))
		{
			$this->view->order_number = $_SESSION['order_number'];
			$listing = Listing::find_by_id($_SESSION['order_number']);
			$this->view->listing	=   $listing;
			$this->view->payment	=   $listing->payment;

			$this->view->taxes	=   self::TAXES;


			if($listing->list_city->price > 0) $PAYMENT_AMOUNT = $listing->list_city->price;
			else $PAYMENT_AMOUNT = self::AMOUNT;


			$this->view->amount	=   $PAYMENT_AMOUNT;

			if( empty($_SESSION['order_email_sent_'.$_SESSION['order_number']]) )
			{

				if(isset($member->email) && !empty($member->email))
				{
					// Need to send the invoice to user email
					// Send email
					$from_name = "City-Blast.com";
					$from_email = "info@cityblast.com";


					$params	=   array(
									'order_number'  	=> $_SESSION['order_number'],
									'listing'				=> $listing,
									'taxes'				=>	self::TAXES,
									'amount'				=>	$PAYMENT_AMOUNT,
									'member'			=>  $_SESSION['member'],
									'total_count' 		=> $this->view->total_count
								);

					$htmlBody	   =   $this->view->partial('email/invoice.html.php', $params);
					$email	=   new ZendMailLogger('INVOICE', $member->id);
					$email->addTo($member->email);
					$email->setSubject('Invoice for order number #'.$_SESSION['order_number']);
					$email->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
					$email->setFrom($from_email, $from_name);
					$email->sendemail();

					$_SESSION['order_email_sent_'.$_SESSION['order_number']]	=	true;
				}
			}

			//Let's start out with a sanity check... have we published this posting before?
			$post = Post::find_by_member_id_and_listing_id($member->id, $listing->id);

			if(!$post)
			{
				//As per Shaun's instructions, the publish should be the "RAW" message
				$listing->message = $listing->raw_message;

				//Ok, let's publish the post for this member right now at "game time"
				$this->publish_facebook($member, $listing);

				//Publish to Twitter
				if($member->twitter_access_token) $this->publish_twitter($member, $listing);

				//Publish to LinkedIn
				if($member->linkedin_access_token) $this->publish_linkedin($member, $listing);
			}

		}
		else $this->view->order_number = null;

		$this->view->free_blast 	= (!empty($_SESSION['lastFreeBlast']) && $_SESSION['lastFreeBlast'] == true);
		$this->view->free_blasts_left   = intval($member->credits);

		unset($_SESSION['lastFreeBlast']);
		//unset($_SESSION['order_number']);
		************************************************************/



	}




	public function payment()
	{
		  /************************************************************************

		//Save the MEMBER info
		if($_SESSION['member'])
		{
			$member = $_SESSION['member'];

			//$member->company_name = $_POST['company_name'];
			//$member->company_link = $_POST['company_link'];
			$member->address = $_POST['address'];
			//$member->address2 = $_POST['address2'];
			$member->zip = $_POST['zip'];
			$member->city = $_POST['city'];
			$member->state = $_POST['state'];
			$member->country = $_POST['cc_country'];
			//$member->phone = $_POST['phone'];

			if(!$member->save())
			{
				echo "<h1>PROBLEM!!!!!</h1>";
				exit();
			}

		}
		else
		{
			echo "ERROR: something went wrong.<BR>";
			exit();
		}


		$params =   $_POST;

		$validator = new Validator();
		$validator->listing($params);

		if(count($validator->errors))
		{
			$this->view->errors = is_array($validator->errors) ? $validator->errors : array($validator->errors);
			$this->view->fields = $validator->fields;
			$this->render("login");
		}
		else
		{
			//Get the listing...
			$listing = Listing::find_by_id($_SESSION['order_number']);




			if($listing->list_city->price > 0) $PAYMENT_AMOUNT = $listing->list_city->price;
			else $PAYMENT_AMOUNT = self::AMOUNT;


			//Set up the parameters
			$desc = self::ITEM_NAME;


			//Charge GBP if the CC is Solo or Maestro
			$currency = self::CURRENCY;
			$taxes	= round($PAYMENT_AMOUNT * self::TAXES, 2);
			$amount 	= $PAYMENT_AMOUNT + $taxes;

			//Submit the payment to PayPal
			$paypal = new PaypalRequest();
			$response = $paypal->HTTP_Post($amount, $currency, $desc, $_REQUEST);

			if("SUCCESS" == strtoupper($response["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($response["ACK"]))
			{
				//Write it down in the Payment Log
					//$response['TRANSACTIONID']  =   'testing';
				$payment = new Payment();

				$payment->paypal_confirmation_number = $response['TRANSACTIONID'];

				$payment->paypal_transaction_expire_date = date("Y-m-d H:i:s", strtotime("+1 year"));
				$payment->cc_expire_date = $_REQUEST['exp_month'].$_REQUEST['exp_year'];
				$payment->first_name = $_REQUEST['fname'];
				$payment->last_name = $_REQUEST['lname'];

				$payment->member_id = $member->id;

				$payment->address1 = $_REQUEST['address'];
				//$payment->address2 = $_REQUEST['address2'];
				$payment->city = $_REQUEST['city'];
				$payment->state = $_REQUEST['state'];
				$payment->country = $_REQUEST['cc_country'];
				$payment->zip = $_REQUEST['zip'];
				//$payment->phone = $_REQUEST['phone'];
				//$payment->email = $_REQUEST['email'];
				$payment->amount =  $PAYMENT_AMOUNT;
				$payment->taxes  =  $taxes;

				$payment->last_four = substr($_REQUEST['cc_number'], -4, 4);
				$payment->save();


				//Store the PAYMENT ID so that we can associate a MEMBER ID after the signup process
				$_SESSION['payment_id'] = $payment->id;




				//Save the POST info
				$listing->payment_id = $payment->id;
				$listing->priority = $listing->id*self::PRIORITY_STEPS;

				//$listing->member_id = $member->id;

				//$listing->message = $message;
				//$listing->created_at = date("Y-m-d H:i:s");

				$listing->save();


				$this->view->order_number = $listing->id;
					$_SESSION['order_number']   =   $listing->id;

				//Send them to a member sign-up page...
				$this->redirect('/blast/thankyou');


			} else {


				info("WE FAILED TO SEND PROCESS THIS CC INFORMATION:");
				info(print_r($e, true));
				info(print_r($response, true));
				info(print_r($_REQUEST, true));

				$this->view->contest_id = $contest_track_id;

				$this->view->fname = $_REQUEST['fname'];
				$this->view->lname = $_REQUEST['lname'];

				$this->view->address = $_REQUEST['address'];
				//$this->view->address2 = $_REQUEST['address2'];
				$this->view->city = $_REQUEST['city'];
				$this->view->state = $_REQUEST['state'];
				$this->view->cc_country = $_REQUEST['cc_country'];
				$this->view->zip = $_REQUEST['zip'];
				$this->view->phone = $_REQUEST['phone'];
				//$this->view->email = $_REQUEST['email'];
				//$this->view->discount = $_REQUEST['discount'];


				$tmp_error_array = array();




				for($i=0; $i<=5; $i++)
				{

					$index = 'L_LONGMESSAGE'.$i;
					if($response[$index] != "")
					{
						if(!in_array(urldecode($response[$index]), $tmp_error_array))
						{

							$tmp_error_array[] = urldecode($response[$index]);

							$message = urldecode($response[$index]);
							$message = str_replace("This transaction cannot be processed. ", "", $message);

							$errors .= "<li>" . $message . "</li>";
						}
					}
				}

				$this->view->footer_off = TRUE;
				$this->view->errors = $errors;



				//DISPLAY THE ERRORS (the login page is also the payment error page)...
				$this->render("login");
			}
		}


		********************************************/
	}


	function sentence_case($string)
	{
		$sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
		$new_string = '';
		foreach ($sentences as $key => $sentence) {
			$new_string .= ($key & 1) == 0 ?
				ucfirst(strtolower(trim($sentence))) :
				$sentence.' ';
		}
		return trim($new_string);
	}



	/*************************************************
	public function sendFreeBlast($member, $message)
	{
			$payment = new Payment();


			$member->credits	=   $member->credits-1;
			$member->save();

			if($member->credits <= 0)
			{
			$_SESSION['lastFreeBlast']	=   true;
		}

			$payment->paypal_confirmation_number = 'Free blast';
			$payment->first_name = $member->first_name;
			$payment->last_name = $member->last_name;
			$payment->member_id = $member->id;

			$payment->address1 = !empty(  $member->address ) ? ($member->address) : "undefined";
			$payment->city = (!empty($member->hometown_city)) ? $member->hometown_city : 'empty';
			$payment->state = (!empty($member->hometown_state)) ? $member->hometown_state : 'empty';
			$payment->country = (!empty($member->hometown_country)) ? $member->hometown_country : 'empty';
			$payment->zip = (!empty($member->zip)) ? $member->zip : 'empty';
			$payment->phone = (!empty($member->phone)) ? $member->phone : 'empty';;
			$payment->amount = "0.00";
			$payment->taxes= "0.00";
			$payment->save();


			//Store the PAYMENT ID so that we can associate a MEMBER ID after the signup process
			$_SESSION['payment_id'] = $payment->id;


		$message = $this->sentence_case($message);




			//Save the POST info
			$listing = new Listing();

			$listing->member_id = $member->id;
			$listing->payment_id = $payment->id;
			$listing->approval = 0;
			$listing->message = $message;
			$listing->created_at = date("Y-m-d H:i:s");
			$listing->save();

			$listing->priority = $listing->id*self::PRIORITY_STEPS;
			$listing->save();

			$this->view->order_number = $listing->id;
			$_SESSION['order_number']   =   $listing->id;


			if(mb_strlen($listing->message) > 140)
			{
				mail("alen@alpha-male.tv","Message Too Long: ".$listing->id,"The message ".$listing->id.": '".$listing->message."' is too long");
				mail("shaun@shaunnilsson.com","Message Too Long: ".$listing->id,"The message ".$listing->id.": '".$listing->message."' is too long");
			}


			return true;
	} *************************************************/


	//Process the hidden return from the Paypal Express Checkout
	public function ipn()
	{
		info('We got this from paypal: ' . print_r($_REQUEST, true) . print_r($_POST, true));


		// Setup class
		require_once('paypal_class.php');  // include the class file
		$paypal_response = new PaypalResponse;			 // initiate an instance of the class
		$paypal_response->paypal_url = PAYPAL_URL;   // testing paypal url
		//$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';	 // paypal url

		// setup a variable for this script (ie: 'http://www.micahcarrick.com/paypal.php')
		//$this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];



		// Paypal is calling page for IPN validation...

		// It's important to remember that paypal calling this script.  There
		// is no output here.  This is where you validate the IPN data and if it's
		// valid, update your database to signify that the user has payed.  If
		// you try and use an echo or printf function here it's not going to do you
		// a bit of good.  This is on the "backend".  That is why, by default, the
		// class logs all IPN data to a text file.

		if ($paypal_response->validate_ipn())
		{
			$confirmed = false;

			// Payment has been recieved and IPN is verified.  This is where you
			// update your database to activate or process the order, or setup
			// the database with the user's order details, email an administrator,
			// etc.  You can access a slew of information via the ipn_data() array.

			// Check the paypal documentation for specifics on what information
			// is available in the IPN POST variables.  Basically, all the POST vars
			// which paypal sends, which we send back for validation, are now stored
			// in the ipn_data() array.

			// For this example, we'll just email ourselves ALL the data.
			$subject = COMPANY_NAME . ' - Instant Payment Notification - Recieved Payment';
			$to = PAYPAL_EMAIL;	//  your email
			$body =  "An instant payment notification was successfully recieved\n";
			$body .= "from ".$paypal_response->ipn_data['payer_email']." on ".date('m/d/Y');
			$body .= " at ".date('g:i A')."\n\nDetails:\n";

			$body .= $paypal_response->ipn_data['payment_status']."\n\n";
			$body .= $paypal_response->ipn_data['item_number']."\n\n";
			$body .= $paypal_response->ipn_data['txn_id']."\n\n";

			$body .= print_r($paypal_response->ipn_data, true);


			mail("alen@alpha-male.tv, shaun@shaunnilsson.com", "PAID BLAST RECEIVED", "You have received a paid blast worth: " .AMOUNT);
		}
		else
		{
			$subject = COMPANY_NAME . " - Invalid IPN";
			foreach ($p->ipn_data as $key => $value) { $body .= "\n$key: $value"; }
		}


		mail(PAYPAL_EMAIL, $subject, $body);

	}

	private function _sanitizeImagePath($path)
	{
		// Remove double dates where saved
		if (preg_match("/\/images\/listings\/\d+\/\d+\/(\/images.*$)/", $path, $matches))
		{
			$path = $matches[1];
		}

		return str_replace(" ", "_", trim($path));
	}

	/****************************
	private function getImageNameFromLog($upload_id,$fieldid)
	{
			$logFile	=	dirname(__FILE__).'/../../log/fileuploads_'.$upload_id.'_'.$fieldid.'.log';

			$imageName	=   '';
			if(file_exists($logFile))
			{
			$imageName  =	file_get_contents($logFile);
			@unlink($logFile);
			}

			return $imageName;
	} ***************************/


}
?>
