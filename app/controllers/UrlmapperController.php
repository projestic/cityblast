<?php
include_once APPLICATION_PATH . "/../lib/google-analytics/src/autoload.php";

use UnitedPrototype\GoogleAnalytics;

class UrlmapperController extends ApplicationController 
{

	protected static $acl = array(
		'*' => 'public'
	);
		
	function index()
	{
		$member_id = $this->_getParam('mid');
		$listing_id = $this->_getParam('listing_id');
		$this->_helper->layout->setLayout('nolayout');

		//fetch url based on listing_id from url mapping tables and redirecr
		$row    = UrlMapping::find_by_listing_id($listing_id);
		$member = Member::find_by_id($member_id);
		
		if($listing_id)
		{
			$click = new Click();
			$click->listing_id = $listing_id;
			$click->member_id = $member_id;
			$click->url = $row->url;
			$click->date = date('Y-m-d H:i');
			$click->count = 1;
			
			if(isset($_SERVER['HTTP_USER_AGENT'])) $click->source = $_SERVER['HTTP_USER_AGENT'];
		
			if(!stristr($_SERVER['HTTP_USER_AGENT'], 'facebook') && $_SERVER['HTTP_USER_AGENT'] != 'robot')
			{
				$click->save();
			}
		}

		$original = '/v/i/'.$listing_id;
		if(!empty($member_id)) 
			$original .= '/'.$member_id;		

		$redirect_url = $row->url;
		if ($member) $redirect_url = str_replace('[useremail]', $member->email, $row->url);

		$this->view->redirect_url = $redirect_url;		
	}
	
	function newindex()
	{
		$this->_helper->layout->setLayout('nolayout');
		 
		$this->view->member_id 		= $member_id 		= $this->_getParam('mid');
		$this->view->click_source 	= $click_source 	= $this->_getParam('click_source');
		$this->view->listing_id 		= $listing_id 		= $this->_getParam('listing_id');

		//fetch url based on listing_id from url mapping tables and redirecr
		try {
			$row = UrlMapping::find_by_listing_id($listing_id);
		} catch(ActiveRecord\RecordNotFound $e) {
			$row = null;
		}
		
		if($listing_id)
		{
			$click = new Click();
			$click->listing_id = $listing_id;
			$click->member_id = $member_id;
			$click->url = ($row) ? $row->url : null;
			$click->date = date('Y-m-d H:i');
			$click->count = 1;
			
			if($click_source == "fb") $click->page_type = "facebook";
			if($click_source == "fp") $click->page_type = "fanpage";
			if($click_source == "tw") $click->page_type = "twitter";
			if($click_source == "ln") $click->page_type = "linkedin";
			
			if(isset($_SERVER['HTTP_USER_AGENT'])) $click->source = $_SERVER['HTTP_USER_AGENT'];
		
			if(!stristr($_SERVER['HTTP_USER_AGENT'], 'facebook') && $_SERVER['HTTP_USER_AGENT'] != 'robot')
			{
				$click->save();
			}
		}
		
		$url = ($row) ? $row->url : '/listing';

		try {
			$listing = Listing::find($listing_id);
		} catch(ActiveRecord\RecordNotFound $e) {
			$listing = null;
		}
		
		if($listing && $listing->email_passthru && !empty($member_id))
		{
			$member = Member::find($member_id);
			
			if($member)
			{
				$member_email = $member->email;
				if(!empty($member->email_override)) $member_email = $member->email_override;
				$query = "email=". urlencode($member_email);
				$separator = (parse_url($url, PHP_URL_QUERY) == NULL) ? '?' : '&';
				$url .= $separator . $query;
			}
		}
   
		$this->view->redirect_url = $url;

		$this->render("index");
	}
}