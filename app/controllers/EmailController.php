<?
class EmailController extends ApplicationController
{	
	CONST AMOUNT	= "49.00";
	CONST TAXES		= "0.13"; // 13%
	CONST CURRENCY	= "CAD";
	private $_email = '';
	
	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		if(!isset($_SESSION['member']))
		{ 
			$_SESSION['member'] = $this->getTestMember();
		}

		$this->view->member = $_SESSION['member'];
		$this->view->blast_reach = 3446;
		$this->view->title = COMPANY_WEBSITE;
		$this->_email = $this->_request->getParam('email_friend');
		parent::init();
	}
	
	
	public function index(){}
	
	public function newsletter()
	{
		$member = $this->getTestMember();

		
		$params	=   array(
			'member'	=>  $member,
		);

		$htmlBody      =   $this->view->partial('email/newsletter-email.html', $params);

		echo $htmlBody;
		exit();
	}
	
	
	public function expired()
	{
		$member = $this->getTestMember();

		
		$params	=   array(
			'member'	=>  $member,
		);

		$htmlBody      =   $this->view->partial('email/expiredtokenfound.html.php', $params);

		echo $htmlBody;
		exit();
	}
	

	public function expiredcc()
	{
		$member = $this->getTestMember();

		
		$params	=   array(
			'member'	=>  $member,
		);

		$htmlBody      =   $this->view->partial('email/creditcardexpiring.html.php', $params);

		echo $htmlBody;
		exit();
	}
	

	public function trial()
	{
		$member = $this->getTestMember();

		
		$params	=   array(
			'member'	=>  $member,
		);

		$htmlBody      =   $this->view->partial('email/trial.html.php', $params);

		echo $htmlBody;
		exit();
	}

	function expiring()
	{
		$member = $this->getTestMember();

		
		$params	=   array(
			'member'	=>  $member,
		);

		$htmlBody      =   $this->view->partial('email/tokenexpiring.html.php', $params);

		echo $htmlBody;

		
		//$member->tokenExpiringNotify();
		
		exit();		
	}

	function activate()
	{
		$member = $this->getTestMember();
		
		$taxes = "0";		
		if(	strtoupper($member->default_city->country) == "CANADA")
		{
			$taxes = "0.13";	
		}
		
		
		$params =	array(
					'member'	=>	$member,
					'amount'		=>	$member->price,
					'taxes'		=>	$taxes,
					'title'		=>	$DESCRIPTION
				    );
		
		$htmlBody       =   $this->view->partial('email/activate.account.html.php', $params);

		echo $htmlBody;
		exit();
	}
	
	function mandrill_invoice()
	{
		$DESCRIPTION = "Monthly ".COMPANY_NAME." Invoice";

		$view = new Zend_View();
		$env = "cityblast";
		if(APPLICATION_ENV == "alenmortgagemingler" || APPLICATION_ENV == "mortgagemingler") $env = "mortgagemingler";
		$view->setBasePath( dirname(__FILE__)."/../views/".$env."/");
		

		if(!isset($_SESSION['member']))
		{ 
			if(stristr(APPLICATION_ENV, "myeventvoice")) $member = Member::find(1);
			else $member = $_SESSION['member'] = Member::find(35);
		}
		
		if(!isset($member)) $member = $_SESSION['member'];


		$invoice_number = 3;
		$taxes = "0.13";
		$amount = "34.99";

		$params = array(
			'member' => $member,
			'order_number' => 29,
			'amount' => $member->price,
			'taxes' => $taxes,
			'title' => $DESCRIPTION,
			'invoice_number' => $invoice_number
		);

		$htmlBody = $this->view->partial('email/clientfinder-invoice.html.php', $params);

		echo $htmlBody;
		exit();
	}

	function cfinvoice()
	{

		$DESCRIPTION = "Monthly ".COMPANY_NAME." Invoice";

		$member = $this->getTestMember();

//echo "FID:".$member->frequency_id . "<BR><BR>";
	

		$invoice_number = 3;
		$taxes  		 = "0.13";
		$amount  		 = "34.99";


		
		$params =	array(
					'member'		=> $member,
					'order_number'	=> 29,
					'amount'		=> $member->price,
					'taxes'		=> $taxes,
					'title'		=> $DESCRIPTION,
					'invoice_number' => $invoice_number
				    );


		
		$htmlBody       =   $this->view->partial('email/clientfinder-invoice.html.php', $params);

		echo $htmlBody;
		exit();

		
	}
	
	function networksdisabled()
	{
		$member = $this->getTestMember();
		
		
		$networks = array('Facebook', 'LinkedIn', 'Twitter');
		$params =	array(
			'member' =>	$member,
			'networks' => $networks
		);
		
		$htmlBody = $this->view->partial('email/networksdisabled.html.php', $params);

		echo $htmlBody;
		exit();
	}
	
	function failed()
	{

		$member = $this->getTestMember();
		
		$params =	array(
			'member' =>	$member,
		);
		
		$htmlBody = $this->view->partial('email/failed_payment.html.php', $params);

		echo $htmlBody;
		exit();
	}

/*
	function failed1()
	{
		$DESCRIPTION = "Monthly ".COMPANY_NAME." Invoice";
		
		$member = $this->getTestMember();
		
		$taxes = "0";		
		if(	strtoupper($member->default_city->country) == "CANADA")
		{
			$taxes = "0.13";	
		}
		
		
		$params =	array(
					'member'	=>	$member,
					'amount'		=>	$member->price,
					'taxes'		=>	$taxes,
					'title'		=>	$DESCRIPTION
				    );
		
		$htmlBody       =   $this->view->partial('email/failed_payment_day1.html.php', $params);

		echo $htmlBody;
		exit();
	}


	function failed2()
	{
		$DESCRIPTION = "Monthly ".COMPANY_NAME." Invoice";
		
		$member = $this->getTestMember();
		
		$taxes = "0";		
		if(	strtoupper($member->default_city->country) == "CANADA")
		{
			$taxes = "0.13";	
		}
		
		
		$params =	array(
					'member'	=>	$member,
					'amount'		=>	$member->price,
					'taxes'		=>	$taxes,
					'title'		=>	$DESCRIPTION
				    );
		
		$htmlBody       =   $this->view->partial('email/failed_payment_day2.html.php', $params);

		echo $htmlBody;
		exit();
	}	


	function failed3()
	{
		$DESCRIPTION = "Monthly ".COMPANY_NAME." Invoice";

		//$view = new Zend_View();
		//$env = "cityblast";
		//if(APPLICATION_ENV == "alenmortgagemingler" || APPLICATION_ENV == "mortgagemingler") $env = "mortgagemingler";
		//$view->setBasePath( dirname(__FILE__)."/../views/".$env."/");
		
		$member = $this->getTestMember();
		
		$taxes = "0";		
		if(	strtoupper($member->default_city->country) == "CANADA")
		{
			$taxes = "0.13";	
		}
		
		
		$params =	array(
					'member'	=>	$member,
					'amount'		=>	$member->price,
					'taxes'		=>	$taxes,
					'title'		=>	$DESCRIPTION
				    );
		
		$htmlBody       =   $this->view->partial('email/failed_payment_day3.html.php', $params);

		echo $htmlBody;
		exit();
	}	
*/

	public function comment()
	{
		$listing = Listing::find(120);
	
		$this->view->member = $member = Member::find(1001704);
	
		$comment                      = new Comment();
		$comment->fb_comment_id       =	12345;
		$comment->fb_uid              =	111;
		$comment->fb_name             =	"bob";
		$comment->fb_admin_member_id  =	intval($member->id);
		$comment->text                =	"This is some sample text";
		$comment->mid                 =	1234;
		$comment->listing_id          =	intval($listing->id);
	
		$params	=   array();
		$params['from_name']	= "Bob";
		$params['from_email']	= "bob@somewhere.com";
		
		$emails = explode(',', $this->_email);
		
		$params['from_email']	= array_shift($emails);
		$params['listing']		= $listing;
		$params['member']		= $member;
		$params['comment']		= $comment;
	
		$htmlBody = $this->view->partial('email/comment-created.html.php', $params);
	

		echo $htmlBody;
		exit();
		
		/*****	
	
		$email = new ZendMailLogger("COMMENT", $member->id);
		$email->addTo($params['from_email']);
		$email->setSubject("You've received a new comment.");
		$email->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
		$email->setFrom("test@cityblast.com");
	
		$viewBody = array();
		try {
			$email->sendemail();
			$viewBody[] = array(
			'body'  => $htmlBody,
			'email' => $params['from_email']
			);
		} catch(exception $e) {
			//echo $e->getMessage();
		}
	
		$this->view->htmlBody = $viewBody;
		$this->_helper->layout()->disableLayout();
		$this->render('bottom');
		
		****/
	}
	    
	public function referral()
	{
		/**************************
		$params["email_friend"] = "alen@alpha-male.tv, alen_bubic@yahoo.com";
        	$params['email_friend'] = $this->_email;
        
		$emails = explode(",",$params["email_friend"]);
		
        	$viewBody = array();
        	foreach($emails as $email)		
		{
			$email = trim($email);
			//Only email VALIDATED emails...
			if( FALSE !== filter_var($email, FILTER_VALIDATE_EMAIL) ) 
			{
				$mail = new ZendMailLogger("REFERRAL");
				$mail->addTo($email);
                
				if(isset($_SESSION['member']))
				{					
					$from_name = $_SESSION['member']->first_name . " " . $_SESSION['member']->last_name;					
					$mail->setSubject('A Personal Invitation from '.$from_name.' to Join CityBlast. 100% Free. No Fees. No Ads. No Gimmicks.');
					$member_id = $_SESSION['member']->id;
				}
				else
				{ 
					$mail->setSubject('An Invitation to Join City-Blast. 100% Free. No Fees. No Ads. No Gimmicks.');
					$from_name = "Your friends at City-Blast.com";
					$member_id = null;
				}
                
				if(isset($_SESSION['member'])) $from_email = $_SESSION['member']->email;
				else $from_email = "info@cityblast.com";
				
                	$params['from_name'] = $from_name;
				$params['member_id'] = $member_id;
				$params['member'] 	 = $this->view->member;
				$params['total_count'] 	= $this->view->total_count;
				
                	$htmlBody	=   $this->view->partial('email/referral.html.php', $params);
				
                	$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
				$mail->setFrom($from_email, $from_name);
				
                	if(isset($from_name) && isset($from_email)) $mail->setReplyTo( $from_email, $from_name);
				
                	$sentEmail = $mail->sendemail();
                
                	$viewBody[] = array(
                    	'body'  => $htmlBody,
                    	'email' => $email
                	);
			}
		}

        	
        	$this->view->htmlBody = $viewBody;
        	$this->_helper->layout()->disableLayout();
        	$this->render('bottom');
        	***********************************/
 
 		//echo "<h1>HELLO WORLD</h1>";


		//$view = new Zend_View();
		//$env = "cityblast";
		//if(APPLICATION_ENV == "alenmortgagemingler" || APPLICATION_ENV == "mortgagemingler") $env = "mortgagemingler";
		//$view->setBasePath( dirname(__FILE__)."/../views/".$env."/");
 
 		$member = $this->getTestMember();

		
		$params	=   array(
			'member'	=>  $member,
		);
		       	
        
        	$htmlBody       =   $this->view->partial('email/referral.html.php', $params);

		echo $htmlBody;
		exit();
	}	


	public function selfreferral()
	{
 		$member = $this->getTestMember();

		
		$params	=   array(
			'member'	=>  $member,
		);
		       	
        
        	$htmlBody       =   $this->view->partial('email/self-referral.html.php', $params);

		echo $htmlBody;
		exit();
	}	


	public function referralpreview()
	{
		$member = $this->getTestMember();

		
		$params	=   array(
			'member'	=>  $member,
		);

		$htmlBody      =   $this->view->partial('email/referral.html.php', $params);

		echo $htmlBody;
		exit();
	}

	public function drip1()
	{
		$member = $this->getTestMember(); 
		
		
		$params	=   array(
		'member'	=>  $member,
		);

		$from_name = COMPANY_WEBSITE;
		$from_email = HELP_EMAIL;


		$drip = $this->getDripCampaignTrial();
		echo "<h1>Subject: ".$drip->subjects[0]."</h1><br/><br/>";

		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);
        
		$htmlBody = $this->view->partial('email/drip/trial/1.html.php', $params);
		
		echo $htmlBody;
		
		
		exit();
	}

	public function drip2()
	{
		$member = $this->getTestMember();

		
		$params	=   array(
			'member'	=>  $member,
		);
		
        $from_name = COMPANY_WEBSITE;
		$from_email = HELP_EMAIL;
        
		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);


		$drip = $this->getDripCampaignTrial();
		echo "<h1>Subject: ".$drip->subjects[1]."</h1><br/><br/>";


		$htmlBody = $this->view->partial('email/drip/trial/2.html.php', $params);
		
		echo $htmlBody;
		
		
		exit();
	}


	public function drip3()
	{
		$member = $this->getTestMember();
		
		
		$params	=   array(
			'member'	=>  $member,
		);
		
		$from_name = COMPANY_WEBSITE;
		$from_email = HELP_EMAIL;

		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);

		$drip = $this->getDripCampaignTrial();
		echo "<h1>Subject: ".$drip->subjects[2]."</h1><br/><br/>";


		$htmlBody = $this->view->partial('email/drip/trial/3.html.php', $params);
		
		echo $htmlBody;
		
		
		exit();
	}



	public function drip4()
	{
		$member = $this->getTestMember();
		
		
		$params	=   array(
			'member'	=>  $member,
		);
		
		$from_name = COMPANY_WEBSITE;
		$from_email = HELP_EMAIL;

		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);

		$drip = $this->getDripCampaignTrial();
		echo "<h1>Subject: ".$drip->subjects[3]."</h1><br/><br/>";


		$htmlBody = $this->view->partial('email/drip/trial/4.html.php', $params);
		
		echo $htmlBody;
		
		
		exit();
	}


	public function drip5()
	{
		$member = $this->getTestMember();
		
		
		$params	=   array(
			'member'	=>  $member,
		);
		
		$from_name = COMPANY_WEBSITE;
		$from_email = HELP_EMAIL;

		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);

		$drip = $this->getDripCampaignTrial();
		echo "<h1>Subject: ".$drip->subjects[4]."</h1><br/><br/>";


		$htmlBody = $this->view->partial('email/drip/trial/5.html.php', $params);
		
		echo $htmlBody;
		
		
		exit();
	}



	public function drip6()
	{
		$member = $this->getTestMember();
		
		
		$params	=   array(
		'member'	=>  $member,
		);

		$from_name = COMPANY_WEBSITE;
		$from_email = HELP_EMAIL;

		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);

		$drip = $this->getDripCampaignTrial();
		echo "<h1>Subject: ".$drip->subjects[5]."</h1><br/><br/>";


		$htmlBody = $this->view->partial('email/drip/trial/6.html.php', $params);
		
		echo $htmlBody;
		
		
		exit();
	}



	public function drip7()
	{
		$member = $this->getTestMember(); 
		
		
		$params	=   array(
		'member'	=>  $member,
		);

		$from_name = COMPANY_WEBSITE;
		$from_email = HELP_EMAIL;

		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);

		$drip = $this->getDripCampaignTrial();
		echo "<h1>Subject: ".$drip->subjects[6]."</h1><br/><br/>";


		$htmlBody = $this->view->partial('email/drip/trial/7.html.php', $params);
		
		echo $htmlBody;
		
		
		exit();
	}
	
	public function welcome()
	{
		$member = $this->view->member;
		
		$params	=   array(
			'member'	=>  $member,
		);

		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);
        
		$htmlBody = $this->view->partial('email/welcome.html.php', $params);
		
		echo $htmlBody;
		exit();
	} 
	
    
	public function postpublish()
	{
        	$listing = Listing::find(120);
		
        	$params =	array(
			'to_name'        =>	$listing->member->last_name.",".$listing->member->first_name,
			'message'        =>	$listing->raw_message,
			'listing_id'     =>	$listing->id,
			'listing'        =>	$listing,
			'listing_url'    =>	APP_URL.'/member/blast/'.$listing->id,
			'member'         => $listing->member,
		);
        
		
		

        
        	$htmlBody = $this->view->partial('email/postpublish.html.php', $params);
		

		
		echo $htmlBody;
		exit();

		/*******************************
		$viewBody = array();
		try {
			$email->addTo($emailTo);
			$email->sendemail();
			$viewBody[] = array(
			'body'  => $htmlBody,
			'email' => $emailTo
			);
		} catch(exception $e) {
			//echo $e->getMessage();
		}
		
		$this->view->htmlBody = $viewBody;
		$this->_helper->layout()->disableLayout();
		$this->render('bottom');
		*****************************/
	}
    


    
	public function invalidtokenfound()
	{
		$member = $this->view->member;
	
		$params =	array(
			'member'      => $member,
			'network_name' => 'Facebook'
		);
	
		$htmlBody = $this->view->partial ( 'email/invalidtokenfound.html.php', $params );
	
		echo $htmlBody;
		exit();
	}
	    
	public function agentsharelisting()
	{
		$params = array();

		$member = $this->getTestMember();
		
		
		$params	=   array(
			'member'	=>  $member,
		);
		
		$params["email_friend"] = "eric@pragmum.com";
		$params['email_friend'] = $this->_email;
		$params['listing_id'] = 120;
	

	
		$emails = explode(",",$params["email_friend"]);
		$this->view->listing = Listing::find($params['listing_id']);
	
		$params['listing']	= Listing::find_by_id($params['listing_id']);
		$params['recent_blasts']= $this->getRecentBlasts(4);


		$htmlBody	=  $this->view->partial('email/agent-share-listing.html.php', $params);
		echo $htmlBody;
		exit();
		


		$viewBody = array();
		foreach($emails as $email)
		{
			$email = trim($email);
			if( FALSE !== filter_var($email, FILTER_VALIDATE_EMAIL) )
			{
				$this->view->to_email	=   $email;
				if(isset($_SESSION['member']))
				{
					$from_name = $_SESSION['member']->first_name . " " . $_SESSION['member']->last_name;
					$member_id = $_SESSION['member']->id;
				}
				else
				{
					$from_name = "Your friends";
					$member_id = null;
				}
				if(isset($_SESSION['member']))
				$from_email = $_SESSION['member']->email;
				else $from_email = HELP_EMAIL;
	
				$params['from_name'] = $from_name;
				$params['member_id'] = $member_id;
	
				$htmlBody	=  $this->view->partial('email/agent-share-listing.html.php', $params);
	
				$mail = new ZendMailLogger("LISTING_EMAIL", $member_id);
				$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
				$mail->setFrom($from_email, $from_name);
				$mail->addTo($email);
	
				if(isset($from_name) && isset($from_email)) $mail->setReplyTo( $from_email, $from_name);
	
				$mail->setSubject('Your new listing on ' . COMPANY_WEBSITE);
	
				$sentEmail = $mail->sendemail();
	
				$viewBody[] = array(
				'email' => $email,
				'body'  => $htmlBody
				);
			}
			else
			{
				$msg = "INVALID Share Listing email ";
				if(isset($_SESSION['member']->email))
				$msg .= "sent by member: ". $_SESSION['member']->email;
				if(isset($email))
				$msg .= " sent to: ".$email;
				mail("alen@alpha-male.tv", "INVALID: Referral Email" . $email, $msg);
			}
		}
	
		$this->view->htmlBody = $viewBody;
	
		$this->_helper->layout()->disableLayout();
		$this->render('bottom');
	}
	
	public function sharelisting()
	{
		$params = array();
		$params["email_friend"] = "alen_bubic@yahoo.com, alen@alpha-male.tv";
		$params['email_friend'] = $this->_email;
		$params['listing_id'] = 120;
	
		$emails = explode(",",$params["email_friend"]);
		$this->view->listing = Listing::find($params['listing_id']);
	
		$params['listing']	= Listing::find_by_id($params['listing_id']);
		$params['recent_blasts']= $this->getRecentBlasts(4);
	
		$params['comments'] = "This is something random the person had to say about the listing";
		
	$htmlBody	=  $this->view->partial('email/share-listing.html.php', $params);
	echo $htmlBody;
	exit();
	
	
	
		$viewBody = array();
		foreach($emails as $email)
		{
			$email = trim($email);
	
			if( FALSE !== filter_var($email, FILTER_VALIDATE_EMAIL) )
			{
				$this->view->to_email	=   $email;
				if(isset($_SESSION['member']))
				{
					$from_name = $_SESSION['member']->first_name . " " . $_SESSION['member']->last_name;
					$member_id = $_SESSION['member']->id;
				}
				else
				{
					$from_name = "Your friends";
					$member_id = null;
				}
	
				if(isset($_SESSION['member']))
				$from_email = $_SESSION['member']->email;
				else $from_email = HELP_EMAIL;
	
				$params['from_name'] = $from_name;
				$params['member_id'] = $member_id;
	
				$htmlBody	=  $this->view->partial('email/share-listing.html.php', $params);
	
				$mail = new ZendMailLogger("LISTING_EMAIL", $member_id);
				$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
				$mail->setFrom($from_email, $from_name);
				$mail->addTo($email);
	
				if(isset($from_name) && isset($from_email)) $mail->setReplyTo( $from_email, $from_name);
	
				$mail->setSubject($from_name.' thought you might like this listing on '.COMPANY_WEBSITE.'.');
	
				$sentEmail = $mail->sendemail();
	
				$viewBody[] = array(
				'email' => $email,
				'body'  => $htmlBody
				);
			}
			else
			{
				$msg = "INVALID Share Listing email ";
				if(isset($_SESSION['member']->email))
				$msg .= "sent by member: ". $_SESSION['member']->email;
				if(isset($email))
				$msg .= " sent to: ".$email;
				mail("alen@alpha-male.tv", "INVALID: Referral Email" . $email, $msg);
			}
		}
	
		$this->view->htmlBody = $viewBody;
	
		$this->_helper->layout()->disableLayout();
		$this->render('bottom');
	}
	
	public function bookingconfirm()
	{
		$member = $this->view->member;
	
		$params['booking_agent'] = $booking_agent = $this->view->booking_agent = $_SESSION['member'];
	
		$listing 		= Listing::find_by_id(120);
	
		//Send the BOOKING AGENT email here
        	$from_name = COMPANY_WEBSITE;
		$from_email = HELP_EMAIL;
	
		$params['from_name']      = $from_name;
		$params['name']           = "Bob";
		$params['booking_agent']  = $_SESSION['member'];
		$params['listing']        = $listing;
		$params['contact_by']     = "EMAIL";
		$params['message']        = "I really love this place!";
		$params['best_time']      = "Tuesday at 2pm";
		$params['listing_agent']  = $listing->member;
		$params['phone']          = "555-555-5555";
		$params['email']          = "bob@somewhere.com";
	
		// Requester email
		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);
	
		$htmlBody       =   $this->view->partial('email/booking-confirmation.html.php', $params);
	
		echo $htmlBody;
		exit();

	}
	
	public function booking()
	{
		$member = $this->view->member;
		$params['booking_agent'] = $booking_agent = $this->view->booking_agent = $_SESSION['member'];
	
		$listing 		= Listing::find_by_id(766);
	
		//Send the BOOKING AGENT email here
        	$from_name = COMPANY_WEBSITE;
		$from_email = HELP_EMAIL;
	
		$params['from_name']      = $from_name;
		$params['name']           = "Bob";
		$params['booking_agent']  = $_SESSION['member'];
		$params['listing']        = $listing;
		$params['contact_by']     = "EMAIL";
		$params['message']        = "I really love this place!";
		$params['best_time']      = "Tuesday at 2pm";
		$params['listing_agent']  = $listing->member;
		$params['phone']          = "555-555-5555";
		$params['email']          = "bob@somewhere.com";
		
		//Member 146 == CityBlast Content
		if(isset($listing->member_id) && !empty($listing->member_id) && $listing->member_id != 146)
		{
			if(isset($listing->member->email_override) && !empty($listing->member->email_override)) $email = $listing->member->email_override;
			else $email = $listing->member->email;
			
			$params['listing_agent_name'] 	= $listing->member->first_name . " " . $listing->member->last_name;
			$params['listing_agent_email'] 	= $email;
			$params['listing_agent_brokerage'] = $listing->member->brokerage;
			$params['listing_agent_phone'] 	= $listing->member->phone;
			
			
		} 
		else
		{
			$params['listing_agent_name'] 	= $listing->list_agent;
			$params['listing_agent_email'] 	= "";
			$params['listing_agent_brokerage'] = $listing->broker_of_record;
			$params['listing_agent_phone'] 	= $listing->list_agent_phone;	
		}

	
		$emails  = explode(',', $this->_email);
		$emailTo = array_shift($emails);
	
		$htmlBody       =   $this->view->partial('email/booking.html.php', $params);
	
		$mail = new Zend_Mail('utf-8');
		$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
		$mail->setFrom($from_email, $from_name);
		$mail->setSubject("Cha-Ching! You've Got A New Lead from ".COMPANY_NAME.".");


		echo $htmlBody;
		exit();
		
		
		
		/***************************
		$viewBody = array();
		try {
			$mail->addTo($emailTo);
			$sentEmail = $mail->send();
			$viewBody[] = array(
			'body'  => $htmlBody,
			'email' => $emailTo
			);
		} catch(exception $e) {
			//echo $e->getMessage();
		}
	
		$this->view->htmlBody = $viewBody;
		$this->_helper->layout()->disableLayout();
		$this->render('bottom');
		***************************/
	}
	
	protected function getTestMember()
	{
		return Member::find_by_type_id(5);
	}
	
	protected function getDripCampaignTrial()
	{
		$result = null;
		if (strstr($_SERVER['HTTP_HOST'], 'cityblast')) {
			$result = new Blastit_Email_DripCampaign_Trial();
		} elseif (strstr($_SERVER['HTTP_HOST'], 'myeventvoice')) {
			$result = new Blastit_Email_DripCampaign_TrialMyEventVoice();
		}
		return $result;
	}
}