<?php
class ExternalcontentController extends ApplicationController
{
	protected static $acl = array(
		'approve' 			=> array('odesk', 'assistanteditor'),
		'edit' 				=> array('odesk', 'assistanteditor'),
		'blogs' 			=> array('odesk', 'assistanteditor'),
		'blogedit' 			=> array('odesk', 'assistanteditor'),
		'blogsave' 			=> array('odesk', 'assistanteditor'),
		'comp' 				=> 'superadmin',
		'republish' 		=> 'superadmin',
		'moveup' 			=> 'superadmin',
		'movedown' 			=> 'superadmin',
		'delete' 			=> 'superadmin',
		'undelete' 			=> 'superadmin',
		'removefromqueue' 	=> 'superadmin',
		'tag' 				=> 'superadmin',
		'*' 				=> 'public'
	);
	
	public function init() {
		$this->_helper->layout->setLayout('admin');
		parent::init();
	}

	public function index()
	{

		$request = $this->getRequest();
		$this->view->page = $page = $request->getParam('page', 1);
		$this->view->sortField = $sortField = $request->getParam('sort', 'id');
		$this->view->sortOrder = $sortOrder = $request->getParam('order', 'desc');
		$this->view->filterTier = $filterTier = $request->getParam('filterTier');

		$delete = $request->getParam('delete', array());
		
		if (count($delete)) {
			$delete = array_filter($delete, function ($value) { return intval($value); } );
			$statement1 = BlogPending::connection()->query('DELETE FROM blog_pending WHERE id IN (' . implode(',', $delete) . ')');
			$statement2 = BlogPending::connection()->query('DELETE FROM blog_image_pending WHERE blog_entry_id IN (' . implode(',', $delete) . ')');
			$statement1->execute();
			$statement2->execute();
		}

		$options = array();
		$options['joins'] = 'LEFT JOIN blog ON blog.id = blog_pending.blog_id';
		$optionsSortOrder = $this->indexSortOrder($sortField, $sortOrder);
		$optionsFilter = $this->indexFilter($filterTier);
		if ($optionsSortOrder) {
			$options['order'] = $optionsSortOrder['order'];
		}
		if ($optionsFilter) {
			$options['conditions'] = $optionsFilter['conditions'];
		}
		else {
			$options['conditions'] = '1 = 1';
		}

		$this->view->blogEntriesPaginator = BlogPending::getPaginator(40, $page, $options);

		$message = new Zend_Session_Namespace('messages');
		if ($message->data) {

			$data = $message->data;
			if (isset($data->lastAction)) {
				if ($data->lastAction == 'approved')
				{
					$this->view->lastAction = $data->lastAction;
					$this->view->blogEntryTitle = $data->blogEntryTitle;
					$this->view->listingId = $data->listingId;
					$this->view->errors = $data->errors;
				}
				$message->data = null;
			}
		}
	}

	protected function indexSortOrder($sortField, $sortOrder) {
		if (!$sortField) {
			return null;
		}
		$sortFieldFormatted = null;
		if ($sortField == 'tier') {
			$sortFieldFormatted = 'blog.tier';
		}
		else
		if ($sortField == 'blog_name') {
			$sortFieldFormatted = 'blog.name';
		}
		else {
			$sortFieldFormatted = $sortField;
		}
		$orderSql = $sortFieldFormatted . ' ' . $sortOrder;
		$options = array('order' => $orderSql);
		return $options;
	}

	protected function indexFilter($filterTier) {
		if (!$filterTier) {
			return null;
		}
		$conditionsSql = 'blog.tier = ' . "'" . $filterTier . "'";
		$options = array('conditions' => $conditionsSql);
		return $options;
	}

	public function edit()
	{
		$request = $this->getRequest();
		
		$id = $request->getParam('id');
		
		$blogEntry = BlogPending::find($id);

		if ($request->isPost()) {
		
			$id = $request->getParam('id');
			$op = $request->getParam('op');
		
			if ($op == 'Approve') {
			
				$imageIds = $request->getParam('images');

				$copier = new Blastit_BlogExternal_CopyToListing_BlogCopyToListing();
				$listing = $copier->copy($blogEntry, 'CONTENT', $imageIds, $_SESSION['member']->id);
				//$listing->approved_by_id = $_SESSION['member']->id;
				//$listing->approved_at = date($dateformatMysql);

				$urlMapping = new UrlMapping();
				$urlMapping->listing_id = $listing->id;
				$urlMapping->url = $listing->content_link;
				$urlMapping->save();

				// del source blog entry
				$blogEntry->delete();

				$message = new Zend_Session_Namespace('messages');
				if (!isset($message->data)) {
					$message->data = new stdClass();
				}
				$message->data->blogEntryTitle = $blogEntry->title;
				$message->data->lastAction = 'approved';
				$message->data->listingId =	$listing->id;
				$message->data->errors = $copier->getErrors();
				
			} elseif ($op == 'Delete') {

				foreach ($blogEntry->images as $image) {
					$image->delete();
				}

				$blogEntry->delete();

				$message = new Zend_Session_Namespace('messages');
				$message->data = new stdclass;
				$message->data->lastAction = 'deleted';
				
			}
		
			$this->redirect("/externalcontent");
		}

		$this->view->blogEntry = $blogEntry;
		$this->view->blogImages = $blogEntry->images;

		if ($blogEntry->blog) {
			$this->view->blogSourceUrl = $blogEntry->blog->url;
		}

	}

	/**
	 * Lists all blogs.
	 */
	public function blogs() {
		$this->_helper->layout->setLayout('admin');

		$request = $this->getRequest();
		$page = $request->getParam('page', 1);

		$this->view->itemsPaginator = Blog::getPaginator(40, $page);

		$message = new Zend_Session_Namespace('messages');
		if ($message->data) {
			$data = $message->data;
			if (isset($data->lastAction))
			{
				$this->view->lastAction = $data->lastAction;
				$this->view->blogId = $data->blogId;
				$this->view->blogName = $data->blogName;
				$this->view->errors = $data->errors;
				$message->data = null;
			}
		}
	}

	/**
	 * Blog add/edit page.
	 */
	public function blogedit() {
		$this->_helper->layout->setLayout('admin');

		$request	= $this->getRequest();
		$id = $request->getParam('id');

		if ($id) {
			$this->view->blogItem = Blog::find($id);
		}
	}

	/**
	 * Blog save.
	 *
	 * @arg int|bool $seek_images_in_external_article
	 */
	public function blogeditsave() {
		$request = $this->getRequest();
		if ($request->isPost()) {
			$id	= $this->_request->getParam('id');
			$name = $request->getParam('name');
			$url = $request->getParam('url');
			$tier = $request->getParam('tier');
			$seek_images_in_external_article = $request->getParam('seek_images_in_external_article');
			$seek_images_in_external_article = !empty($seek_images_in_external_article) ? 1 : 0;

			$blog = Blog::find_by_id($id);

			if (!$blog) {
				$blog = new Blog;
			}

			$blog->name 		= $name;
			$blog->url 			= $url;
			$blog->tier 		= $tier;
			$blog->created_at 	= new DateTime;
			$blog->seek_images_in_external_article = $seek_images_in_external_article;

			$message = new Zend_Session_Namespace('messages');
			$message->data = new stdClass();

			try {
				$blog->save();

				if ($blog->id) {
					$last_action = 'updated';
				} else {
					$last_action = 'created';
				}
			} catch (\ActiveRecord\DatabaseException $e) {
				$last_action = 'error';
				if ($e->getCode() == 23000) {
					if (preg_match("/Duplicate entry \'(.+?)\' for key \'(.+?)\'/", $e->getMessage(), $matches)) {
						switch ($matches[2]) {
							case 'name_idx':
								$field = 'Name';
								break;
							case 'url_idx':
								$field = 'URL';
								break;
							default;
								$field = $matches[2];
								break;
						}
						$message->data->errors = array(sprintf('Duplicate entry (%s) for %s field', $matches[1], $field));
					}
				}
			}

			$message->data->lastAction = $last_action;
			$message->data->blogId = $blog->id;
			$message->data->blogName = $name;
		}

		$this->redirect("/externalcontent/blogs");
	}

	public function blogdelete() {
		$id	= $this->_request->getParam('id');

		$blog = Blog::find($id);

		$message = new Zend_Session_Namespace('messages');
		if (!isset($message->data)) {
			$message->data = new stdClass();
		}
		$message->data->lastAction = 'deleted';
		$message->data->blogId = $id;
		$message->data->blogName = $blog->name;

		$blog->delete();

		$this->redirect("/externalcontent/blogs");
	}

	public function download_blog_list()
	{
		$this->renderNothing();

		$filename = 'blog_list.csv';
		$columns = array(
			'ID',
			'Name',
			'URL',
			'Tier',
			'Seek Images',
			'Created At'
		);

		$list = Blog::all();
		$report = new Blastit_Report();
		$report->enableCSV();
		$report->setColumnTitles($columns);

		foreach ($list as $row) {
			$report->addRow(
				array(
					$row->id,
					$row->name,
					$row->url,
					$row->tier,
					$row->seek_images_in_external_article,
					$row->created_at->format('Y-m-d H:i:s')
				)
			);
		}

		$report->generate();

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false);
		header("Content-Transfer-Encoding: binary");
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-length: " . filesize($report->getCSVFile()));
		fpassthru(fopen($report->getCSVFile(), 'r'));
	}
}
