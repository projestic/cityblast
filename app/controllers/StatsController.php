<?php

class StatsController extends ApplicationController
{
	
	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
		$this->view->admin_selectedCity		= $this->getSelectedAdminCity();
	}
	
	function financiallatest()
	{
		$stats = array();
		
		// Actual Revenue
		$stats['actual_revenue_current_month'] = MemberStatsFinancial::getActualRevenueCurrentMonth();
		$stats['actual_revenue_current_annual'] = MemberStatsFinancial::getActualRevenueAnnual();
		
		// Projected Monthly revenue
		$stats['projected_revenue_paid_member_month'] = MemberStatsFinancial::getProjectedMonthlyRevenuePaidMember('month');
		$stats['projected_revenue_paid_member_biannual'] = MemberStatsFinancial::getProjectedMonthlyRevenuePaidMember('biannual');
		$stats['projected_revenue_paid_member_annual'] = MemberStatsFinancial::getProjectedMonthlyRevenuePaidMember('annual');
		$stats['projected_revenue_paid_member'] = MemberStatsFinancial::getProjectedMonthlyRevenuePaidMember();
		$stats['projected_revenue_cctrial_member_month'] = MemberStatsFinancial::getExpectedRevenueCurrentMonthCcTrialMember('month');
		$stats['projected_revenue_cctrial_member_biannual'] = MemberStatsFinancial::getExpectedRevenueCurrentMonthCcTrialMember('biannual');
		$stats['projected_revenue_cctrial_member_annual'] = MemberStatsFinancial::getExpectedRevenueCurrentMonthCcTrialMember('annual');
		$stats['projected_revenue_cctrial_member'] = MemberStatsFinancial::getExpectedRevenueCurrentMonthCcTrialMember();
		$stats['projected_revenue_month'] = MemberStatsFinancial::getProjectedMonthlyRevenue('month');
		$stats['projected_revenue_biannual'] = MemberStatsFinancial::getProjectedMonthlyRevenue('biannual');
		$stats['projected_revenue_annual'] = MemberStatsFinancial::getProjectedMonthlyRevenue('annual');
		$stats['projected_revenue'] = MemberStatsFinancial::getProjectedMonthlyRevenue();
		
		// Projected 30 day revenue
		$stats['projected_revenue_30_paid_member_month'] = MemberStatsFinancial::getProjectedRevenue30dayPaidMember('month');
		$stats['projected_revenue_30_paid_member_biannual'] = MemberStatsFinancial::getProjectedRevenue30dayPaidMember('biannual');
		$stats['projected_revenue_30_paid_member_annual'] = MemberStatsFinancial::getProjectedRevenue30dayPaidMember('annual');
		$stats['projected_revenue_30_paid_member'] = MemberStatsFinancial::getProjectedRevenue30dayPaidMember();
		$stats['projected_revenue_30_cctrial_member_month'] = MemberStatsFinancial::getExpectedRevenue30dayCcTrialMember('month');
		$stats['projected_revenue_30_cctrial_member_biannual'] = MemberStatsFinancial::getExpectedRevenue30dayCcTrialMember('biannual');
		$stats['projected_revenue_30_cctrial_member_annual'] = MemberStatsFinancial::getExpectedRevenue30dayCcTrialMember('annual');
		$stats['projected_revenue_30_cctrial_member'] = MemberStatsFinancial::getExpectedRevenue30dayCcTrialMember();
		$stats['projected_revenue_30_month'] = MemberStatsFinancial::getProjectedRevenue30day('month');
		$stats['projected_revenue_30_biannual'] = MemberStatsFinancial::getProjectedRevenue30day('biannual');
		$stats['projected_revenue_30_annual'] = MemberStatsFinancial::getProjectedRevenue30day('annual');
		$stats['projected_revenue_30'] = MemberStatsFinancial::getProjectedRevenue30day();
		
		// Projected monthly average price
		$stats['projected_monthly_average_price_paid_member_month'] = MemberStatsFinancial::getProjectedMonthlyAveragePricePaidMember('month');
		$stats['projected_monthly_average_price_paid_member_biannual'] = MemberStatsFinancial::getProjectedMonthlyAveragePricePaidMember('biannual');
		$stats['projected_monthly_average_price_paid_member_annual'] = MemberStatsFinancial::getProjectedMonthlyAveragePricePaidMember('annual');
		$stats['projected_monthly_average_price_paid_member'] = MemberStatsFinancial::getProjectedMonthlyAveragePricePaidMember();
		$stats['projected_monthly_average_price_cctrial_member_month'] = MemberStatsFinancial::getProjectedMonthlyAveragePriceCcTrialMember('month');
		$stats['projected_monthly_average_price_cctrial_member_biannual'] = MemberStatsFinancial::getProjectedMonthlyAveragePriceCcTrialMember('biannual');
		$stats['projected_monthly_average_price_cctrial_member_annual'] = MemberStatsFinancial::getProjectedMonthlyAveragePriceCcTrialMember('annual');
		$stats['projected_monthly_average_price_cctrial_member'] = MemberStatsFinancial::getProjectedMonthlyAveragePriceCcTrialMember();
		$stats['projected_monthly_average_price_month'] = MemberStatsFinancial::getProjectedMonthlyAveragePrice('month');
		$stats['projected_monthly_average_price_biannual'] = MemberStatsFinancial::getProjectedMonthlyAveragePrice('biannual');
		$stats['projected_monthly_average_price_annual'] = MemberStatsFinancial::getProjectedMonthlyAveragePrice('annual');
		$stats['projected_monthly_average_price'] = MemberStatsFinancial::getProjectedMonthlyAveragePrice();
		
		// Member counts
		$stats['paid_member_count_month'] = MemberStatsFinancial::getPaidMemberCount('month');
		$stats['paid_member_count_biannual'] = MemberStatsFinancial::getPaidMemberCount('biannual');
		$stats['paid_member_count_annual'] = MemberStatsFinancial::getPaidMemberCount('annual');
		$stats['paid_member_count'] = MemberStatsFinancial::getPaidMemberCount();
		$stats['cctrial_member_count_month'] = MemberStatsFinancial::getCcTrialMemberCount('month');
		$stats['cctrial_member_count_biannual'] = MemberStatsFinancial::getCcTrialMemberCount('biannual');
		$stats['cctrial_member_count_annual'] = MemberStatsFinancial::getCcTrialMemberCount('annual');
		$stats['cctrial_member_count'] = MemberStatsFinancial::getCcTrialMemberCount();
		$stats['member_count_month'] = MemberStatsFinancial::getMemberCount('month');
		$stats['member_count_biannual'] = MemberStatsFinancial::getMemberCount('biannual');
		$stats['member_count_annual'] = MemberStatsFinancial::getMemberCount('annual');
		$stats['member_count'] = MemberStatsFinancial::getMemberCount();
		
		// Projected monthly average revenue
		$stats['projected_monthly_average_revenue_paid_member_month'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenuePaidMember('month');
		$stats['projected_monthly_average_revenue_paid_member_biannual'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenuePaidMember('biannual');
		$stats['projected_monthly_average_revenue_paid_member_annual'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenuePaidMember('annual');
		$stats['projected_monthly_average_revenue_paid_member'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenuePaidMember();
		$stats['projected_monthly_average_revenue_cctrial_member_month'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenueCcTrialMember('month');
		$stats['projected_monthly_average_revenue_cctrial_member_biannual'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenueCcTrialMember('biannual');
		$stats['projected_monthly_average_revenue_cctrial_member_annual'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenueCcTrialMember('annual');
		$stats['projected_monthly_average_revenue_cctrial_member'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenueCcTrialMember();
		$stats['projected_monthly_average_revenue_month'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenue('month');
		$stats['projected_monthly_average_revenue_biannual'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenue('biannual');
		$stats['projected_monthly_average_revenue_annual'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenue('annual');
		$stats['projected_monthly_average_revenue'] = MemberStatsFinancial::getProjectedMonthlyAverageRevenue();
		
		// Projected annual revenue
		$stats['projected_annual_revenue_paid_member_month'] = MemberStatsFinancial::getProjectedAnnualRevenuePaidMember('month');
		$stats['projected_annual_revenue_paid_member_biannual'] = MemberStatsFinancial::getProjectedAnnualRevenuePaidMember('biannual');
		$stats['projected_annual_revenue_paid_member_annual'] = MemberStatsFinancial::getProjectedAnnualRevenuePaidMember('annual');
		$stats['projected_annual_revenue_paid_member'] = MemberStatsFinancial::getProjectedAnnualRevenuePaidMember();
		$stats['projected_annual_revenue_cctrial_member_month'] = MemberStatsFinancial::getProjectedAnnualRevenueCcTrialMember('month');
		$stats['projected_annual_revenue_cctrial_member_biannual'] = MemberStatsFinancial::getProjectedAnnualRevenueCcTrialMember('biannual');
		$stats['projected_annual_revenue_cctrial_member_annual'] = MemberStatsFinancial::getProjectedAnnualRevenueCcTrialMember('annual');
		$stats['projected_annual_revenue_cctrial_member'] = MemberStatsFinancial::getProjectedAnnualRevenueCcTrialMember();
		$stats['projected_annual_revenue_month'] = MemberStatsFinancial::getProjectedAnnualRevenue('month');
		$stats['projected_annual_revenue_biannual'] = MemberStatsFinancial::getProjectedAnnualRevenue('biannual');
		$stats['projected_annual_revenue_annual'] = MemberStatsFinancial::getProjectedAnnualRevenue('annual');
		$stats['projected_annual_revenue'] = MemberStatsFinancial::getProjectedAnnualRevenue();
		
		$this->view->stats = $stats;
	}
	
	public function content()
	{
		$request = $this->getRequest();
		
		$content_new_start = $request->getParam('content_new_start', '-7days');
		$content_new_end = $request->getParam('content_new_end', '-1days');
		
		$content_new_days = array();

		ListingStats::iterateDateRange($content_new_start, $content_new_end, function($date) use (&$content_new_days) {
			$content_new_days[] = date('D jS M', strtotime($date));
		});
		$this->view->content_new_days = $content_new_days;
		
		$content_new = ListingStats::getNewManualForDateRange($content_new_start, $content_new_end);
		$this->view->content_new = $content_new;
		
		$content_new_tags = ListingStats::getNewManualForAllTagsForDateRange($content_new_start, $content_new_end);
		$this->view->content_new_tags = $content_new_tags;
	}

	public function content_by_city()
	{
		$request = $this->getRequest();

		$content_new_start = $request->getParam('content_new_start', '-7days');
		$content_new_end = $request->getParam('content_new_end', '-1days');

		$content_new_days = array();

		ListingStats::iterateDateRange($content_new_start, $content_new_end, function($date) use (&$content_new_days) {
			$content_new_days[] = date('D jS M', strtotime($date));
		});
		$this->view->content_new_days = $content_new_days;

		$content_by_city = ListingStats::getNewManualForAllCitiesForDateRange($content_new_start, $content_new_end);
		$this->view->content_by_city = $content_by_city;

		$content_new = ListingStats::getNewManualForDateRange($content_new_start, $content_new_end);
		$this->view->content_new = $content_new;
	}

	/**
	 * Time difference between when people signup and when they give us their CC? Need them put into buckets
	 * @since 2013nov07
	 */
	function whenprovidedcc()
	{
		$statistician = new MemberStatsRegCc();
		$this->view->statRecs = $statistician->calc();
	}

	public function fb_comments()
	{
		$sort = $this->view->sort = $this->_getParam('sort', 'date');
		$by = $this->_getParam('by', 'desc');

		$sortOrder = 'DESC';

		switch(strtolower($sort)) {
			case 'member':
				$sortField = 'm.id';
				break;
			case 'likes':
				$sortField = 'comment.likes';
				break;
			case 'date':
				$sortField = 'comment.created_time';
				break;
		}

		if (strtolower($by) == 'asc')
		{
			$sortOrder    = 'ASC';
		}

		$this->view->by = $sortOrder == 'DESC' ? 'asc' : 'desc';

		$message = new Zend_Session_Namespace('messages');
		if(isset($message->data) && isset($message->data->message)) $this->view->message = $message->data->message;
		if (empty($message->data)) $message->data = new stdClass;
		$message->data->message = '';

		$conditions = '';

		$cities        = new City();
		$select = intval($this->getSelectedAdminCity());
		if($select)
		{
			$selected = $cities->find($select);
			$conditions .= " AND (m.publish_city_id=".intval($selected->id) . " OR m.city_id=".intval($selected->id) .")";
			$this->view->selectedCityObject    =   $selected;
		}

		$query = "
			SELECT
				comment.id,
				comment.post_id,
				m.id as member_id,
				m.email,
				m.last_name,
				m.first_name,
				comment.from_name,
				comment.message,
				comment.likes,
				comment.created_time
			FROM
				`facebook_comment` comment
				LEFT JOIN member m ON m.uid = comment.from_uid
			WHERE 1 $conditions
			ORDER BY $sortField $sortOrder
		";


		$comments = FacebookComment::find_by_sql($query);

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($comments));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

		$this->view->paginator = $paginator;
	}
}