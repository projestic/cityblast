<?php

class TwitterController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		parent::init();
		$this->view->section = "twitter";
		//$this->requireLogin();
		//$this->setFacebookUser();

	}

	public function index()
	{

		$this->redirectIfNotLoggedIn();


		// Cleaning unwanted variables
		// Making mandatory to refresh or load them from database
		unset($_SESSION['TWITTER_AUTH_REQUEST']);
		unset($_SESSION['TWITTER_ACCESS_TOKEN']);


		//Saving url to return after the tweet action
		$_SESSION['twitter_http_referrer'] = !empty($_SESSION['redirect_to_after_tweet']) ? $_SESSION['redirect_to_after_tweet'] : $_SERVER['HTTP_REFERER'];

		//Determine where the redirect should go...
		$_SESSION['twt_return'] = null;
		if($_REQUEST['settings']) $_SESSION['twt_return'] = "/member/settings/#settingtab";

		if($_SESSION['member'] && !empty($_SESSION['TWITTER_ACCESS_TOKEN']) )
		{
			//echo "<h1>DO NOTHING...</h1>";
			//$member = Member::find_by_uid($_SESSION['member']->uid);

		}
		else
		{
			$member = Member::find_by_uid($_SESSION['member']->uid);

			if(empty($_SESSION['member']->twitter_id) ||
					$_SESSION['member']->twitter_auto_disabled ==1 ||
					empty($_SESSION['TWITTER_REQUEST_TOKEN']) ||
					empty($_SESSION['TWITTER_ACCESS_TOKEN']))
			{

				//Saving Track information for handling after Twitter OAuth redirection
				$_SESSION['TWITTER_AUTH_REQUEST'] = 1;
				$_SESSION['twitter_added']  = 1;

				$consumer = new Blastit_Oauth_Consumer_Twitter();

				/*
				* Check for already authenticated and
				* app has TWITTER ACCESS TOKEN
				*/

				if (!isset($_SESSION['TWITTER_ACCESS_TOKEN'])) {

					$token = $consumer->getRequestToken();
					$_SESSION['TWITTER_REQUEST_TOKEN'] = serialize($token);
					$consumer->redirect();

				} else {
					return true;
				}

			}
			else
			{

				$_SESSION['profile_synced'] = 1;
				$_SESSION['twitter_added']  = 1;


				//Sanity check
				if(empty($member->twitter_request_token) || empty($member->twitter_access_token))
				{

					$member->twitter_request_token	= $_SESSION['TWITTER_REQUEST_TOKEN'];
					$member->twitter_access_token 	= $_SESSION['TWITTER_ACCESS_TOKEN'];


					$network_app = Zend_Registry::get('networkAppTwitter');
					$member->tw_application_id = $network_app->id;

					$member->save();


					$bingo = $member->published_today($member);
					$blast = $member->find_blast();

					if($blast && $bingo)
					{

						if(isset($blast->listing_id) && !empty($blast->listing_id))
						{
							$member->publishBlast($member_blast);
						}
					}


					/***********************************************
					//Automatically publish to our new user for the first time
					require_once APPLICATION_PATH . "/../scripts/publish.post.inc.php";

					ob_start();

					//Publish a FB Listing right away
					$publishing_queue = PublishingQueue::find_all_by_city_id_and_inventory_id_and_hopper_id($member->publish_city_id, $member->inventory_id, $member->current_hoper, array('order' => 'priority ASC'));

					//Let's find out the next listing this particular member should publish...
					$member_blast = find_member_blast($member, $publishing_queue, false);

					if($member_blast)
					{
					publishTwitter($member, $member_blast->listing, $member_blast);
					}

					ob_end_clean();
					***********************************************/
				}
			}
		}


		// Isn't expected to get into this point
		//$this->_redirect($_SERVER['HTTP_REFERER']);
		$this->_redirect("/member/linkedin");
	}

	public function callback()
	{
		$member = Member::find_by_uid($_SESSION['member']->uid);
		$network_app = Zend_Registry::get('networkAppTwitter');

		$params =	$this->getRequest()->getParams();

		if(!isset($params['denied']) || empty($params['denied'])) 
		{
			$consumer   = new Blastit_Oauth_Consumer_Twitter(null);

			if (!empty($_GET) && isset($_SESSION['TWITTER_REQUEST_TOKEN']))
			{
				$token = $consumer->getAccessToken($_GET, unserialize($_SESSION['TWITTER_REQUEST_TOKEN']));
				$my_twitter = new Blastit_Service_Twitter(array(
						'oauth' => $token,
						'consumerKey' => $network_app->consumer_id,
						'consumerSecret' => $network_app->consumer_secret,
					), 
					$member 
				);
				$_SESSION['TWITTER_ACCESS_TOKEN'] = serialize($token);
				$success = true;
				try {
					$credentials = $my_twitter->accountVerifyCredentials();
				} catch(Exception $e) {
					var_dump($e->getMessage());
					$success = false;
				}
				if ($success) {
					$this->save_account($credentials);
				}
			}

		} else {
			// DO nothing user denied authorization
		}

		if($_SESSION['TWITTER_AUTH_REQUEST'])
		{
			$_SESSION['TWITTER_AUTH_REQUEST'] = 0;
		}

		$_SESSION['twt_authorize'] = 0;
		$this->renderNothing();

		if(!empty($_SESSION['twt_return'])) $this->redirect($_SESSION['twt_return']);
		else $this->redirect('/member/linkedin');
	}

	public function save_account($credentials)
	{
		$member = Member::find_by_uid($_SESSION['member']->uid);

		if(!$member)
		{
			$member = new Member();
		}

		$member->twitter_id 			= $credentials->id;
		$member->twitter_screen_name 	= $credentials->screen_name;
		$member->twitter_name 			= $credentials->name;

		$member->twitter_request_token 	= $_SESSION['TWITTER_REQUEST_TOKEN'];
		$member->twitter_access_token 	= $_SESSION['TWITTER_ACCESS_TOKEN'];
		$member->twitter_followers 		= (int) $credentials->followers_count;
		$member->twitter_publish_flag 	= 1;
		$member->twitter_token_reminder = 0;
		$member->twitter_fails 			= 0;
		$member->twitter_auto_disabled	= 0;
		
		$network_app = Zend_Registry::get('networkAppTwitter');
		$member->tw_application_id = $network_app->id;


		$member->save();
	}

	public function get_credentials()
	{
		$my_twitter = new Blastit_Service_Twitter();
		$credentials = $my_twitter->accountVerifyCredentials();

		$this->save_account($credentials);

	}


	public function verify()
	{

		if($this->fb_user)
		{

			$my_twitter = new Blastit_Service_Twitter();

			try {
				$credentials = $my_twitter->accountVerifyCredentials();
				$twitter = Member::find_by_twitter_id($credentials->id);
			} catch (Exception $e) {

			}

			if(!isset($twitter) || !$twitter)
			{

				$twitter = new Member();
				//$twitter->facebook_id = $this->fb_user->id;

				$twitter->twitter_id = $credentials->id;
				$twitter->twitter_screen_name = $credentials->screen_name;
				$twitter->twitter_name = $credentials->name;

				$twitter->twitter_request_token = $_SESSION['TWITTER_REQUEST_TOKEN'];
				$twitter->twitter_access_token = $_SESSION['TWITTER_ACCESS_TOKEN'];
				$twitter->twitter_followers = (int)$credentials->followers_count;

				$twitter->save();
			}
		}

		exit();
	}

	public function clear_twitter()
	{
		unset($_SESSION);

		ob_flush();
		echo "FIN";

		exit();
	}

	public function confirm_twitter_sync()
	{

		if($this->fb_user)
		{
			$twitter = Member::find_by_facebook_id($this->fb_user->id);

			$_SESSION['profile_synced'] = 1;

			$today = getdate();

			//$status = "Tweet Tweet - Alen Test: ".$today[0];
			//$my_twitter = new Blastit_Service_Twitter(null, $twitter);
			//$my_twitter->updateStatus($status);
		}



		exit();
	}

}

?>
