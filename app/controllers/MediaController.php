<?php
/**
 * this class will make various sizes of images
 * into a separate 'cache' dir
 *
 * supported $_GET params:
 * image - filename
 * width		maximum width of final image in pixels (e.g. 700)
 * height		maximum height of final image in pixels (e.g. 700)
 * color		(optional) background hex color for filling transparent PNGs (e.g. 900 or 16a942)
 * cropratio	(optional) ratio of width to height to crop final image (e.g. 1:1 or 3:2)
 * nocache		(optional) does not read image from the cache
 * quality		(optional, 0-100, default: 100) quality of output image
 * center 		(optional) centers image on bg
 */

// BASED ON:

// Smart Image Resizer 1.4.1
// Resizes images, intelligently sharpens, crops based on width:height ratios, color fills
// transparent GIFs and PNGs, and caches variations for optimal performance

// Created by: Joe Lencioni (http://shiftingpixel.com)
// Date: August 6, 2008
// Based on: http://veryraw.com/history/2005/03/image-resizing-with-php/
/////////////////////
// PARAMETERS
/////////////////////

// Parameters need to be passed in through the URL's query string:
// width		maximum width of final image in pixels (e.g. 700)
// height		maximum height of final image in pixels (e.g. 700)

/////////////////////
// EXAMPLES
/////////////////////

// Resizing a JPEG:
// <img src="/image.php/image-name.jpg?width=100&amp;height=100&amp;image=/path/to/image.jpg" alt="Don't forget your alt text" />

// Resizing and cropping a JPEG into a square:
// <img src="/image.php/image-name.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=/path/to/image.jpg" alt="Don't forget your alt text" />

// Matting a PNG with #990000:
// <img src="/image.php/image-name.png?color=900&amp;image=/path/to/image.png" alt="Don't forget your alt text" />

class MediaController extends ApplicationController
{

	const MEMORY_TO_ALLOCATE = '100M';
	const DEFAULT_QUALITY = 90;

	protected $image_name;
	protected $src_image;
	protected $cache_dir;
	protected $src_width, $src_height;	// actual width/ht of source image
	protected $mime_type;
	protected $etag;
	protected $last_modified;
	protected $center_image;		// whether to center or not
	protected $request = array();

	protected static $acl = array(
		'*' => 'public'
	);

	public function index()
	{
		
		// Set requests
		$this->request = $_REQUEST;
		
		// Set image
		$this->request['image'] = $this->_request->getParam('image');
		
		if (!isset($this->request['image']))
		{
			header('HTTP/1.1 400 Bad Request');
			echo 'Error: no image was specified';
			exit();
		}
		else
			$this->request['image'] = $this->request['image'].'.jpg';
		
		// Images must be local files, so for convenience we strip the domain if it's there
		$this->request['image'] = preg_replace('/^(s?f|ht)tps?:\/\/[^\/]+/i', '', (string) $this->request['image']);
		
		// For security, directories cannot contain ':', images cannot contain '..' or '<', and
		if (strpos(dirname($this->request['image']), ':') || preg_match('/(\.\.|<|>)/', $this->request['image']))
		{
			header('HTTP/1.1 400 Bad Request');
			echo 'Error: malformed image path. Image paths must begin with \'/\'';
			exit();
		}
		
		$this->dst_image = $_SERVER['DOCUMENT_ROOT'] . "/public/images/blank.gif";
		$this->src_image = $_SERVER['DOCUMENT_ROOT'] . "/public/images/listings/{$this->request['image']}";
		$this->cache_dir = $_SERVER['DOCUMENT_ROOT'] . "/public/images/listings/imagecache/";
		
		if (!file_exists($this->src_image))
		{
			header('HTTP/1.1 404 Not Found');
			echo 'Error: image does not exist: ' . $this->src_image;
			exit();
		}

		// Get the size and MIME type of the requested image
		$size = GetImageSize($this->src_image);
		$this->mime_type = $size['mime'];
		$this->src_width = $size[0];
		$this->src_height = $size[1];
		
		// Make sure that the requested file is actually an image
		if (substr($this->mime_type, 0, 6) != 'image/')
		{
			header('HTTP/1.1 400 Bad Request');
			echo 'Error: requested file is not an accepted type: ' . $this->src_image . ' - mime: '.$this->mime_type;
			exit();
		}

		$this->request['width'] = (int) $this->request['width'];
		$this->request['height'] = (int) $this->request['height'];

		if (isset($this->request['color']))
			$this->color = preg_replace('/[^0-9a-fA-F]/', '', (string) $this->request['color']);
		else
			$this->color = "none";

		// If either a max width or max height are not specified, we default to something
		// large so the unspecified dimension isn't a constraint on our resized image.
		// If neither are specified but the color is, we aren't going to be resizing at
		// all, just coloring.
		if (!isset($this->request['width']) && $this->request['height'])
		{
			$this->request['width'] = 99999999999999;
		}
		elseif ($this->request['width'] && !isset($this->request['height']))
		{
			$this->request['height'] = 99999999999999;
		}
		elseif (!isset($this->request['width']) && !isset($this->request['height']))
		{
			$this->request['width'] = $this->src_width;
			$this->request['height'] = $this->src_height;
		}

		// If we don't have a max width or max height, OR the image is smaller than both
		// we do not want to resize it, so we simply output the original image and exit
		if (!$this->request['width'] && !$this->request['height'] && !$this->color)
		{
			die;
			$data = file_get_contents($this->src_image);

			$this->last_modified = gmdate('D, d M Y H:i:s', filemtime($this->src_image)) . ' GMT';
			$this->etag = md5($data);

			$this->doConditionalGet();

			header("Content-type: $this->mime_type");
			header('Content-Length: ' . strlen($data));
			echo $data;
			exit();
		}
		
		$this->get_image();
	}

	protected function get_image()
	{

		// Ratio cropping
		$offsetX = 0;
		$offsetY = 0;
		
		// GET THE X RATIO BY USING THE SUPPLIED WIDTH, IF NO WIDTH USE THE HEIGHT THAT'S SUPPLIED
		$xRatio = ($this->request['width']!=0)?$this->request['width']/$this->src_width:$this->request['height']/$this->src_width;
		
		// GET THE Y RATIO BY USING THE SUPPLIED HEIGHT, IF NO HEIGHT USE THE WIDTH THAT'S SUPPLIED
		$yRatio = ($this->request['height']!=0)?$this->request['height']/$this->src_height:$this->request['width']/$this->src_height;
		
		// GET THE FINAL RATIO
		$fRatio = ( $xRatio < $yRatio ) ? $xRatio : $yRatio;
		
		$origHeight = $this->src_height;
		$origWidth = $this->src_width;
		
		// FIT THE SRC IMAGE IN TO THE TARGET SQUARE IMAGE
		if ( 
				( isset($this->request['width']) && ( $this->request['height'] == 0 || $this->request['width'] == $this->request['height'] ) ) 
				
				|| 
				
				( isset($this->request['height']) && ( $this->request['width'] == 0 || $this->request['width'] == $this->request['height'] ) ) 
			)
		{
			
			$max = ( $this->request['width'] != 0 )?$this->request['width']:$this->request['height'];
			
			// SCALE DOWN TO FIT THE IMAGE
			if( $fRatio < 1 ){
					
				$tnWidth			= round( ( float ) $fRatio * $origWidth );
				$tnHeight			= round( ( float ) $fRatio * $origHeight );
				$offsetX			= ($xRatio<$yRatio )?0:($max-$tnWidth)/2;
				$offsetY			= ($yRatio<$xRatio )?0:($max-$tnHeight)/2;
				
			}
			
			// IMAGE IS SMALLER THAN TARGET, DO NOT SCALE
			else{
				
				$tnWidth			= $this->src_width;
				$tnHeight			= $this->src_height;
				$offsetX			= ( $this->request['width'] ) ? ( $this->request['width'] - $this->src_width ) / 2 : ( $this->request['height'] - $this->src_width ) / 2;
				$offsetY			= ( $this->request['height'] ) ? ( $this->request['height'] - $this->src_height ) / 2 : ( $this->request['width'] - $this->src_height ) / 2;
				
			}
			
		}
		
		// FIT THE SRC IMAGE IN TO THE TARGET RECTANGLE IMAGE
		else
		{
			
			$maxX = $this->request['width'];
			$maxY = $this->request['height'];
			
			// LANDSCAPE IMAGE
			if( $xRatio < $yRatio && $xRatio < 1 ){
				
				$tnWidth			= round( ( float ) $xRatio * $origWidth );
				$tnHeight			= round( ( float ) $xRatio * $origHeight );
				$offsetX			= 0;
				$offsetY			= ( $this->request['height'] - $tnHeight )/2;
				
			}
			
			// PORTRAIT IMAGE
			elseif( $yRatio < $xRatio && $yRatio < 1 ){
				
				$tnWidth			= round( ( float ) $yRatio * $origWidth );
				$tnHeight			= round( ( float ) $yRatio * $origHeight );
				$offsetX			= ( $this->request['width'] - $tnWidth )/2;
				$offsetY			= 0;
				
			}
			
			// IMAGE IS SMALLER THAN CANVAS, DO NOT SCALE, JUST CENTER
			else{
				
				$tnWidth			= $this->src_width;
				$tnHeight			= $this->src_height;
				$offsetX			= ( $this->request['width'] ) ? ( $this->request['width'] - $this->src_width ) / 2 : ( $this->request['height'] - $this->src_width ) / 2;
				$offsetY			= ( $this->request['height'] ) ? ( $this->request['height'] - $this->src_height ) / 2 : ( $this->request['width'] - $this->src_height ) / 2;
				
			}
			
		}
		
		// FIGURE THE SIZE OF THE CANVAS
		if ($this->request['width'] && $this->request['height']){
			$canvasW = $this->request['width'];
			$canvasH = $this->request['height'];
		}
		elseif ($this->request['width'])
			$canvasW = $canvasH = $this->request['width'];
			
		elseif ($this->request['height'])
			$canvasW = $canvasH = $this->request['height'];
		
		// IF WE'RE CROPPING
		if ($this->request['crop']){
			$offsetX	= 0;
			$offsetY	= 0;
			$canvasW = ($canvasW > $tnWidth)?$tnWidth:$canvasW;
			$canvasH = ($canvasH > $tnHeight)?$tnHeight:$canvasH;
		}

		// Determine the quality of the output image
		// default to 100
		$quality = (isset($this->request['quality'])) ? (int) $this->request['quality'] : self::DEFAULT_QUALITY;

		// Before we actually do any crazy resizing of the image, we want to make sure that we
		// haven't already done this one at these dimensions. To the cache!
		// Note, cache must be world-readable

		// We store our cached image filenames as a hash of the dimensions and the original filename
		$resizedImageSource = $canvasW . 'x' . $canvasH . '-' . $this->request['image'];
		$resized = $this->cache_dir . md5($resizedImageSource);
		
		// Check the modified times of the cached file and the original file.
		// If the original file is older than the cached file, then we simply serve up the cached file
		if (!isset($this->request['nocache']) && file_exists($resized))
		{
			$imageModified = filemtime($this->src_image);
			$thumbModified = filemtime($resized);

			if($imageModified < $thumbModified) {
				$data = file_get_contents($resized);

				$this->last_modified = gmdate('D, d M Y H:i:s', $thumbModified) . ' GMT';
				$this->etag = md5($data);

				$this->doConditionalGet();		
		
				header("Content-type: $this->mime_type");
				header('Content-Length: ' . strlen($data));
				echo $data;
				$this->exit_without_render();
				//exit();
			}
		}
		
		// We don't want to run out of memory
		ini_set('memory_limit', self::MEMORY_TO_ALLOCATE);
		
		// SOURCE PNG
		$src	= imagecreatefromjpeg($this->src_image);
		
		// DESTINATION CANVAS
		$dst = imagecreatetruecolor($canvasW, $canvasH);
		$color = imagecolorallocate($dst, 255, 255, 255);
		imagefill($dst, 0, 0, $color);
		
		// RESAMPLE/RESIZE SOURCE AND TARGET TOGETHER
		imagecopyresampled($dst, $src, $offsetX, $offsetY, 0, 0, $tnWidth, $tnHeight, $this->src_width, $this->src_height);
	
		// WRITE FILE
		imagejpeg($dst, $resized, $quality);
	
		// Put the data of the resized image into a variable
		ob_start();
		imagejpeg($dst, null, $quality);
		$data = ob_get_contents();
		ob_end_clean();
	
		// MEMORY CLEAN UP
		imagedestroy($src);
		imagedestroy($dst);

		// See if the browser already has the image
		$this->last_modified = gmdate('D, d M Y H:i:s', filemtime($resized)) . ' GMT';
		$this->etag = md5($data);

		$this->checkCacheDir();
		$this->doConditionalGet();

		// Send the image to the browser with some delicious headers
		header("Content-type: $this->mime_type");
		header('Content-Length: ' . strlen($data));
		echo $data;
		
		$this->renderNothing();
	}

	/**
	 * 
	 * check if cache dir exists, try to create if not
	 */
	protected function checkCacheDir()
	{
				// Make sure the cache exists. If it doesn't, then create it
		if (!file_exists($this->cache_dir))
			mkdir($this->cache_dir, 0755);

		// Make sure we can read and write the cache directory
		if (!is_readable($this->cache_dir))
		{
			header('HTTP/1.1 500 Internal Server Error');
			echo "Error: the cache directory is not readable {$this->cache_dir}";
			exit();
		}
		else if (!is_writable($this->cache_dir))
		{
			header('HTTP/1.1 500 Internal Server Error');
			echo 'Error: the cache directory is not writable';
			exit();
		}

	}
	
	protected function findSharp($orig, $final) // function from Ryan Rud (http://adryrun.com)
	{
		$final = $final * (750.0 / $orig);
		$a = 52;
		$b = -0.27810650887573124;
		$c = .00047337278106508946;

		$result = $a + $b * $final + $c * $final * $final;

		return max(round($result), 0);
	}

	protected function doConditionalGet()
	{
		header("Last-Modified: $this->last_modified");
		header("ETag: \"{$this->etag}\"");

		$if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ?
		stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) :
		false;

		$if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ?
		stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) :
		false;

		if (!$if_modified_since && !$if_none_match)
			return;

		if ($if_none_match && $if_none_match != $this->etag && $if_none_match != '"' . $this->etag . '"')
			return; // etag is there but doesn't match

		if ($if_modified_since && $if_modified_since != $this->last_modified)
			return; // if-modified-since is there but doesn't match

		// Nothing has changed since their last request - serve a 304 and exit
		header('HTTP/1.1 304 Not Modified');
		exit();
	}

}
?>