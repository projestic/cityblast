<?

class greatestController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		parent::init();
	}

	public function index()
	{	
		$this->_helper->layout->setLayout('landing-no-header-footer');

		if ($this->_request->isPost()) 
		{
			$validatorNotEmpty = new Zend_Validate_NotEmpty();
			$validatorEmail = new Zend_Validate_EmailAddress();
			if ($validatorNotEmpty->isValid($this->_request->getParam("email")) && $validatorEmail->isValid($this->_request->getParam("email"))) 
			{
				// Create a TrialMember
				$trial_member = new TrialMember();
				$trial_member->email = $this->_request->getParam("email");
				$trial_member->save();

				// Send email with PDF
				$subject  = 'Your Copy Has Arrived! The "Greatest Real Estate Posts of All Time" are Yours!';
				$htmlBody = @$this->view->partial('email/greatest_100.html.php', $params);
				$mandrill = new Mandrill(array('100_GREATEST'));
				$mandrill->setFrom(COMPANY_WEBSITE, HELP_EMAIL);
				$mandrill->addTo("", $this->_request->getParam("email"));
				
				$mandrill->addAttachment(realpath(APPLICATION_PATH . '/../greatest/CityBlast_100_Greatest_Posts.zip'));
				
				$mandrill->send($subject, $htmlBody);
				$this->_redirect('/100greatest/thanks');
				
			} else {
				$this->view->show_validation = true;
			}
		}

	}

	public function thanks()
	{
		$this->_helper->layout->setLayout('landing-no-header-footer');
	}
}
