<?php

class ContentController extends ApplicationController
{
	CONST TAXES		=   "0.13"; // 13%
	CONST CURRENCY		=   "CAD";

	protected static $acl = array(
		'edit' => array('odesk', 'assistanteditor'),
		'savecontent' => array('odesk', 'assistanteditor'),
		'saveupdatecontent' => array('odesk', 'assistanteditor'),
		'*' => 'superadmin'
	);


	public function init()
	{
		//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');
		parent::init();

		$this->view->admin_selectedCity    = $this->getSelectedAdminCity();
	}

	
	public function test()
	{					
		//$listing = Listing::find(20208);
		

		$listing = Listing::find(20277);
		

		$content = new Listing();
		$content->createShowCase_GuessingGame($listing);
		
		if($listing->price > 1000000)
		{
			$content = new Listing();
			$content->createShowCase_WhatCity($listing);
		}


		//$content = new Listing();
		//$content->createShowCase_OverUnder($listing);


		//getimagesize ( string $filename )

		echo "Finished!<BR>";

		
		exit();
	}
	
	public function edit()
	{
		$this->_helper->layout->setLayout('admin');

		$this->view->listing_only = false;

		$this->view->cities = City::find('all', array('order' => 'name ASC'));
		

		$request    	=  $this->getRequest();
		$id  		=  $request->getParam('listing_id');
		$hopper_id  	=  $request->getParam('hopper');
		$listing_id  	=  $request->getParam('listing_id');


		$city_id	= intval($this->getSelectedAdminCity());
		$this->view->selectedCityObject = null;
		if($city_id != "" && $city_id != "*") $this->view->selectedCityObject = City::find_by_id($city_id);
		
		
		
		$this->view->listing = null;
		if($listing_id)
		{
			$this->view->listing = Listing::find_by_id($listing_id);	
		}


		if($listing_id)
		{
			$query = "select count(*) as total_clicks from click_stat where listing_id=$listing_id";
			$this->view->click_stat = Listing::find_by_sql($query);
			$this->view->click_stat = $this->view->click_stat[0];
		}
		

		$this->view->images			= array();

		$publish = new PublishingQueue();
		$publish->id 			= 0;
		$publish->hopper_id 	= $hopper_id;

		if($this->view->selectedCityObject)
		{
			$publish->min_inventory_threshold 	= $this->view->selectedCityObject->min_inventory_threshold;
			$publish->max_inventory_threshold 	= $this->view->selectedCityObject->max_inventory_threshold;
		}
		else
		{
			$publish->min_inventory_threshold 	= 0;
			$publish->max_inventory_threshold 	= 0;
		}

		$this->view->publish = $publish;
		$this->view->tags    = Tag::all(array('conditions' =>array('publishable = 1')));

		if($listing_id)
		{
			$this->view->selectedTags = TagCloud::find('all', array('conditions' => "listing_id = '{$listing_id}'"));
		}
		
		$config = Zend_Registry::get('config');
		
		$this->view->copy_checkbox = false;
		$this->view->copy_link = false;
		
		if ($config->content && $config->content->copy_to_env) {
			if ($listing_id == 0) {
				$this->view->copy_checkbox = true;
			} else {
				$this->view->copy_link = true;
			}
		}
		
		//$this->view->selected_hopper_id	=   (empty($this->view->selected_hopper)) ? $request->getParam('hoper') : $this->view->selected_hopper;
		//$this->view->selected_inventory_id	=   (empty($this->view->selected_inventory_id)) ? $request->getParam('hoper') : $this->view->selected_inventory_id;
		
		$this->view->franchises = Franchise::find('all');		 
	}
	
	public function copytomm() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$json['copied'] = false;
		$listing = Listing::find($this->getParam('id'));
		$this->copy_to_mortgagemingler($listing);
		$json['copied'] = true;
		echo Zend_Json::encode($json);
	}
	
	protected function copy_to_mortgagemingler($listing) {
		$config = Zend_Registry::get('config');
		if (!$config->content || !$config->content->copy_to_default_member_id) {
			throw new Zend_Exception('No default member ID set (in content.copy_to_default_member_id)');
		}
		$mm_listing = ListingCopy::copy(
			$listing, 
			$config->content->copy_to_default_member_id, 
			$config->content->copy_to_image_path,
			$config->content->copy_to_app_url
		);
		return $mm_listing;
	}
	
	public function savecontent()
	{
		$params =	$this->getRequest()->getParams();

		$id = (int)trim($params['id']);
		if(!empty($id))
		{
			//$publish = PublishingQueue::find_by_id($params['id']);
			$listing = Listing::find_by_id($params['id']);
			$type = "update";
		}
		else
		{
			//For auditing purposes track it in the payment table
			/*****************************************************
			$payment = new Payment();
	
			$payment->paypal_confirmation_number = "Manual blast created by : " . $_SESSION['member']->uid;
			$payment->member_id = $_SESSION['member']->id;
			$payment->amount = "0.00";
			$payment->save();
							
			$publish = new PublishingQueue();
			**************************************************/


			$listing	= new Listing();
			$type = "new";
		}



		$listing->message		 = $params['blast'];


		$listing->pre_approval	 = 1;
		$listing->approval		 = 1;
		$listing->approved_by_id  = $_SESSION['member']->id;
		$listing->member_id		 = $_SESSION['member']->id;
		$listing->published		 = 0;
		
		if(isset($params['city'])) $listing->city_id  = $params['city'];
		
		if(!empty($params['franchise_id'])) $listing->franchise_id  = $params['franchise_id'];
		
		$listing->approved_at	 = date("Y-m-d H:i:s");
		$listing->blast_type 	 = Listing::TYPE_CONTENT;
		$listing->expiry_date  	 = $params['expiry_date'];
	
	
		if(empty($params['expiry_date'])){ 	
			$listing->expiry_date = NULL;
		}
		else{ 
			$expiry_date = date("Y-m-d",strtotime($params['expiry_date']));
			$listing->expiry_date = $expiry_date;
		}
		
		$listing->title  	 	 = $params['title'];
		
		if(isset($params['big_image'])) $listing->big_image = $params['big_image'];
		else $listing->big_image  	 = 0;
		
		if(isset($params['email_passthru'])) $listing->email_passthru  = $params['email_passthru'];
		else $listing->email_passthru  = 0;
		
		$listing->description   	 = $params['meta_description'];

		$originalUrl = null;
		if(!empty($params['content_link']))
		{
			$originalUrl = $params['content_link'];
			$listing->content_link = APP_URL."/v/i/".$listing->id;
		} else {
			$listing->content_link = '';
		}
		
		// If there is no link content must be a big image
		if (empty($listing->content_link) && !empty($listing->big_image)) {
			$listing->big_image = 1;
		}
		
		$listing->save();
		
		if(isset($params['fileUploaded']) && !empty($params['fileUploaded']))
		{	
			$image  = stristr($params['fileUploaded'], TMP_ASSET_URL) ? $params['fileUploaded'] : TMP_ASSET_URL.$params['fileUploaded'];
			$image 	= str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $image);
			$listing->ingestImage($image);
		}
		
		if ($listing->big_image && !$listing->image) {
			throw new Exception('Listing specified as big image but no image was uploaded');
		}

		//now store it un url mapping
		if(!empty($originalUrl))
		{
			if(!$urlMapping = UrlMapping::find_by_listing_id($listing->id))
			{
				$urlMapping = new UrlMapping();
			}
			$urlMapping->listing_id = $listing->id;
			$urlMapping->url = $originalUrl;
			$urlMapping->save();
		}

		if (!isset($params['tags'])) {
			$params['tags'] = array();
		}

		// save the tags for this listing
		TagCloud::setListingTags($listing->id, $params['tags']);	

		if (!isset($params['description']) || !is_array($params['description']))
		{
			// if no additional description
			$params['description']	= array();
		}

		ListingDescription::adjustDescriptions($listing, $params['description']);

		if (!isset($params['imgUploaded']) || !is_array($params['imgUploaded']))
		{
			// if no additional image
			$params['imgUploaded']	= array();
		}

		$uploadedImages = array();
		
		foreach ($params['imgUploaded'] as $upl) {
			if ($upl) {
				$image  = stristr($upl, TMP_ASSET_URL) ? $upl : TMP_ASSET_URL . $upl;
				$image 	= str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $image);
				if (file_exists($image)) $uploadedImages[] =  $image;
			}
		}
	
		ListingImage::adjustImages($listing, $uploadedImages);

		$message = new Zend_Session_Namespace('messages');
		if(empty($message->data))
			$message->data = new stdClass;

		$copied = '';

		if (isset($params['copy_listing']) && $params['copy_listing']) {
			$this->copy_to_mortgagemingler($listing);
			$copied = ' and copied to MortgageMingler';
		}
	
		if($type == "new") 
			$message->data->msg   = "Listing created$copied: <a href='/content/edit/".$listing->id."'>".$listing->message."</a>!";
		else $message->data->msg   = "Listing updated$copied: <a href='/content/edit/".$listing->id."'>".$listing->message."</a>!";


		if(isset($params['city']))
		{
			$city = City::find_by_id($listing->city_id);		
			
			$_SESSION['admin_selectedCity']		= $listing->city_id;
			$_SESSION['admin_selectedCityName']	= $city->name;
			$_SESSION['admin_selectedCountry']		= $city->country;
		}

		if ($_SESSION['member']->type->code == 'ODESK') {
			$this->redirect("/admin/pending");
		} else {
			$this->redirect("/admin/listings/");
		}

	}

	

	public function saveupdatecontent()
	{
		$params =	$this->getRequest()->getParams();

		if(!empty($params['id']))
		{
			$new = false;

			$listing = Listing::find_by_id($params['id']);
			$type = "update";
		}
		else
		{
			$new = true;
		}

		if(isset($params['fileUploaded']) && !empty($params['fileUploaded']))
		{
		
			$image  = stristr($params['fileUploaded'], TMP_ASSET_URL) ? $params['fileUploaded'] : TMP_ASSET_URL.$params['fileUploaded'];
			$image 	= str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $image);

			$listing->ingestImage($image);

		}
		
		if(empty($params['expiry_date'])){ 	
			$listing->expiry_date = NULL;
		}else{ 
			$expiry_date = date("Y-m-d",strtotime($params['expiry_date']));
			$listing->expiry_date = $expiry_date;
		}
		
		$listing->title  	 	 = $params['title'];
		$listing->description   	 = $params['meta_description'];
		
		if(!empty($params['franchise_id'])) $listing->franchise_id  = $params['franchise_id'];
		

	  	$listing->save();

		// save the tags for this listing
		TagCloud::setListingTags($listing->id, $params['tags']);

		if (!isset($params['description']) || !is_array($params['description']))
		{
			// if no additional description
			$params['description']	= array();
		}

		ListingDescription::adjustDescriptions($listing, $params['description']);

		if (!isset($params['imgUploaded']) || !is_array($params['imgUploaded']))
		{
			// if no additional image
			$params['imgUploaded']	= array();
		}

	
		$uploadedImages = array();
		
		foreach ($params['imgUploaded'] as $upl) {
			$image  = stristr($params['imgUploaded'], TMP_ASSET_URL) ? $params['imgUploaded'] : TMP_ASSET_URL.$params['imgUploaded'];
			$image 	= str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $image);
			if (file_exists($imageFile)) $uploadedImages[] =  $image;
		}

		ListingImage::adjustImages($listing, $uploadedImages);


		$message = new Zend_Session_Namespace('messages');
		if (empty($message->data)) $message->data = new stdClass;
		$message->data->msg   =	"Listing updated '".$listing->message."'!";

		$this->redirect("/admin/listings/".$params['inventory_id']);
	}

}
