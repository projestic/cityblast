<?
class CountryController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'public'
	);

	public function index()
	{
		$country = $this->_getParam('id');
		//echo $country . "\n\n";


		$states = State::find_all_by_country_code($country, array("order"=>"name ASC"));

		
		if(empty($country )) $state_array[] = "Please Select Your Country";
		elseif($country == "US") $state_array[] = "Please Select Your State";
		else  $state_array[] = "Please Select Your Province";

		foreach($states as $state)
		{
			$state_array[$state->code] = $state->name;	
		}
		
		
		//print_r($state_array);
		echo json_encode($state_array);
		
		exit();
		
	}
}
?>