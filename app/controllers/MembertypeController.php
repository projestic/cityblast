<?php

class MembertypeController extends ApplicationController
{
	
	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
		$this->view->admin_selectedCity    = $this->getSelectedAdminCity();
	}
	
	function index()
	{
		$conditions = array("conditions" => "", "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('MemberType', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
	}	
	
	function add()
	{
		$this->render('add');
	}

	function update()
	{
		$params = $this->getRequest()->getParams();
	
		$this->view->member_type = !empty($params['id']) ? MemberType::find($params['id']) : null;

		$this->render('add');
	}

	public function save()
	{
		$request = $this->getRequest();
		$params = $request->getParams();

		if($request->isPost()) 
		{
			if(!empty($params['id']))
			{
				$member_type = MemberType::find($params['id']);
			}
			else 
			{
				$member_type = new MemberType();
			}
			
			$member_type->name = $request->getParam('name');
			$member_type->is_default = $request->getParam('is_default', '0');
			$member_type->price_month = $request->getParam('price_month');
			$member_type->price_biannual = $request->getParam('price_biannual');
			$member_type->price_annual = $request->getParam('price_annual');
			$member_type->promo_codes_allowed = $request->getParam('promo_codes_allowed', '0');
			$member_type->save();
			
			if (empty($params['id'])) {
				// Each member type must have a set of related tag settings
				TagMemberTypeSetting::initMemberType($member_type->id);
			}
			
			if ($member_type->is_default) {
				$types = MemberType::all();
				if (!empty($types)) {
					foreach ($types as $type) {
						if ($type->id != $member_type->id) {
							$type->is_default = 0;
							$type->save();
						}
					}
				}
			}
		}
		
		$this->_redirect('/membertype/index');
	}


	public function delete()
	{		
		$params =	$this->getRequest()->getParams();

		if(!empty($params['id']) && $params['id'] != 0)
		{ 
			$member_type = MemberType::find($params['id']);
			$member_type->delete();
		}

		$this->_redirect('/membertype/index');
	}
}
