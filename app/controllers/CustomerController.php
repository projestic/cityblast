<?php
class CustomerController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'public'
	);

    public function init()
	{
		parent::init();  
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('checksitename', 'json')
                    ->initContext();
 
	}


  public function checksitename()
    {
   		$contextSwitch = $this->_helper->getHelper('contextSwitch');
	    $contextSwitch->setDefaultContext('json')->initContext();
        $this->_helper->viewRenderer->setNoRender();
        $flag = false;
        if(isset($this->params["sitename"]) && $this->params["sitename"] != ""){
            $sitename = $this->params["sitename"];
            $flag = Customer::isUniqueSiteName($sitename);
        }
     $this->_helper->json->sendJson(array('success' => $flag));
    }
  

}

?>