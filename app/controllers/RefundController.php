<?php

class RefundController extends ApplicationController
{
	
	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
	}
	
	public function index()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}

		$request    =  $this->getRequest();
		$member_id  =  $request->getParam('id');
		$member     = Member::find($member_id);
		
		if(!empty($member->id))
		{
			$this->view->member = $member;
			
			$conditions = array(
				"select" => "refund.*, 
					m.first_name as mfirst, 
					m.last_name as mlast, 
					ma.first_name as afirst, 
					ma.last_name as alast",
				"conditions" => "refund.member_id = '{$member->id}'",
				"joins" => "LEFT JOIN member m ON m.id = refund.member_id
					LEFT JOIN member ma ON ma.id = refund.admin_id",
				"order" => "id DESC");

			$paginator = new Zend_Paginator(new ARPaginator('Refund', $conditions));
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;
		}

	}	
	
	public function add()
	{
		$params = $this->getRequest()->getParams();
		$params = array_map('trim', $params);
		
		$error_messages = array();
	
		if($this->_request->isPost()) {
			
			if (empty($params['member_id'])) {
				$error_messages[] = 'Member not specified';
			}
			else
			{
				$member = Member::find($params['member_id']);
				if (empty($member))
				{
					$error_messages[] = "Member doesn't exist!";
				}
			}
			
			if (empty($params['note'])) {
				$error_messages[] = 'You must provide a note';
			}

			if (empty($params['amount'])) {
				$error_messages[] = 'You must provide an amount';
			}
			
			if (empty($error_messages)) {
				$refund = new Refund();
				$refund->member_id = $member->id;
				$refund->admin_id = ($_SESSION['member']->id) ? $_SESSION['member']->id : 0;
				$refund->amount = $params['amount'];
				$refund->note = htmlentities($params['note']);
				$refund->created_at = date('Y-m-d H:i:s');
				$refund->save();
				
				$this->_redirect("/admin/member/{$params['member_id']}#refunds");
			}
		} 

		

		if (!empty($params['id']))
		{
			$this->view->member = Member::find($params['id']);
		} else
		{
			$error_messages[] = "Member id is required!";
		}
			
		$this->view->error_messages = $error_messages;
	}
}