<?php

class ApplicationController extends Zend_Controller_Action
{
	
	#
	# Lets keep a convenience copy of the params handy
	#
	protected $params = array();

	const STRING="string";
	const INT="int";
	const FLOAT="float";
	const REST_DATE="date";
	const EMAIL = "email";
	CONST ITEMS_PER_PAGE = 25;

	protected $documentRoot =   '/public';  
  
	protected static $acl = array(
		
	   /* ACL RULES */
		
	   /* Actual ACL rules should not be specified in this class. The rules here are strictly a 
		  default in case they are not specified in the actualy controller class.
		
		  Format is array('action' => 'member_type(s)')
		
		  action may be:
			   string -- to specify an action name in this controller 
			   '*'    -- to specify all actions not matching any other ACL rule

		  member_type(s) may be one of:
			   string   -- to require a single type 
			   array    -- to require one of multiple times (boolean OR)
			   '*'      -- to require any LOGGED-IN member type
			   'public' -- accessible to everyone.


		  For optimistic security (granted by default), leave "'*' => 'public'" at the end.
		  For pessimistic security (denied by default), don't include "'*' => 'public'".
		  Remember that types also inherit from parent types via the inherits_from_type_id property!
		
		*/
		
		'*' => 'public'
	);
	
	protected function loggedInMember() {
		return Member::getActive();
	}

	protected function setLoggedInMember(Member $member) {
		$sso = new Zend_Session_Namespace('sso');		
		// Make sure we've authenticated the same user via Facebok and via SSO
		if ($sso->loginid) {
			if (!$member->saml_login || ($member->saml_login->username != $sso->loginid) ) {
				$this->Redirect('/member/ssomismatch');
			}
		}
		return Member::setActive($member);
	}
	
	protected function isLoggedIn() {
		return Member::isLoggedIn();
	}
	
	protected function setLoginRedirect($url) {
		$_SESSION['REDIRECT_AFTER_LOGIN'] =	$url;
	}

	public function redirectIfNotLoggedIn($url = '/index', $redirectAfterLogin = null)
	{
		if ( !isset($_SESSION['member']) || !($_SESSION['member'] instanceOf Member) ) {
			if ($redirectAfterLogin === null) $redirectAfterLogin = $_SERVER['REQUEST_URI'];
			if ($redirectAfterLogin) $this->setLoginRedirect($redirectAfterLogin);
			$_SESSION['NEEDS_LOGIN'] =	true;
			$this->_redirect($url);
		}
	}
	
	public static function getACL() {
		return static::$acl;
	}
	
	public function checkACL($action = null) {
		$access = new Blastit_Access_Cityblast;
		return $access->isPermitted();
	}

	protected function memberIs($types, $inherits = false)
	{
		if (!isset($_SESSION['member'])) return false;
		if (!is_array($types)) $types = array($types);
		foreach ($types as $type) {
			if ($_SESSION['member']->hasType($type, $inherits)) return true;
		}
		return false;
	}

	protected function loginRedirect() {
		unset($_SESSION['REDIRECT_AFTER_LOGIN']);
		$this->_redirect($this->getLoginRedirectUrl());
	}

	protected function getLoginRedirectUrl() {
		if (isset($_SESSION['REDIRECT_AFTER_LOGIN'])) return $_SESSION['REDIRECT_AFTER_LOGIN'];
		if ($this->memberIs('affiliate')) {
			return '/member/settings';		
		} elseif ($this->memberIs('odesk') || $this->memberIs('assistanteditor')) {
			return '/admin/pending';		
		} else {
			return '/member/settings';
		}
	}
	
	protected function getLoginUrl() {
		if ($this->loggedInMember() && $this->loggedInMember()->saml_login) {
			return $_SESSION['member']->saml_login->authenticationURL();
		}
		return '/member/authenticate';
	}
	
	/* 
	 * $message: Text of the message.
	 * $namespace: Can be any flashMessenger namespace. Values "success" and "error" are shown in the layout and "colorbox" is displayed in a colorbox on page load.
	 */
	
	public function addMessage($message, $namespace = 'success') {
		$flashMessenger = $this->_helper->FlashMessenger;
		$flashMessenger->setNamespace($namespace)->addMessage($message);
	}
	
	public function clearMessages($current = true, $namespaces = array('success', 'error', 'colorbox')) {
		if (!is_array($namespaces)) $namespaces = array($namespaces); 
		foreach ($namespaces as $namespace) {
			$this->_helper->FlashMessenger->setNamespace($namespace)->clearMessages();
			if ($current) $this->_helper->FlashMessenger->setNamespace($namespace)->clearCurrentMessages();
		}
	}
	
	public function checkAppPrivacy() {
		if ($this->loggedInMember() && $this->loggedInMember()->facebook_app_visibility != null && $this->loggedInMember()->facebook_app_visibility != 'EVERYONE') {
			$this->addMessage('We need additional permissions for our connection to your Facebook account to function correctly. <a href="/member/fixvisibility" class="btn btn-danger pull-right">Fix now</a>', 'error');
		}
	}
	
	public function init()
	{
		if ($this->loggedInMember() instanceOf Member) {
			$this->loggedInMember()->reload();
		}

		$this->view->addScriptPath(APPLICATION_PATH.'/views/common/scripts');
		$this->view->addScriptPath(APPLICATION_PATH.'/layouts/common/scripts');
		$this->checkAppPrivacy();
		
		$this->view->flash_success  = array_unique($this->_helper->FlashMessenger->setNamespace('success')->getMessages()); 
		$this->view->flash_error   	= array_unique($this->_helper->FlashMessenger->setNamespace('error')->getMessages()); 
		$this->view->flash_colorbox = array_unique($this->_helper->FlashMessenger->setNamespace('colorbox')->getMessages()); 

		if( !empty($_SESSION["selectedCity"]) && !empty($_SESSION['updatecity']) )
		{
		    $t =    setcookie(
							'selectedcity',
							$_SESSION["selectedCity"],
							time()+ 10*365*24*60*60, 
							'/'
					);
		}

		if(	stristr(APPLICATION_ENV, "cityblast")	)
		{			
			$this->view->title = "CityBlast: Social Experts for your Real Estate Business";
			$this->view->description = "CityBlasts'Social Media Experts will manage your social media for less than $2/day. Real Estate Agents: turn your Facebook, Twitter and LinkedIn into an explosive growth engine with CityBlast!";
			$this->view->keyword = "Social Media Marketing Experts Real Estate Realtors Lead Generation CityBlast";
		}
		elseif(	stristr(APPLICATION_ENV, "insuranceposters")	)
		{	
			$this->view->title = "Insurance Posters - Social Media Assistants for your Fanpage and more!";
			$this->view->description = "Insurance Posters - Social Media Assistants for your Fanpage and more!";
			$this->view->keyword = "Insurance Posters - Social Media Assistants for your Fanpage and more!";	
		}
		elseif(	stristr(APPLICATION_ENV, "socialcapital")	)
		{	
			$this->view->title = "SocialCapital - We're networking, even when you're not!";
			$this->view->description = "SocialCapital - We're networking, even when you're not!";
			$this->view->keyword = "SocialCapital - We're networking, even when you're not!";	
		}
		elseif(	stristr(APPLICATION_ENV, "myeventvoice")	)
		{	
			$this->view->title = "MyEventVoice - Social Media Assistants for your Fanpage and more!";
			$this->view->description = "MyEventVoice - We're networking, even when you're not!";
			$this->view->keyword = "MyEventVoice wedding event planning social media marketing Facebook Twitter LinkedIn";	
		}
		else
		{
			$this->view->title = "MortgageMingler: Social Media Virtual Assistant&reg;";
			$this->view->description = "MortgageMingler is your Social Media Virtual Assistant. We keep your Facebook, Twitter and LinkedIn looking up to date and professional!";
			$this->view->keyword = "Mortgage Lead Generator Social Media Virtual Assistant";			
		}

		$this->view->params = $this->params = $this->getRequest()->getParams();

		//Set a 30 day cookie for an AFFILIATE
		if(isset($this->params['aff_id']) && !empty($this->params['aff_id']))
		{
			setcookie(
						'cityblast_aff_id',
						$this->params['aff_id'],
						time()+ time()+60*60*24*30, 
						'/'
			);		
			$referer_id		= $this->_getParam('aff_id');
			$referer_session	= new Zend_Session_Namespace("Referer");
			$referer_session->id= $referer_id;													
		}
		
		$this->view->controller_name = $this->params['controller'];
		$this->view->action_name = $this->params['action'];
		
		$this->documentRoot       =   APPLICATION_DOCROOT;
		$this->view->documentRoot =   APPLICATION_DOCROOT;

		$network_app_facebook = Zend_Registry::get('networkAppFacebook');
		$facebook = $network_app_facebook ? $network_app_facebook->getFacebookAPI() : null;
      					
		$this->view->facebook = $this->facebook = $facebook;
		
		if ($this->loggedInMember())
		{
			$member = $this->loggedInMember();
			
			//Check to see if you're an administrator
			$this->view->admin = 0;
			if ($member->hasType('superadmin')) {
				$this->view->admin = 1;
			}	 

			$this->getSelectedCity();
			$this->setSelectedCity($member->default_city->id, $member->default_city->name, $member->default_city->country);
      	}
      	elseif(	!empty($_SESSION["selectedCity"]) && 	(!isset($this->view->selectedCityName) || empty($this->view->selectedCityName)) )
		{
	
			$city = City::find($_SESSION["selectedCity"]);
			$this->setSelectedCity($city->id, $city->name, $city->country);
		}

		if (!$this->checkACL()) {
			if ($this->isLoggedIn()) {
				$this->addMessage('You do not have permssion to access "' . $_SERVER['REQUEST_URI'] . '"', 'error');
				$this->_redirect('/');
			} else {
				$this->setLoginRedirect($this->getRequest()->getRequestUri());
				$this->_redirect( $this->getLoginUrl() );
			}
		}

		// If no UID present, must not have completed Facebook signup process 
		// (e.g. because it was provisioned by SOAP, and member authenticated via SAML)

		$sso = new Zend_Session_Namespace('sso');

		if (!$this->isLoggedIn() && $sso->loginid) {
			$request = $this->getRequest();
			if (false == (($request->getControllerName() == 'member') && in_array($request->getActionName(), array('index', 'clientfind-signup', 'clientfind-signupis', 'signup', 'signupis')) )) {
				$saml = SAMLLogin::find_by_username($sso->loginid);
				if ($saml && !$saml->member->uid) {
					$this->Redirect('/member/');
				}
			}
		}
	
		//Member Count
		$this->view->member_count =  Member::count();
	
		//FB Count
		$query = "select sum(facebook_friend_count) as facebook_count from member where facebook_publish_flag=1 and status='active'";
		$result = Member::connection()->query($query)->fetch();
		$this->view->facebook_count = $result['facebook_count'];					
				
		//Twitter Count
		$query = "select sum(twitter_followers) as twitter_count from member where twitter_publish_flag=1 and status='active'";
		$result = Member::connection()->query($query)->fetch();
		$this->view->twitter_count = $result['twitter_count'];	

		//Linkedin Count
		$query = "select sum(linkedin_friends_count) as linkedin_count from member where linkedin_publish_flag=1 and status='active'";
		$result = Member::connection()->query($query)->fetch();
		$this->view->linkedin_count = $result['linkedin_count'];

		//Fanpages Count
		$query = "select sum(likes) as fanpages_count from fanpage";
		$result = Member::connection()->query($query)->fetch();
		$this->view->fanpages_count = $result['fanpages_count'];

		$this->view->total_count = $this->view->twitter_count + $this->view->facebook_count + $this->view->linkedin_count + $this->view->fanpages_count;
	
		if(	$this->params['controller'] == "admin" or $this->params['controller'] == "publishsettings" ) {			
			$this->view->cities = City::find('all', array('order' => 'name'));			
		} else {
			$this->view->cities = City::find('all', array('conditions' => 'UPPER(name) != "CANADA" and UPPER(name) != "UNITED STATES"', 'order' => 'name'));
		}
				
	}


	public function getSelectedCityCookie()
	{
	    return Member::getcity()!==false ? Member::getcity() : $this->getSelectedCity();
	}

	public function getSelectedAdminCity()
	{			
		$this->view->admin_selectedCity 		= null;
		$this->view->admin_selectedCityName 	= null;
		$this->view->admin_selectedCountry 	= null;
		
		if(isset($_SESSION['admin_selectedCountry'])) $this->view->admin_selectedCountry 	= $_SESSION['admin_selectedCountry'];
		if(isset($_SESSION['admin_selectedCity'])) $this->view->admin_selectedCity			= $_SESSION['admin_selectedCity'];
		if(isset($_SESSION['admin_selectedCityName'])) $this->view->admin_selectedCityName	= $_SESSION['admin_selectedCityName'];
					    	
	    	return $this->view->admin_selectedCity;
	}

	public function getSelectedCity()
	{			
		
	    	$selectedCity		=   empty($_SESSION["selectedCity"]) ? "*" : $_SESSION["selectedCity"];
	    	$selectedCityName	=   empty($_SESSION["selectedCityName"]) ? "Select Your City" : $_SESSION["selectedCityName"];

  	
	    	$this->view->selectedCity 	= $selectedCity;
	    	$this->view->selectedCityName = $selectedCityName;
	    	
	    	return $selectedCity;
	}

	public function getSelectedCountry()
	{
		$selectedCountry = null;
	    	if(isset($_SESSION["selectedCountry"])) $selectedCountry = $_SESSION["selectedCountry"];
	    
	    	return $selectedCountry;
	}

	public function setSelectedCity($id, $name, $country)
	{
	    	$_SESSION["selectedCity"]	=  $this->view->selectedCity 		= $id;
	    	$_SESSION["selectedCityName"]	=  $this->view->selectedCityName 	= $name;
		$_SESSION["selectedCountry"]	=  $this->view->selectedCountry 	= $country;
	    	$_SESSION['updatecity']		=  true;	  	
	}


	#
	# Allows us to render a script without a layout
	# Convenience for rendering during an XmlHttpRequest
	#
	protected function renderPartial($path)
	{
		$this->_helper->layout->disableLayout();
		$this->renderScript($path);
	}
  
	protected function renderText($text)
	{
		$this->getResponse()->appendBody($text);
		$this->renderNothing();
	}
  
	protected function renderNothing()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
	}
	
	protected function renderJS($js)
	{
		$this->renderText("<script type='text/javascript'>{$js}</script>");
	}
	
	
  
  
  	protected function setPaginator($model, $options, $track = NULL)
  	{
    		$this->paginator = $this->view->paginator = new Zend_Paginator(
      		new Zend_Paginator_Adapter_Active_Record($model, $options)
    		);
    
    		if($track)
      		$page = $this->paginator->getAdapter()->getPageFromID($track);
      
    		$page = isset($this->params['page']) ? $this->params['page'] : ((empty($page)) ? 1 : $page);
    		$this->view->current_page = $page;
		
		$track_count = ($this->params['controller'] == "listen")?TRACK_PAGE_COUNT:LEADERS_PAGE_COUNT;
    
		#
		# We set the current page number for the paginator, then call getCurrentItems which 
		# will will make the db call for our items with the limit calculated automatically
		# merged the the conditions from the supplied options.
		#
		$this->view->tracks = $this->paginator->setItemCountPerPage($track_count)->setCurrentPageNumber($page)->getCurrentItems();
		$this->view->track_ids = $this->track_ids = $this->getTrackIDS($this->view->tracks);
  	}
  

  



    /*
       Validation for input parameter
      
    */
    protected function validateParams($requireParams)
    {
      $requestParams = $this->params ;
      $flag = true;
      $error = array();
      foreach($requireParams as $key=>$value){				 
          switch($value){
               case self::STRING:									 
                   if($this->isBlank($key) || !is_string($requestParams[$key])){
                       $error[$key] = true;
                       $flag = false;
                   }
                   break;
               case self::INT:
                   if($this->isBlank($key) || ! $this->isInt($requestParams[$key])){
                        $error[$key] = true;
                        $flag = false;
                   }
                   break;
               case self::FLOAT:
                   if($this->isBlank($key) || !is_float($requestParams[$key])){
                        $error[$key] = true;
                        $flag = false;
                   }
                   break;
               case self::APP_DATE:
                   if($this->isBlank($key) || !$this->isDate($requestParams[$key])){
                        $error[$key] = true;
                        $flag = false;
                   }
                   break;

               case self::EMAIL:

                    if($this->isBlank($key) || !eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $requestParams[$key])){
                        $error[$key] = true;
                        $flag = false;
                   }
                   break;
          }
      }
      $this->view->errors=$error;
      return $flag;
    }

    private function isBlank($paramName)
    {
        if($this->params[$paramName]==""){
            return true;
        }
        return false;
    }


    private function isInt($value)
    {
        if(eregi("^[0-9]+$",$value))
            return true;
        else
            return false;
    }


    private function isDate($input)
    {
        $input = trim($input);
        $time = strtotime($input);
        if(date("Y-m-d H:i:s",$time) == $input){
            return true;
        }
        return false;
    }
	
	/**
	 * Change the redirect to a javascript location so that firefox wont rewind post requests.
	 *
	 * @param string $url The /controller/action string
	 */
	public function redirect($url, array $options = array())
	{
		echo "<script>document.location = '{$url}';</script>";
		die;
	}
	
	protected function auto_complete_cities()
	{
		if ($this->view->cities) 
		{
			$city_autocomplete_data	= array();
			foreach ($this->view->cities as $city) 
			{
				$city_autocomplete_data[]	= '{city: "'. $city->name .'", city_id: "'. $city->id .'", country: "'. $city->country .'"}';
			}
			$this->view->city_autocomplete_data	= $city_autocomplete_data;
		}
		
	}

	protected function getRecentBlasts($number_of_recent_blasts=5, $conditions = null)
	{
		$recent_blasts	= Listing::all(array(
			"select" => "distinct *",
			"conditions" => "property_status NOT IN ('SOLD','EXPIRED') ".
				"AND approval=1 and blast_type='".Listing::TYPE_LISTING."' " . $conditions,
			"order" => "id DESC",
			"limit" => $number_of_recent_blasts)
		);

		return $recent_blasts;
	}
	
	//Use affiliate layout if available 
	public function useAffiliateLayout()
	{
		$disable_default_layout = false;
		$layout = $this->_helper->layout;

	
		$layout_name = 'aff-' . $this->getCurrAffiliateId();
		$layout_filename = $layout->getInflector()->filter(array('script' => $layout_name));

		$view = $layout->getView();
	
		if (null !== ($path = $layout->getViewScriptPath())) 
		{
			if (method_exists($view, 'addScriptPath')) 
			{
				$view->addScriptPath($path);
			} else {
				$view->setScriptPath($path);
			}
		} 
		elseif (null !== ($path = $layout->getViewBasePath())) 
		{
			$view->addBasePath($path);
		}
	
		$result = false;
		
		if ($view->getScriptPath($layout_filename)) 
		{
			// Affiliate layout exists
			$layout->setLayout($layout_name);
			$result = true;
		} 
		elseif ($disable_default_layout) 
		{
			$layout->disableLayout();
		}
	
		return $result;
	}
	
	//Render affiliate specific view if available otherwise render the default view
	public function renderAffiliateView($action = null)
	{
		$request = $this->getRequest();
        if (null === $action) 
        {
            $action = $request->getActionName();
        } elseif (!is_string($action)) {
            require_once 'Zend/Controller/Exception.php';
            throw new Zend_Controller_Exception('Invalid action specifier for view render');
        }
        
		$affiliate_view_name = $this->getViewScript($action . '-aff-' . $this->getCurrAffiliateId());
		if ($this->view->getScriptPath($affiliate_view_name)) 
		{
			$this->renderScript($affiliate_view_name);
		} else {
			$this->render($action);
		}
	}
	
	public function getCurrAffiliateId() 
	{
		$affiliate_id	= null;
		if(isset($_COOKIE['affiliate_id']))
			$affiliate_id = $_COOKIE['affiliate_id'];

		if(isset($_COOKIE['cityblast_aff_id']))
			$affiliate_id = $_COOKIE['cityblast_aff_id'];

		if (!$affiliate_id || !is_numeric($affiliate_id))
		{
			$referer_session	= new Zend_Session_Namespace("Referer");

			if ($referer_session->__isset('id'))
			{
				$affiliate_id   = $referer_session->id;
			}
		}

		return $affiliate_id;
	}
	
	public function redirectIfNoView($to) 
	{
		$script = $this->view->getScriptPath( $this->_helper->viewRenderer->getViewScript() );
		if (is_readable($script)) {
			return;
		}
		$this->_redirect($to);
	}

	public function clearCurrAffiliateId() 
	{
		// remove session & cookie to avoid re-credit
		if (isset($referer_session) && $referer_session->__isset('id')) 
		{
			$referer_session->__unset('id');
		} elseif (isset($_COOKIE['affiliate_id'])) {
			setcookie('affiliate_id', null, time() - 3600);
		} elseif (isset($_COOKIE['cityblast_aff_id'])) {
			setcookie('cityblast_aff_id', null, time() - 3600);
		}
	}
	
	protected function getFlashSuccessHtml($messages)
	{
		$html = '';
		
		if (!empty($messages))
		{
			$html .= '<h1 style="margin-top: 0px; padding-top: 0px;"><img src="/images/checkmark1.png" style="width: 30px; margin: 4px 10px 0 0;" />Success!</h1>';
			foreach ($messages as $message)
			{
				$html .= '<p style="font-size: 130%; margin-top: 12px;">'.$this->view->escape($message).'</p>';
			}
		}
		
		return $html;
	}
	


	public function _join()
	{
		$this->view->has_comments = 1;
		$this->view->member_type = $this->_getParam('mtype');
		$this->view->is_royal = 0;
		
		if ($this->_getParam('is_royal')) {
			$this->view->is_royal = 1;
			$this->view->cities = City::find('all', array('conditions' => 'UPPER(name) != "CANADA" and UPPER(name) != "UNITED STATES" and UPPER(country) = "CANADA" ', 'order' => 'name'));
		}

		$this->getSelectedCity();

		if (!$this->view->selectedCity) {
			$this->view->selectedCity = 0;
			$this->view->selectedCityName = "Select Your City";
		}

		$this->view->show_city_error = false;
		if (isset($_SESSION['city_missing']) && $_SESSION['city_missing'] == true) {
			$this->view->show_city_error = true;
			unset($_SESSION['city_missing']);
		}

		$failed_permission = intval($this->_getParam('failed_permission'));
		$this->view->failed_permission = $failed_permission;
		$signupSession = new Zend_Session_Namespace('signup');

		if (!empty($signupSession->default_city)) {
			$this->view->selectedCity = $signupSession->default_city;
		}
		
		if (isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1183) mail("alen@alpha-male.tv","FB Click - Join Page","");
		if (isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1184) mail("alen@alpha-male.tv","Google Click - Join Page","");
	
		$this->view->has_comments = 1;

	}		
}

?>
