<?php

class SSOController extends ApplicationController
{
	
	protected $sourceId = null;
	
	protected static $acl = array(
		'*' => 'public'
	);

	public function init() {
		$this->sourceId = $this->getRequest()->getParam('idp');
		if (!$this->sourceId) $this->Redirect('/');
		$this->renderNothing();
	}

	public function index()
	{

	}
	
	public function logout() {
	
		$as = new SimpleSAML_Auth_Simple($this->sourceId);
		if ($as->isAuthenticated()) {
			$as->logout('/');
		}

	}
	
	public function saml2() {

		$config = SimpleSAML_Configuration::getInstance();

		$sourceId = $this->sourceId;

		if ($sourceId == 'clareity' || $sourceId == 'store1') $sourceId = 'clareity-qa';
		$as = new SimpleSAML_Auth_Simple($sourceId);

		if (array_key_exists(SimpleSAML_Auth_State::EXCEPTION_PARAM, $_REQUEST)) {
			$state = SimpleSAML_Auth_State::loadExceptionState();
			assert('array_key_exists(SimpleSAML_Auth_State::EXCEPTION_DATA, $state)');
			$e = $state[SimpleSAML_Auth_State::EXCEPTION_DATA];
			throw $e;
		}

		if (!$as->isAuthenticated()) {
			$params = array(
				'ErrorURL' => '/sso/saml2/' . $this->sourceId,
				'ReturnTo' => '/sso/saml2/' . $this->sourceId,
			);
			$as->login($params);
		}

		$attributes = $as->getAttributes();
		
		if (isset($attributes['loginid'])) {
		
			if ($saml = SAMLLogin::find_by_username($attributes['loginid'])) {
				
				$sso = new Zend_Session_Namespace('sso');
				foreach ($attributes as $key => $value) {
					$sso->$key = (is_array($value) ? $value[0] : $value);
				}
				
				$fblogin = false;
				
				if (!$this->loggedInMember()) {
					
					$fblogin = true;
					
				} elseif ($this->loggedInMember()->id != $saml->member->id) {
				
					// SSO authenticated member does not match Facebook authenticated member!
					// Don't allow login.
					
					Zend_Session::destroy();
					$this->Redirect('/member/logout?redirect=/member/ssomismatch');

				}

				if ($fblogin) {
					$this->Redirect('/member/login');
					echo 1;
				} else {
					$this->loginRedirect();
					echo 2;
				}
				
			} else {
				
				// SSO authenticated member username not found in database!
				// Don't allow login.

				$emailer = Zend_Registry::get('alertEmailer');
				$emailer->send('SSO user not found', "Member " . $member->id . ' tried to log in via SSO with username "' . $attributes['loginid'] . "\", but that username does not exist in the saml_login table.\n\nSSO attributes:" . print_r($attributes, true), 'sso-member-not-found');

				Zend_Session::destroy();
				$this->Redirect('/member/logout?redirect=/member/ssonotfound');
				
			}
			
		}
		
		throw new Exception('SAML login failed');

	}
	
}