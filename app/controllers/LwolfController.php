<?
class LwolfController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		parent::init();
	}

	public function sync()
	{
		$lw_id = $this->_getParam('lw_id');

		$singlesignonSession	=   new Zend_Session_Namespace("SingleSignOn");		
		$singlesignonSession->external_key = $lw_id;


		$_SESSION['REDIRECT_AFTER_LOGIN'] = "/lwolf/success";		
	}
	
	function success()
	{
		$this->view->$member = $member = Member::find($_SESSION['member']->id);
	}

}
?>