<?php

class MemberController extends ApplicationController
{
	CONST MEMBER_FREE_CREDITS   =	1;

	CONST TAXES		=   "0.13"; // 13%
	CONST CURRENCY		=   "CAD";	


	protected static $acl = array(
		'fanpages' 			 => 'superadmin',
		'update' 			 => 'superadmin',
		'update_details' 	 => 'superadmin',
		'cancelaccount' 	 => '*',
		'settings' 			 => '*',
		'tab_booking' 		 => array('agent', 'broker'),
		'tab_settings' 		 => array('agent', 'broker', 'affiliate'),
		'updatecontentprefs' => array('agent', 'broker'),
		'updatetestimonial'  => '*',
		'tab_contact' 		 => '*',
		'tab_invoices' 		 => array('agent', 'broker'),
		'tab_affiliates' 	 => 'affiliate',
		'tab_dashboard' 	 => array('agent', 'broker'),
		'tab_listings' 		 => array('agent', 'broker'),
		'tab_testimonial'    => '*',
		'tab_affiliate_dashboard' => array('affiliate'),
		'tab_referagent' 	 => array('agent', 'broker', 'affiliate'),
		'tab_thankyou' 	 	 => '*',
		'settings_fanpages'  => array('agent', 'broker', 'affiliate'),
		'fixvisibility'  	 => '*',
		'*' => 'public'
	);

	public function test()
	{
		echo "<h1>Hello</h1>";

		require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

		
		KissMetrics::init(KISSMETRICS_API_KEY);
		KissMetrics::identify("bob@bob.com");
		
			KissMetrics::record('Step 1 Click');
			
		echo "Great success!<BR>";
		
		exit();	
	}
	
	public function broker()
	{		
		$signupSession  = new Zend_Session_Namespace('signup');
		$signupSession->member_type = "BROKER";

		$member_type = MemberType::find_by_code('BROKER');
		
		$signup_urls = array(
			'month' => '/member/set_signup_billing_cycle/billing_cycle/month',
			'biannual' => '/member/set_signup_billing_cycle/billing_cycle/biannual',
			'annual' => '/member/set_signup_billing_cycle/billing_cycle/annual'
		);
		
		$this->view->member_type = $member_type;
		$this->view->signup_urls = $signup_urls;

		$this->recordKissMetrics('Viewed Broker Pricing');
		
		$this->renderScript('index/pricing.html.php');
		
	}

	protected function recordKissMetrics($event) {
	
		//KISS METRICS
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record($event);
		}	

	}

	public function index()
	{
		$params = $this->_request->getParams();
		$signupSession  = new Zend_Session_Namespace('signup');
		$mtype = !empty($params['mtype']) ? strtoupper($params['mtype']) : 'AGENT';
		$signupSession->member_type = in_array($mtype, array('AGENT', 'BROKER')) ? $mtype : 'AGENT';
		
		$tag_groups = TagGroup::find('all');
		
		$member_type = MemberType::find_by_code($signupSession->member_type);


		$tag_group_data = array();
		if (!empty($tag_groups)) 
		{
			foreach ($tag_groups as $group) 
			{
				$group_tags = Tag::findAllByMemberTypeAndTagGroup($member_type, $group);
				$tag_group_data[] = array(
					'group' => $group,
					'tags' => $group_tags
				);
			}
		}
		$this->view->tag_groups = $tag_group_data;
		
		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed Try Us Now');
		}

	}

	public function set_signup_billing_cycle()
	{
		$params = $this->_request->getParams();
		
		$signupSession  = new Zend_Session_Namespace('signup');
		$signupSession->billing_cycle = !empty($params['billing_cycle']) ? $params['billing_cycle'] : null;

		$this->_redirect("/member/index");


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Changed Billing To: ' . $params['billing_cycle']);
		}

	}

	public function what_is_cityblast()
	{
		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed What Is '.COMPANY_NAME);
		}		
	}

	public function tmpstoresettings()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout()->disableLayout();
			
		$params = $this->_request->getParams();

		$tmp_member_settings = new Zend_Session_Namespace('tmp_member_settings');
		
		$tmp_member_settings->city 			 = $params['locality'];
		$tmp_member_settings->state 		 = $params['administrative_area_level_1'];
		$tmp_member_settings->country 		 = $params['country'];
		$tmp_member_settings->location_input = $params['location_input'];
		
		$tmp_member_settings->frequency_id = $params['frequency_id'];			

		$tag_ids = array();
		if (!empty($params['tag_ids'])) {
			foreach($params['tag_ids'] as $tag_id) {
				$tag_ids[$tag_id] = $tag_id;
			}		
		}
		unset($tmp_member_settings->tag_ids);
		$tmp_member_settings->tag_ids = $tag_ids;
	}

	public function sunshine()
	{
		/****************************************
		$this->_helper->layout->setLayout('wizard');
		
		$this->view->promo_code = "SUNSHINE";
		$_SESSION['REDIRECT_AFTER_LOGIN'] = "/sunshine";

		$request = $this->getRequest();
		if ($request->isPost()) 
		{
			$this->_redirectToAuthentication(); 
				
			$member = Member::find($_SESSION['member']->id);

			if ($member->new_stripe_card($params)) {
				$_POST['fname'] = $member->first_name;
				$_POST['lname'] = $member->last_name;

				
				$member->saveAuthorization($_POST);
				
				
				//Next Payment Date = today + 30 days
				$date	   =	new DateTime;
				$date->modify('+30 days');
				
				$member->next_payment_date	   =	$date;
			

				//New Price = 20% off UNIT PRICE
				$member->price =  $member->price * 0.8;
				
				$member->update_account_status(false);

				$member->save();
				
				$this->_redirect("/member/settings");
			}
		}
		***********************/
		
		$this->_redirect("/member/settings");
		
	}

	public function join()
	{
		//getScriptPath
		/*********************		
		if ($this->_getParam('wizard_responsive')) {
			$this->_helper->layout->setLayout('wizard-responsive');
		} else {
			$this->_helper->layout->setLayout('wizard');		
		} *******************/
		
		
				
		$this->view->has_comments = 1;
		
		// Defining aditional meta
		$permalink  =	APP_URL . "/member/index";
		$aditional_meta	=   '
			<meta property="og:url" content="'.$permalink.'"/>
			<meta property="og:site_name" content="'.APP_URL.'"/>
			<meta property="og:title" content="'.COMPANY_NAME.': Member Join Page"/>';

		$this->view->aditional_meta =	$aditional_meta;
		$this->view->city_id_passthru = NULL;
		if (isset($_REQUEST['city_id'])) $this->view->city_id_passthru = $_REQUEST['city_id'];
		
		
		

		$params = $this->_request->getParams();
		$signupSession  = new Zend_Session_Namespace('signup');
		$mtype = !empty($params['mtype']) ? strtoupper($params['mtype']) : 'AGENT';
		$signupSession->member_type = in_array($mtype, array('AGENT', 'BROKER', 'AFFILIATE')) ? $mtype : 'AGENT';
	
		$tag_groups = TagGroup::find('all');
		
		$member_type = MemberType::find_by_code($signupSession->member_type);
		
		$tag_group_data = array();
		if (!empty($tag_groups)) 
		{
			foreach ($tag_groups as $group) 
			{
				$group_tags = Tag::findAllByMemberTypeAndTagGroup($member_type, $group);
				$tag_group_data[] = array(
					'group' => $group,
					'tags' => $group_tags
				);
			}
		}



		
		$this->view->tag_groups = $tag_group_data;			
		
		
		
		$this->_join();
		
		//KEVIN, I added this condition to make the funnels work properly again, please leave it in!
		/**************
		if (!($this->_getParam('wizard_responsive'))) {		
			$this->useAffiliateLayout();
			$this->renderAffiliateView();
		} *************/



	}

	public function paymentrenewal()
	{
		$this->redirect("/member/payment?rebill=1");	
	}
			
	public function cancelaccount(){}

	public function cancelconfirm()
	{
		if (isset($_SESSION['member']) && !empty($_SESSION['member']))
		{		
			$member = $_SESSION['member'];
			
			$subject 	= "Cancel Account Request: ".$member->id . ": ".$member->first_name." " . $member->last_name;
			$body	= "REASON: " . $this->_getParam('reason') . "\n\n<BR>\n\n" .APP_URL."/admin/member/".$member->id;
			
			mail(HELP_EMAIL, $subject, $body);
		
			$cancel_account = new CancelAccount();
			$cancel_account->member_id = $member->id;
			$cancel_account->status = "new";
			
			$cancel_account->save();
		}		
	}

	public function settings()
	{
		$this->view->page = $this->getParam('page', 1);
		$this->view->has_comments = 1;
		$this->view->isAjax = $this->getRequest()->isXmlHttpRequest();
		
		if (isset($_SESSION['member']) && !empty($_SESSION['member'])) {
			$this->view->member	= $member = Member::find_by_uid($_SESSION['member']->uid);

			if (is_array($member->payments)) {
				$this->view->payments = $member->payments;
			} else {
				$this->view->payments = null;
			}
			
		} 
		else 
		{
			$this->view->payments	=   null;
		}
		
		$conditions = array(
			'conditions' => array(
				"member_id = ? AND blast_type IN ('".Listing::TYPE_IDX."', '".Listing::TYPE_LISTING."') ".
				"AND reblast_id IS NULL", 
				$_SESSION['member']->id
			)
		);
		$this->view->listings = Listing::count($conditions);
		
		$conditions = "member_id = ".$_SESSION['member']->id." AND viewed = 0";
		$this->view->new_bookings = Booking::count('all', array('conditions' => $conditions));
		//find_all($_SESSION['member']->id, array("order" => "id DESC"));

		$conditions = "member_id = ".$_SESSION['member']->id." AND viewed = 1";
		$this->view->total_bookings = Booking::count('all', array('conditions' => $conditions));
		
		

		$this->view->new_registry = false;
		if (isset($_SESSION['new_registry']) && $_SESSION['new_registry'])
		{
			$this->view->new_registry = true;
			unset($_SESSION['new_registry']);
		}

		$this->view->thankyou = 0;
		$this->view->is_new_paying_member = 0;
		if ($this->_getParam('thankyou')) 
		{
			$this->view->thankyou = 1;
			
			// If the member has recently set their card details for the first time they are a new paying member
			$oldest_payment = Payment::find(array(
				'conditions' => "member_id='" . addslashes($member->id) . "'", 
				'limit' => 1, 
				'order'=>'id asc'
			));
			if ($oldest_payment) 
			{
				$interval = $oldest_payment->created_at->diff(new DateTime(), true);
				$this->view->is_new_paying_member = (
					$interval->y == 0 &&
					$interval->m == 0 &&
					$interval->d == 0 &&
					$interval->h == 0 &&
					$interval->i < 2
				);
			}
		}
		
	}

		
	public function tab_booking()
	{		
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
		
		$bookings = Booking::find_all_by_member_id($_SESSION['member']->id, array("order" => "id DESC"));
		foreach($bookings as $booking){
			$booking->viewed = 1;
			$booking->save();
		}
		
		$listings_content_pref_tag = ContentPref::findForMemberAsTagByTagRefCode($_SESSION['member']->id, 'LISTINGS');
		$this->view->listings_enabled = ($listings_content_pref_tag && $listings_content_pref_tag->pref);
		
		echo "<script type='text/javascript'>$('#new_bookings_bubble').remove();</script>";
		$this->view->bookings = $bookings;
	
	}

	public function tab_settings()
	{
		$this->settings();

		$member = $_SESSION['member'];
		$this->view->hasFacebookPublishPermissions	=	$member->isPublishPermissionOn();
		$this->view->hasManagePagesPermission 		= 	$member->isManagePagesPermissions();
		$this->view->hasPhotoPermission 			= 	$member->hasPhotoPermissions();
		$this->view->lastFacebookPost 				=	$member->getLastPublishedPost('FACEBOOK');
		$this->view->lastTwitterPost 				=	$member->getLastPublishedPost('TWITTER');
		$this->view->lastLinkedinPost 				=	$member->getLastPublishedPost('LINKEDIN');

		// Select tags from DB in one go and sort into groups
		$tags = ContentPref::findAllForMemberAsTag($member->id);
		$tag_groups = array();
		if (!empty($tags)) 
		{
			foreach ($tags as $tag) 
			{
				if ($tag->tag_group) 
				{
					if (!isset($tag_groups[$tag->tag_group->id])) {
						$tag_groups[$tag->tag_group->id]['group'] = $tag->tag_group;
					}
					$tag_groups[$tag->tag_group->id]['tags'][] = $tag;
				}
			}
		}
		$this->view->tag_groups = $tag_groups;

		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
		
	}
	
	public function updatecontentprefs()
	{
		$request = $this->getRequest();
		$tag_ids = $request->getParam('tag_ids');
		$tag_ids = !empty($tag_ids) ? array_flip($tag_ids) : array();
		
		$member = !empty($_SESSION['member']) ? $_SESSION['member'] : null;

		if (!empty($member)) 
		{
			$member->update_content_prefs($tag_ids);
			
			$success_message = $this->getFlashSuccessHtml(array('Your content preferences have been updated.'));
			$this->addMessage($success_message, 'colorbox');
		}

		$this->redirect("/member/settings#settingtab");
	}
	
	public function tab_testimonial()
	{
		$this->view->member = $member = Member::find($_SESSION['member']->id);
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
	}
		
	public function tab_contact()
	{
		$this->settings();
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
	}
		
	public function tab_invoices()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
		
		$this->invoices();
	}
	
	public function tab_affiliates()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}

		$this->view->member = $member = $_SESSION['member'];
		$affiliate_id = ($member->affiliate_account) ? $member->affiliate_account->id : null;
		
		$this->view->total_active = Member::count(array('conditions' => array(
			'referring_affiliate_id = ? AND payment_status IN (?)', 
			$affiliate_id, 
			array('signup', 'paid', 'free', 'extended_trial')
		)));
		
		$conditions = array('conditions' => array('referring_affiliate_id = ?', $affiliate_id), 'order' => 'id DESC');

		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
	}

	public function tab_dashboard()
	{

		$this->settings();		
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}

		$conditions  = " ( status = 'active' and ( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) ";
		$conditions .= " or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) and payment_status != 'cancelled' and payment_status != 'payment_failed' and (next_payment_date IS NULL or next_payment_date >= NOW()) )";
		$conditions .= " OR account_type='free' ";


		$this->view->total_active_members = Member::count('all', array('conditions' => $conditions));
		
		
		
		$this->view->member = $member = $_SESSION['member'];


		$this->view->total_clicks = $member->clicks_trailing_thirty();
		
		$this->view->clicks_today = $member->clicks_today();
		$this->view->clicks_seven = $member->clicks_trailing_seven();
		
		
		$return_array	= $member->likes_and_comments_trailing_thirty();
		$this->view->total_likes 	= $return_array['likes'];
		$this->view->total_comments 	= $return_array['comments'];

		$this->view->total_phone_calls = $member->getTotalPhoneCalls();
		


		//$this->view->avg_clicks_per_user = ($this->view->total_clicks / $this->view->total_active_members);	

		/*******************************
		$query = "select sum(facebook_likes) as total_likes from post where member_id=".$this->view->member->id;
		$likes = Post::find_by_sql($query);
		$this->view->total_fb_likes = $likes[0]->total_likes;

		$query = "select sum(facebook_comments) as total_comments from post where member_id=".$this->view->member->id;
		$comments = Post::find_by_sql($query);
		$this->view->total_fb_comments = $comments[0]->total_comments;
		***************************/
		
		//$this->view->total_blasts = Listing::count("all", array('conditions' => "member_id=".$this->view->member->id));
		
		
		//Average Member's Stats...
		$this->view->dash_stats = DashboardStats::find(1);
	}

	public function tab_affiliate_dashboard()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}

		$this->view->member = Member::find_by_id($_SESSION['member']->id);
		
		$promos = Promo::find('all', array('conditions' => 'affiliate_id='.$_SESSION['member']->id, 'order' => 'id DESC', "limit" => 1)	);
		$this->view->promos = $promos;
		$this->view->promo = ($promos) ? $promos[0] : null;
	}
	
	public function tab_listings()
	{
		$request 		= $this->getRequest();
	
		$this->settings();

		$this->view->search = $request->getParam('search', false);
		
		$member  		= $_SESSION['member'];
		$conditions 	= array(
			'select'=> "l.*, (SELECT sum(`count`) FROM click_stat_by_date_listing WHERE listing_id = l.id) AS total_clicks", 
			'from' => "listing AS l",
			'conditions' => array(
				"member_id = ? AND blast_type IN ('".Listing::TYPE_IDX."', '".Listing::TYPE_LISTING."') ".
				"AND reblast_id IS NULL", 
				$member->id
			)
		);
		
		$searchstring 	= $request->getParam('search', false);

		if ($searchstring !== false) {
			if (strlen($searchstring)) {
				$conditions['conditions'][0] .= " AND street LIKE ?";
				$conditions['conditions'][]   = "%" . $searchstring. "%";
			}
			$this->view->search = true;
		}
		
		if ($status = $request->getParam('status')) {
			switch ($status) {
				case 'active' :
					$conditions['conditions'][0] .= " AND (property_status NOT IN ('SOLD','EXPIRED') OR property_status IS NULL)";
					break;
				case 'offmarket' :
					$conditions['conditions'][0] .= " AND property_status = 'EXPIRED'";
					break;
				case 'sold' :
					$conditions['conditions'][0] .= " AND property_status = 'SOLD'";
					break;
				case 'pricechanges' :
					$conditions['conditions'][0] .= " AND property_status = 'PRICE_CHANGE'";
					break;
			}
		}

		$this->view->count = Listing::count($conditions);

		if ($this->view->count) {
			$paginator = new Zend_Paginator(new ARPaginator('Listing', $conditions));
			$paginator->setCurrentPageNumber($this->view->page);
			$paginator->setItemCountPerPage(10);
			$this->view->paginator = $paginator;
		}

		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
	}

	public function tab_referagent()
	{
		if ($this->_request->isXmlHttpRequest()){
	
			$this->_helper->layout()->disableLayout();
		}
		
		$member = $this->view->member = isset($_SESSION['member']) ? $_SESSION['member'] : null;
		
		if ($member) {
			// referral stats
			$this->view->referral_emails_sent = $member->referral_email_count ? $member->referral_email_count : 0;
			
			$conditions_sql = "referring_member_id = ?";
			$conditions = array($conditions_sql, $member->id);
			$this->view->referral_trails = Member::count('all', array('conditions' => $conditions));
			
			$conditions_sql = "referring_member_id = ? AND created_at > ? AND created_at < ?";
			$conditions = array(
				$conditions_sql, 
				$member->id, 
				'2013-05-13 09:00:00', // Date that Las Vegas comp started
				'2013-05-27 23:59:59' // Date that Las Vegas comp ends
			);
			$this->view->las_vagas_ballots = Member::count('all', array('conditions' => $conditions));
			
			$conditions_sql = "member_id = ?";
			$conditions = array($conditions_sql, $member->id);
			$this->view->referral_credits = ReferralCredit::count('all', array('conditions' => $conditions));
			$this->view->referral_credits_needed = (10 - $this->view->referral_credits);
		}
	}
	
	public function settings_fanpages()
	{
		if ($this->_request->isXmlHttpRequest()){
	
			$this->_helper->layout()->disableLayout();
		}

		$member = $_SESSION['member'];
		$this->view->hasManagePagesPermission 		= $member->isManagePagesPermissions();

		$this->view->is14DayFreeTrial				=	$member->is14DayFreeTrial();
	}
	
	public function tab_thankyou()
	{
		$this->useAffiliateLayout();
		
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
		$this->renderAffiliateView();



		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Thank-You Page');
		}	

		$email = "unknown";
		if(isset($_SESSION['member']))
		{
			$email = $_SESSION['member']->email;
			
			$member = Member::find($_SESSION['member']->id);
			$member->show_thankyou_page = 0;
			$member->save();
			
			$_SESSION['member'] =  $member;
		}

	}

	public function changepublishcity()
	{
		$params = $this->getRequest()->getParams();

		$city_name = $params['selected_city'];

		if (isset($_SESSION['member']) && !empty($_SESSION['member']) && isset($city_name) && !empty($city_name) )
		{
			$this->view->member = $member = Member::find_by_id($_SESSION['member']->id);

			//Let's check to see if this is a "blasting" city, if not, default to NATIONAL
			//$city = City::find($city_id);
			$city = City::find_by_name($city_name);

			$city_id = $city->id;
			if($city_id) {
				$member->city_id = $city->id;
				if ($city->blasting == "ON")
				{
					$member->publish_city_id = $city_id;
				}
				else
				{
					//Publish "NATIONAL"
					if ( strtoupper($city->country) == "CANADA")
					{
						$member->publish_city_id = 1102;
					}
					else
					{
						$member->publish_city_id = 1103;
					}
				}

				$member->save();

				$_SESSION['member'] = $member;
				$_SESSION['selectedCity'] = $city_id;

				$t = setcookie(
					'selectedcity',
					$_SESSION["selectedCity"],
					time()+ 10*365*24*60*60
				);

				$_COOKIE['selectedcity'] = $_SESSION['selectedCity'];
				$this->view->selectedCity = $city_id;

				$city = City::find_by_id($city_id);
				$success_message = $this->getFlashSuccessHtml(array('Your default city has been successfully changed to ' . $city->name . '.'));				
				$this->addMessage($success_message, 'colorbox');
			}
		}

		$this->redirect('/member/settings#settingtab');
	}

	public function changemarketingsettings()
	{
		$request         = $this->getRequest();
		$frequency_id    = $request->getParam('frequency_id');
		$auto_like       = $request->getParam('auto-like');
		$click_tracking  = $request->getParam('click-tracking');
		$city_name       = $request->getParam('selected_city');
		$state_name      = $request->getParam('selected_state');
		$country_name    = $request->getParam('selected_country');
		$location_input  = $request->getParam('location_input');

		if (!empty($_SESSION['member']) && !empty($frequency_id) && !empty($city_name)) {
			
			$this->view->member = $member = Member::find_by_uid($_SESSION['member']->uid);

			$city = City::all(
				array(
					'conditions' => array(
						'name = ? AND state = ?',
						$city_name,
						$state_name
					),
					'limit' => 1,
					'order' => 'id ASC'
				)
			);

			$city_id = 0;
			if (count($city) > 0) {
				$city_id = $city[0]->id;
				$this->view->selectedCity = $member->city_id = $member->publish_city_id = $city_id;
				if ($city[0]->blasting == "ON") {
					$member->publish_city_id = $city_id;
				} else{
					//Publish "NATIONAL"
					if ( strtoupper($city[0]->country) == "CANADA") {
						$member->publish_city_id = 1102;
					} else {
						$member->publish_city_id = 1103;
					}
				}
			} else {
				$city = new City();
				$city->name = $city_name;
				$city->state = $state_name;
				$city->country = $country_name;
				$city->save();

				$city_id = $city->id;

				$emailer = Zend_Registry::get('alertEmailer');
				$msg = 'Member ' . $member->id . ' has updated publishing city in settings. ' .
					'The member entered "' . $location_input . '" ' .
					'which resulted in the creation of a new city record.' . "\n\n" .
					'New City: '. $city->id . ', ' . $city->name . ', ' .$city->state . ', ' . $city->country;
				$emailer->send('Member edit settings created new city', $msg, 'member-new-city');

				$this->view->selectedCity = $member->city_id = $member->publish_city_id = $city_id;
			}

			if (is_array($member->payments)) {
				$this->view->payments = $member->payments;
			} else {
				$this->view->payments = null;
			}
			
			$member->click_tracking = ($click_tracking ? 1 : 0);
			$member->auto_like 		= ($auto_like ? 1 : 0);
			$member->frequency_id 	= $frequency_id;
			$member->save();

			unset($_SESSION['member']);
			$_SESSION['member'] 	 = $member;
			$_SESSION['selectedCity'] = $city_id;

			$t = setcookie(
				'selectedcity',
				$_SESSION["selectedCity"],
				time()+ 10*365*24*60*60
			);
			
			$success_message = $this->getFlashSuccessHtml(array('Your settings have been updated.'));
			$this->addMessage($success_message, 'colorbox');

		} else {
			$this->view->payments	=   null;
		}

		$this->redirect("/member/settings#settingtab");
	}

	public function google_how_cityblast_works()
	{
		$this->view->is_google = true;	

		$request    =   $this->getRequest();
		$cb_cid = $_SESSION['cb_cid']  = $request->getParam('cid');
		
		if ($cb_cid)
		{
			setcookie('cb_cid',$cb_cid,time()+ 10*365*24*60*60, '/');		
		}
	
		setcookie('cityblast_aff_id',1184, time()+ 10*365*24*60*60, '/');		
		setcookie('affiliate_id',1184,time()+ 10*365*24*60*60, '/');
		
		mail("alen@alpha-male.tv", "Google Click - " . COMPANY_NAME. " ","");
		
		$this->render("videojoin");
	}

	public function fb_promo()
	{
		$this->view->is_facebook = true;	

		$request    	= $this->getRequest();
		$promo_id  	= $request->getParam('promo_id');
				
		if ($promo_id)
		{
			setcookie('promo_code', $promo_id, time()+ 10*365*24*60*60, '/');						
		}

		mail("alen@alpha-male.tv", "FB Promo - " . COMPANY_NAME. " ","");
		
		$this->render("videojoin");		
	}
	
	public function fb_video_lander()
	{
		$this->view->is_facebook = true;	

		$request    	= $this->getRequest();
		$price_id  	= $request->getParam('price_id');
				
		if ($price_id)
		{
			setcookie('cb_price_id', $price_id, time()+ 10*365*24*60*60, '/');						
		}

	
		setcookie('cityblast_aff_id',1183,time()+ 10*365*24*60*60, '/');	
		setcookie('affiliate_id',1183,time()+ 10*365*24*60*60, '/');	
		
		
		$email = "unknown";
		if(isset($_SESSION['member']))
		{
			$email = $_SESSION['member']->email;
		}

		mail("alen@alpha-male.tv", "FB Click - " . COMPANY_NAME . " ".$email,"");


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed FB Video Lander');
		}
		
		$this->render("videojoin");		
	}

	public function fb_how_cityblast_works()
	{
		$this->view->is_facebook = true;	

		$request    =   $this->getRequest();
		$cb_cid  = $request->getParam('cid');
				
		if ($cb_cid)
		{
			setcookie('cb_cid',$cb_cid,time()+ 10*365*24*60*60, '/');						
		}
	
		setcookie('cityblast_aff_id',1183,time()+ 10*365*24*60*60, '/');	
		setcookie('affiliate_id',1183,time()+ 10*365*24*60*60, '/');	
		
		$email = "unknown";
		if(isset($_SESSION['member']))
		{
			$email = $_SESSION['member']->email;
		}

		mail("alen@alpha-male.tv","FB Click - " . COMPANY_NAME. " ".$email,"");
		
		
		$this->render("how_it_works");
	}

	public function membernotfound(){}


	public function register()
	{
		echo "<h1>Two options here...</h1>";
		exit();
	}

	public function authenticate(){}

	public function blastcity()
	{
		$this->view->blast_cities	= City::find('all', array('conditions' => 'UPPER(name) != "CANADA" and UPPER(name) != "UNITED STATES" and blasting="ON"', 'order' => 'name'));
	}

	public function registercity()
	{
		#$_SESSION['unknown_city']	= 1;
		$this->view->city_error	= 0;
		if (isset($_SESSION['unknown_city']) && !empty($_SESSION['unknown_city']))
		{
			$this->view->city_error	= 1;
			unset($_SESSION['unknown_city']);
		}
		$this->view->blast_cities	= City::all();
	}

	public function twshare()
	{
		//STEP 1: Get the MEMBER...

		$member =	$_SESSION['member'];
		$paymentsList= Payment::all(array('conditions'=>array('member_id=?',$member->id)));
		$this->view->payments	=   $paymentsList;

	    //STEP 2: Get the LISTING...
		$request    =   $this->getRequest();
		$id  =  $this->view->listing_id = $request->getParam('id');

		if (isset($id) && !empty($id))
		{
			$listing = $this->view->listing = Listing::find_by_id($id);

			$_SESSION['TWITTER_REQUEST_TOKEN'] = $member->twitter_request_token;
			$_SESSION['TWITTER_ACCESS_TOKEN'] = $member->twitter_access_token;
			$my_twitter = new Blastit_Service_Twitter();

			if (	isset($member->twitter_request_token) && !empty($member->twitter_request_token) && isset($member->twitter_access_token) && !empty($member->twitter_access_token)	)
			{

				//echo "\t".date("Y-m-d H:i:s")."\t TWITTER Publishing Member: ".$member->first_name . " " . $member->last_name . "\n";

			   $response = $my_twitter->statusUpdate($listing->message);

				if ($response)
				{

		               if ( !empty($response->error) )
		               {
						//echo "No TWITTER response\n";
						$this->view->result = "FAIL";

						$member->twitter_fails++;

						if ($member->twitter_fails == 3)
						{
							$member->twitter_auto_disabled	=   1;
							$member->twitter_publish_flag	=   0;

							//Let the member know somethings gone wrong
						}

						$member->save();
					}
					else
					{
						//THIS IS A TWITTER POST!
						$post = new Post();

						$post->listing_id = $listing->id;
						$post->member_id = $member->id;
						$post->message = $listing->message;
						$post->twitter_post_id = $response->id_str;
						$post->result	  = "1";
						$post->save();

						//echo "Successful post to Twitter: " .$fb_result['id']."\n";

						$this->view->result = "SUCCESS";

						$member->twitter_fails = 0;
						$member->save();
					}
				}
				else
				{
					//echo "No TWITTER response\n";
					$this->view->result = "FAIL";

					$member->twitter_fails++;

					if ($member->twitter_fails == 3)
					{
						$member->twitter_auto_disabled =   1;
						$member->twitter_publish_flag	 =   0;
					}

					$member->save();

				}
			}
			else
			{
				//echo "Missing AUTH tokens!<BR>";
				//exit();
				$this->view->result = "FAIL";
			}
		}
		else
		{
			$this->view->result = "FAIL";
		}

	}

	public function fbshare()
	{
		//STEP 1: Get the MEMBER...
		$member =	$_SESSION['member'];

		//STEP 2: Get the LISTING...
		$request    =   $this->getRequest();
		$id   = $this->view->listing_id = $request->getParam('id');

		if (isset($id) && !empty($id))
		{
			$listing = $this->view->listing = Listing::find_by_id($id);

			//STEP 3: Push to FACEBOOK..	.
			$publish_array['access_token'] = $member->network_facebook_app->getFacebookAppAccessToken();
			$publish_array['message'] = $listing->message;

			//Set up the FB Interface Class
			$facebook = $member->network_facebook_app->getFacebookAPI();

			try {

				$fb_result = $facebook->api("/".$member->uid."/feed", "post", $publish_array);


				//THIS IS A FB POST!
				$post = new Post();

				$post->listing_id   = $listing->id;
				$post->member_id    = $member->id;
				$post->message      = $listing->message;
				$post->result	  = "1";
				$post->fb_post_id   = $fb_result['id'];
				$post->save();

				$this->view->result = "SUCCESS";

				$member->facebook_fails = 0;
				$member->save();

			} catch(Exception $e) {

				// Handling invalid token
				if ( strstr($e->getMessage(), "Error validating") )
				{
					$member->facebook_fails++;

					if ($member->facebook_fails == 3)
					{
						$member->facebook_auto_disabled	=   1;
						$member->facebook_publish_flag	=   0;
					}

					$member->save();
				}

				$this->view->result = "FAIL";
			}

		}
		else
		{
			$this->view->result = "FAIL";
		}

	}

	public function logout()
	{

		// log out of any attached SAML services
		
		if (($this->loggedInMember() instanceOf Member) && $this->loggedInMember()->saml_login) {
			$as = new SimpleSAML_Auth_Simple($this->loggedInMember()->saml_login->sourceId);
			if ($as->isAuthenticated()) {
				$as->logout($logoutUrl);
			}
		}
		
		if ($this->isLoggedIn()) {
			Zend_Session::destroy();
			$this->Redirect($this->facebook->getLogoutUrl());
		}
		
		$url = $this->getRequest()->getParam('redirect', '/');
		
		$this->Redirect($url);
		
	}

	public function fblogout()
	{
	    if (!$_SESSION) session_start();
	    session_destroy();
	    session_start();
	    $this->renderNothing();
	}

	public function reset()
	{
		if ($this->params['publish_type']) $_SESSION['RESET_TYPE'] = $this->params['publish_type'];

		if ($_SESSION['member'])
		{

			//Check that the HASH matches here...
			/**************************************
			if (incoming hash == expected hash)
			{

				if (strtoupper($this->params['publish_type']) == "FACEBOOK")
				{
					//$member->access_token 	=	null;
					//$member->save();

					//Redirect to a FB Login to get a new FB login Access Token (this has not been written yet)
					//$this->_redirect('/facebook');
				}
				if (strtoupper($this->params['publish_type']) == "TWITTER")
				{
					//$member->twitter_request_token 	=	null;
					//$member->twitter_access_token 	=	null;
					//$member->save();

					//$this->_redirect('/twitter');
				}
				if (strtoupper($this->params['publish_type']) == "LINKEDIN")
				{
					//$member->linkedin_request_token 	=	null;
					//$member->linkedin_access_token 	=	null;
					//$member->save();

					//$this->_redirect('/linkedin');
				}
			} **************************/
		}
		else
		{
			//Show the login page and redirect the user back to this page
			$this->_redirect("/member/authenticate");
		}
	}

	public function fblogin()
	{
		$network_app_facebook = Zend_Registry::get('networkAppFacebook');
		
		$this->renderNothing();

		$request      = $this->getRequest();
		$access_token = $request->getParam('access_token');

		$facebook = $network_app_facebook->getFacebookAPI();

		$member = null;

		try {
			$fb_user = $facebook->api("/me?access_token=" .  $access_token);
			$member = Member::find_by_uid($fb_user['id']);
		} catch(Exception $e) {
			echo 0;
			return;
		}

		if ($member instanceof Member) {
			if ($member->status == 'deleted')
			{
				$returnResult =	array('redirectBackTo'=> "/member/isdeleted/");
				echo json_encode($returnResult);
				return;
			}
		} else {
			$returnResult =	array('redirectBackTo'=> "/member/membernotfound/");
			echo json_encode($returnResult);
			return;
		}

		// If account was auto disabled, lets re-enable it with new access token
		if ( $member->facebook_auto_disabled ==  1 )
		{
			$member->facebook_fails		=   0;
			$member->facebook_publish_flag	=   1;
			$member->facebook_auto_disabled	=   0;
		}

		$member->application_id = $network_app_facebook->id; //SWITCH TO THE NEW FB APP!!!
		$member->pword_reminder = 0;
		$member->token_reminder = 0;

		$member->createExtendedAccessToken($access_token);
		$member->updateFacebookFriendCount();
		$member->updateFacebookAppVisibility();
		$member->save();
		
		$this->clearMessages();
		
		$this->setLoggedInMember($member);
		
		$this->checkAppPrivacy();

		$redirectTo	= $this->getLoginRedirectUrl();
	
		if (!empty($_SESSION['REDIRECT_AFTER_LOGIN']))
		{
			$redirectTo =	$_SESSION['REDIRECT_AFTER_LOGIN'];
			unset($_SESSION['REDIRECT_AFTER_LOGIN']);
			unset($_SESSION['NEEDS_LOGIN']);
		}
		
		$returnResult    =	array('redirectBackTo'=> $redirectTo);
		echo json_encode($returnResult);

	}
	
	public function login()
	{
		if (!$this->isLoggedIn())
		{

			$params['next'] 	 = '/member/login';
			$params['req_perms'] = FB_SCOPE;
	
			if ( $this->facebook->getUser() !=0 )
			{
				$member = Member::find_by_uid($this->facebook->getUser());
				if ( !($member instanceof  Member) ) {
					$this->render('membernotfound');
					return;
				}
				$this->setLoggedInMember($member);
			} else {
				$this->Redirect($this->facebook->getLoginUrl($params));
			}
		}
		
		$this->loginRedirect();
		
	}

	public function denied()
	{
		echo "<h1>Seems you've denied the application...that's a problem!</h1>";
		mail("alen@alpha-male.tv", "user denied app", "user denied app");
		exit();
	}

	public function clientfindsignupis()
	{		
		$this->renderNothing();
		
		$request 	  = $this->getRequest();
		$code 		  = $request->getParam('code');
		
		$this->checkUserDenied();
		
		try {
			$member = $this->general_signupis($code, "/member/clientfind-signupis");
		} catch (Exception $e) {
			$emailer = Zend_Registry::get('alertEmailer');
			$msg = get_class($e) . ': ' . $e->getMessage() . "\n\n" . $e->getTraceAsString();
			$emailer->send('Signup failure', $msg, 'signup-failure');
			
			$this->addMessage("We're sorry, it seems you took too long and this operation has timed out. Please try again!", 'colorbox');
			$this->_redirect('/member');
		}
		
		if (isset($_COOKIE['cb_cid'])) $member->creative_id = $_COOKIE['cb_cid'];
	
		$member->save();
		
		$redirect_url = '/member/info';
		
		if (!empty($_SESSION['REDIRECT_AFTER_LOGIN']))
		{
			$redirect_url = $_SESSION['REDIRECT_AFTER_LOGIN'];
	
			unset($_SESSION['REDIRECT_AFTER_LOGIN']);
			unset($_SESSION['NEEDS_LOGIN']);
		}
		elseif (!empty($_SESSION['next']))
		{
			$redirect_url = $_SESSION['next'];
			unset($_SESSION['next']);
		}
		// If the member registered via landing page then we need to use that landing pages member info URL
		// this is to ensure proper tracking by google analytics
		elseif (isset($_COOKIE['landing_page_group_name']) && isset($_COOKIE['landing_page_name'])) 
		{					
			$redirect_url = $this->view->url(array(
					'group_name' => $_COOKIE['landing_page_group_name'],
					'page_name' => $_COOKIE['landing_page_name']
				), 
				'landing_page_group_member_info'
			);
			
			
			setcookie("landing_page_group_name", "", time() - 3600);
			setcookie("landing_page_name", "", time() - 3600);
		}


		$this->_redirect($redirect_url);
	}

	private function processReferral($member) 
	{
		if ($affiliate_id	= $this->getCurrAffiliateId()) 
		{
			$affiliate = Affiliate::find_by_id($affiliate_id);
			if ($affiliate instanceOf Affiliate) 
			{
				$member->referring_affiliate_id = $affiliate->id;
			} 
			elseif ($referring_member = Member::find_by_id($affiliate_id)) 
			{
				$member->referring_member_id = $referring_member->id;
			}
			$this->clearCurrAffiliateId();
		}
	}

	//This is a "NON-PUBLISHING" signup (people who only blast)
     public function signupis()
     {
		$this->renderNothing();
		$request    =   $this->getRequest();
		$code   =   $request->getParam('code');

		$this->checkUserDenied();

		$member = $this->general_signupis($access_token, $code, "/member/signupis");
		$this->_redirect('index');

	}
	
	public function checkUserDenied() {
		if (isset($_REQUEST['error_reason']) && $_REQUEST['error_reason'] == "user_denied")
		{
			//Ooops, you've denied the Application!
			$this->_redirect("/user/denied");
		}
	}

	//This s a "PUBLISHING" signup (people who get their walls written on)
	public function general_signupis($code, $redirect_url)
	{

		$signupSession = new Zend_Session_Namespace('signup');
		$tmp_member_settings = new Zend_Session_Namespace('tmp_member_settings');

		$city = null;

		$sso  = new Zend_Session_Namespace('sso');
		
		// Pick up incomplete Member provisioned by SOAP if present
		
		if ($sso->loginid && ($saml = SAMLLogin::find_by_username($sso->loginid)) ) {
			$member = $saml->member;
		} else {
			$member = new Member();
		}
		
		$network_app_facebook = Zend_Registry::get('networkAppFacebook');
		$member->application_id = $network_app_facebook->id;
		
		$facebook = $network_app_facebook->getFacebookAPI();
		
		try {
			$member->createAccessToken($code, $redirect_url);
		} catch (FacebookApiException $e) {
			if ($e->getCode() == 100) {
				// Auth code has been used or has expired
				$this->RedirectUrl('/member');
			} else {
				throw $e;
			}
		}
		
		$fb_user = $facebook->api("/me?access_token=" .  $member->access_token);

		if (!$member->isPublishPermissionOn())
		{		
			// Temp disble redirecting back to signup page and nothing else happens (kfoster 2014-04-28)
			//$this->_redirect("/member/index/?failed_permission=1");
			//exit;
		}

		$new_member = true;
		if ($m = Member::find_by_uid($fb_user['id'])) {
			$member = $m;
			$_SESSION['new_registry'] = false;
			$new_member = false;
		} else {
			// FIXME: check this against old code						
			$member->account_type = "paid";
			$_SESSION['new_registry'] = true;
			$this->processReferral($member);
		}
		
		if ($new_member) {
			$member_type = ($signupSession->member_type) 
							? MemberType::find_by_code(strtoupper($signupSession->member_type))
							: null;
			$member_type = empty($member_type) ? MemberType::find_by_is_default(1) : $member_type;
			$member->type_id = $member_type->id;
			
			$billing_cycle = ($signupSession->billing_cycle) ? $signupSession->billing_cycle : 'month';
			$member->billing_cycle = $billing_cycle;
		}
		
		//Check to see if we have passthru settings from the UPFRONT LANDERS
		if (isset($tmp_member_settings->frequency_id))
		{
			$member->frequency_id = $tmp_member_settings->frequency_id;
		} elseif ($member->type->is_customer) {
			$member->frequency_id = 4;	//Default Frequency (4 times a week)
		} else {
			$member->frequency_id = 0;
		}
		
		if ($tmp_member_settings->city) {
			
			// if this is a geolocated city, find its record or create a new one
			
			$cities = City::all(
				array(
					'conditions' => array(
						'name = ? AND state = ? AND country = ?', 
						$tmp_member_settings->city, 
						$tmp_member_settings->state,
						$tmp_member_settings->country
					),
					'limit' => 1,
					'order' => 'id ASC'
				)
			);
			$city = !empty($cities[0]) ? $cities[0] : null;
			if(!$city)
			{
				$city = new City();
				$city->name = $tmp_member_settings->city;
				$city->state = $tmp_member_settings->state;
				$city->country = $tmp_member_settings->country;
			
				$city->save();
				
				$emailer = Zend_Registry::get('alertEmailer');
				$msg = 'A new signup has been received for member ' . $member->id . '. ' . 
						'The member entered "' . $tmp_member_settings->location_input . '" ' . 
						'in the signup form which resulted in the creation of a new city record.' . "\n\n" .
						'New City: '. $city->id . ', ' . $city->name . ', ' .$city->state . ', ' . $city->country;
				$emailer->send('Member signup created new city', $msg, 'member-new-city');
			}
		} else {
			// if there's only a raw input string, try to find a corresponding record
			$matcher = new Blastit_CityMatcher;
			if ( 
				false == ($city = $matcher->fuzzyMatchLocation($tmp_member_settings->location_input)) &&
				false == $member->city_id // Only default the city to 'unkown' if its not already set
			) {
				$city = City::find(0);
			}
		}
		//Check to see if we have passthru settings from the UPFRONT LANDERS
		if ($city instanceOf City)
		{
			$member->publish_city_id = $city->id;
			$member->city_id = $city->id;
			
			$this->setSelectedCity($city->id, $city->name, $city->country);
			
			if ($city->id == 0) {
				$emailer = Zend_Registry::get('alertEmailer');
				$emailer->send('Member signed up with unknown city', "A new signup has been received for member {$member->id}, but no matching city was found. The member entered \"{$tmp_member_settings->location_input}\" in the signup form.", 'member-unknown-city');
			}
		}

		$member->facebook_publish_flag = 1;

		$member->save_user($fb_user);
		
		if ($_SESSION['new_registry'] || (empty($member->inventory_id) || empty($member->current_hoper))) 
		{
			$member->assign_inventory_hopper();
		} 
		
		$member->save();

		if ($new_member) {
			$emailer = Zend_Registry::get('alertEmailer');
			$emailer->send('New Facebook member', "Member's Facebook profile: http://www.facebook.com/" . $member->uid, 'new-member-facebook');
		}

		//Check to see if we have passthru settings from the UPFRONT LANDERS
		if (isset($tmp_member_settings->tag_ids))
		{
			$member->update_content_prefs($tmp_member_settings->tag_ids);				
		}
	
		if ($_SESSION['new_registry'] && isset($_COOKIE['landing_page_group_name']) && isset($_COOKIE['landing_page_name'])) 
		{
			$landing_page_url = $this->view->url(array(
									'group_name' => $_COOKIE['landing_page_group_name'],
									'page_name' => $_COOKIE['landing_page_name']
								), 
								'landing_page_group'
							);
		
			$member_landing_page 			= new MemberLandingPage();
			$member_landing_page->member_id 	= $member->id;
			$member_landing_page->group_name 	= $_COOKIE['landing_page_group_name'];
			$member_landing_page->page_name 	= $_COOKIE['landing_page_name'];
			$member_landing_page->url 		= $landing_page_url;
			$member_landing_page->save();
		}

		$_SESSION['member'] 	= $member;


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');


			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('FB Connect Success');
		}	

		return($member);
     }

	public function clientfindsignup()
	{
		$network_app_facebook = Zend_Registry::get('networkAppFacebook');
		
		$code = $this->_request->getParam('code');

		$this->checkUserDenied();

		if (empty($code))
		{
			$signup_params = array(
				'client_id' => $network_app_facebook->consumer_id,
				'scope' => FB_SCOPE,
				'redirect_uri' => APP_URL . '/member/clientfind-signupis'
			);
			$signup_url = 'http://www.facebook.com/dialog/oauth?' . http_build_query($signup_params);
			$this->_redirect($signup_url);
		}
	
	}
	
	public function signup()
	{

		$signupSession = new Zend_Session_Namespace('signup');
		//Ok, we know what the original city the person is from, does this city support publishing?
		$default_city	= $this->_request->getParam('publish_city');

		if (!empty($default_city))
		{

			$city = City::find($default_city);
			if ($city->blasting != "ON")
			{
				//This is a city that doesn't support BLASTING, pick Canada or the US
				$city = City::find_by_name($city->country);
				$signupSession->publish_city = $city->id;
				$signupSession->default_city = $default_city;
			}
			else
			{
				//All good, this city supports BLASTING
				$signupSession->publish_city    =	$default_city;
				$signupSession->default_city    =	$default_city;
			}

			$this->setSelectedCity($city->id, $city->name, $city->country);
		}

		//http://developers.facebook.com/docs/authentication/
		//http://YOUR_URL?error_reason=user_denied&error=access_denied&error_description=The+user+denied+your+request.
		$code = $_REQUEST["code"];

		$network_app_facebook = Zend_Registry::get('networkAppFacebook');

		if (empty($code))
		{
			$signup_url = "http://www.facebook.com/dialog/oauth?client_id=" . $network_app_facebook->consumer_id . "&scope=".FB_SCOPE."&redirect_uri=" . urlencode(APP_URL . "/member/signupis");
			$this->_redirect($signup_url);
		}
	}

	public function thankyou()
	{
		$this->_helper->layout->setLayout('wizard');
				
		/******************************************
		$today = getdate();


		if ($today['hours'] >= 18)
		{
			$this->view->next_listing = "Tomorrow Morning at 9:00am";
		}
		elseif ($today['hours'] >= 15 && $today['hours'] < 18)
		{
			$this->view->next_listing = "Today at 6:00pm";
		}
		elseif ($today['hours'] >= 12 && $today['hours'] < 15)
		{
			$this->view->next_listing = "Today at 3:00pm";
		}
		elseif ($today['hours'] >= 9 && $today['hours'] < 12)
		{
			$this->view->next_listing = "Today at noon";
		}
		else
		{
			$this->view->next_listing = "Today at 9:00am";
		} **************************************/


		
		
		//Should we BLAST to their wall right away?
	}

	public function turn_on()
	{
		$this->redirectIfNotLoggedIn();

		$member = $_SESSION['member'];

		$publish_setings = new PublishSettings();
		$publish_setings->member_id    = $member->id;
		$publish_setings->changed_to   ='ON';
		$publish_setings->change_date  = date('Y-m-d');

		if (strtolower($this->params['publish_type']) == "facebook")
		{
			$member->facebook_publish_flag = 1;

			$publish_setings->publish_type = 'FACEBOOK';
			$publish_setings->save();

			//If account was auto disabled, lets reset counters
			if ( $member->facebook_auto_disabled	==   1 )
			{
			    $member->facebook_fails		=   0;
			    $member->facebook_auto_disabled	=   0;
			}
			
			$success_message = $this->getFlashSuccessHtml(array('Facebook publishing was turned on.'));
			$this->addMessage($success_message, 'colorbox');
		}
		elseif (strtolower($this->params['publish_type']) == "twitter")
		{
			$member->twitter_publish_flag = 1;

			$publish_setings->publish_type = 'TWITTER';
			$publish_setings->save();

			//If account was auto disabled, lets re-enable it with new access token
			if ( $member->twitter_auto_disabled	==   1 )
			{
			    $member->twitter_fails		=   0;
			    $member->twitter_auto_disabled	=   0;
			}
			
			$success_message = $this->getFlashSuccessHtml(array('Twitter publishing was turned on.'));
			$this->addMessage($success_message, 'colorbox');
		}
		elseif (strtolower($this->params['publish_type']) == "linkedin")
		{

			//echo "PUB TYPE: linkedin<BR>";
			
			$member->linkedin_publish_flag = 1;

			$publish_setings->publish_type = 'LINKEDIN';
			$publish_setings->save();

			//If account was auto disabled, lets re-enable it with new access token
			if ( $member->linkedin_auto_disabled	==   1 )
			{
			    $member->linkedin_fails			=   0;
			    $member->linkedin_auto_disabled	=   0;
			}

			$success_message = $this->getFlashSuccessHtml(array('LinkedIn publishing was turned on.'));
			$this->addMessage($success_message, 'colorbox');
		}



		$member->save();

		if (strtolower($this->params['publish_type']) == "twitter")
		{
			//If either token is empty... reauthorize
			if (empty($member->twitter_request_token) || empty($member->twitter_access_token))
			{
				$this->_redirect("/twitter/index");
			}
		}
		elseif (strtolower($this->params['publish_type']) == "linkedin")
		{
			//If either token is empty... reauthorize
			if (empty($member->linkedin_request_token) || empty($member->linkedin_access_token))
			{
				$this->_redirect("/linkedin");
			}
		}


		$_SESSION['member'] = $member;
		
		/*********
		if (isset($this->params['publish_type']) && $this->params['publish_type'])
		{ 
			echo "1";
			exit();
		}
		else
		{ 
			$this->_redirect("/member/settings/#settingtab");
		}*********/
		
		
		$this->_redirect('/member/settings#settingtab');
	}

	public function turn_off()
	{
		
		$member = $_SESSION['member'];

		$publish_setings = new PublishSettings();
		$publish_setings->member_id    = $member->id;
		$publish_setings->changed_to   ='OFF';
		$publish_setings->change_date  = date('Y-m-d');

		if (strtolower($this->params['publish_type']) == "facebook")
		{
			$member->facebook_publish_flag = 0;
			$publish_setings->publish_type = 'FACEBOOK';
			$publish_setings->save();
			
			$success_message = $this->getFlashSuccessHtml(array('Facebook publishing was turned off.'));
			$this->addMessage($success_message, 'colorbox');
		}
		elseif (strtolower($this->params['publish_type']) == "twitter")
		{
			$member->twitter_publish_flag = 0;
			$publish_setings->publish_type = 'TWITTER';
			$publish_setings->save();
			
			$success_message = $this->getFlashSuccessHtml(array('Twitter publishing was turned off.'));
			$this->addMessage($success_message, 'colorbox');
		}

		elseif (strtolower($this->params['publish_type']) == "linkedin")
		{
			$member->linkedin_publish_flag = 0;
			$publish_setings->publish_type = 'LINKEDIN';
			$publish_setings->save();
			
			$success_message = $this->getFlashSuccessHtml(array('LinkedIn publishing was turned off.'));
			$this->addMessage($success_message, 'colorbox');
		}

		//If account was auto disabled, lets reset counters
		if ( $member->facebook_auto_disabled	==   1 )
		{
		    $member->facebook_fails		=   0;
		    $member->facebook_auto_disabled	=   0;
		}
		//If account was auto disabled, lets reset counters
		if ( $member->twitter_auto_disabled	==   1 )
		{
		    $member->twitter_fails		=   0;
		    $member->twitter_auto_disabled	=   0;
		}

		//If account was auto disabled, lets reset counters
		if ( $member->linkedin_auto_disabled	==   1 )
		{
		    $member->linkedin_fails		=   0;
		    $member->linkedin_auto_disabled	=   0;
		}

		$member->save();

		$_SESSION['member'] = $member;
		$this->_redirect('/member/settings#settingtab');
	}


	//Action for twitter
	public function twitter()
	{
		if ($_COOKIE['cityblast_aff_id'] == 1183) mail("alen@alpha-male.tv","FB Click - Twitter - " . COMPANY_NAME. " ","");
		if ($_COOKIE['cityblast_aff_id'] == 1184) mail("alen@alpha-male.tv","Google Click - Twitter - " . COMPANY_NAME. " ","");	
	}


	//Action for twitter
	public function linkedin()
	{
		if ($_COOKIE['cityblast_aff_id'] == 1183) mail("alen@alpha-male.tv","FB Click - LinkedIn - " . COMPANY_NAME. " ","");
		if ($_COOKIE['cityblast_aff_id'] == 1184) mail("alen@alpha-male.tv","Google Click - LinkedIn - " . COMPANY_NAME. " ","");
	}

	public function whitelist(){}


	public function invoices()
	{
		$member =	$_SESSION['member'];

		$conditions = array('conditions' => ' member_id = "'.$member->id.'" ', 'order' => 'id DESC');

		$paginator = new Zend_Paginator(new ARPaginator('Payment',$conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		/*$paymentsList= Payment::all(array('conditions'=>array('member_id=?',$member->id)));
		$this->view->paymentsList	=   $paymentsList;*/
	}

	public function printinvoice()
	{
		$id = (int)$this->_getParam('id');
		if (empty($id)) {
			$this->redirect('/member/settings#invoices');
		}
		
		if ($this->memberIs('superadmin')) {
			$invoice = Payment::find_by_id($id);
			$member = $invoice->member;
		} else {
			$member = $_SESSION['member'];
			$invoice = Payment::find_by_member_id_and_id($member->id, $id);
		}

		if ( is_null($invoice) )
			$this->redirect('/member/settings#invoices');

		if ( empty($invoice->address1) ) {
			// Grab the last payment with up to dated data
			$sql = 'select * from payment where member_id = '.intval($member->id).' and (address1 IS NOT NULL AND address1 != "") order by created_at DESC limit 1';
			$invoiceData = Payment::find_by_sql($sql);

			if ( count($invoiceData)>0 ) {
				$invoiceData =  array_shift($invoiceData);
				$invoice->address1 = $invoiceData->address1;
				$invoice->address2 = $invoiceData->address2;
				$invoice->city = $invoiceData->city;
				$invoice->state = $invoiceData->state;
				$invoice->country = $invoiceData->country;
				$invoice->zip = $invoiceData->zip;
			}

		}

		$this->view->member = $member;
		$this->view->invoice = $invoice;
		$this->renderScript('member/printinvoice.html.php');
		$this->_helper->layout()->disableLayout ();
	}


	public function updatesettings()
	{
		$this->view->updated = false;
		
		$member = Member::find($_SESSION['member']->id);
		$this->view->member = $member;

		if ($this->_request->isPost())
		{
			$error_array = array();

			$member->email_override = $this->_request->getParam("email_override");


			$first_name = $this->_request->getParam("first_name");
			$last_name = $this->_request->getParam("last_name");
			
			$brokerage = $this->_request->getParam("brokerage");
			$broker_address = $this->_request->getParam("broker_address");
			
			if (empty($first_name)) $error_array[] = "first_name";
			if (empty($last_name)) $error_array[] = "last_name";
			if (empty($brokerage)) $error_array[] = "brokerage";
			if (empty($broker_address)) $error_array[] = "broker_address";
			$this->view->fields = $error_array;


			$member->phone = $this->_request->getParam("phone");
			$member->first_name = $this->_request->getParam("first_name");
			$member->last_name = $this->_request->getParam("last_name");
			
			if (in_array($member->type->code, array('AGENT', 'BROKER'))) {
				$member->brokerage = $this->_request->getParam("brokerage");
				$member->broker_address = $this->_request->getParam("broker_address");
			}
			
			$member->save();
			
			//if (!empty( $_SESSION['RETURN_TO'])) $this->redirect( $_SESSION['RETURN_TO']);

			$success_message = $this->getFlashSuccessHtml(array('Your personal settings have been updated.'));
			$this->addMessage($success_message, 'colorbox');
			
			$this->_redirect("/member/settings#myaccounttab");
		}
	}

	/**
	 * Method to handle franchise field edit and view
	 */
	public function emailoverride()
	{
		$this->view->updated = false;
		
		$member = Member::find($_SESSION['member']->id);
		$this->view->member = $member;

		if ($this->_request->isPost())
		{
			$this->view->fields = $error_array;

			$member->email_override = $this->_request->getParam("email_override");
			$member->save();
			
			if (!empty( $_SESSION['RETURN_TO'])) $this->redirect( $_SESSION['RETURN_TO']);

			
			$this->_redirect("/member/settings");
		}
	}

	/**
	 * Method to handle franchise field edit and view
	 */
	public function brokerage()
	{
		$request = $this->getRequest();
		$is_ajax = $request->isXmlHttpRequest();
		
		if ($is_ajax) {
			$this->_helper->viewRenderer->setNoRender(true);
			$this->_helper->layout->disableLayout();
		}
		
		$member = Member::find_by_id($_SESSION['member']->id);
		$this->view->member = $member;

		if ($this->_request->isPost())
		{
			$error_array = array();
			$brokerage = $this->_request->getParam("brokerage");
			$broker_address = $this->_request->getParam("broker_address");
			if (empty($brokerage)) $error_array[] = "brokerage";
			if (empty($broker_address)) $error_array[] = "broker_address";
			$this->view->fields = $error_array;

			$member->brokerage = $this->_request->getParam("brokerage");
			$member->broker_address = $this->_request->getParam("broker_address");
			$member->save();
			
			$member->mapFranchise();
			
			$this->view->updated = false;
			
			if ($is_ajax) {
				$this->_helper->json(array(
					'success' => empty($error_array), 
					'errors' => $error_array
				));
			}
			
			if (!$is_ajax && count($error_array) == 0)
			{ 
				if (!empty( $_SESSION['RETURN_TO'])) $this->redirect( $_SESSION['RETURN_TO']);
				$this->_redirect("/member/settings");
			}
		}
	}

	public function contact()
	{
		$member = Member::find($_SESSION['member']->id);
		$this->view->member = $member;

		if ($this->_request->isPost())
		{

			$member->phone = $this->_request->getParam("phone");
			$member->save();

			if (!empty( $_SESSION['RETURN_TO'])) $this->redirect( $_SESSION['RETURN_TO']);

			$this->_redirect("/member/settings");
			
		}
	}
	
	public function updatetestimonial()
	{
		$testimonial = Testimonial::find_by_member_id($_SESSION['member']->id);
		if (!$testimonial) {
			$testimonial = new Testimonial(array(
				'member_id' => $_SESSION['member']->id,
				'link' => 'https://www.facebook.com/profile.php?id=' . $_SESSION['member']->uid
			));
		}
		$testimonial->testimonial = strip_tags( $this->getRequest()->getParam('testimonial') , '<b><i><strong><em><br>');
		$testimonial->save();

		// Send email to notify a testimonial has been updated
		/**
		 * @var Blastit_AlertEmailer $emailer
		 */
		$emailer = Zend_Registry::get('alertEmailer');
		$emailer->send('Testimonial updated', $testimonial->testimonial);

		$this->_redirect("/member/settings#testimonialtab");
	}

	public function update_name()
	{
		$member = Member::find($_SESSION['member']->id);
		$this->view->member = $member;

		if ($this->_request->isPost())
		{
			$member->first_name = $this->_request->getParam("first_name");
			$member->last_name = $this->_request->getParam("last_name");
			$member->save();

			if (!empty( $_SESSION['RETURN_TO'])) $this->redirect($_SESSION['RETURN_TO']);

			$this->_redirect("/member/settings");
			
		}
	}
	
	// Update access token
	public function update_access_token()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
	
		if ($this->_request->isPost())
		{
			$member = Member::find_by_id($_SESSION['member']->id);
			$token = $this->_request->getParam('access_token');
	
			if (!empty($token)) {
				$member->access_token = $token;
				$member->save();
				echo json_encode( array("error"=>false) );
				return;
			}
		}
	
		echo json_encode( array("error"=>true) );
	}

	/**
	 * Update the next payment date ...
	 */
	public function update_next_payment()
	{
		$this->_helper->layout()->disableLayout();
		$member = Member::find($this->_request->getParam('id'));
		$nextPayDate = new DateTime($this->_request->getParam('next_payment'));
		
		if (
			$nextPayDate > $member->next_payment_date ||
			$nextPayDate < $member->next_payment_date
		) 
		{
			$oldValue = $member->next_payment_date->format('Y-m-d');
			$newValue = $nextPayDate->format('Y-m-d');
			
			$member->next_payment_date = $newValue;
			$member->save();
			
			EventLog::create(
				EventLog::ENTITY_TYPE_MEMBER,
				'next_payment_date_update',
				'',
				$member->id,
				$member->id,
				$newValue,
				$oldValue
			);
		}

		exit;
	}
	
	/**
	 * Update the next payment date ...
	 */
	public function update_trail_period()
	{
		$this->_helper->layout()->disableLayout();
		$member = Member::find($this->_request->getParam('id'));
		$trial_extended = $this->_request->getParam('trail_period');
		$member->trial_extended = $trial_extended;
		$member->payment_status = 'extended_trial';
		$member->save();
		exit;
	}
	
	public function fanpages()
	{
		$member = Member::find($this->_request->getParam('id'));
		
		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}
		
		$this->view->member   =	$member;
	}


	public function update()
	{
		$member = Member::find($this->_request->getParam('id'));
		if (!$member) {
			throw new Exception("member not found");
		}
		
		
		//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');
	
		$this->update_common();
		
		$this->_redirect("/admin/member/".$this->_request->getParam('id').'#update');
	}


// old code? also present in AdminController.
	
	public function update_common()
	{
		$params = $this->_request->getParams();
		$member = Member::find($this->_request->getParam('id'));
		$memberDataInitial = $member->attributes();

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$member->credits				=	$request->getParam("credits");
			$member->facebook_publish_flag	=	$request->getParam("facebookpublish");
			$member->twitter_publish_flag	=	$request->getParam("twitterpublish");
			$member->linkedin_publish_flag	=	$request->getParam("linkedinpublish");
			
			$type_id = $request->getParam("type_id");
			if (isset($type_id)) $member->type_id	=	$type_id;
			
			$member->referring_affiliate_id =   $request->getParam("affiliate_id");
			if (!$member->referring_affiliate_id) $member->referring_affiliate_id = null;
			
			$first_name = $request->getParam('first_name');
			$first_name = trim($first_name);
			if (!empty($first_name)) {
				$member->first_name	= $first_name;
			}
			$last_name = $request->getParam('last_name');
			$last_name = trim($last_name);
			if (!empty($last_name)) {
				$member->last_name = $last_name;
			}
			
			$member->email_override			=	$request->getParam("email_override");
			$member->inventory_id  			=	$request->getParam("inventory_id");
			$current_hopper					=   $request->getParam("current_hoper");
			$member->current_hoper 			=   empty($current_hopper) ?  1 : $current_hopper;
			$member->frequency_id 			=	$request->getParam("frequency_id");
			$member->auto_like 				=	$request->getParam("auto-like", $member->auto_like);
			$member->publish_city_id  		=	$request->getParam("publish_city_id");
			$member->city_id  				=	$request->getParam("city_id");
			
			$member->brokerage 				= $this->_request->getParam("brokerage");
			$member->broker_address 		= $this->_request->getParam("broker_address");
			
			$member->phone = $request->getParam("phone");
			
			//Only update the email if it's been updated
			$email = $request->getParam("email");
			if (isset($email) && !empty($email)) $member->email = $request->getParam("email");

			
			$franchise_id = $request->getParam('franchise_id');
			if ($franchise_id) {
				$member->franchise_id = $franchise_id;
			}
			
			if ($member->billing_cycle != $request->getParam("billing_cycle")) {
				$member->promo_id = null;
			}
			
			$member->billing_cycle = $request->getParam("billing_cycle");
			
			
			$price_new = $request->getParam('price');
			if ($member->type->is_customer) {
				/**
				 * Ensure price is not greater than related price in member_type for the members selected billing cycle.
				 * If the previous value was low enough, leave it the same.
				 * Else set as the default price for the member type and billing cycle selected
				 */
				$price_default = $member->type->getPrice($member->billing_cycle);
				if ($price_new <= $price_default) {
					$member->price = $price_new;
				} else {
					if ($price_default && $member->price > $price_default) {
						$this->addMessage('Price too high, set to billing cycle default.', 'colorbox');
						$member->price = $price_default;
					} elseif ($price_new > $price_default) {
						$this->addMessage('Price too high. Price change not saved.', 'colorbox');
					}
				}
			} else {
				$member->price = $price_new;
			}
			

			$member->account_type = $request->getParam("account_type");
			
			if ($member->account_type == 'free')
			{ 
				$member->payment_status = 'free';
				$member->clientfinder_switch = 1;
			}
			
			$member->save();
			
			if (!$member->referring_affiliate_id) $member->referring_affiliate_id = null;

			if ($member->affiliate_account instanceOf Affiliate && $request->getParam("is_affiliate_member") == 0) {
				if (!$member->affiliate_account->hasReferredMembers()) {
					$member->affiliate_account->delete();
				}
			} elseif (!($member->affiliate_account instanceOf Affiliate) && $request->getParam("is_affiliate_member") == 1) {
				$member->create_affiliate_account();
			}
			
			$member->reload();
			
			if ($member->affiliate_account instanceOf Affiliate) {
				$member->affiliate_account->first_payout_percent 		= $request->getParam("affiliate_first_payout_percent");
				$member->affiliate_account->recurring_payout_percent 	= $request->getParam("affiliate_recurring_payout_percent");

				if ($override_payout_percent = $request->getParam("affiliate_override_payout_percent")) {
					$member->affiliate_account->override_payout_percent = $override_payout_percent;
				} else {
					$member->affiliate_account->override_payout_percent = null;
				}
				
				if ($override_affiliate_id = $request->getParam("affiliate_override_affiliate_id")) {
					$member->affiliate_account->override_affiliate_id = $override_affiliate_id;
				} else {
					$member->affiliate_account->override_affiliate_id = null;
				}
				
				$member->affiliate_account->slug = $request->getParam("affiliate_slug");
		
				if ($logo_uploaded = $request->getParam("logo_uploaded")) {
					$tmpfile = strstr($logo_uploaded, TMP_ASSET_URL) ?
								str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $logo_uploaded) : 
								TMP_ASSET_STORAGE . $logo_uploaded;	
					if (file_exists($tmpfile)) {
						$ext = pathinfo($tmpfile, PATHINFO_EXTENSION);		
						$final_logo = '/images/affiliates/' . $member->affiliate_account->id . '.' . $ext;
						copy($tmpfile, CDN_ORIGIN_STORAGE . $final_logo);
						$member->affiliate_account->logo = $final_logo;
						@unlink($tmpfile);
					}
				}

				$member->affiliate_account->save();
				$member->affiliate_account->reload();
				$cache = Zend_Registry::get('cache');
				$cache->remove('affiliate_slugs');
			}

			if ($member->isOAuthClient() && $request->getParam("is_oauth_client") == 0) {
				if (!$member->oauth_client->hasTokens()) $member->oauth_client->delete();
			} elseif (!$member->isOAuthClient() && $request->getParam("is_oauth_client") == 1) {
				$member->create_oauth_client();
			}
			
			if ($member->isOAuthClient()) {
				$member->oauth_client->client_name   = $request->getParam("oauth_client_name");
				$member->oauth_client->client_site   = $request->getParam("oauth_client_site");
				$member->oauth_client->redirect_uri  = $request->getParam("redirect_uri");
				if ($request->getParam("tmp_api_key")) $member->oauth_client->client_id     = $request->getParam("tmp_api_key");
				if ($request->getParam("tmp_api_secret")) $member->oauth_client->client_secret = $request->getParam("tmp_api_secret");
			}
			
			$preferences = $request->getParam("preferences");
			if (is_array($preferences)) {
				//member model :: update_content_prefs currently sets preference if tag id
				//is set as key in array passed to update_content_prefs;
				$tags = array();
				foreach ($preferences as $i => $preference) {
					if ($preference === "1") {
						$tags[$i] = $preference;
					}
				}
				if (count($tags) < 5) {
					$this->addMessage('Member must have at least 5 content setting', 'colorbox');
				} else {
					$member->update_content_prefs($tags);
				}
			}
			
			if ($franchise_id && $member->franchise) {
				$franchiseUnknown = FranchiseUnknownMember::find_by_member_id($member->id);
				if ($franchiseUnknown) {
					$franchiseUnknown->delete();
				}
			}
			
			$this->view->message    =	"The member has been successfully updated! <a href='/admin/members/'>Return to members listing</a>";
						
			EventLog::create(
				EventLog::ENTITY_TYPE_MEMBER,
				'update',
				'',
				$member->id,
				$member->id,
				$member,
				$memberDataInitial
			);
			
			if (isset($memberDataInitial['account_type']) && $memberDataInitial['account_type'] != $member->account_type) {
				$note = new Note();
				$note->note = 'Account type changed from "' . $memberDataInitial['account_type'] . '" to "' . $member->account_type . '"';
				$note->member_id = $member->id;
				$note->admin_id = (isset($_SESSION['member'])) ? $_SESSION['member']->id : 0;
				$note->save();
			}
		}

		$this->view->member   =	$member;
		
		if (!$member->isOAuthClient()) {
			$keys = OAuthClient::generateCredentials();
			$this->view->tmp_api_key    = $keys['client_id'];
			$this->view->tmp_api_secret = $keys['client_secret'];
		}
		
	    $query  = "select * from (select city.id, city.name, city.blasting, count(member.id) as total_members from city left outer join member on city.id=member.city_id group by city.id) tmptbl";
		$cities = City::find_by_sql($query);

		foreach($cities as $city)
		{
			if ($city->total_members > 0 or $city->blasting == "ON")
			{
				$final_cities[] = $city;
			}
		}

		/* Below disabled by Shane Dec 13 2013 -- the output of these does not seem to be used anywhere! */
		/* If it does need to be used somewhere, this probably is not the place to do it. */
		
		/* 

		$this->view->publish_cities = $final_cities;
		//$this->view->selectedAdminCityObject = City::find_by_id(intval($this->getSelectedAdminCity()));


		$this->view->affiliate_owners = Member::find('all', array( 'joins' => 'JOIN affiliate ON affiliate.member_id = member.id', 'order' => 'first_name, last_name'));

		$query  = "select click_stat.*, sum(click_stat.count) as total_clicks, member.id, member.first_name, member.last_name, member.uid, member.email, ";
		$query .= "member.facebook_friend_count, member.twitter_followers, member.linkedin_friends_count, ";
		$query .= "member.facebook_publish_flag, member.twitter_publish_flag, member.linkedin_publish_flag, member.frequency_id, member.created_at ";
		$query .= " from click_stat left outer join member on click_stat.member_id=member.id ";
		$query .= "where member.id=".$member->id . " ";
		//$query .= "order by total_clicks DESC";
		$this->view->clicks = Click::find_by_sql($query);

		$query  = "select DATEDIFF(NOW(), created_at) AS number_of_days from member ";
		$query .= "where id=".$member->id;
		$this->view->days_on_site = Member::find_by_sql($query);

		$query  = "select sum(amount) as total_amount from payment ";
		$query .= "where member_id=".$member->id . " ";
		$this->view->total_payments = Payment::find_by_sql($query);	*/
		
	}		


	public function update_details()
	{
		//Disable the layout
    	$this->_helper->layout()->disableLayout();
		$this->update_common();
		
	}	


	public function blast()
	{
	    $params =	$this->getRequest()->getParams();
	    $member =	$_SESSION['member'];

	    $id	= (int)$params['id'];
	    $this->view->listing = $listing = Listing::find_by_id($id);

		/*******
	    if ($member->id != $listing->member_id)
	    {
		$this->renderPartial('error/404.html.php');
		return;
	    }
		*******/

	    $paginator = new Zend_Paginator(new ARPaginator('BlastReach',' listing_id='.$id));
	    $paginator->setCurrentPageNumber($this->_getParam('page', 1));
	    $paginator->setItemCountPerPage(100);

	    $this->view->listCount  =	$paginator->getTotalItemCount();;
	    $this->view->paginator = $paginator;
	}

	public function delete()
	{
	    $this->renderNothing();
	    $params =	$this->getRequest()->getParams();
	    $members	    =	new Member();
	    $member	    =   $members->find($params['id']);
	    $member->status =   'deleted';
	    $member->save();

	    if (count($member->listings)>0) {
			foreach($member->listings as $listing)
			{
				if ($listing->published==0) {
				$listing->status	=   'deleted';
				$listing->save();
				}
			}
	    }

	    $message = new Zend_Session_Namespace('messages');
	    if (!isset($message->data)) $message->data = new stdClass;
	    $message->data->message =	"Member sucessfuly deleted.";

	    $this->_redirect('/admin/allmembers/');

	}

	public function setcity()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout->disableLayout();

	    if ($this->_request->isPost())
	    {
			$city = City::find_by_id($this->_request->getParam('city_id'));
			$this->setSelectedCity($city->id, $city->name, $city->country);

			$_SESSION['updatecity']	=   true;

			// Save member selected city
			$member =	$_SESSION['member'];
			if (!is_null($member) && $this->_request->getParam('city_id') > 0)
			{
				$member->city_id	=   $this->_request->getParam('city_id');
				$member->save();

			}
			echo json_encode( array("error"=>false) );
	    }
	}


	public function uploadimage() { 

		$tmp_subdir = date('Y-m-d-H');

		if (!empty($_FILES))
		{

			$tempFile = $_FILES['Filedata']['tmp_name'];

			$arr 		= explode(".",$_FILES['Filedata']['name']);
			$extension  = end($arr);
			$filename   = str_replace(".".$extension,"",$_FILES['Filedata']['name']);

			$filename   = str_replace("-","_",$filename);
			$finalName  = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $filename);
			$finalName  = trim($finalName, '-');
			$finalName  = preg_replace("/[\/_|+ -]+/", '_', $finalName) . ".$extension";

			$targetFile = TMP_ASSET_STORAGE . "/" . $tmp_subdir . "/". $finalName;

			if (	!file_exists(TMP_ASSET_STORAGE . "/" . $tmp_subdir)	)
			{
				mkdir(TMP_ASSET_STORAGE . "/" . $tmp_subdir, 0777, true);
			}
			
			if (file_exists($targetFile)) {
				$targetFileInfo = pathinfo($targetFile);
				$targetFile = $targetFileInfo['dirname'] . '/' . $targetFileInfo['filename'] . '_' . strtotime('now') . '.' . $targetFileInfo['extension'];
			}

			if (move_uploaded_file($tempFile, $targetFile))
			{
				echo TMP_ASSET_URL . '/' . $tmp_subdir . "/". basename($targetFile);
			}
			else
			{
				$msg = null;

				switch ($_FILES['Filedata']['error'])
				{
					case 0:
						$msg = "No Error"; // comment this out if you don't want a message to appear on success.
						break;
					case 1:
						$msg = "The file is bigger than this PHP installation allows";
						break;
					case 2:
						$msg = "The file is bigger than this form allows";
						break;
					case 3:
						$msg = "Only part of the file was uploaded";
						break;
					case 4:
						$msg = "No file was uploaded";
						break;
					case 6:
						$msg = "Missing a temporary folder";
						break;
					case 7:
						$msg = "Failed to write file to disk";
						break;
					case 8:
						$msg = "File upload stopped by extension";
						break;
					default:
						$msg = "unknown error ".$_FILES['Filedata']['error'];
						break;
				}

				//echo $_FILES['Filedata']['error'];

				echo $msg;
			}

		}
		else
		{
			echo "0";
		}
		$this->renderNothing();
	}


	public function setphoto()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout->disableLayout();

		$request	= $this->getRequest();
	    	if ($request->isPost() && $request->getParam('photo', 0))
	    	{
			// Save member photo
			$member =	$_SESSION['member'];
			if (!is_null($member))
			{
				$photo = $request->getParam('photo');
				// Convert image URL back to storage path
				$photo = str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $photo);
				$member->ingestPhoto($photo);
				echo json_encode( array("error"=>false) );
			}
			else
			{
				echo json_encode( array("error"=>true) );
			}
	    	}
		else
		{
			echo json_encode( array("error"=>true) );
		}
	}

	public function isdeleted(){}

	function search()
	{
		$this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout->disableLayout();
		$term = $this->_getParam('term');
		$conditions = "first_name like '".$term."%' OR last_name like '".$term."%'";
		$members = Member::find('all',array('conditions' => $conditions, 'select' => 'id , CONCAT(first_name," ",last_name) as label','limit' => 20));
		$items = array();
		if ($members){
			foreach($members as $member){
				$items[] = array('id' => $member->id, 'label' => $member->label);
			}
		}
		echo json_encode($items);
	}


	protected function _publishingcityoptions($options)
	{
		$request = $this->getRequest();
		
		/*
		if ( !$request->isPost() || !$request->isXmlHttpRequest() ) {
			$this->_redirect('/');
			die;
		}*/
	
		//$cityId  = (int)$request->getPost('publish_city_id');
		$cityId  = (int)$request->getParam('publish_city_id');
		
		if ( empty($cityId) ) {
			die(Zend_Json::encode(array('error' => true)));
		}
	
		$response = array('error' => false);
	
		try
		{
			$city = City::find_by_id($cityId);
	
	
			// options of inventory
			if ( 'inventories' == $options ) {
				$response['options'] = '<option value="">Select A Hopper</option>';
				for ( $i = 1; $i <= (int)$city->number_of_inventory_units; $i++ ) {
					$response['options'] .= "<option value=\"{$i}\">Hopper #{$i}</option>";
				}
			}
	
			// options of hoppers
			if ( 'hoppers' == $options ) {
				
				$response['options'] = '<option value="">Select A Hopper</option>';
				
				for ( $i = 1; $i <= (int)$city->number_of_hoppers; $i++ ) 
				{
					$response['options'] .= "<option value=\"{$i}\">Hopper #{$i}</option>";
				}	
			}
		} catch ( Exception $e ) {
			$response['error'] = true;
		}
	
		die(Zend_Json::encode($response));
	}

	public function changepublishingcity()
	{
	    $this->_publishingcityoptions('hoppers');
	}

	public function changeinventoryunit()
	{
        $this->_publishingcityoptions('hoppers');
	}

	public function validate_affiliate_slug() {
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$member = null;
		
		if ($_SESSION['member']) $member = $_SESSION['member'];
		
		$slug 			= $this->getRequest()->getParam('slug');
		$slug  			= preg_replace('/[^A-Za-z0-9\-]/', '', $slug);
		$validator 		= new Blastit_Affiliate_SlugValidator;
		$valid   		= $validator->validate($slug, $member);

		$json 		= array('slug' => $slug, 'allowed' => $valid, 'message' => $validator->getMessage());
		
		echo Zend_Json::encode($json);
		
	}


	public function info()
	{
		//$this->_helper->layout->setLayout('wizard');

		$this->view->member = $member = $_SESSION['member'];

		$this->useAffiliateLayout();

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('View Member Info Page');
		}	

		$request = $this->getRequest();
		if ( !$request->isPost() || empty($member) )
		{
			$this->renderAffiliateView();
			return;
		}
	
		$photo = $request->getPost('photo');
		$email = $request->getPost('email');
		$phone = $request->getPost('phone');
		$fname = $request->getPost('first_name');
		$lname = $request->getPost('last_name');
		$slug  = $request->getPost('slug');
		
		$slug  = preg_replace('/[^A-Za-z0-9\-]/', '', $slug);
		
		if ( empty($member->email) && empty($email) )
		{
			$this->view->emailError = true;
			$this->renderAffiliateView();
			return;
		}	

		$member->photo = $photo;
		$member->email_override = $email;
		$member->phone = $phone;
		
		if ($fname) $member->first_name = $fname;
		if ($lname) $member->last_name  = $lname;

		$validator = new Blastit_Affiliate_SlugValidator;
		
		if ($member->affiliate_account && $slug) {
			$validator = new Blastit_Affiliate_SlugValidator;
			if ($validator->validate($slug, $member)) {
				$member->affiliate_account->slug = $slug;
				$member->affiliate_account->save();
				$cache = Zend_Registry::get('cache');
                $cache->remove('affiliate_slugs');
			} else {
				return;
			}
		} 
		
		if(stristr(APPLICATION_ENV, "cityblast") && $member->type->is_customer)
		{
			$member->brokerage = $request->getPost('brokerage');

			$promo_code = !empty($_COOKIE['promo_code']) ? $_COOKIE['promo_code'] : null;
			
			$member->mapFranchise($promo_code);	
		}

		$member->save();

		if (!empty($_COOKIE['cityblast_aff_id'])) 
		{
			if ($_COOKIE['cityblast_aff_id'] == 1183) mail("alen@alpha-male.tv", "FB Click - Info - " . COMPANY_NAME. " " . $member->email,"");
			if ($_COOKIE['cityblast_aff_id'] == 1184) mail("alen@alpha-male.tv", "Google Click - Info - " . COMPANY_NAME. " " . $member->email,"");
		}
		
		if ($member->type->is_customer && (!$member->payment_gateway || ($member->payment_gateway->local_processing == 1)) ) {
			$redirect_url = '/member/payment?new=1';
		} else {
			$redirect_url = '/member/settings';
		}
		
		// If the member registered via landing page then we need to use that landing pages payment page
		// this is to ensure proper tracking by google analytics
		if (isset($_COOKIE['landing_page_group_name']) && isset($_COOKIE['landing_page_name'])) 
		{
			$payment_url = $this->view->url(array(
					'group_name' => $_COOKIE['landing_page_group_name'],
					'page_name' => $_COOKIE['landing_page_name'],
				), 
				'landing_page_group_payment'
			);
		}
		
		$this->_redirect($redirect_url);
	}
	

	
	public function payment()
	{	
		$request = $this->getRequest();
		$params  = $request->getParams();
				
		$this->view->payment_error = null;
		
		$this->view->promo_code = !empty($_COOKIE['promo_code']) ? $_COOKIE['promo_code'] : null;
		
		if (!isset($_SESSION['member']) || empty($_SESSION['member'])) $this->redirect("/member/authenticate");
		
		
		$member = Member::find($_SESSION['member']->id);
    
		if (!empty($params['type_id'])) 
		{
			$member->applyType($params['type_id']);
		}
		
		if (!empty($params['billing_cycle'])) 
		{
			$member->applyBillingCycle($params['billing_cycle']);
		}

		if (isset($_COOKIE['cb_price_id']) && !empty($_COOKIE['cb_price_id'])) 
		{
			if ($_COOKIE['cb_price_id'] == 1) $member->price = "49.99";
			if ($_COOKIE['cb_price_id'] == 2) $member->price = "39.99";
			if ($_COOKIE['cb_price_id'] == 3) $member->price = "29.99";
			$member->save();
		} 
		elseif (empty($member->price) || $member->price == 0) 
		{
			$member->applyPrice();
		}		
		
		//The PROMO CODE should override any other price
		if (!empty($params['promo_code'])) 
		{
			$promo = Promo::find_by_promo_code(strtolower($params['promo_code']));
		} 

		if (isset($promo) && $promo instanceOf Promo) 
		{
			$member->applyPromo($promo);
			
			if(isset($promo->franchise_id) && !empty($promo->franchise_id)) $member->franchise_id = $promo->franchise_id;

			$member->save();
			$this->view->promo_code = $promo->promo_code;
		}
		
		$this->view->rebill = 0;
		if (isset($_REQUEST['rebill']) && $_REQUEST['rebill']) $this->view->rebill = $_REQUEST['rebill'];
				
        	        	
        	$_SESSION['member'] = $this->view->member = $member;   

		if (isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1183) mail("alen@alpha-male.tv","FB Click - Payment Started - " . COMPANY_NAME. " " . $member->email,"");
		if (isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1184) mail("alen@alpha-male.tv","Google Click - Payment Started - " . COMPANY_NAME. " " . $member->email,"");  
		
		// Set payment processing to default
		$default_payment_gateway = PaymentGateway::find_by_is_default(1);
		$member->payment_gateway_id = $default_payment_gateway->id;

		$member->save();
		
		
		$this->view->member = $member;
		
		$states = State::all(array('order' => 'name ASC'));
		$this->view->states = $states;


		
		$request = $this->getRequest();
		if ($request->isPost()) 
		{
			if (!$member->payment_gateway) {
				throw new Exception('Payment gateway not set');
			}
			
			$params = $request->getParams();
			if ($member->payment_gateway->makePayment($member, $params))
			{
				//Everything worked well, pass them along to the thank-you page
				
				$thankyou_url = "/member/settings#invoices";

		
		
				//Let's figure out how many previous non-zero payments this person has made. If it's zero, then send the welcome email
				$total_payments = Payment::count("all", array("conditions" => "member_id=".$member->id));

				
				if(!$total_payments)
				{
					$thankyou_url = '/member/settings/thankyou/1#thankyoutab';
						
					// Send welcome email
					$params	=   array(
						'member'	=>  $member,
						'total_count' => $this->view->total_count
					);
				
					$member->welcome_email($params);		
				}
				
					
				
				// If the member registered via landing page then we need to use that landing pages thank you URL
				// this is to ensure proper tracking by google analytics
				if (isset($_COOKIE['landing_page_group_name']) && isset($_COOKIE['landing_page_name'])) 
				{
					$thankyou_url = $this->view->url(array(
							'group_name' => $_COOKIE['landing_page_group_name'],
							'page_name' => $_COOKIE['landing_page_name']
						), 
						'landing_page_group_thankyou'
					);
				}
		
				// We can unset the tracking cookies at this stage
				setcookie("landing_page_group_name", "", time() - 3600, '/');
				setcookie("landing_page_name", "", time() - 3600, '/');

				//If this is a REBILL, then override all of the thank-you settings and send them to the /invoices page
				if(isset($params['rebill']) && $params['rebill'])
				{
					$success_message = $this->getFlashSuccessHtml(array('Your credit card information has been successfully updated.'));
					$this->addMessage($success_message, 'colorbox');
				}
	
				//KISS METRICS IDENTIFY USER
				if(defined('KISSMETRICS_API_KEY'))
				{
					require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');
		
					KissMetrics::init(KISSMETRICS_API_KEY);
					KissMetrics::record('Payment RECEIVED');
				}	
				
				$this->_redirect($thankyou_url);
			}
			else
			{
				//There was a problem, show the error
				$this->view->payment_error = $member->payment_error;


				//KISS METRICS IDENTIFY USER
				if(defined('KISSMETRICS_API_KEY'))
				{
					require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');
		
					KissMetrics::init(KISSMETRICS_API_KEY);
					KissMetrics::record('Payment ERROR - ' . $member->payment_error);
				}	

			} 						
		}  
		else
		{
			//KISS METRICS IDENTIFY USER
			if(defined('KISSMETRICS_API_KEY'))
			{
				require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');
	
				KissMetrics::init(KISSMETRICS_API_KEY);
				KissMetrics::record('View Payment Page');
			}				
		} 	    
		
		$this->useAffiliateLayout();
		$this->renderAffiliateView(); 	       	        	        			
	}
	
	public function get_prices()
	{	
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$params = $this->getRequest()->getParams();
		$member_id = !empty($params['member_id']) ? $params['member_id'] : null;
		$member = !empty($member_id) ? Member::find($member_id) : null;
		
		$continue = true;
		$params['error'] = false;
		
		if (empty($member)) 
		{
			$params['error'] = "Hmmmm, that's weird, we can't seem to find your Member record!";
			$continue = false;
		}
		
		$promo_code = !empty($params['promo_code']) ? $params['promo_code'] : null;
		$promo = !empty($promo_code) ? Promo::find_by_promo_code(strtolower($promo_code)) : null;
		
		$promo_code_valid = false;
		if ($promo) 
		{
			if (!empty($params['type_id'])) 
			{
				$type = MemberType::find($params['type_id']);
				if ($type && $type->is_customer) $member->type_id = $params['type_id'];
			}
			$promo_code_valid = $promo->isPermittedFor($member);
			$params['promo_code_discount'] = ($promo->type == 'FIXED_AMOUNT' ? '$' : '') . $promo->amount . ($promo->type == 'PERCENT' ? '%' : '');
		}
		
		$price = $member->price;
		$price_parts = explode('.',  number_format($price, 2));
		$billing_cycle_months = MemberType::getBillingCycleMonths($member->billing_cycle);
		$price_month = number_format($price / $billing_cycle_months, 2, '.', '');
		$price_month_parts =  explode('.', $price_month);

		$params['promo_code_valid'] = $promo_code_valid;
				
		if ($continue) 
		{
			$member_types = MemberType::all();
			$billing_cycles = array('month', 'biannual', 'annual');
			foreach ($member_types as $member_type) 
			{
				foreach ($billing_cycles as $billing_cycle) 
				{
					$default_price = $member_type->getPrice($billing_cycle);
					$price = $promo_code_valid ? $promo->getPrice($member_type->id, $billing_cycle) : $default_price;
					$price_parts =  explode('.',  number_format($price, 2));
					
					$billing_cycle_months = MemberType::getBillingCycleMonths($billing_cycle);
					$price_month = number_format($price / $billing_cycle_months, 2, '.', '');
					$price_month_parts =  explode('.', $price_month);

					$params[$member_type->id][$billing_cycle]['price'] = number_format($price, 2);
					$params[$member_type->id][$billing_cycle]['price_dollars'] = $price_parts[0];
					$params[$member_type->id][$billing_cycle]['price_cents'] = $price_parts[1];
					$params[$member_type->id][$billing_cycle]['price_month'] = $price_month;
					$params[$member_type->id][$billing_cycle]['price_month_dollars'] = $price_month_parts[0];
					$params[$member_type->id][$billing_cycle]['price_month_cents'] = $price_month_parts[1];
					$params[$member_type->id][$billing_cycle]['billing_cycle_months'] = $billing_cycle_months;
				}
			}
		}


		$result = json_encode($params);
		echo $result;
	}

	public function membersonly()
	{
		$this->_redirect("/member/authenticate");	
	}

	public function memberstatuschange()
	{
		$request = $this->getRequest();
		
		if ($request->isPost())
		{
			$userid = $request->getPost('userid');
			$status = $request->getPost('status');
			$member = Member::find_by_id($userid);
			$member->payment_status = $status;
			$member->save();
		}
		exit;
	}

	public function testpublishtofacebook()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}

		$member_id = $_SESSION['member']->id;
		$member = Member::find($member_id);

		/************
		$query = "select * from post where type='FACEBOOK' AND member_id=".intval($member_id)." AND result=1 AND fb_post_id IS NOT NULL AND fb_post_id != 'EMPTY' order by created_at DESC LIMIT 1";
		$posts = Post::find_by_sql($query);			
		$post = current($posts);

		$publish_queue 	= PublishingQueue::find_by_listing_id($post->listing->id);
		************/
		
		
		$publish_queue 	= $member->find_blast(array('target_network' => 'facebook'));

		if (is_object($publish_queue)) {
			$blast = $member->publishFacebook($publish_queue, FALSE);
		} else {
			$blast = array('error'=>true,'message'=>'Unable to find a message to publish.');
		}
		
		$this->view->blast = $blast;
	}

	public function testpublishtofacebookfanpage()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}

		$member_id = $_SESSION['member']->id;
		$member = Member::find($member_id);

		
		$publish_queue 	= $member->find_blast(array('target_network' => 'facebook'));

		if (is_object($publish_queue) && $publish_queue) {
			$blast = $member->publishFacebookFanpage($publish_queue, FALSE);
		} else {
			$blast = array('error'=>true,'message'=>'Unable to find a message to publish.');
		}

		
		$this->view->blast = $blast;
	}

	public function testpublishtotwitter()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}

		$member_id = $_SESSION['member']->id;
		$member = Member::find($member_id);

		/*******
		$query = "select * from post where type='TWITTER' AND member_id=".intval($member_id)." AND result=1 AND twitter_post_id IS NOT NULL AND twitter_post_id != 'EMPTY' order by created_at DESC LIMIT 1";
		$posts = Post::find_by_sql($query);			
		$post = current($posts);

		$publish_queue 	= PublishingQueue::find_by_listing_id($post->listing->id);
		*********/
		
		$publish_queue 	= $member->find_blast(array('target_network' => 'twitter'));
		
		if (!is_null($publish_queue)) {
			$blast = $member->publishTwitter($publish_queue, FALSE);
		} else {
			$blast = array('error'=>true,'message'=>'Unable to find a message to publish.');
		}
		
		$this->view->blast = $blast;
	}

	public function testpublishtolinkedin()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}

		$member_id = $_SESSION['member']->id;
		$member = Member::find($member_id);

		/*******
		$query = "select * from post where type='TWITTER' AND member_id=".intval($member_id)." AND result=1 AND twitter_post_id IS NOT NULL AND twitter_post_id != 'EMPTY' order by created_at DESC LIMIT 1";
		$posts = Post::find_by_sql($query);			
		$post = current($posts);

		$publish_queue 	= PublishingQueue::find_by_listing_id($post->listing->id);
		*********/
		
		$publish_queue 	= $member->find_blast(array('target_network' => 'linkedin'));


		if (!is_null($publish_queue)) 
		{
			$blast = $member->publishLinkedin($publish_queue, TRUE);
		} else {
			$blast = array('error'=>true,'message'=>'Unable to find a message to publish.');
		}	

		//exit();
		
		$this->view->blast = $blast;
	}

	public function hide_blast_video() {
		$this->_helper->viewRenderer->setNoRender(true);
        	$this->_helper->layout->disableLayout();
		return $this->setMemberFieldValues('show_blast_video', 0);
	}

	public function hide_blast_thankyou() {
		$this->_helper->viewRenderer->setNoRender(true);
        	$this->_helper->layout->disableLayout();
		return $this->setMemberFieldValues('show_thankyou_page', 0);
	}

	private function setMemberFieldValues ($field, $value = 0)
	{
		$allowedFields = array('show_blast_video', 'show_thankyou_page');
		if (!in_array($field, $allowedFields))
			return false;
		
		$member_id = !empty($_SESSION['member']) ? $_SESSION['member']->id : null;
		$member  = Member::find_by_id($member_id);
		$member->$field = $value;
		return $member->save();
	}

	public function get_stats()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}

		$request    =  $this->getRequest();
		$member_id  =  $request->getParam('id');
		
		if ($member_id)
		{
			$member = Member::find($member_id);
			$this->view->member = $member;
		}
		
		$query  = "select click_stat.*, sum(click_stat.count) as total_clicks, member.id, member.first_name, member.last_name, member.uid, member.email, ";
		$query .= "member.facebook_friend_count, member.twitter_followers, member.linkedin_friends_count, ";
		$query .= "member.facebook_publish_flag, member.twitter_publish_flag, member.linkedin_publish_flag, member.frequency_id, member.created_at ";
		$query .= " from click_stat left outer join member on click_stat.member_id=member.id ";
		$query .= "where member.id=".$member->id . " ";
		//$query .= "order by total_clicks DESC";
		$this->view->clicks = Click::find_by_sql($query);


	}
	
	public function fixvisibility()
    {
        /* @var $member Member */
        $request = $this->getRequest();
		$member  = $_SESSION['member'];

		try {
			$member->updateFacebookAppVisibility();
			$member->save();
		} catch (Exception $e) {
			// ignore errors here
		}
		
		$this->view->fixed = false;
		$this->view->fixing = false;

		if ($member->facebook_app_visibility == 'EVERYONE') {
			$this->view->fixed = true;
		}

		if ($request->isPost()) {
			$facebook  = $member->network_app_facebook->getFacebookAPI();
			
			try {
				$data 	   = $facebook->api('/me/permissions', 'DELETE', array('access_token' => $member->access_token));
			} catch (Exception $e) {
				// ignore errors here
			}
				
			$_SESSION['REDIRECT_AFTER_LOGIN'] = '/member/fixvisibility';

			$this->view->doFacebookLogin = true;			
			$this->view->fixing = true;
			
		}
	}
	
	public function videojoin()
	{
		$redirector = $this->_helper->getHelper('Redirector');
		
		$redirector->setCode(301)
				   ->setGotoSimple('index', 'member');
	}
	
	public function howcityblastworks()
	{
		$redirector = $this->_helper->getHelper('Redirector');
		
		$redirector->setCode(301)
				   ->setGotoSimple('why-cityblast', 'index');
	}
	
	public function ssomismatch()
	{
	
	}

	public function ssonotfound()
	{
	
	}

    /**
     * The method which make send listing
     * to the socials
     */
    public function ajaxblastit()
    {
        $data = array(
            'isSuccess'  => false,
            'message'    => '',
            'data'       => array(),
        );
        $listingId = (int) $this->getRequest()->getParam('listingId', 0);
        $memberId = $_SESSION['member']->id;
        $member = Member::find($memberId);

        if (true === Listing::exists($listingId)) {
            $listing = Listing::find($listingId);
            $publishQueueItem = PublishingQueue::find(array('listing_id' => $listingId));
            if (!empty($publishQueueItem)) {
                $firstSocialsTargets = array(
                    'facebook',
                    'twitter',
                    'linkedin',
                );
                $existsSocialsTargets = array();
                foreach ($firstSocialsTargets as $socialName) {
                    $networkProperty = (string) 'network_app_' . $socialName;
                    if (is_object($member->$networkProperty)) {
                        array_push($existsSocialsTargets, $socialName);
                    }
                }
                if (!empty($existsSocialsTargets)) {
                    $member->publishBlast($publishQueueItem, true, $existsSocialsTargets);
                    $data['isSuccess']  = true;
                    $successMessage = 'Listing was successfully posted on the following social ';
                    if (1 < count($existsSocialsTargets)) {
                        $successMessage .= 'networks: ';
                    } else {
                        $successMessage .= 'network: ';
                    }
                    $data['message'] = $successMessage . '"' . implode('", "', $existsSocialsTargets) . '" <br />';
                } else {
                    $data['message'] = "Unfortunately, we couldn't post to any social networks";
                }
            } else {
                $data['message']    = 'Publish queue item does not exists';
            }
        } else {
            $data['message']    = 'Listing does not exists';
        }
        echo json_encode($data);
        exit(0);
    }
}
