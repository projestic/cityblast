 <?php

class TestimonialController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function init()
	{		
		//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');		

		parent::init();

		$this->view->admin_selectedCity    = $this->getSelectedAdminCity();
		$this->view->daily_stats = $this->todaysTrials();
		
	}

	public function listingshowcase()
	{
		$request = $this->getRequest();
	

		$listing = Listing::find($request->getParam('listing_id'));		
		
		//echo "<pre>";
		//print_r($listing);
		
		
		$listing->createShowCase();
		

		$this->redirect("/admin/listings");
		exit();
		
	}


    public function index()
	{
		$cities        = new City();

		$conditions    = "";
		$conditions    = " ( testimonial.testimonial is not null or testimonial.testimonial != '' ) ";

        $select        = intval($this->getSelectedAdminCity());

        if($select)
        {
            $selected = $cities->find($select);
            $conditions .= " and ( member.publish_city_id=".intval($selected->id) . " or member.city_id=".intval($selected->id) ." )";
            $this->view->selectedCityObject    =   $selected;
        }

        $joins = array();
        $joins[] = ' JOIN testimonial ON member.id = testimonial.member_id';

		$conditions = array("joins" => $joins, "conditions" => $conditions, "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Member', $conditions));

		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;

		$this->render( "testimonials" );

	}


    public function view()
    {

        $member = Member::find($this->_request->getParam('id'));


        $this->view->member   =	$member;

    }

	/**
	 * Update member's testimonial status.
	 *
	 * @return string
	 */
	public function updatetestimonial()
	{
		$this->renderNothing();

		$credit_percentage = 0.10;
		$request = $this->getRequest();

		if ($request->isPost()) {
			$testimonial = Testimonial::find($request->getParam('testim_id'));

			if (!$testimonial) {
				echo json_encode(array(
					'status' => 'ERROR',
					'message' => 'Could not find testimonial'
				));
				return;
			}

			if ($testimonial->approved === null) {
				switch ($request->getParam('status')) {
					// Unapproved
					case 0:
						$testimonial->approved = 0;
						break;
					// Approved
					case 1:
						$testimonial->approved = 1;

						// Credit member for entering an approved testimony
						// Disabled for now
//						$member = Member::find($testimonial->member_id);
//						CashTrans::newCredit(
//							$testimonial->member_id,
//							$member->price_month * $credit_percentage,
//							'Approved testimonial',
//							$request->getParam('admin_id')
//						);
						break;
				}
			} else {
				echo json_encode(array(
					'status' => 'ERROR',
					'message' => 'Testimonial already updated'
				));
				return;
			}

			if ($testimonial->save()) {
				echo json_encode(array(
					'status' => 'SUCCESS'
				));
				return;
			} else {
				echo json_encode(array(
					'status' => 'ERROR',
					'message' => ''
				));
				return;
			}
		}
	}

    public function updatetestimoniallink()
    {

        $this->renderNothing();

        $request = $this->getRequest();

        if ( $request->isPost() ) {
            $testimonial = Testimonial::find($this->_request->getParam('testim_id'));
                if( $testimonial ) {

                    $testimonial->link = $this->_request->getParam('link');

                    $testimonial->save();

                }
        }
    }

    public function testimonialimage()
    {

        $this->renderNothing();

        $request = $this->getRequest();

        if (!$request->isPost()) return;

        $testimonial = Testimonial::find($this->_request->getParam('testim_id'));

        $upload = new Zend_File_Transfer();

        $files = $upload->getFileInfo();

        $image_uploaded = $files["Filedata"]["tmp_name"];

        if (!empty($_FILES))
        {
            $tempFile = $_FILES['Filedata']['tmp_name'];

            $fileParts	= explode(".", $_FILES['Filedata']['name']);
            $extension  = end($fileParts);
            $filename   = $testimonial->id .'_'. $testimonial->member_id;

            $filename   = str_replace("-","_",$filename);
            $finalName  = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $filename);
            $finalName  = trim($finalName, '-');
            $finalName  = preg_replace("/[\/_|+ -]+/", '_', $finalName) . ".$extension";

            $imageDest  = '/images/testimonials/' . date('Y') . "/" . date('m') . "/" . $testimonial->id . '/' . $finalName;
            $imageDestPath = CDN_ORIGIN_STORAGE . $imageDest;


            if ( !file_exists(dirname($imageDestPath)) ) mkdir(dirname($imageDestPath), 0777, true);

            if(move_uploaded_file($tempFile, $imageDestPath))
            {
                $testimonial->image = $imageDest;
                $testimonial->save();

                echo 'Image uploaded';
            }
            else
            {
                $msg = null;

                switch ($_FILES['Filedata']['error'])
                {
                    case 0:
                        $msg = "No Error"; // comment this out if you don't want a message to appear on success.
                        break;
                    case 1:
                        $msg = "The file is bigger than this PHP installation allows";
                        break;
                    case 2:
                        $msg = "The file is bigger than this form allows";
                        break;
                    case 3:
                        $msg = "Only part of the file was uploaded";
                        break;
                    case 4:
                        $msg = "No file was uploaded";
                        break;
                    case 6:
                        $msg = "Missing a temporary folder";
                        break;
                    case 7:
                        $msg = "Failed to write file to disk";
                        break;
                    case 8:
                        $msg = "File upload stopped by extension";
                        break;
                    default:
                        $msg = "unknown error ".$_FILES['Filedata']['error'];
                        break;
                }

                echo $msg;
            }


        }
        else
        {
            echo "0";
        }

    }

    private function todaysTrials()
    {
        //Last 45 days...
        $query  = "select DATE(member.cancelled_date) as day, count(distinct(member.id)) as drops from member ";
        $query .= "INNER JOIN payment ON (payment.member_id=member.id) where payment.amount>0 AND member.payment_status = 'cancelled' ";
        $query .= "AND DATE(member.cancelled_date) = DATE(NOW()) ";
        $query .= "GROUP BY day order by day desc LIMIT 1";

        $dropoffs = Member::find_by_sql($query);


        //Last day SIGNUPS...
        $select        = "DATE(created_at) AS join_date, COUNT(id) AS all_trials";
        $conditions    = "DATE(created_at) = DATE(NOW()) AND payment_status='signup' ";

        $group        = "join_date";
        $order        = "join_date DESC";
        $limit        = 1;

        $trials    = Member::find('all', array(
                'select'		=> $select,
                'conditions'   => $conditions,
                'group' 		=> $group,
                'order' 		=> $order,
                'limit' 		=> $limit,
            )
        );



        $sql = "
		SELECT
			distinct(P1.member_id) as member_id,
			P1.id,
			P1.amount,
			DATE_FORMAT(P1.created_at, '%Y-%m-%d') AS day_str,
			P1.created_at,
			member.first_name,
			member.last_name,
			P1.created_at,
			(select SUM(P2.amount)from payment P2 where P2.member_id = P1.member_id AND P2.paypal_confirmation_number!='FREE_REFERRAL_CREDIT') as total_revenue,
			(select count(P3.id) as total_payments from payment P3 where P3.member_id = P1.member_id AND P3.paypal_confirmation_number!='FREE_REFERRAL_CREDIT') as total_payments
		FROM payment P1
		JOIN member ON P1.member_id=member.id
		WHERE
			P1.payment_type = 'clientfinder' and member.payment_status='paid'
			AND (DATE(P1.created_at) = DATE(NOW()))
			AND P1.paypal_confirmation_number!='FREE_REFERRAL_CREDIT'
		GROUP by P1.member_id
		HAVING total_revenue = 0
		ORDER BY P1.id DESC
		";

        $paid = Payment::find_by_sql($sql);

        $dailyStats = new stdClass();
        $dailyStats->trials = !empty($trials[0]) ? $trials[0]->all_trials : 0;
        $dailyStats->paid = count($paid);
        $dailyStats->dropoffs = !empty($dropoffs[0]) ? $dropoffs[0]->drops : 0;
        $dailyStats->timestamp = date('Y-m-d H:i:s');

        $_SESSION['daily_stats'] = $dailyStats;
        return $dailyStats;
    }
}
