<?php
class ListingController extends ApplicationController
{

	protected static $acl = array(
		'approve' 			=> array('odesk', 'assistanteditor'),
		'update' 			=> array('odesk', 'assistanteditor'),
		'comp' 				=> 'superadmin',
		'republish' 		=> 'superadmin',
		'moveup' 			=> 'superadmin',
		'movedown' 			=> 'superadmin',
		'delete' 			=> 'superadmin',
		'undelete' 			=> 'superadmin',
		'removefromqueue' 	=> 'superadmin',
		'tag' 				=> 'superadmin',
		'*' 				=> 'public'
	);

	public function index()
	{
		$request	= $this->getRequest();
		
		// if no view script, we shouldn't be rendering this -- redirect to home		
		$this->redirectIfNoView('/');

		$this->listing404 = false;
		if ($listing_id = $this->_getParam('listing404')) 
		{
			$this->view->listing404 = $listing_id;
			if ($listing_id != 'notfound') 
			{
				$listing = Listing::find_by_id($listing_id);
				if ($listing) $this->_setBookingAgentId($listing);
			}
			
			$this->getResponse()->setHttpResponseCode(404);
		}
		
		/**
		 * display a list of active listing
		 */

		$city_id				= $this->getSelectedCity();

		if (intval($request->getQuery('city_id')) > 0) {
			$city_id = intval($request->getQuery('city_id'));
		}

		$conditions			= "approval=1 AND blast_type!='".Listing::TYPE_CONTENT."' AND image IS NOT NULL";
		
		if (!empty($city_id) && $city_id != '*') 
		{
			$conditions	.= " AND city_id=".$city_id;
		}

		if (isset($_SESSION['search_params']) && is_array($_SESSION['search_params']))
		{
			unset($_SESSION['search_params']);
		}
		//$conditions['conditions'] = "published=1";
		
		$conditions = array('conditions' => $conditions);
		$conditions['order'] = 'id desc';
		$paginationAdapter = new ARPaginator("Listing", $conditions);
		$paginator = new Zend_Paginator($paginationAdapter);
		
		$paginator->setCurrentPageNumber($this->_getParam('page', 1))
				  ->setItemCountPerPage(15);

		$this->view->total		= $paginator->getCurrentItemCount();
		$this->view->paginator	= $paginator;
	}

	public function sample()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}

		$request = $this->getRequest();
		$tag_id = $request->getParam('tag_id');
		
		$tag = Tag::find($tag_id);
		$this->view->tag = $tag;
		
		switch ($tag->ref_code)
		{
			case 'LISTINGS':
				$query = "
					SELECT l.* FROM listing l
					INNER JOIN publishing_queue pq ON pq.listing_id = l.id 
						AND pq.current_count < pq.max_inventory_threshold
					WHERE l.blast_type = '".Listing::TYPE_LISTING."' 
					AND l.status = 'active'
					ORDER BY RAND() LIMIT 1"; 
			break;
			case 'CONTENT_LOCAL':
				$query = "
					SELECT l.* FROM listing l
					INNER JOIN publishing_queue pq ON pq.listing_id = l.id 
						AND pq.current_count < pq.max_inventory_threshold
					WHERE l.blast_type = '".Listing::TYPE_CONTENT."' 
					AND l.status = 'active'
					ORDER BY RAND() LIMIT 1"; 
			break;
			default:
				$query = "
					SELECT l.*
					FROM listing l
					INNER JOIN publishing_queue pq ON pq.listing_id = l.id 
						AND pq.current_count < pq.max_inventory_threshold
					INNER JOIN tag_cloud tc ON l.id = tc.listing_id 
					WHERE tc.tag_id = ? 
					AND l.status = 'active'
					ORDER BY RAND() LIMIT 1";
			break;
		}
		
		$listing = Listing::find_by_sql($query, array($tag_id));
		$listing = !empty($listing) ? $listing[0] : null;
		
		//Need to pull a random desc
		$this->view->description = ListingDescription::getRandomDescription($listing);
	
		
		//Need to pull a random image
		$this->view->image = ListingImage::getRandomImage($listing);


		if($listing->big_image) {
			$filename = CDN_ORIGIN_STORAGE . $this->view->image;
			$filesize = getimagesize($filename);
			
			if ($filesize) {
				$width = $filesize[0];
				$height = $filesize[1];
				if ($width && $height) {
					$this->view->new_height = round(($height * 446) / $width);
				}
			}
		}


		if(!empty($_SESSION['member'])) $member = $_SESSION['member'];
		else $member = Member::find(35);
		$publish_array = $listing->getFacebookPayload($member, "fb"); 


		$this->view->link_name = !empty($publish_array['name']) ? $publish_array['name'] : null;
		$this->view->description = !empty($publish_array['description']) ? $publish_array['description'] : '';

		$this->view->listing = $listing;
	}

	public function login()
	{
		//This should lead you through the same process as the /member/signup

		//Once we have the user, redirect them bact to /listing/details
	}


	public function signuplander()
	{

		if(isset($_SESSION['member']))
		{
			//Automatically redirect them to /listing/create
			$this->_redirect("/blast/login");
		}
		else
		{
			//After you sign-up or log in, redirect to the listing details
			$_SESSION['next'] = "/listing/details";
			$this->_redirect("/member/signup");
		}
	}

	public function unapprove() {
		$this->renderNothing();
		
		$request     = $this->getRequest();
		$listing_id  = $request->getParam('id');
		$listing	 = Listing::find($listing_id);
		
		if (!$this->memberIs('superadmin')) {
			if ($this->memberIs('assistanteditor')) {
				$listing->assistant_approval = 0;
			} else {
				$listing->pre_approval = 0;
			}
			$listing->save();
		}
		
		if ($this->getParam('redirect')) {
			$this->redirect($this->getParam('redirect'));
		} else {
			$this->redirect("/admin");
		}
		
	}

	public function approve()
	{
		$this->renderNothing();

		$request    	= $this->getRequest();
		$params        	= $request->getParams();
		$listing_id  	= $request->getParam('id');
		$hopper_id		= $request->getParam('hopper_id');
		$city_id		= $request->getParam('city_id');
		
		$message = new Zend_Session_Namespace('messages');
		if (!isset($message->data)) $message->data = new stdClass;

		$listing	= Listing::find($listing_id);

		if (!$this->memberIs('superadmin')) {
		
			if ($this->memberIs('assistanteditor')) {
				$listing->assistant_approval = 1;
			} else {
				$listing->pre_approval = 1;
				
				// this value must be set so listing goes through assistant editor role
				if ($listing->blast_type == 'CONTENT') $listing->assistant_approval = 0;
				
			}
			$listing->save();
			$message->data->msg   =	"Listing '".$listing->message."' was approved!";
		
		} else {		
			
			$listing->pre_approval = 1;
			$listing->approval		=   1;
			$listing->approved_by_id	=   $_SESSION['member']->id;
			$listing->approved_at	=   date("Y-m-d H:i:s");
			$listing->save();
			
			//Ok, now let's insert the item into the Publishing Queue
			$publish = new PublishingQueue();
			$publish->listing_id 	= $listing->id;
			$publish->city_id 		= $request->getParam('city_id');
			$publish->hopper_id 	= $hopper_id;
			$publish->current_count = 0;

			if(empty($listing->list_city->max_inventory_threshold))
			{
				$max = 10000;
			}
			else
			{
				$max = $listing->list_city->max_inventory_threshold;
			}

			$publish->min_inventory_threshold 	= $listing->list_city->min_inventory_threshold;
			$publish->max_inventory_threshold 	= $max;

			$publish->save();
			
			if(empty($message->data))
				$message->data = new stdClass;
			$message->data->msg   =	"Listing '".$listing->message."' was approved!";

			$city = City::find_by_id($listing->city_id);
			$_SESSION['admin_selectedCity']		= $listing->city_id;
			$_SESSION['admin_selectedCityName']	= $city->name;
			$_SESSION['admin_selectedCountry']		= $city->country;
			
            $listing->sendPublishNotification();
		}

		if ($this->getParam('redirect')) {
			$this->redirect($this->getParam('redirect'));
		} else {
			$this->redirect("/admin");
		}

	}
	
	
	public function deleteimage()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
		
		if($this->getRequest()->isPost())
		{
			ListingImage::find($this->params['image_id'])->delete();
		}
		exit;
	}

	public function update()
	{

		$this->_helper->layout->setLayout('admin');

		$request    =   $this->getRequest();
		($id 		= $request->getParam('id')) || ($id = $request->getParam('list_id')); 
		$conditions = array('id = ?', $request->getParam('id'));
		
		if (!$this->memberIs('superadmin')) {
			$conditions = array('id = ? AND pre_approval = 0 AND approval = 0', $id);
		}

		$listing	= $this->view->listing = Listing::find(array('conditions' => $conditions) );

		if (!$listing) $this->_redirect('/');

		if(!$this->getRequest()->isPost())
		{
				//STEP 2: Get the LISTING...
			
			if($id){
				$query = "select count(*) as total_clicks from click_stat where listing_id=$id";
				$this->view->click_stat = Listing::find_by_sql($query);
				$this->view->click_stat = $this->view->click_stat[0];
			}
			
			$twitter_suffix = "... " . APP_URL . "/listing/view/".$id;


			//Only cities that support blating...
			$this->view->cities = $cities	= City::find('all', array('conditions' => 'UPPER(name) != "CANADA" and UPPER(name) != "UNITED STATES"', 'order' => 'name'));

			$city_id	= intval($this->getSelectedAdminCity());

			$this->view->selectedCityObject = null;
			if($city_id != "" && $city_id != "*") $this->view->selectedCityObject = City::find_by_id($city_id);



			$current_length = mb_strlen($listing->message);
			$suffix_length = mb_strlen($twitter_suffix);


			if(($current_length + $suffix_length) > 140)
			{
				$total_available = 140 - $suffix_length;
				$prefix = mb_substr($listing->message, 0, $total_available);
				$this->view->twitter_preview = $prefix . $twitter_suffix;
			}
			else
			{
				$this->view->twitter_preview = $listing->message . $twitter_suffix;
			}

		}
		else
		{
				//STEP 2: Save the LISTING...
			$params =	$this->getRequest()->getParams();

			$member = $_SESSION['member'];

			$listing->city_id		=   $params['city_id'];
			//$listing->hoper_id		=   $params['hoper_id'];

			$listing->message		=   $params['message'];
			$listing->mls			=   $params['mls'];
			$listing->square_footage =  $params['square_footage'];
			$listing->appartment	=   $params['appartment'];
			$listing->street		=   $params['street'];
			$listing->price			=   $params['price'];
			$listing->city			=   $params['city'];
			$listing->postal_code	=   $params['postal_code'];

			$listing->number_of_bedrooms  = intval($params['number_of_bedrooms']);
			$listing->number_of_extra_bedrooms  = intval($params['number_of_extra_bedrooms']);
			$listing->number_of_bathrooms = intval($params['number_of_bathrooms']);
			$listing->number_of_parking   = intval($params['number_of_parking']);
			$listing->maintenance_fee 	= $params['maintenance_fee'];
			$listing->property_type_id 	= $params['property_type_id'];
			
			if ($this->memberIs('superadmin')) {
				if (!$listing->approval && $params['approval']) {
					$listing->approved_by_id = $member->id;
					$listing->approved_at	 =   date("Y-m-d H:i:s");
				} elseif (!$params['approval']) {
					$listing->approved_by_id = null;
					$listing->approved_at	 = null;
				}
				$listing->approval		 = $params['approval'];
				$listing->pre_approval	 = 1;
			} else {
				$listing->pre_approval	=   $params['approval'];
			}
			
			$listing->member_id		= $params['member_id'];
			$listing->property_status = $params['property_status'];

			#$listing->descriptions	= array();

			$listing->save();
			$listing->setLatitude();

			if(isset($params['fileUploaded']) && !empty($params['fileUploaded']))
			{

				$image  = stristr($params['fileUploaded'], TMP_ASSET_URL) ? $params['fileUploaded'] : TMP_ASSET_URL.$params['fileUploaded'];
				$image 	= str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $image);
				$listing->ingestImage($image);

			}
			
			if(isset($params['thumbUploaded']) && !empty($params['thumbUploaded']))
			{
				if(!stristr($params['thumbUploaded'], TMP_ASSET_URL))
				{
					$image = TMP_ASSET_URL . $params['thumbUploaded'];
				}
				else
				{
					$image = $params['thumbUploaded'];
				}
				$image 	= str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $image);
				
				$listing->ingestThumbnail($image);
			}

			if ($listing->property_status == 'SOLD' || $listing->property_status == 'EXPIRED') {
				$listing->retire();
			}

			$i = 1;
			$endCycle = false;
			do {
				$uploadField = 'fileUploaded' . $i;

				if(!isset($params[$uploadField]))
				{
					$endCycle = true;
				} 
				elseif (!empty($params[$uploadField]))
				{
					$path = stristr($params[$uploadField], TMP_ASSET_URL) ? $params[$uploadField] : TMP_ASSET_URL . $params[$uploadField];
					$path = str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $path);

					$altImage = new ListingImage();
					$altImage->listing_id = $listing->id;
					$altImage->save();
					$altImage->ingestImage($path);
					unset($altImage);
				}
				$i++;
			} while (!$endCycle);


			if (!isset($params['description']) || !is_array($params['description']))
			{
				// if no additional description
				$params['description']	= array();
			}

			//ListingDescription::adjustDescriptions($listing, $params['description']);
			if(isset($params['description']) && is_array($params['description']))
			{
				ListingDescription::adjustDescriptions($listing, $params['description']);
			}


			$message    =	new Zend_Session_Namespace('messages');
			if(!isset($message->data)) $message->data = new stdClass;
			
			$message->data->msg   =	"Listing changed to '".$listing->message."'!";

			if ($this->memberIs('odesk') || $this->memberIs('assistanteditor')) {
				$this->redirect("/admin/pending");
			}

			if($listing->approval)
			{
				$this->redirect("/admin/");
			}
			else
			{
				$this->redirect("/admin/listings/unpublished/");
			}
		}

	}

	//Give someone a free message
	public function comp()
	{
		$params =	$this->getRequest()->getParams();

		$id	= (int)$params['id'];
		$listing	= Listing::find($id);


		//For auditing purposes track it in the payment table
		$payment = new Payment();


		$payment->paypal_confirmation_number = "Complimentary listing courstesy of: " . $_SESSION['member']->uid;
		$payment->member_id = $listing->member_id;
		$payment->amount = "0.00";
		$payment->save();


		//$listing->payment_id = $payment->id;
		$listing->save();


		$message = new Zend_Session_Namespace('messages');
		if (!isset($message->data)) $message->data = new stdClass;
		$message->data->msg   =	"Complementary Listing Approved '".$listing->message."'";


		$this->redirect("/admin/");

	}


	public function agentemail()
	{
		$listing_id = $this->_getParam("id");

		if( FALSE === filter_var($listing_id, FILTER_VALIDATE_INT) ) {

		    $this->_redirect('/listing');
		    return;
		}
		$this->view->listing_id	= $listing_id;
	}


	public function agentshare()
	{
		$params =	$this->_getAllParams();


		if( FALSE === filter_var($params["listing_id"], FILTER_VALIDATE_INT) || !($listing = Listing::find($params["listing_id"]) ))
		{
		    $this->_redirect('/listing');
		    return;
		}

		$this->view->listing = $listing;

		$params["email_friend"] = str_replace(";", ",", $params["email_friend"]);
		$emails = explode(",",$params["email_friend"]);

		foreach($emails as $email)
		{
			$email = trim($email);
			//echo "<h1>EMAILING:".$email."</h1>";


			if( FALSE !== filter_var($email, FILTER_VALIDATE_EMAIL) )
			{


				$this->view->to_email	=   $email;

				if(isset($_SESSION['member']))
				{
					 $from_name = $_SESSION['member']->first_name . " " . $_SESSION['member']->last_name;
					 $member_id = $_SESSION['member']->id;
				}
				else
				{
					$from_name = "Your friends";
					$member_id = null;
				}

				$params['total_count']	= $this->view->total_count;

				if(isset($_SESSION['member']))
				    $from_email = $_SESSION['member']->email;
				else $from_email = "info@cityblast.com";

				$params['from_name'] = $from_name;
				$params['member_id'] = $member_id;
				$params['listing']	= $listing;
				$params['recent_blasts']= $this->getRecentBlasts(4);

				$subject = 'Your listing on ' . COMPANY_WEBSITE;
				$htmlBody	=   $this->view->partial('email/agent-share-listing.html.php', $params);
				
				$mandrill = new Mandrill(array('AGENT_LISTING_EMAIL'));
				$mandrill->setFrom($from_name, $from_email);
				$mandrill->addTo("", $email);
				$mandrill->send($subject, $htmlBody);
	
				//Keep track of the Email in our local LOG
				$mail = new ZendMailLogger('AGENT_LISTING_EMAIL', $listing->id);
				$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
				$mail->setFrom($from_email, $from_name);
				$mail->addTo($email, $email);
				$mail->setSubject($subject);
				$mail->log();	

				$msg = "Listing sharing email ";
				if(isset($_SESSION['member']->email))
					$msg .= "sent by member: ". $_SESSION['member']->email;
				if(isset($email))
				    $msg .= " sent to: ".$email;
			}
			else
			{
				$msg = "INVALID Agent Share Listing email ";
				if(isset($_SESSION['member']->email))
					$msg .= "sent by member: ". $_SESSION['member']->email;
				if(isset($email))
				    $msg .= " sent to: ".$email;


				mail("alen@alpha-male.tv", "INVALID: Referral Email" . $email, $msg);

			}
		}


		mail("alen@alpha-male.tv", "Agent Sharing Listing Email Sent", $msg);
	}



	public function email()
	{
		$listing_id = $this->_getParam("listing_id", "");

		if( FALSE === filter_var($listing_id, FILTER_VALIDATE_INT) ) {

		    $this->_redirect('/listing');
		    return;
		}
		$this->view->listing_id	= $listing_id;
	}


	public function share()
	{
		$params =	$this->_getAllParams();


		if( FALSE === filter_var($params["listing_id"], FILTER_VALIDATE_INT) )
		{
		    $this->_redirect('/listing');
		    return;
		}

		$this->view->listing = $listing = Listing::find($params["listing_id"]);

	
		
		$params["email_friend"] = str_replace(";", ",", $params["email_friend"]);

		$emails = explode(",",$params["email_friend"]);

		foreach($emails as $email)
		{
			$email = preg_replace('/\s+/', '', $email);
			$email = trim($email);
			//echo "<h1>EMAILING:".$email."</h1>";

			if( FALSE !== filter_var($email, FILTER_VALIDATE_EMAIL) )
			{
				$this->view->to_email	=   $email;

				if(isset($_SESSION['member']))
				{
					 $from_name = $_SESSION['member']->first_name . " " . $_SESSION['member']->last_name;
					 $member_id = $_SESSION['member']->id;
				}
				else
				{
					$from_name = "Your friends";
					$member_id = null;
				}

				$params['total_count']	= $this->view->total_count;

				if(isset($_SESSION['member']))
				    $from_email = $_SESSION['member']->email;
				else $from_email = "info@cityblast.com";


				$params['from_name'] = $from_name;
				$params['member_id'] = $member_id;

			
			$params['comments']	= $params["comments"];

				$params['listing']	= Listing::find_by_id($params['listing_id']);
				$params['recent_blasts']= $this->getRecentBlasts(4);

				$subject = $from_name.' thought you might like this listing on '.COMPANY_WEBSITE.'.';
				$htmlBody	=   $this->view->partial('email/share-listing.html.php', $params);


				/********
				$mail = new ZendMailLogger("LISTING_EMAIL", $member_id);
				$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
				$mail->setFrom($from_email, $from_name);
				$mail->addTo($email);

				if(isset($from_name) && isset($from_email)) $mail->setReplyTo( $from_email, $from_name);

				$mail->setSubject();
				$sentEmail = $mail->sendemail();
				*******/


				$mandrill = new Mandrill(array('AGENT_LISTING_EMAIL'));
				$mandrill->setFrom($from_name, $from_email);
				$mandrill->addTo("", $email);
				$mandrill->send($subject, $htmlBody);
	
	
				//Keep track of the Email in our local LOG
				$mail = new ZendMailLogger('AGENT_LISTING_EMAIL', $this->id);
				$mail->setBodyHtml ( $htmlBody, 'utf-8', 'utf-8' );
				$mail->setFrom ( $fromEmail, $fromName );
				$mail->addTo ( $email, $email );
				$mail->setSubject ( $subject );
				$mail->log();	



				$msg = "Listing share email ";
				if(isset($_SESSION['member']->email))
					$msg .= "sent by member: ". $_SESSION['member']->email;
				if(isset($email))
				    $msg .= " sent to: ".$email;
			}
			else
			{
				$msg = "INVALID Share Listing email ";
				if(isset($_SESSION['member']->email))
					$msg .= "sent by member: ". $_SESSION['member']->email;
				if(isset($email))
				    $msg .= " sent to: ".$email;

				mail("alen@alpha-male.tv", "INVALID: Referral Email" . $email, $msg);

			}
		}

		if ( !empty($listing) && is_array($listing->comments) )
		{
			$msg .= "\n\nListing Comments\n";
			foreach ( $listing->comments as $comment )
			{
				$msg .= "{$comment->fb_name}: {$comment->text}\n";
			}
		}
		
		$msg .= APP_URL."/listing/view/".$listing->id;
		
		mail("alen@alpha-male.tv", "Listing Share Email Sent", $msg);
	}


	public function republish()
	{
		$params =	$this->getRequest()->getParams();

		$id	= (int)$params['id'];
		$listing	= Listing::find($id);


		$listing->published = 0;
		$listing->save();


		$message = new Zend_Session_Namespace('messages');
		if (!isset($message->data)) $message->data = new stdClass;
		$message->data->msg   =	"Listing Republished Set '".$listing->message."'";


		$this->redirect("/admin/");

	}


	public function view()
	{
		$this->_helper->layout->setLayout('listing-layout');	
		
				
		$id			=  $this->_getParam('id', 0);
		$member_id	=  $this->_getParam('mid');

		$this->view->has_comments = 1;

		//Set a 30 day cookie for the BOOKING AGENT ID
		if(!empty($member_id))
		{
			// If the useragent is a bot or the referer is a search engine
			// - redirect to the same URL minus the member id
			// - This is to prevent search engine traffic from overiding a listings default agent
			if(Blastit_Http_UserAgentIdentifier::isBot() || Blastit_Http_RefererIdentifier::isSearchEngine())
			{
				$redirector = $this->_helper->getHelper('Redirector');
				$redirector->setCode(301) // Moved Permanently
                           ->setExit(true)
                           ->gotoRoute(array('id' => $id), 'listing_view1');
			}

			$memberExists = true;
			try {
				// actually it seems that the member is not used in the view anyway, as for 2013dec02
				$this->view->member = Member::find($member_id);
			}
			catch (ActiveRecord\RecordNotFound $ex) 
			{
				$this->view->warnings = array();
				$this->view->warnings[] = "Couldn't find agent with ID: " . $member_id;
				$memberExists = false;
			}

			if ($memberExists)
			{
				setcookie(
					'cityblast_booking_agent_id',
					$member_id,
					time()+ time()+60*60*24*60
				);
				$_COOKIE['cityblast_booking_agent_id'] = $member_id;
			}
		}
		elseif(isset($_SESSION['member']))
		{
			$member_id	=  	$_SESSION['member']->id;

			$memberExists = true;
			try {
				// actually it seems that the member is not used in the view anyway, as for 2013dec02
				$this->view->member = Member::find($member_id);
			}
			catch (ActiveRecord\RecordNotFound $ex) 
			{
				$this->view->warnings = array();
				$this->view->warnings[] = "Couldn't find agent with ID: " . $member_id;
				$memberExists = false;
			}

			if ($memberExists)
			{
				setcookie(
					'cityblast_booking_agent_id',
					$member_id,
					time()+ time()+60*60*24*60
				);
				$_COOKIE['cityblast_booking_agent_id'] = $member_id;
			}
			
		}
		
		
		if ($id)
		{
			//Grab the LISTING from the DB
			$this->view->listing = $listing = Listing::find_by_id($id);
		
			if (!$listing)  
			{
				$listing_deleted = Listing::find_deleted($id);

				if ($listing_deleted) 
				{
					$this->_redirector = $this->_helper->getHelper('Redirector');
					$this->_redirector->gotoSimple('index', 'listing', null, array('listing404' => $listing_deleted->id));
				} else {
					$this->_redirector = $this->_helper->getHelper('Redirector');
					$this->_redirector->gotoSimple('index', 'listing', null, array('listing404' => 'notfound'));
				}
			}
			

			if(($listing->reblast_id > 0) && $listing->reblast && ($listing->reblast_id != $listing->id)) // Preventing infinite recursivity 
			{
				$this->_redirect("/listing/view/".$listing->reblast_id.'/'.$member_id);
			}

			$original_listing = $listing;
			while ($original_listing->reblast_from) 
			{
				$original_listing = $original_listing->reblast_from;
			}
			$this->view->image_listing_id = $original_listing->id;

			$query = "select count(id) as total_clicks from interaction where type='facebook_share' and listing_id=".$listing->id;
			$this->view->fb_clicks = Click::find_by_sql($query);
			
			$query = "select count(id) as total_clicks from interaction where type='twitter_share' and listing_id=".$listing->id;
			$this->view->tw_clicks = Click::find_by_sql($query);
			
			$query = "select count(id) as total_clicks from interaction where type='share_listing_email' and listing_id=".$listing->id;
			$this->view->em_clicks = Click::find_by_sql($query);
	
	
			//This should redirect the user to whatever external link
			
			if($listing->isContent())
			{
				//prevent loop of infinite redirect and redirect back
				if (!isset($_SESSION['listing_loop_' . $listing->id]) || $_SESSION['listing_loop_' . $listing->id] <= (strtotime('now')-3) ) 
				{
					$_SESSION['listing_loop_' . $listing->id] = strtotime('now');
					$this->redirect($listing->content_link);
				}
			}

			$click = new Click();
			$click->listing_id = $listing->id;

			$click->url = "/listing/view/".$listing->id;
			$click->date = date('Y-m-d H:i');
			$click->count = 1;


			$page_type 	= $this->_getParam('page_type');
			if($page_type == "fb") $click->page_type = "facebook";
			if($page_type == "fp") $click->page_type = "fanpage";
			if($page_type == "tw") $click->page_type = "twitter";
			if($page_type == "ln") $click->page_type = "linkedin";				
				

			//Set the BOOKING AGENT (start with the current agent that's been passed)

			$this->_setBookingAgentId($listing, $click);

			$this->view->booking_agent = Member::find($this->view->booking_agent_id);

	
				
			if(isset($_SERVER['HTTP_USER_AGENT'])) $click->source = $_SERVER['HTTP_USER_AGENT'];
		
			if(!stristr($_SERVER['HTTP_USER_AGENT'], 'facebook') && $_SERVER['HTTP_USER_AGENT'] != 'robot')
			{
				$click->save();
			}
		
			



			if(is_null($listing))
			{
				throw new Exception("Invalid listing id provided: ".$id);
			}

			$price_components = explode(".", $listing->price);
			$price_components[0] = preg_replace("/[^a-zA-Z0-9\s]/", "", $price_components[0]);
			$listing->price = $price_components[0];




			if(!$listing->isContent())
			{
				if($listing->price==0) $listing->price=0;

				//$this->view->title = $listing->street . ", " . $listing->city . " $" . number_format($listing->price) . " : City-Blast";
				$title = $this->view->title = $listing->street . ", " . $listing->city;

				$description	=   $this->view->description = $listing->message;
				$this->view->keyword = "Real Estate Home For Sale " . $listing->street . " " . $listing->city . " " . number_format($listing->price);


				// Definig aditional meta
				$permalink  =	APP_URL . "/listing/view/" . $listing->id;
				$additional_meta	=   '
					<meta property="og:image" content="'.CDN_URL.$listing->getOriginal()->image.'"/>
					<meta property="og:url" content="'.$permalink.'"/>
					<meta property="og:site_name" content="'.APP_URL.'"/>
					<meta property="og:title" content="'.$title.'"/>
					<meta property="og:description" content="'.$description.'"/>
					<meta property="twitter:card" content="summary_large_image"/>
					<meta property="twitter:domain" content="' . DOMAIN . '"/>
					<meta property="twitter:image" content="'.CDN_URL.$listing->getOriginal()->image.'"/>
					<meta property="twitter:title" content="'.$title.'"/>
					<meta property="twitter:description" content="'.$description.'"/>
					';

				$twitter_app = Zend_Registry::get('networkAppTwitter');

				if ($twitter_app->username) $additional_meta .= '<meta property="twitter:site" content="' . $twitter_app->username . '"/>
				';
				if ($twitter_app->user_id) $additional_meta .= '<meta property="twitter:site:id" content="' . $twitter_app->user_id .'"/>
				';

				$this->view->additional_meta =	$additional_meta;
			}



			if (!empty($listing))
			{
				$conditions	= " AND id != " . $listing->id;

				if(!empty($listing->city_id))
				$conditions	.= " AND city_id = ".$listing->city_id;
			}

			$this->view->last_4 = $this->getRecentBlasts(3, $conditions);
			
			//LAST THREE BLOG ENTRIES
			$result = null;
			$app_config = Zend_Registry::get('appConfig');





			$this->view->blogs = null;
			if ($app_config['wordpress']['enabled']) 
			{
			
				$config = Zend_Registry::get('config');				
				$wp_de_conn = mysql_connect($config->db->host, $config->db->username, $config->db->password);
				mysql_select_db($config->db->blog->database, $wp_de_conn);
				
				$query   = "SELECT ID, post_name, post_title, post_excerpt, post_date, guid, ( ";
				$query  .= "SELECT COUNT( C1.comment_ID ) ";
				$query  .= "FROM wp_dpvjuu_comments AS C1 ";
				$query  .= "JOIN wp_dpvjuu_posts AS P1 ON P1.ID = C1.comment_post_ID WHERE P1.ID=P2.ID AND C1.comment_approved=1 ";
				$query  .= ") AS total_comments ";
				$query  .= "FROM wp_dpvjuu_posts as P2 ";
				$query  .= "WHERE P2.post_status = 'publish' ";
				$query  .= "ORDER BY P2.post_date DESC ";
				$query  .= "LIMIT 3 ";


				$result = mysql_query($query);
				if (!$result) 
				{
					die('Invalid query: ' . mysql_error());
				}
			
				$this->view->blogs = $result;
			}

		}
		else
		{
			//display a list of active listing
			$this->_redirect('/listing');
		}
		
	}

	protected function _setBookingAgentId($listing, $click = null) 
	{
		if(isset($_COOKIE['cityblast_booking_agent_id']) && !empty($_COOKIE['cityblast_booking_agent_id']))
		{		
			$this->view->booking_agent_id = $_COOKIE['cityblast_booking_agent_id'];
			if ($click) $click->member_id = $_COOKIE['cityblast_booking_agent_id'];
		}
		elseif(isset($_SESSION['member']->id) && !empty($_SESSION['member']->id))
		{
			$this->view->booking_agent_id = $_SESSION['member']->id;
			if ($click) $click->member_id = $_SESSION['member']->id;
		}
		elseif(!empty($member_id))
		{
			$this->view->booking_agent_id = $member_id;				
			if ($click) $click->member_id = $member_id;					
		}
		else
		{

			if(isset($listing->member->id)) 
			{
				$this->view->booking_agent_id = $listing->member->id;
			} else {
				$this->view->booking_agent_id = null;
			}
		}
	}

	public function moveup()
	{
		$this->renderNothing();

		$request    =   $this->getRequest();
		$id  =  $request->getParam('id');

		$current_hoper = Hoper::find_by_listing_id($id);


		// Get all listing with priority < CURRENT LISGING FROM HOPERS QUEUE

		$smallerHopersList	=   Hoper::all(array("conditions"=>"priority<".$current_hoper->priority." AND hoper_id='".$current_hoper->hoper_id."'","order"=>"priority DESC", "limit"=>1));
		if(count($smallerHopersList)>0)
		{
		$current_hoper->priority    =	$smallerHopersList[0]->priority-5;
		$current_hoper->save();
		}

		// Reorder the list

		// Reordering the whole list
		$hoperList	=   Hoper::all(array("conditions"=>"hoper_id='".$current_hoper->hoper_id."'","order"=>"priority ASC"));
		$many	=   1;
		foreach($hoperList as $h)
		{
		if($h->listing_id==0) continue;
		$h->priority    =	$many*10;
		$h->save();
		$many++;

		$tmp_listing = Listing::find_by_id($h->listing_id);
		$tmp_listing->priority = $h->priority;
		$tmp_listing->save();
		}

		$this->redirect("/admin/");
	}

	/**********
	public function tmp()
	{
		$listings = Listing::all();

		foreach($listings as $listing)
		{
			$listing->priority = $listing->id * 10;
			$listing->save();
		}

		echo "Fin<BR>";
		exit();
	} ************/

	public function movedown()
	{
		$this->renderNothing();

		$request    =   $this->getRequest();
		$id  =  $request->getParam('id');

		$current_hoper = Hoper::find_by_listing_id($id);


		// Get all listing with priority < CURRENT LISGING FROM HOPERS QUEUE

		$smallerHopersList	=   Hoper::all(array("conditions"=>"priority>".$current_hoper->priority." AND hoper_id='".$current_hoper->hoper_id."'","order"=>"priority ASC", "limit"=>1));
		if(count($smallerHopersList)>0)
		{
		$current_hoper->priority    =	$smallerHopersList[0]->priority+5;
		$current_hoper->save();
		}

		// Reorder the list

		// Reordering the whole list
		$hoperList	=   Hoper::all(array("conditions"=>"hoper_id='".$current_hoper->hoper_id."'","order"=>"priority ASC"));
		$many	=   1;
		foreach($hoperList as $h)
		{
		if($h->listing_id==0) continue;
		$h->priority    =	$many*10;
		$h->save();
		$many++;

		$tmp_listing = Listing::find_by_id($h->listing_id);
		$tmp_listing->priority = $h->priority;
		$tmp_listing->save();
		}

		$this->redirect("/admin/");
	}

	public function delete()
	{
		$this->renderNothing();

		if($this->getRequest()->isPost())
		{
		    $request    =   $this->getRequest();
		    $id  =  $request->getParam('id');

		    $listing = Listing::find_by_id($id);
		    if ($listing) 
		    {	    
                $reason = $request->getParam('reason');
                
                if (!empty($reason) && $request->getParam('notify_creator')) {
                    $listing->sendDeleteNotification($reason);
                }
                
				$listing->delete();

				$action = new DeleteAction;
				if ( isset($_SESSION['member']) ) $action->member_id = $_SESSION['member']->id;
				
				$action->listing_id = $listing->id;
				$action->reason = $reason;
				$action->save();

				$message = new Zend_Session_Namespace('messages');
				$message->data = new stdClass;
				$message->data->msg = "Listing successfully deleted '".$listing->message."'!";
			}
			
		    $this->redirect("/admin/listings/");
		}

		$this->redirect("/");
	}

	public function undelete()
	{
		$this->renderNothing();

		$request    =   $this->getRequest();
		$id  =  $request->getParam('id');

		$listing = Listing::find_deleted($id);		    
		$message = new Zend_Session_Namespace('messages');
		if (!isset($message->data)) $message->data = new stdClass;
		
		if ($listing) {
			$listing->undelete();
			$message->data->msg   =	"Listing undeleted: '".$listing->message."'";
		} else {
			$message->data->msg   =	"Listing $id not found";
		}
		$this->redirect("/admin/listings/");

	}

	public function removefromqueue()
	{
	    $this->renderNothing();

		$request    =   $this->getRequest();
		$id  =  $request->getParam('id');

		$listing = Listing::find_by_id($id);
		$listing->priority	=   0;
		$listing->save();

		// Removing from hopers queue
		$hoper  =	Hoper::find_by_listing_id($listing->id);
		if(!is_null($hoper)){
		$hoper->delete();
		}

		$message = new Zend_Session_Namespace('messages');
		if (!isset($message->data)) $message->data = new stdClass;
		$message->data->msg   =	"Blast successfully removed from queue: '".$listing->message."'!";

		$this->redirect("/admin/");

	}


	public function search()
	{

		$request	= $this->getRequest();

		if ($request->isPost() || (isset($_SESSION['search_params']) && is_array($_SESSION['search_params'])))
		{

			if ($request->isPost())
			{
				$params		= $request->getParams();
				$_SESSION['search_params']	= $params;
			}
			else
			{
				$params		= $_SESSION['search_params'] ;
			}

			$searchParams = $params;
			array_walk($searchParams, function($val,$key) use(&$searchParams){$searchParams[$key] = addslashes($val);});
			
			$paginator	= Listing::search($searchParams, 15, $this->_getParam('page', 1));
			$this->view->paginator	= $paginator;
			$this->view->total		= $paginator->getTotalItemCount();

			foreach ($params as $key => $value) {

				$this->view->$key	= $value;
			}

			$this->render('index');
		}
		else
		{

			$this->_redirect("/listing/");
		}

		#$this->view->cities	= City::find('all', array('conditions' => 'UPPER(name) != "CANADA" and UPPER(name) != "UNITED STATES"', 'order' => 'name'));

	}
	
	private function retire($listingId){
		$publish = PublishingQueue::find_by_sql("select * from publishing_queue where listing_id = $listingId");
		
		if($publish){
			foreach($publish as $pid){
				$pid->delete();
			}
		}
		
		$listing	= Listing::find($listingId);
		$listing->published = $listing->published + 1;
		$listing->published_on = date("Y-m-d H:i:s");
		$listing->save();
	}
	
	public function listingstatus()
	{
		$request =  $this->getRequest();
		$listing_id = $request->getParam('id');
		$status = $request->getParam('status');
		
		$listing = Listing::find($listing_id);
		if($listing)
		{
			$status = strtolower($status);
			if('sold'==$status)
			{
				$listing->property_status = 'SOLD';
			}
			if('expired'==$status)
			{
				$listing->property_status = 'EXPIRED';
			}
			if('price_change'==$status)
			{
				$newprice = $request->getParam('newprice');
				$listing->property_status = 'PRICE_CHANGE';
				$listing->old_price = $listing->price;
				$listing->price = $newprice;
			}
			
			$listing->save();
			if('sold'==$status || 'expired'==$status)
			{
				$this->retire($listing_id);
			}
		}

		$this->_redirect("/member/settings#listingstab");
		exit;
	}

	public function relist() 
	{
		$request =  $this->getRequest();
		$listing_id = $request->getParam('id');
		$listing = Listing::find($listing_id);
		
		if($listing)
		{
			$listing->property_status = 'NULL';
			$listing->save();
		}
		
		$this->_redirect("/member/settings#listingstab");
		exit;
	}

	public function reblast() 
	{
		$request =  $this->getRequest();
		$listing_id = $request->getParam('id');
		$listing = Listing::find_by_id_and_reblast_id($listing_id, null);
		
		if ($listing && $listing->reblastPermitted()) {

			$attributes = $listing->attributes();
			unset($attributes['id']);
			unset($attributes['reblast_id']);
			unset($attributes['published']);
			unset($attributes['created_at']);
			unset($attributes['published_on']);
			unset($attributes['expiry_date']);
			$attributes['expiry_date']  = 	date("Y-m-d +30days");
			
			$newListing = new Listing($attributes);
			$newListing->save();

			$listing->reblast_id = $newListing->id;
			$listing->save();
			
			//Cheat the PUB-QUEUE
			$publish_queue = new PublishingQueue();
			$publish_queue->city_id = $newListing->city_id;
			$publish_queue->hopper_id = 3;
			$publish_queue->listing_id = $newListing->id;
			$publish_queue->priority = 1;
			$publish_queue->min_inventory_threshold = 1;
			$publish_queue->max_inventory_threshold = 9999;
			$publish_queue->current_count = 0;
			$publish_queue->save();
		
			$member = $_SESSION['member'];
			
			//PUBLISH...
			$member->publishBlast($publish_queue, false);
		}
		$this->_redirect("/member/settings#listingstab");
		exit;
	}
	
	function tag()
	{
		$this->_helper->layout->setLayout('admin');		

		$query = 'SELECT
				l.id,
				l.title,
				tc.id as tag_id
			FROM
				listing AS l
			INNER JOIN publishing_queue pq ON l.id = pq.listing_id AND pq.hopper_id = 1
			LEFT JOIN tag_cloud as tc ON l.id = tc.listing_id
			WHERE
				l.blast_type = "'.Listing::TYPE_CONTENT.'" AND
				l.status != "deleted" AND
				tag_id IS NULL
			GROUP BY
				l.id
			ORDER BY
				l.id DESC';
		
		$tags = Listing::find_by_sql($query);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($tags));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);

		$this->view->paginator = $paginator;
	}

	function embedded_block()
	{
		$this->_helper->layout->disableLayout();
		$request =  $this->getRequest();
		$listing_id = $request->getParam('id');
		$member_id = $request->getParam('member_id');
		$block_id = $request->getParam('block_id');
		$listing = Listing::find_by_id_and_reblast_id($listing_id, null);
		$member = Member::find_by_id($member_id);
		if ($listing != null && $member != null) {
			$this->view->listing = $listing;
			$this->view->member = $member;
			$this->view->block_id = $block_id;
		} else {
			$this->_helper->viewRenderer->setNoRender(true);
		}
	}
}
?>
