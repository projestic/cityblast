<?
class TestController extends ApplicationController
{
	CONST AMOUNT			=   "49.00";
	CONST TAXES			=   "0.13"; // 13%
	CONST ITEM_NAME		=   "City-Blast Listing Payment";
	CONST CURRENCY			=   "CAD";
	CONST PRIORITY_STEPS	=   10;
	
	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{			
		parent::init();
	}

	/************
	public function paytest()
	{
		$member = Member::find(35);
		$member->processRecurringPayment();	
		
		exit();
	} **************/

	public function blast()
	{
		$member = Member::find(1439);
		
		$member_blast = $member->find_blast();

		
		$member->publishBlast($member_blast);
		$member->rotateCurrentContent();	
		
		echo "Finished<BR>";
		exit();	
	}

	public function canadianrealestatemagazine()
	{
		$query = "
				select click_stat.listing_id, url_mapping.url, count(click_stat.id) as total_clicks, listing.created_at from click_stat 
				join url_mapping on click_stat.listing_id=url_mapping.listing_id join listing on listing.id=url_mapping.listing_id 
				where url_mapping.url like '%canadianrealestatemagazine%' group by click_stat.listing_id order by listing_id DESC;";
		
		$output = Listing::find_by_sql($query);		
		
		echo "<table style='border: 1px;'>";
		echo "<tr><th>List ID</th><th>URL</th><th>Clicks</th><th>Created At</th></tr>";
		foreach($output as $row)
		{
			echo "<tr><td>".$row->listing_id . "</td><td>" . $row->url . "</td><td>" . $row->total_clicks . "</td><td>" . $row->created_at->format('Y-m-d') . "</td></tr>\n";	
		}
		echo "</table>";
			
		exit();		
	}

	public function album()
	{
		$member = Member::find(35);
		
		$member->publishAlbum();	
		
		echo "FINISHED";
		exit();			
	}
	

	public function welcome()
	{
		$member = Member::find(35);

		// Send welcome email
		$params	=   array(
			'member'	=>  $member,
			'total_count' => '375000'
		);

		$htmlBody      =   $this->view->partial('email/welcome.html.php', $params);

		echo $htmlBody;
		exit();
	}

	public function image()
	{
		
		$listings = Listing::find_by_sql(
			"SELECT * FROM listing ".
			"WHERE blast_type != '".Listing::TYPE_CONTENT."' ".
			"AND status != 'deleted' ".
			"LIMIT 0 10 ORDER BY ID ASC"
		);
		
		$resizer	= new Image();
		
		foreach($listings as $listing ){
			
			echo "Updating:".$listing->id."\n";
		
			if(isset($listing->image) && !empty($listing->image))
			{
				$resizer->resizeListingImage($listing->id, $listing->image);
			}
			
			if(isset($listing->alt_image_1) && !empty($listing->alt_image_1))
			{
				$resizer->resizeListingImage($listing->id, $listing->alt_image_1);
			}
			
			if(isset($listing->alt_image_2) && !empty($listing->alt_image_2))
			{
				$resizer->resizeListingImage($listing->id, $listing->alt_image_2);
			}
			
			if(isset($listing->alt_image_3) && !empty($listing->alt_image_3))
			{
				$resizer->resizeListingImage($listing->id, $listing->alt_image_3);
			}
			
			if(isset($listing->alt_image_4) && !empty($listing->alt_image_4))
			{
				$resizer->resizeListingImage($listing->id, $listing->alt_image_4);
			}	
			
		}	
	}

	public function fanpage_direct()
	{
		$member_id = $this->getRequest()->getParam('member_id');
		$listing_id = $this->getRequest()->getParam('listing_id');
		
		$member = Member::find($_SESSION['member']->id);
		$listing = Listing::find($listing_id);
		
		echo "<pre>";

		
		$facebook = $member->network_app_facebook->getFacebookAPI();
		
		
		
		foreach($member->fanpages as $fanpage)
		{
			//print_r($fanpage);	
			
			if($fanpage->active == "yes")
			{
				//Source is fp = fanpage
				$publish_array = $listing->getFacebookPayload($member, "fb");
				
				//$result = $member->publishDirectToFacebook($facebook, $listing, $publish_array);
				$result = $member->publishDirectToFanpage($facebook, $fanpage, $listing, $publish_array);
				
				
				print_r($result);
				echo "<br>";
				echo "\n\n";				
			}
		}
		
		
		exit();
	}
	
	public function facebook_direct()
	{
		$member_id = $this->getRequest()->getParam('member_id');
		$listing_id = $this->getRequest()->getParam('listing_id');
		
		$member = Member::find($_SESSION['member']->id);
		$listing = Listing::find($listing_id);
		
		echo "<pre>";

		
		$facebook = $member->network_app_facebook->getFacebookAPI();



		//Source is fp = fanpage
		$publish_array = $listing->getFacebookPayload($member, "fb");
		
		$result = $member->publishDirectToFacebook($facebook, $listing, $publish_array);

		
		
		print_r($result);
		echo "<br>";
		echo "\n\n";		
		
				
		
		exit();
	}

	public function fanpage()
	{
		//include_once (APPLICATION_PATH . "/../scripts/publish.post.inc.php");
		
				
		$member = Member::find(35);



		//Set up the FACEBOOK VARIABLES...
		$publish_array['access_token'] = $member->network_app_facebook->getFacebookAppAccessToken();		
		$facebook = $member->network_app_facebook->getFacebookAPI();

		$api_url		= "https://graph.facebook.com/me/accounts";
		$accounts 	= json_decode(file_get_contents($api_url));

		//get the access token to post to your page via the graph api
		foreach ($accounts->data as $account)
		{
		    $page_id = $account->id;
		    
				//found the access token, now we can break out of the loop
				$page_access_token = $account->access_token;
				
				$api_url		= "https://graph.facebook.com/".$page_id."/?access_token=" . $page_access_token;
				$page_info 	= json_decode(file_get_contents($api_url));		
		
				if($account->category != "Application")
				{
					echo "<pre>";
					print_r($page_info);	
				}
				
				/***********
		    //publish a story to the page's wall (as the page)
		    $post_id = $fb->api("/{$your_page_id}/feed", "POST", array(
		        'message' => "Hello to all my fans, I love you!"
		        'access_token'  => $page_access_token;
		    )); ************/
					
				
		}



		exit();
		
	}

	
	
	function testblast()
	{
		Logger::setEcho(true);
		Logger::setHtml(true);
		Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
		Logger::logTimeMarker();

		$member_id = $this->getRequest()->getParam('member_id');
		
		$member = Member::find($member_id);
		
		
		//Let's find out the next listing this particular member should publish...	
		$member_blast = $member->find_blast();
		
		//echo "<pre>";
		//print_r($member_blast);
	
	
		$member->publishBlast($member_blast);
		echo "<h1>FINISHED!</h1>";
		
		//publishFacebook($member, $pubqueue->listing, $pubqueue);
		
		exit();	
	}
	
	


	public function facebook()
	{
		Logger::setEcho(true);
		Logger::setHtml(true);
		Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
		Logger::logTimeMarker();
		
		$request 	= $this->getRequest();
		
		$id 			= $request->getParam('id');
		$member_id 	= $request->getParam('member_id');
		
		if(isset($member_id) && !empty($member_id)) $member = Member::find($member_id);
		else $member = $_SESSION['member'];
		
		$listing_id 	= $request->getParam('listing_id');
		
		if (empty($id) && empty($listing_id)) {
			Logger::log('No content specified!!', Logger::LOG_TYPE_DEBUG);
		}
		
		if(isset($listing_id) && !empty($listing_id))
		{
			$pubqueue = PublishingQueue::find(array('conditions' => 'listing_id='.$listing_id, 'order' => 'id DESC', 'limit' => 1) );
		}
		else
		{		
			$pubqueue = PublishingQueue::find($id);
		}

		$member->publishFacebook($pubqueue);
		echo "<h1>FINISHED!</h1>";
		
		//publishFacebook($member, $pubqueue->listing, $pubqueue);
		
		exit();
	}

	public function twitter()
	{
		Logger::setEcho(true);
		Logger::setHtml(true);
		Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
		Logger::logTimeMarker();
		
		$request 	= $this->getRequest();
		$id 	= $request->getParam('id');
		$member_id 	= $request->getParam('member_id');
		if(isset($member_id) && !empty($member_id)) $member = Member::find($member_id);
		else $member = $_SESSION['member'];
	
		$listing_id 	= $request->getParam('listing_id');
		
		if (empty($id) && empty($listing_id)) {
			Logger::log('No content specified!!', Logger::LOG_TYPE_DEBUG);
		}
		
		if(isset($listing_id) && !empty($listing_id))
		{
			$pubqueue = PublishingQueue::find(array('conditions' => 'listing_id='.$listing_id, 'order' => 'id DESC', 'limit' => 1) );
		}
		else
		{		
			$pubqueue = PublishingQueue::find($id);
		}

		$member->publishTwitter($pubqueue);
		echo "<h1>FINISHED!</h1>";

		exit();
	}

	public function linkedin()
	{
		Logger::setEcho(true);
		Logger::setHtml(true);
		Logger::setLogTypes(Logger::LOG_TYPE_DEBUG);
		Logger::logTimeMarker();
		
		$request 	= $this->getRequest();
		$id 	= $request->getParam('id');
		$member_id 	= $request->getParam('member_id');
		if(isset($member_id) && !empty($member_id)) $member = Member::find($member_id);
		else $member = $_SESSION['member'];
			
		$listing_id 	= $request->getParam('listing_id');
		
		
		if (empty($id) && empty($listing_id)) {
			Logger::log('No content specified!!', Logger::LOG_TYPE_DEBUG);
		}
		
		if(isset($listing_id) && !empty($listing_id))
		{
			$pubqueue = PublishingQueue::find(array('conditions' => 'listing_id='.$listing_id, 'order' => 'id DESC', 'limit' => 1) );
		}
		else
		{		
			$pubqueue = PublishingQueue::find($id);
		}

		//echo "<pre>";
		//print_r($pubqueue);


		$member->publishLinkedin($pubqueue);
		echo "<h1>FINISHED!</h1>";
				

		exit();
	}

}
?>
