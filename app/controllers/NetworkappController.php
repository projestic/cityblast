<?php

class NetworkappController extends ApplicationController
{
	protected static $acl = array(
		'*' => array('salesadmin', 'superadmin')
	);
	
	public function init()
	{
		parent::init();
		$this->_helper->layout->setLayout('admin');
	}
	
	public function index()
	{
		$conditions = array(
			'select'=> "
				n.*, 
				CASE n.network
					WHEN 'FACEBOOK' THEN 
						(
							SELECT count(id) AS total 
							FROM member 
							WHERE 
								application_id = n.id AND 
								status = 'active' AND 
								payment_status IN ('signup', 'paid', 'free')
						)
					WHEN 'TWITTER' THEN 
						(
							SELECT count(id) AS total 
							FROM member 
							WHERE 
								tw_application_id = n.id AND 
								status = 'active' AND 
								payment_status IN ('signup', 'paid', 'free')
						)
					WHEN 'LINKEDIN' THEN 
						(
							SELECT count(id) AS total 
							FROM member 
							WHERE 
								ln_application_id = n.id AND 
								status = 'active' AND 
								payment_status IN ('signup', 'paid', 'free')
						)
				END AS members_registered,
				(
					SELECT count(id) AS total 
					FROM member 
					WHERE 
						status = 'active' AND 
						payment_status IN ('signup', 'paid', 'free')
				) AS members_total
			", 
			'from' => 'network_app AS n',
			"conditions" => '', 
			"order" => "registration_enabled DESC, network ASC, id DESC"
		);
		$paginator = new Zend_Paginator(new ARPaginator('NetworkApp', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
	}
	
	public function edit()
	{
		$request = $this->getRequest();
		
		$id	= $request->getParam('id');
		$params = $request->getParams();

		$error_fields = array();
		if($request->isPost()) {
			$required_params = array('network', 'app_name', 'consumer_id', 'consumer_secret');
			if ($params['network'] == 'TWITTER') {
				$required_params[] = 'username';
				$required_params[] = 'user_id';
			}
			$is_valid = true;
			foreach ($required_params as $required_param) {
				if (!isset($params[$required_param])) continue;
				$value = trim($params[$required_param]);
				if (empty($value)) {
					$is_valid = false;
					$this->view->error = 'Required field missing';
					$error_fields[] = $required_param;
				}
			}
			
			if ($is_valid) {
				if(!empty($id)) {
					$app = NetworkApp::find($id);
				} else {
					$app = new NetworkApp();
				}
				
				if (isset($params['network'])) $app->network = !empty($params['network']) ? $params['network'] : '';
				if (isset($params['app_name'])) $app->app_name = !empty($params['app_name']) ? $params['app_name'] : '';
				if (isset($params['consumer_id'])) $app->consumer_id = !empty($params['consumer_id']) ? $params['consumer_id'] : '';
				if (isset($params['user_id'])) $app->user_id = !empty($params['user_id']) ? $params['user_id'] : null;
				if (isset($params['username'])) $app->username = !empty($params['username']) ? $params['username'] : null;
				
				$app->consumer_secret = !empty($params['consumer_secret']) ? $params['consumer_secret'] : '';
				$app->notes = !empty($params['notes']) ? $params['notes'] : '';
				$app->registration_enabled = !empty($params['registration_enabled']) ? 1 : 0;

				$app->save();
				
				// Only one app for a given network can be enabled at any time
				if ($app->registration_enabled && $app->id) {
					$sql = "UPDATE network_app SET registration_enabled = 0 WHERE network = ? AND id != ?";
					$values = array($app->network, $app->id);
					NetworkApp::table()->conn->query($sql, $values);
				}
				
				$this->_redirect('/networkapp/');
			}
		}

		$app = null;
		if(!empty($id)) {
			$app = NetworkApp::find($id);
		}
		
		$this->view->error_fields = $error_fields;
		
		$form_values = array();
		$form_values['network'] = $request->getParam('network', (($app) ? $app->network : ''));
		$form_values['app_name'] = $request->getParam('app_name', (($app) ? $app->app_name : ''));
		$form_values['consumer_id'] = $request->getParam('consumer_id', (($app) ? $app->consumer_id : ''));
		$form_values['consumer_secret'] = $request->getParam('consumer_secret', (($app) ? $app->consumer_secret : ''));
		$form_values['notes'] = $request->getParam('notes', (($app) ? $app->notes : ''));
		$form_values['registration_enabled'] = $request->getParam('registration_enabled', (($app) ? $app->registration_enabled : ''));
		
		$this->view->app = $app;
		$this->view->form_values = $form_values;
	}
}