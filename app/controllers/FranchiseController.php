<?php

class FranchiseController extends ApplicationController
{
	protected static $acl = array(
		'*' => array('salesadmin', 'superadmin')
	);
	
	public function init()
	{
		parent::init();
		//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');
	}
	
	public function index()
	{
		$sql = "
			SELECT 
				f.*,
				(
					SELECT 
						COUNT(id) 
					FROM member 
					WHERE
						franchise_id = f.id AND
						status =  'active'
				) AS all_agents,
				(
					SELECT 
						COUNT(id) 
					FROM member 
					WHERE
						franchise_id = f.id AND
						status =  'active' AND 
						(	
							(
								facebook_publish_flag > 0 AND 
								facebook_fails < 3
							) OR 
							(
								linkedin_publish_flag > 0 AND
								linkedin_fails < 3 AND 
								linkedin_access_token IS NOT NULL
							) OR 
							(
								twitter_publish_flag > 0 AND 
								twitter_fails < 3 AND
								twitter_access_token IS NOT NULL
							)
						)
						AND payment_status != 'cancelled'
						AND payment_status != 'payment_failed'
						AND 
						(
							next_payment_date IS NULL OR 
							next_payment_date >= DATE(NOW())
						)
				) AS current_agents,
				(
					SELECT 
						(
							COALESCE(SUM(facebook_friend_count ), 0) + 
							COALESCE(SUM(twitter_followers ), 0) +
							COALESCE(SUM(linkedin_friends_count ), 0)
						) 
					FROM member 
					WHERE
						franchise_id = f.id AND
						status =  'active' AND 
						(	
							(
								facebook_publish_flag > 0 AND 
								facebook_fails < 3
							) OR 
							(
								linkedin_publish_flag > 0 AND
								linkedin_fails < 3 AND 
								linkedin_access_token IS NOT NULL
							) OR 
							(
								twitter_publish_flag > 0 AND 
								twitter_fails < 3 AND
								twitter_access_token IS NOT NULL
							)
						)
						AND payment_status != 'cancelled'
						AND payment_status != 'payment_failed'
						AND 
						(
							next_payment_date IS NULL OR 
							next_payment_date >= DATE(NOW())
						)
				) AS reach
			FROM `franchise` AS f";

		$this->view->franchises = Member::find_by_sql($sql);
	}

	public function alias()
	{
		$request = $this->getRequest();
		
		$franchise_id = $request->getParam('franchise_id');
		
		$aliases = array();
		if (!empty($franchise_id)) {
			$aliases = FranchiseAlias::find_all_by_franchise_id($franchise_id);
		} else {
			$aliases = FranchiseAlias::find('all');
		}
		
		$this->view->alias_list = $aliases;
	}

	public function email()
	{
		$request = $this->getRequest();
		
		$franchise_id = $request->getParam('franchise_id');
		
		$emails = array();
		if (!empty($franchise_id)) {
			$emails = FranchiseEmail::find_all_by_franchise_id($franchise_id);
		} else {
			$emails = FranchiseEmail::find('all');
		}
		
		$this->view->email_list = $emails;
	}


	public function add()
	{
		$this->_helper->layout->setLayout('admin');

		$id	=   $this->_request->getParam('id');



		if($this->_request->isPost())
		{
			if(!empty($id) && $id != 0)
			{
				$franchise = Franchise::find($id);
			}
			else
			{
				$franchise = new Franchise();
			}
			$franchise->name = $this->_request->getParam('name');
			
			$is_enabled = $this->_request->getParam('is_enabled');
			$franchise->is_enabled = !empty($is_enabled) ? 1 : 0;
			
			$is_branded = $this->_request->getParam('is_branded');
			$franchise->is_branded = !empty($is_branded) ? 1 : 0;
			
			$franchise->save();
			
			$this->_redirect('/franchise/index');
		}

		$this->view->franchise = null;
		if(!empty($id))
		{
			$this->view->franchise = Franchise::find($id);
		}
	}
	
	public function delete()
	{
		$id = $this->_request->getParam('id');
		Franchise::table()->delete(array('id' => $id));
		$this->_redirect('/franchise/index');
	}


	public function addalias()
	{
		$this->_helper->layout->setLayout('admin');

		$id = $this->_request->getParam('id');

		$request = $this->_request;

		if($request->isPost())
		{
			$error = null;
			
			$name = $request->getParam('name');
			$name = preg_replace('/[^a-zA-Z0-9\s]+/', '', $name); // Strip none alphanumeric
			$name = preg_replace('/\s+/', ' ', $name); // Normalise white space
			$name = trim($name);
			if (empty($name)) {
				$error = 'You must provide a name';
			} else {
				$existing_alias = FranchiseAlias::find_by_name($name);
				if (!empty($existing_alias)) {
					$franchise = $existing_alias->franchise;
					$franchise_name = !empty($franchise) ? $franchise->name : 'Unknown';
					$franchise_id = !empty($franchise) ? $franchise->id : '?';
					$error = 'This alias "' . $name . '" is already assigned to: ' . $franchise_name . ' (' . $franchise_id . ')';
				}
			}
			
			$franchise_id = $request->getParam('franchise_id');
			if (empty($franchise_id)) {
				$error = 'You must select a franchise';
			}
			
			if (empty($error)) {
				if(!empty($id) && $id != 0)
				{
					$alias = FranchiseAlias::find($id);
				}
				else
				{
					$alias = new FranchiseAlias();
				}

				$alias->name = $name;
				$alias->franchise_id = $franchise_id;
				$alias->save();

				$this->_redirect('/franchise/alias');
			} else {
				$this->view->error = $error;
			}
		}

		$this->view->alias = null;
		if(!empty($id))
		{
			$this->view->alias = FranchiseAlias::find($id);
		}

		$this->view->franchises = Franchise::find('all');
	}
	
	
	public function deletealias()
	{
		$id = $this->_request->getParam('id');
		FranchiseAlias::table()->delete(array('id' => $id));
		$this->_redirect('/franchise/alias');
	}


	public function addemail()
	{
		$this->_helper->layout->setLayout('admin');

		$id	=   $this->_request->getParam('id');

		if($this->_request->isPost())
		{
			if(!empty($id) && $id != 0)
			{
				$email = FranchiseEmail::find($id);
			}
			else
			{
				$email = new FranchiseEmail();
			}

			$email->email = $this->_request->getParam('email');
			$email->franchise_id = $this->_request->getParam('franchise_id');
			$email->save();

			$this->_redirect('/franchise/email');
		}

		$this->view->alias = null;
		if(!empty($id))
		{
			$this->view->email = FranchiseEmail::find($id);
		}

		$this->view->franchises = Franchise::find('all');
	}
	
	public function deleteemail()
	{
		$id = $this->_request->getParam('id');
		FranchiseEmail::table()->delete(array('id' => $id));
		$this->_redirect('/franchise/email');
	}

	public function uploadimage() 
	{ 

		$tmp_subdir = date('Y-m-d-H');

		if (!empty($_FILES))
		{

			$tempFile = $_FILES['Filedata']['tmp_name'];

			$arr		= explode(".",$_FILES['Filedata']['name']);
			$extension  = end($arr);
			$filename   = str_replace(".".$extension,"",$_FILES['Filedata']['name']);

			$filename   = str_replace("-","_",$filename);
			$finalName  = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $filename);
			$finalName  = trim($finalName, '-');
			$finalName  = preg_replace("/[\/_|+ -]+/", '_', $finalName) . ".$extension";

			$targetFile = TMP_ASSET_STORAGE . "/" . $tmp_subdir . "/". $finalName;

			if (!file_exists(TMP_ASSET_STORAGE . "/" . $tmp_subdir)	)
			{
				mkdir(TMP_ASSET_STORAGE . "/" . $tmp_subdir, 0777, true);
			}
			
			if (file_exists($targetFile)) 
			{
				$targetFileInfo = pathinfo($targetFile);
				$targetFile = $targetFileInfo['dirname'] . '/' . $targetFileInfo['filename'] . '.' . $targetFileInfo['extension'];
			}

			if (move_uploaded_file($tempFile, $targetFile))
			{
				echo TMP_ASSET_URL . '/' . $tmp_subdir . "/". basename($targetFile);
			}
			else
			{
				$msg = null;

				switch ($_FILES['Filedata']['error'])
				{
					case 0:
						$msg = "No Error"; // comment this out if you don't want a message to appear on success.
						break;
					case 1:
						$msg = "The file is bigger than this PHP installation allows";
						break;
					case 2:
						$msg = "The file is bigger than this form allows";
						break;
					case 3:
						$msg = "Only part of the file was uploaded";
						break;
					case 4:
						$msg = "No file was uploaded";
						break;
					case 6:
						$msg = "Missing a temporary folder";
						break;
					case 7:
						$msg = "Failed to write file to disk";
						break;
					case 8:
						$msg = "File upload stopped by extension";
						break;
					default:
						$msg = "unknown error ".$_FILES['Filedata']['error'];
						break;
				}

				echo $msg;
			}

		}
		else
		{
			echo "0";
		}
		$this->renderNothing();
	}
	
	
	public function unknown()
	{
		$franchise_unknown_member = FranchiseUnknownMember::all(array('order' => 'id DESC', 'limit' => 100));
		$this->view->franchise_unknown_member = $franchise_unknown_member;
		
		$conditions = array("conditions" => '', "order" => "id DESC");			
		$paginator = new Zend_Paginator(new ARPaginator('FranchiseUnknownMember', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
	}
	
	public function unknowndelete()
	{
		$id = $this->_request->getParam('id');
		FranchiseUnknownMember::table()->delete(array('id' => $id));
		$this->_redirect('/franchise/unknown');
	}
}
