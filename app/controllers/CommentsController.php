<?
class CommentsController extends ApplicationController
{
	protected static $acl = array(
		'delete' => 'superadmin',
		'*' => 'public'
	);

	public function index(){}

	public function broadcast()
	{
		$this->renderNothing();
		if ($this->getRequest()->isPost()) {
			$mid = $this->getRequest()->getParam("mid");
			$facebook_uid = $this->getRequest()->getParam("fuid");
			$name = $this->getRequest()->getParam("name");
			$text = $this->getRequest()->getParam("text");
			$facebook_comment_id = $this->getRequest()->getParam("comment_id");
			$referring_url = $this->getRequest()->getParam("referring_url");
			$text = $this->getRequest()->getParam("text");
			
			$emailer = Zend_Registry::get('alertEmailer');
			$subject = "URGENT - RESPONSE NEEDED - New comment [ ".strtoupper($referring_url)." ] ";
			$msg = "MESSAGE: ".$text . " \n\n URL:" . APP_URL . $referring_url;
			if (strpos($referring_url, '/index/faq') === 0 || strpos($referring_url, '/help') === 0) {
				$emailer->send($subject, $msg, 'comment-urgent');
			} else {
				$emailer->send($subject, $msg, 'comment');
			}
		}
	}

	public function create()
	{
		
		if( $this->getRequest()->isPost())
		{
			$listing	    =   Listing::find_by_id($this->getRequest()->getParam("listing_id"));
	
			if(is_null($listing)) 
			{
				echo "Comments :: Invalid listing id provided: ".$this->getRequest()->getParam("listing_id");
				exit();
			}
	
			//This is retarded!!! DO NOT USE THE LISTING MEMBER HERE!!!
			//
			//  Tge whole value benefit of CITYBLAST is that we show the SALES AGENT info and NOT the LISTING AGENT!!!!
			//
			//$member	    =   $listing->member;
			
			
			$mid	   = $this->getRequest()->getParam("mid");
			$member =	Member::find($mid);
	
			if(is_null($member)) 
			{
				throw new Exception("Comments :: Listing has invalid member attached to it. Listing ID: ".$this->getRequest()->getParam("listing_id"));
			}
							
			
			$facebook_uid  = $this->getRequest()->getParam("fuid");
			$name	    	= $this->getRequest()->getParam("name");
			$text	    	= $this->getRequest()->getParam("text");
			
			$facebook_comment_id    =	$this->getRequest()->getParam("comment_id");
	
			$comment	=   new Comment();
			$comment->fb_comment_id 		=	$facebook_comment_id;
			$comment->fb_uid	    		=	$facebook_uid;
			$comment->fb_name	    		=	$name;
			$comment->fb_admin_member_id  =	intval($member->id);
			$comment->text	    			=	$text;
			$comment->mid	    			=	intval($mid);
			$comment->listing_id    		=	intval($listing->id);
			$comment->save();
	
			
	
			if( $comment->id > 0 ) 
			{
				// Send email
				$from_name = COMPANY_WEBSITE;
				$from_email = HELP_EMAIL;
	
				try 
				{
					$send_to	=   (!empty($mid)) ? Member::find($mid) : $member;
				} 
				catch(Exception $e)
				{
					// Can't find member on mid
					$send_to	=   $member;
					$comment->mid	    =	0;
					$comment->save();
				}
	
				$params	= array();
				$params['member'] = $member;
				$params['from_name'] = $from_name;
				$params['from_email'] = $from_email;
				$params['listing'] = $listing;
				$params['comment'] = $comment;

				$subject  = "You've received a new comment.";
				$htmlBody = $this->view->partial('email/comment-created.html.php', $params);
	
	
				$member_email = !empty($send_to->email_override) ? $send_to->email_override : $send_to->email;


				$mandrill = new Mandrill(array('COMMENT'));
				$mandrill->setFrom($from_name, $from_email);

	
				$mandrill->addTo($send_to->first_name . " " . $send_to->last_name, $member_email);
				$mandrill->send($subject, $htmlBody);


				//Keep track of the Email in our local LOG
				$mail = new ZendMailLogger('COMMENT', $send_to->id);
				$mail->setBodyHtml ( $htmlBody, 'utf-8', 'utf-8' );
				$mail->setFrom ( $from_email, $from_name );
				$mail->addTo ( $send_to->email, $send_to->email );
				$mail->setSubject ( $subject );
				$mail->log();	
	
				$message = $comment->fb_name . ' has added a comment on a listing '
										. '#' . $listing->id . "\n"
										. APP_URL . '/listing/view/' . $listing->id . "\n\n"
										. ' (' . $listing->street . ', ' . $listing->city . ' $' . number_format($listing->price) . ').'
										. "\n\nThe text of the comment: " . $comment->text
										. "\n\nThe list of comments\n";
				
				foreach ( $listing->comments as $comment ) 
				{
					$message .= "{$comment->fb_name}: {$comment->text}\n";
				}
				
				mail("alen@alpha-male.tv, ".HELP_EMAIL, "New " . COMPANY_NAME . " comment", $message);
			}														
		}

		
		$this->renderNothing();	
		exit();
	}


	public function delete()
	{
		//You can only hide the comments from the MODERATOR VIEW on page


		$this->renderNothing();
		$comment_id	=   $this->getRequest()->getParam("id");

			$comment    =	Comment::find($comment_id);
			$comment->delete();

			$this->_redirect( "/admin/comments" );
	}

}
?>