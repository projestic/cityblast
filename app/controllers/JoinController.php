<?
class JoinController extends ApplicationController
{
	CONST MEMBER_FREE_CREDITS   =	1;
	CONST TAXES		=   "0.13"; // 13%
	CONST CURRENCY		=   "CAD";

	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		parent::init();
		$this->view->has_comments = 1;
	}

	public function index()
	{
		$this->landing();
	}

	public function savetime()
	{
		$this->landing();
	}

	public function savemoney()
	{
		$this->landing();
	}

	public function makemoney()
	{
		$this->landing();
	}

	public function vovo()
	{
		$this->landing();
	}

	protected function landing() 
	{
		
		$this->_helper->layout->setLayout('landing-responsive');
		$this->view->title = "Make More Money Now. Hire a CityBlast Social Media Assistant!";

		if ($this->_getParam('aff_id')) {
			//@todo add some hashing
			$referer_id			= $this->_getParam('aff_id');
			$referer_session		= new Zend_Session_Namespace("Referer");
			$referer_session->id	= $referer_id;

            // we also keep referer info in cookie
           	$_COOKIE['affiliate_id']    = $referer_id;
            $_COOKIE['cityblast_aff_id']= $referer_id;
		}

		$this->view->is_royal = 0;
	}


}
