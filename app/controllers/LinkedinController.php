<?php

class LinkedinController extends ApplicationController
{
	protected static $acl = array(
		'*' => '*'
	);

	public function init()
	{
		parent::init();
		$this->view->section = "linkedin";
		$this->session = new Zend_Session_Namespace('linkedin_oauth');
	}

	public function index()
	{
		$this->redirectIfNotLoggedIn();
		$this->renderNothing();
		
        Zend_Session::namespaceUnset('linkedin_oauth');
        
        $request = $this->getRequest();

		$this->session->return_url = '';
		if($request->getParam('settings')) {
			$this->session->return_url = '/member/settings/#settingtab';
		}

		$member = Member::find_by_uid($_SESSION['member']->uid);

		$consumer = new Blastit_Oauth_Consumer_LinkedIn();
		$token = $consumer->getRequestToken(array('scope' => 'rw_nus'));
		$this->session->request_token = serialize($token);
		$consumer->redirect();
	
	}
	
	public function callback()
	{
		$this->renderNothing();
		$member = Member::find_by_uid($_SESSION['member']->uid);
		
		$request = $this->getRequest();
		$params = $this->getRequest()->getParams();
	
		if(!isset($params['denied']) || empty($params['denied'])) {
			$consumer = new Blastit_Oauth_Consumer_LinkedIn();

			if ($request->isGet() && isset($this->session->request_token)) 
			{
				try {
					$token = $consumer->getAccessToken($params, unserialize($this->session->request_token));
				} catch(Exception $e) {
					$emailer = Zend_Registry::get('alertEmailer');
					$msg = 'LinkedIn auth callback failure: ' . "\n" . get_class($e) . ' [code: ' . $e->getCode() . '] ' . $e->getMessage();
					$emailer->send('LinkedIn Auth Failure', $msg, 'error-auth-linkedin', true);
				
					$this->addMessage('<h2 style="margin-bottom: 20px;">Something went wrong!</h2>There was a error while communicating with LinkedIn. Please try again.', 'colorbox');
					$this->redirect('/member/settings/#settingtab');
				}
				$this->session->access_token = serialize($token);
				
				// Update member account
				$member = Member::find_by_uid($_SESSION['member']->uid);
				$network_app = Zend_Registry::get('networkAppLinkedIn');
				$member->ln_application_id = $network_app->id;
				$member->linkedin_request_token = $this->session->request_token;
				$member->linkedin_access_token 	= $this->session->access_token;
				$member->linkedin_publish_flag = 1;
				$member->linkedin_fails = 0;
				$member->linkedin_auto_disabled	= 0;

				$ln_service = Blastit_Service_LinkedIn::newInstance($member);
				$ln_profile = $ln_service->getProfileData();
				$member->linkedin_id = !empty($ln_profile['id']) ? $ln_profile['id'] : null;
				
				//Blastit_Service_LinkedIn currently using JSON request with CamelCase keys in result
				if ($ln_profile && isset($ln_profile['numConnections'])) {
					$member->linkedin_friends_count = (int) $ln_profile['numConnections'];
				}
				
				$member->save();
			}
		}
	
		if($this->session->return_url) {
			$this->redirect($this->session->return_url);
		} else {
			$this->redirect('/');
		}
	}
}
