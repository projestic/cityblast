<?php

class AdministratorController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'superadmin'
	);
	
	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
		$this->view->admin_selectedCity    = $this->getSelectedAdminCity();
	}
	
	public function index()
	{
		$sql = "SELECT 
				m.id,
				m.type_id,
				m.first_name,
				m.last_name
			FROM member m 
			JOIN member_type ON m.type_id = member_type.id
			WHERE member_type.is_admin = 1";
		
		$this->view->admin_list = Member::find_by_sql($sql);	
		
	}	
	
	public function search() 
	{
	}

	public function list_results() 
	{
		$conditions = array();
		!empty($this->params['first_name']) ? $conditions[] = "first_name LIKE '%{$this->params['first_name']}%' " : null;
		!empty($this->params['last_name']) ? $conditions[] = " last_name LIKE '%{$this->params['last_name']}%' " : null;
		!empty($this->params['email']) ? $conditions[] =  " email LIKE '%{$this->params['email']}%' " : null;
		
		$this->view->results = Member::all(array(
		    				'select' => 'id, first_name, last_name, email, type_id',
		    				'conditions' => join(" OR ", $conditions)));
	}


	public function changetype()
	{
		$this->view->member = Member::find($this->params['id']);
	}

	public function save()
	{
		$params =	$this->getRequest()->getParams();

		if($this->_request->isPost())
		{
			$member = Member::find($params['member_id']);
			$member->applyType($params['type_id']);
			$member->save();
		}
		
		$this->_redirect('/administrator/index');
	}
	    		
	    		
	public function delete()
	{
		$this->renderNothing();
		$id =  $this->getRequest()->getParams('id');
		$admin = Administrator::find_by_id($id);
		$admin->delete();
		$this->_redirect('/administrator/index');
	}
	    			
	    		

}
?>