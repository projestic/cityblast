<?
class LpController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		$request = $this->getRequest();
		$cb_cid = $_SESSION['cb_cid']  = $request->getParam('cid');
		
		if ($cb_cid)
		{
			setcookie('cb_cid',$cb_cid,time()+ 10*365*24*60*60, '/');		
		}
		
		parent::init();
		
	}

	public function index()
	{
		$this->view->is_facebook = false;

		//SET THE FB AFF_ID
	}


	public function the_real_estate_talk_show(){}
	
	
	/**
	 * Landing page group action
	 * 
	 * Handler for landing pages which are sorted into named groups for tracking purposes.
	 * 
	 * This action sets the landing page group-name and page-name to cookies which are read
	 * when the user visits member/join, member/payment, member/thankyou. Each of those actions
	 * will modify the links they render to match the landing page signup URLs (for analytics tracking purposes).
	 * When a user registers the landing page group-name and page-name are stored.
	 */
	public function grouppage()
	{
		if(isset($_SERVER['HTTP_REFERER']))
		{
			if(stristr( strtolower($_SERVER['HTTP_REFERER']), 'facebook'))
			{
				setcookie('cityblast_aff_id',1183,time()+ 10*365*24*60*60, '/');	
				setcookie('affiliate_id',1183,time()+ 10*365*24*60*60, '/');					
			}
		}
		
		$group_name = $this->_getParam('group_name');
		

	
		
		$page_name = $this->_getParam('page_name');
		

		$regular_landing_page[] = "social-expert";
		$regular_landing_page[] = "social-media";		
		if(!in_array($group_name, $regular_landing_page))
		{
			$this->_helper->layout->setLayout('landing-responsive');
			//$this->_helper->layout->setLayout('wizard-responsive');
		}




		$no_header_footer[] = "results";
		$no_header_footer[] = "callus";
		if(!in_array($group_name, $no_header_footer))
		{
			$this->_helper->layout->setLayout('landing-no-header-footer');
		}


		$base_layout[] = "index";
		$base_layout[] = "short";
		if( $group_name == "join" && (in_array($page_name, $base_layout)) )
		{
			$this->_helper->layout->setLayout('layout');
		}

		if( $group_name == "social-media" && $page_name == "bootcamp" )
		{
			$this->_helper->layout->setLayout('landing-white');
		}

		

		$one_year_from_now = strtotime('+3 months');
		setcookie("landing_page_group_name", $this->_getParam('group_name'), $one_year_from_now, '/');
		setcookie("landing_page_name", $this->_getParam('page_name'), $one_year_from_now, '/');
		
		$this->view->member_join_url = $this->view->url(array(
				'group_name' => $this->_getParam('group_name'),
				'page_name' => $this->_getParam('page_name')
			), 
			'landing_page_group_join'
		);


		$this->view->group_name 	= "/lp/". $this->_getParam('group_name');
		$this->view->page_name 	= $this->_getParam('page_name');


		//Time needs to be 60 days...
//		if($this->view->group_name == "make-money" && $this->view->page_name == "video")
//		{
//			setcookie(APPLICATION_ENV.'_hasoffers_offer_id', 2,time()+ 10*365*24*60*60, '/');
//		}

		//KISS METRICS IDENTIFY USER
		/*****************
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			$page = 'Viewed Landing Page: ' . $this->_getParam('group_name') . '/' . $this->_getParam('page_name');
			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record($page);
		}***********/
		
		$config = Zend_Registry::get('appConfig');
		if (!empty($config['hasoffers']['default_offer_id'])) {
			setcookie('hasoffers_offer_id', $config['hasoffers']['default_offer_id'], time() + (60 * 60 * 24 * 60), '/');
		}
		
		$this->render($this->_getParam('group_name') . '/' . $this->_getParam('page_name'));
	}


}

?>
