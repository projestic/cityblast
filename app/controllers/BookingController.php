<?
class BookingController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'public'
	);

	public function error(){}

	public function index()
	{
		$params =	$this->getRequest()->getParams();

		//If there's no listing id, there was a problem, simply rediret to index
		if(!isset($params['listing_id']) || empty($params['listing_id']))
		{
			$this->_redirect("/");
		}
		else
		{
			$listing = Listing::find($params['listing_id']);


			if(isset($_COOKIE['cityblast_booking_agent_id']) && !empty($_COOKIE['cityblast_booking_agent_id']))
			{
				$booking_agent = Member::find_by_id($_COOKIE['cityblast_booking_agent_id']);
			}	
			elseif(isset($params['mid']) && !empty($params['mid']))
			{

				$booking_agent = Member::find_by_id($params['mid']);

				//Otherwise, grab the "booking agent"
				if(!$booking_agent)
				{
					$booking_agent = $listing->member;
				}
			}					
			elseif(isset($_SESSION['member']) && !empty($_SESSION['member']))
			{
				$booking_agent = $_SESSION['member'];
			}
			
			else
			{
				//If the "booking agent" is empty, use the listing agent as the contact
				$booking_agent = $listing->member;
			}
		}


		$this->view->listing = $listing;
		$this->view->booking_agent = $booking_agent;


		$this->view->request_type = $params['request_type'];

       	$this->_helper->layout->disableLayout();
        	//$this->_helper->viewRenderer->setNoRender(TRUE);
	}



	public function save()
	{
		$params =	$this->getRequest()->getParams();

		//Save the listing...
		$booking = new Booking();

		$booking->listing_id 	= $params['listing_id'];
		$booking->member_id 	= $params['mid'];

		$booking->name 		= $params['name'];

		$booking->email 		= $params['email'];
		$booking->phone 		= $params['phone'];
		$booking->contact_by 	= $params['contact_by'];
		$booking->best_time 	= $params['best_time'];
		$booking->message   	= $params['message'];
		$booking->type   		= $params['type'];

		$booking->save();

		$booking_agent = Member::find_by_id($params['mid']);
		$this->view->booking_agent	=	$booking_agent;
		$params['booking_agent'] = $booking_agent;

		$listing = Listing::find_by_id($params['listing_id']);
		$params['listing'] = $listing;

		//Member 146 == CityBlast Content
		if(isset($listing->member_id) && !empty($listing->member_id) && $listing->member_id != 146)
		{
			if(isset($listing->member->email_override) && !empty($listing->member->email_override)) $email = $listing->member->email_override;
			else $email = $listing->member->email;

			$params['listing_agent_name'] 	= $listing->member->first_name . " " . $listing->member->last_name;
			$params['listing_agent_email'] 	= $email;
			$params['listing_agent_brokerage'] = $listing->member->brokerage;
			$params['listing_agent_phone'] 	= $listing->member->phone;


		}
		else
		{
			$params['listing_agent_name'] 	= $listing->list_agent;
			$params['listing_agent_email'] 	= "";
			$params['listing_agent_brokerage'] = $listing->broker_of_record;
			$params['listing_agent_phone'] 	= $listing->list_agent_phone;
		}

		if(isset($booking_agent->email) && !empty($booking_agent->email))
		{
			//Send the BOOKING AGENT email here
			$from_name = COMPANY_WEBSITE;
			$from_email = HELP_EMAIL;


			$params['from_name'] = $from_name;



			$subject = 'Cha-Ching! You\'ve Got A New Lead from ' . COMPANY_NAME . ".";
			$htmlBody =   @$this->view->partial('email/booking.html.php', $params);


		    	$member_email = !empty($booking_agent->email_override) ? $booking_agent->email_override : $booking_agent->email;
			
			$mandrill = new Mandrill(array('REQUEST_INFO'));
			$mandrill->setFrom($from_name, $from_email);
			$mandrill->addTo($booking_agent->first_name . " " . $booking_agent->last_name, $member_email);
			$mandrill->send($subject, $htmlBody);


			//Keep track of the Email in our local LOG
			$mail = new ZendMailLogger('REQUEST_INFO', $this->id);
			$mail->setBodyHtml ( $htmlBody, 'utf-8', 'utf-8' );
			$mail->setFrom ( $from_email, $from_name );
			$mail->addTo ( $email, $email );
			$mail->setSubject ( $subject );
			$mail->log();	


			/***********
			$mail = new Zend_Mail();
			$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
			$mail->setFrom($from_email, $from_name);
			$mail->addTo($email);

			if(isset($from_name) && isset($from_email)) $mail->addHeader('Reply-To', $from_email, $from_name);


			$mail->setSubject('Cha-Ching! You\'ve Got A New Lead from ' . COMPANY_NAME . ".");
			$sentEmail = $mail->send();
			***********/




			// Requester email
			/**************************
			$htmlBody       =   @$this->view->partial('email/booking-confirmation.html.php', $params);

			$mailr = new Zend_Mail('utf-8');
              	$mailr->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
              	$mailr->setFrom($from_email, $from_name);
              	$mailr->addTo($params['email']);
			$mailr->setSubject('Your request for more information has been received!');
              	$sentEmail = $mailr->send();
			****************************/


			$subject  = 'Your request for more information has been received!';
			$htmlBody = @$this->view->partial('email/booking-confirmation.html.php', $params);

			$mandrill = new Mandrill(array('REQUEST_INFO_RECEIVED'));
			$mandrill->setFrom($from_name, $from_email);
			$mandrill->addTo("", $params['email']);
			$mandrill->send($subject, $htmlBody);


			//Keep track of the Email in our local LOG
			$mail = new ZendMailLogger('REQUEST_INFO_RECEIVED', $this->id);
			$mail->setBodyHtml ( $htmlBody, 'utf-8', 'utf-8' );
			$mail->setFrom ( $from_email, $from_name );
			$mail->addTo ( $params['email'], $params['email'] );
			$mail->setSubject ( $subject );
			$mail->log();	




			$msg = "Booking email ";
			$msg .= "sent by user: ". $params['name'];
			$msg .= " sent to: ".$booking_agent->first_name . " " . $booking_agent->last_name;
			$msg .= " for listing: ".$listing->id;


			mail("alen@alpha-male.tv, shaun@shaunnilsson.com, ".HELP_EMAIL, "Book Showing Email", $msg);
		}


		/*********************************
		if($booking_agent->id != $listing->member->id)
		{
			//Send the LISTING AGENT email here
			$from_name = "City-Blast.com";
			$from_email = "info@cityblast.com";


			$params['from_name'] = $from_name;


			$htmlBody	=   $this->view->partial('email/listing.html.php', $params);


			$mail = new Zend_Mail();
			$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
			$mail->setFrom($from_email, $from_name);
			$mail->addTo($params["email_friend"]);

			if(isset($from_name) && isset($from_email)) $mail->addHeader('Reply-To', $from_email, $from_name);


			$mail->setSubject('Request for a Home Showing');
			//$sentEmail = $mail->send();
		} ************************************/

		//exit();

	}

	public function savebox()
	{
		$params =	$this->getRequest()->getParams();
		$listing = $this->view->listing = Listing::find_by_id($params['listing_id']);



		$error	=   false;
		if (empty($params['name']))
		{
		    $this->_helper->layout->disableLayout();

		    $this->view->booking_agent = Member::find_by_id($params['mid']);

		    $this->view->error	=   true;
		    $this->render('error');
		    return;
		}

		//Save the listing...
		$booking = new Booking();

		$booking->listing_id 	= $params['listing_id'];
		$booking->member_id 	= $params['mid'];

		$booking->name 		= $params['name'];
		$booking->city_id 		= $listing->city_id;

		$booking->email 		= $params['email'];
		//$booking->phone 		= $params['phone'];
		//$booking->contact_by 	= $params['contact_by'];
		//$booking->best_time 	= $params['best_time'];
		$booking->message   	= $params['message'];

		$booking->save();



		$params['booking_agent'] = $booking_agent = $this->view->booking_agent = Member::find_by_id($params['mid']);

		$params['listing'] = $listing = Listing::find_by_id($params['listing_id']);



		if(isset($booking_agent->email) && !empty($booking_agent->email) )
		{
			//Send the BOOKING AGENT email here
			$from_name = COMPANY_WEBSITE;
			$from_email = HELP_EMAIL;


			$params['from_name'] = $from_name;


			//Member 146 == CityBlast Content
			if(isset($booking_agent->member_id) && !empty($booking_agent->member_id) && $booking_agent->member_id != 146)
			{
				if(isset($listing->member->email_override) && !empty($listing->member->email_override)) $email = $listing->member->email_override;
				else $email = $listing->member->email;

				$params['listing_agent_name'] 	= $listing->member->first_name . " " . $listing->member->last_name;
				$params['listing_agent_email'] 	= $email;
				$params['listing_agent_brokerage'] = $listing->member->brokerage;
				$params['listing_agent_phone'] 	= $listing->member->phone;


			}
			else
			{
				//echo "<pre>";
				//print_r($listing);
				
				$params['listing_agent_name'] 	= $listing->list_agent;
				$params['listing_agent_email'] 	= $listing->member->email;
				$params['listing_agent_brokerage'] = $listing->broker_of_record;
				$params['listing_agent_phone'] 	= $listing->list_agent_phone;
			}



			if(empty($params['listing_agent_email'])) $params['listing_agent_email'] = "info@cityblast.com";
			

			$htmlBody =   @$this->view->partial('email/booking.html.php', $params);
			$subject = "Cha-Ching! You've Got A New Lead from " . COMPANY_NAME . ".";
			
			if(empty($params['listing_agent_email'])) $subject .= " UNAFFILIATED LEAD";
			if($listing->member_id == 146 && $listing->blast_type == "IDX") $subject .= " UNAFFILIATED LEAD";
			
			
			$mandrill = new Mandrill(array('BOOKING_NOTIFICATION'));
			$mandrill->setFrom($from_name, $from_email);
			$mandrill->addTo($booking_agent->email, $booking_agent->email);
			$mandrill->send($subject, $htmlBody);
			
			//Keep track of the Email in our local LOG
			$mail = new ZendMailLogger('BOOKING_NOTIFICATION', $booking_agent->id);
			$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
			$mail->setFrom($from_email, $from_name);
			$mail->addTo($booking_agent->email, $booking_agent->email);
			$mail->setSubject($subject);
			$mail->log();

			if( filter_var($params["email"], FILTER_VALIDATE_EMAIL) !== FALSE )
			{
			    // Requester email
			    $htmlBody = @$this->view->partial('email/booking-confirmation.html.php', $params);
			    $subject = 'Your request for a home showing has been received!';

				$mandrill = new Mandrill(array('BOOKING_CONFIRMATION'));
				$mandrill->setFrom($from_name, $from_email);
				$mandrill->addTo($params["email"], $params["email"]);
				$mandrill->send($subject, $htmlBody);
			}


			$msg = "Booking email ";
			$msg .= "sent by user: ". $params['name'];
			$msg .= " sent to: ".$booking_agent->first_name . " " . $booking_agent->last_name;
			$msg .= " for listing: ".$listing->id;


			mail("alen@alpha-male.tv, shaun@shaunnilsson.com, ".HELP_EMAIL, "Book Showing Email", $msg);
		}




		//exit();

		$this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(TRUE);
	}

	public function xhrphonecalllog() {

		// Throws 404 not exists exception if it is not AJAX request
		if (!$this->getRequest()->isXmlHttpRequest()) {
			throw new Zend_Controller_Action_Exception('Page not found', 404);
		}

		try {
			$listingId = $this->getRequest()->getPost('listing_id');
			$bookingAgentId = $this->getRequest()->getPost('booking_agent_id');

			// Log phone call interaction
			Interaction::entry($listingId, $bookingAgentId, 'phone_call');
		} catch (Exception $e) {
			throw new Zend_Controller_Action_Exception('Phone call interaction log error', 500);
		}

		$this->renderNothing();
	}

}
?>
