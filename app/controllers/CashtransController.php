<?php

class CashtransController extends ApplicationController
{
	
	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
	}
	
	public function index()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}

		$request    =  $this->getRequest();
		$member_id  =  $request->getParam('id');
		
		if($member_id)
		{
			$this->view->member = Member::find($member_id);
		}
		
		if($member_id)
		{
			$conditions = array(
				"select" => "cash_trans.*, 
					m.first_name as mfirst, 
					m.last_name as mlast, 
					mc.first_name as mcfirst, 
					mc.last_name as mclast",
				"conditions" => "cash_trans.member_id = " . addslashes($member_id),
				"joins" => "LEFT JOIN member m ON m.id = cash_trans.member_id
					LEFT JOIN member mc ON mc.id = cash_trans.created_by",
				"order" => "id DESC");

			$paginator = new Zend_Paginator(new ARPaginator('CashTrans', $conditions));
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;
		}

	}

	public function add()
	{
		$params = $this->getRequest()->getParams();
		$params = array_map('trim', $params);
		
		$error_messages = array();
		
		if($this->_request->isPost()) {
			
			if (empty($params['member_id'])) {
				$error_messages[] = 'Memeber not specified';
			}
			
			if (empty($params['amount'])) {
				$error_messages[] = 'You must specify an amount';
			} elseif ($params['amount'] < 0) {
				$error_messages[] = 'Amount can not be negative';
			}
			
			if (empty($params['note'])) {
				$error_messages[] = 'You must provide a note';
			}

			if (empty($error_messages)) {
				if ($params['type'] == CashTrans::TYPE_CREDIT) {
					$cash_trans = CashTrans::newCredit(
						$params['member_id'], 
						$params['amount'], 
						$params['note'], 
						$_SESSION['member']->id
					);
				} elseif ($params['type'] == CashTrans::TYPE_DEBIT) {
					$cash_trans = CashTrans::newDebit(
						$params['member_id'], 
						$params['amount'], 
						$params['note'], 
						$_SESSION['member']->id
					);
				}
				
				$this->_redirect("/admin/member/{$params['member_id']}#cash");
			}
		} 
		
		$this->view->error_messages = $error_messages;
		
		$member_id  =  $this->getRequest()->getParam('id');
		
		if($member_id)
		{
			$this->view->member = Member::find($member_id);
		}
	}   			
}