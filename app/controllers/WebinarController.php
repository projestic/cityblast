<?

class WebinarController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		parent::init();
	}

	public function index() 
	{
		setcookie('affiliate_id', 999999, time() + (60 * 60 * 24 * 365), '/');
		
		$this->_helper->layout->setLayout('landing-no-header-footer');


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed Webinar Lander');
		}	

	}


		
	
	public function watch() 
	{
		setcookie('affiliate_id', 999999, time() + (60 * 60 * 24 * 365), '/');
		
		$this->_helper->layout->setLayout('landing-no-header-footer');



		if ($this->_request->isPost())
		{
			$trial_member = new TrialMember();
			
			$trial_member->email = $this->_request->getParam("email");
			$trial_member->save();

		}

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed Webinar Video Page');
		}	

	}	
	
	
	/****************
	
		Need an AJAX call here that triggers a KissMetrics record event if/when the video player is clicked
	
	*****************/
}

?>