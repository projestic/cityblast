<?
class IndexController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'public'
	);

	public function index()
	{
		$this->view->footerType = 'fancy';
		
		$city_id    =	$this->getSelectedCity();


		$conditions['select'] = "distinct(listing_id)";

		$conditions['conditions'] = '';
		if($city_id != "*")
		{
			$city = City::find($city_id);


			if($city->blasting != "ON")
			{
				//This is a city that doesn't support BLASTING, pick Canada or the US
				$city = City::find_by_name($city->country);

				$conditions['conditions'] .= "city_id='" . intval($city->id) . "' AND";
			}
			elseif($city_id > 0)
			{
			    	$cities =	new City();
			    	$this->view->selectedCityObject =	$cities->find($city_id);

			    	$conditions['conditions'] .= "city_id='" . intval($city_id) . "' AND";
			}
		}
		else
		{
			$canada = City::find_by_name("Canada");
			$usa = City::find_by_name("United States");

			$conditions['conditions'] .= "city_id='" . $canada->id . "' OR city_id='".$usa->id."' AND";
		}
		
		$conditions['conditions'] .= " (`duplicate` IS NULL OR `duplicate` = 'NONE')";

		$conditions['limit'] = 5;
		$conditions['order'] = "id DESC";


		$pub_queue = PublishingQueue::find('all', $conditions);



		if($pub_queue)
		{
			foreach($pub_queue as $queue)
			{
				if ($queue->listing) {
					$listings[] = $queue->listing;
				}
			}

			$this->view->last_5 = $listings;
		}
		else
		{
			$this->view->last_5 = null;
		}

		if(isset($_SESSION['message']) && !empty($_SESSION['message'])) $this->view->message	= $_SESSION['message'];

		$_SESSION['next'] = '';

		if(!empty($_SESSION['NEEDS_LOGIN']) && $_SESSION['NEEDS_LOGIN']==true)
		    $this->view->triggerLogin	    =	true;


		$this->auto_complete_cities();
	}

	public function api()
	{
		$this->view->footerType = 'fancy';	
	}

	public function content()
	{
		//$this->view->footerType = 'fancy';
		$this->view->stats = LifetimeStats::find(1);	
	}

	public function ambassador()
	{
		//$this->view->footerType = 'fancy';	
	}		

	public function training()
	{
		//$this->view->footerType = 'fancy';	
	}		
	
	public function press()
	{
		$this->view->footerType = 'fancy';	
	}		

	public function idx()
	{
		$this->view->footerType = 'fancy';	
	}		

	public function partner(){}		
		

	public function whyus()
	{		
		//$this->view->addScriptPath(APPLICATION_PATH.'/views/cityblast/scripts');
		
		$this->view->footerType = 'fancy';
		$this->view->has_comments = 1;		
		$this->view->is_facebook = false;	
		
		$this->redirectIfNoView('/');
	}

	public function whymortgagemingler()
	{
		$this->_redirect("/index/why-us");	
	}
	
	public function whycityblast()
	{
		$this->_redirect("/index/why-us");	
	}

 	public function feedback_submit()
 	{
 		mail(HELP_EMAIL,"Feedback",$_POST['feedback_comments']);
 	}

 	public function pricing()
 	{
 	
 		// view also used by /member/broker
 		
		$member_type = MemberType::find_by_code('AGENT');
		
		$signupSession  = new Zend_Session_Namespace('signup');
		$signupSession->member_type = "AGENT";

		// member is not logged in
		// - assume they have not registered yet and set the billing cycle for signup process
		$signup_urls = array(
			'month' => '/member/set_signup_billing_cycle/billing_cycle/month',
			'biannual' => '/member/set_signup_billing_cycle/billing_cycle/biannual',
			'annual' => '/member/set_signup_billing_cycle/billing_cycle/annual'
		);

		$this->view->member_type = $member_type;
		$this->view->signup_urls = $signup_urls;
	}

 	public function contact_us(){}


 	public function our_story()
 	{
		$this->view->footerType = 'fancy';
		$this->view->addScriptPath(APPLICATION_PATH.'/views/cityblast/scripts');
		 		
		$this->view->stats = LifetimeStats::find(1);
 	}

	public function faq()
	{
		$this->view->footerType = 'fancy';
		$this->view->current_url = $this->getRequest()->getRequestUri();
	}

	public function faqs()
	{
		$this->redirect("/index/faq");
	}

	public function fanpage(){}

	public function send_referral()
	{
		$params =	$this->_getAllParams();
		$this->view->to_email	=   $params["email_friend"];

		$emails = explode(',', str_replace(';', ',', $params["email_friend"]));

		$this->view->member = null;
		if(isset($_SESSION['member'])) $this->view->member = $_SESSION['member'];

		if (empty($emails[0]))
		{
			$this->redirect('/index/refer-an-agent');
		}
		
		$cleanEmails = array();
		foreach($emails as $email)
		{
			$email = preg_match('/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i', $email, $matches);
			!empty($matches[0]) ? $cleanEmails[] = $matches[0] : '';
		
		}

		if (empty($cleanEmails))
		{
			$this->redirect('/index/refer-an-agent');
		}

		foreach ($cleanEmails as $email)
		{
			$mandrill = new Mandrill(array('REFERRAL'));
			$mandrill->addTo("", $email);

			if(isset($_SESSION['member']))
			{
				$from_name = $_SESSION['member']->first_name . " " . $_SESSION['member']->last_name;
				$member_id = $_SESSION['member']->id;

				$subject = 'A Personal Invitation from '.$from_name.' to Join '. COMPANY_NAME .'. 100% Free. No Fees. No Ads. No Gimmicks.';
			}
			else
			{
				$subject = 'An Invitation to Join '. COMPANY_NAME .'. 100% Free. No Fees. No Ads. No Gimmicks.';
				$from_name = "Your friends at " . COMPANY_WEBSITE;
				$member_id = null;
			}

			if(isset($_SESSION['member'])) $from_email = $_SESSION['member']->email;
			else $from_email = HELP_EMAIL;

			$params['from_name'] = $from_name;
			$params['member_id'] = $member_id;
			$params['member'] 	 = $this->view->member;
			$params['total_count'] 	= $this->view->total_count;

			$htmlBody	=   $this->view->partial('email/referral.html.php', $params);
			
			$mandrill->setFrom($from_name, $from_email);
			$mandrill->send($subject, $htmlBody);

			$msg = "Referral email ";
			if(isset($_SESSION['member']->email))
				$msg .= "sent by member: ". $_SESSION['member']->email;
			if(isset($email))
			    $msg .= " sent to: ".$email;




			//DO NOT REMOVE THIS CODE!!!!!!
			
			//Keep track of the Email in our local LOG
			$mail = new ZendMailLogger('REFERRAL', $_SESSION['member']->id);
			$mail->setBodyHtml( $htmlBody, 'utf-8', 'utf-8');
			$mail->setFrom($_SESSION['member']->email, $_SESSION['member']->email);
			$mail->addTo($email, $email);
			$mail->setSubject($subject);
			$mail->log();


			mail("alen@alpha-male.tv", "Referral EMAIL Sent", $msg);
			mail("info+referral@cityblast.com", "Referral EMAIL Sent", $msg);
		}

		$member  = $_SESSION['member'];
		$member->referral_email_count++;
		$member->save();
		
		if(!empty($params['redirect_user_back']))
		{
			$this->view->return_link	=	$params['redirect_user_back'];
		}
	}




	public function send_fb_referral()
	{
		$member = Member::find($_SESSION['member']->id);

		$facebook = $member->network_app_facebook->getFacebookAPI();

		$publish_array = null;
		
		if(stristr(APPLICATION_ENV, "myeventvoice"))
		{
			$publish_array['message'] 	= "Fellow wedding and event planners – check out this awesome service I discovered that helps you with your social media marketing. It’s great!";
			$publish_array['picture'] 	= APP_URL . "/images/myeventvoice/refer_preview_thumb.jpg";
		}		
		elseif(stristr(APPLICATION_ENV, "cityblast"))
		{
			$publish_array['message'] 	= "Fellow real estate agents and brokers – check out this awesome service I discovered that helps you with your social media marketing. It’s great!";
			$publish_array['picture'] 	= APP_URL . "/images/refer_preview_thumb.jpg";
		}
		else
		{
			$publish_array['message'] 	= "Fellow mortgage professionals – check out this awesome service I discovered that helps you with your social media marketing. It’s great!";
			$publish_array['picture'] 	= APP_URL . "/images/refer_preview_thumb.jpg";
		}
		
				
		$publish_array['link'] 		= APP_URL ."/a/i/".$member->id;
		$publish_array['name'] 		= COMPANY_NAME.": Hire Your Social Expert Now!";
		$publish_array['description'] = COMPANY_NAME." is now offering a 14-Day FREE Trial. Check out what our virtual social experts can do for you!";

		$publish_array['access_token'] = $member->network_app_facebook->getFacebookAppAccessToken();

		try 
		{
			$fb_result = $facebook->api("/".$member->uid."/feed", "post", $publish_array);
			
		} catch(Exception $e) {

			echo "\t\t"."ERROR: ".$e->getMessage()."\n";
			exit();
		
		}

		$msg = 'Referral Facebook Blast ';
		if($member->email)
		{
			$msg .= 'sent by member: ' . $member->email;
		}
		$emailer = Zend_Registry::get('alertEmailer');
		$emailer->send('Referral BLAST Sent', $msg, 'referral-post');
		
		$member->referral_blast_count++;
		$member->save();
		
		//$this->_render("send_referral.html.php");
		$this->render("send_referral");
		
		
		//echo "<h1>HELLO?</h1>";
		//exit();
	}




 	public function contact_us_send()
 	{
 		$message = "You received an email from: " .$_POST['email'];
 		$message .= "\n\n".$_POST['message'];

 		mail(HELP_EMAIL,$_POST['subject'], $message);
 	}


	public function create_your_message(){}

	public function how_blasting_works()
	{
		$this->view->has_comments = 1;
				
		$params =	$this->_getAllParams();

		if(isset($params['id'])) $this->view->listing_id = $params['id'];
		else $this->view->listing_id = null;
	}

	public function blast_our_network(){}

	public function sell_your_listing_fast(){}

  	public function refer_an_agent() 
  	{

		if(!isset($_SESSION['member']) || empty($_SESSION['member']))
		{
			//After you sign-up or log in, redirect to the listing details
			$_SESSION['REDIRECT_AFTER_LOGIN'] = $_SESSION['next'] = "/index/refer-an-agent";

			// instead of redirect we are showing popup with colorbox
			//$this->_redirect("/member/authenticate");
			$this->view->popup	= 1;
		}

	    if($this->_request->isPost()) 
	    {
			$email	=   $this->getRequest()->getParam("email_friend");
			
			if( FALSE === filter_var($email, FILTER_VALIDATE_EMAIL) ) 
			{
				$this->view->invalidEmail	=   true;
			}
	    }

	}

  	public function feedback(){}

  	public function about(){}

  	public function privacy()
  	{
  		$this->view->footerType = 'fancy';	
  	}

  	public function guarantee()
  	{
		$this->_redirect("/index/why-us");
	}

  	public function terms()
  	{
  		$this->view->footerType = 'fancy';	
  	}

  	public function blast(){}

  	public function cityblast()
  	{
  		$this->view->has_comments = 1;	
  	}



	public function error_test()
	{
		#throw new Exception('Just a test error, nothing to see here');
		$this->renderPartial('error/404.html.php');
	}

	public function sitemap()
	{
		$this->view->active_listing_cities = City::find_by_sql("
			SELECT 
				c.id,
				c.name, 
				c.state,
				c.country
			FROM city c
			INNER JOIN listing l ON l.city_id = c.id 
			AND l.blast_type = '".Listing::TYPE_LISTING."' 
			AND l.status = 'active' 
			INNER JOIN publishing_queue pq ON pq.listing_id = l.id
			WHERE c.country IS NOT NULL
			GROUP BY c.name
		");
	}
}

?>
