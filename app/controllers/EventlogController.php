 <?php

class EventlogController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function init()
	{		
		//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');		

		parent::init();

	}
	
	public function membertab()
	{
		if ($this->_request->isXmlHttpRequest())
		{
			//Disable the layout
			$this->_helper->layout()->disableLayout();
		}
		else
		{
			$this->_helper->layout->setLayout('admin');
		}

		$request    =  $this->getRequest();
		$member_id  =  $request->getParam('id');
		$member     = Member::find($member_id);
		
		if(!empty($member->id))
		{
			$this->view->member = $member;
			
			$conditions = array(
				"select" => "event_log.*, 
					ma.first_name AS afirst, 
					ma.last_name AS alast",
				"conditions" => array("event_log.entity_member_id = ?", $member->id),
				"joins" => "LEFT JOIN member ma ON ma.id = event_log.actor_member_id",
				"order" => "event_log.id DESC");

			$paginator = new Zend_Paginator(new ARPaginator('EventLog', $conditions));
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
			$this->view->paginator = $paginator;
		}

	}
}