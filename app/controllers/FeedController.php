<?php

class FeedController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'public'
	);

	public function index()
	{
		$this->view->addScriptPath(APPLICATION_PATH.'/views/common/scripts');	
		
		/**********************
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$this->_helper->layout->disableLayout();
		
		
		if(APPLICATION_ENV == "alen")
		{
			$listings		=	Listing::all();
			$this->view->listings	= $listings;
		}
		else
		{
			//Grab the last 24hrs of listings
			$yesterday = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d")-1,date("Y")));
			$conditions	=   array('approval=1 AND approved_at>=?',$yesterday);
			$listings = Listing::find('all', array('conditions' => $conditions));
		}
		
		$this->publishfeed($listings);
		************************/

	}		

	private function publishfeed($listings)
	{
				
	    //Create an array for our feed
	    $feed = array();

	    //Setup some info about our feed
	    $feed['title']          = COMPANY_WEBSITE;
	    $feed['description']    = COMPANY_NAME . "sIs How The World's Realtors Unleash the Massive Marketing Power of Their Whole City's Social Media Network. Sell it fast. " . COMPANY_NAME;
	    $feed['link']           = APP_URL.'/rss/feed.xml';
	    $feed['charset']    = 'utf-8';
	    $feed['language']   = 'en-us';
	    $feed['published']  = time();
	    $feed['modified']  = time();
	    $feed['entries']    = array();

	    foreach($listings as $listing)
	    {
			$entry = array();
			$entry['guid']	    = APP_URL."/listing/view/".$listing->id;
			$entry['title']     = "BlastID ".$listing->id;
			$entry['link']      = APP_URL."/listing/view/".$listing->id;
			$entry['description']   = $listing->message;
			$entry['lastUpdate']    = strtotime($listing->created_at->format("Y-m-d H:i:s"));

			$feed['entries'][]  = $entry;
	    }
	
	    $feedObj = Zend_Feed::importArray($feed, 'rss');

	    $feedObj->send();
	}

}

?>
