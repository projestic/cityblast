<?
class PublishingqueueController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
	}
	
	public function update()
	{
		$this->view->cities = City::find("all",array("order"=>"name"));
	   	$this->view->publish = PublishingQueue::find_by_id($this->getRequest()->getParam('id'));
	   	
	   	if($this->view->publish->listing->id){
	   		$listing_id = $this->view->publish->listing->id;
	   		$query = "select count(*) as total_clicks from click_stat where listing_id=$listing_id";
	   		$this->view->click_stat = PublishingQueue::find_by_sql($query);
	   		$this->view->click_stat = $this->view->click_stat[0];
	   	}
	   	
	}

	public function save()
	{
		$id	=   $this->_request->getParam('id');
	    		
	    	if($this->_request->isPost())
	    	{
	    		if(!empty($id) && $id != 0)
	    		{	    		
	    			$publish = PublishingQueue::find($id);
	    			
		    		$publish->priority 				= $this->_request->getParam('priority');
		    		$publish->min_inventory_threshold 	= $this->_request->getParam('min_inventory_threshold');
		    		$publish->max_inventory_threshold 	= $this->_request->getParam('max_inventory_threshold');
		    		$publish->inventory_id 			= $this->_request->getParam('inventory_id');
		    		$publish->hopper_id 			= $this->_request->getParam('hopper_id');
		    		$publish->city_id 				= $this->_request->getParam('city_id');
	    			    			    			    			    			    		
	    			$publish->save();
	    		}
	    		
	    		$this->_redirect("/admin/index/".$publish->inventory_id);
	    		
	    	}
	    		
	}

	public function retire()
	{
		$retireArr = array();
		
		if($this->getRequest()->getParam('id'))
		{
			$retireArr[] = $this->getRequest()->getParam('id'); 
		}
		
		if($this->getRequest()->getParam('retire'))
		{
			$retireArr = $this->getRequest()->getParam('retire'); 
		}
		
		
		foreach($retireArr as $pid)
		{
			$publish = PublishingQueue::find_by_id($pid);
			//$return_url = "/admin/index/".$publish->inventory_id;
	
			//echo "Retiring BLAST: " . $publish->id . " LISTING: " . $publish->listing_id ."\n\n";	
	

			$listing	= Listing::find($publish->listing_id);
			$listing->published = $listing->published + 1;
			$listing->published_on = date("Y-m-d H:i:s");
			$listing->save();
			
			$publish->delete();

		
		}
	
		$this->_redirect("/admin/index/");						
	}

	public function delete()
	{
		$publish = PublishingQueue::find_by_id($this->getRequest()->getParam('id'));
		$return_url = "/admin/index/".$publish->inventory_id;

		$message = new Zend_Session_Namespace('messages');
		if (!isset($message->data)) {
			$message->data = new stdClass();
		}
		$message->data->msg   =	"Publishing Item '".$publish->id."' was deleted!";
		
		$publish->delete();
		
		$this->_redirect($return_url);
	}

}
?>
