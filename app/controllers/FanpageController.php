<?php

class FanpageController extends ApplicationController
{
	protected static $acl = array(
		'testblast' => 'superadmin',
		'contentfill' => 'superadmin',
		'*' => 'public'
	);	
	
	public function test()
	{
		$this->_helper->layout->setLayout('layout');
	}
	
	public function index()
	{
		//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');
				
		$conditions = array('conditions' => "1=1",  'order' => 'id DESC');
		$paginator = new Zend_Paginator(new ARPaginator('FanPage', $conditions));
		$paginator->setItemCountPerPage(30);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		
		$this->view->fanpages = $paginator;	
	}	
	
	public function contentfill()
	{

		//Set the LAYOUT
		$this->_helper->layout->setLayout('admin');
		
		$request = $this->getRequest();
		
		$this->view->member = $member = Member::find($request->getParam('member_id'));
		$this->view->fanpage = $fanpage = FanPage::find($request->getParam('fanpage_id'));
		
		

		if ($request->isPost() )
		{
			$params = $request->getParams();
			//echo "<pre>";
			//print_r($params);
			
			
			$query = "select distinct listing.id from publishing_queue join listing on publishing_queue.listing_id=listing.id ";
			if(isset($params['content_type']) && $params['content_type'] == "BIG_PHOTO") $query .= " where listing.big_image = 1 ";
			elseif(isset($params['content_type']) && $params['content_type'] == "SMALL_PHOTO") $query .= " where listing.big_image = 0 ";
			
			$query .= "ORDER BY RAND() ";
			$query .= "limit ".$params['number_of_posts'];
			
			//echo $query . "<BR>";
			
			$listing_array = FanPage::find_by_sql($query);


			$facebook = $member->network_app_facebook->getFacebookAPI();

			
			foreach($listing_array as $listing_detail)
			{
				//print_r($listing_detail);	
				$listing = Listing::find($listing_detail->id);
				
				//print_r($listing);

				//Source is fp = fanpage
				$publish_array = $listing->getFacebookPayload($member, "fp");
				
				$result = $member->publishDirectToFanpage($facebook, $fanpage, $listing, $publish_array);
				
				print_r($result);
			}
			
			
			echo "FINISHED!<BR><BR>";
			exit();
			
		}
				
	}
	
	public function update()
	{
 		$request    	= $this->getRequest();		
		$member_id	= $request->getParam('member_id');	
		$fanpage_id  	= $request->getParam('id');
	

		$this->view->member = Member::find_by_id($member_id);

		
		$this->view->fanpage = null;
		if(isset($fanpage_id) && !empty($fanpage_id))
		{
			$this->view->fanpage = FanPage::find_by_id($fanpage_id);		
		}
	}


	public function delete()
	{
		$request    	=   $this->getRequest();
		$fanpage_id  	= $request->getParam('fanpage_id');
		

		$fanpage = FanPage::find($fanpage_id);
		
		if($fanpage)
		{
			$fanpage->active = "no";
			$fanpage->save();	
			
			$this->view->notification = "Fanpage deleted successfully";
		}
	
		$this->_redirect("/fanpage/index");
	}


	public function testblast()
	{
			
		$request    	=   $this->getRequest();
		$member_id  	= $request->getParam('member_id');
		$fanpage_id  	= $request->getParam('fanpage_id');

		$member = Member::find($member_id);
		$fanpage = FanPage::find($fanpage_id);
		
		if ($publish_queue = $member->find_blast()) {
			$member->publishFacebookFanpage($publish_queue, true);
			echo "Finished.<BR>";
		} else {
			echo "Couldn't find anything to publish.";
		}

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
	}

	public function on()
	{

		$member 		= $_SESSION['member'];
		$request    	= $this->getRequest();
		$fanpage_id  	= $request->getParam('fanpage_id');
		
		//Set up the FACEBOOK VARIABLES...
		$publish_array['access_token'] = $member->access_token;

		$api_url		= "https://graph.facebook.com/me/accounts?access_token=" . $member->access_token;
		
		$accounts = null;
		if(file_get_contents($api_url))
		{
			$accounts = json_decode(file_get_contents($api_url));

			if(!empty($accounts))
			{
				//get the access token to post to your page via the graph api
				foreach ($accounts->data as $account)
				{

					if($account->category != "Application")
					{
						$api_url = "https://graph.facebook.com/" . $account->id . "/?access_token=" . $account->access_token;
					
						try {
							$page_info 	= json_decode(file_get_contents($api_url));		
						} catch (Exception $e) {
							// Facebook may have returned an error on this page (due to Facebook bug present as of 2013-04-17). Continue processing the rest of the account's pages.
							continue;
						}

						if(!$fanpage = FanPage::find_by_member_id_and_fanpage_id($member->id, $page_info->id))
						{
							$fanpage = new FanPage();		
						}
						
						$state = ($page_info->id == $fanpage_id) ? 'yes':'no';
						$fanpage->active		= $state;
						$fanpage->member_id 	= $member->id;
						$fanpage->fanpage_id 	= $page_info->id;
						$fanpage->fanpage		= $page_info->name;
						$fanpage->access_token 	= $account->access_token;
						$fanpage->likes 		= $page_info->likes;
						$fanpage->save();		
						
					}
				}
			}
		}	
		
		$reopenfanpage = new Zend_Session_Namespace('reopenfanpage');
		if (!isset($reopenfanpage->data)) {
			$reopenfanpage->data = new stdClass();
		}
		$reopenfanpage->data->condition = 'yes';

		$this->_redirect("/member/settings#settingtab");

	}
	
	public function off()
	{

		$member 		= $_SESSION['member'];
		$request    	= $this->getRequest();
		$fanpage_id  	= $request->getParam('fanpage_id');
		
		$fanpage = FanPage::find_by_member_id_and_fanpage_id($member->id, $fanpage_id);
		if(!$fanpage) $fanpage = new FanPage();
		
		//Set up the FACEBOOK VARIABLES...
		$publish_array['access_token'] = $member->access_token;

		$api_url = "https://graph.facebook.com/me/accounts?access_token=" . $member->access_token;
		
		$accounts = null;
		if(file_get_contents($api_url))
		{
			$accounts = json_decode(file_get_contents($api_url));

			if(!empty($accounts))
			{
				//get the access token to post to your page via the graph api
				foreach ($accounts->data as $account)
				{
				    $page_id = $account->id;
				    
					//found the access token, now we can break out of the loop
					$page_access_token = $account->access_token;
					
					$api_url	= "https://graph.facebook.com/".$page_id."/?access_token=" . $page_access_token;
					$page_info 	= json_decode(file_get_contents($api_url));		
			
					if ($account->category != "Application" && $page_info->id == $fanpage_id) {
				
						$fanpage->member_id 	= $member->id;
						$fanpage->fanpage_id 	= $fanpage_id;
						$fanpage->fanpage		= $page_info->name;
						$fanpage->access_token 	= $account->access_token;
						$fanpage->likes 		= $page_info->likes;
						$fanpage->active		= "no";							
						$fanpage->save();

					}
				}
			}
		}	
		
		$this->_redirect("/member/settings#settingtab");		
	}

	
	public function save()
	{

		$request    	= $this->getRequest();
		$member_id		= $request->getParam('member_id');	
		$id  			= $request->getParam('id');
		$fanpage		= $request->getParam('fanpage');	
		$fanpage_id  	= $request->getParam('fanpage_id');
		$facebook 		= $member->network_app_facebook->getFacebookAPI();
		$member 		= Member::find($member_id);

		$facebook 		= $member->network_app_facebook->getFacebookAPI();

		if (file_get_contents("http://graph.facebook.com/".$fanpage_id))
		{	
				
			$fp_user = json_decode(file_get_contents($graph_fanpage));					
			
			if (!$fp_user) {
				$likes = 0;	
			} else {
				$likes = $fp_user->likes;
			}
									
			if(!isset($id) || empty($id))
			{
				$fp = new FanPage();
			}
			else
			{
				$fp = FanPage::find($id);
			}
			
			$fp->member_id 		= $member_id;
			$fp->fanpage 		= $fanpage;
			$fp->fanpage_id 	= $fanpage_id;
			$fp->active 		= $request->getParam('active');
			$fp->likes 			= $likes;
			$fp->save();
		}

		
		$this->_redirect("/admin/member/".$member_id);
	}
}	