<?php

class ErrorController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'public'
	);

	public function test()
	{
		$error_type = 404;
		
		if($error_type == 404)
		{
			$this->render("404");
		}
		else
		{
			$this->render("500");
		}
	}
	
			
	public function error()
	{
		$errors = $this->_getParam('error_handler');
		$this->sql = null;
		$this->sql_args = null;
	
		switch ($errors->type) 
		{ 
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
    
    			$error_type = 404;
    				
          		// 404 error -- controller or action not found
          		$this->getResponse()->setHttpResponseCode(404);
          		$this->view->message = 'Page not found';
          	break;
        
        		default:

				$error_type = 500;

				$this->airbrake($errors);

	          	// application error 
	          	$this->getResponse()->setHttpResponseCode(500);
	          	$this->view->message = 'Application error';
	          	break;
      	}
      
      	$this->view->exception = $errors->exception;
      	$this->view->request   = $errors->request;

      
      	//error("Exception for request => " . print_r($_SERVER, true));
      	//error($errors->exception);



       	/***********
		if(isset($errors->exception))
		{
			echo get_class($errors->exception) . ": " . $errors->exception->getMessage() . "\n";
		}

    
		$trace = $errors->exception->getTrace();
		foreach($trace as $line)
		{
			//echo $line['file']  . ":" . $line['line'] . " in <strong>" . $line['function'] . "()" . "</strong><br />";
			echo $line['file']  . ":" . $line['line'] . " in " . $line['function'] . "()" . "\n\n";
			
			//print_r($line);
			//echo $line['line'] . " in " . $line['function'] . "()" . "\n\n";
			//echo "\n\n";
    		}
      	exit();
      	/**********/
      	
      	
      	$this->extractSQL($errors->exception);
      
      	$this->_helper->layout->disableLayout();
      
		$display_exceptions = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOption('display_exceptions');

      	if( $display_exceptions != 1)
      	{
			$requestUri = $errors->request->getRequestUri();

			if (isset($_SERVER['HTTP_USER_AGENT']) && stristr($_SERVER['HTTP_USER_AGENT'], 'GoogleBot')) {
				return false;
			}
			    

			if(
				$errors->request->action != "favicon.ico" 

				&& Blastit_Http_UserAgentIdentifier::isBot() == false
				
				&& $requestUri != "favicon.ico"
				&& $requestUri != "favicon.png"
				&& $requestUri != "favicon.gif"
				
				&& !stristr($errors->request->action, "favicon")
				&& !stristr($errors->request->controller, "favicon")

				&& !stristr($errors->request->controller, "PIE.htc")

				&& $errors->request->controller != "favicon.ico"
				&& $errors->request->controller != "favicon.png"
				&& $errors->request->controller != "favicon.gif"
				
				&& !stristr($errors->request->action, "/images/listings/") 
				
				&& !stristr($errors->request->action, "Mississ") 
				&& !stristr($errors->request->action, "Sebastian_Cut_out_1344559032.gif") 
				
				&& $requestUri != "/images/agents/2013/02/Terry.jpg"        			
				
				&& $requestUri != "/PIE.htc"
				&& $requestUri != "/pie/PIE.htc"      			
				
				&& $requestUri != "/images/agents/2012/08/Sebastian_Cut_out_1344559032.gif"

				&& $requestUri != "/v/"      			
				&& $requestUri != "/v/i/"      			

				&& $requestUri != "/n/"      			
				&& $requestUri != "/n/i/"      			
				&& $requestUri != "/n/i/fb/"      			

				
				&& !stristr($requestUri, ".git")
				&& !stristr($requestUri, "register.php")
				&& !stristr($requestUri, "forum.php")
				&& !stristr($requestUri, "login.php")

				&& !stristr($requestUri, "apple-touch-icon")
				&& !stristr($requestUri, "browserconfig.xml")
				&& !stristr($requestUri, "crossdomain.xml")
				&& !stristr($requestUri, "_vti_bin")
				&& !stristr($requestUri, "MSOffice")
				&& !stristr($requestUri, "wp-login.php")
				&& !stristr($requestUri, ".asp")
				&& !stristr($requestUri, "LiveSiteAuth.xml")
				&& !stristr($requestUri, "atom.xml")
				&& !stristr($requestUri, "index.rdf")
				&& !stristr($requestUri, "index.xml")
				&& !stristr($requestUri, "admin.php")
				&& !stristr($requestUri, "/trackback")
				&& !stristr($requestUri, "/manager")
				&& !stristr($requestUri, "/manager")
				
				&& !stristr($_SERVER['HTTP_USER_AGENT'], "bingbot")
				&& !stristr($_SERVER['HTTP_USER_AGENT'], "googlebot")
				
				// suppress errors from clients that don't know how to handle URLs beginning with "//"
				
				&& !(strpos($requestUri, "//") === 0)
								
				&& $requestUri != "/images/listings/2011/11/sample2.jpg"  
				&& $requestUri != "/images/listings/2011/08/Mississ" 
								
			)
			{
				$this->handleProductionError($errors, $error_type);
			}

			//$this->_redirect("/");
      	}    

	}

	private function airbrake($errors) {
	
		require_once( APPLICATION_PATH . '/../lib/Airbrake/Client.php' );
		$airbrake = $this->getInvokeArg('bootstrap')->getOption('airbrake');
		
		if (!$airbrake || !isset($airbrake['apikey']) || !isset($airbrake['enabled']) || !$airbrake['enabled']) return false;
		
		if ($errors->request->action == "favicon.ico") return false;
		
		$apiKey = $airbrake['apikey'];
		$options = array(
			'secure' => true, 
			'host' => 'exceptions.codebasehq.com', 
			'resource' => '/notifier_api/v2/notices',
			'environmentName' => APPLICATION_ENV,
			'timeout' => 10,
		);

		$config = new Airbrake\Configuration($apiKey, $options);
		$client = new Airbrake\Client($config);

		$client->notifyOnError($errors->exception);

	}

    
    	private function handleProductionError($errors, $error_type) 
    	{

		if(! defined('ERROR_RECIPIENTS') )
		{
			define('ERROR_RECIPIENTS',"alen@alpha-male.tv");	
		}		
		



		$from_email	=   "noreply@cityblast.com";
		$from_name	=   COMPANY_NAME . " error";

		$params	=   array(
	    					"exception"	=> $errors->exception,
	    					"sql"	=>  $this->sql,
	    					"sql_args"	=>  $this->sql_args,
	    					"request"	=>  $this->getRequest()
					);
		
		//$htmlBody       =   $this->view->partial('error/email.html.php', $params);
		$htmlBody = $this->view->render('error/email.html.php');

		$mail = new Zend_Mail('utf-8');
		$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
		$mail->setFrom($from_email, $from_name);
		$emailsList =	explode(",",  ERROR_RECIPIENTS);

		foreach($emailsList as $email)
	    		$mail->addTo($email);

		$mail->setSubject(COMPANY_NAME . " error found");


		$sentEmail = $mail->send();
		info("Sent mail to " . ERROR_RECIPIENTS);

		
		$this->_helper->layout->setLayout('layout');

		if($error_type == 404)
		{
			$this->render("404");
		}
		else
		{
			$this->render("500");
		}	

    }
    
	private function extractSQL($exception)
	{
		if($exception instanceOf ActiveRecord\DatabaseException) 
      	{
        		$trace = $exception->getTrace();
        		$first = $trace[0];
      
        		if(isset($first['function']) && $first['function'] == 'query' && is_array($first['args']))
        		{
          		$this->sql = $this->view->sql = $first['args'][0];
          		$this->sql_args = $this->view->sql_args = $first['args'][1];
        		}
      	}
	}

}

?>
