<?php

class RestController extends ApplicationController
{

	private $_serialize_param = COMPANY_NAME;
	private $_token_lifetime =  315576000; // Tokens last ten years.
	private $_oauthServer;
	
	private $_defaultScope = 'listings';
	
	private $_supportedScopes = array(
			  'account_info' => 'Access your account information',
			  'listings'     => 'Manage your listings',
			);
			
	private $_scopeResourceMap = array(
			  'account_info' => 'account',
			  'listings'     => 'listings',
			);
	
	protected static $acl = array(
		'*' => 'public'
	);

	public function index() 
	{
	
	}
	
	protected function oauthServer() 
	{
	
		if (!$this->_oauthServer) {

			$config  = array('access_lifetime' => $this->_token_lifetime);
			$storage = new Blastit_OAuth2_Storage_ActiveRecord;

			$memory = new OAuth2\Storage\Memory(array(
			  'default_scope'    => $this->_defaultScope,
			  'supported_scopes' => array_keys($this->_supportedScopes)
			));
			
			$server = new OAuth2\Server($storage, $config);
			$server->addGrantType( new OAuth2\GrantType\ClientCredentials($storage) );
			$server->addGrantType( new OAuth2\GrantType\AuthorizationCode($storage) );			
			$server->setScopeUtil( new OAuth2\Scope($memory) );

			$this->_oauthServer = $server;

		}
		
		return $this->_oauthServer;
	}
	
	public function resource() 
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->oauthServer()->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
			echo json_encode(array('success' => true, 'message' => 'You accessed my APIs!'));
		} else {
			$this->oauthServer()->getResponse()->send();
		}
	}

 	public function token() 
 	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$this->oauthServer()->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
	}
	
	public function authorize()
	{
		
		$this->redirectIfNotLoggedIn( '/member/authenticate', $this->getRequest()->getRequestUri() );
		
		$response = new OAuth2\Response();
		$request = OAuth2\Request::createFromGlobals();
		
		// only do authorization code requests
		$request->query['response_type'] = 'code';
		
		if (!$this->oauthServer()->validateAuthorizeRequest($request, $response)) 
		{
			$response->send();
			die;
		}		
	
		if (!$this->getRequest()->isPost()) 
		{
			$client_id = $this->oauthServer()->getAuthorizeController()->getClientId();	
			$client = OAuthClient::find_by_client_id($client_id);
			$scopes = explode(' ', $this->oauthServer()->getAuthorizeController()->getScope());
			$scopeLabels = array();
			
			foreach ($this->_supportedScopes as $scope => $scopeLabel) 
			{
				if (in_array($scope, $scopes)) $scopeLabels[] = $scopeLabel;
			}
			$this->view->client = $client;
			$this->view->scopes = $scopeLabels;
			return;
		} 

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$is_authorized = ($this->getRequest()->getParam('authorized') === 'Allow Access');

		$this->oauthServer()->handleAuthorizeRequest($request, $response, $is_authorized, $this->loggedInMember()->id);
		
		$response->send();
		
	}

	public function listing() 
	{		
		$this->authorizeResourceRequest();

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$request = $this->getRequest();
		$result  = array('errors' => array(), 'created' => false );
		
		$token   = OAuthAccessToken::find( $request->getParam('access_token') );
		$member  = $token->member;
		
		if ($member->isActive() == false) {
			$result['errors'][] = 'This member is no longer active. Please contact ' . COMPANY_NAME . ' to reinstate this account.';
		}
		
		if ($request->isPost()) {
			$params  = $request->getParams();
			
			if (!$this->_validateListing($params)) {
				$result['errors'][] = 'A listing with this information already exists.';
			} elseif (false == ($listing = $this->_listingFromArray($params, $member)) ) {
				$result['errors'][] = 'Could not create listing: missing required parameters.';
			} else {
				$listing->save();
				/*
				 Thumbnails disabled - Kfoster 2014-01-24
				if (isset($_FILES['thumbnail'])) {
					$tmpFile = APPLICATION_PATH . '/..' . TMP_ASSET_URL . '/' . $token->access_token . '/' . $_FILES['thumbnail']['name'];
					if (!file_exists(dirname($tmpFile))) mkdir(dirname($tmpFile), 0777, true);
					if (move_uploaded_file($_FILES['thumbnail']['tmp_name'], $tmpFile)) {
						$listing->ingestThumbnail($tmpFile);
					} else {
						@unlink($tmpFile);
					}
				}
				*/

				if (isset($_FILES['photos'])) {
					$i    = 0;
					$keys = array_keys($_FILES['photos']['name']);
					foreach ($_FILES['photos']['name'] as $photo) {
						if (!strlen($photo)) continue;
						$key = $keys[$i];
						$tmpFile = APPLICATION_PATH . '/..' .TMP_ASSET_URL . '/' . $token->access_token . '/' . $photo;
						if (!file_exists(dirname($tmpFile))) mkdir(dirname($tmpFile), 0777, true);
						if (move_uploaded_file($_FILES['photos']['tmp_name'][$key], $tmpFile)) {
							if (!$listing->image) {
								$listing->ingestImage($tmpFile);
							} else {
								$altImage = new ListingImage();
								$altImage->listing_id = $listing->id;
								$altImage->save();
								$altImage->ingestImage($tmpFile);
								$altImage->save();
							}
						} else {
							@unlink($tmpFile);
						}
						$i++;
					}
				}
			
				$listing->save();
			
				$member->publishOneOff($listing);				
				$result['created'] = true;
				$result['listing_id'] = $listing->id;
			}
		
		}
		
		echo Zend_Json::encode( $result );
		
	}

	protected function _listingFromArray($params, $member) {

		$required = array(
			'message',
			'street_number',
			'street',
			'city',
			'postal_code',
			'price',
			'property_type'
		);

		foreach ($required as $var) {
			if (isset($params[$var]) == false || strlen($params[$var]) == 0) {
				return false;
			}
		}

		$listing = new Listing();
		$listing->raw_message 			= $this->getFromArray($params, 'message');
		$listing->message 				= $this->sentence_case($this->getFromArray($params, 'message'));
		$listing->street 				= $this->getFromArray($params, 'street_number')." ".$this->getFromArray($params, 'street');
		$listing->appartment			= $this->getFromArray($params, 'apartment');
		$listing->postal_code 			= $this->getFromArray($params, 'postal_code');
		$listing->price 				= $this->getFromArray($params, 'price');
		$listing->mls 					= $this->getFromArray($params, 'mls');
		$listing->city   				= $this->getFromArray($params, 'city');
		$listing->square_footage 		= $this->getFromArray($params, 'square_footage');
		$listing->blast_type   			= Listing::TYPE_LISTING;
		$listing->pre_approval 			= 0;
		$listing->approval 				= 0;
		$listing->number_of_bedrooms	= intval($this->getFromArray($params, 'bedrooms'));
		$listing->number_of_extra_bedrooms	= intval($this->getFromArray($params, 'extra_bedrooms'));
		$listing->number_of_bathrooms	= intval($this->getFromArray($params, 'bathrooms'));
		$listing->number_of_parking		= intval($this->getFromArray($params, 'parking'));
		$listing->maintenance_fee		= $this->getFromArray($params, 'maintenance_fee');
		$listing->property_type_id		= $this->getFromArray($params, 'property_type');
		$listing->expiry_date			= date('Y-m-d H:i:s', strtotime('+30 days'));
		
		$matcher = new Blastit_CityMatcher;
		$city = $matcher->fuzzyMatchLocation($this->getFromArray($params, 'city'), $member);
		
		$listing->city_id 				= $city->id;
		$listing->member_id 			= $member->id;
		$listing->setLatitude();
		return $listing;

	}
	
	protected function getFromArray($array, $key) {
		if (isset($array[$key])) return $array[$key];
		return null;
	}

	protected function _validateListing(array $params) 
	{
		
		$result = true;
		$street = "{$params['street_number']} {$params['street']}";
		$listing = Listing::find( array('conditions' => array('street=? AND postal_code=? AND appartment=? AND city=? AND status != ?', $street, $params['postal_code'], $params['apartment'], $params['city'], 'deleted') ) );		
		
		if ($listing) {
			$created_at = strtotime($listing->created_at->format('Y-m-d H:i:s'));
			$time = time();
			$diffDay = (($time - $created_at)/(3600 * 24));			
			if ($diffDay <= 7) $result = false;
		}
		
		return $result;
	}

	protected function sentence_case($string)
	{
		$sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
		$new_string = '';
		foreach ($sentences as $key => $sentence) {
			$new_string .= ($key & 1) == 0 ?
				ucfirst(strtolower(trim($sentence))) :
				$sentence.' ';
		}
		return trim($new_string);
	}
	
	protected function getRequiredScope() {
		$action = $this->getRequest()->getActionName();
		if ( $scope = array_search($action, array_values($this->_scopeResourceMap)) ) {
			return $scope;
		}
		return false;
	}
	
	protected function authorizeResourceRequest() {
		$response = new OAuth2\Response();
		if (false === ( $scope = $this->getRequiredScope() )) return false;
		if ($this->oauthServer()->verifyResourceRequest(OAuth2\Request::createFromGlobals(), $response, $scope)) {
			return true;
		} else {
			$this->oauthServer()->getResponse()->send();
			exit;
		}
	}

}
