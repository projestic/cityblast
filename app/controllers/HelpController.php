<?php

class HelpController extends ApplicationController
{
	/**
	* Section Config
	*
	* @var array
	*/
	protected $sections = array(
		'referral'          => 'referral',
		'linkedinreset'     => 'linkedinreset',
		'twitterreset'      => 'twitterreset',
		'fanpagereset'      => 'fanpagereset',
		'frequency'         => 'frequency',
		'customcontent'     => 'customcontent',
		'reblasting'        => 'reblasting',
		'addphoto'          => 'addphoto',
		'dashboard'         => 'dashboard',
		'bookings'          => 'bookings',
		'updatecc'          => 'updatecc',
		'listings'          => 'listings',
		'addlinkedin'       => 'addlinkedin',
		'addtwitter'        => 'addtwitter',
		'addfanpage'        => 'addfanpage',
		'index'        	=> 'index'
	);

	/**
	* Help Index Action
	*/
	public function index()
	{
		$this->view->footerType = 'fancy';
		$this->view->current_url = $this->getRequest()->getRequestUri();
	}

	/**
	* View Help Section Action
	*/
	public function view()
	{	
		$section = $this->getParam('section');
		
		if($section == "index") $this->view->footerType = 'fancy';
		
		if (isset($this->sections[$section])) 
		{
			$section = $this->sections[$section];
			$this->_helper->viewRenderer($section);
			return;
		}
		//render 404
		throw new Zend_Controller_Action_Exception('This page does not exist', 404);
	}

}