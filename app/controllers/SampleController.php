<?
class SampleController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'public'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('landing-no-header-footer');
		
		$request = $this->getRequest();
		$cb_cid = $_SESSION['cb_cid']  = $request->getParam('cid');

		if(isset($_SERVER['HTTP_REFERER']))
		{
			if(stristr( strtolower($_SERVER['HTTP_REFERER']), 'facebook'))
			{
				setcookie('cityblast_aff_id',1183,time()+ 10*365*24*60*60, '/');	
				setcookie('affiliate_id',1183,time()+ 10*365*24*60*60, '/');					
			}
		}
		
		if ($cb_cid)
		{
			setcookie('cb_cid',$cb_cid,time()+ 10*365*24*60*60, '/');		
		}
		
		parent::init();
		
	}

	public function index()
	{
		$this->view->is_facebook = false;
	}

	public function overlay()
	{
		$this->view->is_facebook = false;
	}

	public function bigvideo1()
	{
		$this->_helper->layout()->setLayout('landing-blank');
	}

	public function video1()
	{
		$this->_helper->layout->setLayout('landing-white');			
	}

	public function video2()
	{
		$this->_helper->layout->setLayout('landing-white');			
	}

	public function video3()
	{
		$tag_groups = Tag::getTagGroups($this->_request->getParams());
		$this->view->tag_groups = $tag_groups;
		$this->_helper->layout->setLayout('landing-white');
	}

	public function video4()
	{
		$this->_helper->layout->setLayout('landing-white');			
	}

	public function video5()
	{
		$this->_helper->layout->setLayout('landing-white');
	}

	public function preview()
	{
		$this->_helper->layout->setLayout('nolayout');		


		$request = $this->getRequest();
		
		if ($request->isPost()) 
		{

			$city = ucwords($request->getParam('city_and_state'));

			$dummyContent = array(
				array(
					  'description' => null,
					  'message' => "Are you looking for a home in $city? Get in touch to discuss which neighborhood best suits your needs!", 
					  'image'   => '/images/dummycontent/1.jpg',
					  'content_link'    => '',
					  'blast_type' => 'CONTENT'
				),
				array(
					  'description' => null,
					  'message' => "Selling your home but having trouble taking great real estate listing photos? These new and innovative apps can help:", 
					  'image'   => '/images/dummycontent/6.jpg',
					  'content_link'    => 'http://www.mashable.com/2014/06/01/real-estate-apps',
					  'blast_type' => 'CONTENT'
				),
				array(
					  'description' => null,
					  'message' => "Looking to buy a home in $city? Good news: the latest research shows that property values in $city are on the rise!", 
					  'image'   => '/images/dummycontent/2.jpg',
					  'content_link'    => 'http://www.realtor.org/news-releases/2014/06/survey-of-property-values',
					  'blast_type' => 'CONTENT'
				),
				array(
					  'description' => null,
					  'message' => 'What are you most interested in when visiting a new house... <br/>A) The kitchen, <br/>B) The bedroom, <br/>C) Closet space ... <br/><br/>Leave a comment with your answer!', 
					  'image'   => '/images/dummycontent/3.jpg',
					  'content_link'    => '',
					  'blast_type' => 'CONTENT'
				),
				array(
					  'description' => null,
					  'message' => "Do you like living within walking distance of local businesses and amenities? This handy tool will give you the “walkability score” of homes in $city and help you choose the right one:", 
					  'image'   => '/images/dummycontent/4.jpg',
					  'content_link'    => 'http://www.techcrunch.com/2014/05/30/walkability-score/',
					  'blast_type' => 'CONTENT'
				),
				array(
					  'description' => null,
					  'message' => "Are you looking to find out what homes in $city have been selling for before making an offer? This useful guide will give you all the information that you need.", 
					  'image'   => '/images/dummycontent/5.jpg',
					  'content_link'    => 'http://www.realtor.com/home-value-calculator/',
					  'blast_type' => 'CONTENT'
				),

			);

			//shuffle($dummyContent);

			$this->view->listings = $dummyContent;
			
		}
	}

	public function clicktocall1()
	{
		$this->view->is_facebook = false;
	}

	public function clicktocall2()
	{
		$this->view->is_facebook = false;
	}

	public function clicktocall3()
	{
		$this->view->is_facebook = false;
	}

	public function clicktocall4()
	{
		$this->view->is_facebook = false;
	}

	public function clicktocall5()
	{
		$this->view->is_facebook = false;
	}

	public function clicktocall6()
	{
		$this->view->is_facebook = false;
	}

	public function clicktocall7()
	{
		$this->view->is_facebook = false;
	}
}

?>