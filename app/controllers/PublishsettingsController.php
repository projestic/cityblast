<?php

class PublishsettingsController extends ApplicationController
{
	protected static $acl = array(
		'*' => 'superadmin'
	);

	public function index()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->selected_status = "publishsettings";
		$conditions = array("conditions" => "1=1", "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('PublishSettings', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;				
	}

	public function member()
	{

		if ($this->_request->isXmlHttpRequest()) {
			$this->_helper->layout()->disableLayout();
		} else {
			$this->_helper->layout->setLayout('admin');
		}	    	
		
		$this->view->selected_status = "publishsettings";
		
		$member_id = $this->getRequest()->getParam('id');
		
		$this->view->selected_status = "publishsettings";
		$conditions = array("conditions" => array("member_id" => $member_id), "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('PublishSettings', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;				
		
		$this->render("index");
	}
}
