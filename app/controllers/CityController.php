<?php

class CityController extends ApplicationController
{
	protected static $acl = array(
		'delete' => 'superadmin',
		'*' => 'public'
	);

	public function index()
	{
		$this->_helper->layout->setLayout('nolayout');
		$country = $this->_getParam('country');
		$blasting = $this->_getParam('blast');
		if($this->_getParam('hasmember')){
			$sql = "select * from ".City::$table." as city where country = '".$country."' and id IN (select city_id from ".Member::$table." group by city_id)";
			$cities	= City::find_by_sql($sql);
		}else if($blasting == 'ANY'){
			$conditions = "country = '".$country."'";
			$cities	= City::find('all',  array('conditions' => $conditions, 'order' => 'name ASC'));
		}
		else{
			$conditions = "country = '".$country."' and blasting = '".$blasting."'";
			$cities	= City::find('all', array('conditions' => $conditions));
		}
		$this->view->cities = $cities;

		/*$this->_redirect($_SERVER['HTTP_REFERER']);*/
	}

	public function select()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		if($this->_request->isPost())
		{

			$city = City::find_by_id(intval($this->_request->getParam('city_id')));
			$this->setSelectedCity($city->id, $city->name, $this->_request->getParam('country'));
			echo json_encode( array("error"=>false) );
		}
	}

	public function changecity()
	{
		$city = City::find_by_id($this->_getParam('admin_city'));


		$_SESSION['admin_selectedCity']		= $this->_getParam('admin_city');
		$_SESSION['admin_selectedCityName']	= $city->name;
		$_SESSION['admin_selectedCountry']		= $city->country;


		$this->_redirect($_SERVER['HTTP_REFERER']);
	}

	public function add()
	{
		$this->_helper->layout->setLayout('admin');

		$id	=   $this->_request->getParam('id');

	    	if($this->_request->isPost())
	    	{
	    		if(!empty($id) && $id != 0)
	    			$city = City::find($id);
	    		else
	    			$city = new City();

	    		$city->name 					= $this->_request->getParam('name');
	    		$city->active 					= $this->_request->getParam('active');
	    		$city->blasting				= $this->_request->getParam('blasting');
	    		$city->inventory_unit_size 		= $this->_request->getParam('inventory_unit_size');
	    		$city->min_inventory_threshold 	= $this->_request->getParam('min_inventory_threshold');
	    		$city->max_inventory_threshold 	= $this->_request->getParam('max_inventory_threshold');
	    		$city->number_of_inventory_units	= $this->_request->getParam('number_of_inventory_units');
	    		$city->number_of_hoppers 		= 5; //5 is now the value to be used now, 
								     //if anther value is to be used, uncomment the line below
	    		//$city->number_of_hoppers 		= $this->_request->getParam('number_of_hoppers');


	    		$city->type 					= $this->_request->getParam('type');
	    		$city->domain 					= $this->_request->getParam('domain');
	    		$city->facebook_app_id 			= $this->_request->getParam('facebook_app_id');
	    		$city->facebook_app_secret 		= $this->_request->getParam('facebook_app_secret');
	    		$city->facebook_access_token 		= $this->_request->getParam('facebook_access_token');
	    		$city->logo 					= $this->_request->getParam('logo');

	    		$city->state 					= $this->_request->getParam('state');
	    		$city->country 				= $this->_request->getParam('country');


	    		$city->save();

	    		$this->_redirect('/admin/cities');

	    	}

		$this->view->city = null;
	    	if(!empty($id))
	    	{
	    		$this->view->city = City::find($id);
	    	}

	}

	function autocomplete()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$term = $this->_getParam('term');
		$conditions = array("name LIKE ?", $term . '%' );
		$cities = City::find('all',array('conditions' => $conditions, 'select' => 'id , name, CONCAT(name,", ",state) as label','limit' => 20));

		if($cities){
			$items = array();
			if($cities){
				foreach($cities as $city){
					$items[] = array('id' => $city->id, 'label' => $city->label, 'name' => $city->name);
				}
			}
			echo json_encode( $items );
		}else{
			echo false;
		}
	}

	public function delete()
	{
	    $this->renderNothing();

	    $cityId	= (int)$this->getRequest()->getParam('id');

	    if ( $cityId > 0) {
	        $message = new Zend_Session_Namespace('messages');
	        $message->data = new stdClass;
	        try {
                $city = City::find($cityId);
                $city->delete();
	            $message->data->message = 'The city "' . $city->name . '" is successfully deleted.';
	        } catch ( Exception $e ) {
                $message->data->message = $e->getMessage();
            }

            $this->_redirect('/admin/cities/');
	    }

        $this->_redirect($this->getRequest()->getServer('HTTP_REFERER', '/'));
	}
	
	public function get_all()
	{
		if ($this->_request->isXmlHttpRequest()) 
		{
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
		}
		
		$params = $this->getRequest()->getParams();
		
		$group_by = !empty($params['group_by']) && in_array($params['group_by'], array('country', 'state')) ? $params['group_by'] : null;
		$country = !empty($params['country']) ? $params['country'] : null;
		$state = !empty($params['state']) ? $params['state'] : null;
		
		$sql_params = array();
		$sql = "SELECT * FROM city ";
		$where_clauses = array();
		if ($country) 
		{
			$where_clauses[] = "country = ?";
			$sql_params[] = $country;
			
			$order_by = " ORDER BY state ASC ";
		}
		if ($state) 
		{
			$where_clauses[] = "state = ?";
			$sql_params[] = $state;
			
			$order_by = " ORDER BY name ASC ";
		}

		
		if (!empty($where_clauses)) {
			$sql .=  "WHERE ". implode(" AND ", $where_clauses);
		}
		if (in_array($group_by, array('country', 'state'))) 
		{
			$sql .= " GROUP BY " . $group_by;
		}
		
		$sql .= $order_by;

	
		
		$cities = City::find_by_sql($sql, $sql_params);
		
		$repsonse = array();
		if (!empty($cities)) 
		{
			foreach ($cities as $city) 
			{
				$key = empty($group_by) ? $city->id : $city->{$group_by};
				$repsonse[$key] = array(
					'id' => $city->id,
					'name' => $city->name,
					'state' => $city->state,
					'country' => $city->country
				);
			}
		}
		
		$this->_helper->json($repsonse);
	}
}
?>