<?
class AffiliateController extends ApplicationController
{

	protected static $acl = array(
		'*' => 'public',
		'signupinfo' => '*'
	);

	public function init()
	{
		parent::init();
		
		$tag_groups = TagGroup::find('all');
		
		$signupSession  = new Zend_Session_Namespace('signup');
		$member_type = MemberType::find_by_code('AGENT');
		
		$tag_group_data = array();
		if (!empty($tag_groups)) 
		{
			foreach ($tag_groups as $group) 
			{
				$group_tags = Tag::findAllByMemberTypeAndTagGroup($member_type, $group);
				$tag_group_data[] = array(
					'group' => $group,
					'tags' => $group_tags
				);
			}
		}
		$this->view->tag_groups = $tag_group_data;
	}

	public function index()
	{
		if ($this->_getParam('aff_id')) 
		{
			//@todo add some hashing
			$referer_id			= $this->_getParam('aff_id');
			$referer_session		= new Zend_Session_Namespace("Referer");
			$referer_session->id	= $referer_id;

			// we also keep referer info in cookie
			$_COOKIE['affiliate_id']     = $referer_id;
			$_COOKIE['cityblast_aff_id'] = $referer_id;

			try {
				$affiliate = Affiliate::find($referer_id);
				$member = $affiliate->owner;
			} catch (ActiveRecord\RecordNotFound $e) {
				$member = Member::find_by_id($referer_id);
				if (!$member) $this->RedirectUrl('/');
			}
			
			$this->view->member = $member;

		}

	}
	
	public function dashboard() {}

	public function signup()
	{	
		$auth_token = $this->_request->getParam('access_token');	

		$code = $this->_request->getParam('code');

		if (!$auth_token && !$code) return;

		// $this->checkUserDenied();
		
		$network_app_facebook = Zend_Registry::get('networkAppFacebook');

		$facebook = $network_app_facebook->getFacebookAPI();

		if (empty($code))
		{
			$signup_params = array(
				'client_id' 	=> $network_app_facebook->consumer_id,
				'scope' 		=> FB_SCOPE,
				'redirect_uri' 	=> APP_URL . '/affiliate/signup'
			);
			$signup_url = 'http://www.facebook.com/dialog/oauth?' . http_build_query($signup_params);
			$this->_redirect($signup_url);
		}


		$member = new Member();
		
		$member->application_id = $network_app_facebook->id;
		$member->createAccessToken($code, "/affiliate/signup");

		$access_token = $member->access_token;
		
		$fb_user = $facebook->api("/" . $member->uid . "?access_token=" .  $member->access_token);

		if ($memberExisting = Member::find_by_uid($fb_user['id'])) {
			$member = $memberExisting;
			$member->application_id = $network_app_facebook->id;
			$member->access_token = $access_token;
			$_SESSION['new_registry'] = false;
		} else {
			$_SESSION['new_registry'] = true;
		}
		
		$member_type = MemberType::find_by_code('AFFILIATE');
		if ($member->type_id != 5) $member->type_id = $member_type->id;
		$member->save_user($fb_user);
		
		$member->account_type = "signup";
		$member->payment_status = 'paid';
		$member->frequency_id = 0;
		$member->publish_city_id = 1;
		$member->facebook_publish_flag = 0;
		$member->twitter_publish_flag = 0;
		$member->linkedin_publish_flag = 0;
		$member->city_id = 1;
		$member->save();
		
		if (false == ($affiliate = Affiliate::find_by_member_id($member->id))) {
			$affiliate = new Affiliate;
			$affiliate->id 						 = $member->id;
			$affiliate->member_id 				 = $member->id;
			$affiliate->first_payout_percent 	 = 20;
			$affiliate->recurring_payout_percent = 20;
			$affiliate->save();
		}
				
		$_SESSION['member'] 	= $member;
		
		$this->_redirect('/affiliate/signupinfo');
		
	}
	
	public function signupinfo() 
	{

		$request = $this->getRequest();

		$this->view->member = $member = $_SESSION['member'];

		$this->view->error_fields = array();
		$this->view->error_messages = array();
		
		if (!$request->isPost() || empty($member)) return;
	
		$email = $request->getPost('email');
		$phone = $request->getPost('phone');
		$slug  = $request->getPost('slug');
		
		$logo_uploaded  = $request->getPost('logo_uploaded');
		
		$testimonial = strip_tags($request->getPost('testimonial'), '<b><i><strong><em><br>');

		$testimonialModel = Testimonial::find_by_member_id($member->id);
		if ($testimonialModel) {
			$testimonialModel->testimonial = $testimonial;
		} else {
			$testimonialModel = new Testimonial();
			$testimonialModel->member_id = $member->id;
			$testimonialModel->testimonial = $testimonial;
		}
		$testimonialModel->save();

		$logo_uploaded = $request->getPost('logo_uploaded');
		
		$slug  = preg_replace('/[^A-Za-z0-9\-]/', '', strtolower($slug));
		
		$validator = new Blastit_Affiliate_SlugValidator;
		
		$member->affiliate_account->slug = $slug;

		if (!empty($logo_uploaded)) {

			$image  = stristr($logo_uploaded, TMP_ASSET_URL) ? $logo_uploaded : TMP_ASSET_URL . $logo_uploaded;
			$image 	= str_replace(TMP_ASSET_URL, TMP_ASSET_STORAGE, $image);
			
			if (file_exists($image)) {
				$ext = pathinfo($image, PATHINFO_EXTENSION);		
				$final_logo = '/images/affiliates/' . $member->affiliate_account->id . '.' . $ext;
				copy($image, CDN_ORIGIN_STORAGE . $final_logo);
				$member->affiliate_account->logo = $final_logo;
				@unlink($image);
			}
		}
		
		if (false === (mb_strlen($testimonial) >= 100 && mb_strlen($testimonial) <= 500)) {
			$this->view->error_fields[]   = 'testimonial';
			$this->view->error_messages[] = 'Your testimonial must be between 100 and 500 characters.';
		}
		
		if ($validator->validate($slug, $member) == false) {
			$this->view->error_fields[]   = 'slug';
			$this->view->error_messages[] = $validator->getMessage();
		}

		if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
			$this->view->error_fields[]   = 'email';
			$this->view->error_messages[] = 'Please enter a valid email address.';
		}

		if (count($this->view->error_fields) == 0) {
		
			$member->affiliate_account->save();
			$member->save();
			
			if (false == ($promo = Promo::find_by_affiliate_id($member->affiliate_account->id))) {
				$promo 				 = new Promo;
				$promo->affiliate_id = $member->affiliate_account->id;
				$promo->amount		 = 20;
			}

			$promo->promo_code   = strtolower($slug);
			$promo->type 		 = 'PERCENT';
			$promo->save();
		
			$cache = Zend_Registry::get('cache');
			$cache->remove('affiliate_slugs');

			$this->_redirect('/member/settings');

		}
		
	}

	protected function cleanupTmpDir() 
	{
		foreach (scandir(TMP_ASSET_STORAGE) as $file) 
		{
			$file = TMP_ASSET_STORAGE . '/' . $file;
			if (is_file($file) && $file != '..' && $file != '.' && !stristr($file, '.svn') && (filemtime($file) < time() - 14400)) {
				@unlink($file);
			} elseif (is_dir($file)) {
				@rmdir($file);
			}
		}
	}

	public function uploadlogo() 
	{ 

		$tmp_subdir = date('Y-m-d-H');

		$this->renderNothing();

		if (!empty($_FILES))
		{

			$tempFile = $_FILES['Filedata']['tmp_name'];

			$size = getimagesize($tempFile);

			if (($size[0] > 335) || ($size[1] > 55)) {
				echo "Sorry! Your logo must be no larger than 335px wide and 55px high.";
				return;
			}
			
			$arr		= explode(".",$_FILES['Filedata']['name']);
			$extension  = end($arr);
			$filename   = str_replace(".".$extension,"",$_FILES['Filedata']['name']);

			$filename   = str_replace("-","_",$filename);
			$finalName  = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $filename);
			$finalName  = trim($finalName, '-');
			$finalName  = preg_replace("/[\/_|+ -]+/", '_', $finalName) . ".$extension";

			$targetFile = TMP_ASSET_STORAGE . "/" . $tmp_subdir . "/". $finalName;

			if(	!file_exists(TMP_ASSET_STORAGE . "/" . $tmp_subdir)	)
			{
				mkdir(TMP_ASSET_STORAGE . "/" . $tmp_subdir, 0777, true);
			}
			
			if (file_exists($targetFile)) 
			{
				$targetFileInfo = pathinfo($targetFile);
				$targetFile = $targetFileInfo['dirname'] . '/' . $targetFileInfo['filename'] . '_' . strtotime('now') . '.' . $targetFileInfo['extension'];
			}

			if(move_uploaded_file($tempFile, $targetFile))
			{
				echo TMP_ASSET_URL . '/' . $tmp_subdir . "/". basename($targetFile);
			}
			else
			{
				$msg = null;

				switch ($_FILES['Filedata']['error'])
				{
					case 0:
						$msg = "No Error"; // comment this out if you don't want a message to appear on success.
						break;
					case 1:
						$msg = "The file is bigger than this PHP installation allows";
						break;
					case 2:
						$msg = "The file is bigger than this form allows";
						break;
					case 3:
						$msg = "Only part of the file was uploaded";
						break;
					case 4:
						$msg = "No file was uploaded";
						break;
					case 6:
						$msg = "Missing a temporary folder";
						break;
					case 7:
						$msg = "Failed to write file to disk";
						break;
					case 8:
						$msg = "File upload stopped by extension";
						break;
					default:
						$msg = "unknown error ".$_FILES['Filedata']['error'];
						break;
				}

				echo $msg;
			}

		}
		else
		{
			echo "0";
		}

		$this->cleanupTmpDir();

	}
	
	public function landingpage() 
	{
		$slug = $this->getRequest()->getParam('slug');

		if (!$slug || (false === ($a = Affiliate::find_by_slug($slug))) ) {
			$this->getResponse()->setHttpResponseCode(404);
			return;
		}
		
		if (empty($_COOKIE['cityblast_aff_id']) && empty($_COOKIE['cityblast_aff_id'])) {
			setcookie('cityblast_aff_id',$a->member_id,time()+ 10*365*24*60*60, '/');
			setcookie('affiliate_id',$a->member_id,time()+ 10*365*24*60*60, '/');
		}
		
		$this->_helper->layout->setLayout('landing');
		$this->view->affiliate = $a;
		$this->render("affiliate-lander");

	}

	public function landingpage_referral() 
	{
		$slug = $this->getRequest()->getParam('slug');
	
		if (!$slug || (false === ($a = Affiliate::find_by_slug($slug))) ) {
			$this->getResponse()->setHttpResponseCode(404);
			return;
		}
		
		if (empty($_COOKIE['cityblast_aff_id']) && empty($_COOKIE['cityblast_aff_id'])) {
			setcookie('cityblast_aff_id',$a->member_id,time()+ 10*365*24*60*60, '/');
			setcookie('affiliate_id',$a->member_id,time()+ 10*365*24*60*60, '/');
		}
		
		$this->_helper->layout->setLayout('landing');
		$this->view->affiliate = $a;
		$this->render("affiliate-lander");

	}

	public function videojoin()
	{
		$this->view->has_comments = 1;
		
		$this->_helper->layout->setLayout('landing-responsive');		
		$this->_join();
	}


	public function vision()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();

		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}

		$this->render("affiliate-lander");		
	}

	public function estatevue()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();

		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}	
		
		$this->render("affiliate-lander");		
	}

	public function retechulous()
	{
		$request = $this->getRequest();
		$cb_cid = $_SESSION['cb_cid']  = $request->getParam('cid');
		
		if ($cb_cid)
		{
			setcookie('cb_cid',$cb_cid,time()+ 10*365*24*60*60, '/');		
		}

		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');
					
				
		$this->view->city_id_passthru = NULL;
		if (isset($_REQUEST['city_id'])) $this->view->city_id_passthru = $_REQUEST['city_id'];
		
		//$this->_helper->layout->setLayout('landing');		
		$this->_join();

		$this->view->logo = '<img src="/images/retechulous.png" alt="Retechulous" />';

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
				
		
		$this->render("affiliate-lander");
	}

	public function lonewolf()
	{
		setcookie('affiliate_id', $this->_getParam('aff_id'), time() + (60 * 60 * 24 * 365), '/');
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time() + (60 * 60 * 24 * 365), '/');
		
		
		$this->_helper->layout->setLayout('landing-responsive');
				
		$this->view->headMeta()->appendName('WT.z_Connect_Company', 'City Blast');
		$this->view->headMeta()->appendName('WT.z_Connect_Site_Type', '3RD Party');
		
		// Tracking code provided by lonewolf affiliate
		$this->view->headScript()->appendFile('/js/webtrends.js');
		$js_a = 'var trendVar = "www.city-blast.com";
			var dcsidVar = "dcs6p84o900000w0yf68kq216_9x3g";
			var _tag=new WebTrends();
			_tag.dcsGetId();';
		$this->view->headScript()->appendScript($js_a);
		$js_b = '_tag.dcsCustom=function(){
			// Add custom parameters here.
			//_tag.DCSext.param_name=param_value;
			}
			_tag.dcsCollect();';
		$this->view->headScript()->appendScript($js_b);
				
		$this->_join();

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
	}


	public function property()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = "Performance Property";	
		//$this->view->logo = '<img src="/images/retechulous.png" alt="Retechulous" />';

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");		
	}
	
	public function boost()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');
		
		$this->view->logo = "Agents Boost";	


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");		
	}

	public function crew()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');
		
		$this->view->logo = "CREW-LOGO";	


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");		
	}

	public function royal()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();


		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = '<img src="/images/royal.png" alt="Royal LePage" />';	


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");			
	}

	public function ripley()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();


		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = 'Take Action';	


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
					
		$this->render("affiliate-lander"); 
	}

	public function garrett()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();


		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = 'Garrett Christianson';	


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
					
		$this->render("affiliate-lander"); 
	}
	
	public function leader()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();


		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = '<img src="/images/leader.jpg" alt="Chris Leader" />';	

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander"); 
	}	
	
	public function agentpartner()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();


		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = '<img src="/images/agent-partner.png" alt="Agent Parnter" />';	

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");			
	}
	

	public function listingboss()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = '<img src="/images/hoss.png" alt="Hoss Pratt" />';	


		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");			
	}

	public function realbies()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = '<img src="/images/realbies.png" alt="Realbies" />';	

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");	
	}

	public function coach()
	{	
		$this->_helper->layout->setLayout('landing');		
		$this->_join();
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = "COACH-LOGO";	

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");	
	}

	public function webinar()
	{

		//James Kupka
		if (stristr(APPLICATION_ENV, 'mortgagemingler'))
		{
			setcookie('affiliate_id', 97, time() + (60 * 60 * 24 * 365), '/');		
		}
		else
		{
			setcookie('affiliate_id', 1000452, time() + (60 * 60 * 24 * 365), '/');	
		}
				
		$this->view->has_comments = 1;
		
		//$this->_helper->layout->setLayout('landing-responsive');		
		
		
		$this->_join();
		
			
		$this->view->logo = "RANDOM-LOGO";	

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");		
	}
	
	public function special()
	{
		setcookie('affiliate_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');
				
		$this->view->has_comments = 1;
		$this->_join();
		
		$this->render("affiliate-lander");		
	}

	public function seevirtual()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = "SEE_VIRTUAL-LOGO";

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
			
		$this->render("affiliate-lander");	
	}

	public function recvideojoin()
	{
		$this->_helper->layout->setLayout('landing');		
		$this->_join();
		setcookie('cityblast_aff_id', $this->_getParam('aff_id'), time()+60*60*24*60, '/');

		$this->view->logo = '<img src="/images/rec.jpg" alt="Real Estate Talk Show" />';

		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			KissMetrics::init(KISSMETRICS_API_KEY);
			KissMetrics::record('Viewed '.$this->view->action.' Lander');
		}
			
				
		$this->render("affiliate-lander");	
	}
	
	
}


?>