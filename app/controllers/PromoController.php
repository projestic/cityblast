<?php

class PromoController extends ApplicationController
{
	
	protected static $acl = array(
		'code' => 'public',
		'*' => 'superadmin'
	);

	public function init()
	{
		$this->_helper->layout->setLayout('admin');
		parent::init();
		$this->view->admin_selectedCity    = $this->getSelectedAdminCity();
	}
	
	protected function getStandardPriceData()
	{
		$result = array();
		$billing_cycles = array('month', 'biannual', 'annual');
		$member_types = MemberType::all();
		if (!empty($member_types)) {
			foreach ($member_types as $member_type) {
				if ($member_type->promo_codes_allowed) {
					$member_type_data = array('member_type' => $member_type);
					foreach ($billing_cycles as $billing_cycle) {
						$member_type_data['prices'][$billing_cycle] = $member_type->getPrice($billing_cycle);
					}
					$result[$member_type->id] = $member_type_data;
				}
			}
		}
		return $result;
	}
	
	public function calculate_prices()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$params = $this->getRequest()->getParams();
		$type = !empty($params['type']) ? $params['type'] : null;
		$amount = !empty($params['amount']) ? $params['amount'] : null;
		
		$continue = true;
		$params['error'] = false;

		$member_types = MemberType::all();
		$billing_cycles = array('month', 'biannual', 'annual');
		foreach ($member_types as $member_type) {
			if ($member_type->promo_codes_allowed) {
				foreach ($billing_cycles as $billing_cycle) 
				{
					$default_price = $member_type->getPrice($billing_cycle);
					$price = Promo::calculatePrice($default_price, $type, $amount);
					
					$billing_cycle_months = MemberType::getBillingCycleMonths($billing_cycle);
					$price_month = number_format($price / $billing_cycle_months, 2, '.', '');
					$price_month_parts =  explode('.', $price_month);

					$params['prices'][$member_type->id][$billing_cycle] = number_format($price, 2);
				}
				}
		}

		
		$result = json_encode($params);
		echo $result;
	}
	
	public function index()
	{
		$conditions = array("conditions" => "", "order" => "id DESC");
		$paginator = new Zend_Paginator(new ARPaginator('Promo', $conditions));
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		$paginator->setItemCountPerPage(self::ITEMS_PER_PAGE);
		$this->view->paginator = $paginator;
	}	
	
	public function add()
	{			
		$this->view->standard_price_data = $this->getStandardPriceData();
		$this->view->affiliates = Affiliate::all(array('joins' => array('owner'), 'order' => 'member.first_name ASC'));
		
		$this->view->franchises = Franchise::find('all');
	}

	public function update()
	{
		$params =	$this->getRequest()->getParams();
		$id = $params['id'];
		
		$this->view->promo = Promo::find($id);

		$this->view->standard_price_data = $this->getStandardPriceData();
		$this->view->affiliates = Affiliate::all(array('joins' => array('owner'), 'order' => 'member.first_name ASC'));

		$this->view->franchises = Franchise::find('all');

		$this->render("add");
	}

	public function save()
	{
		$params =	$this->getRequest()->getParams();

		if($this->_request->isPost()) {
			$id = $params['id'];
			if(!empty($id) && $id != 0) $promo = Promo::find($id);
			else $promo = new Promo();
			$promo->promo_code = strtolower($params['promo_code']);
			$promo->amount = $params['amount'];
			$promo->type = $params['type'];
			if (isset($params['affiliate_id']) && $params['affiliate_id']) {
				$promo->affiliate_id = $params['affiliate_id'];
			}

			if (isset($params['franchise_id']) && $params['franchise_id']) {
				$promo->franchise_id = $params['franchise_id'];
			}

			$promo->save();
		}
		
		$this->_redirect('/promo/index');
	}


	public function delete()
	{
		
		$params =	$this->getRequest()->getParams();


		$id = $params['id'];
		
		
		if(!empty($id) && $id != 0)
		{ 
			$promo = Promo::find($id);
			$promo->delete();
		}
			

		$this->_redirect('/promo/index');
	}

	public function code()
	{	
		$request = $this->getRequest();
		$params =	$this->getRequest()->getParams();

		if (!empty($params['promo_code'])) 
		{
			setcookie('promo_code', $params['promo_code'], time()+ 10*365*24*60*60, '/');
		}
		
		$this->_redirect('/member/index');

	}
}
