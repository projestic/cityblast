<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		<table style="width: 100%; border-bottom: 0px;">
		<tr>
			<td style="border-bottom: 0px;">
				<h1>
					Refunds
				</h1>
			</td>

			<td style="text-align: right;border-bottom: 0px;">
				<a href="/refund/add/id/<?=$this->member->id;?>/" class="uiButton" style="float: right;">Add Refund</a>
			</td>
			
		</tr>
		</table>
		
	
		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>ID</th>
				<th>Member</th>
				<th>Admin</th>
				<th>Amount</th>
				<th>Note</th>
				<th>Created</th>
			</tr>
			<? 

			$row = true;
			$row_class = 'odd';
			
			foreach($this->paginator as $ref)
			{
				$row_class = empty($row_class) ? 'odd' : '';
			
?>

			<tr class="<?=$row_class?>">
				<td><?=$ref->id?></td>
				<td><a href="/admin/member/<?=$ref->member_id?>" style="color: #3D6DCC;"><? 
					if(!empty($ref->mfirst)) echo $ref->mfirst . " ";
					if(!empty($ref->mlast)) echo $ref->mlast;
				?>
				</td>
				<td>
				<? if($ref->admin_id): ?>
					<a href="/admin/member/<?=$ref->admin_id?>" style="color: #3D6DCC;"><? 
					if(!empty($ref->afirst)) echo $ref->afirst . " ";
					if(!empty($ref->alast)) echo $ref->alast;
					?>
				<? else: ?>
				-
				<? endif;?>
				<td><?=$ref->amount?></td>
				<td><?=$ref->note?></td>
				<td><?=$ref->created_at->format('Y-M-d H:i:s')?></td>

			</tr>
<?php
			}

		?>
				
		</table>


		<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />


	</div>



</div>