<?php
$app = !empty($this->app) ? $this->app : null;
$networks = array('FACEBOOK', 'TWITTER', 'LINKEDIN');
$values = $this->form_values;
?>

<form action="/networkapp/edit" method="POST" class="clearfix">
<input type="hidden" name="id" value="<?=is_object($this->app) ? $this->app->id:'';?>" />
				

<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">
	<div class="grid_12">
		<?php if(!empty($this->updated)): ?>
			<div class="grid_12">
				<h3>Your App was successfully added!</h3>
			</div>
		
			<div class="clearfix">&nbsp;</div>	
		<?php endif; ?>


		<?php if( !empty($this->error) ) : ?>
			<div style="color:red;font-size:18px;text-align:center;"><?php echo $this->error;?></div>
			<div class="clearfix">&nbsp;</div>
		<?php endif; ?>
			
		<p style="text-align:right;"><a href="/networkapp/index/">Return to list</a></p>
		<div class="box form">
			<div class="box_section">
				<h1>Network App</h1>
			</div>

			<ul class="uiForm full">
		
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Network <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="padding-top: 20px;">
							<select name="network">
								<?php foreach($networks as $network): ?>
									<option value="<?=$network?>" <?=($values['network'] == $network)?'selected="selected"':'';?> class="uiInput<?=(in_array('consumer_id', $this->error_fields)?" error":"")?>"><?=$network?></option>
								<?php endforeach; ?>
							</select>
						</li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Name <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="padding-top: 20px;">
							<input type="text" name="app_name" id="app_name" value="<?=$values['app_name']?>" class="uiInput<?=(in_array('app_name', $this->error_fields)?" error":"")?>" style="width: 500px" />
						</li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Consumer Id <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="padding-top: 20px;">
							<input type="text" name="consumer_id" id="consumer_id" value="<?=$values['consumer_id']?>" class="uiInput<?=(in_array('consumer_id', $this->error_fields)?" error":"")?>" style="width: 500px" />
						</li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Consumer Secret <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="padding-top: 20px;">
							<input type="text" name="consumer_secret" id="consumer_secret" value="<?=$values['consumer_secret']?>" class="uiInput<?=(in_array('consumer_secret', $this->error_fields)?" error":"")?>" style="width: 500px" />
						</li>
					</ul>
				</li>


				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Username <span style="font-style: italic; color: #990000">required for Twitter</span></li>
						<li class="field" style="padding-top: 20px;">
							<input type="text" name="username" id="username" value="<? if(isset($values)) echo $values['username']?>" class="uiInput<?=(in_array('username', $this->error_fields)?" error":"")?>" style="width: 500px" />
						</li>
					</ul>
				</li>

				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">User ID <span style="font-style: italic; color: #990000">required for Twitter</span></li>
						<li class="field" style="padding-top: 20px;">
							<input type="text" name="user_id" id="user_id" value="<? if(isset($values)) echo $values['user_id']?>" class="uiInput<?=(in_array('user_id', $this->error_fields)?" error":"")?>" style="width: 500px" />
						</li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Registration Enabled</li>
						<li class="field" style="padding-top: 20px;">
							<input type="checkbox" name="registration_enabled" id="registration_enabled" <?=($values['registration_enabled'])?'checked="checked"':'';?> />
						</li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Notes</li>
						<li class="field" style="padding-top: 20px;">
							<textarea name="notes" id="notes" style="width:580px;height:200px;"><?=$values['notes']?></textarea>
						</li>
					</ul>
				</li>

			</ul>


		</div>


		<? if(isset($this->app->id) && ($this->app->id)) :?>
			<input type="submit" name="submitted" class="uiButton large right"  value="Save">
		<? else : ?>
			<input type="submit" name="submitted" class="uiButton large right" value="Add">
		<? endif; ?>		
		
	</div>
</div>



</form>



