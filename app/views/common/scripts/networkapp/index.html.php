<div style="padding-top: 30px;">&nbsp;</div>
<div class="container_12 clearfix">
	<div class="grid_12">
		<?php
			if(!empty($this->message)):
			?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
			<?php
			endif;
		?>
	</div>
    <div class="grid_12"><h1>Network Apps <a href="/networkapp/edit/" class="uiButton"  style="float: right; margin-top: 0px; padding-top:0px; margin-right: 0px;">Add App</a></h1></div>

	<div class="grid_12" style="padding-bottom: 80px;">
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<th>Network</th>
				<th>Name</th>
				<th>Consumer Id</th>
				<th>Reg Enabled</th>
				<th>Registered Members</th>
                <th>Actions</th>
			</tr>
			<?php foreach($this->paginator as $app): ?>
			<tr>
				<td><?=$this->escape($app->id)?></td>
				<td><?=$this->escape($app->network)?></td>
				<td><?=$this->escape($app->app_name)?></td>
				<td><?=$this->escape($app->consumer_id)?></td>
				<td><?=($app->registration_enabled)?'Yes':'No'?></td>
				<td style="text-align:right;"><?=$this->escape($app->members_registered)?> / <?=$this->escape($app->members_total)?></td>
                <td style="text-align:right;"><a href="/networkapp/edit/<?=$this->escape($app->id)?>" >Edit</a></td>
			</tr>
			<?php endforeach; ?>
		</table>
		<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />
	</div>
</div>