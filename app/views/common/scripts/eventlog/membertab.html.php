<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		<table style="width: 100%; border-bottom: 0px;">
		<tr>
			<td style="border-bottom: 0px;">
				<h1>
					Events
				</h1>
			</td>
		</tr>
		</table>
		
	
		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Type</th>
				<th>Actor</th>
				<th>Description</th>
				<th>Old Val</th>
				<th>New Val</th>
			</tr>
			<? 

			$row = true;
			$row_class = 'odd';
			
			foreach($this->paginator as $event)
			{
				$row_class = empty($row_class) ? 'odd' : '';
			
?>
			<tr class="<?=$row_class?>">
				<td><?=$this->escape($event->id)?></td>
				<td><?=$event->created_at->format('Y-M-d H:i:s')?></td>
				<td><?=$this->escape($event->entity_type . ':' . $event->event_type)?></td>
				<td>
					<? if ($event->actor_member_id): ?>
					<a href="/admin/member/<?=$this->escape($event->actor_member_id)?>" style="color: #3D6DCC;"><? 
						if(!empty($event->afirst)) echo $this->escape($event->afirst) . " ";
						if(!empty($event->alast)) echo $this->escape($event->alast);
					?></a>
					<? else: ?>
						System
					<? endif; ?>
				</td>
				<td><?=($event->description)?$event->description:'-'?></td>
				<td><textarea style="width: 120px; height: 20px;"><?print_r($event->getOldValue());?></textarea></td>
				<td><textarea style="width: 120px; height: 20px;"><?print_r($event->getNewValue());?></textarea></td>
			</tr>
<?php
			}

		?>
				
		</table>


		<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />


	</div>



</div>