<div class="container_12 clearfix">


	
	<div class="grid_12" style="margin-top: 30px;">
		
		<h1>Member Setting Changes</h1>
		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>ID</th>
				<th>Member ID</th>
				<?/*<th>Photo/Link</th>*/?>
				<th>Name</th>
	
				<th>Publishing Type</th>
				<th>Change To</th>
				<th>Difference</th>
				<th>Date</th>		
			</tr>
		
			<?
				$row = true;
				foreach($this->paginator as $setting)
				{				

					$member = $setting->member;
					echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}
					echo ">\n";

										
					echo "<td>".$setting->id."</td>";
					echo "<td><a href='/admin/member/".$member->id."'>".$setting->member->id."</td>";

					if($member)
					{ 
						//echo '<td><a href="http://www.facebook.com/profile.php?id='.$setting->member->uid.'" target="_new"><img src="'. (empty($setting->member->photo) ? "https://graph.facebook.com/". $setting->member->uid ."/picture" : $setting->member->photo) .'" style="width: 30px; height: 30px;"/></a></td>'."\n";
						echo "<td><a href='/admin/member/".$member->id."'>".$setting->member->first_name." " . $setting->member->last_name."</a></td>\n";
					}
					
					else echo "<td colspan='3'></td>";
					
					echo "<td>".$setting->publish_type."</td>\n";
					echo "<td>".$setting->changed_to."</td>\n";
					
					if(strtoupper($setting->changed_to) == "ON")
					{
						echo "<td style='color: #000000;'>+";
					}
					else
					{
						echo "<td style='color: #ff0000;'>-";	
					}
					if($member)
					{
						switch($setting->publish_type)
						{
							case "FACEBOOK":
								echo $setting->member->facebook_friend_count;
								break;
							case "TWITTER":
								echo $setting->member->twitter_followers;
								break;							
							case "LINKEDIN":
								echo $setting->member->linkedin_friends_count;
								break;						
						}
					}
					echo "</td>";
					
					echo "<td>".$setting->change_date->format('Y-M-d')."</td>\n";
					echo "</tr>";
				}
				
			?>
		</table>
	</div>

	
	<div class="grid_12" style="margin-bottom: 80px; text-align: right;">
		<div style="float: right;">
			<?php echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?>
		</div>
	</div>
	
	
</div>