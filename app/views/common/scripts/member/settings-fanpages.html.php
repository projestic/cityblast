<?php 

$member = isset($_SESSION['member']) && isset($_SESSION['member']->id) ? $_SESSION['member'] : new stdClass();

if(isset($member)):

	//Set up the FACEBOOK VARIABLES...
	$publish_array['access_token'] = $member->access_token;
	$facebook = $member->network_app_facebook->getFacebookAPI();

	if(isset($member->access_token) && !empty($member->access_token)):
		
		$api_url		= "https://graph.facebook.com/me/accounts?access_token=" . $member->access_token;
		
		$accounts = null;
		
		if($account_info = @file_get_contents($api_url)):
		
			$accounts = json_decode($account_info);

			if(!empty($accounts)): ?>
			
				<?/*<ul class="dashboardForm clearfix">*/?>
				
					<?/*<li class="dashboardRowHeader">Fanpage Publish Settings</li>*/?>
						
					<?php //get the access token to post to your page via the graph api ?>
					<?php foreach ($accounts->data as $account): ?>
				
						<?php if($account->category != "Application"): 
						
							try {
								$api_url		= "https://graph.facebook.com/".$account->id."/?access_token=" . $account->access_token;
								$response 		= file_get_contents($api_url);
								$page_info 		= json_decode($response);
							} catch (Exception $e) { ?>
								<li class="dashboardRow clearfix">								
									<ul>
										<li class="dashboardFieldname"><?=$account->name;?></li>
										<li class="dashboardField">
											<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />'<?=$account->name;?>' isn't available right now.</h5>
											<p style="color: #b7b7b7;">We weren't able to retrieve any information about this fanpage from Facebook. This is usually caused by an error on the Facebook servers. Please try again later.</p>
										</li>
									</ul>
								<?php
								continue;							
							}
							
						?>
				
					
							<li class="dashboardRow clearfix">
								
								<ul>
									
									<li class="dashboardFieldname"><?=$page_info->name;?></li>
	
									<?php $fanpage_account_settings = FanPage::find_by_fanpage_id_and_member_id($page_info->id, $_SESSION['member']->id); ?>
									
									<li class="dashboardField">
										<?php if(isset($fanpage_account_settings) && $fanpage_account_settings->active == "yes") : ?>
										
											<div class="clearfix action_items" style="margin-top: 0px; padding-top: 0px; float:right;">
												<a href="#facebookpage-test" rel="facebookpage-test" listingid="134" class="uiButton aux small" style="width: 110px; text-align: left; margin-right: 9px;" title="Test publish to Facebook Page">
													<img style="background-position: -30px -60px" class="uiIcon icon-white icon-small" src="/images/blank.gif">Test publish
												</a>			
											</div>

											<h5 class="dashboardSettingHeading"><img src="/images/blank.gif" class="uiIcon" />'<?=$page_info->name;?>' is working.</h5>
											<p style="color: #b7b7b7;">It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
											business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
										
										<?php else : ?>
										
											<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />'<?=$page_info->name;?>' is Off. No leads.</h5>
											<p style="color: #b7b7b7;">It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
											business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
										
										<?php endif; ?>
										
										<?php if(isset($fanpage_account_settings) && $fanpage_account_settings->active == "yes") : ?>
											
											<a href="/fanpage/off/<?=$page_info->id;?>" class="dashboardSwitch" style="float: right;"><img src="/images/blank.gif" /></a>
											<span style="float: left; line-height: 40px; font-weight: bold; font-size: 11px; opacity: 0.6;"><strong style="color: #cc0000;">Note: </strong>Turning 
												this off means no new leads will be generated.</span>
										<?php else : ?>
			
											<a href="/fanpage/on/<?=$page_info->id;?>" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
											<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
										<?php endif; ?>
									</li>
								</ul>
							</li>
						
						<?php endif; ?>
				
					<?php endforeach; ?>
					
				<?/*</ul>*/?>
				
			<?php endif; ?>
			
		<?php endif; ?>
		
	<?php endif; ?>
	
<?php endif; ?>

<script type="text/javascript">
	
	$(document).ready(function(){

		$('.action_items > a').click(function(event){
			event.preventDefault();
			var href = $(this).attr("rel");
			$.colorbox({
				width:"550px", 
				height: "300px", 
				inline:true, 
				href:"#"+href,
				onClosed:function(){
					var content = $('.dashboardContent')
					content.html('<img src="/images/dashboard_loading.gif" height="250" width="250" style="margin: 80px 230px 0;" />');
					content.load('/member/tab_settings', function(){ $('.zebrad').each(function(){ $(this).zebraTable(); }); });			
				}
			});
		});

		$('#cboxLoadedContent a[rel="open_ajax"]').live('click', function(e){
		    // prevent default behaviour
		    e.preventDefault();

		    var url = $(this).attr('href'); 

		    $.ajax({
		        type: 'GET',
		        url: url,
		        dataType: 'html',
		        cache: true,
		        beforeSend: function() {
		            $('#cboxLoadedContent').empty();
		            $('#cboxLoadingGraphic').show();
		        },
		        complete: function() {
		            $('#cboxLoadingGraphic').hide();
		        },
		        success: function(data) { 
		            $('#cboxLoadedContent').empty();
		            $('#cboxLoadedContent').append(data);
		        }
    			});

		});
	});
</script>