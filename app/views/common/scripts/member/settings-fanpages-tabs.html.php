<? $member = isset($_SESSION['member']) && isset($_SESSION['member']->id) ? $_SESSION['member'] : new stdClass(); ?>
<? if(isset($member)):

	//Set up the FACEBOOK VARIABLES...
	$publish_array['access_token'] = $member->access_token;
	$facebook = $member->network_app_facebook->getFacebookAPI();

	//echo "ACCESS TOKEN:".$member->access_token . "<BR>";
	
	if(isset($member->access_token) && !empty($member->access_token)):
		
		$api_url		= "https://graph.facebook.com/me/accounts?access_token=" . $member->access_token;
		
		$accounts = null;
		
		if($account_info = file_get_contents($api_url)):
		
			$accounts = json_decode($account_info);

			if(!empty($accounts)): ?>
			
						
					<? //get the access token to post to your page via the graph api ?> 
					<? foreach ($accounts->data as $account): ?>
				
						<?
							$page_id = $account->id;
					    
							//found the access token, now we can break out of the loop
							$page_access_token = $account->access_token;
							
							$api_url		= "https://graph.facebook.com/".$page_id."/?access_token=" . $page_access_token;
							$page_info 	= json_decode(file_get_contents($api_url));
						?>
				
						<? if($account->category != "Application"): ?>
					
							
							<li class="dashboardFieldname"><?=$page_info->name;?></li>

							<? $fanpage_account_settings = FanPage::find_by_fanpage_id($page_info->id); ?>
							
							<li class="dashboardField">
								<? if(isset($fanpage_account_settings) && $fanpage_account_settings->active == "yes") : ?>
								
									<div class="clearfix action_items" style="margin-top: 0px; padding-top: 0px; float:right;">
										<a href="#facebookfapage-test" rel="facebookfanpage-test" listingid="134" class="uiButton aux small" style="width: 110px; text-align: left; margin-right: 9px;" title="Test publish to Facebook FanPage">
											<img style="background-position: -30px -60px" class="uiIcon icon-white icon-small" src="/images/blank.gif">Test publish
										</a>			
									</div>

									<h5 class="dashboardSettingHeading"><img src="/images/blank.gif" class="uiIcon" />'<?=$page_info->name;?>' is working.</h5>
									<p style="color: #b7b7b7;">It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
									business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
								
										<?php						
										$lastFanPagePost = $member->getLastPublishedPost('FACEBOOK_FANPAGE',$page_info->id);
										if( $lastFanPagePost ):
										list($member_id,$post_id) = explode("_",$lastFanPagePost->fb_post_id);	
										?>
										<span class="hr">&nbsp;</span>
										<p style='line-height: 20px; font-weight: bold; font-size: 12px; opacity: 0.7;'>You last Facebook FanPage post was published on <?php echo $lastFanPagePost->created_at->format("d-m-Y H:i:s");?>. <a href="http://www.facebook.com/<?php echo $member_id;?>/posts/<?php echo $post_id;?>" target="_blank">View on Facebook</a></p>				
										<?php
										endif; 


								 else : ?>
								
									<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />'<?=$page_info->name;?>' is Off. No leads.</h5>
									<p style="color: #b7b7b7;">It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
									business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
								
								<? endif; ?>
								
								<? if(isset($fanpage_account_settings) && $fanpage_account_settings->active == "yes") : ?>
									<span class="hr">&nbsp;</span>
									<a href="/fanpage/off/<?=$page_info->id;?>" class="dashboardSwitch" style="float: right;"><img src="/images/blank.gif" /></a>
									<span style="float: left; line-height: 40px; font-weight: bold; font-size: 11px; opacity: 0.6;"><strong style="color: #cc0000;">Note: </strong>Turning 
										this off means no new leads will be generated.</span>
								<? else : ?>
								
									<?php if($member->is14DayFreeTrial()) : ?>	
										<h3 style="text-align: left; font-size: 16px; line-height: 25px; margin: 0px; padding-top: 0px;width: 350px;float: left;">
											Activate your full 14 day trial to turn on Fanpage publishing  
										</h3>
										<a href="/member/payment" id="payemnt" class="uiButton red" style='font-size: 12px;float: right;display: block;width: 93px;margin-top: 10px;'>Activate Now</a>
									<?php else:  ?>
										<span class="hr">&nbsp;</span>
										<a href="javascript:void();" onClick="setFanpageOn('<?=$page_info->id;?>');" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
										<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
						
									<?php endif;?>

								<? endif; ?>
							</li>
							
						
						<? endif; ?>
				
					<? endforeach; ?>
					
				
				
			<? endif; ?>
			
		<? endif; ?>
		
	<? endif; ?>
	
<? endif; ?>

<div style="display: none;">
	<div id="facebookfanpage-test">
	    <h2>Publish To Facebook Fan Page</h2>
	    <p>Are you sure you want to publish to your Facebook FanPage?</p>
	    <a rel='open_ajax' href="/member/testpublishtofacebookfanpage/post/1/" class="uiButton right">Test Publish to Facebook FanPage</a>
	</div>
</div>

<script>


function setFanpageOn(id){
	var htmldata = '<div id="payment_failed"><div class="clearfix"><h2>Are you sure you want to turn on this Facepage?</h2><p>You can only have <strong>one Fanpage on at a time</strong>, this action will turn off publishing to all your other Fanpages.</p><p><a href="/fanpage/on/'+id+'" class="uiButton right">Turn on this Fanpage now</a></p></div></div>';
	$.colorbox({width:"650px", height: "250px", html:htmldata});
}


$('.action_items > a').click(function(event){
	event.preventDefault();
	var href = $(this).attr("rel");
	$.colorbox({
		width:"550px", 
		height: "300px", 
		inline:true, 
		href:"#"+href,
		onClosed:function(){
			var content = $('.dashboardContent')
			content.html('<img src="/images/dashboard_loading.gif" height="250" width="250" style="margin: 80px 230px 0;" />');
			content.load('/member/tab_settings', function(){ $('.zebrad').each(function(){ $(this).zebraTable(); }); });			
		}
	});
});

$(document).ready(function(){});

</script>