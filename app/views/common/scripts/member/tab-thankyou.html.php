<h1 class="dashboardSection">&nbsp;&nbsp;You've successfully registered.</h1>

<?/*
<h2>What's next?</h2>
<p>Thanks for joining. Blah, blah, blah. Shaun do you want copy here?...</p>
*/ ?>

<h3 style="color: #808080;">Now customize your Marketing Settings.</h3>
<p>How often do you want us to post?  Do you want <?=COMPANY_NAME;?> posts to go to your Facebook Fanpage, Twitter or LinkedIn?  Or maybe you only want to receive certain types of content, or never post listings?</p>

<p>This is all possible.  Watch this short, helpful video on setting up your publishing preferences.  Once you've got your account customized, your Virtual Assistant will be able to tailor your social media marketing to your personal style and needs.  And best of all?  It only needs to be configured once.</p>
				
<center><iframe width="640" height="480" src="//www.youtube.com/embed/lPLLauHKWSI?rel=0" frameborder="0" allowfullscreen></iframe></center>

<div style="padding-bottom: 5px;">&nbsp;</div>
<a class="uiButton right" style="margin-left: 5px;" href="#" onclick="$('#settingtab').trigger('click');">Change your settings now</a>


<?/*
<div class="clearfix" style="position: relative">

	<div style="float: left; width: 335px; padding-right: 19px; margin-right: 20px; border-right: solid 1px #d3d3d3;">

		<h3 style="color: #808080;">Change your publishing preferences.</h3>
		<p>Do you want CityBlast posts to publish to your Fanpage? Maybe you only want to receive content and not listings. 
			Watch this short, helpful video on <a href="javascript:void(0);" class="help_video_btn">setting up your publishing preferences</a> and get the exact tool you want!</p>

		<?/*<center><div style="width: 275px; height: 170px; background-color: #000000; margin-top: 15px; margin-bottom: 15px;"><a href="javascript:void(0);" class="help_video_btn"><img src="/images/play-help.png"></a></div></center>* ?>
	
		<a class="uiButton right help_video_btn" style="margin-left: 5px;" href="javascript:void(0);">How to change your settings</a>
	
	</div>
	
	<div style="float: left; width: 335px;">

		<h3 style="color: #808080;">Blast your first listing.</h3>
		<p>Are you ready to experience the awesome power of CityBlast for your listing? Reach our entire network for your local market with our Blast technology. 
			Watch this short, helpful video and learn how you can sell your listing faster when you <a href="javascript:void(0);" class="help_video_btn1">Blast your listing</a>.</p>

		<?/*<center><div style="width: 275px; height: 170px; background-color: #000000; margin-top: 15px;"><a href="javascript:void(0);" class="help_video_btn1"><img src="/images/play-help.png"></a></div></center>*?>
	
		<a class="uiButton right help_video_btn" style="margin-left: 5px;" href="javascript:void(0);">Listing your first property</a>
	
	</div>

</div>
*/?>


<div class="clearfix" style="padding-bottom: 25px;">&nbsp;</div>


<? /* CLIENTFINDER VIDEO * ?>
<div style="display: none;">
	
	<div id="help_video_popup" style="width: 640px; height: 480px;">
	    	   		
		<iframe width="640" height="480" src="//www.youtube.com/embed/fs4kv_5a5HY?rel=0" frameborder="0" allowfullscreen></iframe>	

	</div>
	
</div>


<? /* BLAST VIDEO * ?>
<div style="display: none;">
	
	<div id="help_video_popup1" style="width: 640px; height: 480px;">
	    	   		
		<iframe width="640" height="480" src="//www.youtube.com/embed/nOTl5Svnmy0?rel=0" frameborder="0" allowfullscreen></iframe>	

	</div>
	
</div>
***********/ ?>

				




<!-- Google Code for CityBlast /index/how-blasting-works Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = "<?=GOOGLE_CONVERSION_ID;?>";
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "<?=GOOGLE_CONVERSION_LABLE;?>";
var google_conversion_value = "<?=$_SESSION['member']->price;?>";
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/956260748/?value=49&amp;label=IL1qCITeowMQjMP9xwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>



<script type="text/javascript">
var fb_param = {};
fb_param.pixel_id = '6008716178830';
fb_param.value = '0.00';
fb_param.currency = 'CAD';
(function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = '//connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
})();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6008716178830&amp;value=0&amp;currency=CAD" /></noscript>


<!-- Offer Conversion: CityBlast New Member Signup -->
<iframe src="http://cityblast.go2cloud.org/aff_l?offer_id=2&amount=AMOUNT" scrolling="no" frameborder="0" width="1" height="1"></iframe>
<!-- // End Offer Conversion -->

						
<script type="text/javascript">

	$(document).ready(function() 
	{

		$('#help_div_button').click(function(event)
		{
			event.preventDefault();
			$('#help_div').toggle();
		});

		$('.help_video_btn').click(function(event)
		{
			event.preventDefault();
			$.colorbox({width:"720px", height: "570px", inline:true, href:"#help_video_popup"});
		});
		
		$('#help_div_button1').click(function(event)
		{
			event.preventDefault();
			$('#help_div1').toggle();
		});

		$('.help_video_btn1').click(function(event)
		{
			event.preventDefault();
			$.colorbox({width:"720px", height: "570px", inline:true, href:"#help_video_popup1"});
		});	
		
		
		$.ajax({
			url: "hide_blast_thankyou"
		}).done(function() {});
		
	});

</script>