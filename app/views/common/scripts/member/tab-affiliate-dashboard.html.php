<? 
	$affiliate_account = $this->member->affiliate_account; 

	if (isset($affiliate_account) && $affiliate_account->slug)	
	{
		$affiliate_link = "/" . $this->member->affiliate_account->slug;
	}
	else
	{
		$affiliate_link = "/a/i/" . $this->member->id;
	}
?>
<h1 class="dashboardSection">Your Affiliate Links</h1>


<div class="dashboardBox earnFreeMonths">
	<div class="dashboardHeading clearfix">
		<div class="headingIcon"></div>
		<h4>Earn Commission Now With These Handy Links!</h4>
	</div>
	
	<div class="earnToolsContainer">
		<div class="earnMonthsTool">
			<div class="number">1</div>
			<div class="toolDetail">
				<h3>Share Your Unique Referral Link</h3>
				<p>Paste in an email and send it to other professionals in your office, or share it on Facebook.</p>
				<div class="clearfix" style="line-height: 40px;">
					<span class="referral_link_wrapper" style="float: left; line-height: 40px; font-size: 16px; width: 440px; text-align: left;">
						Your Unique Link:&nbsp;&nbsp;<span style="color: #404040; font-weight: bold;" id="referral_link"><?= DOMAIN ?><?=$affiliate_link;?></span>
					</span>
					<a href="javascript:void(0);" class="uiButton" id="copy_btn" style="margin-left: 5px; width: 70px; float: right; font-size: 16px !important;">Copy</a>
				</div>
			</div>
		</div>

		<div class="earnMonthsTool">
			<div class="number">2</div>
			<div class="toolDetail">
				<h3>Use Your Unique Promo Code</h3>
				<p>Paste in an email and send it to other professionals in your office, or share it on Facebook.</p>
				<div class="clearfix" style="line-height: 40px;">
					<?php if ($this->member->affiliate_account && $this->member->affiliate_account->promo) : ?>
					<span class="referral_link_wrapper" style="float: left; line-height: 40px; font-size: 16px; width: 440px; text-align: left;">
						Your Code:&nbsp;&nbsp;<span style="color: #404040; font-weight: bold;" id="referral_link"><?= DOMAIN ?>/promo/code/<?= $this->member->affiliate_account->promo->promo_code ;?></span>
					</span>
					<a href="javascript:void(0);" class="uiButton" id="promo_copy_btn" style="margin-left: 5px; width: 70px; float: right; font-size: 16px !important;">Copy</a>
					<?php else : ?>
						<strong>Your promo code is not set! Please <a href="mailto:<?= HELP_EMAIL ?>">contact us</a> to set your promo code.</strong>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="earnMonthsTool">
			<div class="number">3</div>
			<div class="toolDetail">
				<h3>Tell Your Friends on Facebook</h3>
				<p>Personalize the message below, then click send to post to your Facebook.</p>
				<form action="/index/send-fb-referral" method="POST" id="send_fb_referral">
			
					<div class="clearfix" style="position: relative">
					
						<div class="facebook_preview clearfix">
					
							<div class="facebook_preview_inner clearfix">
					
								<? if(isset($_SESSION['member'])):?>
									<img src="<?php echo empty($_SESSION['member']->photo) ? "https://graph.facebook.com/". $_SESSION['member']->uid ."/picture" : CDN_URL . $_SESSION['member']->photo ?>" class="facebook_userpic" id="facebook_userpic" />
								<? else: ?>
									<img src="/images/blank.gif" style="background-color: #f2f2f2;" class="facebook_userpic" id="facebook_userpic" />
								<? endif; ?>
					
								<div class="facebook_member" id="facebook_member" style="width: 368px;"><?=$_SESSION['member']->first_name;?> <?=$_SESSION['member']->last_name;?></div>
					
								<div class="facebook_message" id="facebook_message" 
									style="background-color: #ffffff; padding: 0px!important; border: none!important; resize: none; width: 368px;">
										
										<? if(stristr(APPLICATION_ENV, "myeventvoice"))	: ?>
											Fellow wedding and event planners - check out this awesome service I discovered that helps you with your social media marketing. It's great!
										<? elseif(stristr(APPLICATION_ENV, "cityblast"))	: ?>
											Fellow real estate agents and brokers - check out this awesome service I discovered that helps you with your social media marketing. It's great!
										<? else : ?>
											Fellow mortgage professionals - check out this awesome service I discovered that helps you with your social media marketing. It's great!
										<? endif; ?>			
								</div>
					
								<div class="facebook_postpic_wrapper" id="facebook_postpic_wrapper" style="margin-top: 3px">
					
									<? if(stristr(APPLICATION_ENV, "myeventvoice"))	: ?>
										<img src="/images/myeventvoice/refer_preview_thumb.jpg" width="90" style="background-color: #f2f2f2;" class="facebook_postpic" id="facebook_postpic" />
									<? else:  ?>
										<img src="/images/refer_preview_thumb.jpg" width="90" style="background-color: #f2f2f2;" class="facebook_postpic" id="facebook_postpic" />
									<? endif; ?>
					
								</div>
					
								<div class="facebook_postinfo clearfix">
					
									<div class="facebook_linktext" id="facebook_linktext"><?=COMPANY_NAME;?>: Hire Your Social Expert Now!</div>
					
									<div class="facebook_baseurl" id="facebook_baseurl"><?=DOMAIN;?><?=$affiliate_link;?></div>
					
									<div class="facebook_description" id="facebook_description"><?=COMPANY_NAME;?> is now offering a 14-Day FREE Trial. Check out what our social experts can do for you!</div>
					
								</div>
					
							</div>
					
						</div>
						<input type="submit" value="Send" class="uiButton right" id="send_id" style="width: 90px; margin-top: -40px; font-size: 16px !important;" />
					</div>
					
					
					
				</form>
			</div>
		</div>


		<? /*****
		<div class="earnMonthsTool">
			<div class="number">4</div>
			<div class="toolDetail">
				<h3>Tell Your Friends through Email</h3>
				<p>Enter your friends' email in the space below and click send. We'll forward them a signup email with your referral link in it.</p>
				<form action="/index/send-referral" method="POST" id="send_referral">
					<input type='hidden' name="redirect_user_back" value='/member/settings#referagenttab'/> 
					<input type="text" class="input email email_input referField uiInput" name="email_friend" id="email_friend" style="width: 450px; float: left;" value="" />
					<input type="submit" value="Send" class="uiButton right" id="send_id" style="width: 90px; font-size: 16px !important;" />
				</form>
			</div>
		</div>
		******/ ?>
	</div>
	
</div>


<script type="text/javascript">

$(document).ready(function(){
		
	$('#copy_btn').click(function(){
		$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3 style='margin-top: -20px;'>Affiliate Link</h3><p>Here is your personal <?=COMPANY_NAME;?> Affiliate Link. Simply triple click the link and CTRL+C to copy it.</p><div class='box black10' style='text-align: center; padding: 10px;'><strong><span style='color: #0c76a7;'>http://<?=DOMAIN;?>/a/i/<?=$_SESSION['member']->id;?></span></strong></div><p>Don't forget to paste your referral link in your email signature and website for further exposure.</p><p style='margin-bottom: 0px;'>Remember, <strong>10 referrals</strong> gets you a <strong>FREE Lifetime Membership!</strong></p>"});		
	});
		
	$("#send_referral").validate({errorElement: "div",
	    errorPlacement: function(error, element) {
        error.insertBefore($("#email_friend"));
     },rules: {
        email_friend: {
			required: true,
            email: true
        }
     },
     messages: {
        email_friend: {
			required: "Please enter a valid email address.",
            email: "Please enter a valid email address."
        }
     }});

});
</script>