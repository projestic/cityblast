<? $member = isset($_SESSION['member']) && isset($_SESSION['member']->id) ? $_SESSION['member'] : new stdClass(); ?>

<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript">
	
	var no_flash = false;

	$(document).ready(function () {

		$('#file_upload').uploadify({
			'uploader'  : '/js/uploadify/uploadify.swf',
			'script'    : '/member/uploadimage',
			'cancelImg' : '/js/uploadify/cancel.png',
			'folder'    : '/images/',
			'auto'      : true,
			'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
			'fileDesc'  : 'Image Files',
			'buttonImg' : '/images/browse_button.png',
			'removeCompleted' : true,
			'height'	: 30,
			'width'		: 106,
			'name'		: 'fileUploaded',

   

			'onComplete': function(event, ID, fileObj, response, data) 
			{
				//alert('RESPONSE: ' +response+' -- DATA:'+fileObj.toSource());
				$('#fileUploaded').val(response);


				//$("#file_uploadQueue").hide();
				//$("#file_uploadStatus").show();
				//$("#file_uploadStatus").html("<span style='display: block; text-align: center;'>Upload Complete</span>");
				//$('#member_photo_progress').css('display', 'none');
				

				$('#member_photo').attr('src', response);
				
				setPhoto(response);
			},

			'onError'	: function (event,ID,fileObj,errorObj) {
				alert(errorObj.type + ' Error: ' + errorObj.info);
			}/*,

			'onProgress'  : function(event,ID,fileObj,data) {
				//$('#member_photo_progress').css('display', 'block');
				//$('#member_photo_progress').css('height', Math.round(data.percentage/100 * 106) + 'px');
				return false;
			}*/

		});

		if(swfobject.hasFlashPlayerVersion('9.0.24') == false)
		{
			no_flash = true;
		}

		function setPhoto(photo) 
		{
			$.ajax({

				type: "POST",
				url: "/member/setphoto",
				data: "photo="+encodeURI(photo),
				dataType: "json",

				success: function(msg)
				{
					if (msg.error) {
						alert("File upload failed!");
					} else {
						$('#photoSetStatus').html('<img src="/images/right.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your photo is set.');
					}
					
					//alert( "About to redirect");
					//window.location =	"/member/signup/both";
				}
			});
		}


	})
</script>

<h1 class="dashboardSection">My Account<a href="<?php echo $this->url( array("controller"=>"member","action"=>"payment"),"default");?>?rebill=1" class="uiButton green pull-right">Update Credit Card</a></h1>

<ul class="dashboardForm clearfix">
				
	<li class="dashboardRowHeader">Account Settings</li>

	<li class="dashboardRow clearfix">
					
		<ul>
			<li class="dashboardFieldname">Member ID</li>
			<li class="dashboardField" style="font-size: 16px;"><?=$this->member->id;?></li>
		</ul>
		
	</li>
					
	<li class="dashboardRow clearfix">
		<ul>
			
			<li class="dashboardFieldname">
				
				Name
				
			</li>
			<li class="dashboardField" style="font-size: 16px;">
			
				<?php if( empty($this->member->first_name) || empty($this->member->last_name) ) : ?>
					<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Your name is incomplete.</h5>
					<div style="padding-left: 21px; font-size: 12px; line-height: 20px;">
						<strong>First Name: </strong><span style="color: #999;"><?=empty($this->member->first_name)?$this->member->first_name:'N/A';?></span><br />
						<strong>Last Name: </strong><span style="color: #999;"><?=empty($this->member->last_name)?$this->member->last_name:'N/A';?></span>
					</div>
				<?php else: ?>
					<?=$this->member->first_name;?> <?=$this->member->last_name;?>

				<? endif; ?>
				

				
				<? /*<a href="<?php echo $this->url( array("controller"=>"member","action"=>"update_name"),"default");?>" class="uiButton pull-right" style="width: 140px;">Change Name</a>*/?>
				
			</li>
		</ul>
		
	</li>
	
	<li class="dashboardRow clearfix">
					
		<ul>
			<li class="dashboardFieldname">
				
				Your Photo
				
			</li>
			<li class="dashboardField">
			

				<div style="font-size: 12px; line-height: 20px;">
				<?php if( !isset($this->member->photo) || empty($this->member->photo) ) : ?>
					<img src="https://graph.facebook.com/<?= $_SESSION['member']->uid ?>/picture" width="96" id="member_photo" style="margin-bottom: 3px; margin-left: 5px;"  />
				<?php else: ?>
					<img src="<?php echo CDN_URL . $this->member->photo ?>" width="96" id="member_photo" style="margin-bottom: 3px; margin-left: 5px;" />
				<? endif; ?>
					<div id="change-photo-upload">
						<input id="file_upload" name="file_upload" type="file" width="88" height="104" />
						<div id="file_uploadStatus" class="uploadstatus"></div>
						<div id="main_photo_progress" class="progress"></div>
						<form name="photo_change" id="photo_change" action="/member/changephoto/" method="POST">
							<input type="hidden" name="fileupload" id="fileupload" value="<?= isset($this->member->photo) && !empty($this->member->photo) ? CDN_URL . $this->member->photo : '' ?>" />
						</form>
					</div>
				</div>
				<a href="javascript:void(0)" class="uiButton pull-right change-photo" style="width: 140px;">Change Photo</a>
			</li>			
		</ul>
	</li>
	

	
	<li class="dashboardRow clearfix">
					
		<ul>
			<li class="dashboardFieldname">
				
				Contact Info
				<span class="fieldhelp">If you'd like other agents to be able to contact you directly, please enter your contact number here.</span>
				
			</li>
			<li class="dashboardField">
			
				<?php if( !isset($this->member->phone) || empty($this->member->phone) ) : ?>
					<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Your contact information is not set.</h5>
				<?php else: ?>

					<div style="font-size: 16px; line-height: 20px;">
						<?php echo $this->member->phone;?>
					</div>
				<? endif; ?>
				

				
				<?/*
				<?php if( !isset($this->member->phone) || empty($this->member->phone) ) : ?>
					<a href="<?php echo $this->url( array("controller"=>"member","action"=>"contact"),"default");?>" class="uiButton green pull-right" style="width: 140px;">Add Contact</a>
				<?php else: ?>
					<a href="<?php echo $this->url( array("controller"=>"member","action"=>"contact"),"default");?>" class="uiButton pull-right" style="width: 140px;">Update Contact</a>
				<? endif; ?>
				*/ ?>
				
			</li>
			
		</ul>
		
	</li>
	
	<li class="dashboardRow clearfix">
					
		<ul>
			<li class="dashboardFieldname">
				
				Current Email
				<span class="fieldhelp">Is this your REAL email? If not, you're not receiving your new client leads from our site!  Please enter an email you check daily.</span>
				
			</li>
			<li class="dashboardField">
			
				<?php if( empty($this->member->email) && empty($this->member->email_override) ) : ?>
					<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Your Email is not set.</h5>
				<?php endif; ?>
												
				<div style="font-size: 16px; line-height: 20px;">
				<?
					if(isset($this->member->email_override) && !empty($this->member->email_override))
					{
						echo "<a href='mailto:".$this->member->email_override."'>".$this->member->email_override."</a>";
					}
					else
					{
						echo "<a href='mailto:".$this->member->email."'>".$this->member->email."</a>";
					}
				?>
				

				
				<?/*
				<?php if( empty($this->member->email) && empty($this->member->email_override) ) : ?>
					<a href="<?php echo $this->url( array("controller"=>"member","action"=>"emailoverride"),"default");?>" class="uiButton green pull-right" style="width: 140px;">Add Email</a>
				<? else: ?>
					<a href="<?php echo $this->url( array("controller"=>"member","action"=>"emailoverride"),"default");?>" class="uiButton pull-right" style="width: 140px;">Update Email</a>
				<? endif; ?>
				*/ ?>
				
			</li>
			
		</ul>
		
	</li>
	
	<?php  if ((strpos(APPLICATION_ENV, 'cityblast') !== false) && in_array($this->member->type->code, array('AGENT', 'BROKER')) ): ?>
	<li class="dashboardRow clearfix">
					
		<ul>
			<li class="dashboardFieldname">
				
				Brokerage Info
				<span class="fieldhelp"><?=COMPANY_NAME;?> requires your Brokerage info in order to ensure your Blasts are compliant with advertising rules in all jurisdictions.</span>
				
			</li>
			<li class="dashboardField">
			
				<?php if( empty($this->member->brokerage) || empty($this->member->broker_address) ) : ?>
					<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Your Brokerage is not set.</h5>

				<?php else: ?>

					<div style="font-size: 16px; line-height: 20px;">
						<?php echo $this->member->brokerage;?><br/>
						<i><?php echo $this->member->broker_address;?></i>
					</div>
				<? endif; ?>
				

				
				<?/*
				<?php if( empty($this->member->brokerage) || empty($this->member->broker_address) ) : ?>
					<a href="<?php echo $this->url( array("controller"=>"member","action"=>"brokerage"),"default");?>" class="uiButton pull-right" style="width: 140px;">Add Brokerage </a>
				<?php else: ?>
					<a href="<?php echo $this->url( array("controller"=>"member","action"=>"brokerage"),"default");?>" class="uiButton pull-right" style="width: 140px;">Update Brokerage</a>
				<? endif; ?>
				*/ ?>
				
			</li>
			
		</ul>
		
	</li>
	<?php endif; ?>
	
</ul>


<p><a href="<?php echo $this->url( array("controller"=>"member","action"=>"updatesettings"),"default");?>" class="uiButton pull-right" style="width: 140px;">Update Settings</a></p>
<div class="clearfix"></div>

<span>&nbsp;</span>
<p><img style="background-position: -90px -60px; vertical-align: middle;" class="uiIcon icon-small" src="/images/blank.gif">I would like to <a href="/member/cancelaccount/<?=$_SESSION['member']->id;?>">cancel my <?=COMPANY_NAME;?></a> account.</span>		


<? /*if($this->member->payment_status != "paid") : ?>

<? endif; */ ?>
		
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.change-photo').click( function () {
			$('#change-photo-upload').show();
			$(this).hide();
		});
		
	}); 
</script>