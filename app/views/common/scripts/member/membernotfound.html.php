<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Oh Dear, There Was a Problem</h1>
		</div>
		

		<p>We looked everywhere but we can't seem to find your <a href="<?php echo $this->url(array('controller'=>'member','action'=>'index'))?>" class="name">registration</a>. 
			Are you sure this isn't your first time visiting with us? No matter, it's a simple fix. 
			Just <a href="<?php echo $this->url(array('controller'=>'member','action'=>'index'))?>" class="name">register now</a> and you'll be on your way!</p>



		<a href="/member/index" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" id="submitBtn">Register Now</a>
		
		<div class="clearfix"></div>
		

	</div>

</div>