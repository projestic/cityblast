<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Success!</h1>
		</div>
			
		<h3>Your <?=COMPANY_NAME;?> account is completely configured. Now watch the magic happen.</h3>
		 
		<p>You can now look forward to attracting new clients through your social media accounts.  Look for your first ClientFinder posts tomorrow.  What now?   </p>

		<p>For one thing, you may wish to set your <a href="/member/settings">publishing frequency</a>.  A higher frequency generally means <a href="/member/settings" style="color: #4D4D4D;">more leads</a>.</p>

		<?/*<p>You should also add <?=COMPANY_WEBSITE;?> to your email program's "<a href="/member/whitelist">safe list</a>".  New client leads are often emailed to your inbox - make sure they don't wind up lost in your filter.</p> */?>
		
		
		<p>Next, you might like to go to: <a href="/member/settings">change your settings</a></strong></p>
		

		<div class="clearfix"></div>	
		
			
		<p style="text-align: right;"><a href="/member/settings" class="btn btn-primary btn-large pull-right" style="width: 195px">Adjust My Settings</a></p>
		
		<div class="clearfix"></div>	
					
	</div>
		

	


</div>

<!-- Google Code for CityBlast /index/how-blasting-works Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 956260748;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "IL1qCITeowMQjMP9xwM";
var google_conversion_value = "<?=$_SESSION['member']->price;?>";
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/956260748/?value=49&amp;label=IL1qCITeowMQjMP9xwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php 
	if (!empty($_COOKIE['hasoffers_offer_id'])) 
	{
		$config = Zend_Registry::get('appConfig');
		if (!empty($config['hasoffers']['tracking_url'])) {
?>
<!-- Offer Conversion: CityBlast New Member Signup -->
<iframe src="<?=$config['hasoffers']['tracking_url']?>/aff_l?offer_id=<?=urlencode($_COOKIE['hasoffers_offer_id'])?>&amount=AMOUNT" scrolling="no" frameborder="0" width="1" height="1"></iframe>
<!-- // End Offer Conversion -->
<?php 
		}
	} 
?>