<?php
if ($this->member && $this->member->network_app_facebook) {
	$network_app_facebook = $this->member->network_app_facebook;
} else {
	$network_app_facebook = Zend_Registry::get('networkAppFacebook');
}
?>
<div class="contentBox">
	
	<div class="contentBoxTitle">
		<h1>Members Only Area.</h1>
	</div>
	
	<p>Hello, before you can continue, you'll need to <a href="#" class="login1">login</a> or <a href="/member/join">register</a>.</p>
	<p>If you're a returning member please <a href="#" class="login1">login</a>. If you're new to the site please <a href="/member/join">register a new account</a> to get started.</p>
	
	
	<a href="#" class="btn btn-primary btn-large pull-right login1" style="width: 195px; margin-top: 10px;">Login Now</a>
	
	<div class="clearfix"></div>
	
</div>
	



<script>
	$(document).ready(function()
	{
		var USE_FB_JS_SDK	=   true;

		if(USE_FB_JS_SDK) {


			// avoid multiple initialization
			if (typeof is_facebook_init == 'undefined' || !is_facebook_init)
			{
				FB.init({
					appId  : '<?=$network_app_facebook->consumer_id;?>',
					status : true, // check login status
					cookie : true, // enable cookies to allow the server to access the session
					xfbml  : true, // parse XFBML
					oauth  : true, // enable OAuth 2.0
					channelUrl: 'http://<?php echo $_SERVER['HTTP_HOST'] ?>/channel.php' // channeling JS SDK
				});

				is_facebook_init	= true;
			}


		    $('.login1').click(function(event){


			    event.preventDefault();
			    FB.login(function(response) {

				    if (response.authResponse)
				    {

					    $.ajax({

						    type: "POST",
						    url: "/member/fblogin",
						    data: "access_token="+response.authResponse.accessToken,
						    dataType: "json",

						    success: function(msg)
						    {
							   window.location =	msg.redirectBackTo;
						    }
					    });

				    }

			    }, {scope: '<?php echo FB_SCOPE;?>'});

		    });

		    $('.register1').click(function(event){

			    	event.preventDefault();			    											
				
				
				//alert('What is my City?'+$("#city").val());
				var city_id	= $("#city").val();
				if (city_id == null || city_id == '' || city_id == '*') 
				{
					window.location	= "/member/blastcity";
					return false;
				}
				else
				{
				
				    	FB.login(function(response) {
	
					    if (response.authResponse)
					    {
						    // console.log(response.authResponse);
						    $.ajax({
	
							    type: "POST",
							    url: "/member/fbsignup",
							    data: "access_token="+response.authResponse.accessToken+"&href="+$('#facebook-signup').attr('href'),
							    dataType: "json",
	
							    success: function(msg)
							    {						    								    	
									window.location =	msg.redirectBackTo;
							    }
						    });
	
					    }
	
				    }, {scope: '<?php echo FB_SCOPE;?>'});
				}


		    });

		    $('#facebook-logout').click(function(event){


				event.preventDefault();
				FB.logout(function(response)
				{
				    $.ajax({

						    type: "POST",
						    url: "/member/fblogout",
						    data: "",
						    dataType: "json",

						    success: function(msg)
						    {
							    window.location = "/";
						    }
					    });

				});

		    });

		}

	});
</script>