<? if (!empty($this->tag_groups)): ?>
<ul class="pull-left unstyled settings">
	<? foreach ($this->tag_groups as $tag_group): ?>
	<li class="pull-left">
		<h5><?=$this->escape($tag_group['group']->name)?>.</h5>
		<p><?=$this->escape($tag_group['group']->slogan)?></p>
		<ul class="unstyled">
		<? foreach ($tag_group['tags'] as $tag): ?>
					
			<li>
								
				<input class="content_pref" id="tag_<?=$this->escape($tag->id)?>" type="checkbox" name="tag_ids[]" value="<?=$this->escape($tag->id)?>"<?=!empty($tag->default_value) ? ' checked="checked" ' : ''?> />

				<a href="/content/sample/<?=$this->escape($tag->id)?>" id="sample_<?=$this->escape($tag->id)?>" tag_id="<?=$this->escape($tag->id)?>" class="samplecontent">
					<img src="/images/blank.gif" height="14" width="14" style="border: 0px;" />
					<span>View Sample</span>
				</a>

				<label for="tag_<?=$this->escape($tag->id)?>"><?=$this->escape($tag->name)?></label>
					
			</li>
			
							
		<? endforeach; ?>
		</ul>
	</li>
	<? endforeach; ?>
</ul>
<? endif; ?>