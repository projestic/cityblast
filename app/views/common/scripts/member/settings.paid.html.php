<? $member = isset($_SESSION['member']) && isset($_SESSION['member']->id) ? $_SESSION['member'] : new stdClass(); ?>
<h1 class="dashboardSection">Publishing Settings</h1>

<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
<p style="font-style: italic"><strong>Confused?</strong>&nbsp;&nbsp;Watch this helpful video:&nbsp;&nbsp;<a href="javascript:void(0);" class="help_video_btn">How to manage your settings</a>.</p>
<? endif; ?>


<?
// The content pref's / tag options will not be made visible until the content pubishing process has been updated
// to make use of the preferences set here
?>

<ul class="dashboardForm contentSettings clearfix">
	<li class="dashboardRowHeader">Content Settings<span class="notification">New</span></li>
	<li class="dashboardRow clearfix">
		<p id="content_prefs_error" style="color: red; font-weight: italic; margin: 10px auto; width: 100%; text-align: center;"></p>
		<form id="content_prefs_form" name="content_prefs_form" action="/member/updatecontentprefs/" method="POST">
			<p style="padding-bottom: 18px;">Choose exactly the type of content you'd like to see</p>
			<? if (!empty($this->tag_groups)): ?>
			<ul class="settings clearfix">
				<? foreach ($this->tag_groups as $tag_group): ?>
				<li>
					<h4><?=$this->escape($tag_group['group']->name)?>.</h4>
					<p><?=$this->escape($tag_group['group']->slogan)?></p>
					<? if (!empty($tag_group['tags'])): ?>
						
						<? foreach ($tag_group['tags'] as $tag): ?>
							<div>

								<input class="content_pref" id="tag_<?=$this->escape($tag->id)?>" type="checkbox" name="tag_ids[]" value="<?=$this->escape($tag->id)?>"<?=!empty($tag->pref) ? 'checked="checked"' : ''?> />
								<label for="tag_<?=$this->escape($tag->id)?>"><?=$this->escape($tag->name)?></label>
								<? if ($tag->publishable): ?>
								<a href="/content/sample/<?=$this->escape($tag->id)?>" id="sample_<?=$this->escape($tag->id)?>" tag_id="<?=$this->escape($tag->id)?>" class="samplecontent"><img src="/images/blank.gif" height="14" width="14" style="border: 0px;" /></a>
								<? endif; ?>
							</div>
						<? endforeach; ?>
					<? endif; ?>
				</li>
				<? endforeach; ?>
			</ul>
			<? endif; ?>
			<div class="clearfix">
				<input type="submit" class="uiButton" value="Update" />
			</div>
		</form>
	</li>
	

	

	<?php if(!$this->hasPhotoPermission): ?>	
	<li class="dashboardRow clearfix">

		<p style="padding: 20px;">We do not have permission to manage your photos. <a href="#" id="photo_permissions">Yes, allow <?=COMPANY_NAME;?> to publish photos to my wall</a>.</p>
	
	</li>
	<? endif; ?>

	
</ul>


<ul class="dashboardForm clearfix">
	
	<li class="dashboardRowHeader">Marketing Settings</li>

    <form name="frequency_change" id="frequency_change" action="/member/changemarketingsettings/" method="POST">
		<input id="selected_city" name="selected_city" type="hidden" value="">
		<input id="selected_state" name="selected_state" type="hidden" value="">
		<input id="selected_country" name="selected_country" type="hidden" value="">
		<li class="dashboardRow clearfix">
			<ul>
				<li class="dashboardFieldname">
					Publishing City
					<span class="fieldhelp">Choose where to focus <?=COMPANY_NAME;?>'s marketing efforts.</span>
				</li>
				<li class="dashboardField">
					<div style="overflow: hidden; margin-top: 15px;">
						<ul class="uiForm full">
							<li class="clearfix">
								<ul>
									<li class="field cc clearfix">
										<input id="defaultpubcity" name="location_input" type="text" class="geocomplete uiInput"/><br/>
									</li>
								</ul>
							</li>
							<li class="clearfix">
								<div class="map_canvas"></div>
							</li>
							<li class="uiFormRow clearfix">
								<ul class="clearfix">
									<li class="clearfix">
										<hr />
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</li>
			</ul>
		</li>

		<li class="dashboardRow clearfix">
			<ul>
				<li class="dashboardFieldname">
					Post Frequency
					<span class="fieldhelp"><?=COMPANY_NAME;?> publishes new posts to your Wall or Feed as often as you choose.  A higher posting frequency attracts more new leads.</span>
				</li>
				<li class="dashboardField clearfix">
					<?= $this->render('member/frequency-slider.html.php'); ?>
				</li>
			</ul>
		</li>

		<li class="dashboardRow clearfix">
			<ul>
				<li class="dashboardFieldname">
					Auto-like posts
					<span class="fieldhelp">Choose whether or not to auto-like the posts we make to your Facebook account.</span>
				</li>
				<li class="dashboardField">
					<ul class="settings">
						<li style="width: 400px">
							<div <?php if ($this->member->auto_like) : ?>class="selected"<?php endif; ?>>
								<input type="checkbox" name="auto-like" id="auto-like" value="1" <?php if ($this->member->auto_like) : ?>checked="checked"<?php endif; ?> />
								<label for="auto-like">Yes, I want to maximize my engagement &mdash; auto-like my posts to Facebook!</label>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</li>

		<li class="dashboardRow clearfix">
			<ul>
				<li class="dashboardFieldname">
					Click tracking
					<span class="fieldhelp">We normally track <?=COMPANY_NAME;?> posts to your Facebook account. If you'd prefer to not have your posted links go to <?= DOMAIN ?>, you change this by disabling click tracking.</span>
				</li>
				<li class="dashboardField">
					<ul class="settings">
						<li style="width: 400px">
							<div <?php if ($this->member->click_tracking) : ?>class="selected"<?php endif; ?>>
								<input type="checkbox" name="click-tracking" id="click-tracking" value="1" <?php if ($this->member->click_tracking) : ?>checked="checked"<?php endif; ?> />
								<label for="click-tracking">Yes, I want to track clicks on the posts CityBlast makes to my social media accounts</label>
							</div>
						</li>
					</ul>
					<div class="clearfix"></div>
						<input type="submit" class="uiButton" value="Update" />
					</div>
				</li>
			</ul>
		</li>


	</form>
	
</ul>


<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
<p style="text-align: right; font-style: italic; margin-bottom: 30px;">Do you still need <a href="#" class="help_video_btn" style="font-weight: bold;">more help</a>?</p>
<? endif; ?>


<?

/***************************************************************
//TABS THAT NEED TO BE TESTED!!!
****************************************************************/
?>

<div id="tabcontainer">
	<? echo $this->render('member/tab-dashboard-socialmedia-tabs.html.php'); ?>
</div>


<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
<p style="text-align: right; font-style: italic; margin-bottom: 30px;">Do you still need <a href="#" class="help_video_btn" style="font-weight: bold;">more help</a>?</p>
<? endif; ?>



<?/****
<p style="text-align: right; font-style: italic; margin-bottom: 30px;">Have a question? Check out our <a href="#" id="help_div_button" style="font-weight: bold;">members helping members</a> section.</p>


<div style="display: none;" id="help_div">
		
	<div class="clearfix" style="min-height: 161px; padding-left: 8px; background-color: #FFFFFF;">		
		<h3 style="color: #808080; ">Members helping members.</h3>
		<fb:comments xid="member_clientfinder_help" href="/member/settings" width='640px'></fb:comments>
	</div>

</div>
*****/ ?>
		
<div style="display: none;">
	<div id="facebook-test" class="dashboardContent">
	    <h2>Publish To Facebook</h2>
	    <p>Are you sure you want to publish to your personal Facebook account?</p>
	    <a rel='open_ajax' href="/member/testpublishtofacebook/post/1/" class="uiButton right">Test Publish to Facebook</a>
	</div>
</div>

<div style="display: none;">
	<div id="facebookpage-test" class="dashboardContent">
	    <h2>Publish To FanPage</h2>
	    <p>Are you sure you want to publish to your Facebook FanPage?</p>
	    <a rel='open_ajax' href="/member/testpublishtofacebookfanpage/post/1/" class="uiButton right">Test Publish to FanPage Page</a>
	</div>
</div>

<div style="display: none;" >
	<div id="twitter-test" class="dashboardContent">
	    <h2>Publish To Twitter</h2>
	    <p>Are you sure you want to publish to your Twitter account?</p>
	    <a rel='open_ajax' href="/member/testpublishtotwitter/post/1/" class="uiButton right">Test Publish to Twitter</a>
	</div>
</div>

<div style="display: none;">
	<div id="linkedin-test" class="dashboardContent">
	    <h2>Publish To Linkedin</h2>
	    <p>Are you sure you want to publish to your Linkedin account?</p>
	    <a rel='open_ajax' href="/member/testpublishtolinkedin/post/1/" class="uiButton right">Test Publish to Linkedin</a>
	</div>
</div>
		

<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
<div style="display: none;">
	
	<div id="help_video_popup" style="width: 640px; height: 480px;">
	    	   		
		<iframe width="640" height="480" src="//www.youtube.com/embed/lPLLauHKWSI?rel=0" frameborder="0" allowfullscreen></iframe>	

	</div>
	
</div>
<? endif; ?>


<div style="display: none;">
	
	<div id="sample_popup" style="width: 640px; height: 490px;"></div>
	
</div>




		
<script type="text/javascript">
	
	$(document).ready(function()
	{
		$('#content_prefs_form').submit(function(event){
			var errorContainer = $('#content_prefs_error');
			errorContainer.hide();
			errorContainer.text('');
			if ($('.content_pref:checked').length < 5) {
				errorContainer.text('You must select at least 5 types of content');
				errorContainer.show();
				return false;
			}
			return true;
		});

		/***********
		$('#inline_view').hide();
		$('#change_to_inline').click(function(event)
		{
			$('#tabbed_view').hide();
			$('#inline_view').show();
		}); *******/
		
		$('#help_div_button').click(function(event)
		{
			event.preventDefault();
			$('#help_div').toggle();
		});

		$('.help_video_btn').click(function(event)
		{
			event.preventDefault();
			$.colorbox({width:"720px", height: "570px", inline:true, href:"#help_video_popup"});
		});
				

		$('.samplecontent').click(function(event)
		{
			event.preventDefault();
			
			var tag_id = $(this).attr("tag_id");
		    	var url = $(this).attr('href'); 

		    	$.ajax({
				type: 'GET',
				url: url,
				dataType: 'html',
				
				success: function(data) { 
					$('#sample_popup').empty();
					$('#sample_popup').append(data);
				}
    			});
		   
		     

			$.colorbox({height: "660px",inline:true, href:"#sample_popup",onClosed:function(){ $('#sample_popup').empty(); }});
		});
		

		$('.action_items > a').click(function(event){
			event.preventDefault();
			var href = $(this).attr("rel");
			$.colorbox({
				width:"550px", 
				height: "300px", 
				inline:true, 
				href:"#"+href,
				onClosed:function(){
					var content = $('.dashboardContent')
					content.html('<img src="/images/dashboard_loading.gif" height="250" width="250" style="margin: 80px 230px 0;" />');
					content.load('/member/tab_settings', function(){ $('.zebrad').each(function(){ $(this).zebraTable(); }); });			
				}
			});
		});
		
		$('ul.settings input[type="checkbox"]').change(function(){
			var $this = $(this);
			var $Parent = $this.parent();
			if($this.prop('checked') || $this.attr('checked') == 'checked')
			{				
				$Parent.addClass('selected');
			}
			else{
				$Parent.removeClass('selected');
			}
		}).change();
		
		$('#cboxLoadedContent a[rel="open_ajax"]').live('click', function(e){
		    // prevent default behaviour
		    e.preventDefault();

		    var url = $(this).attr('href'); 

		    $.ajax({
		        type: 'GET',
		        url: url,
		        dataType: 'html',
		        cache: true,
		        beforeSend: function() {
		            $('#cboxLoadedContent').empty();
		            $('#cboxLoadingGraphic').show();
		        },
		        complete: function() {
		            $('#cboxLoadingGraphic').hide();
		        },
		        success: function(data) { 
		            $('#cboxLoadedContent').empty();
		            $('#cboxLoadedContent').append(data);
		        }
    	});
});



<?php if(!$this->hasFacebookPublishPermissions): ?>
  
	
  	$('a[id^="facebook_permissions"]').click(function(event){

		event.preventDefault();

		
 		FB.login(function(response) {

                if (response.authResponse)
                {

                	var newAccessToken = response.authResponse.accessToken;
                	FB.api('/me/permissions', function (response) {
					var perms = response.data[0];

    		        	if (perms.publish_stream) {   
            		    	// User has permission
							$.post("/member/update_access_token", { access_token: newAccessToken}, function(data) {
								// Ignoring the result	
							 });
  							var content = $('.dashboardContent')
							content.html('<img src="/images/dashboard_loading.gif" height="250" width="250" style="margin: 80px 230px 0;" />');
							content.load('/member/tab_settings', function(){ $('.zebrad').each(function(){ $(this).zebraTable(); }); });	
		
            			} else {                
                			// User DOESN'T have permission
                			alert("You refused the publish permissions. Your account will stay inactive!");
            			}                                            
					} );        	
		                    	
                }
		
            }, {scope: '<?php echo FB_SCOPE;?>'});
	});

<?php endif;?>



<?php if(!$this->hasPhotoPermission): ?>

	$('a[id^="photo_permissions"]').click(function(event){

		event.preventDefault();
		$.colorbox.close();
		
		
 		FB.login(function(response) 
 		{

          	if (response.authResponse)
               {

	
                	var newAccessToken = response.authResponse.accessToken;
                	FB.api('/me/permissions', function (response) 
                	{
					var perms = response.data[0];

    		        		if (perms.user_photos) 
    		        		{   
    		        			$.post("/member/update_access_token", { access_token: newAccessToken } );
    		        			
						//$("#photo_permissions_div").hide();
						
												
						$.colorbox({width:"550px", height: "220px", html:"<h1 style='margin-top: 0px; padding-top: 0px;'><img src='/images/checkmark1.png' style='width: 30px; margin: 4px 10px 0 0;' />Success!</h1><p style='font-size: 130%; width: 480px;'>You have successfully given us permission to publish large images to your Facebook account</p>"});
						
            			} else {                
                			
         					$.colorbox({width:"550px", height: "300px", html:"<h1 style='margin-top: 0px; padding-top: 0px;'>Error!</h1><p style='font-size: 130%;'>You did not approve the Facebook Photo's permission. Unfortunately we will not be able to upload large photos to your account!</p>"});
     			
            			}                                            
				});        	
				

		                    	
                }
		
            }, {scope: '<?php echo FB_SCOPE;?>'});
	});



<? endif; ?>


<?php if(!$this->hasManagePagesPermission): ?>
  $('a[id^="fanpages_permissions"]').click(function(event){

            event.preventDefault();

 			FB.login(function(response) {

                if (response.authResponse)
                {

                	var newAccessToken = response.authResponse.accessToken;
                	FB.api('/me/permissions', function (response) {
					var perms = response.data[0];

    		        	if (perms.manage_pages) {   
            		    	
            		    		// User has permission
						$.post("/member/update_access_token", { access_token: newAccessToken}, function(data) {
								// Ignoring the result	
							 });
  							var content = $('.dashboardContent')
							content.html('<img src="/images/dashboard_loading.gif" height="250" width="250" style="margin: 80px 230px 0;" />');
							content.load('/member/tab_settings', function(){ $('.zebrad').each(function(){ $(this).zebraTable(); }); });	
							
						$.colorbox({width:"500px", height: "200px", html:"<h1 style='margin-top: 0px; padding-top: 0px;'><img src='/images/checkmark1.png' style='width: 30px; margin: 4px 10px 0 0;' />Success!</h1><p style='font-size: 130%; width: 480px;'>You have successfully given us permission to manage your Facebook Fanpages</p>"});
													
		
            			} else {                
                			
                			// User DOESN'T have permission
                			//alert("You refused the manage pages permissions. Your facebook pages will stay inactive!");
         					$.colorbox({width:"550px", height: "300px", html:"<h1 style='margin-top: 0px; padding-top: 0px;'>Error!</h1><p style='font-size: 130%;'>You did not approve the Facebook Manage Fanpages permission. Unfortunately we will not be able to manage your Fanpages at this time!</p>"});
     			                			
            			}                                            
					} );        	
		                    	
                }
		
            }, {scope: '<?php echo FB_SCOPE;?>'});
	});

<?php endif;?>

	});

	$('.callthroughajax').click(function(event) {
		   event.preventDefault();
		   var href = $(this).attr("href");
		   $(this).removeClass('dashboardSwitch');
		   $(this).html("Please wait...");
		   $.post(href, {'stop_redirect' : '1'}, function(data) {
			    var content = $('.dashboardContent')
				content.html('<img src="/images/dashboard_loading.gif" height="250" width="250" style="margin: 80px 230px 0;" />');
				content.load('/member/tab_settings', function(){ $('.zebrad').each(function(){ $(this).zebraTable(); }); });	
		   });
		});
			
</script>

<style>

	.map_canvas:after
	{
		color: #999999;
		/*content: "Type in an address in the input above.";*/
		display: block;
		font-size: 2em;
		padding-top: 170px;
		text-align: center;
	}
	.map_canvas {
		border: 1px solid #e7e7e7;
		height: 200px;
		margin: 10px 0;
		width: 100%;
		background-color: #f1f1f1 !important;
		overflow: hidden;
		position: relative;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
	}
	.uiForm .field, .uiForm .fieldname {
		padding-left: 0px!important;
	}

</style>

<script>
/*
* $('#defaultpubcity').change( function (e) {
 $('#city_change').submit();
 });*/
	$(function(){
		<?php
			if (isset($this->member)) {
				if ($this->member->default_city) {
					$city    = $this->member->default_city->name;
					$state   = $this->member->default_city->state;
					$country = $this->member->default_city->country;
				} elseif ($this->member->publish_city)
				{
					$city    = $this->member->publish_city->name;
					$state   = $this->member->publish_city->state;
					$country = $this->member->publish_city->country;
				}
			}
		?>
		var city 		= "<?php echo $city; ?>";
		var region 		= "<?php echo $state; ?>";
		var country_code 	= "<?php echo $country; ?>";
		var city_initialized = false;
		


		google.load("search", "1.x", {callback: initialize});

		function initialize()
		{
			
			
			$("#selected_city").val(city);
			$("#selected_state").val(region);
			$("#selected_country").val(country_code);

			var options = {
				map: ".map_canvas"
			};
			console.log(city+region+country_code);

			$(".geocomplete").geocomplete({
				map: ".map_canvas",
				details: "form",
				location: city + ", " + region + ", " + country_code
			});

			$(".geocomplete")
				.geocomplete(options)
				.bind("geocode:result", function(event, result)
				{

					var geocode_country = '';
					var geocode_state   = '';
					var geocode_city    = '';

					$.each(result.address_components, function (ix, item) {
						if ($.inArray('locality', item.types) > -1) {
							geocode_city = item.long_name;
						} else if ($.inArray('administrative_area_level_1', item.types) > -1) {
							geocode_state = item.long_name;
						} else if ($.inArray('country', item.types) > -1) {
							geocode_country = item.short_name;
						}
					});
					
					if (city_initialized) {
						if((geocode_city != "" && geocode_city != city) || (geocode_state != "" && geocode_state != region)) {
							$("#selected_city").val(geocode_city);
							$("#selected_state").val(geocode_state);
							$("#selected_country").val(geocode_country);
							
							console.log([geocode_city, geocode_state, geocode_country]);
						}
					} else {
						$("#defaultpubcity").val(city);
						city_initialized = true;
					}
				})
				.bind("geocode:multiple", function(event, status){
					console.log('multiple');
				})
		}

	});

</script>