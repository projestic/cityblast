<?php if (!$this->search) : ?>
<div class="dashboardSection clearfix">
	<h1 style="float: left; margin: 0; line-height: 40px; font-size: 30px; font-weight: 600;">My Listings (<?php echo $this->count;?>)</h1>
	<a href="/blast" class="uiButton green pull-right">Create a Listing</a>
</div>

<div>
	<form class="searchform">
		<input type="text" id="listing-search" class="uiInput" name="search" placeholder="Search listings" style="width: 350px"/>
		<div class="pull-right">
			Show
			<select name="status" id="search-status">
				<option value="all">All properties</option>
				<option value="active" selected="selected">Active properties</option>
				<option value="sold">Sold properties</option>
				<option value="offmarket">Off-market properties</option>
				<option value="pricechanges">Price changes</option>
			</select>
		</div>
	</form>
</div>
<?php endif; ?>

<?php if ($this->count || $this->search) : ?>

	<?php if ($this->count == 0) : ?>
	<div class="listingItemContainer">
		<h5 style="margin-left: 10px; font-weight: 600;">No listings found.</h5>
	</div>
	<?php else : ?>

	<ul class="listingItemContainer clearfix">
		
		<?php foreach($this->paginator as $listing) : ?>
			
				<li class="listingItem">
					<div class="listingItemHeader clearfix">
						<div class="left">
							ID: <strong><a href="/listing/view/<?=$listing->id?>"><?=$listing->id?></a></strong>
						</div>
						<div class="right clearfix">
							<div class="status">

								<? if($listing->property_status == "EXPIRED") : ?><span class="status offTheMarket">Off The Market</span>
								<? elseif($listing->property_status == "SOLD") : ?><span class="status sold">Sold</span>
								<? elseif($listing->property_status == "PRICE_CHANGE") : ?><span class="status newPrice">New Price</span>
								<? else : ?><span class="status listing">Listing</span>
								<? endif; ?>
																																								
							</div>
							<div class="links">
								<?php 
                                    $property_status = strtolower($listing->property_status);
                                    if('sold'!=$property_status && 'expired'!=$property_status && (int)$listing->reblast_id==0):
								?>
                                <?php if ($listing->isPublishingQueueExists()): ?>
								    <a href='#' rel="blast-it" data-listing-id="<?= $listing->id; ?>" class="blastIt" title="Blast It!">Blast It!</a>
                                <?php endif; ?>
								<a href='#' rel="mark-sold" listingid="<?=$listing->id?>" class="markSold™" title="Mark Sold">
									Mark Sold
								</a>
								<a href='#' rel="off-the-market" listingid="<?=$listing->id?>" class="offMarket" title="Off The Market">
									Off Market
								</a>
								<a href='#' rel="price-change" listingid="<?=$listing->id?>" class="priceChange" title="Price Change">
									Price Change
								</a>
	
								<?php if($listing->reblastPermitted()): ?>
								<a href='#' rel="reblast" listingid="<?=$listing->id?>" class="reblast" title="Reblast">
									Reblast
								</a>
								<?php endif;?>
								
								<?php elseif((int)$listing->reblast_id==0): ?>
								
								<a href='#' rel="relist" listingid="<?=$listing->id?>" class="relist" title="Relist">
									Relist
								</a>
							
								<?php 
								else:
									// empty
								endif; ?>
							</div>
							<div style="display: none;">
								<div id="mark-sold-<?=$listing->id?>">
									<h2>Congratulations!</h2>
									<p>Great job on selling this property. Mark this property as sold and we'll immediately stop publishing your listing.</p>
									<a href="/listing/listingstatus/status/sold/id/<?=$listing->id?>" class="uiButton right">Sold</a>
								</div>
							</div>
							
							<div style="display: none;">
								<div id="off-the-market-<?=$listing->id?>">
									<h2>Off the Market</h2>
									<p>Have you taken this listing off the market? We want to keep your listings up to date. Simply click the 'off the market' button below and we'll remove this listing.</p>
									<a href="/listing/listingstatus/status/expired/id/<?=$listing->id?>" class="uiButton right">Off the Market</a>
								</div>
							</div>
							
							<div style="display: none; height: 400px;">
								<div id="price-change-<?=$listing->id?>">
									<h2 style="margin-top: 0px;">New Price</h2>
									<p>Have you recently changed the price for this listing? Changing the price is quick and easy. Just fill in the form below:</p>
									<p><b>New price:&nbsp;</b> <input type="text" name="new_price_<?=$listing->id?>" id="new_price_<?=$listing->id?>" class="uiInput" style="float:none; width: 350px;"/></p>
									<br>
									<a href="#" onClick="setNewPrice('<?=$listing->id?>');" class="uiButton right">Change the Price Now</a>

								</div>
							</div>
							<div style="display: none; height: 400px;">
								<div id="reblast-<?=$listing->id?>">
									<h2 style="margin-top: 0px;">Reblast</h2>
									<p>Do you want to reblast this listing? Making this listing available is easy. Simply click the 'Reblast' button below and we'll reblast this listing.</p>
									<br>
									<a href="/listing/reblast/id/<?=$listing->id?>"  class="uiButton right">Reblast</a>

								</div>
							</div>
							<div style="display: none; height: 300px;">
								<div id="relist-<?=$listing->id?>" style="margin:10px">
									<h1>Relist</h1>
									<p style="margin-top:20px">Do you want to relist this listing? Making this listing available is easy. Simply click the 'Relist' button below and we'll relist this listing.</p>
									<br>
									<a href="/listing/relist/id/<?=$listing->id?>" class="uiButtonNew popup right">Relist</a>
								</div>
							</div>
						</div>
					</div>
					<div class="listingItemInfo clearfix">
						<div class="left">
							<div class="listingImg">
								<span class="listingImgArrow"></span>
								<a href="/listing/view/<?=$listing->id?>"><img src="<?=$listing->getSizedImage(76, 76) ?>" width="50" height="50" border="0" /></a>
							</div>
						</div>
						<div class="right clearfix">
							<div class="infobox">
								<? if($listing->property_status == "EXPIRED") : ?><h4 style="margin-top:0px; padding-top: 0px;">*Off The Market</h4><? endif; ?>
								<? if($listing->property_status == "SOLD") : ?><h4 style="margin-top:0px; padding-top: 0px;">*Sold</h4><? endif; ?>
								<? if($listing->property_status == "PRICE_CHANGE") : ?><h4 style="margin-top:0px; padding-top: 0px;">*New Price</h4><? endif; ?>
								<div class="address">
									<?=$listing->street?>, <?=$listing->city?>
								</div>
								<div class="listedOn">
									<?=$listing->created_at->format("l, F jS, Y");?>
								</div>
							</div>

							<?
								//Total Impressions
								$query = "select SUM(reach) as total_impressions from post where listing_id=".$listing->id;
								//echo $query . "<BR>";
								$impressions = Listing::find_by_sql($query);
							?>
							
							<div class="statbox">
								<div class="clicked">
									<div>Clicked By</div>
									<h2><?=number_format($listing->total_clicks)?></h2>
								</div>
								<div class="seen">
									<div>Seen By</div>
									<h2><?=number_format($impressions[0]->total_impressions)?></h2>
								</div>
							</div>
						</div>
					</div>
				</li>
				
		<?php endforeach; ?>
		<li><div style="float: right;"><?php $params = array(); if (isset($this->isAjax)) $params['isAjax'] = $this->isAjax; echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $params); ?></div></li>
	</ul>
	<?php endif; ?>


	<? /*
		<?php 
			
			//CHANGE PROPERTY STATUS POPUPS ****************************************************************************************8	
			
			$property_status = strtolower($listing->property_status);
			if('sold'!=$property_status && 'expired'!=$property_status && (int)$listing->reblast_id==0):
		?>
		<div class="clearfix action_items" style="margin-top: 10px; padding-top: 10px; border-top: solid 1px #e5e5e5;">
			<a href='#' rel="mark-sold" listingid="<?=$listing->id?>" class="uiButton aux small" style="width: 70px; text-align: left; margin-right: 5px; padding-left: 8px" title="Mark Sold">
				Mark Sold
			</a>
			<a href='#' rel="off-the-market" listingid="<?=$listing->id?>" class="uiButton aux small" style="width: 70px; text-align: left; margin-right: 5px; padding-left: 6px" title="Off The Market">
				Off Market
			</a>
			<a href='#' rel="price-change" listingid="<?=$listing->id?>" class="uiButton aux small" style="width: 90px; text-align: left; margin-right: 5px; padding-left: 6px" title="Price Change">
				Price Change
			</a>

			<?php if($listing->created_at->format("Y-m-d") <= date("Y-m-d", strtotime("-7days"))): ?>
				<a href='#' rel="reblast" listingid="<?=$listing->id?>" class="uiButton aux small" style="width: 55px; text-align: left; margin-right: 5px; padding-left: 6px;" title="Reblast">
					Reblast
				</a>
			<?php endif;?>
		</div> 

		<div style="display: none;">
			<div id="mark-sold-<?=$listing->id?>">
				<h2>Congratulations!</h2>
				<p>Great job on selling this property. Mark this property as sold and we'll immediately stop publishing your listing.</p>
				<a href="/listing/listingstatus/status/sold/id/<?=$listing->id?>" class="uiButton right">Sold</a>
			</div>
		</div>
		
		<div style="display: none;">
			<div id="off-the-market-<?=$listing->id?>">
			    <h2>Off the Market</h2>
			    <p>Have you taken this listing off the market? We want to keep your listings up to date. Simply click the 'off the market' button below and we'll remove this listing.</p>
			    <a href="/listing/listingstatus/status/expired/id/<?=$listing->id?>" class="uiButton right">Off the Market</a>
			</div>
		</div>
		
		<div style="display: none; height: 400px;">
			<div id="price-change-<?=$listing->id?>">
			    <h2 style="margin-top: 0px;">New Price</h2>
			    <p>Have you recently changed the price for this listing? Changing the price is quick and easy. Just fill in the form below:</p>
			    <p><b>New price:&nbsp;</b> <input type="text" name="new_price_<?=$listing->id?>" id="new_price_<?=$listing->id?>" class="uiInput" style="float:none; width: 350px;"/></p>
			    <br>
			    <a href="#" onClick="setNewPrice('<?=$listing->id?>');" class="uiButton right">Change the Price Now</a>

			</div>
		</div>


	<?php elseif((int)$listing->reblast_id==0): ?>
	
		<div class="clearfix action_items" style="margin-top: 10px; padding-top: 10px; border-top: solid 1px #e5e5e5;">
			
			<a href='#' rel="relist" listingid="<?=$listing->id?>" class="uiButton aux small" style="width: 70px; padding-left: 8px; text-align: left; margin-right: 5px;" title="Relist">
				Relist
			</a>
			
			
		</div> 

			

		<div style="display: none; height: 400px;">
			<div id="reblast-<?=$listing->id?>">
			    <h2 style="margin-top: 0px;">Reblast</h2>
			    <p>Do you want to reblast this listing? Making this listing available is easy. Simply click the 'Reblast' button below and we'll reblast this listing.</p>
			    <br>
			    <a href="/listing/reblast/id/<?=$listing->id?>"  class="uiButton right">Reblast</a>

			</div>
		</div>
		

	<?php endif; ?>
*/ ?>




<? else : ?>

	<h3 style="margin-left: 10px; font-weight: 600;">New to <?=COMPANY_NAME;?>?</h3>

	<p style="margin-left: 10px;">Did you know <?=COMPANY_NAME;?> members can share every one of their new listings with potential buyers all across their market for free? <a href="/blast">Blast now</a>, and
	reach up to thousands more prospects with <?=COMPANY_NAME;?>'s amazing <a href="/blast">Blasting service</a>, by submitting your listings now to our Editors for immediate
	publication. To learn more about how blasting works, <a href="#" class="help_video_btn">watch this short video</a>.</p>
	


	<p style="text-align: right;"><a href="#" class="uiButton blue right help_video_btn">Learn about Blasting</a></p>

<? endif; ?>


<div style="display: none;">
	
	<div id="help_video_popup" style="width: 640px; height: 540px;">
	    	   
	    	<div style="width: 640px; height: 480px;">   		
			<iframe width="640" height="480" src="//www.youtube.com/embed/nOTl5Svnmy0?rel=0" frameborder="0" allowfullscreen></iframe>	
		</div>
		
		<div style="text-align: right; margin-top: 5px; border-top: 1px solid #b7b7b7; padding-top: 5px"><input type="checkbox" value="1" id="show_blast_help" name="show_blast_help">&nbsp;Please, don't show this message again.</div>
		
	</div>

</div>

<div id="blastItAjaxRequestErrorBox">
    <h2>Blast Listing</h2>
    <p id="messageArea"></p>
</div>
<?php if (!$this->search) : ?>

<script>

var search_update_timeout;

$(document).ready(function(e){
    search();

	$('.help_video_btn').click(function(event)
	{
		event.preventDefault();
		$.colorbox({width:"720px", height: "640px", inline:true, href:"#help_video_popup"});
	});
		
	$('#listing-search').bind('keyup', search);
	$('#search-status').bind('change', search);

	function search() {
		if (search_update_timeout) clearTimeout(search_update_timeout);
		search_update_timeout = setTimeout(searchUpdateListings, 200);
	}

	function searchUpdateListings(link) {
		$('.listingItemContainer').css('opacity', .3);
		if (!link) link = '/member/tab_listings/page/<?= $this->page ?>';
		$.get(link + '?' + $('.searchform').serialize() , function (data) {
			$('.listingItemContainer').replaceWith(data);
			bindLinks();
			bindSearchPagination();
		});
	}
	
	function bindLinks() {
		$('.links > a').not('.blastIt').click(function(event){
            event.preventDefault();
            var listing_id = $(this).attr("listingid");
            var href = $(this).attr("rel");
            href += '-' +listing_id;
            $.colorbox({width:"550px", height: "300px", inline:true, href:"#"+href});
		});
	}
	
	function bindSearchPagination() {
		$('.listingItemContainer #pagination li a').unbind('click').bind('click', function (e) {
			searchUpdateListings(e.target.href);
			e.preventDefault();
		});
	}

	bindPaginationAjax();
	bindLinks();

    // 'Blast it' functional
    $(document).on('click', 'a.blastIt', function(event){
        event.preventDefault();
        $.ajax({
            'url': '/member/ajaxBlastIt',
            dataType: 'json',
            type: 'post',
            data: {
                listingId: $(this).data('listingId')
            },
            success: function(data, textStatus, jqXHR) {
                if (data.isSuccess) {
                } else {
                }
                $('#blastItAjaxRequestErrorBox #messageArea').empty().append(data.message);
                $.colorbox({width:"550px", height: "300px", inline:true, href:"#blastItAjaxRequestErrorBox"});
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#blastItAjaxRequestErrorBox #messageArea').empty().append(textStatus);
                $.colorbox({width:"550px", height: "300px", inline:true, href:"#blastItAjaxRequestErrorBox"});
            }
        })
    });

});


function setNewPrice(listingid){
	var new_price = $("#new_price_"+listingid).val();
	if(new_price=='' || isNaN(new_price)){
		alert("Please enter valid new price.");
		return false;
	}
	window.location = '/listing/listingstatus/status/price_change/id/'+listingid+'/newprice/'+new_price;
}
</script>
<?php endif; ?>