<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Oh Dear, There Was a Problem.</h1>

		</div>
											
		<p>It seems your membership has been temporarily suspended.</p>

		<p>If you need more information, please feel free to drop us a line at <a href="mailto:<?=HELP_EMAIL;?>"><?=HELP_EMAIL;?>.</p>
		
		<a href="mailto:<?=HELP_EMAIL;?>" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" id="submitBtn">Send Email</a>
		
		<div class="clearfix"></div>
		
	</div>
	
</div>
