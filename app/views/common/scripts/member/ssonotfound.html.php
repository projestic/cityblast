<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Oh Dear, There Was a Problem</h1>
		</div>
		
		<p>We looked everywhere but we can't seem to find your account. This may be an error with the site you signed up through. We'll look into it, and you may also <a href="info@cityblast.com">contact us</a> for further assistance.
		
		<div class="clearfix"></div>
		

	</div>

</div>