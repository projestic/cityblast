<?php 

$city 		= null;
$state 		= null;
$country 	= null;

if (isset($this->member)) 
{
	if ($this->member->publish_city) 
	{
		$city    = $this->member->publish_city->id;
		$state   = $this->member->publish_city->state;
		$country = $this->member->publish_city->country;
	} elseif ($this->member->default_city) {
		$city    = $this->member->default_city->id;
		$state   = $this->member->default_city->state;
		$country = $this->member->default_city->country;
	}
}

if (isset($this->notification)) : ?>
	<p style="color: #790000; font-style: italic;"><?=$this->notification; ?></p>
<?php endif; ?>



<div style="margin-top: 15px; position: relative;">
	<div id="country" style="float: left; clear: none !important;">
		<select name="country" id="country_select" class="city_select" style="background-color: #EEEEEE !important; width: 115px !important;">
			<?php if (!$country) : ?><option value="">-- Select Country --</option><?php endif; ?>
			<option value="Canada" <?= ($country == 'Canada') ? 'selected="1"' : '' ?>>Canada</option>
			<option value="United States" <?= ($country == 'United States') ? 'selected="1"' : '' ?>>United States</option>
		</select>
	</div>
	
	<div id="state" style="display: none;float: left; margin-left: 10px !important; clear: none !important;">
		<select name="state" id="state_select" class="city_select"  style="background-color: #EEEEEE !important; width: 155px !important;"></select>
	</div>
	
	<div id="city" style="display: none;float: left; margin-left: 10px !important; clear: none !important;">
		<select id="defaultpubcity" name="city_id" class="city_select" style="background-color: #EEEEEE !important; width: 175px !important;"></select>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){

	$("#country").change(function(e) {	
		$('#city').hide();
		$('#state').hide();
		if ($('#country select').val()) {
			$('.step3').addClass('opac');
			$('.step2').removeClass('complete');
			$("#country_select").css('background-color','#EEEEEE');
			setCountry($('#country select').val());
		}
	}).trigger('change');	
	
	$("#state").change(function(e) {
		$('#city').hide();
		if ($('#state select').val()) {
			$('.step3').addClass('opac');
			$('.step2').removeClass('complete');
			$("#state_select").css('background-color','#EEEEEE');
			setState($('#country select').val(), $('#state select').val());
		}
	});

	

	function setCountry(country)
	{
		var state_select = $('#state select');
		if (country) 
		{
			state_select.empty();
			var states = getStates(country, function(states){
				var type;
				if (country == 'United States') type = 'State';
				if (country == 'Canada') type = 'Province';
				state_select.append('<option value="">-- Select ' + type + ' --</option>');
				$.each(states, function(key, value){
					state_select.append('<option value="' + value + '">' + value + '</option>');
				});
				$('#state').show();
				if (!state_select.val() && '<?= $state ?>') state_select.val('<?= $state ?>').trigger('change');
			});
		}
	}
	
	function setState(country, state)
	{
		var city_select = $('#city select');
		if (country && state) 
		{
			city_select.empty();
			var cities = getCities(country, state, function(cities){
				city_select.append('<option value="">-- Select City --</option>');
				$.each(cities, function(key, value){
					city_select.append('<option value="' + key + '">' + value + '</option>');
				});
				$('#city').show();
				if (!city_select.val() && '<?= $city ?>') city_select.val('<?= $city ?>');
			});
		}
	}

	
	function getCities(country, state, onSuccess)
	{
		getCityData(country, state, null, function(results){
			var cities = {};
			$.each(results, function(key, value){
				cities[value['id']] = value['name'];
			});
			onSuccess(cities);
		});
	}
	
	
	function getStates(country, onSuccess)
	{
		getCityData(country, null, 'state', function(results){
			var states = {};
			$.each(results, function(key, value){
				states[value['state']] = value['state'];
			});
			onSuccess(states);
		});
	}
	
	function getCityData(country, state, group_by, onSuccess)
	{
		var jsonUrl = "/city/get_all";  
		if (country) {
			jsonUrl += "/country/"+country;  
		}
		if (state) {
			jsonUrl += "/state/"+state;  
		}
		if (group_by) {
			jsonUrl += "/group_by/"+group_by;  
		}
		
		$.getJSON(  
			jsonUrl,   
			function(result) 
			{  
				onSuccess(result);
			}  
		); 
	}
});
</script>
