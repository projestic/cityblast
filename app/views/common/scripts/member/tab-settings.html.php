<? 

$member = isset($_SESSION['member']) && isset($_SESSION['member']->id) ? $_SESSION['member'] : new stdClass(); 

if ($this->member && $this->member->network_app_facebook) {
	$network_app_facebook = $this->member->network_app_facebook;
} else {
	$network_app_facebook = Zend_Registry::get('networkAppFacebook');
}

?>
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>


<? if (isset($_SESSION['member']) && !empty($_SESSION['member'])) : ?>


	<? if( $this->member->payment_status == "cancelled" || $this->member->payment_status == "signup_expired") : ?>

		<? echo $this->render('member/settings.paid.html.php'); ?>


		<script type="text/javascript">
		$(document).ready(function()
		{
			$.colorbox({width:"650px", height: "500px", inline:true, href:"#payment_failed"});
		});
		</script>
	
		<div style="display: none;">
	
			<div id="payment_failed">
				
				<div class="box clearfix">
				
					<h1>Inactive Account.</h1>
														
					<p>Your account is currently <a href="/member/payment/?rebill=1">inactive</a>.
						To start developing professional relationships with the friends in your social network, simply
						<a href="/member/payment/?rebill=1">activate your account now</a>.</p>
			
				
					<p>With your publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers stay tuned 
						with consistent and professional local real estate content.</p>	
						
					<p>Turn your social media presence into a powerful marketing tool, <a href="/member/payment/?rebill=1">activate your account now</a>.</p>				
												
				</div>
				
				<a href="/member/payment/?rebill=1" class="btn btn-primary btn-large pull-right">Reactivate <?=COMPANY_NAME;?> now</a>
	
			</div>
		</div>
		

	<? elseif($this->member->payment_status == "payment_failed") : ?>

		<? echo $this->render('member/settings.paid.html.php'); ?>


		<script type="text/javascript">
		$(document).ready(function()
		{
			$.colorbox({width:"650px", height: "500px", inline:true, href:"#payment_failed"});
		});
		</script>
	
		<div style="display: none;">
	
			<div id="payment_failed">
				
				<div class="box clearfix">
			
		
					<? if($this->member->payment_status == "payment_failed") : ?>
						<h1>Oh Dear, Your Payment Failed.</h1>
					<? else : ?>
						<h1>Activate Your Account.</h1>
					<? endif; ?>
					
	
					<p>Unfortunately, something went wrong with your last payment. We're sure it's just a simple mix up on our end!</p>
					
					<p>Your account is currently <a href="/member/payment/?rebill=1">inactive</a>.
						To continue developing professional relationships with the friends in your social network, simply
					<a href="/member/payment/?rebill=1">update your credit card information now</a>.</p>
				
					<p>With your publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers stay tuned 
						with consistent and professional local real estate content.</p>	
						
					<p>Turn your social media presence into a powerful marketing tool, <a href="/member/payment/?rebill=1">activate your account now</a>.</p>				
												
				</div>
				
				<a href="/member/payment/?rebill=1" class="btn btn-primary btn-large pull-right">Update Your Payment Information</a>
	
			</div>
		</div>

		
	<? else: ?>	

		<? echo $this->render('member/settings.paid.html.php'); ?>

	
	<? endif; /* Active/Inactive else statement end */ ?>	

<? endif; /* Logged In */ ?>

<?php if (empty($member->id)) : ?>

<script type="text/javascript">
	var USE_FB_JS_SDK	=   true;
	var fb_login_status	=	false;

	$(document).ready(function() 
	{
		callPopup();
	});


	function callPopup() {

		$.colorbox({
			href: false,
			innerWidth:420,
			innerHeight:480,
			html:'	<div>'+
						'<div class="box clearfix">'+
							'<div class="box_section">'+
								'<h1 style="margin-bottom: 0px;">Welcome to <?=COMPANY_NAME;?>.</h1> <h3>Please Login or Register.</h3>'+
							'</div>'+
							'<h3>Hello, before you can start, you\'ll need to login or register.</h3>'+
							'<p>If you\'re a returning member please <a href="#" class="login1">login</a> before Blasting. If you\'re new to the site please <a href="#" class="register1">register a new account</a> to get started.</p>'+
						'</div>'+
					'</div>'+
					'<div>'+
						'<div class="box clearfix">'+

							'<a href="#" class="btn btn-primary login1" style="margin-left: 50px; width: 250px; margin-top: 20px;">Login Now</a>'+
							'<a href="#" class="btn btn-primary register1 cancel" style="margin-left: 50px; width: 250px; margin-top: 20px;">Register Now</a>'+

						'</div>'+
					'</div>',

			onComplete: function(e) {

				$('.login1').bind('click', fb_login);
				$('.register1').bind('click', fb_login);

			},

			onClosed: function(e) {

				if (!fb_login_status) {

					$('.login1').unbind();
					$('.register1').unbind();
					callPopup();

				} else {

					$('.register1').unbind();
					$('.register1').bind('click', fb_register);
				}
			}

		});


		if(USE_FB_JS_SDK) 
		{
			// avoid multiple initialization
			if (typeof is_facebook_init == 'undefined' || !is_facebook_init)
			{
				FB.init({
					appId  : '<?=$network_app_facebook->consumer_id;?>',
					status : true, // check login status
					cookie : true, // enable cookies to allow the server to access the session
					xfbml  : true, // parse XFBML
					oauth  : true, // enable OAuth 2.0
					channelUrl: 'http://<?php echo $_SERVER['HTTP_HOST'] ?>/channel.php' // channeling JS SDK
				});

				is_facebook_init	= true;
			}



		    	function fb_login (event)
			{

			    event.preventDefault();
			    FB.login(function(response) {

				    if (response.authResponse)
				    {
					    $.ajax({

						    type: "POST",
						    url: "/member/fblogin",
						    data: "access_token="+response.authResponse.accessToken,
						    dataType: "json",

						    success: function(msg)
						    {
								fb_login_status	= true;
							   window.location =	msg.redirectBackTo;
						    }
					    });
				    }

			    }, {scope: '<?php echo FB_SCOPE;?>'});

		    }



			function fb_register (event)
			{
				event.preventDefault();

				//alert('What is my City?'+$("#city").val());
				var city_id	= $("#city").val();
				if (city_id == null || city_id == '' || city_id == '*')
				{
					window.location	= "/member/blastcity";
					return false;
				}
				else
				{

				    	FB.login(function(response) {

					    if (response.authResponse)
					    {
						    // console.log(response.authResponse);
						    $.ajax({

							    type: "POST",
							    url: "/member/fbsignup",
							    data: "access_token="+response.authResponse.accessToken+"&href="+$('#facebook-signup').attr('href'),
							    dataType: "json",

							    success: function(msg)
							    {
									fb_login_status	= true;
									window.location =	msg.redirectBackTo;
							    }
						    });

					    }

				    }, {scope: '<?php echo FB_SCOPE;?>'});
				}
			}
		}
	}
</script>

<?php endif ?>