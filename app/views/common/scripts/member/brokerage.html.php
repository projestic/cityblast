<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Member Brokerage.</h1>
			<? /*<p>This is the phone number the buying agent will see via email.</p>*/ ?>
		</div>
			
				

		<p><?=COMPANY_NAME;?> works hand-in-hand with your local regulating bodies to make sure this service is compliant with all of their rules and codes.  With that in mind, please keep your Brokerage updated, so that it can be included where required.</p>
		<br/><br/>	
			
			<?php if(!isset($this->updated) || $this->updated === false): ?>
			
				
				
				<form action="/member/brokerage" method="POST">
					<ul class="uiForm clearfix">
						<li class="uiFormRow clearfix">
							<ul>
								<li class="fieldname cc"><span style="font-weight: bold; color: #990000">*</span>Brokerage of Record</li>
								<li class="field cc">
								<input type="text" name="brokerage" value="<?=$this->member->brokerage;?>" class="uiInput<?=($this->fields && in_array("brokerage", $this->fields)?" error":"")?> validateNotempty" />
								</li>
							</ul>
						</li>

						<li class="uiFormRow clearfix">
							<ul>
								<li class="fieldname cc"><span style="font-weight: bold; color: #990000">*</span>Brokerage Address </li>
								<li class="field cc">
								<input type="text" name="broker_address" value="<?=$this->member->broker_address;?>" class="uiInput<?=($this->fields && in_array("broker_address", $this->fields)?" error":"")?> validateNotempty" />
								</li>
							</ul>
						</li>

						<li class="uiFormRow clearfix">
							<ul>
								<li class="buttons">
									<input type="submit" name="submitted" class="btn btn-primary btn-large pull-right" style="width: 195px;" value="Save Info" id="submitBtn">
								</li>
							</ul>
						</li>
					</ul>
				</form>
			
			<?php else: ?>
				
				<h3>Your Broker of Record information was successfully updated.</h3>
				
				<p><a href="/member/settings">Return to your personal settings.</a></p>
				
			<?php endif; ?>
			
		</div>
	
	</div>