<?php if(!isset($this->updated) || $this->updated === false): ?>

	<div class="row-fluid">
	
	    <div class="contentBox">
	
	        	<div class="contentBoxTitle">
				<h1>Email Address.</h1>
				<? /*<p>This is the phone number the buying agent will see via email.</p>*/ ?>
			</div>
				
					
					
			
			<form action="/member/emailoverride" method="POST">
				<ul class="uiForm clearfix">

					<? /* 
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Current Email</li>
							<li class="field cc">
								<?
									if(isset($this->member->email_override) && !empty($this->member->email_override))
									{
										echo $this->member->email_override;
									}
									else
									{
										echo $this->member->email;
									}
								?>
							</li>
						</ul>
					</li>
					*/ ?>		


					<? /* if(isset($this->member->email_override) && !empty($this->member->email_override)) : ?>
									
						<li class="uiFormRow clearfix">
							<ul>
								<li class="fieldname cc">Alternate Email</li>
								<li class="field cc">
									<?
										echo $this->member->email;
									?>
								</li>
							</ul>
						</li>
						
					<? endif; */ ?>
					

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>Email Address</li>
							<li class="field cc">
							<input type="text" name="email_override" value="<?=$this->member->email_override;?>" class="uiInput <?=($this->fields && in_array("email_override", $this->fields)?" error":"")?> validateNotempty" />
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<input type="submit" name="submitted" class="btn btn-primary btn-large pull-right" style="width: 195px;" value="Save Info" id="submitBtn">
							</li>
						</ul>
					</li>
				</ul>
			</form>
			
		</div>
		
	</div>
		
<?php else: ?>
	
	<div class="grid_12">
	
		<div class="box" style="padding-top: 25px;">
			
			<h2>Your primary Email address was successfully updated.</h2>
			
			<p><a href="/member/settings">Return to your personal settings.</a></p>
		</div>
	
	</div>
	
<?php endif; ?>
		

