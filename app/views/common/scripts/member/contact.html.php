<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Contact Phone Number.</h1>
			<? /*<p>This is the phone number the buying agent will see via email.</p>*/ ?>
		</div>
			
				
				

		<?php if(!isset($this->updated) || $this->updated === false): ?>
			
			<form action="/member/contact" method="POST">
				<ul class="uiForm clearfix">

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>Phone Number</li>
							<li class="field cc">
							<input type="text" name="phone" value="<?=$this->member->phone;?>" class="uiInput <?=($this->fields && in_array("phone", $this->fields)?" error":"")?> validateNotempty" />
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<input type="submit" name="submitted" class="btn btn-primary btn-large pull-right" style="width: 195px;" value="Save Info" id="submitBtn">
							</li>
						</ul>
					</li>
				</ul>
			</form>
		
		<?php else: ?>
			
			<h3>Your Contact Information was successfully updated.</h3>
			
			<p><a href="/member/settings">Return to your personal settings.</a></p>
			
		<?php endif; ?>
		
	</div>

</div>