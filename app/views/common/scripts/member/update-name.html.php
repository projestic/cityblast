
<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Display Name.</h1>
		</div>
			
		<?php if(!isset($this->updated) || $this->updated === false): ?>
			
			<form action="/member/update_name" method="POST" id="nameForm">
				<ul class="uiForm clearfix">
			
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>First Name</li>
							<li class="field cc">
							<input type="text" name="first_name" value="<?=$this->member->first_name;?>" class="uiInput <?=empty($this->member->first_name)?" error ":"";?> validateNotempty" />
							</li>
						</ul>
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>Last Name</li>
							<li class="field cc">
							<input type="text" name="last_name" value="<?=$this->member->last_name;?>" class="uiInput <?=empty($this->member->last_name)?" error ":"";?> validateNotempty" />
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<input type="submit" name="submitted" class="btn btn-primary btn-large pull-right" style="width: 195px;" value="Save Info" id="submitBtn">
							</li>
						</ul>
					</li>
				</ul>
			</form>
		
		<?php else: ?>
			
			<h3>Your name was successfully updated!</h3>
			
			<p><a href="/member/settings">Return to your personal settings.</a></p>
			
		<?php endif; ?>
		
	</div>

</div>

<script type="text/javascript">

$(document).ready(function() {
    $("#submitBtn").click(function(event){
        	event.preventDefault();
	    	var errors = 0;
		
		$(".validateNotempty").each( function()
		{
			$(this).removeClass('error');

			if($(this).val() == '') {
				errors = errors + 1;
				$(this).addClass('error');

				var htmldata = '<div><div class="clearfix"><h3>Oh Dear</h3><p>Your first and last names are required! Please enter your name now and try again.</p><a href="#" class="uiButton right" onclick="$.colorbox.close();">Enter Your Name</a></p></div></div>';
				$.colorbox({width:"650px", height: "290px", html:htmldata});
			}
		});
		
		if(errors == 0)
		{
			$("#nameForm").submit();
		}
    });
});
</script>