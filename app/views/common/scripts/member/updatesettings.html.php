<?php if(!isset($this->updated) || $this->updated === false): ?>

	<div class="row-fluid">
	
	    <div class="contentBox">
	
	        	<div class="contentBoxTitle">
				<h1>Your Settings.</h1>
				<? /*<p>This is the phone number the buying agent will see via email.</p>*/ ?>
			</div>
				
					
					
			
			<form action="/member/updatesettings" method="POST">
				<ul class="uiForm clearfix">

			
			
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>First Name</li>
							<li class="field cc">
							<input type="text" name="first_name" value="<?=$this->member->first_name;?>" class="uiInput <?=empty($this->member->first_name)?" error ":"";?> validateNotempty" />
							</li>
						</ul>
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>Last Name</li>
							<li class="field cc">
							<input type="text" name="last_name" value="<?=$this->member->last_name;?>" class="uiInput <?=empty($this->member->last_name)?" error ":"";?> validateNotempty" />
							</li>
						</ul>
					</li>


					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>Email Address</li>
							<li class="field cc">
							<input type="text" name="email_override" value="<?=$this->member->email_override;?>" class="uiInput <?=($this->fields && in_array("email_override", $this->fields)?" error":"")?> validateNotempty" />
							</li>
						</ul>
					</li>




					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>Phone Number</li>
							<li class="field cc">
							<input type="text" name="phone" value="<?=$this->member->phone;?>" class="uiInput <?=($this->fields && in_array("phone", $this->fields)?" error":"")?> validateNotempty" />
							</li>
						</ul>
					</li>
				</ul>

				
				<?php if (in_array($this->member->type->code, array('AGENT', 'BROKER'))) : ?>
				<h3 style="margin-top: 20px;">Broker of Record</h3>
				<ul class="uiForm clearfix">
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="font-weight: bold; color: #990000">*</span>Brokerage of Record</li>
							<li class="field cc">
							<input type="text" name="brokerage" value="<?=$this->member->brokerage;?>" class="uiInput<?=($this->fields && in_array("brokerage", $this->fields)?" error":"")?> validateNotempty" />
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="font-weight: bold; color: #990000">*</span>Brokerage Address </li>
							<li class="field cc">
							<input type="text" name="broker_address" value="<?=$this->member->broker_address;?>" class="uiInput<?=($this->fields && in_array("broker_address", $this->fields)?" error":"")?> validateNotempty" />
							</li>
						</ul>
					</li>
				</ul>
				<?php endif; ?>

				<ul class="uiForm clearfix">
					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<input type="submit" name="submitted" class="btn btn-primary btn-large pull-right" style="width: 195px;" value="Save Info" id="submitBtn">
							</li>
						</ul>
					</li>
				</ul>
				
			</form>
			
		</div>
		
	</div>
		
<?php else: ?>
	
	<div class="grid_12">
	
		<div class="box" style="padding-top: 25px;">
			
			<h2>Your settings were successfully updated.</h2>
			
			<p><a href="/member/settings">Return to your personal settings.</a></p>
		</div>
	
	</div>
	
<?php endif; ?>
		

