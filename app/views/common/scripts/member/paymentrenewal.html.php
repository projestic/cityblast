<div class="grid_8">

	<div class="box smallform clearfix">

		<div class="box_section">
			<h1>ClientFinder Renewal.</h1>

			<p>In order to activate your ClientFinder account, you'll need to supply your credit card information again.</p>
			
		</div>

		<? if (isset($this->errors) && !empty($this->errors)): ?>

			<ul class="uiForm small clearfix" style="margin-top: -20px;">
				<li class="uiFormRowHeader" style="color: #cc0000;">Oh Dear, Something Went Wrong!</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Error(s)</li>
						<li class="field">
							<ul>
								<?
									if(is_array($this->errors))
									{								
										foreach($this->errors as $error):
											$error = trim($error);
								
											if(isset($error) && !empty($error) && $error != "" && strlen($error) > 0)
											{
												echo $error;
											}
										endforeach;
									}
									else
									{
										//echo "<h1>ELSE!!</h1>";
										echo $this->errors;	
									}
								?>
							</ul>
						</li>
					</ul>
				</li>
			</ul>

		<? endif; ?>

		<form id="billing_form" autocomplete="off" action="/member/payment_save" method="post">

			<? echo $this->render('common/creditcard.html.php'); ?>

		</form>

	</div>

</div>

<div class="grid_4">


	<div class="box clearfix">
		<h4>Have Any Questions?</h4>		
		<p><?=TOLL_FREE;?></p>
		
		<h4>Live Chat Works Too!</h4>
		<p>We're also on stand by on <a href="#">Live Chat</a>.</p>
	</div>

	<div class="box clearfix">

		<h4><img src="/images/prize_winner.png" style="margin: 0 3px -8px 0;">100% Money Back Guarantee</h4>

		<hr style="margin-top: 0px" />

		<p style="margin-top: 2px; padding-top: 2px; color: #8c8c8c;"><?=COMPANY_WEBSITE;?> 100% guarantees your satisfaction or your money back. No argument. No resistance. No issues. If you're not 100% happy,
		we'll give you your money back.</p>

		<h4><img src="/images/security.png" style="margin: 0 3px -8px 0;">Security</h4>

		<hr style="margin-top: 0px" />

		<p style="margin-top: 2px; padding-top: 2px; color: #8c8c8c;">We take security very seriously. That's why we have partnered with PayPal Security to offer the latest in fraud protection,
		encryption and 24/7 monitoring.  Our first priority is our deep commitment to making sure your credit card information is always secure.</p>

		<h4><img src="/images/locked.png" style="margin: 0 3px -8px 0;">Privacy</h4>

		<hr style="margin-top: 0px" />

		<p style="margin-top: 2px; padding-top: 2px; color: #8c8c8c;">The only thing we take more seriously than your security is your privacy. We'll never share your email address or any other
		personal information with a third party. Rest easy, your information is safe with us.</p>

		<hr />

		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td style="border-bottom: 0px;">
					<a target="_blank" href="https://www.paypal.com/us/verified/pal=crowonder%40yahoo%2ecom"><img border="0" alt="Official PayPal Seal" src="/images/verification_seal.gif" style="width: 50px; height: 50px;"></a>
				</td>
				<td style="border-bottom: 0px; vertical-align: middle;">
					<span id="cdSiteSeal1"><script type="text/javascript" src="//tracedseals.starfieldtech.com/siteseal/get?scriptId=cdSiteSeal1&amp;cdSealType=Seal1&amp;sealId=55e4ye7y7mb733503ab0e5940b525a15m53y7mb7355e4ye77f17ede32bda8ca5"></script></span>
				</td>
			</tr>
		</table>

	</div>

</div>

<!-- Google Code for CityBlast /index/how-blasting-works Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 956260748;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "IL1qCITeowMQjMP9xwM";
var google_conversion_value = 0;
if (49) {
  google_conversion_value = 49;
}
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/956260748/?value=49&amp;label=IL1qCITeowMQjMP9xwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>