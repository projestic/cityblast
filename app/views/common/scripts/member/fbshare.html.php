<? if($this->result == "SUCCESS") : ?>

	<div class="row-fluid">
	
	    <div class="contentBox">
	
	        	<div class="contentBoxTitle">
				<h1>Nice One - Facebook Share Complete.</h1>
			</div>
		

			
			<p>Great idea to spread the word about your listing: <strong><?=$this->listing->message; ?></strong></p>
			
			<? if($this->listing->member->twitter_id) :?>
				<p>Did you want to <a href="/member/twshare/<?=$this->listing->id;?>">publish it on Twitter</a> too?</p>	

				<a href="/member/twshare/<?=$this->listing->id;?>"" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" id="submitBtn">Try Again</a>
		
				<div class="clearfix"></div>	
			
				
			<? else : ?>


				<a href="/listing/view/<?=$this->listing->id;?>"" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" id="submitBtn">View Listing</a>
		
				<div class="clearfix"></div>	
							
						
			<? endif; ?>
			
		</div>
	
	</div>					

<? else : ?>

	<div class="row-fluid">
	
	    <div class="contentBox">
	
	        	<div class="contentBoxTitle">
				<h1>Oh Dear, Something Went Wrong!</h1>
			</div>
			

			<p>Looks like something went wrong. Did you want to try <a href="/member/fbshare/<?=$this->listing->id;?>">publishing it on Facebook</a> again?</p>

			<a href="/member/twshare/<?=$this->listing->id;?>"" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" id="submitBtn">Try Again</a>
		
			<div class="clearfix"></div>	
			
		</div>
	
	</div>
				
<? endif; ?>
