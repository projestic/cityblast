<div class="row-fluid">
	<div class="span12 banner">
		<div class="span5 agent">
			<img src="/images/new-images/cindy-thompson.png" />
			<div class="info">
				<div class="name">
					<h5>Cindy Thompson</h5>
					<h6>Real Estate Agent</h6>
				</div>
				<div class="other">
					<h5>Social Media Presence</h5>
					<h6><?=COMPANY_NAME;?>.com</h6>
				</div>
			</div>
		</div>
		<div class="span7">
			<h2><?=COMPANY_NAME;?> is your proven online expert in Social Media for your Real Estate business.</h2>
			<div class="row-fluid">
				<div class="span5">
					<h5>Get your Free 30-Day Trial Now or read below to see how <?=COMPANY_NAME;?> works.</h5>
				</div>
				<div class="span7">
					<a href="/member/index" class="uiButtonNew tryDemo">Try Demo.</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row-fluid">
	<div class="contentBox">
		<div class="contentBoxTitle">
			<h1>Your Social Media Success Strategy...</h1>
		</div>
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-1.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">Tell us what you want us to post.</h3>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac sem vel eros dapibus bibendum sed non ipsum. Sed eu lorem lectus.
				</div>
			</div>
		</div>
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-2.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">Your Social Media Expert plans your campaign.</h3>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac sem vel eros dapibus bibendum sed non ipsum. Sed eu lorem lectus.
				</div>
			</div>
		</div>
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-3.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">As often as you choose, she posts articles, videos & information.</h3>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac sem vel eros dapibus bibendum sed non ipsum. Sed eu lorem lectus.
				</div>
			</div>
		</div>
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-4.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">Follow your marketing success in your dashboard.</h3>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac sem vel eros dapibus bibendum sed non ipsum. Sed eu lorem lectus.
				</div>
			</div>
		</div>
		<div class="clearfix">
			<div class="pull-left tryDemoTxt">
				<h3 style="font-size: 28px;">Your now ready to conquer social media.</h3>
				<h4>Get started today with your Free 30-Day Trial of <?=COMPANY_NAME;?>.</h4>
			</div>
			<div class="pull-right">
				<a class="uiButtonNew tryDemo" href="/member/info">Try Demo.</a>
			</div>
		</div>
	</div>
</div>