	<div class="grid_12">
		
		<div class="box clearfix">
	
			<div class="box_section">
				<h1>Whitelist <?=COMPANY_NAME;?>.</h1>
			</div>
	
			<h3>Add <?=COMPANY_NAME;?> to your address book to ensure email delivery.</h3>
			<p>Find your web-based email subscriber below, and follow the step-by-step instructions on how to add <a href="mailto:<?=HELP_EMAIL;?>"><?=HELP_EMAIL;?></a> to your address book.</p>
			<p>If your email provider is not listed below, please contact them directly.</p>
	
			<h4>AOL Webmail Users:</h4>
			
			<ol style="margin-left: 30px!important;">
				<li>Open your email message.</li>
				<li>Click on &quot;More Details&quot; at the top of your email message.</li>
				
				<li>Hover mouse over the From address.</li>
				<li>Our email address is automatically placed in the email field in the &quot;Add Contact&quot; pop-up box.</li>
				<li>Add additional contact information.</li>
				<li>Click on &quot;Add Contact&quot;.</li>
				
				<li>Our email address will be automatically entered into your AOL Address Book.</li>
			</ol>
			<p>If you encounter any problems, <a href="http://help.aol.com/help/microsites/microsite.do" target="_blank">contact AOL Support</a>.</p>
	
			<h4>AOL Users:</h4>
			
			<ol style="margin-left: 30px!important;">
				<li>Open your email message.</li>
				<li>Click on the &quot;Add Address&quot; icon.</li>
				<li>Our email address is automatically placed in   the name and email field in the &quot;Add Contact&quot; pop-up box. <br>Verify the   information is correct and then...</li>
				
				<li>Click the Save button.</li>
				<li>Our email address will be automatically entered into your AOL Address Book.</li>
			</ol>
			
			<p>If you encounter any problems, <a href="http://help.aol.com/help/microsites/microsite.do" target="_blank">contact AOL Support</a>.</p>
			
			<h4>Yahoo Users:</h4>
			
			<ol style="margin-left: 30px!important;">
				<li>Open your email message.</li>
				<li>Click on &quot;Add&quot; icon next to From address.</li>
				
				<li>Our email address is automatically placed in the email field in the &quot;Add Contact&quot; pop-up box.</li>
				<li>Add additional contact information.</li>
				<li>Click on &quot;Save&quot;.</li>
				<li>Our email address will be automatically entered into your Yahoo! Address Book.</li>
			</ol>
			
			<p>If you encounter any problems, <a href="http://help.yahoo.com/l/us/yahoo/helpcentral/" target="_blank">contact Yahoo Support</a>.</p>
	
			<h4>Google Mail Users:</h4>
			
			<ol style="margin-left: 30px!important;">
				<li>Open your email message.</li>
				<li>Click on down arrow next to &quot;Reply&quot; on top right of the message.</li>
				<li>From drop down menu click on &quot;Add to Contact List&quot;.</li>
				<li>Our email address will be automatically entered into your contacts list.</li>
			</ol>
			
			<p>If you encounter any problems, <a href="http://mail.google.com/support/" target="_blank">contact Google Support</a>.</p>
			
			<h4>Window Live Hotmail Users:</h4>
			
			<ol style="margin-left: 30px!important;">
				<li>Open your email message.</li>
				<li>Click on &quot;Mark as safe&quot; at the top of the message.</li>
				<li>Our email address will be automatically entered into your Safe senders list.</li>
			</ol>
			
			<p>If you encounter any problems, <a href="http://help.live.com/help.aspx?project=a&amp;market=en-us" target="_blank">contact Window Live Hotmail Support</a>.</p>
	
			<h4>EarthLink Users:</h4>
			
			<ol style="margin-left: 30px!important;">
				<li>Open your email message.</li>
				<li>Click your mailbox's &quot;Message&quot; menu and choose &quot;Add Senders&quot; to your Address Book.</li>
				<li>Your email message will be automatically entered into your EarthLink Address Book.</li>
			</ol>
			
			<p>If you encounter any problems, <a href="http://support.earthlink.net/" target="_blank">contact EarthLink Support</a>.</p>
			
			<h4>Microsoft Outlook 2003/2007 Users:</h4>
			
			<ol style="margin-left: 30px!important;">
				<li>Open your email message.</li>
				<li>Click on &quot;Actions&quot; from the menu bar.</li>
				<li>Click on &quot;Junk E-mail&quot; from drop down menu.</li>
				<li>Click on &quot;Add Sender to Safe Senders List&quot;.</li>
				<li>Our email address will be automatically entered into your Safe senders list.</li>
				<li>Click on &quot;Junk E-mail&quot; from drop down menu.</li>
			</ol>
			
			<p>If you encounter any problems, <a href="http://office.microsoft.com/en-us/outlook/default.aspx" target="_blank">contact Outlook Support</a>.</p>
			
			<h4>Another Email Provider?</h4>
			
			<p>Please contact your email provider directly if not on the list above.</p>
			
	</div>
