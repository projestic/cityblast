<form action="" method="POST" />
<div class="row-fluid">

    <div class="contentBox">

        <div class="contentBoxTitle">
			<h1>Facebook settings</h1>
		</div>
		
		<?php if ($this->fixed) : ?>

		<p>Your Facebook settings are correct! We can now give you accurate statistics on your account's performance.</p>

		<a href="/member/settings" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" id="submitBtn">Return to dashboard</a>

		<?php elseif ($this->fixing) : ?>

		<p>Now fixing your settings ... you should see a popup window from Facebook. If you don't see the popup window, please click the button below.</p>

		<input type="button" class="facebook-login btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" value="Fix now" />

		<?php else : ?>

		<p>There's a problem with your Facebook settings, but don't worry. Just click the button below, and we'll reset everything. Please be sure to leave the visibility settings at "public" so we can keep track of your account's performance for you!</p>

		<input type="submit" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" value="Fix now" />
		
		<?php endif; ?>
		
		<div class="clearfix"></div>
		

	</div>
		
	<?php if (isset($this->doFacebookLogin) && $this->doFacebookLogin) : ?>
		<script type="text/javascript">
		$(document).ready( function () {
			$('.contentBox .facebook-login').trigger('click');
		});
		</script>
	<?php endif; ?>

</div>
</form>
