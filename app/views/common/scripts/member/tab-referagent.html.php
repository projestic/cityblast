<?php $referralCount = count($this->member->referral_credits); ?>
<h1 class="dashboardSection">Refer A Friend</h1>

<? /* if(isset($_SESSION['member'])): ?>
	<div class="clearfix" style="font-size: 20px; font-weight: bold; text-align: center;">
		Total Referral Blasts: <span style="color: #cc0000;"><?php echo $_SESSION['member']->referral_blast_count; ?></span>

		Total Referral Emails: <span style="color: #cc0000;"><?php echo $_SESSION['member']->referral_email_count; ?></span>
	</div>
	<br />
<? endif; */ ?>
<? /*
<div class="referAgentBanner clearfix">
	<div class="yourEntryBallots">
		<img src="/images/giving-away-trip-referral.png" alt="We're Giving away a trip for 2 to Las Vegas!" />
		<h4>Refer your friends to CityBlast now.</h4>
		<p>Each successful referral earns you another entry in our Grand Prize draw!</p>
		<h3>Your Entry Ballots For Las Vegas</h3>
		<div class="count">
			<span><?=($this->las_vagas_ballots)?$this->las_vagas_ballots:0?></span>
		</div>
		<a href="/index/faq">Contest Rules</a>
	</div>
	<div class="maximizeChanceToWin">
		<div class="step">
			<h3>Want to maximize your chances to win?</h3>
			<p>Follow these 3 steps:</p>
			<ul>
				<li><span class="number">1</span>Send an email out to the agents in your office.</li>
				<li><span class="number">2</span>Tell your agent friends and team members to sign up.</li>
				<li><span class="number">3</span>Ask your Broker/Owner to send out an email blast containing your link.</li>
			</ul>
		</div>
		<p class="enterMoreBallots">
			Use the referral tools below <br/>to enter more <br/>ballots!
		</p>
	</div>
</div>
*/ ?>
<div class="dashboardBox referSteps">
	<div class="dashboardHeading clearfix">
		<div class="headingIcon"></div>
		<h4>Earn a free <?= COMPANY_NAME ?> membership for life</h4>
	</div>
	<div class="stepDesc clearfix">
		<div class="step share" style="min-height: 280px;">
			<span class="stepNo">1</span>
			<h4>share</h4>
			<div class="stepIcon"></div>
			<p class="stepDescription">Share <?= COMPANY_NAME ?> with other pros by using the tools below.</p>
			<p class="stepDetail">You've sent <b><?=($this->referral_emails_sent) ? $this->referral_emails_sent : '0';?></b> referrals</p>
			<span class="nextStepArrow"></span>
		</div>
		<div class="step watch" style="min-height: 280px;">
			<span class="stepNo">2</span>
			<h4>watch</h4>
			<div class="stepIcon"></div>
			<p class="stepDescription">Keep track of your friends and see who signs up.</p>
			<p class="stepDetail"><b><?=($this->referral_trails) ? $this->referral_trails : '0';?></b> friends have started trials</p>
			<span class="nextStepArrow"></span>
		</div>
		<div class="step earn" style="min-height: 280px;">
			<span class="stepNo">3</span>
			<h4>earn</h4>
			<div class="stepIcon"></div>
			<p class="stepDescription">Earn a FREE MONTH of <?= COMPANY_NAME ?> every time they sign up.</p>
			<p class="stepDetail">You’ve earned <b><?=($this->referral_credits) ? $this->referral_credits : '0';?></b> free months</p>
			<span class="nextStepArrow"></span>
		</div>
		<div class="step itsFree" style="min-height: 280px;">
			<span class="stepNo">4</span>
			<h4>it’s free</h4>
			<div class="stepIcon"></div>
			<p class="stepDescription">When 10 agent or brokers sign up, your membership is set to free - for life!</p>
			<p class="stepDetail"><b><?=($this->referral_credits_needed) ? $this->referral_credits_needed : '0';?></b> referrals left to go</p>
		</div>
	</div>
</div>
<?php if ($referralCount < 10 && ($this->member->account_type == 'free' && $this->member->payment_status == 'free') == false) : ?>
<div class="referedFriends">
	<div class="title">
		<span>Friends you’ve referred to <?= COMPANY_NAME ?></span>
	</div>
	<div class="referralSlotContainer clearfix">
<?php 
		for ($i = 0; $i < 9; $i++) : 
			if ($i < $referralCount) : 
				$credit = isset($this->member->referral_credits[$i]) ? $this->member->referral_credits[$i] : null;
				$credit_member = ($credit) ? $credit->referred_member : null;
?>
		<div class="referralSlot">
			<?php if ($credit) : ?><img src="<?php echo $this->member->referral_credits[$i]->referred_member->getFacebookProfilePic(); ?>" class="referral-img-facebook"/><?php else : ?><div style="width: 50px; height: 50px;"></div><?php endif; ?>
			<img src="/images/referral-img-mask.png" class="referral-img-mask" title="<?=($credit) ? $credit_member->first_name . ' ' . $credit_member->last_name : 'nope'?>"/>
			<p><b>Free</b> Month</p>
		</div>
		<?php else : ?>
		<div class="referralSlot">
			<img src="/images/referral-img.png" />
		</div>
		<?php endif; endfor; ?>
		<div class="referralSlot goal">
			<img src="/images/goal-img<?php if ($referralCount < 10) : ?>-off<?php endif; ?>.png" />
			<p><b>Free</b> <br/>for Life!</p>
		</div>
	</div>
	<?php if ($referralCount < 10) : ?><h3 class="note"><span>Only <span class="bold"><?php echo 10-$referralCount ?></span> more referrals to unlock your <span class="bold">FREE ACCOUNT</span> for life!</span></h3><?php endif; ?>
</div>
<?php else : ?>
<div class="dashboardBox achievement">
	<span>Congrats! Your Account Is FREE!</span>
</div>
<?php endif; ?>
<div class="dashboardBox earnFreeMonths">
	<div class="dashboardHeading clearfix">
		<div class="headingIcon"></div>
		<h4>Earn Free Months Now With These Handy Tools!</h4>
	</div>
	<div class="earnToolsContainer">
		<div class="earnMonthsTool">
			<div class="number">1</div>
			<div class="toolDetail">
				<h3>Share Your Unique Referral Link</h3>
				<p>Paste in an email and send it to other professionals in your office, or share it on Facebook.</p>
				<div class="clearfix" style="line-height: 40px;">
					<span class="referral_link_wrapper" style="float: left; line-height: 40px; font-size: 16px; width: 440px; text-align: left;">
						Your Unique Link:&nbsp;&nbsp;<span style="color: #404040; font-weight: bold;" id="referral_link"><?= DOMAIN ?>/a/i/<?=$_SESSION['member']->id;?></span>
					</span>
					<a href="javascript:void(0);" class="uiButton" id="copy_btn" style="margin-left: 5px; width: 70px; float: right; font-size: 16px !important;">Copy</a>
				</div>
			</div>
		</div>
		<div class="earnMonthsTool">
			<div class="number">2</div>
			<div class="toolDetail">
				<h3>Tell Your Friends on Facebook</h3>
				<p>Personalize the message below, then click send to post to your Facebook.</p>
				<form action="/index/send-fb-referral" method="POST" id="send_fb_referral">
			
					<div class="clearfix" style="position: relative">
					
						<div class="facebook_preview clearfix">
					
							<div class="facebook_preview_inner clearfix">
					
								<? if(isset($_SESSION['member'])):?>
									<img src="<?php echo empty($_SESSION['member']->photo) ? "https://graph.facebook.com/". $_SESSION['member']->uid ."/picture" : CDN_URL . $_SESSION['member']->photo ?>" class="facebook_userpic" id="facebook_userpic" />
								<? else: ?>
									<img src="/images/blank.gif" style="background-color: #f2f2f2;" class="facebook_userpic" id="facebook_userpic" />
								<? endif; ?>
					
								<div class="facebook_member" id="facebook_member" style="width: 368px;"><?=$_SESSION['member']->first_name;?> <?=$_SESSION['member']->last_name;?></div>
					
								<div class="facebook_message" id="facebook_message" 
									style="background-color: #ffffff; padding: 0px!important; border: none!important; resize: none; width: 368px;">
										
										<? if(stristr(APPLICATION_ENV, "myeventvoice"))	: ?>
											Fellow wedding and event planners – check out this awesome service I discovered that helps you with your social media marketing. It’s great!
										<? elseif(stristr(APPLICATION_ENV, "cityblast"))	: ?>
											Fellow real estate agents and brokers – check out this awesome service I discovered that helps you with your social media marketing. It’s great!
										<? else : ?>
											Fellow mortgage professionals – check out this awesome service I discovered that helps you with your social media marketing. It’s great!
										<? endif; ?>			
								</div>
					
								<div class="facebook_postpic_wrapper" id="facebook_postpic_wrapper" style="margin-top: 3px">
					
									<? if(stristr(APPLICATION_ENV, "myeventvoice"))	: ?>
										<img src="/images/myeventvoice/refer_preview_thumb.jpg" width="90" style="background-color: #f2f2f2;" class="facebook_postpic" id="facebook_postpic" />
									<? else:  ?>
										<img src="/images/refer_preview_thumb.jpg" width="90" style="background-color: #f2f2f2;" class="facebook_postpic" id="facebook_postpic" />
									<? endif; ?>
					
								</div>
					
								<div class="facebook_postinfo clearfix">
					
									<div class="facebook_linktext" id="facebook_linktext"><?=COMPANY_NAME;?>: Hire Your Social Expert Now!</div>
					
									<div class="facebook_baseurl" id="facebook_baseurl"><?=DOMAIN;?>/a/i/<?=$_SESSION['member']->id;?></div>
					
									<div class="facebook_description" id="facebook_description"><?=COMPANY_NAME;?> is now offering a 14-Day FREE Trial. Check out what our social experts can do for you!</div>
					
								</div>
					
							</div>
					
						</div>
						<input type="submit" value="Send" class="uiButton right" id="send_id" style="width: 90px; margin-top: -40px; font-size: 16px !important;" />
					</div>
					
					
					
				</form>
			</div>
		</div>
		<div class="earnMonthsTool">
			<div class="number">3</div>
			<div class="toolDetail">
				<h3>Tell Your Friends through Email</h3>
				<p>Enter your friends’ email in the space below and click send. We’ll forward them a signup email with your referral link in it.</p>
				<form action="/index/send-referral" method="POST" id="send_referral">
					<input type='hidden' name="redirect_user_back" value='/member/settings#referagenttab'/> 
					<input type="text" class="input email email_input referField uiInput" name="email_friend" id="email_friend" style="width: 450px; float: left;" value="" />
					<input type="submit" value="Send" class="uiButton right" id="send_id" style="width: 90px; font-size: 16px !important;" />
				</form>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">

$(document).ready(function(){
		
	$('#copy_btn').click(function(){
		$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3 style='margin-top: -20px;'>Affiliate Link</h3><p>Here is your personal <?=COMPANY_NAME;?> Affiliate Link. Simply triple click the link and CTRL+C to copy it.</p><div class='box black10' style='text-align: center; padding: 10px;'><strong><span style='color: #0c76a7;'>http://<?=DOMAIN;?>/a/i/<?=$_SESSION['member']->id;?></span></strong></div><p>Don't forget to paste your referral link in your email signature and website for further exposure.</p><p style='margin-bottom: 0px;'>Remember, <strong>10 referrals</strong> gets you a <strong>FREE Lifetime Membership!</strong></p>"});		
	});
		
	$("#send_referral").validate({errorElement: "div",
	    errorPlacement: function(error, element) {
        error.insertBefore($("#email_friend"));
     },rules: {
        email_friend: {
			required: true,
            email: true
        }
     },
     messages: {
        email_friend: {
			required: "Please enter a valid email address.",
            email: "Please enter a valid email address."
        }
     }});

});
</script>