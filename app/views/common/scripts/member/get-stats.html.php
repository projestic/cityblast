<?

	
	$tw_share = 0;
	$fb_share = 0;
	$email_share = 0;
	
	$rf_info = 0;
	$rf_booking = 0;
	
	if($this->member->interactions)
	{
		foreach($this->member->interactions as $interaction)
		{
			if($interaction->type == "twitter_share")
			{
				$tw_share++;
			}
			if($interaction->type == "facebook_share")
			{
				$fb_share++;
			}					
			if($interaction->type == "share_listing_email")
			{
				$email_share++;
			}					
		}
	}

?>

<div class="container_12 clearfix">
	<div class="grid_12">
		
		<table style="width: 100%">
			
			<tr>
				
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">Credits</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">Total Reach</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">CB FB Shares</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">CB TW Shares</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">CB Email Shares</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">CB Bookings</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">FB Likes</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">FB Comments</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">Total Blasts</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">Total Clicks</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">Avg Clicks Per Blast</td>
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">Interaction Score</td>

				
			</tr>
			
			<tr>
				<?
					$total_friends = 0;
					if(isset($this->member->facebook_friend_count) && !empty($this->member->facebook_friend_count)) $total_friends+= $this->member->facebook_friend_count;
					if(isset($this->member->twitter_followers) && !empty($this->member->twitter_followers)) $total_friends+= $this->member->twitter_followers;
					if(isset($this->member->linkedin_friends_count) && !empty($this->member->linkedin_friends_count)) $total_friends+= $this->member->linkedin_friends_count;
				?>

				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;"><?=count($this->member->credits);?></span>
				</td>
				

				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;"><?=number_format($total_friends);?></span>
				</td>

				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;"><?=$fb_share;?></span>
				</td>

				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;"><?=$tw_share;?></span>
				</td>				

				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;"><?=$email_share;?></span>
				</td>	

				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;"><?=count($this->member->bookings);?></span>
				</td>


				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;">
						<?
						
						$query = "select sum(facebook_likes) as total_likes from post where member_id=".$this->member->id;
						$likes = Post::find_by_sql($query);
						
						echo $likes[0]->total_likes;
						$total_likes = $likes[0]->total_likes;
							
						?>
					</span>
				</td>

				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;">						
						<?
						
						$query = "select sum(facebook_comments) as total_comments from post where member_id=".$this->member->id;
						$comments = Post::find_by_sql($query);
						
						echo $comments[0]->total_comments;
						$total_comments = $comments[0]->total_comments;
							
						?>
					</span>
				</td>				
								
				
				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;">
						<?
							$params['conditions'] = "member_id=".$this->member->id . " and result=1";
							$total_blasts = Post::count("all", $params);
							echo $total_blasts;
						?>
					</span>
				</td>

				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">
					<span style="font-size: 32px; font-weight: bold;"><?=number_format($this->clicks[0]->total_clicks);?></span>
				</td>				

				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">

					<span style="font-size: 32px; font-weight: bold;">
						<?
							//$posts = count($this->member->posts);
							$clicks = $this->clicks[0]->total_clicks;

							if($total_blasts)
							{
								echo round(($clicks / $total_blasts), 2);
							}
							else
							{
								echo "0";
							}
						
						?>
					</span>
				</td>	


				<td style="vertical-align: top; text-align: center; border-right: 1px solid #333;">

					<span style="font-size: 32px; font-weight: bold;">
						<?
							$bookings = count($this->member->bookings);
							
							if($total_blasts)
							{
								$interactions = $clicks + $fb_share + $tw_share + $email_share + $bookings + $total_likes + $total_comments;
								echo round(($interactions / $total_blasts), 2);
								
								//echo "<BR>";
								//echo $this->member->interaction_score;
							}
							else
							{
								echo "0";
							}
						
						?>
					</span>
				</td>	
				
			</tr>
		</table>
	</div>


</div>