<!--[if lt IE 9]>
	<style type="text/css">
		.box #note{
			margin-top: -10px;
			margin-bottom: 0px;
		}
	</style>
<![endif]-->
<!--[if IE 7]>
	<style type="text/css">
		.box #note{
			margin-top: -15px;
		}
	</style>
<![endif]-->


	<div class="row-fluid">
	
	    <div class="contentBox">
	
	        	<div class="contentBoxTitle">
	        		<h1>Problems with your account?</h1> 
	        		<h2>Call us at <span style="color: #333333;">1-888-712-7888</span>
	        		<?/* or <a href="#">Live Chat Now!</a>*/?></h2>
   		 	</div>


		
			<h3>We're sorry to see you go!</h3>
	
			<p>Was it something we said? Our goal is to make you the most successful Real Estate Agent around. In order for us to do that successfully, we need your feedback! If there's anything 
			we can do to improve your experience with <?=COMPANY_NAME;?>, please let us know and we'll try to fix it today.</p>
			


		
			<div class="contentBoxTitle">
				<h1>Having an issue?   It might be easily solved...</h1>	
			</div>
			
	
	<p>Our reason for existing is to make you the most successful real estate agent you can be.  In order for us to do that, we need your help in getting your account 'just right'.  
	Perhaps the tweak you need to set up <?=COMPANY_NAME;?> perfectly is already at your fingertips?</p>

	<p style="padding-bottom: 20px;">Check out four simple solutions...</p>


	

	<h5>Want to change how often we post?</h5>
	<p>Are the posts a bit overwhelming? Or not often enough?  Easily solved! How often we post is entirely up to you.  Changing it is a breeze: <a href="/member/settings#settingtab">Adjust your Frequency</a>.</p>
	
	
	<h5>Don't want other people's listings?</h5>
	<p>Do you prefer to not have other agents' listings appear on your wall?  No worries.  With one click, you can get that taken care of, and only feature articles.  Simply: <a href="/member/settings#settingtab">Turn off Listings</a>.</p>	




	<h5>Prefer no posts on your Personal Facebook?</h5>
	<p>No problem.  We do that!  You can effortlessly pick and choose where you'd like your <?=COMPANY_NAME;?> posts to go... including to your Facebook Business Fan Page.  Try this: <a href="/member/settings#settingtab">Stop publishing to my personal Facebook profile</a>.</p>	
			
			
	<h5>Want to publish to your Business Fan Page?</h5>
	<p>Facebook gave us Fan Pages - so let's use them!  If you want to have your posts directed to your Business Page, simply: <a href="/member/settings#settingtab">Turn on your Fanpage</a>.</p>	


	
	
	<h5><?=COMPANY_NAME;?> is hassle free.</h5>
	<p>If there's nothing that we can possibly do to make <?=COMPANY_NAME;?> better, not to worry. <?=COMPANY_NAME;?> is 100% hassle free. Just click on "Cancel My Account" below and your account will be cancelled 
		in the next 24 hours. Guaranteed.</p>	


	<h5>Can we do something to make you stay?</h5>
	<p>We want you to stay.  If there's anything we can to, please contact us now and let us know.  We're very easy-going, and will do what we can to make <?=COMPANY_NAME;?> the product you want it to be.</p>

	<p>Give us a call now, and let's see if we can make you happy:</p>
	
		<div class="contentBoxTitle">
			<h2><?=TOLL_FREE;?></h2>
		</div>


		
	<h5 style='margin-top: 35px;'>Sorry, there's nothing you can do to make my experience better.</h5>  
	
	<a class="btn btn-primary btn-large pull-right" style="width: 195px;"  id="cancelbutton" style="margin: 10px 0 0;" href="#">Cancel My Account Now</a>

	<div class="clearfix"></div>

</div>



<div style="display: none;">
	
	<div id="cancel_popup" style="position: absolute;">

<h3 style='margin-top: -20px;'>Cancel Now.</h3>
<p>In order to help us serve you better in the future, please let us know why you're leaving.</p>

<div class='box black10' style='text-align: center; padding: 10px;'><strong><a href="/member/cancelconfirm/tooexpensive" style='color: #0c76a7;'>I don't have the budget for it / It's just too expensive!</a></strong></div>

<div class='box black10' style='text-align: center; padding: 10px;'><strong><a href="/member/cancelconfirm/adjacency" style='color: #0c76a7;'>Too many Agents on my Facebook are using <?=COMPANY_NAME;?>.</a></strong></div>

<div class='box black10' style='text-align: center; padding: 10px;'><strong><a href="/member/cancelconfirm/noleads" style='color: #0c76a7;'>I've tried it for a while, and I'm not getting enough leads.</a></strong></div>

<div class='box black10' style='text-align: center; padding: 10px;'><strong><a href="/member/cancelconfirm/trial" style='color: #0c76a7;'>To be honest, I just wanted to try it.</a></strong></div>

<div class='box black10' style='text-align: center; padding: 10px;'><strong><a href="/member/cancelconfirm/other" style='color: #0c76a7;'>I'd rather not say</a></strong></div>


<?/*
<p>Don't forget to paste your referral link in your email signature and website for further exposure.</p>
<p style='margin-bottom: 0px;'>Remember, <strong>10 referrals</strong> gets you a <strong>FREE Lifetime Membership!</strong></p>

<a class="uiButton right" id="cancelbutton" style="margin: 10px 0 0;" href="/index/cancelconfirm">Cancel My Account Now</a
*/ ?>

	</div>
	
</div>


	

<script type="text/javascript">

	$(document).ready(function(){
		
		$('#cancelbutton').click(function(event){
			
			event.preventDefault();
			$.colorbox({width:"550px", height: "500px", inline:true, href:"#cancel_popup"});
			
		});
		
	});

</script>
