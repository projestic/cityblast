<?php
if ($this->member && $this->member->network_app_facebook) {
	$network_app_facebook = $this->member->network_app_facebook;
} else {
	$network_app_facebook = Zend_Registry::get('networkAppFacebook');
}
?>
<div class="contentBox">
	
	<div class="contentBoxTitle">
		<h1>Sorry!</h1>
	</div>
	
	<p><strong>We weren't able to correctly authenticate you.</strong></p>
	
	<p>You may be signed into the wrong Facebook account. Please try logging in again by clicking below, or contact our <a href="info@cityblast.com">support department</a> for help.</p>
	
	<a href="#" class="btn btn-primary btn-large pull-right facebook-login" style="width: 195px; margin-top: 10px;">Try again</a>
	
	<div class="clearfix"></div>
	
</div>
	