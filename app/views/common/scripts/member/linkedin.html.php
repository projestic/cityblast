<div class="grid_12">
    <div style="padding: 0;" class="box white clearfix">
        <h1 style="text-align: center; margin: 0; line-height: 60px; color: #666666">Need assistance? Call us at <span style="color: #333333;">1-888-712-7888</span>
        	<?/* or <a href="#">Live Chat Now!</a>*/?></h1>
    </div>
</div>

<div class="grid_12">

	<div class="box form clearfix">

		<div class="box_section">
			<h1>3. Add to LinkedIn.</h1>

			<p>ClientFinder can also bring you new clients through LinkedIn. LinkedIn is a great network for advertising your professional services, and a perfect fit
			for <?=COMPANY_NAME;?>. Add the power of ClientFinder to your LinkedIn below.</p>

		</div>

		<ul class="uiForm full clearfix" style="margin-top: -20px">
			<li class="uiFormRowHeader">LinkedIn</li>
			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname">Do you use LinkedIn?</li>
					<li class="field">We recommend you add ClientFinder to your LinkedIn account to generate more client leads.</li>
				</ul>
			</li>
			<li class="uiFormRow clearfix">
				<ul>
					<li class="buttons">
						<span style="float: left; margin-left: 350px; line-height: 40px;"><a href="/member/info" style="margin-right: 5px;">maybe later...</a></span>
						<a href="/linkedin/index" class="uiButton linkedin right">Add to LinkedIn</a>
					</li>
				</ul>
			</li>
		</ul>

	</div>

</div>