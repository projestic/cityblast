<div class="grid_12">
	
	<table width="1000" border="0" cellspacing="0" cellpadding="0" style="background: transparent url(/images/lp_logos/partner_logos_bg.png) 0 0 no-repeat;" id="logos_table">
		<tr>
			<td valign="middle" align="center" height="100" width="250">
				<a href="http://www.royallepage.ca/" target="_blank"><img src="/images/lp_logos/royal_lepage.png" /></a>
			</td>
			<td valign="middle" align="center" height="100" width="250">
				<a href="http://blog.lwolf.com/partners/news/lone-wolf-invests-in-cityblast-81759/" target="_blank"><img src="/images/lp_logos/lone_wolf.png" /></a>
			</td>
			<td valign="middle" align="center" height="100" width="250">
				<a href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/" target="_blank"><img src="/images/lp_logos/real_estate_talk_show.png" /></a>
			</td>
			<td valign="middle" align="center" height="100" width="250">
				<a href="http://www.seevirtual360.com/" target="_blank"><img src="/images/lp_logos/see_virtual.png" /></a>
			</td>
		</tr>
		<tr>
			<td valign="middle" align="center" height="100" width="250">
				<a href="http://www.techvibes.com/blog/cityblast-enables-canadian-real-estate-agents-to-build-their-business-through-social-media-2012-08-03" target="_blank"><img src="/images/lp_logos/techvibes.png" /></a>
			</td>
			<td valign="middle" align="center" height="100" width="250">
				<a href="http://www.canadianrealestatemagazine.ca" target="_blank"><img src="/images/lp_logos/real_estate_wealth.png" /></a>
			</td>
			<td valign="middle" align="center" height="100" width="250">
				<a href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/" target="_blank"><img src="/images/lp_logos/globe_and_mail.png" /></a>
			</td>
			<td valign="middle" align="center" height="100" width="250">
				<a href="http://retechulous.com/" target="_blank"><img src="/images/lp_logos/retechulous.png" /></a>
			</td>
		</tr>
	</table>
	
</div>

<script type="text/javascript">

	$(document).ready(function(){
		
		$('#logos_table img').each(function(){
			
			$(this).css('opacity', .6);
			$(this).mouseover(function(){ $(this).css('opacity', 1); });
			$(this).mouseout(function(){ $(this).css('opacity', .6); });
			
		});

	});
	
</script>