<? $member = isset($_SESSION['member']) && isset($_SESSION['member']->id) ? $_SESSION['member'] : new stdClass(); ?>
<ul class="dashboardForm dashboardTabbedForm clearfix">

	<li class="publishTabContainer clearfix">
		<a class="publishTab selected" style="cursor: pointer" rel="facebook"><span>Facebook</span></a>
		<a class="publishTab" style="cursor: pointer" rel="fanpage"><span>Fanpage</span></a>
		<a class="publishTab" style="cursor: pointer" rel="twitter"><span>Twitter</span></a>
		<a class="publishTab" style="cursor: pointer" rel="linkedIn"><span>LinkedIn</span></a>
	</li>
				
	<li class="publishTabContent facebook clearfix">
	
		<ul class="facebook clearfix">
			<li class="dashboardFieldname">Facebook <br/><i>Personal Page</i>

			<br/><span class="fieldhelp" style='margin-top: 20px;'>You may add your Business Fan Page below by selecting "<a id="manageFanpages" href="#manage_fanpages">Manage My Fan Page Settings</a>." </span>
			
			</li>
			<li class="dashboardField">
	
				<? if($this->hasFacebookPublishPermissions && !empty($member->facebook_publish_flag)) : ?>

					<div class="clearfix action_items" style="margin-top: 0px; padding-top: 0px; float:right;">
						<a href="#facebook-test" rel="facebook-test" listingid="134" class="uiButton aux small" style="width: 110px; text-align: left; margin-right: 9px;" title="Test publish to Facebook">
							<img style="background-position: -30px -60px" class="uiIcon icon-white icon-small" src="/images/blank.gif">Test publish
						</a>			
					</div>


					<h5 class="dashboardSettingHeading"><img src="/images/blank.gif" class="uiIcon" />Facebook is working.</h5>
					
					<p>It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
					business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
					<?php 
					if($this->lastFacebookPost && strpos($this->lastFacebookPost->fb_post_id, '_') !== false):
					list($member_id,$post_id) = explode("_",$this->lastFacebookPost->fb_post_id);
					?>
					<span class="hr">&nbsp;</span>
					<p style='line-height: 20px; font-weight: bold; font-size: 12px; opacity: 0.7;'>Your last Facebook post was published on <?php echo $this->lastFacebookPost->created_at->format("d-m-Y H:i:s");?>. <a href="http://www.facebook.com/<?php echo $member_id;?>/posts/<?php echo $post_id;?>" target="_blank">View on Facebook</a></p>				
					<?php
					endif; ?>
				<? elseif(!$this->hasFacebookPublishPermissions) : ?>
					<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Facebook Publishing Issue.  No leads.</h5>
					<p style="color: #990000;">Oh Dear! There seems to be a problem with your Facebook Publishing settings. You have not given us permission
						to publish content to your Facebook page. Without permission, it's impossible for us to keep your social media presence professional and up to date!</p>				
				
				<? else : ?>

					<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Facebook is Off.  No leads.</h5>
					<p style="color: #b7b7b7;">It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
					business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
					
				<? endif; ?>
				
			
				<span class="hr">&nbsp;</span>
				
				<? if($this->hasFacebookPublishPermissions && !empty($member->facebook_publish_flag)) : ?>
					
					<a href="/member/turn_off/facebook" class="dashboardSwitch" style="float: right;"><img src="/images/blank.gif" /></a>
					<span style="float: left; line-height: 40px; font-weight: bold; font-size: 11px; opacity: 0.6;"><strong style="color: #cc0000;">Note: </strong>Turning this 
						off means no new leads will be generated.</span>
				<? elseif($this->hasFacebookPublishPermissions && empty($member->facebook_publish_flag)) : ?>
	
					<a href="/member/turn_on/facebook" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
					<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
				<?php else: ?>
					<a href="/member/publish_permissions_enable" id="facebook_permissions" class="uiButton red right" style='font-size: 12px;'>Grant Permission to Publish to my Wall!</a>

				<? endif; ?>
				
			</li>
		</ul>
			
		<ul class="twitter clearfix">
			<li class="dashboardFieldname">Twitter

				<span class="fieldhelp">Have you recently reset your Twitter password? You'll need to hit the Reset Button below:</span>
				<a href="/twitter/?settings=1" class="dashboardReset"><img src="/images/blank.gif" /></a>
				
			</li>
			
			<li class="dashboardField">
			
				<? if( !empty($member->twitter_publish_flag) ) : ?>
				
					<?php if(empty($member->twitter_id)): ?>
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Twitter is Off.  No leads.</h5>
						
						<p style="color: #b7b7b7;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers 
							stay tuned with consistent and professional local real estate content.</p>

					<?php else: ?>
		
						<div class="clearfix action_items" style="margin-top: 0px; padding-top: 0px; float:right;">
							<a href="#twitter-test" rel="twitter-test" class="uiButton aux small" style="width: 110px; text-align: left; margin-right: 5px;" title="Test publish to Twitter">
								<img style="background-position: -30px -60px" class="uiIcon icon-white icon-small" src="/images/blank.gif">&nbsp;Test Publish</a>			
						</div>	
																		
						<h5 class="dashboardSettingHeading"><img src="/images/blank.gif" class="uiIcon" />Twitter is working.</h5>
		
						<p style="color: #b7b7b7;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers 
							stay tuned with consistent and professional local real estate content.</p>
	
						<?php
						if( $this->lastTwitterPost ):	
					?>
					<span class="hr">&nbsp;</span>
					<p style='line-height: 20px; font-weight: bold; font-size: 12px; opacity: 0.7;'>Your last Twitter post was published on <?php echo $this->lastTwitterPost->created_at->format("d-m-Y H:i:s");?>. <a href="https://twitter.com/<?php echo $this->lastTwitterPost->member->twitter_screen_name;?>/status/<?php echo $this->lastTwitterPost->twitter_post_id;?>" target="_blank">View on Twitter</a></p>				
					<?php
					endif; ?>

					<? endif; ?>
				
				<? else : ?>

	
					<?php if(empty($member->twitter_id)): ?>
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Twitter is Off.  No leads.</h5>
						
						<p style="color: #b7b7b7;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers stay 
							tuned with consistent and professional local real estate content.</p>
						
					<?php else: ?>
						
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Twitter is Off.  No leads.</h5>
	
						<p style="color: #b7b7b7;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers stay 
							tuned with consistent and professional local real estate content.</p>
	
					<? endif; ?>
					
				<? endif; ?>
				
				<span class="hr">&nbsp;</span>
				
				<? if( !empty($member->twitter_publish_flag) ) : ?>
				
					<?php if(empty($member->twitter_id)): ?>
						<a href="/twitter/?settings=1" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
					<?php else: ?>
						<a href="/member/turn_off/twitter" class="dashboardSwitch" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold; font-size: 11px; opacity: 0.6;"><strong style="color: #cc0000;">Note: </strong>Turning 
							this off means no new leads will be generated.</span>
					<? endif; ?>
				
				<? else : ?>
	
					<?php if(empty($member->twitter_id)): ?>
						<a href="/twitter/?settings=1" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
					<?php else: ?>
						<a href="/member/turn_on/twitter" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>																		
					<? endif; ?>
					
				<? endif; ?>
				
			</li>
		</ul>

	
		<ul class="linkedIn clearfix">
			<li class="dashboardFieldname">LinkedIn


				<span class="fieldhelp">Have you recently reset your LinkedIn password? You'll need to hit the Reset Button below:</span>
				<a href="/linkedin/?settings=1" class="dashboardReset"><img src="/images/blank.gif" /></a>
			
			</li>
			
				<? if( !empty($member->linkedin_publish_flag) ) : ?>
	
					<?php if(empty($member->linkedin_id)): ?>									
						<li class="dashboardField">
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />LinkedIn is Off.  No leads.</h5>
						<p style="color: #b7b7b7;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying 
							visible and active in this network is highly recommended.</p>
					<?php else: ?>
						<li class="dashboardField">

						<div class="clearfix action_items" style="margin-top: 0px; padding-top: 0px;float:right;">
							<a href="#linkedin-test" rel="linkedin-test" listingid="134" class="uiButton aux small" style="width: 110px; text-align: left; margin-right: 5px;" title="Test publish to Linkedin">
								<img style="background-position: -30px -60px" class="uiIcon icon-white icon-small" src="/images/blank.gif">Test publish
							</a>		
						</div>							
	
							
						<h5 class="dashboardSettingHeading"><img src="/images/blank.gif" class="uiIcon" />LinkedIn is working.</h5>
						<p style="color: #b7b7b7;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying 
							visible and active in this network is highly recommended.</p>

						<?php
						if( $this->lastLinkedinPost ):	
						?>
						<span class="hr">&nbsp;</span>
						<p style='line-height: 20px; font-weight: bold; font-size: 12px; opacity: 0.7;'>Your last Linkedin post was published on <?php echo $this->lastLinkedinPost->created_at->format("d-m-Y H:i:s");?>. </p>				
						<?php
						endif; 
					endif; ?>
				
				<? else : ?>
	
					<?php if(empty($member->linkedin_id)): ?>
					
						<li class="dashboardField">
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />LinkedIn is Off.  No leads.</h5>			
						<p style="color: #b7b7b7;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying 
							visible and active in this network is highly recommended.</p>
					<?php else: ?>
						
						<li class="dashboardField">
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />LinkedIn is Off.  No leads.</h5>
						<p style="color: #b7b7b7;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying 
							visible and active in this network is highly recommended.</p>
					<? endif; ?>
					
				<? endif; ?>
				
				<span class="hr">&nbsp;</span>

				<? if( !empty($member->linkedin_publish_flag) ) : ?>
				
					<?php if(!empty($member->linkedin_id)): ?>
						
						<a href="/member/turn_off/linkedin" class="dashboardSwitch " style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold; font-size: 11px; opacity: 0.6;"><strong style="color: #cc0000;">Note: </strong>Turning 
							this off means no new leads will be generated.</span>
															
					<?php else: ?>
				
						<a href="/member/turn_on/linkedin" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
						
				
					<? endif; ?>
				
				<? else : ?>
				
					<?php if(empty($member->linkedin_id)): ?>
						<a href="/linkedin?settings=1" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>									
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
					<?php else: ?>
					
						<a href="/member/turn_on/linkedin" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
						
					<? endif; ?>
					
				<? endif; ?>
			</li>
		</ul>
		
		<ul id="fb_fanpages" class="fanpage clearfix">
		<? if(isset($member)):

			//Set up the FACEBOOK VARIABLES...
			$publish_array['access_token'] = $member->access_token;
			
			$facebook = $member->network_app_facebook->getFacebookAPI();

			//echo "ACCESS TOKEN:".$member->access_token . "<BR>";
			
			if(isset($member->access_token) && !empty($member->access_token)):
						
				$api_url		= "https://graph.facebook.com/me/accounts?access_token=" . $member->access_token;				
				$accounts = null;
				
				if($account_info = @file_get_contents($api_url)):
				
					$accounts = json_decode($account_info);
					
					$page_count = 0;
					foreach ($accounts->data as $account)
					{
						if($account->category != "Application")
						{
							$page_count++;
						}
					}			


					if( !$this->hasManagePagesPermission) {
					?>

							<li class="dashboardFieldname">Fanpage <span class="notification">Error!</span>								
								<span class="fieldhelp">According to our records, you have not given us permission to manage your Fanpages.</span>							
							</li>
							
							<?/*<ul class="linkedIn clearfix">*/?>
								
							<li class="dashboardField">				

									<h3 style="font-size: 18px; line-height: 32px; display: inline-block; margin-top: 15px; ">
										We do not have permission to manage your Fan Pages  
									</h3>
																		

								<a href="/member/fanpages_permissions_enable" id="fanpages_permissions" class="uiButton red" style='margin-top: 15px; font-size: 12px; float: right;display: block;width: 275px;'>Grant Permission to Publish to my Wall</a>
							
							</li>
						
					<?php } elseif( $page_count > 0) { ?>

						<div class="clearfix" id="fb_fanpages_load" style="padding: 20px; text-align: center;"></div>
			
						<script type="text/javascript">
							$(document).ready(function(){ 
								$('a.publishTab[rel="fanpage"]').click(function(e){
									$('#fb_fanpages').load('/member/settings_fanpages'); 
									//$('#fb_fanpages_load').removeClass('black10'); 
									$('#fb_fanpages_load').html('<img src="/images/dashboard_loading.gif" height="150" width="150" style="margin: 0 260px 0;" />'); 
									e.preventDefault();
								});
								<?php 
								$reopenfanpage = new Zend_Session_Namespace('reopenfanpage');
								if(isset($reopenfanpage) && isset($reopenfanpage->data) & isset($reopenfanpage->data->condition) && $reopenfanpage->data->condition == 'yes'){
								?>
								$('#load_fb_fanpages').trigger('click');
								<?php
									$reopenfanpage->data->condition = NULL;
								}
								?>
							});
						</script>
					<? } else { ?>
						<div class="box black10 clearfix" id="fb_fanpages_load" style="padding: 20px; text-align: center;">
							<h3 style="text-align: center; font-size: 18px; line-height: 32px; margin: 0px; padding-top: 3px; display: inline-block;">
								Oh Dear
							</h3>
							<p>It doesn't seem that you have any Facebook Fanpages for us to manage.</p>
						</div>
					<? }  ?>
						
					</ul>
								
				<? endif; ?>
				
			<? endif; ?>
			
		<? endif; ?>
	</li>
							
</ul>

<script type="text/javascript">
	$(document).ready(function(){ 
		var $PublishTabs = $('a.publishTab');
		var $PublishTabContent = $('li.publishTabContent'); 
		$PublishTabs.click(function(e){
			e.preventDefault();
			$PublishTab = $(this);
			$PublishTabs.removeClass('selected');
			$PublishTab.addClass('selected');
			$PublishTabContent.removeClass().addClass('publishTabContent clearfix ' + $PublishTab.attr('rel'));
		});
		
		$('#manageFanpages').click(function(e){
			e.preventDefault();
			$PublishTabs.filter('[rel="fanpage"]').click();
		});
	});
</script>