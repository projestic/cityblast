<?php if($this->blast['error']==FALSE): ?>
 <h2>Published To Linkedin</h2>
 <p>Successfully published to Linkedin</p>
   <a class="uiButton right" href="<?php echo $this->blast['postlink'];?>">View your LinkedIn profile</a>
 <?php else: ?>
 <h2>Error Publishing To Linkedin</h2>
 <p>Unable to publish to Linkedin: <?php echo $this->blast['message'];?></p>
 <?php endif;?>