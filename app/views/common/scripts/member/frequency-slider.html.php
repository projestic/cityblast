<? if(isset($this->fqnotification)) : ?>
	<p><?=$this->fqnotification; ?></p>
<? endif; ?>
<div class="postNumberContainer pull-left">
	<input type="hidden" name="frequency_id" id="frequency_id" value="<?= (isset($this->member) && $this->member->frequency_id) ? $this->member->frequency_id : 4 ?>" />
	<h4>How often do you want us to post?</h4>
	<ul class="postNumbers unstyled clearfix">
		<li>1</li>
		<li>2</li>
		<li>3</li>
		<li>4</li>
		<li>5</li>
		<li>6</li>
		<li>7</li>
	</ul>
	<div class="postNumberSliderWrapper">
		<div class="postNumberSlider"></div>
	</div>
</div>
<div class="noteContainer pull-left">
	<h6>Post <span id="frequency_amount"></span> per week.</h6>	
	<p id="frequency_message"></p>
	<img src="/images/new-images/note-right-arrow.png" class="note-right-arrow"/>
</div>
<script type="text/javascript">

		function onFrequencySliderChange(v)
		{
			var s = '';
			if (v != 1) s = 's';

			$('#frequency_amount').text(v + ' day' + s);

			var messages = { 
							 1: "The bare minimum.",
							 2: "Just adding a bit of extra content each week.",
							 3: "Good for if you don't frequently post yourself.",
							 4: "A good place to start - recommended.",
							 5: "Good if you post a lot of your own updates.",
							 6: "This is quite a lot of posts.",
							 7: "Are you sure?  This is a lot of business posts.",
						   };
		
			$('#frequency_message').html(messages[v]);
		}
		
	$(document).ready( function () {
		$('.postNumberSlider').slider({
			range: "min",
			value: <?= (isset($this->member) && $this->member->frequency_id) ? $this->member->frequency_id : 4 ?>,
			min: 1,
			max: 7,
			step: 1,
			change: function(event, ui) {
				$('#frequency_id').val(ui.value);
			}
		}).bind('slide', function(event, ui) {
			onFrequencySliderChange(ui.value);
		});
		
		onFrequencySliderChange(<?= (isset($this->member) && $this->member->frequency_id) ? $this->member->frequency_id : 4 ?>);

	});


</script>