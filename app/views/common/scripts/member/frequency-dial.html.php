			<li class="dashboardRow clearfix">
				<ul>
					<li class="dashboardFieldname">
						ClientFinder Frequency
						<span class="fieldhelp"><?=COMPANY_NAME;?> publishes new posts to your Wall or Feed as often as you choose.  A higher publishing frequency attracts more new leads.</span>
					</li>
					<li class="dashboardField clearfix">
						
						<? if(isset($this->fqnotification)) : ?>
							<p style="color: #790000; font-style: italic; margin-bottom: -10px;"><?=$this->fqnotification; ?></p>
						<? endif; ?>
						
							
							<div class="dialWrapper" style="width: 120px; height: 130px; padding: 30px 20px 0; float: left; background: transparent url(/images/frequency_dial_bg.png) 0 10px no-repeat; cursor: pointer;">
						
								<input type="text" name="frequency_id" class="dial" id="frequency_id" value="4" />
							
							</div>
				
							<div style="width: 290px; float: left; margin-left: 20px;">
							
								<?
									$v = 4;
									
									if($v==1){
										$frequency_amount = '1 day';
										$frequency_message = 'This is the absolute minimum.';
									}
									else if($v < 3){
										$frequency_amount = $v . ' days';
										$frequency_message = 'Moving on up! Updating few times a week will help with your social media presence.';
									}
									else if($v < 5){
										$frequency_amount = $v . ' days';
										$frequency_message = 'This is really what we suggest as the \'bare minimum.\'';
									}
									else if($v <= 6){
										$frequency_amount = $v . ' days';
										$frequency_message = 'Now we\'re talking! This is our \'business\' level. Watch the leads flow in';
									}
									else if($v == 7){
										$frequency_amount = $v . ' days';
										$frequency_message = "Your best choice! You've decided to maximize your earning potential. Great idea!";
									}
								
								?>
							
								<h3 style="margin-top: 10px;">Publish <span id="frequency_amount"><?=$frequency_amount?></span> a week.</h3>	
								<div id="frequency_message" style="line-height: 16px; height: 32px;"><?=$frequency_message?></div>
								
		
											
							</div>
							
						</form>
						
					</li>
				</ul>
			</li>