<div class="grid_12">
	
	<div class="box form clearfix">
	
		<div class="box_section">
			<h1>Oh Dear! There's a problem.</h1>
			
			<p>It seems you forgot to tell us what city you would like to publish to. That's easy to fix, just select your from the dropdown below.</p>
			<p><a href="http://www.facebook.com/blog.php?post=41735647130" target="_new" style="font-style:italic;">What is Facebook Connect?</a></p>
			
		</div>
		
		<ul class="uiForm full clearfix" style="margin-top: -20px">
			<li class="uiFormRowHeader">Select Your City</li>
			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname">Blast To<span style="font-weight: bold; color: #990000">*</span></li>
					<li class="field">
						<span class="uiSelectWrapper">
							<span class="uiSelectInner">
								<select name="pubcity" id="pubcity" class="uiSelect" style="width: 590px;">
									<option value="*" <?php echo (empty($this->selectedCity)||$this->selectedCity=="*") ? "selected" : "";?>>Select Your City.</option>
									<?php if(isset($this->blast_cities) && is_array($this->blast_cities)): ?>
									<?php foreach($this->blast_cities as $city): ?>
										<option value="<?php echo $city->id;?>" <?php echo (!empty($this->selectedCity)&&$this->selectedCity==$city->id) ? "selected" : "";?>><?php echo $city->name;?></option>
									<?php endforeach; ?>
									<?php endif ?>
								</select>
							</span>
						</span>
						<div id='city_error' class="clearfix" style='display: none; margin-top: 5px;'>
							<hr />
							<div class='error' style="padding-left: 4px; color: #CC0000"><strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> select a city</div><div class='clearfix'>&nbsp;</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="uiFormRow clearfix">
				<ul>
					<li class="buttons">
						<a href="/member/clientfind-signup/" class="uiButton facebook right" id="facebook_signup_button">Connect With Facebook</a>
					</li>
				</ul>
			</li>
		</ul>

	</div>

</div>

<script type="text/javascript"  language="javascript">

	$(document).ready(function() {

	    	$('#facebook_signup_button').click(function(event) {
		
				event.preventDefault();
				$("#pubcity").removeClass('error');
				$('#city_error').hide();

				if( $("#pubcity option:selected").val() == "*" || $("#pubcity option:selected").val() == "") {
					 $("#pubcity").addClass('error');
					 $('#city_error').show();
				} else {

					FB.login(function(response) {

						if (response.authResponse) {
														
							//console.log(response.authResponse); 							
							//alert($("#pubcity option:selected").val());
							//alert('ABOUT TO GO!');							
							
							$.ajax({

								type: "POST",
								url: "/member/fbsignup",
								data: "access_token="+response.authResponse.accessToken+"&city_id="+$("#pubcity option:selected").val()+"&href=blast_only",
								dataType: "json",

								success: function(msg)
								{
									if (msg.redirectBackTo) {
										window.location =	msg.redirectBackTo;
									} else {
										$("#pubcity").addClass('error');
										$('#city_error').show();
									}
								},

								error: function(xhr, errText) 
								{
									alert("Error:"+errText)
								}
							});

						}

					}, {scope: '<?php echo FB_SCOPE ?>'});
				}
			});
	});
</script>