
<form name="upfrontsettings" id="upfrontsettings" onsubmit="return false;" method="POST" style="margin: 0px !important; padding: 0px !important;">
<input type="hidden" name="locality" value="" id="locality" /><? /* locality == city */ ?>
<input type="hidden" name="administrative_area_level_1" value="" id="administrative_area_level_1" /><? /* administrative_area_level_1 == state/province */ ?>
<input type="hidden" name="country" value="" id="country" />
	

	<div class="contentBox " id="step1">
		<div class="contentBoxTitle">
			

			<?/*<h3><?=COMPANY_NAME;?>'s Social Media Experts always keep you fresh and up-to-date. Tell us...</h3>*/?>
			<?/*<h3>Our Social Experts Are Ready To Create A Custom Social Media Campaign For You!</h3>*/?>


			<? if (strtolower($this->controller_name) == "member") : ?>
				<h1>You're 30 Seconds Away From Your Own Personal Social Media Marketing Manager.</h1>
			<? elseif ($this->controller_name == "affiliate" && $this->action_name == "index") : ?>
				<h2>Start Your <u>FREE</u> 14 Day Trial of <?=COMPANY_NAME;?>.</h2>
			<? endif; ?>
			
			<?				
				if(isset($_COOKIE['promo_code']))
				{
					if($promo = Promo::find_by_promo_code($_COOKIE['promo_code']))
					{
						echo "<h3 style='color: #5fb004'>Lock In ";
						if($promo->type == "FIXED_AMOUNT") echo "$".number_format($promo->amount, 2);
						else echo number_format($promo->amount) . "%";
						
						echo " in Savings. Promo Code: <u><i>".$_COOKIE['promo_code']."</i></u></h3>";	
					}
				}
			?>
			
		</div>
		
		<? // SECTION - NOT COMPLETED ?>
		<div class="stepBox step1">
			<div class="stepTitle">
				<div class="titleNumber">
					<span>1</span>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Tell us where you work.</h2>
			</div>
			<div class="stepContent clearfix" style="margin-top: 30px;">
				<div class="ladyCartoon pull-left">
					<img src="/images/new-images/step2-lady-cartoon.png">
				</div>
				<div style="overflow: hidden;">
					<ul class="uiForm full">
						<li class="clearfix">
							<ul>
								<li class="fieldname cc">
									<span style="color: #990000;">*</span>What City and State or Province do you work in? Please select a known city from the dropdown.
								</li>
								<li class="field cc clearfix">
									<input id="defaultpubcity" name="location_input" type="text" class="geocomplete uiInput"/><br/>
								</li>
							</ul>
						</li>
						<li class="clearfix">
							<div class="map_canvas"></div>
						</li>
						<li class="uiFormRow clearfix">
							<ul class="clearfix">
								<li class="clearfix">
									<hr />
									<a href="javascript:void(0);" class="btn btn-primary btn-large pull-right disabled next-section" id="tryusnow_step1_btn"><i class="icon-chevron-sign-down icon-fixed-width"></i> Next</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		
		<? // SECTION - COMPLETED ?>
		<div class="stepBox step1" style="display: none;">
			<div class="stepTitle">
				<div class="titleNumber">
					<i class="icon-ok" style="color: #090;"></i>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Tell us where you work.</h2>
			</div>
			<p><strong>Address:</strong> <span id="address_holder"></span><a href="javascript:void(0);" id="edit_address" style="margin-left: 10px;"><i class="icon-pencil"></i> Edit</a></p>
		</div>
		
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" />
		<img src="/images/new-images/pulsating-arrow.png" class="pulsatingArrow" />
		
	</div>
	
	
	<div class="contentBox" id="step2" style="display: none;">
		
		<? // SECTION - NOT COMPLETED ?>
		<div class="stepBox step2">
			<div class="stepTitle">
				<div class="titleNumber">
					<span>2</span>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Tell us how often we should post.</h2>
			</div>
			<div class="stepContent clearfix" style="margin-top: 30px;">
				<div class="ladyCartoon pull-left">
					<img src="/images/new-images/step2-lady-cartoon.png">
				</div>
				<div style="overflow: hidden;">
					<ul class="uiForm full">
						<li class="clearfix">
							<ul>
								<li class="fieldname cc">
									Our experts say 4 is the magic starting point.
								</li>
								<li class="field cc clearfix">
									<div class="postNumberContainer pull-left">
										<input type="hidden" name="frequency_id" id="frequency_id" value="<?= (isset($this->member) && $this->member->frequency_id) ? $this->member->frequency_id : 4 ?>" />
										<ul class="postNumbers unstyled clearfix">
											<li>1</li>
											<li>2</li>
											<li>3</li>
											<li>4</li>
											<li>5</li>
											<li>6</li>
											<li>7</li>
										</ul>
										<div class="postNumberSliderWrapper">
											<div class="postNumberSlider"></div>
										</div>
									</div>
									<div class="noteContainer pull-left">
										<h6>Post <span id="frequency_amount"></span> per week.</h6>	
										<p id="frequency_message"></p>
										<img src="/images/new-images/note-right-arrow.png" class="note-right-arrow"/>
									</div>
								</li>
							</ul>
						</li>
						<li class="uiFormRow clearfix">
							<ul class="clearfix">
								<li class="clearfix">
									<hr />
									<a href="javascript:void(0);" class="btn btn-primary btn-large pull-right next-section" id="tryusnow_step2_btn"><i class="icon-chevron-sign-down icon-fixed-width"></i> Next</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<? // SECTION - COMPLETED ?>
		<div class="stepBox step2" style="display: none;">
			<div class="stepTitle">
				<div class="titleNumber">
					<i class="icon-ok" style="color: #090;"></i>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Tell us how often we should post.</h2>
			</div>
			<p><span id="frequency_holder"></span><a href="javascript:void(0);" id="edit_frequency" style="margin-left: 10px;"><i class="icon-pencil"></i> Edit</a></p>
		</div>
		
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" style="display: block;" />
		<img src="/images/new-images/pulsating-arrow.png" class="pulsatingArrow" />
	</div>
	

	<div class="contentBox" id="step3" style="display: none;">
		
		<? // SECTION - NOT COMPLETED ?>
		<div class="stepBox step3">
			<div class="stepTitle">
				<div class="titleNumber">
					<span>3</span>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Tell us what you want us to post.</h2>
			</div>
			<p>Your Social Media Expert has many options for content she can post for you.</p>
			<p><span class="bold">Choose at least 5</span> from below to get started.</p>
			<div class="stepContent clearfix" style="margin-top: 30px;">
				<div class="ladyCartoon pull-left">
					<img src="/images/new-images/step1-lady-cartoon.png">
				</div>
				<div>
					<div class="clearfix">
						<? echo $this->render('member/content-select.html.php'); ?>
					</div>
					<hr />
					<a href="javascript:void(0);" class="btn btn-primary btn-large pull-right next-section" id="tryusnow_step3_btn"><i class="icon-chevron-sign-down icon-fixed-width"></i> Next</a>
				</div>
			</div>
		</div>
		
		<? // SECTION - COMPLETED ?>
		<div class="stepBox step3" style="display: none;">
			<div class="stepTitle">
				<div class="titleNumber">
					<i class="icon-ok" style="color: #090;"></i>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Tell us what you want us to post.</h2>
			</div>
			<p><span id="content_holder"></span><a href="javascript:void(0);" id="edit_content" style="margin-left: 10px;"><i class="icon-pencil"></i> Edit</a></p>
		</div>
		
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" />
		<img src="/images/new-images/pulsating-arrow.png" class="pulsatingArrow" />
	</div>
	
	
	<div class="contentBox" id="step4" style="display: none;">
		<div class="stepBox step4">
			<div class="stepTitle">
				<div class="titleNumber">
					<span>4</span>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Submit your preferences to your new Social Media Expert.</h2>
			</div>
			<div class="stepContent clearfix" style="margin-left: 130px;">
				<div class="noteContainer pull-left">
					<h6>What's This:</h6>
					<p>You'll be asked to connect your Facebook profile with <?=COMPANY_NAME;?>. That's so we know where to post the content and updates you have previously chosen in Step 1. It's as simple as  that!</p>
					<img src="/images/new-images/note-left-arrow.png" />
				</div>
				<div class="ladyCartoon pull-left" style="margin: 0 12px 0 13px;">
					<img src="/images/new-images/step3-lady-cartoon.png" />
				</div>
				<div class="btnContainer pull-left">
					<h4>Connect with Facebook to continue...</h4>
					<div class="termsBox"><input type="checkbox" name="terms" id="terms" checked="checked"> I agree to the <a href="/index/terms" >Terms and Conditions</a>.</div>
					<a href="#" class="uiButtonNew" id="facebook_signup_button">Send.</a>
				</div>
				
			</div>
			
		</div>
	</div>
	

	<? /* echo $this->render('logos-responsive-small.html.php'); */	?>
	<div style="margin-bottom: 25px;">&nbsp;</div>

	
</form>

	


<? echo $this->render('member/upfront-settings.html.php'); ?>
<?
	//http://ubilabs.github.io/geocomplete/
	
	//Do we want drag and drop?
	//http://xilinus.com/jquery-addresspicker/demos/
?>


<style>

.map_canvas:after 
{
    color: #999999;
    /*content: "Type in an address in the input above.";*/
    display: block;
    font-size: 2em;
    padding-top: 170px;
    text-align: center;
}
.map_canvas {
    border: 1px solid #e7e7e7;
    height: 200px;
    margin: 10px 0;
    width: 100%;
    background-color: #f1f1f1 !important;
    overflow: hidden;
    position: relative;   
	border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px; 
}
.uiForm .field, .uiForm .fieldname {
	padding-left: 0px!important;
}
	
</style>

<script>

	$(function(){

		var city 			= null;
		var country_code 	= null;	
		var region 		= null;
			
		google.load("search", "1.x", {callback: initialize});
		
		function initialize()
		{

			if (google.loader.ClientLocation)
			{
				var lat = google.loader.ClientLocation.latitude;
				var long = google.loader.ClientLocation.longitude;
				
				city 		= google.loader.ClientLocation.address.city;
				country 		= google.loader.ClientLocation.address.country;
				region 		= google.loader.ClientLocation.address.region;
				country_code 	= google.loader.ClientLocation.address.country_code;
				
				$("#locality").val(city);
				$("#administrative_area_level_1").val(region); //State/Province
				$("#country").val(country);
				
				
			
				
			}
			else 
			{ 
				//Random default
				city 		= "Chicago";
				region 		= "IL";
				country_code 	= "USA";		
			} 

			var options = {
				map: ".map_canvas"
			};
			

			$(".geocomplete").geocomplete({
				map: ".map_canvas",
				details: "form",
				location: city + ", " + region + ", " + country_code,
			});	

	
			$(".geocomplete").geocomplete(options).bind("geocode:result", function(event, result)
			{
				var country = '';
				var state   = '';
				var city    = '';
				
				$.each(result.address_components, function (ix, item) {
					if ($.inArray('locality', item.types) > -1) {
						city = item.long_name;
					} else if ($.inArray('administrative_area_level_1', item.types) > -1) {
						state = item.long_name;
					} else if ($.inArray('country', item.types) > -1) {
						country = item.short_name;
					} 
				});

				/***********
				if (country != 'US' && country != 'CA') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>We're sorry, but we don't support countries outside of North America at this time!</p>"});
					return false;
				} *********/

				if (state == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>We need your city and state/province to continue.</p>"});
					return false;
				}

				if (city == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>We need your city to continue.</p>"});
					return false;
				}

				step1_complete = true;
				$('#tryusnow_step1_btn').removeClass('disabled');
				$('#step1').find('.boxBottomArrow').show();

			})
			.bind("geocode:multiple", function(event, status){
				console.log('multiple');
			})

			// City validation disabled 2013.10.17

			/* .bind("geocode:error", function(event, status){
				$('#tryusnow_step1_btn').addClass('disabled');
				step1_complete = false;
				$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>This part is a bit tricky! You must select a city <span style='color: #ff0000;'>from the Google Maps autocomplete dropdown</span>.</p><p>You'll know you've successfully picked a city when the <span style='color: #ff0000;'>red pin</span> shows up on the Google map!</p>"});
			}); */
				
		}

	});
	
</script>

<script type="text/javascript">

	var step1_complete = false;
	var step2_complete = false;
	var step3_complete = false;
	
	$(document).ready(function()
	{
		
		// STEP 1 OPERATIONS
		
			$(".geocomplete").on('keyup', function(){
				// disable next on any change, don't reenable until an autocomplete value has been selected.
				$('#administrative_area_level_1, #country, #locality').val('');
				
				step1_complete = false;
				$('#tryusnow_step1_btn').addClass('disabled');
				$('#step1').find('.boxBottomArrow').hide();
				
				// HIDE OTHER SECTIONS
				$('#step1').find('a.next-section').html('<i class="icon-chevron-sign-down icon-fixed-width"></i> Next').addClass('btn-primary');
				$('#step2').slideUp();
				$('#step3').slideUp();
				$('#step4').slideUp();
			});
			
			$('#tryusnow_step1_btn').on('click', function(){
				var location = $(".geocomplete").val().trim();
				if (location.length){
					$('#address_holder').text(location);
					$($('.step1')[0]).slideUp();
					$($('.step1')[1]).slideDown();
					
					// OPEN STEP 2
					$('#step2').slideDown();
					
					// OPEN 3 & 4 IF 2 & 3 ARE GOOD
					if( step2_complete == true && step3_complete == true ) {
						$('#step3').slideDown();
						$('#step4').slideDown();
					}
						
					// OPEN JUST 3 IF 2 IS GOOD
					else if( step2_complete == true && step3_complete == true ) {
						$('#step3').slideDown();
					}
					$('html, body').stop().animate({ scrollTop: $('#step1 .stepTitle').offset().top - 20 }, {duration: 400, queue: false});
				} else {
					$('.geocomplete').trigger('geocode:error');
				}

				_gaq.push(('_trackPageview','click-next-1'));
			});
			
			$('#edit_address').on('click', function(){
				$($('.step1')[1]).slideUp();
				$($('.step1')[0]).slideDown();
				$('html, body').stop().animate({ scrollTop: $('#step1 .stepTitle').offset().top - 20 }, {duration: 400, queue: false});
				$('#step1').find('a.next-section').html('<i class="icon-save icon-fixed-width"></i> Save').removeClass('btn-primary');
			});
			
			
		// STEP 2 OPERATIONS
			
			$('#tryusnow_step2_btn').on('click', function(){
				
				step2_complete = true;
				$($('.step2')[0]).slideUp();
				$($('.step2')[1]).slideDown();
				$('#step3').slideDown();
				$('#frequency_holder').html('<strong>' + $('#frequency_id').val() + ' times per week: </strong>' + frq_messages[$('#frequency_id').val()]);
				$('html, body').stop().animate({ scrollTop: $('#step2 .stepTitle').offset().top - 20 }, {duration: 400, queue: false});

				_gaq.push(('_trackPageview','click-next-2'));
			});
			
			$('#edit_frequency').on('click', function(){
				$('#step2').find('a.next-section').html('<i class="icon-save icon-fixed-width"></i> Save').removeClass('btn-primary');
				$($('.step2')[1]).slideUp();
				$($('.step2')[0]).slideDown();
				$('html, body').stop().animate({ scrollTop: $('#step2 .stepTitle').offset().top - 20 }, {duration: 400, queue: false});
			});
		
		
		// STEP 3 OPERATIONS
			
			var $SettingChkbx = $('#step3 ul.settings input[type="checkbox"]');
			
			$('#tryusnow_step3_btn').on('click', function(){
				
				if(!$(this).hasClass('disabled')){
					$($('.step3')[0]).slideUp();
					$($('.step3')[1]).slideDown();
					$('#step4').css('background', '#fbfbfc');
					$('#step4').slideDown(
						function(){
							for (var i = 0; i < 3; i++ ) {
								$("#step4")
									.animate( { backgroundColor: "#ffffe5" }, 300 )
									.animate( { backgroundColor: "#fbfbfc" }, 300 );
							}
						}
					);
					
					// SET THE LIST OF CONTENT TYPES HERE
					var checkedVals = $('ul.settings :checkbox:checked').map(function() {
						return $(this).nextAll('label').text();
					}).get();
					var lastVal = checkedVals.pop();
					$('#content_holder').html('<strong>Content Types: </strong> ' + checkedVals.join(", ") + ' and ' + lastVal + '.');
					$('html, body').stop().animate({ scrollTop: $('#step4 .stepTitle').offset().top - 20 }, {duration: 400, queue: false});
				}

				_gaq.push(('_trackPageview','click-next-3'));
			});
			
			$SettingChkbx.change(function()
			{
				var $this = $(this);
				var $Parent = $this.parent();
				if($this.prop('checked') || $this.attr('checked') == 'checked')
				{				
					$Parent.addClass('selected');
				}
				else{
					$Parent.removeClass('selected');
				}
				
				if($SettingChkbx.filter(':checked').length > 4){
					step3_complete = true;
					$('#tryusnow_step3_btn').removeClass('disabled');
					$('#step3').find('.boxBottomArrow').show();
				}
				else{
					$('#step3').find('a.next-section').html('<i class="icon-chevron-sign-down icon-fixed-width"></i> Next').addClass('btn-primary');
					step3_complete = false;
					$('#tryusnow_step3_btn').addClass('disabled');
					$('#step3').find('.boxBottomArrow').hide();
					$('#step4').slideUp();
				}
				
			}).change();
			
			$('#edit_content').on('click', function(){
				$('#step3').find('a.next-section').html('<i class="icon-save icon-fixed-width"></i> Save').removeClass('btn-primary');
				$($('.step3')[1]).slideUp();
				$($('.step3')[0]).slideDown();
			});

		$('#facebook_signup_button').click(function(event){
			_gaq.push(('_trackPageview','click-next-4'));
		});
		
	});
	
	var frq_messages = { 
					 1: "The bare minimum.",
					 2: "Just adding a bit of extra content each week.",
					 3: "Good if you post a lot of your own updates.",
					 4: "A good place to start - recommended.",					 
					 5: "Good for if you don't frequently post yourself.",
					 6: "This is quite a lot of posts.",
					 7: "Are you sure?  This is a lot of business posts.",
				   };

	function onFrequencySliderChange(v)
	{
		var s = '';
		if (v != 1) s = 's';

		$('#frequency_amount').text(v + ' day' + s);
	
		$('#frequency_message').html(frq_messages[v]);
	}
		
	$(document).ready( function () {
		$('.postNumberSlider').slider({
			range: "min",
			value: <?= (isset($this->member) && $this->member->frequency_id) ? $this->member->frequency_id : 4 ?>,
			min: 1,
			max: 7,
			step: 1,
			change: function(event, ui) {
				$('#frequency_id').val(ui.value);
			}
		}).bind('slide', function(event, ui) {
			onFrequencySliderChange(ui.value);
		});
		
		onFrequencySliderChange(<?= (isset($this->member) && $this->member->frequency_id) ? $this->member->frequency_id : 4 ?>);
		
	});
	
	
</script>