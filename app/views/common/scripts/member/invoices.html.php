<div class="grid_12">

	<div class="box form clearfix">

		
		<ul class="uiForm full clearfix">
			<li class="uiFormRowHeader">Invoices <span style="float: right;"><a href="<?php echo $this->url( array("controller"=>"member","action"=>"payment"),"default");?>" class="uiButton right">Update Your Credit Card</a></span></li>
			
			<li class="uiFormRow clearfix" style="margin-top: 14px; width: 960px;">
				<ul style="width: 960px;">
			
					<li style="width: 960px; margin-left: 16px;">

						
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="datatable" style="border: 1px solid #b7b7b7;">
						
							<tr>
								<th scope="col" style="text-align: left">Invoice #</th>
								<?/*<th width="15%" scope="col">Name</th>*/?>
								<th scope="col" style="text-align: left">Date</th>
								<th scope="col" style="text-align: left">Type</th>
								<th scope="col" style="text-align: right">Amount</th>
								<th scope="col" style="text-align: right">Taxes</th>
								<th scope="col" style="text-align: right">Total</th>
								<?/*<th width="30%" scope="col">Message</th>*/?>
							</tr>
							
							<?php $i=0; foreach($this->paginator as $payment): ?>
							
								<tr>
									<td scope="col"><?php echo $payment->id;?></td>
									<? /*<td scope="col"><?php echo $payment->first_name . " " . $payment->last_name;?></td>*/?>
									<td scope="col">
										<?php 
											if(isset($payment->created_at) AND !empty($payment->created_at))	echo $payment->created_at->format("Y-m-d H:i");
										
										?>
										</td>
									<td scope="col">
										<?php
										 	if($payment->payment_type == "clientfinder")
										 	{ 
										 		echo "<?= COMPANY_NAME ?> monthly charge"; 
										 	}
											else
											{ 
												if(isset($payment->listing_id) AND !empty($payment->listing_id))
												{
													echo "Blast listing service: <a href='/listing/view/" . $payment->listing->id . "'>Listing #" . $payment->listing->id . "</a>"; 
												}
											}
										?>
									</td>
									<td scope="col" style="text-align: right;">$ <?php echo money_format('%!i', $payment->taxes);?></td>
									<td scope="col" style="text-align: right;">$ <?php echo money_format('%!i', $payment->amount);?></td>
									<td scope="col" style="text-align: right;">$ <?php echo money_format('%!i', ($payment->amount + $payment->taxes));?></td>
									
									<? /*<td scope="col"><?php if(isset($payment->listing->message) && !empty($payment->listing->message)) echo $payment->listing->message;?></td>*/ ?>
								</tr>
								
							<?php $i++; endforeach;?>
								
							<?php if($i==0): ?>
								
								<tr>
									<td scope="col" colspan="6">You have no invoices.</td>
								</tr>
								
							<?php endif; ?>
						
						</table>
		
		
		
		
						<div style="float: right;">
							<?php echo $this->pagination();?>
						</div>
							
					</li>
				</ul>
			</li>
		
		</ul>
		

			
		</div>
		
	</div>
	
</div>
	
<div class="grid_12">
	
	<div class="box gray">
		
	<h3 style="margin-top: 0px;">Account Settings</h3>
	
	<p>I would like to <a href="/member/cancelaccount/<?=$_SESSION['member']->id;?>">cancel my ClientFinder</a> account.</span>		
	
	</div>
	
</div>