
<h1 class="dashboardSection">Your Testimonial</h1>

<ul class="dashboardForm testimonialSettings clearfix">
	<li class="dashboardRowHeader">Testimonial<span class="notification">New</span></li>
	<li class="dashboardRow clearfix" style="background-color: #fff;">
		
		<div style="margin: 0 26px;">
			<p>This testimonial appears on 
				<a href="/<?= ($this->member->affiliate_account && $this->member->affiliate_account->slug) ? $this->member->affiliate_account->slug : 'a/i/' . $this->member->id ?>" target="_blank">your referral signup page</a>.
				
				<? if (empty($this->member->testimonials->testimonial)) : ?>In case you didn't have time to write up a quick testimonial, we've whipped one up for you!<?php endif; ?>
				</p>
					

			<div id="testimonialUpdateForm" style="margin-top: 30px;">

				<p id="testimonial_error" style="color: red; font-weight: italic; margin: 10px auto; width: 100%; text-align: center;"></p>
				<form action="/member/updatetestimonial" method="POST">
					<textarea name="testimonial" class="uiTextArea" style="height: 120px; width: 640px"><?= ($this->member->testimonials) ? $this->member->testimonials->testimonial : '' ?></textarea>
					<div class="clearfix">
						<input type="submit" class="uiButton" value="Save Testimonial">
					</div>
				</form>
			</div>
		
			<form action="#testimonial" method="POST">

	
				
				<div class="contentBox testimonialsContainer">
					<div class="testimonialsDesc">
						<span class="openQutote">&ldquo;</span>
						<p >
							<? if ($this->member->testimonials && !empty($this->member->testimonials->testimonial)) : echo $this->member->testimonials->testimonial; ?>
							<? else : ?>

								<p>My friends noticed my Facebook updates right away. &nbsp;In only my first week using <?=COMPANY_NAME;?>, I received a call from a family friend who said she'd seen my Facebook and wanted to talk to me
								about how I could help! Social Media has never been so easy. I just told my <i>Social Expert</i> what type of posts I wanted and <strong><?=COMPANY_NAME;?> did the rest!</strong></p>		
														
							<? endif; ?>
						</p>
						<span class="closeQutote">&rdquo;</span>
					</div>
				</div>
				<input type="button" class="uiButton" id="testimonialUpdateShow" value="Update your testimonial">
			</form>
			<script type="text/javascript">
			
				$(document).ready( function () {
				
					$('#testimonialUpdateForm').hide();
					$('#testimonialUpdateShow').click( function () {
						$('#testimonialUpdateForm').show();
						$(this).parents('form').hide();
					});
				
				});
			
			</script>
		</div>
		
	</li>
</ul>