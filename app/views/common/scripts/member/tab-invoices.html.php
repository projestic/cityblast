<h1 class="dashboardSection">Invoices<a href="<?php echo $this->url( array("controller"=>"member","action"=>"payment"),"default");?>?rebill=1" class="uiButton green pull-right">Update Credit Card</a></h1>
						
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="ui-datatable bordered zebrad hover">

	<tr>
		<th scope="col" style="text-align: left">Invoice #</th>
		<?/*<th width="15%" scope="col">Name</th>*/?>
		<th scope="col" style="text-align: left">Date</th>
		<th scope="col" style="text-align: left">Type</th>
		<th scope="col" style="text-align: right">Amount</th>
		<th scope="col" style="text-align: right">Taxes</th>
		<th scope="col" style="text-align: right">Total</th>
		<th scope="col" style="text-align: right">Action</th>
	</tr>
	
	<?php $i=0; foreach($this->paginator as $payment): ?>
	
		<tr>
			<td scope="col"><?php echo $payment->id;?></td>
			<? /*<td scope="col"><?php echo $payment->first_name . " " . $payment->last_name;?></td>*/?>
			<td scope="col">
				<?php 
					if(isset($payment->created_at) AND !empty($payment->created_at))	echo $payment->created_at->format("Y-m-d H:i");
				
				?>
				</td>
			<td scope="col">
				<?php
					if($payment->payment_type == "clientfinder")
					{ 
						echo "<?= COMPANY_NAME ?> subscription charge"; 
					}
					else
					{ 
						if(isset($payment->listing_id) AND !empty($payment->listing_id))
						{
							echo "Blast listing service: <a href='/listing/view/" . $payment->listing->id . "'>Listing #" . $payment->listing->id . "</a>"; 
						}
					}
				?>
			</td>
			<td scope="col" style="text-align: right;">$ <?php echo money_format('%!i', $payment->amount);?></td>
			<td scope="col" style="text-align: right;">$ <?php echo money_format('%!i', $payment->taxes);?></td>
			<td scope="col" style="text-align: right;">$ <?php echo money_format('%!i', ($payment->amount + $payment->taxes));?></td>
			<td scope="col" style="text-align: center"><a target='_blank' href='<?php echo $this->url( array("controller"=>"member","action"=>"printinvoice","id"=>$payment->id),"default");?>'>print</a></td>
			<? /*<td scope="col"><?php if(isset($payment->listing->message) && !empty($payment->listing->message)) echo $payment->listing->message;?></td>*/ ?>
		</tr>
		
	<?php $i++; endforeach;?>
		
	<?php if($i==0): ?>
		
		<tr>
			<td scope="col" colspan="7">You have no invoices.</td>
		</tr>
		
	<?php endif; ?>

</table>
		
<div style="float: right;">
	<?php echo $this->pagination();?>
</div>