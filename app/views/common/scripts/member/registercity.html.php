<div class="grid_12">
	
	<div class="box form clearfix">
	
		<div class="box_section">
			<?php if ($this->city_error) : ?>
			<div class="error">
				<h1>Ooops! There was a problem.</h1>
				<p>It seems you forgot to pick a city. Please select a city from the dropdown and try again!</p>
			</div>
			<?php else : ?>
			<h1>1. Connect to <?=COMPANY_NAME;?>.</h1>
			
			<p>Just like most other major websites, <?=COMPANY_NAME;?> entrusts Facebook.com with handling our member security. In order to access <?=COMPANY_NAME;?> securely, the first step is to login with Facebook Connect.</p>
			<p><a href="http://www.facebook.com/blog.php?post=41735647130" target="_new" style="font-style:italic;">What is Facebook Connect?</a></p>
			<?php endif ?>
		</div>
		
		<ul class="uiForm full clearfix" style="margin-top: -20px">
			<li class="uiFormRowHeader">Select Your City</li>
			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname">Blast With<span style="font-weight: bold; color: #990000">*</span></li>
					<li class="field">
						<span class="uiSelectWrapper">
							<span class="uiSelectInner">
								<select name="pubcity" id="pubcity" class="uiSelect" style="width: 590px;">
									<option value="*" <?php echo (empty($this->selectedCity)||$this->selectedCity=="*") ? "selected" : "";?>>Select Your City.</option>
									<?php if(isset($this->blast_cities) && is_array($this->blast_cities)): ?>
									<?php foreach($this->blast_cities as $city): ?>
										<option value="<?php echo $city->id;?>" <?php echo (!empty($this->selectedCity)&&$this->selectedCity==$city->id) ? "selected" : "";?>><?php echo $city->name;?></option>
									<?php endforeach; ?>
									<?php endif ?>
								</select>
							</span>
						</span>
						<div id='city_error' class="clearfix" style='display: none; margin-top: 5px;'>
							<hr />
							<div class='error' style="padding-left: 4px; color: #CC0000"><strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> select a city</div><div class='clearfix'>&nbsp;</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="uiFormRow clearfix">
				<ul>
					<li class="buttons">
						<a href="/member/clientfind-signup/" class="uiButton facebook right" id="facebook_signup_button">Connect With Facebook</a>
					</li>
				</ul>
			</li>
		</ul>

	</div>

</div>

<script type="text/javascript"  language="javascript">

	$(document).ready(function() {

	    	$('#facebook_signup_button').click(function(event) {
		
				event.preventDefault();
				$("#pubcity").removeClass('error');
				$('#city_error').hide();

				if( $("#pubcity option:selected").val() == "*" ) {
					 $("#pubcity").addClass('error');
					 $('#city_error').show();
				} else {

					FB.login(function(response) {

						if (response.authResponse) {
							/* console.log(response.authResponse); */
							$.ajax({

								type: "POST",
								url: "/member/fbsignup",
								data: "access_token="+response.authResponse.accessToken+"&city_id="+$("#pubcity option:selected").val()+"&href="+$('#facebook-signup').attr('href'),
								dataType: "json",

								success: function(msg)
								{
									if (msg.redirectBackTo) {
										window.location =	msg.redirectBackTo;
									} else {
										$("#pubcity").addClass('error');
										$('#city_error').show();
									}
								},

								error: function(xhr, errText) {
									alert(errText)
								}
							});

						}

					}, {scope: '<?php echo FB_SCOPE ?>'});
				}
			});
	});
</script>