<?/*
<div class="grid_12">
    <div style="padding: 0;" class="box white clearfix">
        <h1 style="text-align: center; margin: 0; line-height: 60px; color: #666666">Need assistance? Call us at <span style="color: #333333;">1-888-712-7888</span>
        	<?/* or <a href="#">Live Chat Now!</a>*?></h1>
    </div>
</div>
*/?>

<script type="text/javascript">
var fb_param = {};
fb_param.pixel_id = '6006773476030';
fb_param.value = '0.00';
(function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = '//connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
})();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6006773476030&amp;value=0" /></noscript>


<form action="" method="POST" id="infoform">
        		
        		

<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
        		
            	<h1>Your Contact Information.</h1>

			<?
				if(isset($_COOKIE['promo_code']))
				{
					if($promo = Promo::find_by_promo_code($_COOKIE['promo_code']))
					{
						echo "<h3 style='color: #5fb004'>Lock In ";
						if($promo->type == "FIXED_AMOUNT") echo "$".number_format($promo->amount, 2);
						else echo number_format($promo->amount) . "%";
						
						echo " in Savings. Promo Code: <u><i>".$_COOKIE['promo_code']."</i></u></h3>";	
					}
				}
			?>
			<?/*
            	<p>Your contact information is critical for your successful use of <?=COMPANY_NAME;?>.  Every one of your online leads, and important <?=COMPANY_NAME;?> communications will be sent to this email; so make sure it is always up to 
            	date and accurate.  Additionally, your phone number will be used in connecting you to agents with interested buyers for your properties.  Rest assured, this info will never be shared.</p>
            
            	<p style="font-style: italic"><strong>Need more help?</strong>  Watch this helpful video: <a href="javascript:void(0);" class="help_video_btn">Setting up your contact information.</a></p>
           	 */?>
            	
        	</div>


			<p><?=COMPANY_NAME;?> sends all of your new leads to your email, and publishes your business phone number to interested clients.  Please make sure you enter your best email and phone number below, and never miss a lead.</p>

			

			<ul class="uiForm full" style="margin-top: 15px; padding: 20px;">
			
			
				<li class="clearfix">
					<?php if (empty($this->member->first_name)): ?>
					<ul>
						<li class="fieldname cc"><span style="color: #990000;">*</span>First name</li>
						<li class="field cc clearfix">
							<input type="text" name="first_name" id="first_name" value="<?= $this->member->first_name;?>" class="uiInput <?= empty($this->firstNameError) ? '' : 'error'; ?>" />
						</li>
					</ul>
					<?php endif; ?>
					
					<?php if (empty($this->member->last_name)): ?>
					<ul>
						<li class="fieldname cc"><span style="color: #990000;">*</span>Last name</li>
						<li class="field cc clearfix">
							<input type="text" name="last_name" id="last_name" value="<?= $this->member->last_name;?>" class="uiInput <?= empty($this->lastNameError) ? '' : 'error'; ?>" />
						</li>
					</ul>
					
					<?php endif; ?>

					<ul>
						<li class="fieldname cc"><span style="color: #990000;">*</span>Business Email Address
							<?/*<span class="fieldhelp">The best email address to send leads to.</span>*/?>
						</li>
						<li class="field cc clearfix">
							<input type="text" name="email" id="email" value="<?= $this->member->email;?>" class="uiInput <?= empty($this->emailError) ? '' : 'error'; ?>" />
						</li>
					</ul>
					
					<ul style="margin-top: 15px;">
						<li class="fieldname cc"><span style="color: #990000;">*</span>Phone Number
						<?/*<span class="fieldhelp">A phone number for interested buyers.</span>*/?></li>
						<li class="field cc clearfix">
							<input type="text" name="phone" id="phone" value="<?= $this->member->phone;?>" class="uiInput" />
						</li>

					</ul>

				<?php if ($this->member->type && ($this->member->type->code == 'AFFILIATE')) : ?>
					<ul style="margin-top: 15px;">
						<li class="fieldname cc"><span style="color: #990000;">*</span>Your Signup Page URL
						<li class="field cc clearfix" style="position: relative">
							<i id="slug-ok" class="icon-ok" style="display: none; float: left; margin-left: -25px; margin-top: 7px; font-size: 18px; color: #090"></i> 
							<i id="slug-invalid" class="icon-ban-circle" style="display: none; float: left; margin-left: -25px; margin-top: 7px; font-size: 18px; color: #900"></i> 
							<span style="position: absolute; font-size: 16px; font-weight: normal; top: 9px; left: 30px; z-index: 10; color: #999">http://www.cityblast.com/</span><input type="text" name="slug" id="slug" value="<?php if ($this->member->affiliate_account) : ?><?= $this->member->affiliate_account->slug ?><?php endif; ?>" class="uiInput" style="padding-left: 195px !important; width: 560px;" />
							<br/><small id="slug-message">Must be at least three characters. Only alphanumeric digits and dashes allowed.</small>
						</li>
					</ul>
				<?php endif; ?>

					<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
						<ul style="margin-top: 15px;">
							<li class="fieldname cc"><span style="color: #990000;">*</span>Broker of Record
							<?/*<span class="fieldhelp">A phone number for interested buyers.</span>*/?></li>
							<li class="field cc clearfix">
								<input type="text" name="brokerage" id="brokerage" value="<?= $this->member->brokerage;?>" class="uiInput" />
							</li>
	
						</ul>
					<? endif; ?>

				</li>
				
				
				<li class="uiFormRow clearfix">
					<ul class="clearfix">
						<li class="clearfix span12">
							<input type="button" class="btn btn-primary btn-large pull-right" id="submitform" style="width: 195px;" id="member_info_save_btn" value="Save Info" />
						</li>
					</ul>
				</li>
			</ul>
   		</ul>
   			
    </div>

</div>

</form>





<div style="display: none;">
	
	<div id="help_video_popup" style="width: 640px; height: 480px;">
	    	   		
		<iframe width="640" height="480" src="//www.youtube.com/embed/0hskCF0TWPo?rel=0" frameborder="0" allowfullscreen></iframe>	

	</div>
	
</div>




<script type="text/javascript">
<!--

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

</script>
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

		var slug_validate;

		$('#slug').bind('keyup', function () {
			
			var slug = $(this).val();
			
			$(this).val( $(this).val().replace(/[^A-Za-z0-9\-]/, '') );
			
			if (slug_validate) clearTimeout(slug_validate);
			
			slug_validate = setTimeout( validate, 100 );
			
			function validate() {
				$('#slug-ok, #slug-invalid').hide();
				$('#slug-message').html('');
				$.get('/member/validate_affiliate_slug?slug=' + slug, function (response) {
					if ($('#slug').val() == response.slug) {
						if (response.allowed) {
							$('#slug-ok').show();
							$('#slug-message').css('color', '#090');
						} else {
							$('#slug-invalid').show();
							$('#slug-message').css('color', '#900');
						}
						$('#slug-message').html(response.message);
					}
				}, 'json');
			}
			
		});

		$( "#submitform" ).click(function( event ) 
		{			
			event.preventDefault();

			var firstName = $("#first_name").val();
			var lastName = $("#last_name").val();
			var phone = $("#phone").val();
			var email = $("#email").val();
			
			if (firstName && firstName.length < 1){
				var htmldata = '<div><div class="clearfix"><h3>Oops!</h3><p style="margin-top: 15px;">Your <b>first name</b> is required!</p><p></p><a href="#" class="btn btn-warning pull-right" onclick="$.colorbox.close();">Enter your first name now</a></p></div></div>';
				$.colorbox({width:"650px", height: "290px", html:htmldata});
				$("#first_name").addClass('error');
				return false;
			}
			
			if (lastName && lastName.length < 1){
				var htmldata = '<div><div class="clearfix"><h3>Oops!</h3><p style="margin-top: 15px;">Your <b>last name</b> is required!</p><p></p><a href="#" class="btn btn-warning pull-right" onclick="$.colorbox.close();">Enter your last name now</a></p></div></div>';
				$.colorbox({width:"650px", height: "290px", html:htmldata});
				$("#last_name").addClass('error');
				return false;
			}
			
			if (phone.length < 4){
				var htmldata = '<div><div class="clearfix"><h3>Oops!</h3><p style="margin-top: 15px;">Your <b>phone number</b> is required!</p><p>It will be used to connect you with other agents who have interested buyers for your listings. It will also be used by buyers who are interested in listings that appear on your wall.</p><a href="#" class="btn btn-warning pull-right" onclick="$.colorbox.close();">Enter your phone number now</a></p></div></div>';
				$.colorbox({width:"650px", height: "290px", html:htmldata});
				$("#phone").addClass('error');
				return false;
			}
			
			if (validateEmail(email) == false)
			{
				$("#email").addClass('error');
				
				var htmldata = '<div><div class="clearfix"><h3>Oops!</h3><p style="margin-top: 15px;">A valid <b>email address</b> is required.</p><p>It will be used to connect you with other agents who have interested buyers for your listings. It will also be used by buyers who are interested in listings that appear on your wall.</p><a href="#" class="btn btn-warning pull-right" onclick="$.colorbox.close();">Enter your email address now</a></p></div></div>';
				$.colorbox({width:"650px", height: "290px", html:htmldata});
				
				return false;
			}
			
			if (phone.length < 4){
				var htmldata = '<div><div class="clearfix"><h3>Oops!</h3><p style="margin-top: 15px;">Your <b>phone number</b> is required!</p><p>It will be used to connect you with other agents who have interested buyers for your listings. It will also be used by buyers who are interested in listings that appear on your wall.</p><a href="#" class="btn btn-warning pull-right" onclick="$.colorbox.close();">Enter your phone number now</a></p></div></div>';
				$.colorbox({width:"650px", height: "290px", html:htmldata});
				$("#phone").addClass('error');
				return false;
			}
		
		
			<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
		
				var brokerage = $("#brokerage").val();
		
				if (brokerage.length < 1)
				{
					var htmldata = '<div><div class="clearfix"><h3>Oops!</h3><p style="margin-top: 15px;">Your <b>Broker of Record</b> is required!</p><p>Governing bodies in most regions require that we display the broker of record on any listings you have with <?=COMPANY_NAME;?></p><a href="#" class="btn btn-warning pull-right" onclick="$.colorbox.close();">Enter your phone number now</a></p></div></div>';
					$.colorbox({width:"650px", height: "290px", html:htmldata});
					$("#brokerage").addClass('error');
					return false;
				}

			<? endif; ?>
			
			
			$("#infoform").submit();

		});

		

        $('#file_upload').uploadify({
            'uploader'  : '/js/uploadify/uploadify.swf',
            'script'    : '/blast/uploadimage',
            'cancelImg' : '/js/uploadify/cancel.png',
            'folder'    : '/images/',
            'auto'      : true,
            'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
            'fileDesc'  : 'Image Files',
            'buttonImg' : '/images/browse_button.png',
            'removeCompleted' : true,
            'height'    : 30,
            'width'     : 106,
            'name'      : 'fileUploaded',

            'onComplete': function(event, ID, fileObj, response, data) {
                $('#member_photo').attr('src', response);
                $('#photo').val(response);

            },

            'onError'   : function (event,ID,fileObj,errorObj) {
                alert(errorObj.type + ' Error: ' + errorObj.info);
            }
        });

        if (swfobject.hasFlashPlayerVersion('9.0.24') == false) {
            if(confirm("You do not have the minimum required flash version. Do you want to download it ?")){
                window.location.href= "http://www.adobe.com/go/getflashplayer";
            }
        }



		$('#help_div_button').click(function(event)
		{
			event.preventDefault();
			$('#help_div').toggle();
			location.href = "#video";
		});

		$('.help_video_btn').click(function(event)
		{
			event.preventDefault();
			$.colorbox({width:"720px", height: "570px", inline:true, href:"#help_video_popup"});
		});
		
	


		<? echo $this->render('fb-comments.html.php'); ?>

	



        
    });
</script>