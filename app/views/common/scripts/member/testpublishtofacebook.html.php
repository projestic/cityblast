<div class="dashboardContent">
	<?php if($this->blast['error']==FALSE): ?>
	<h2>Published To Facebook</h2>
	<p>Successfully published to Facebook</p>
	<a class="uiButton right" href="<?php echo $this->blast['postlink'];?>">View post update on Facebook</a>
	<?php else: ?>
	<h2>Error Publishing To Facebook</h2>
	<p>Unable to publish to facebook: <?php echo $this->blast['message'];?></p>
	<?php endif;?>
</div>