<div style="display: none;">
	
	<div id="sample_popup" style="width: 640px; height: 490px;"></div>
	
</div>
	
<?php if ($this->failed_permission==1) : ?>
<div style="display: none;">

	<div id="facebook-popup">

		<div class="box white80 clearfix">

			<h3>Oh Dear, There Was A Problem</h3>

			<p>We're sorry, but without the proper publishing permission <?=COMPANY_NAME;?> will not be able to manage your account.</p>
			
			<p>Even if you want to only publish to your Fan page (and not your personal Facebook page), we'll still need this permission.<p>
				
			<p>If you want to turn off the publishing to your personal Facebook wall, you can do that once you've set up all of your accounts.<p>

			<a href="/member/join" id="facebook_signup_button_modal" class="uiButton" style='font-size: 18px;'>Grant Permission to Publish to my Wall!</a>

		</div>

	</div>

</div>

	
<script type="text/javascript">
	$(document).ready(function()
	{
		$.colorbox({width:"500px", inline:true, href:"#facebook-popup"});

	});		
</script>
<?php endif; ?>



		
<script type="text/javascript">
	
$(document).ready(function()
{

	$('a[id^="facebook_signup_button"]').click(function(event)
     {
     	event.preventDefault();
            
        
          $("#defaultpubcity").removeClass('error');
          $('#city_error').hide();
          $('#citywrapper').removeClass('city_error');


		  // City validation disabled 2013.10.17

         /*  if($("#locality").val() == "")
          {
               $('#citywrapper').addClass('city_error');
               $("#defaultpubcity").addClass('error');
          	$('#city_error').show();
          }
          else
          {     */      	
			if ($('#terms').is(':checked')) 
			{

				var errorContainer = $('#content_prefs_error');
				errorContainer.hide();
				errorContainer.text('');
				
				if ($('.content_pref:checked').length < 5) 
				{
		          	$.colorbox({width: "500", html:"<h1>Oooops!</h1><p style='font-size: 16px; color: #ff0000; margin-bottom: 25px; font-weight: bold;'>You must select at least 5 types of content.</p>"});					
					return false;
				}
	

				if(!$(this).hasClass('alreadyclicked')) 
				{					
					$(this).addClass('alreadyclicked');
	
					var country = $('#country').val();
					var state   = $('#administrative_area_level_1').val();
					var city    = $('#locality').val();

					if (country.length && country != 'United States' && country != 'Canada') {
						$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>We're sorry, but we don't support countries outside of North America at this time!</p>"});
						return false;
					}

					// City validation disabled 2013.10.17

					/* if (state == '') {
						$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>We need your city and state/province to continue.</p>"});
						return false;
					}

					if (city == '') {
						$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>We need your city to continue.</p>"});
						return false;
					} */
	
					$.post("/member/tmp_store_settings", $("#upfrontsettings").serialize(),function() {/*alert("success")*/;});
	
		
		               FB.login(function(response) 
		               {
		
		                    if (response.authResponse)
		                    {
		                        	window.location.href="/member/clientfind-signup/?access_token="+response.authResponse.accessToken+"&member_type=" + $("#member_type").val();
		                   	} else {
								$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>Either your login failed, or you didn't authorize us to access your Facebook account. Please be sure to click \"Yes\" in the Facebook popup in order to proceed.</p><p><a href=\"#\" class=\"btn facebook-retry\">Retry</a></p>"});
		                   		bindFacebookRetry();
		                   	}
		
		                }, {scope: '<?php echo FB_SCOPE;?>'});		              
	          	}
	          }
	          else
	          {
	          	$.colorbox({width: "400", html:"<h1>Oooops!</h1><p style='font-size: 16px;'>You must agree to the Terms and Conditions.</p>"});
	     	}            	
     	// }            
        	
	});

		
		function bindFacebookRetry() {
			$('.facebook-retry').click( function(e) {
				$('#facebook_signup_button').removeClass('alreadyclicked').trigger('click');
				$.colorbox.close();
				e.preventDefault();
			});
		}

	

		$('.samplecontent').click(function(event)
		{
			event.preventDefault();
			
			var tag_id = $(this).attr("tag_id");
		    	var url = $(this).attr('href'); 

		    	$.ajax({
				type: 'GET',
				url: url,
				dataType: 'html',
				
				success: function(data) 
				{ 
					$('#sample_popup').empty();
					$('#sample_popup').append(data);
				}
    			});
		   
		     

			$.colorbox({height: "660px",inline:true, href:"#sample_popup",onClosed:function(){ $('#sample_popup').empty(); }});
		});
		

		$('.action_items > a').click(function(event){
			event.preventDefault();
			var href = $(this).attr("rel");
			$.colorbox({
				width:"550px", 
				height: "300px", 
				inline:true, 
				href:"#"+href,
				onClosed:function(){
					var content = $('.dashboardContent')
					content.html('<img src="/images/dashboard_loading.gif" height="250" width="250" style="margin: 80px 230px 0;" />');
					content.load('/member/tab_settings', function(){ $('.zebrad').each(function(){ $(this).zebraTable(); }); });			
				}
			});
		});
		
		var $SettingChkbx = $('.stepContent ul.settings input[type="checkbox"]');
		var $Step1 = $('.step1');
		var $Step2 = $('.step2');
		var $Step3 = $('.step3');
		
		$SettingChkbx.change(function()
		{
			var $this = $(this);
			var $Parent = $this.parent();
			if($this.prop('checked') || $this.attr('checked') == 'checked')
			{				
				$Parent.addClass('selected');
			}
			else{
				$Parent.removeClass('selected');
			}
			
			if($SettingChkbx.filter(':checked').length > 0)
			{
				$Step2.removeClass('opac');
				if($Step2.find('select').val() == false)
				{
					$Step3.addClass('opac');
					$Step2.removeClass('complete');
				}
				else
				{
					$Step3.removeClass('opac');
					$("#citywrapper").removeClass('errorBorder');
					$Step2.addClass('complete');
				}
					
				$Step1.addClass('complete');
			}
			else
			{
				$Step2.removeClass('complete').add($Step3).addClass('opac');
				$Step1.removeClass('complete');
			}
		}).change();
		
		$Step2.find('.city_select').change(function()
		{
			var all_opts_Selected = true;
			$Step2.find('.city_select').each(function(){
				if (!$(this).val()) {
					all_opts_Selected = false;
				}
			});
			
			if(!all_opts_Selected)
			{
				$Step3.addClass('opac');
				$Step2.removeClass('complete');
			}
			else
			{
				$Step3.removeClass('opac');
				$("#citywrapper").removeClass('errorBorder');
				$Step2.addClass('complete');
			}
		});
		
		
		
		$('#cboxLoadedContent a[rel="open_ajax"]').live('click', function(e){
		    // prevent default behaviour
		    e.preventDefault();

		    var url = $(this).attr('href'); 

		    $.ajax({
			        type: 'GET',
			        url: url,
			        dataType: 'html',
			        cache: true,
			        beforeSend: function() {
			            $('#cboxLoadedContent').empty();
			            $('#cboxLoadingGraphic').show();
			        },
			        complete: function() {
			            $('#cboxLoadingGraphic').hide();
			        },
			        success: function(data) { 
			            $('#cboxLoadedContent').empty();
			            $('#cboxLoadedContent').append(data);
			        }
	   	 	});
		});


    		<?/*
		function onValueChange(v)
		{
			if(v==1)
			{
				$('#frequency_amount').text('1 day');
				$('#frequency_message').html('This is the absolute minimum.');
			}
			else if(v < 3)
			{
				$('#frequency_amount').text(v + ' days');
				$('#frequency_message').html('Moving on up! Updating few times a week will help with your social media presence.');
			}
			else if(v < 5)
			{
				$('#frequency_amount').text(v + ' days');
				$('#frequency_message').html('This is really what we suggest as the "bare minimum."');
			}
			else if(v <= 6)
			{
				$('#frequency_amount').text(v + ' days');
				//$('#frequency_message').html("Now we're talking! This is our \"business\" level. Watch the leads flow in");
			}
			else if(v == 7)
			{
				$('#frequency_amount').text(v + ' days');
				//$('#frequency_message').html('Your Best Choice! You\'ve decided to maximize your earning potential. Good idea!');
			}
		}
		$('.postNumberSlider').slider({
			range: "min",
			value: 4,
			min: 1,
			max: 7,
			step: 1,
			slide: function(event, ui)
			{
				var v = ui.value;
				onValueChange(v);
			},
			change: function(event, ui)
			{
				$('#frequency_id').val(ui.value);
			}
		})
		*/?>
		<?/*
		if($.browser.msie && $.browser.version < 9)
		{
				
			$('.dialWrapper').detach();
			
			var sHtml = '<input type="hidden" name="frequency_id" id="frequency_id" value="4" /><div id="slider" style="width: 120px; float: left; margin-top: 60px;"></div>';
			$('#frequency_change').prepend(sHtml);
			$('#slider').slider({
				value: 4,
				min: 1,
				max: 7,
				step: 1,
				slide: function( event, ui )
				{
					var v = ui.value;
					onValueChange(v);
				},
				change: function( event, ui )
				{
					$('#frequency_id').val(ui.value);
				}
			});
			
		}
		else
		{	
			$(".dial").knob({
				
				'min': 			1,
				'max': 			7,
				'width': 			120,
				'thickness': 		.2,
				'fgColor': 		'#222222',
				'bgColor': 		'transparent',
				'displayPrevious': 	true,
				'angleOffset':		-125,
				'angleArc':		250,
				
				'draw' : function () {
					
					//this.cv; THIS IS THE CURRENT VALUE
					// MIGHT COME IN HANDY LATER ON
					
					var v = this.v;
					
					if(v < 7 && v > 1)
					{
						$(this.i).closest('.dialWrapper').css('background-image', 'url(/images/frequency_dial_bg.png)');
						this.o.bgColor = '#cccccc';
					}
					else if(v <= 1)
					{
						$(this.i).closest('.dialWrapper').css('background-image', 'url(/images/frequency_dial_bg-off.png)');
						this.o.bgColor = '#ffe5e5';
					}
					else
					{
						$(this.i).closest('.dialWrapper').css('background-image', 'url(/images/frequency_dial_bg-pro.png)');
						this.o.bgColor = '#cccccc';
					}
					
					if(v == 1)
					{
						this.o.fgColor = '#ff0000';
					}
					else if(v == 2)
					{
						this.o.fgColor = '#ff0000';
					}
					else if(v == 3)
					{
						this.o.fgColor = '#ff8c01';
					}
					else if(v == 4)
					{
						this.o.fgColor = '#ffd000';
					}
					else if(v == 5)
					{
						this.o.fgColor = '#eaff00';
					}
					else if(v == 6)
					{
						this.o.fgColor = '#99e600';
					}
					else if(v == 7)
					{
						this.o.fgColor = '#00cc00';
					}
					
					onValueChange(v);
					
				},
				
				'change' : function (v) {}
				
			});
		}*/?>
});			
</script>