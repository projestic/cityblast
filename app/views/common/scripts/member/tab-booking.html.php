<h1 class="dashboardSection">My Bookings</h1>

<?php

	//$this->bookings = 0;		//For testing
	if(isset($this->bookings) && !empty($this->bookings))
	{
		
		echo '<ul class="dashboardForm clearfix">';

		foreach($this->bookings as $booking) 
		{
			if (!$booking->listing) {
				// Listing does not exist - skip this booking
				continue;
			}
		?>
				
			<li class="dashboardRow clearfix">
				<ul>
					<li class="dashboardFieldname">
						Property Inquiry
						
						<span class="hr">&nbsp;</span>
						
						<span style="font-size: 12px; display: block;">
							<img style="background-position: -450px -30px; vertical-align: middle;" class="uiIcon icon-small" src="/images/blank.gif"> 
							<?=$booking->created_at->format("F j, Y");?>
						</span>
						
						<span style="font-size: 12px; display: block;">
							<img style="background-position: -361px -150px; vertical-align: middle;" class="uiIcon icon-small" src="/images/blank.gif"> 
							<?=$booking->listing->street;?>
						</span>
						
						<div class="fieldnameThumb"><a href="/listing/view/<?=$booking->listing->id;?>"><img src="<?=$booking->listing->getSizedImage(280, 250) ?>" width="150" border="0" /></a></div>
								
						<span class="hr">&nbsp;</span>
						
						<a href="<? echo APP_URL . "/listing/view/" . $booking->listing->id;?>" class="uiButton small aux right" style="width: 150px; text-align: left;"><img style="background-position: -480px -1px; vertical-align: middle;" class="uiIcon icon-white icon-small" src="/images/blank.gif"> View This Listing</a>
					</li>

					<li class="dashboardField clearfix">
					
						<ul class="bookingTabContainer clearfix">
							<li class="selected" name="cust">Customer Info</li>
							<li name="list">Listing Agent</li>
							<?/*<li name="property">Property Details</li>*/?>
						</ul>
						<div class="bookingTabDetailContainer cust">
							<div class="customerInfo">
								<div class="clearfix">
									<div class="userImg">
										<img class="uiIcon" src="/images/blank.gif" border="0" />
									</div>
									<h1 class="userName"><?=ucwords($booking->name);?></h1>
								</div>
								<div>
									<?=nl2p($booking->message)?>
								</div>
								<div class="clearfix contactInfo">
									<div>
										<h6>Email</h6>
										<span><?=isset($booking->email) && !empty($booking->email) ? "<a href='mailto:".$booking->email."' style='color:#3D6DCC;'>".$booking->email . '</a>' : "N/A" ?></span>
									</div>
								</div>
								<?/*
								<div class="clearfix contactInfo">
									<div>
										<h6>Phone</h6>
										<span><?=isset($booking->phone) && !empty($booking->phone) ? $booking->phone : 'N/A' ?></span>
									</div>
									<div>
										<h6>Email</h6>
										<span><?=isset($booking->email) && !empty($booking->email) ? "<a href='mailto:".$booking->email."' style='color:#3D6DCC;'>".$booking->email . '</a>' : "N/A" ?></span>
									</div>
								</div>
								*/ ?>
								
								<?/*
								<div>
									<h6>Best Contact: 
									<span><?=ucfirst(empty($booking->best_time) ? "anytime" : $booking->best_time) ?> / 
									<?=strtolower($booking->contact_by) == "email" ? "<a href='mailto:".$booking->email."' style='color:#3D6DCC;'>" . trim(strtolower($booking->contact_by)) . "</a>" : ucfirst(trim(strtolower($booking->contact_by)));?></span></h6>
								</div>
								*/ ?>
							</div>
							<?php 
							
							if (($booking->listing instanceOf Listing) && ($booking->listing->member instanceOf Member)) :
							
								$listing_agent = $booking->listing->member; ?>
								<div class="listingAgent">
									<div>
										<h6>Name:</h6>
										<span><?= ucwords($listing_agent->first_name) . " " . ucwords($listing_agent->last_name); ?></span>
									</div>
									<div>
										<h6>Phone:</h6>
										<span><? if (isset($listing_agent->phone) && !empty($listing_agent->phone)) echo $listing_agent->phone; else echo "No phone number on record"; ?></span>
									</div>
									<div>
										<? if (isset($listing_agent->email_override) && !empty($booking->listing->member->email_override)) : ?>
											<h6>Email:</h6>
											<span><a href='mailto:<?=$listing_agent->email_override;?>' style='color:#3D6DCC;'><?=$listing_agent->email_override;?></a></span>
										<? else: ?>
											<h6>Email: </h6>
											<span><? if (isset($listing_agent->email) && !empty($booking->listing->member->email)) echo "<a href='mailto:".$listing_agent->email."' style='color:#3D6DCC;'>".$listing_agent->email."</a>"; else echo "No email on record"; ?></span>
										<? endif; ?>
									</div>
								</div>
							
							<?php endif; ?>

						</div>
						
					</li>
					
				</ul>
				
			</li>						
					
		<?
			
		}
			
		echo '</ul>';
	}
	else
	{
	?>
		
		<? if (!$this->listings_enabled) : ?>
			<h3 style="margin-left: 10px;">Listings are off.</h3>
			<p style="margin-left: 10px;">It looks like you've chosen not to publish listings in your <a href="javascript:void(0);" onclick="$('#settingtab').trigger('click'); $('#top_menu').removeClass('on');">Content Settings</a>. You will not receive "Bookings" unless you decide to start to publish Listings.</p> 
		<? else : ?>
			<h3 style="margin-left: 10px;">Ah Rats! No Bookings.</h3>
			<p style="padding-left: 10px; padding-top: 8px;">You have no Bookings at this time.</p>
		<? endif; ?>
		
	
	<?
	
	}
	
?>
	

<script type="text/javascript">
	$(function(){
		$BookingTabs = $('.bookingTabContainer li');		
		$BookingTabs.click(function(e){
			var $Tab = $(this);
			$Tab.siblings('li').removeClass('selected');
			$Tab.addClass('selected');
			$Tab.closest('.dashboardField').find('.bookingTabDetailContainer').removeClass().addClass('bookingTabDetailContainer ' + $Tab.attr('name'));
		});
	});
</script>