<? $member = isset($_SESSION['member']) && isset($_SESSION['member']->id) ? $_SESSION['member'] : new stdClass(); ?>
<? $payments = $member->payments;?>
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>

<div style="display: none;">
	<div id="brokerage_not_set">
		<div class="row-fluid">
			<div class="contentBox">
				<div class="contentBoxTitle">
					<h1>Oooops</h1>
				</div>
			</div>
			<h4 style="margin: 30px 0 0 0;">You havent given us the name of your brokerage</h4>
			<form action="">
				<ul class="uiForm clearfix">
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>Brokerage</li>
							<li class="field cc">
							<input type="text" name="brokerage" value="" class="uiInput validateNotempty" />
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc"><span style="color: #990000;">*</span>Brokerage Address</li>
							<li class="field cc">
							<input type="text" name="broker_address" value="" class="uiInput validateNotempty" />
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<input type="submit" name="submitted" class="btn btn-primary btn-large pull-right" style="width: 195px;" value="Save">
							</li>
						</ul>
					</li>
				</ul>
			</form>
		</div>
	</div>
</div>

<? if(isset($_SESSION['member']) && !empty($_SESSION['member'])) : ?>

<?php if($this->is_new_paying_member): ?>
	<script type="text/javascript">
	var fb_param = {};
	fb_param.pixel_id = '6006773471430';
	fb_param.value = '0.00';
	(function(){
	  var fpw = document.createElement('script');
	  fpw.async = true;
	  fpw.src = '//connect.facebook.net/en_US/fp.js';
	  var ref = document.getElementsByTagName('script')[0];
	  ref.parentNode.insertBefore(fpw, ref);
	})();
	</script>
	<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6006773471430&amp;value=0" /></noscript>
<?php endif; ?>
					
	<div class="row-fluid">

		<div class="dashboard clearfix">
		
			<div class="dashboardMenuWrapper clearfix">
			
				<div class="dashboardMenu clearfix">
					<?/* for Notifications:
						structure for that : <span class="notification"></span>
						this span tag should be fill dynamically with notifications number
						for unread notification additional class name "unread" for added span tag						
					*/
					$thankyou = false;		
					?>	

					<?php if ((isset($member->show_thankyou_page) && $member->show_thankyou_page) || ($this->thankyou) ) : $thankyou = true; ?>
						<a rel="/member/tab_thankyou" href="#thankyoutab" id="thankyoutab" class="dashboardTab active"><img src="/images/blank.gif" class="uiIcon" style="background-position:  1px -120px;" />Thank You!</a>
					<?php endif; ?>
					
					<?php if ($this->canAccess('/member/tab_dashboard')) : ?><a rel="/member/tab_dashboard" href="#dashboardtab" id="dashboardtab" class="dashboardTab<?php if (!$thankyou) : ?> active<?php endif; ?>"><img src="/images/blank.gif" class="uiIcon" style="background-position: -150px -270px;" />Dashboard</a><?php endif; ?>
					<?php if ($this->canAccess('/member/tab_affiliate_dashboard')) : ?><a rel="/member/tab_affiliate_dashboard" href="#dashboardtab" id="dashboardtab" class="dashboardTab<?php if (	(!$thankyou) && (!$this->canAccess('/member/tab_affiliate_dashboard'))	) : ?> active<?php endif; ?>"><img src="/images/blank.gif" class="uiIcon" style="background-position: -150px -270px;" />Affiliate Tools</a><?php endif; ?>

					
					<?php if ($this->canAccess('/member/tab_referagent')) : ?><a rel="/member/tab_referagent" href="#referagenttab" id="referagenttab" class="dashboardTab"><img src="/images/blank.gif" class="uiIcon" style="background-position: -300px -89px;" />Refer an Agent</a><?php endif; ?>
					<?php if ($this->canAccess('/member/tab_affiliates')) : ?><a rel="/member/tab_affiliates" href="#affiliates" id="affiliatestab" class="dashboardTab"><img src="/images/blank.gif" class="uiIcon" style="background-position: -90px -208px;" />Affiliate Sales</a><?php endif; ?>


					<?php if ($this->canAccess('/member/tab_settings')) : ?><a rel="/member/tab_settings" href="#settingtab" id="settingtab" class="dashboardTab"><img src="/images/blank.gif" class="uiIcon" style="background-position: -120px 1px;" />Settings</a><?php endif; ?>

					
					<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
						<?php if ($this->canAccess('/member/tab_listings')) : ?><a rel="/member/tab_listings" href="#listingstab" id="listingstab" class="dashboardTab"><img src="/images/blank.gif" class="uiIcon" style="background-position: -180px -118px;" />Listings<span class="notification"><?=!empty($this->listings)?$this->listings:0;?></span></a><?php endif; ?>
						<?php if ($this->canAccess('/member/tab_booking')) : ?><a rel="/member/tab_booking" href="#mybookingtab" id="mybookingtab" class="dashboardTab">
							<img src="/images/blank.gif" class="uiIcon" style="background-position: -240px -90px;" />Bookings
							
							<? if(isset($this->new_bookings) && $this->new_bookings): ?> <span id="new_bookings_bubble"><?=$this->new_bookings?></span><span class="notification unread"><?=$this->new_bookings;?>
							<? elseif(isset($this->total_bookings) && $this->total_bookings) : ?><span class="notification"><?=$this->total_bookings;?></span><?php endif; ?>
							
							</span></a><?php endif; ?>
					<?php endif; ?>
						
					<?php if ($this->canAccess('/member/tab_invoices')) : ?><a rel="/member/tab_invoices" href="#invoices" id="invoicestab" class="dashboardTab"><img src="/images/blank.gif" class="uiIcon" style="background-position: -90px -208px;" />Invoices<span class="notification"><?=count($payments);?></span></a><?php endif; ?>
					<?php if ($this->canAccess('/member/tab_contact')) : ?><a rel="/member/tab_contact" href="#myaccounttab" id="myaccounttab" class="dashboardTab"><img style="background-position: <?=($member->gender && strtolower($member->gender)=="female") ? '-29px' : '1px'?> 1px; vertical-align: middle;" class="uiIcon" src="/images/blank.gif">My Account</a><?php endif; ?>

					<?php if ($this->canAccess('/member/tab_testimonial')) : ?><a rel="/member/tab_testimonial" href="#testimonialtab" id="testimonialtab" class="dashboardTab<?php if (	(!$thankyou) && (!$this->canAccess('/member/tab_dashboard'))	) : ?> active<?php endif; ?>"><img src="/images/blank.gif" class="uiIcon" style="background-position: -300px -240px;" />Testimonial<span class="notification red">New</span></a></a><?php endif; ?>
						
				</div>
			
			</div>
			
			<div class="dashboardContentContainer dashboardContent clearfix"></div>
		
		</div>

	</div>


<?php endif; ?>


<script language="javascript">

	var no_flash = false;

	$(document).ready(function()
	{
		<? if ($this->memberIs(array('agent', 'broker')) && empty($member->brokerage) && stristr(APPLICATION_ENV, "cityblast")): ?>
		$.colorbox({
			width:"720px", 
			height: "420px", 
			inline:true, 
			href: '#brokerage_not_set',
			escKey: false,
			closeButton: false,
			overlayClose: false,
			onComplete: function() {
				$('#cboxClose').hide();
				$('#brokerage_not_set form').submit(function(e){
					e.preventDefault();
					var post_data = {};
					$('#brokerage_not_set .uiInput').each(function(k, element){
						post_data[$(element).attr('name')] = $(element).val();
					});
					$.post('/member/brokerage', post_data,function(data) {
						if (data.success) {
							window.location = '/member/settings';
						} else {
							$(data.errors).each(function(k, value){
								$('#brokerage_not_set input[name="' + value +'"]').addClass('error');
							});
						}
					});
				});
			}
		});
		<?php endif; ?>

		$('#file_upload').uploadify({
			'uploader'  : '/js/uploadify/uploadify.swf',
			'script'    : '/member/uploadimage',
			'cancelImg' : '/js/uploadify/cancel.png',
			'folder'    : '/images/',
			'auto'      : true,
			'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
			'fileDesc'  : 'Image Files',
			'buttonImg' : '/images/browse_button.png',
			'removeCompleted' : true,
			'height'	: 30,
			'width'		: 106,
			'name'		: 'fileUploaded',


			'onComplete': function(event, ID, fileObj, response, data) 
			{
				$('#fileUploaded').val(response);
				$('#member_photo').attr('src', response);

				setPhoto(response);
			},

			'onError'	: function (event,ID,fileObj,errorObj) 
			{
				alert(errorObj.type + ' Error: ' + errorObj.info);
			}

		});

		if(swfobject.hasFlashPlayerVersion('9.0.24') == false)
		{
			no_flash = true;
		}

		function setPhoto(photo) 
		{
			$.ajax({

				type: "POST",
				url: "/member/setphoto",
				data: "photo="+encodeURI(photo),
				dataType: "json",

				success: function(msg)
				{
					if (msg.error) {
						alert("File upload failed!");
					} else {
						$('#photoSetStatus').html('<img src="/images/right.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your photo is set.');
					}
				}
			});
		}


		function loadTabContent(content, url)
		{
		 	content.html('<img src="/images/dashboard_loading.gif" height="250" width="250" style="margin: 80px 230px 0;" />');
			content.load(url, function(){ $('.zebrad').each(function(){ $(this).zebraTable(); }); });	 
		}

		$(".dashboardMenu").each( function()
		{
			
			var tabset	= $(this);
			var tabs		= $(this).find('.dashboardTab');
			var content	= $('.dashboardContent');
			
			// SET LOAD EVENTS ON EACH TAB
			(tabs).click( function()
			{
				var tab = $(this);
				var url = tab.attr('rel');

				tabs.removeClass('active');
				tab.addClass('active');
				loadTabContent(content, url);
				
			});
			
			var firstTab;
			if(window.location.hash)
			{
				firstTab = tabs.filter('[href="' + window.location.hash + '"]')[0];	
			} else 
			{
				firstTab = tabs[0];
			}
	
			// INITIALIZE FIRST TAB
			$(firstTab).addClass('active');
			loadTabContent(content, $(firstTab).attr('rel'));
		

			if(firstTab != tabs[0])
			{
				$(tabs[0]).removeClass('active');
			}
		});

	});
</script>
