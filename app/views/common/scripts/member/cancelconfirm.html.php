<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
        		<h1>Success.</h1>
        	</div>

		<p>Your account has been successfully cancelled. We're sorry to see you go!</p>
		
		
		<a href="/member/logout" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" id="submitBtn">Logout</a>
		
		<div class="clearfix"></div>
		
	</div>
	
	
</div>	