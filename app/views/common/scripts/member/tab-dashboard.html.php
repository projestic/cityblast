<?
	$count = Member::count();	
	$avg_friends = ($count>0) ? ($this->total_count / $count) : 0;
	
	$your_reach  = $this->member->total_reach();
	$your_clicks = $this->total_clicks;	
	

	
	if($avg_friends>0) $reach_percent = round(( ($your_reach - $avg_friends) / $avg_friends ) * 100);
	else $reach_percent = 0;

	if($this->avg_clicks_per_user>0) $click_percent = round(( ($your_clicks - $this->avg_clicks_per_user) / $this->avg_clicks_per_user ) * 100);
	else $click_percent = 0;

	
	$fb_share_count = $this->member->facebook_shares_trailing_thirty();
	$tw_share_count = $this->member->twitter_shares_trailing_thirty();
	$li_share_count = $this->member->linkedin_shares_trailing_thirty();
	$em_share_count = $this->member->email_shares_trailing_thirty();
	
	$ph_share_count = $this->member->phonecalls_trailing_thirty();
	$bk_share_count = $this->member->booking_trailing_thirty();
	
	$your_total_shares = $fb_share_count + $tw_share_count + $li_share_count + $em_share_count;
	
	
	$total_touches = $your_clicks + $this->total_likes + $this->total_comments + $fb_share_count + $tw_share_count + $li_share_count + $em_share_count + $ph_share_count + $bk_share_count;


	
	/*************************
	$share_padding = "";
	if($your_total_shares < 100 && $your_total_shares > 9)
	{
		$share_padding = '0';
	}
	elseif($your_total_shares < 10)
	{
		$share_padding = '00';
	}
	
	$share_index = number_format( ( ($your_reach>0) ? $your_total_shares / ( $your_reach / 100) : 0 ), 2 );  


	$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 30 DAY)";
	$shares = Interaction::find_by_sql($query);
	$total_member_shares = $shares[0]->total_shares;

	
	$avg_shares = ($total_member_shares / $this->total_active_members); //Average share per active member

 	if($avg_shares>0) $share_percent = round(( ($your_total_shares - $avg_shares) / $avg_shares ) * 100);
 	else $share_percent = 0;
 	****************************/

?>
<script type="text/javascript" src="/js/highcharts.js"></script>
<div class="clearfix" style="padding: 20px 15px;">
	
	<?/******
	
	<div class="linechartControlls clearfix">
		<span class="clickLegend"><span class="clickLegend"></span>Clicks</span>
		<span class="interactionLegend"><span class="interactionLegend"></span>Interactions</span>
		<span class="horizontalAxis">Month</span>
	</div>
	<div class="dashboardPlainBox">
		<div id="linechart" style="height: 200px; width: 600px;"></div>
	</div>

	<div class="earnFree clearfix">

		<h2><a href="javascript:void(0);" onclick="$('#referagenttab').trigger('click'); $('#top_menu').removeClass('on');">Earn a <span>Free</span> <?=COMPANY_NAME;?> membership!</a></h2>
		<a href="javascript:void(0);" onclick="$('#referagenttab').trigger('click'); $('#top_menu').removeClass('on');">Learn More</a>

	</div>
	
	*******/ ?>
	
	

	<div class="dashboardBox">
		<div class="dashboardHeading clearfix">
			<div class="headingIcon"></div>
			<h4>People you've touched</h4>
		</div>
		<div class="pieChart clearfix">
			<div class="colored">
				<div class="clearfix">
					<div class="chartInfo">
						<h1 class="chartHeading"><?=number_format($total_touches);?> Touches</h1>					
						<ul>
							<li class="clearfix">
								<span class="legendIcon clicks"></span>
								<label>clicks</label>
								<span class="data"><?=number_format($your_clicks);?></span>
							</li>
							<li class="clearfix">
								<span class="legendIcon likes"></span>
								<label>likes</label>
								<span class="data"><?=number_format($this->total_likes);?></span>
							</li>
							<li class="clearfix">
								<span class="legendIcon comments"></span>
								<label>comments</label>
								<span class="data"><?=number_format($this->total_comments);?></span>
							</li>
							<li class="clearfix">
								<span class="legendIcon shares"></span>
								<label>shares</label>
								<span class="data"><?=number_format($your_total_shares);?></span>
							</li>
							
							<?/**
							<li class="clearfix">
								<span class="legendIcon emailReq"></span>
								<label>email request</label>
								<span class="data"><?=number_format($your_total_shares);?></span>
							</li>
							<li class="clearfix">
								<span class="legendIcon phoneReq"></span>
								<label>phone  request</label>
								<span class="data"><?=number_format($your_total_shares);?></span>
							</li>
							*/ ?>
						</ul>
					</div>
					<div class="chartContainer">
						
						<? if($your_clicks == 0 && $this->total_likes == 0 && $this->total_comments == 0 && $this->total_comments == 0 && $your_total_shares == 0) : ?>
							<div style="width: 171px; height: 163px; padding-top: 40px; color: #b7b7b7;"><center>no data</center></div>				
						<? else : ?>
							<div id="coloredChart" style="width: 171px; height: 163px;"></div>
							<span>Results for 30 days</span>						
						<? endif; ?>

					</div>
				</div>
			</div>
			<div class="nonColored">
				<div class="clearfix">
					<div class="chartInfo">
						<span class="subTitle">Average</span>
						<h2 class="chartHeading">Member</h2>
						<ul>
							<li class="clearfix">
								<span class="legendIcon clicks"></span>
								<label>clicks</label>
								<span class="data"><?=number_format($this->dash_stats->avg_clicks);?></span>
							</li>
							<li class="clearfix">
								<span class="legendIcon likes"></span>
								<label>likes</label>
								<span class="data"><?=number_format($this->dash_stats->avg_likes);?></span>
							</li>
							<li class="clearfix">
								<span class="legendIcon comments"></span>
								<label>comments</label>
								<span class="data"><?=number_format($this->dash_stats->avg_comments);?></span>
							</li>
							<li class="clearfix">
								<span class="legendIcon shares"></span>
								<label>shares</label>
								<span class="data"><?=number_format($this->dash_stats->avg_shares);?></span>
							</li>
							
							<?/*
							<li class="clearfix">
								<span class="legendIcon emailReq"></span>
								<label>email request</label>
								<span class="data"><?=number_format($your_total_shares);?></span>
							</li>
							<li class="clearfix">
								<span class="legendIcon phoneReq"></span>
								<label>phone  request</label>
								<span class="data"><?=number_format($your_total_shares);?></span>
							</li>
							*/?>
						</ul>
					</div>
					<div class="chartContainer">
						<div id="bwChart" style="width: 129px; height: 163px; margin-top: 3px;"></div>
						<span>Results for 30 days</span>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="earnFree clearfix" style="margin-top: 20px">

		<h2><a href="javascript:void(0);" onclick="$('#referagenttab').trigger('click'); $('#top_menu').removeClass('on');">Earn a <span>Free</span> <?=COMPANY_NAME;?> membership!</a></h2>
		<a href="javascript:void(0);" onclick="$('#referagenttab').trigger('click'); $('#top_menu').removeClass('on');">Learn More</a>

	</div>


<? /*
	<div class="earnFreeLv clearfix" style="margin-top: 20px">
		<a href="javascript:void(0);" onclick="$('#referagenttab').trigger('click'); $('#top_menu').removeClass('on');">
			<span class="fill"> </span>
		</a>
	</div>
*/ ?>
	
	<div class="clearfix">
		<div class="dashboardBox half clearfix readPosts">
			<div class="dashboardHeading clearfix">
				<div class="headingIcon"></div>
				<h4>People Reading Your Posts</h4>
			</div>
			<div class="summary clearfix">
				<div class="summaryTxt">
					<label>Last 30 Days</label>
					<div class="data"><?=number_format($your_clicks);?></div>
				</div>
				<div class="summaryIcon"></div>
			</div>
			<ul class="readers">
				<li class="clearfix">
					<span class="label">Readers Today...</span>
					<span class="count"><?=number_format($this->clicks_today);?></span>
				</li>
				<li class="clearfix">
					<span class="label">Readers This Week...</span>
					<span class="count"><?=number_format($this->clicks_seven);?></span>
				</li>
			</ul>
		</div>
		<div class="dashboardBox half clearfix buzzScore">
			<div class="dashboardHeading clearfix">
				<div class="headingIcon"></div>
				<h4>Your BuzzScore</h4>
			</div>
			<div class="summary">
				
				<?
					$posts = $this->member->monthly_posts();
					$reach = $this->member->total_reach();
			
					//echo $total_touches . " " . $posts . " " . $reach ."<BR>";	
				
					$raw_buzz = 0;
					if($posts && $reach)
					{										
						//TOTAL NUM TOUCHES / TOTAL NUMBER OF POSTS / REACH
						$raw_buzz = (($total_touches / $posts) / $reach) * BUZZ_WEIGHT;									
					}

$raw_buzz+=40;

						
					if($raw_buzz <= 15)
					{
						$buzz_class = "";										
						$quote = "Low.  Try liking<br/> and commenting on others' posts.";			
					}
					elseif($raw_buzz > 15 && $raw_buzz <= 30)
					{
						$buzz_class = "mediumLow";
						$quote = "Not bad.  Try posting<br/> more personal updates.";
					}
					elseif($raw_buzz > 30 && $raw_buzz <= 45)
					{
						$buzz_class = "medium";
						$quote = "Good.  Stick with it<br/> you're getting results.";
					}
					elseif($raw_buzz > 45 && $raw_buzz <= 60)
					{
						$buzz_class = "mediumHigh";
						$quote = "Great!  Your social media<br/> is really buzzing.";
					}
					elseif($raw_buzz > 60)
					{
						$buzz_class = "high";
						$quote = "Amazing!  You're the <br/>centre of your social circle!";
					}										
						
				?>
				
				<div class="data"><?=round($raw_buzz);?></div>
				<label><?=$quote;?></label>
			</div>
			<div class="buzzImg">
				<label>Low</label>
				<span class="<?=$buzz_class;?>"></span>
				<label>High</label>
			</div>
		</div>
	</div>
	<div class="clearfix">
		<div class="dashboardBox half clearfix reach">
			<div class="dashboardHeading clearfix">
				<div class="headingIcon"></div>
				<h4>Your <?=COMPANY_NAME;?> Reach</h4>
			</div>
			<div class="summary clearfix">
				<div class="summaryTxt">
					<label>Total</label>
					<div class="data"><?=number_format($your_reach);?></div>
				</div>
				<div class="summaryIcon"></div>
			</div>
			<div class="individualReach clearfix">	
				<ul class="reachBox">
					<li class="clearfix">
						<span class="label">Facebook</span>
						<span class="count"><?=number_format($this->member->facebook_friend_count);?></span>
					</li>
					<li class="clearfix">
						<span class="label">Twitter</span>
						<span class="count"><?=number_format($this->member->twitter_followers);?></span>
					</li>
				</ul>
				<ul class="reachBox">
					<li class="clearfix">
						<span class="label">Fan Page</span>
						<span class="count"><?=number_format($this->member->fanpage_count());?></span>
					</li>
					<li class="clearfix">
						<span class="label">Linkedin</span>
						<span class="count"><?=number_format($this->member->linkedin_friends_count);?></span>
					</li>
				</ul>
			</div>
			<div class="avgCbr clearfix">
				<span class="label">Average <?=COMPANY_NAME;?> Reach (cbr)...</span>
				<span class="count"><?=number_format($avg_friends);?></span>
				<span class="arrow"></span>
			</div>
		</div>
		<div class="dashboardBox half clearfix settings">
			<div class="dashboardHeading clearfix">
				<div class="headingIcon"></div>
				<h4>Settings</h4>
			</div>
			<div class="summary">
				<label>This Week</label>
				<div class="data"><?=$this->member->weekly_posts();?> POSTS</div>
			</div>
			
			<?
				$fb_posts = $this->member->weekly_posts_facebook();
				
				if($fb_posts > 7)
				{									
					$facebook = ($fb_posts / 2);				 	
					$fanpage  = ($fb_posts / 2);
				}
				else
				{
					$facebook = $fb_posts;
					$fanpage  = $this->member->weekly_posts_fanpage();
				}
			
			?>
			
			<div class="individualPost clearfix">	
				<ul class="postBox">
					<li class="clearfix">
						<span class="postIcon fb"></span>
						<span class="label">Facebook</span>
						<span class="count"><?=$facebook;?></span>
					</li>
					<li class="clearfix">
						<span class="postIcon twt"></span>
						<span class="label">Twitter</span>
						<span class="count"><?=$this->member->weekly_posts_twitter();?></span>
					</li>
				</ul>
				<ul class="postBox">
					<li class="clearfix">
						<span class="postIcon fp"></span>
						<span class="label">Fan Page</span>
						<span class="count"><?=$fanpage;?></span>
					</li>
					<li class="clearfix">
						<span class="postIcon lnkin"></span>
						<span class="label">Linkedin</span>
						<span class="count"><?=$this->member->weekly_posts_linkedin();?></span>
					</li>
				</ul>
			</div>
			<div class="curSetting clearfix">
				<label>Your current setting</label>
				<span class="count clearfix">
					<input type="text" value="<?=$this->member->frequency_id;?>" />
					<span>posts/<br/>week</span>
				</span>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
	<? /**********************************
	chart1 = new Highcharts.Chart({
		chart: {
			renderTo: 'linechart',
			type: 'line',
			marginRight: 0,
			marginBottom: 25,
			marginTop: 25,
			marginLeft: 35,
			width: 678
		},
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
				'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: null
			},
			plotLines: [{
				value: 0,
				width: 1,
				color: '#808080'
			}]
		},
		tooltip: {
			formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+
					this.x +': '+ this.y +'�C';
			}
		},
		series: [{
			name: 'Tokyo',
			data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
			marker: {
				fillColor: '#ffffff',
				lineWidth: 2,
				radius: 5,
				lineColor: null //inherit from series
			}
		}, {
			name: 'New York',
			data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5],
			marker:{
				symbol: "circle"
			}
		}],
		colors: ['#8dcf68', '#14abf9'],
		legend: {
			enabled: true
		},
		title: {
			text: ""
		}
	});
	
	****************************************/ ?>
	
	
	<? if($your_clicks != 0 or $this->total_likes != 0 or $this->total_comments != 0 or $this->total_comments != 0 or $your_total_shares != 0) : ?>
	
	chart2 = new Highcharts.Chart({
		chart: {
			renderTo: 'coloredChart',
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			height: 165,
			width: 165,
			margin : [0, 0, 0, 0]
		},
		colors:["#f3eb73", "#88db57", "#69c5f5", "#ec8b78"],
		tooltip: {
			enabled: false
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: false
				},
				showInLegend: true
			}
		},
		series: [{
			type: 'pie',
			name: 'Browser share',
			data: [
				['clicks',   <?=$your_clicks;?>],
				['likes',       <?=($this->total_likes * LIKE_WEIGHT);?>],
				['comments',    <?=($this->total_comments * COMMENT_WEIGHT);?>],
				['shares',     <?=($your_total_shares * SHARE_WEIGHT);?>]
			]
		}]
	});

	<? endif; ?>

	chart3 = new Highcharts.Chart({
		chart: {
			renderTo: 'bwChart',
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			height: 135,
			width: 135,
			margin: [30, 0, -10, 0]
		},
		colors:["#b5b5b5", "#a6a6a6", "#dddddd", "#c6c6c6"],
		tooltip: {
			enabled: false
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: false
				},
				showInLegend: true
			}
		},
		series: [{
			type: 'pie',
			name: 'Browser share',
			data: [
				['clicks', <?=($this->dash_stats->avg_clicks);?>],
				['likes',  <?=($this->dash_stats->avg_likes * LIKE_WEIGHT);?>],
				['comments', <?=($this->dash_stats->avg_comments * COMMENT_WEIGHT);?>],
				['shares', <?=($this->dash_stats->avg_shares * SHARE_WEIGHT);?>]
			]
		}]
	});
	
    
</script>