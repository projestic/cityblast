			<li class="dashboardRow clearfix">
				<ul>
					<li class="dashboardFieldname">
						Publishing City
						<span class="fieldhelp">Choose where to focus <?=COMPANY_NAME;?>'s marketing efforts.</span>
					</li>
					<li class="dashboardField">
						<? if(isset($this->notification)) : ?>
							<p style="color: #790000; font-style: italic;"><?=$this->notification; ?></p>
						<? endif; ?>
					
					
							<span class="uiSelectWrapper">
								<span class="uiSelectInner">
									<select name="city_id" id="defaultpubcity" class="uiSelect" style="width: 468px;">
									<?php 
									foreach($this->cities as $city)
									{
										//print_r($city);
										
										echo "<option value='".$city->id."' ";
										if($this->selectedCity == $city->id) echo " selected ";
										if(isset($this->city_id_passthru) && !empty($this->city_id_passthru) && $this->city_id_passthru == $city->id) echo ' selected '; 
										echo ">".$city->name;
										if(isset($city->state) && !empty($city->state)) echo ", " .$city->state;
										echo "</option>"; 	
									}										
									?>				
									</select>
								</span>
							</span>
						</form>
					</li>
				</ul>
			</li>