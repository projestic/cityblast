
<ul class="dashboardForm clearfix">

	<li class="dashboardRowHeader">Publish Settings</li>
				
	<li class="dashboardRow clearfix">
	
		<ul>
			<li class="dashboardFieldname">Facebook
			<? /*<br/><span style="margin-top: 3px; font-size: 9px;">Have you recently reset your Facebook password?<br/>You'll need to <a href="/twitter/?settings=1" class="right">reset your Facebook connection.</a></span>*/ ?>
			</li>
			<li class="dashboardField">
	
				<? if($this->hasFacebookPublishPermissions && !empty($member->facebook_publish_flag)) : ?>
					<h5 class="dashboardSettingHeading"><img src="/images/blank.gif" class="uiIcon" />Facebook is working.</h5>
					<p>It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
					business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
				
				<? elseif(!$this->hasFacebookPublishPermissions) : ?>
					<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Facebook Publishing Issue.  No leads.</h5>
					<p style="color: #990000;">Oh Dear! There seems to be a problem with your Facebook Publishing settings. You have not given us permission
						to publish content to your Facebook page. Without permission, it's impossible for us to keep your social media presence professional and up to date!</p>				
				
				<? else : ?>

					<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Facebook is Off.  No leads.</h5>
					<p style="color: #b7b7b7;">It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
					business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
					
				<? endif; ?>
				
			
				<span class="hr">&nbsp;</span>
				
				<? if($this->hasFacebookPublishPermissions && !empty($member->facebook_publish_flag)) : ?>
					
					<a href="/member/turn_off/facebook" class="dashboardSwitch callthroughajax" style="float: right;"><img src="/images/blank.gif" /></a>
					<span style="float: left; line-height: 40px; font-weight: bold; font-size: 11px; opacity: 0.6;"><strong style="color: #cc0000;">Note: </strong>Turning this 
						off means no new leads will be generated.</span>
				<? elseif($this->hasFacebookPublishPermissions && empty($member->facebook_publish_flag)) : ?>
	
					<a href="/member/turn_on/facebook" class="dashboardSwitch callthroughajax off" style="float: right;"><img src="/images/blank.gif" /></a>
					<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
				<?php else: ?>
					<a href="/member/publish_permissions_enable" id="facebook_permissions" class="uiButton red right" style='font-size: 12px;'>Grant Permission to Publish to my Wall!</a>

				<? endif; ?>
				
			</li>
		</ul>
			
	</li>
	
	<li class="dashboardRow clearfix">
		<ul>
			<li class="dashboardFieldname">Twitter

				<span class="fieldhelp">Have you recently reset your Twitter password? You'll need to hit the Reset Button below:</span>
				<a href="/twitter/?settings=1" class="dashboardReset"><img src="/images/blank.gif" /></a>
				
			</li>
			
			<li class="dashboardField">
			
				<? if( !empty($member->twitter_publish_flag) ) : ?>
				
					<?php if(empty($member->twitter_id)): ?>
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Twitter is Off.  No leads.</h5>
						
						<p style="color: #b7b7b7;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers 
							stay tuned with consistent and professional local real estate content.</p>
						
					<?php else: ?>
						<h5 class="dashboardSettingHeading"><img src="/images/blank.gif" class="uiIcon" />Twitter is working.</h5>
	
						<p style="color: #b7b7b7;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers 
							stay tuned with consistent and professional local real estate content.</p>
	
					<? endif; ?>
				
				<? else : ?>

	
					<?php if(empty($member->twitter_id)): ?>
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Twitter is Off.  No leads.</h5>
						
						<p style="color: #b7b7b7;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers stay 
							tuned with consistent and professional local real estate content.</p>
						
					<?php else: ?>
						
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />Twitter is Off.  No leads.</h5>
	
						<p style="color: #b7b7b7;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers stay 
							tuned with consistent and professional local real estate content.</p>
	
					<? endif; ?>
					
				<? endif; ?>
				
				<span class="hr">&nbsp;</span>
				
				<? if( !empty($member->twitter_publish_flag) ) : ?>
				
					<?php if(empty($member->twitter_id)): ?>
						<a href="/twitter/?settings=1" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
					<?php else: ?>
						<a href="/member/turn_off/twitter" class="dashboardSwitch" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold; font-size: 11px; opacity: 0.6;"><strong style="color: #cc0000;">Note: </strong>Turning 
							this off means no new leads will be generated.</span>
					<? endif; ?>
				
				<? else : ?>
	
					<?php if(empty($member->twitter_id)): ?>
						<a href="/twitter/?settings=1" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
					<?php else: ?>
						<a href="/member/turn_on/twitter" class="dashboardSwitch off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>																		
					<? endif; ?>
					
				<? endif; ?>
				
			</li>
		</ul>
	</li>
	
	<li class="dashboardRow clearfix">
		<ul>
			<li class="dashboardFieldname">LinkedIn

				<span class="fieldhelp">Have you recently reset your LinkedIn password? You'll need to hit the Reset Button below:</span>
				<a href="/linkedin/?settings=1" class="dashboardReset"><img src="/images/blank.gif" /></a>
			
			</li>
			
				<? if( !empty($member->linkedin_publish_flag) ) : ?>
	
					<?php if(empty($member->linkedin_id)): ?>									
						<li class="dashboardField">
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />LinkedIn is Off.  No leads.</h5>
						<p style="color: #b7b7b7;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying 
							visible and active in this network is highly recommended.</p>
					<?php else: ?>
						<li class="dashboardField">
						<h5 class="dashboardSettingHeading"><img src="/images/blank.gif" class="uiIcon" />LinkedIn is working.</h5>
						<p style="color: #b7b7b7;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying 
							visible and active in this network is highly recommended.</p>
					<? endif; ?>
				
				<? else : ?>
	
					<?php if(empty($member->linkedin_id)): ?>
					
						<li class="dashboardField">
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />LinkedIn is Off.  No leads.</h5>			
						<p style="color: #b7b7b7;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying 
							visible and active in this network is highly recommended.</p>
					<?php else: ?>
						
						<li class="dashboardField">
						<h5 class="dashboardSettingHeading off"><img src="/images/blank.gif" class="uiIcon" />LinkedIn is Off.  No leads.</h5>
						<p style="color: #b7b7b7;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying 
							visible and active in this network is highly recommended.</p>
					<? endif; ?>
					
				<? endif; ?>
				
				<span class="hr">&nbsp;</span>

				<? if( !empty($member->linkedin_publish_flag) ) : ?>
				
					<?php if(!empty($member->linkedin_id)): ?>
						
						<a href="/member/turn_off/linkedin" class="dashboardSwitch " style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold; font-size: 11px; opacity: 0.6;"><strong style="color: #cc0000;">Note: </strong>Turning 
							this off means no new leads will be generated.</span>
															
					<?php else: ?>
				
						<a href="/member/turn_on/linkedin" class="dashboardSwitch  off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
						
				
					<? endif; ?>
				
				<? else : ?>
				
					<?php if(empty($member->linkedin_id)): ?>
						<a href="/member/turn_on/linkedin" class="dashboardSwitch callthroughajax off" style="float: right;"><img src="/images/blank.gif" /></a>									
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
					<?php else: ?>
					
						<a href="/member/turn_on/linkedin" class="dashboardSwitch callthroughajax off" style="float: right;"><img src="/images/blank.gif" /></a>
						<span style="float: left; line-height: 40px; font-weight: bold;">Attract new clients now.</span>
						
					<? endif; ?>
					
				<? endif; ?>
			</li>
		</ul>
	</li>
	
</ul>

<? if(isset($member)):

	//Set up the FACEBOOK VARIABLES...
	$publish_array['access_token'] = $member->access_token;
	$facebook = $member->network_app_facebook->getFacebookAPI();

	//echo "ACCESS TOKEN:".$member->access_token . "<BR>";
	
	if(isset($member->access_token) && !empty($member->access_token)):
		
		$api_url		= "https://graph.facebook.com/me/accounts?access_token=" . $member->access_token;
		
		$accounts = null;
		
		if($account_info = file_get_contents($api_url)):
		
			$accounts = json_decode($account_info);
			
			$page_count = 0;
			foreach ($accounts->data as $account){
				if($account->category != "Application"){
					$page_count++;
				}
			}			

			if( !$this->hasManagePagesPermission) : 
			?>
			<div id="fb_fanpages">
				
					<div class="box black10 clearfix" id="fb_fanpages_load" style="padding: 20px; text-align: center;">
						
						<h3 style="text-align: center; font-size: 18px; line-height: 32px; margin: 0px; padding-top: 3px; display: inline-block;">We do not have permission to manage your Fan Pages</h3>
						<a href="/member/fanpages_permissions_enable" id="fanpages_permissions" class="uiButton red" style='font-size: 12px;margin-left: auto;margin-right: auto;float: none;display: block;width: 275px;'>Grant permission to manage my Fanpages</a>
						
					</div>
				
				</div>
			<?php
			endif;

			if( $page_count > 0): ?>

				<div id="fb_fanpages">
				
					<div class="box black10 clearfix" id="fb_fanpages_load" style="padding: 20px; text-align: center;">
						
						<h3 style="text-align: center; font-size: 18px; line-height: 32px; margin: 0px; padding-top: 3px; display: inline-block;">
							You have <?=$page_count?> Facebook Fan Page<?=$page_count>1?'s':''?>.
							<a href="javascript:void(0);" id="load_fb_fanpages" class="uiButton small" style="margin: -3px 0 0 10px; padding: 10px; float: right; font-size: 18px!important;">Manage My Fan Page Setting<?=$page_count>1?'s':''?></a>
						</h3>
						
					</div>
				
				</div>
						
				<script type="text/javascript">
					$(document).ready(function(){ $('#load_fb_fanpages').click(function(){ $('#fb_fanpages').load('/member/settings_fanpages'); $('#fb_fanpages_load').removeClass('black10'); $('#fb_fanpages_load').html('<img src="/images/dashboard_loading.gif" height="150" width="150" style="margin: 0 260px 0;" />'); }); });
				</script>
				
			<? endif; ?>
			
		<? endif; ?>
		
	<? endif; ?>
	
<? endif; ?>