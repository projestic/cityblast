<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?=COMPANY_NAME;?>: Invoice #<?=$this->invoice->id?></title>
</head>

<body onload="window.print();">

<div class="grid_12" id="invoiceContainer">


	<h3 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 0px; padding-top: 25px;">Invoicing Details</h3>

			<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="24"><h3>Invoice Number: #<?=number_format(sprintf("%03d",$this->invoice->id), 0, '.', '-');?></h3></td>

					<td height="24" style="text-align: right;"><strong>Invoice Date: </strong><? if(isset($this->invoice->created_at) && !empty($this->invoice->created_at)) echo $this->invoice->created_at->format("M d, Y"); ?></td>
				</tr>
				<tr>
					<td width="50%" valign="top" style="padding-top: 15px; padding-bottom: 15px; border-top: solid 1px #d9d9d9; border-bottom: solid 1px #d9d9d9; background-color: #f7f7f7;">
						<strong><?=$this->member->first_name;?> <?=$this->member->last_name;?></strong><br />
						<? if($this->member->default_city) echo $this->member->default_city->name;?>,
						<? if(isset($this->invoice->state) && !empty($this->invoice->state)  && $this->invoice->state != "empty" && $this->invoice->state != "undefined") echo $this->invoice->state;?>,
						<? if(isset($this->invoice->country) && !empty($this->invoice->country) && $this->invoice->country != "empty" && $this->invoice->country != "undefined") echo $this->invoice->country;?><br/><br/>
						<?php if($this->member->brokerage): ?>
							<?=$this->member->brokerage?><br/>
							<?=$this->member->broker_address?><br/>
						<?php endif; ?>
					</td>
					<td width="50%" valign="top" style="padding-top: 15px; padding-bottom: 15px; border-top: solid 1px #d9d9d9; border-bottom: solid 1px #d9d9d9; background-color: #f7f7f7;">
						<strong><?=COMPANY_NAME;?></strong><br />
						<?=COMPANY_ADDRESS;?><br />
						<?=COMPANY_CITY;?>, <?=COMPANY_PROVINCE;?><br />
						<?=COMPANY_POSTAL;?><br />
						<?=COMPANY_COUNTRY;?>
						<?if(defined('COMPANY_TAX_TYPE') && defined('COMPANY_TAX_ID')):?>
						<br /><br />
						<strong><?=COMPANY_TAX_TYPE . '#';?>:</strong> <?=COMPANY_TAX_ID;?>
						<?endif;?>
					</td>
				</tr>
				<tr>
					<td height="30"></td>
					<td></td>
				</tr>
			</table>

			<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th width="300" align="left">Purchased Item</th>
					<th width="100">Quantity</th>
					<th width="100" align="right">Unit Price</th>
					<th width="100" align="right">Item Total</th>
				</tr>
				<tr>
					<td><?=COMPANY_NAME;?> - Facebook + Twitter + LinkedIn</td>
					<td align="center">1</td>
					<td align="right">
						<? if(	strtoupper($this->invoice->paypal_confirmation_number) == "FREE BLAST") : ?>
							Free Blast!
						<? else : ?>
							$<?=number_format($this->invoice->amount, 2);?>
						<? endif;?>
					</td>
					<td align="right">
						
							$<?=number_format($this->invoice->amount, 2);?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right" style="padding-top: 12px;"><strong>Subtotal</strong></td>
					<td align="right" style="padding-top: 12px;">
						
							$<?=number_format($this->invoice->amount, 2);?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right"><strong>Tax</strong></td>
					<td align="right">
						$<?=number_format( $this->invoice->taxes , 2);?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right"><strong>Total</strong></td>
					<td align="right">
						<strong>$<?=number_format( $this->invoice->amount + $this->invoice->taxes , 2);?></strong>
					</td>
				</tr>
			</table>

	

</div>

</body>
</html>
