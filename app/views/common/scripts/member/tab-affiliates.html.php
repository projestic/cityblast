<h1 class="dashboardSection">Your Affiliates
	<span style="float: right; font-size: 16px;">Active: <?=$this->total_active?>, Total: <?= $this->paginator->getTotalItemCount();?></span>
</h1>

<?php
	$pageNumder = $this->paginator->getCurrentPageNumber();
	$initItemNumder = $this->paginator->getAbsoluteItemNumber(1, $pageNumder);
?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="ui-datatable bordered zebrad hover">
	<tr>
		<th scope="col" style="text-align: left">#</th>
		<th scope="col" style="text-align: left">Name</th>
		<th scope="col" style="text-align: left">Email</th>
		<?/*<th scope="col" style="text-align: left">Phone</th>*/?>
		<th scope="col" style="text-align: left">Status</th>
	</tr>
	<?php $i=$initItemNumder; foreach($this->paginator as $affiliate): ?>
	
		<tr>
			<td scope="col" style="text-align: left;"><?php echo $i;?></td>
			<td scope="col" style="text-align: left;"><?php echo $affiliate->first_name . " " . $affiliate->last_name;?></td>
			<td scope="col" style="text-align: left;"><a href="mailto:<?php echo $affiliate->email;?>"><?php echo $affiliate->email;?></a></td>
			<?/*<td scope="col" style="text-align: left;"><?php echo $affiliate->phone;?></td>*/?>
			<td scope="col" style="text-align: left;"><?php echo $affiliate->payment_status;?></td>
		</tr>
		
	<?php $i++; endforeach;?>
</table>
		
<div style="float: right;">
	<?php echo $this->pagination();?>
</div>