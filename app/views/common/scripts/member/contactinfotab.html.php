<? $member = isset($_SESSION['member']) && isset($_SESSION['member']->id) ? $_SESSION['member'] : new stdClass(); ?>

<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript">

	$(document).ready(function () {

		$('#file_upload').uploadify({
			'uploader'  : '/js/uploadify/uploadify.swf',
			'script'    : '/blast/uploadimage',
			'cancelImg' : '/js/uploadify/cancel.png',
			'folder'    : '/images/',
			'auto'      : true,
			'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
			'fileDesc'  : 'Image Files',
			'buttonImg' : '/images/browse_button.png',
			'removeCompleted' : true,
			'height'	: 30,
			'width'		: 106,
			'name'		: 'fileUploaded',

			'onComplete': function(event, ID, fileObj, response, data) {
				//alert('RESPONSE: ' +response+' -- DATA:'+fileObj.toSource());
				$('#fileUploaded').val(response);

				//$("#file_uploadQueue").hide();
				//$("#file_uploadStatus").show();
				//$("#file_uploadStatus").html("<span style='display: block; text-align: center;'>Upload Complete</span>");
				$('#member_photo').attr('src', response);
				//$('#member_photo_progress').css('display', 'none');

				setPhoto(response);

			},

			'onError'	: function (event,ID,fileObj,errorObj) {
				alert(errorObj.type + ' Error: ' + errorObj.info);
			}/*,

			'onProgress'  : function(event,ID,fileObj,data) {
				//$('#member_photo_progress').css('display', 'block');
				//$('#member_photo_progress').css('height', Math.round(data.percentage/100 * 106) + 'px');
				return false;
			}*/

		});

		if(swfobject.hasFlashPlayerVersion('9.0.24') == false)
		{
			if(confirm("You do not have the minimum required flash version. Do you want to download it ?")){
				window.location.href= "http://www.adobe.com/go/getflashplayer";
			}
		}


		function setPhoto(photo) {

			$.ajax({

				type: "POST",
				url: "/member/setphoto",
				data: "photo="+encodeURI(photo),
				dataType: "json",

				success: function(msg)
				{
					if (msg.error) {
						alert("File upload failed!");
					} else {
						$('#photoSetStatus').html('<img src="/images/right.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your photo is set.');
					}
					//alert( "About to redirect");
					//window.location =	"/member/signup/both";
				}
			});
		}


	})
</script>


		<div class="grid_12">



			<div class="box form clearfix">


				<ul class="uiForm full clearfix">
					<li class="uiFormRowHeader">My Account</li>
					
				
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">
								Your photo
							</li>
							<li class="field">
								<?php if( !isset($this->member->photo) || empty($this->member->photo) ) : ?>
									<h5 id="photoSetStatus"><img src="/images/cross.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your photo is not set.</h5>
									<div style="padding-left: 25px; font-size: 12px; line-height: 20px;">
										<img src="/images/blank.gif" width="96" id="member_photo" style="margin-bottom: 3px; margin-left: 5px;"  />
										<br />
										<input id="file_upload" name="file_upload" type="file" width="88" height="104" />
										<div id="file_uploadStatus" class="uploadstatus"></div>
										<div id="main_photo_progress" class="progress"></div>
									</div>
								<?php else: ?>
									<h5 id="photoSetStatus"><img src="/images/right.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your photo is set.</h5>
									<div style="padding-left: 25px; font-size: 12px; line-height: 20px;">
										<img src="<?php echo CDN_URL . $this->member->photo ?>" width="96" id="member_photo" style="margin-bottom: 3px; margin-left: 5px;" />
										<br />
										<input id="file_upload" name="file_upload" type="file" width="88" height="104" />
										<div id="file_uploadStatus" class="uploadstatus"></div>
										<div id="main_photo_progress" class="progress"></div>
									</div>
								<? endif; ?>
								<form name="photo_change" id="photo_change" action="/member/changephoto/" method="POST">
									<input type="hidden" name="fileupload" id="fileupload" value="<?= isset($this->member->photo) && !empty($this->member->photo) ? CDN_URL . $this->member->photo : '' ?>" />
								</form>
							</li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">
								Contact Info
								<span class="fieldhelp">If you'd like other agents to be able to contact you directly, please enter your contact number here.</span>
							</li>
							<li class="field">
								<?php if( !isset($this->member->phone) || empty($this->member->phone) ) : ?>
									<h5><img src="/images/cross.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your contact information is not set.</h5>
									<div style="padding-left: 21px; font-size: 12px; line-height: 20px;">
										<strong>Phone: </strong><span style="color: #999;">N/A</span>
									</div>
								<?php else: ?>
									<h5><img src="/images/right.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your contact information is set.</h5>
									<div style="padding-left: 21px; font-size: 12px; line-height: 20px;">
										<strong>Phone: </strong><?php echo $this->member->phone;?>
									</div>
								<? endif; ?>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<?php if( !isset($this->member->phone) || empty($this->member->phone) ) : ?>
									<a href="<?php echo $this->url( array("controller"=>"member","action"=>"contact"),"default");?>" class="uiButton right" style="width: 195px;">Add Contact Info</a>
								<?php else: ?>
									<a href="<?php echo $this->url( array("controller"=>"member","action"=>"contact"),"default");?>" class="uiButton cancel right" style="width: 195px;">Update Contact Info</a>
								<? endif; ?>
							</li>
						</ul>
					</li>
					
					

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Current Email
								<span class="fieldhelp">Is this your REAL email? If not, you're not receiving your new client leads from our site!  Please enter an email you check daily.</span>
							</li>

							<li class="field cc">
								
								<?php if( empty($this->member->email) && empty($this->member->email_override) ) : ?>
									<h5><img src="/images/cross.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your Email is not set.</h5>
								<?php else: ?>
									<h5><img src="/images/right.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your Email is set.</h5>
								<?php endif; ?>
																
								<div style="padding-left: 21px; font-size: 12px; line-height: 20px;">
								<?
									if(isset($this->member->email_override) && !empty($this->member->email_override))
									{
										echo $this->member->email_override;
									}
									else
									{
										echo $this->member->email;
									}
								?>
								</div>
							</li>
					
						</ul>
					</li>
					

					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<?php if( empty($this->member->email) && empty($this->member->email_override) ) : ?>
									<a href="<?php echo $this->url( array("controller"=>"member","action"=>"emailoverride"),"default");?>" class="uiButton right" style="width: 195px;">Update Your Email</a>
								<? else: ?>
									<a href="<?php echo $this->url( array("controller"=>"member","action"=>"emailoverride"),"default");?>" class="uiButton cancel right" style="width: 195px;">Update Your Email</a>
								<? endif; ?>
							</li>
						</ul>
					</li>

				
	
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">
								Brokerage Info
								<span class="fieldhelp"><?=COMPANY_NAME;?> requires your Brokerage info in order to ensure your Blasts are compliant with advertising rules in all jurisdictions.</span>
							</li>
							<li class="field">
								<?php if( empty($this->member->brokerage) || empty($this->member->broker_address) ) : ?>
									<h5><img src="/images/cross.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your Brokerage is not set.</h5>
									<div style="padding-left: 21px; font-size: 12px; line-height: 20px;">
										<strong>Name: </strong><span style="color: #999;">N/A</span><br/>
										<strong>Address: </strong><span style="color: #999;">N/A</span>
									</div>
								<?php else: ?>
									<h5><img src="/images/right.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your brokerage is set.</h5>
									<div style="padding-left: 21px; font-size: 12px; line-height: 20px;">
										<strong>Name: </strong><?php echo $this->member->brokerage;?><br/>
										<strong>Address: </strong><?php echo $this->member->broker_address;?>
									</div>
								<? endif; ?>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<?php if( empty($this->member->brokerage) || empty($this->member->broker_address) ) : ?>
									<a href="<?php echo $this->url( array("controller"=>"member","action"=>"brokerage"),"default");?>" class="uiButton right" style="width: 195px;">Add Brokerage Info</a>
								<?php else: ?>
									<a href="<?php echo $this->url( array("controller"=>"member","action"=>"brokerage"),"default");?>" class="uiButton cancel right" style="width: 195px;">Update Your Brokerage</a>
								<? endif; ?>
							</li>
						</ul>
					</li>
		
					
					<? /*******************************
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Invoices</li>
							<li class="field">
								
								<? if($this->member->payment_status == "paid") : ?>
									<h5><img src="/images/right.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Your ClientFinder account is active.</h5>
								<?php else: ?>
									<h5><img src="/images/cross.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">Oh dear, your Clientfinder account is inactive.</h5>
								<?php endif;?>
									
								<?php /*if( ($manyPayments = count($this->payments) > 0) ) : ?>
									<h5><img src="/images/right.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">You have Blasted <?php echo count($this->payments);?> Listings.</h5>
								<?php else: ?>
									<h5><img src="/images/cross.jpg" width="17" height="17" style="float: left; padding: 2px 4px 0 0;">You have yet to Blast.</h5>
									<div style="padding-left: 21px; font-size: 12px; line-height: 20px;">
										<span style="color: #999;">You have no invoices.</span>
									</div>
								<?php endif;*************?>
							</li>
						</ul>
					</li>

					**********************************/ ?>
					

				</ul>
			</div>
				
		</div>
	
	</div>

			
</div>



<? /*if($this->member->payment_status != "paid") : ?>

<? endif; */ ?>
		
<script type="text/javascript">
	$(document).ready(function(){
		
		$( "#slider" ).slider({
			value:<?=empty($member->frequency_id) ? 1 : $member->frequency_id;?>,
			min: 1,
			max: 7,
			step: 1,
			slide: function( event, ui ) 
			{
				$("#amount").val(ui.value);
			},
			change: function( event, ui ) 
			{			
				$('#frequency_id').val(ui.value);
				$("#frequency_change").submit();
				/*alert('Current frequency:'+$("#frequency_id").val());*/
			}
		});
		$("#amount").val($( "#slider" ).slider( "value" ) );
		
		
		$('#city_id').change(function() 
		{
			$("#city_change").submit();
		});
	}); 
</script>