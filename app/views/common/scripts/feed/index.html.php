<div class="grid_12">
	
	<div class="box clearfix">

		<div class="box_section">
			<h1>Were you looking for an RSS Feed?</h1>
		</div>

		<h3>We don't support RSS</h3>
	
		<p><?=COMPANY_NAME;?> Clientfinder is a personalized social media management tool. We help you maintain your professional image on Twitter, LinkedIn and Facebook. Unfortunately, at this time we do not support RSS.</p>
	
	</div>
	
</div>


<? /**********************************************************************

<?
	echo '<';
	echo '?';
	echo 'xml version="1.0" ';
	echo '?';
	echo '>';
?>
<rss version="2.0">
	<channel>
		<title>City-Blast.com</title>
		<description>City Blast Is How The World's Realtors Unleash the Massive Marketing Power of Their Whole City's Social Media Network. Sell it fast. City-Blast</description>
		
		<? $today = getdate(); ?>
		<pubDate><?=date("D, d M Y H:i:s", $today[0]);?> +0000</pubDate> 
		<language>en</language>
	
		<? if(is_array($this->listings)) { ?>
			<? foreach($this->listings as $listing) { ?>
		     	
		     	<item>
		    			<title>BlastID <?=$listing->id;?></title>
		    			<guid isPermaLink="true"><?=APP_URL;?>/listing/view/<?=$listing->id;?></guid>
					<link><?=APP_URL;?>/listing/view/<?=$listing->id;?></link>
					<description><?=$listing->message;?></description>
				</item>
			
			<? } ?>			
		<? } ?>   
	</channel>
</rss> 

***************************************************************************/

?>