<h3><?=$this->tag->name;?></h3>
<p><?=$this->tag->description;?></p>
<p style="display:none;"><?=$this->listing->id;?></p>

<? if($this->listing->big_image) : ?>

	<div style="margin: 10px auto; width: 460px; clear:both">
		<div class="facebook-block">
			<div class="fbb-content">
				<div class="clearfix fbb-top">
					<? if(isset($_SESSION['member'])):?>
					<a href="#" target="_blank"><img src="https://graph.facebook.com/<?=$_SESSION['member']->uid ?>/picture" width="50" height="50" alt="" /></a>
					<div class="fbb-top-title-container">
						<div class="clearfix">
							<div class="fbb-top-button">
								<a role="button" href="#" target="_blank"><i></i>Timeline</a>
							</div>
							<div class="fbb-top-title" style="height: 48px"></div>
							<div class="fbb-top-title">
								<h5><a class="profileLink" href="#" target="_blank"><?=$_SESSION['member']->first_name;?> <?=$_SESSION['member']->last_name;?></a></h5>
								<div class="fbb-top-likes">Community &middot; 1 Like</div>
							</div>
						</div>
					</div>
					<? else: ?>
					<a href="#" target="_blank"><img src="/images/janet-realtor.jpg" width="50" height="50" alt="" /></a>
					<div class="fbb-top-title-container">
						<div class="clearfix">
							<div class="fbb-top-button">
								<a role="button" href="#" target="_blank"><i></i>Timeline</a>
							</div>
							<div class="fbb-top-title" style="height: 48px"></div>
							<div class="fbb-top-title">
								<h5><a class="profileLink" href="#" target="_blank">
									<? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>
									Wendy Weddings
									<? elseif(stristr(APPLICATION_ENV, "mortgagemingler")) : ?>
									Morgan Mortgage
									<? elseif(stristr(APPLICATION_ENV, "insuranceposters")) : ?>
									Andy Agent 
									<? else : ?>
									Janet Realtor
									<? endif; ?></a></h5>
								<div class="fbb-top-likes">Community &middot; 1 Like</div>
							</div>
						</div>
					</div>
					<? endif; ?>
				</div>
				<p><?=$this->listing->message;?></p>
				<div style="line-height: 1px"><a href="#" style="width:442px;" target="_blank">
					<? if (isset($this->image) && !empty($this->image)) : ?>
						<img src="<?=$this->image;?>" width="446" height="<?=$this->new_height;?>" />
					<? endif; ?>
				</a></div>
			</div>
			<div class="fbb-footer">
				<a href="#" target="_blank" title="Like this item" class="like"><i></i>Like</a>
				<a href="#" target="_blank" title="Comment this item" class="comment"><i></i>Comment</a>
				<a href="#" target="_blank" title="Share this item"><i></i>Share</a>
			</div>
		</div>
	</div>

<? else : ?>

	<div style="margin: 10px auto; width: 460px; clear:both">
		<div class="facebook-block">
			<div class="fbb-content">
				<div class="clearfix fbb-top">
					<? if(isset($_SESSION['member'])):?>
					<a href="#" target="_blank"><img src="https://graph.facebook.com/<?=$_SESSION['member']->uid ?>/picture" width="50" height="50" alt="" /></a>
					<div class="fbb-top-title-container">
						<div class="clearfix">
							<div class="fbb-top-button">
								<a role="button" href="#" target="_blank"><i></i>Timeline</a>
							</div>
							<div class="fbb-top-title" style="height: 48px"></div>
							<div class="fbb-top-title">
								<h5><a class="profileLink" href="#" target="_blank"><?=$_SESSION['member']->first_name;?> <?=$_SESSION['member']->last_name;?></a></h5>
								<div class="fbb-top-likes">Community &middot; 1 Like</div>
							</div>
						</div>
					</div>
					<? else: ?>
					<a href="#" target="_blank"><img src="/images/janet-realtor.jpg" width="50" height="50" alt="" /></a>
					<div class="fbb-top-title-container">
						<div class="clearfix">
							<div class="fbb-top-button">
								<a role="button" href="#" target="_blank"><i></i>Timeline</a>
							</div>
							<div class="fbb-top-title" style="height: 48px"></div>
							<div class="fbb-top-title">
								<h5><a class="profileLink" href="#" target="_blank">
									<? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>
									Wendy Weddings
									<? elseif(stristr(APPLICATION_ENV, "mortgagemingler")) : ?>
									Morgan Mortgage
									<? elseif(stristr(APPLICATION_ENV, "insuranceposters")) : ?>
									Andy Agent 
									<? else : ?>
									Janet Realtor
									<? endif; ?></a></h5>
								<div class="fbb-top-likes">Community &middot; 1 Like</div>
							</div>
						</div>
					</div>
					<? endif; ?>
				</div>
				<p><?=$this->listing->message;?></p>
				<div class="inner-content-container">
					<div class="clearfix">
						<div class="icc-image">
							<a href="#" target="_blank" rel="nofollow">
								<? if(isset($this->image) && !empty($this->image)) { ?>
								<img src="<?=$this->listing->getSizedImage(154, 154);?>" width="154" height="154" />
								<? } else { ?>
								<img src="/images/blank.gif" style="background-color: #f2f2f2;" width="154" height="154"/>
								<?php } ?>
							</a>
						</div>
						<div class="icc-text">
							<div class="icc-text-title">
								<a href="#" target="_blank" rel="nofollow"><?=$this->link_name;?></a>
							</div>
							<div class="icc-text-text"><?=$this->description;?></div>
							<div class="icc-text-link"><?=APP_URL;?></div>
						</div>
					</div>
					<a class="full-link" href="#" target="_blank" rel="nofollow"></a>
				</div>
			</div>
		</div>
	</div>

<? endif; ?>
