<!doctype html>
<html>
<head>
	<title>CityBlast listing</title>
	<link type="text/css" rel="stylesheet" href="new-style.css" />
	<style>
		*,ul,ol,h1,h2,h3,h4,h5,h6 {
	margin: 0;
	padding: 0;
}
h1{
	font-size: 48px;
}
h2{
	font-size: 36px;
}
h3 {
	font-size: 24px;
}
h4 {
	font-size: 18px;
}
h5 {
	font-size: 16px;
}
h6 {
	font-size: 14px;
}
iframe {
	display: block;
}
body {
	font-family : "myriad-pro-n4", "myriad-pro", "Helvetica Neue", Helvetica, Arial, sans-serif!important;
	font-weight: 400;
	font-size: 14px;
	background: url(/images/new-images/main-bg.png) center center #f0f2f5;
}
select {
	background-color: #EEE;
	border: 1px solid #C9C9C9 !important;
}
input, input *,
input::-moz-focus-inner,
button::-moz-focus-inner {
	border: 0 none!important;
	outline: none !important;
}
.clear {
	clear:both;
	display:block;
	overflow:hidden;
	visibility:hidden;
	width:0;
	height:0;
	float: none !important;
}
.center {
	text-align: center;
}

/*** COMMON STYLE STARTS ***/

.uiButton.right {
	float: right;
	margin-top: 10px;
}

.uiButtonNew {
	background-color: #6fad51;
	border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	color: #fff;
	display: inline-block;
	font-size: 36px;
	font-weight: 600;
	line-height: 1em;
	outline: none;
	padding: 22px 0;
	position: relative;
	text-decoration: none;
	text-align: center;
	width: 250px;
	behavior: url(/css/PIE.htc);
}
.uiButtonNew:hover,
.uiButtonNew:active,
.uiButton:focus,
a.uiButtonNew:hover,
a.uiButtonNew:active,
a.uiButtonNew:focus {
	color: #fff;
	text-decoration: none;
	outline: none;
}
.selectWrapper {
	border: 1px solid #e1e1e1;
	border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	box-sizing: border-box;
	display: inline-block;
	margin-bottom: 10px;
	padding: 4px 0;
	position: relative;
	behavior: url(/css/PIE.htc);
}
.selectWrapper select {
	-webkit-appearance: none;
	-moz-appearance: none;
	border: none;
	background: url(/images/new-images/select_arrow.png) 99% center no-repeat;
	font-size: 16px;
	height: 22px;
	line-height: 22px;
	margin-bottom: 0;
	padding: 0 0 0 10px;
	outline: medium none !important;
	width: 100%;
}

/*** COMMON STYLE ENDS ***/

/*** HEADER STYLE STARTS ***/
.pageTop {
	display: none;
}
.logoHeader{
	background: url(/images/new-images/header-bg.png) center center #e3e6e8;
	padding: 21px 0;
}
#header_wrapper div.phone {
	display: none;
}
#logo {
	margin-left: 10px;
	float:left;
	width: 220px;
	height: 50px;
	display: inline-block;
}
#user_nav {
	background: url(/images/new-images/opac7.png);
	border-radius: 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	box-shadow: inset 1px 1px 4px 0 #b3b3b3, 1px 1px 0 #f8f8f8, -1px -1px 0 #a4a4a4;
	-moz-box-shadow: inset 1px 1px 4px 0 #b3b3b3, 1px 1px 0 #f8f8f8, -1px -1px 0 #a4a4a4;
	-webkit-box-shadow: inset 1px 1px 4px 0 #b3b3b3, 1px 1px 0 #f8f8f8, -1px -1px 0 #a4a4a4;
	padding: 9px 10px 8px;
	position: relative;
	float:right;
	font-size: 11px;
	margin-right: 20px;
	behavior: url(/css/PIE.htc);
}
#open_login .name {
	background-color: #71ac55;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	color: #fff;
	display: inline-block;
	font-size: 14px;
	margin-left: 5px;
	padding: 0 15px;
	line-height: 30px;
	position: relative;
	text-decoration: none;
	text-shadow: -1px -1px 0 rgba(0,0,0,.3);
	font-family : "myriad-pro-n6", "myriad-pro", "Helvetica Neue", Helvetica, Arial, sans-serif!important;
	font-weight: 600;
	behavior: url(/css/PIE.htc);
}
#open_login .facebook-login {
	background-color: #1c9ada;
	margin-left: 0;	
}
#top_menu {
	float: left;
	color: #666;
	padding: 0;
	position: relative;
}
#top_menu > a {
	float: left;
}
#top_menu .my_account_text {
	font-size: 14px;
	border: 1px solid #bfbfbf;
	color: #666;
	padding: 6px 10px 5px;
	margin-right: 10px;
	position: relative;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	background: #fff;
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#fff), to(#ededed));
	background: -webkit-linear-gradient(#fff, #ededed);
	background: -moz-linear-gradient(#fff, #ededed);
	background: -ms-linear-gradient(#fff, #ededed);
	background: -o-linear-gradient(#fff, #ededed);
	background: linear-gradient(#fff, #ededed);
	-pie-background: linear-gradient(#fff, #ededed);
	behavior: url('/css/PIE.htc');
}
#top_menu .my_account_text img {
	position: absolute;
	top: 9px;
	right: -6px;
}
#top_menu .my_account_user_img {
	background: #d6d6d6;
	border: 1px solid #bfbfbf;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	padding: 2px;
	margin-right: 7px;
	position: relative;
	behavior: url(/css/PIE.htc);
}
#top_menu .my_account_user_img img {
	height: 24px;
}
#top_menu .my_account_down_arrow {
	margin-top: 13px;
}
#top_menu .my_account_popup {
	display: none;
	position: absolute;
	z-index: 1000;
	top: 40px;
	left: -89px;
	background: #fff;
	border: 1px solid #bfbfbf;
	border-radius: 2px;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	width: 250px;
	font-size: 12px;
	-webkit-box-shadow: 3px 4px 15px -2px;
	-moz-box-shadow: 3px 4px 15px -2px;
	box-shadow: 3px 4px 15px -2px;
	behavior: url(/css/PIE.htc);
}
#top_menu .my_account_popup > img {
	position: absolute;
	top: -6px;
	right: 22px;
}
#top_menu .my_account_popup .user_info > span {
	display: block;
}
#top_menu .my_account_popup .user_info,
#top_menu .my_account_popup .user_account,
#top_menu .my_account_popup .logout {
	padding: 10px 20px;
}
#top_menu .my_account_popup .user_info {
	background-color: #ffffe5;
	line-height: 16px;
}
#top_menu .user_info span.name {
	font-weight: bold;
}
#top_menu .my_account_popup .logout {
	background-color: #f2f2f2;
	text-align: right;
}
#top_menu .logout a {
	padding: 5px 10px;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	background-color: #878787;
	color: #fff;
	position: relative;
	display: inline-block;
	behavior: url(/css/PIE.htc);
}
#top_menu .my_account_popup .user_account {
	border-top: 2px solid #f2f2c2;
	border-bottom: 1px solid #d9d9d9;
}
#top_menu .user_account > a,
#top_menu .user_account > ul {
	float: left;
}
#top_menu .user_account > a img {
	height: 50px;
}
#top_menu .user_account > ul {
	margin: 0 0 0 15px;
	padding: 0 5px;
	width: 125px;
}
#top_menu .user_account > ul li {
	color: #3D6DCC;
	list-style-type: disc;
	list-style-position: inside;
}
#top_menu .user_account .clr {
	clear: both;
}
#top_menu.on .my_account_popup {
	display: block;
}
.navigation {
	background: url(/images/new-images/opac12.png);
	border-width: 1px 0;
	border-style: solid;
	border-color: #b1b1b1;
}
.navigation .nav {
	margin: 0;
}
.navigation .nav-pills li {
	position: relative;
}
.navigation .nav-pills li a {
	border-radius: 0;
	color: #333;
	font-size: 14px;
	font-weight: bold;
	margin: -1px 0 -1px 1px;
	padding : 13px 18px;
	text-shadow: 1px 1px 1px #fff;
	/*border-width: 1px 0;
	border-style: solid;
	border-color: #b1b1b1;*/
}
.navigation .nav-pills li:first-child a {
	margin-left: 0;
}
.navigation .nav-pills li.active a,
.navigation .nav-pills li a:hover {
	border-bottom-color: #666;
	background: url(/images/new-images/opac12.png);
	text-shadow: -1px -1px 0 #999;
	color: #fff;
}
.navigation .nav-pills li.active a {
	font-size: 15px;
	position: relative;
}
.navigation .nav-pills li.active a:before,
.navigation .nav-pills li.active a:after,
.navigation .nav-pills li:hover a:before,
.navigation .nav-pills li:hover a:after {
	position: absolute;
	width: 100%;
	height: 0;
	content: " ";
	left: 0;
}
.navigation .nav-pills li:hover a:before,
.navigation .nav-pills li:hover a:after {
	width: 99%;
	left: 1px;
}
.navigation .nav-pills li.active a:before,
.navigation .nav-pills li:hover a:before {
	border-bottom: 1px solid #626263;
}
.navigation .nav-pills li:hover a:before {
	bottom: -1px;
}
.navigation .nav-pills li.active a:after,
.navigation .nav-pills li:hover a:after {
	border-top: 1px solid #737475;
}
.navigation .nav-pills li:hover a:after {
	top: -1px;
}
.navigation .nav-pills li.active a:before {
	bottom: 0;
}
.navigation .nav-pills li.active a:after {
	top: 0;
}
.navigation .nav-pills li img {
	position: absolute;
	display: none;
	bottom: -8px;
}
.navigation .nav-pills li.active img {
	display: block;
}
.navigation span.pull-right, .bottomContacts .pull-left {
	color: #666;
	font-size: 13px;
	font-weight: bold;
	margin-top: 10px;
	margin-right: 20px;
	text-shadow: 1px 0 0 #fff;
}
.navigation span.phone, .bottomContacts .phone {
	color: #000;
}
.bottomContacts .phone {
	background: url(/images/new-images/phone-icon.png) no-repeat 0 0;
	padding-bottom: 3px;
	padding-left: 33px;
}
.mobileNavigation .toggleMenu {
	cursor: pointer;
	font-size: 18px;
	height: 22px;
	margin: 5px 0 0 10px;
	width: 32px;
}
.mobileNavigation .toggleMenu i {
	position: static !important;
}
.mobileNavigation ul {
	display: none;
}
.mobileNavigation li a {
	color: #333;
	display: block;
	font-weight: bold;
	padding: 5px 0;
	text-decoration: none;
	text-align: center;
}
.mobileNavigation li a:hover,
.mobileNavigation li.active {
	background: url(/images/new-images/opac30.png);
	color: #fff;
}

.mobileNavigation li.active a {
	font-size: 15px;
	color:#fff;
}

/*** HEADER STYLE ENDS ***/

/*** BANNER STYLE STARTS ***/

.bannerContainer {
	background-color: #fff;
	box-shadow: 0 0 30px #e1e9f0, 0 1px 0 #fff;
	-moz-box-shadow: 0 0 30px #e1e9f0, 0 1px 0 #fff;
	-webkit-box-shadow: 0 0 30px #e1e9f0, 0 1px 0 #fff;
	margin-bottom: 30px;
	position: relative;
	text-align: center;
	behavior: url(/css/PIE.htc);
}
.bannerContainer > a {
	display: block;
	position: relative;
	text-decoration: none;
}
.bannerContainer img {
	position: relative;
	z-index: 1;
}
.bannerContainer span.bannerInfo {
	left: 0;
	position: absolute;
	padding-top: 43px;
	top: 0;
	width: 100%;
	z-index: 2;
	letter-spacing: -.4px;
}
.bannerContainer span.bannerInfo > span {
	color: #fff;
}
.bannerContainer .bannerTitle,
.bannerContainer .bannerSubTitle {
	display: block;
	line-height: 1.4em;
	text-shadow: 1px 1px 1px #000;
}
.bannerContainer .bannerTitle {
	font-size: 34px;
	margin: 0 auto 45px;
	width: 925px;
}
.bannerContainer .bannerInfo .bannerSubTitle {
	font-size: 24px;
	margin-bottom: 25px;
	color: #000;
	text-shadow: none;
}
.bannerContainer .playButton {
	background: #ff9933;
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#ff9933), to(#e67300));
	background: -webkit-linear-gradient(#ff9933, #e67300);
	background: -moz-linear-gradient(#ff9933, #e67300);
	background: -ms-linear-gradient(#ff9933, #e67300);
	background: -o-linear-gradient(#ff9933, #e67300);
	background: linear-gradient(#ff9933, #e67300);
	-pie-background: linear-gradient(#ff9933, #e67300);
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	display: inline-block;
	padding: 21px 47px 21px 48px;
	position: relative;
	behavior: url(/css/PIE.htc);
}
.bannerContainer .playButton span{
	background: url(/images/play-button-arrow.png) center right no-repeat;
	font-size: 24px;
	padding-right: 30px;
	font-style: italic;
	text-shadow: -1px -1px 0 #666;
}
.bannerContainer.howBlastingWorks .bannerTitle {
	font-size: 49px;
	margin-bottom: 5px;
}
.bannerContainer.howBlastingWorks .bannerSubTitle {
	display: block;
	font-size: 22px;
	text-shadow: 1px 1px 1px #000;
	color: white;
	width: 600px;
	margin: 0 auto;
}
.bannerBottom {
	bottom: 0;
	left: 0;
	position: absolute;
	width: 100%;
	z-index: 1;
	color: #00331b;
	text-shadow: 1px 1px 1px #0bb364;
}
.bannerBottom h3 {
	font-size: 20px;
	line-height: 34px;
}
.bannerBottom p {
	font-size: 17px;
	margin-bottom: 15px;
}

.bannerBottomBlack {
	bottom: 0;
	left: 0;
	position: absolute;
	width: 100%;
	z-index: 1;
	color: #00331b;
}
.bannerBottomBlack h3 {
	font-size: 24px;
	font-weight: normal;
	line-height: 28px;
	margin-bottom: 10px;
}
.bannerBottomBlack p {
	font-size: 17px;
	margin-bottom: 35px;
}

/*** BANNER STYLE ENDS ***/

/*** CONTENT STYLE STARTS ***/
@font-face {
	font-family: 'mayfield';
	src: url('../fonts/Mayfield.eot');
	src: local('☺'), url('../fonts/Mayfield.woff') format('woff'), url('../fonts/Mayfield.ttf') format('truetype'), url('../fonts/Mayfield.svg') format('svg');
	font-weight: normal;
	font-style: normal;
}
.contentBox h2 {
	text-align: center;
	font-size: 41px;
	padding-bottom: 40px;
}
.contentBox p {
	font-size: 18px;
}
.whatIsCBHolder p {
	letter-spacing: -.4px;
	text-align: center;
}
.fbEmbedPost {
	margin: 30px auto;
	position: relative;
	min-height: 405px;
	max-width: 440px !important;
	width: 100% !important;
}


.fbPostBefore, .fbPostAfter {
	position: absolute;
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAVCAMAAAAU5szlAAAAS1BMVEUAAAAzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzNOfcgEAAAAGHRSTlMAc4DJTyX01pkFsmYQFh5Do7vgjOkqOVX+XZ7dAAAAs0lEQVQoz4WR3Q6DIAxGC/KPIODUvv+TrmW7WGLszkUDyUn5WuCHPLo6QWLFSdgEJ+GHl65PSkw41IJMs0/OVgCsdiwlOdXWWVquKElHYMnph/cuM+dryCyZz7vKKoTmv1AScmJ2yPCleLyhwYw5PZcKAZ0nAqEVlcU3UzsHUYUyGUsd77GN407CODFwSiv9FK83g4CliOMQ98qrXyXjJCOAhCWjR1FJc5kiO+IOf6gF7rwBTlwOUh6TEh8AAAAASUVORK5CYII=) no-repeat 100% 23px;
}
.fbPostBefore, .content .fbPostAfter p {
	font: normal 24px/29px 'mayfield';
	color: #666;
}
.fbPostBefore {
	left: -178px;
	padding-right: 35px;
	top: 171px;
	width: 133px;
}
.fbPostAfter {
	right: -177px;
	top: 65px;
	width: 113px;
	padding-left: 57px;
	background-position: 0 24px;
}
.fbPostAfter .last {
	margin-left: -25px;
	text-align: center;
	width: 153px;
	padding-top: 93px;
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAAA+CAMAAABp7XfMAAAANlBMVEUAAAAzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzN4uB2OAAAAEXRSTlMAJLx1Mwpb1JhnpUvnFYM892Hui3gAAAHMSURBVFjDzZfpit0wDEblJZblJfb3/i9bq2Gae2Gm3BQZen6HgyIZLbSBkTrZ0xmYh703YDHsvad6nb13qJfsiUtbyJ66J71UAE/2pC1VO/Q1VHOrtKUVY2ktWLRqqIxSGhRfTXRVzsyMi1mkP5e4uEgiEvKC1XbD5RT3uSmpxKviBxr7HKqjT+mpfCebvMiLU6RGRw+Jl5PLZZAUF4dFA2WpjqxpSLQDD6EduIlw0AYcY+4xy8TMkTaQGGihkz0xax9JG/JxJK8zcEc+emgAy44iDm3X5w7zEeY1XbaY26AN6KANtIMxcdIOHDY1OkF7lrtPv3uy0AWGwj5L7YYdH28sezz+Em994tUwXQ2ZcdHKmdz3G35/sq7ePxdT9veuEWqkG1fw6KGVtwd/hV74Xhl8VnwDkOkJGSj3/73ZJ25ajv+wYofjx7VMFjV2eo7zQEu0gaCFGq+3aLQRa7Xhv8yn4U0TGX9GjgCIlrN9kZ0GbHt/VP46EgbblnJ4KD71pJO42pnd2aA0jwVLtx3tNz45uzFZz/eDJ9stVD1lbi9q2/kuDMX0So/hpRsnK2meXw0yRctb4Dc+ONO0QmHzhX1oqJHM6doz/0d+AfZ4IZve4T0vAAAAAElFTkSuQmCC) no-repeat 0 25px;
}
.whatIsCBHolder .secondLine {
	line-height: 24px;
}
.bold {
	font-weight: bold;
}

.contentBox.whyCB h2 {
	margin-bottom: 5px;
	padding-bottom: 0;
}

.contentBox.whyCB h3 {
	font-size: 17px;
	text-align: center;
	margin-top: 0;
	line-height: 1.2em;
}

.contentBox .case {
	float: left;
	width: 230px;
	margin-right: 40px;
	text-align: center;
	color: #050505;
}

.contentBox.case.last {
	margin-right: 0;
}

.contentBox .case img {
	margin: 20px 0;
}

.contentBox .case h3 {
	font-size: 17px;
	line-height: 1.3em;
	margin-bottom: 10px;
}

.contentBox .case p {
	font-size: 15px;
	line-height: 1.3em;
}

.contentBox .arrow-step {
	float: left;
	width: 220px;
	padding-right: 60px;
	margin-right: 10px;
	text-align: center;
	color: #050505;
	background: url(../images/left-to-right-arrow.png) no-repeat right 60px;
}

.contentBox .arrow-step.last {
	padding-right: 0;
	margin-right: 0;
	background: transparent;
}

.contentBox .arrow-step img {
	margin: 20px 0;
}

.contentBox .arrow-step h3 {
	font-size: 17px;
	line-height: 1.3em;
	margin-bottom: 10px;
}

.contentBox .arrow-step p {
	font-size: 15px;
	line-height: 1.3em;
	color: #666;
}

.contentBox.whyCB .features {
	margin: 60px 0 0 300px;
}

.contentBox.whyCB .feature {
	font-family: myriad-pro,Helvetica,Arial,Verdana,sans-serif;
	border-bottom: 1px solid #eeeeee;
	padding-bottom: 20px;
	padding-left: 130px;
	margin-bottom: 20px;
	margin-top: 20px;
}

.contentBox.whyCB .feature.last {
	border-bottom: 0;
}

.contentBox.whyCB .search {
	background: url(../images/feature-search.png) no-repeat left top;
}

.contentBox.whyCB .people {
	background: url(../images/feature-people.png) no-repeat left top;
}

.contentBox.whyCB .clock {
	background: url(../images/feature-clock.png) no-repeat left top;
}

.contentBox.whyCB .feature h3 {
	font-size: 18px;
	color: #333;
	text-align: left;
	line-height: 1.3em;
}

.contentBox.whyCB .feature p {
	font-size: 13px;
	color: #666;
	text-align: left;
	line-height: 1.3em;
	margin-top: 10px;
}

.whoUsesCB h3, .whoUsesCB .links {
	text-align: center;
}
.whoUsesCB h3 {
	font-size: 23px;
	letter-spacing: -1px;
}
.whoUsesCB .links, .whoUsesCB .links a {
	color: #666;
	font-size: 14px;
}
.whoUsesCB .links a {
	text-decoration: underline;
}
.whoUsesCB .links a:hover {
	text-decoration: none;
}
.whoUsesCB .mediaObject {
	margin: 17px 0 30px;
}

.testimonialsHolder {
	border-top: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	padding: 18px 0 23px;
	margin-bottom: 18px;
}
.testimonialsHolder p {
	font-size: 14px;
}
.testimonialsHolder .colLeft, .testimonialsHolder .colRight {
	float: left;
	width: 395px;
	color: #666;
	margin-top: 40px;
}
.testimonialsHolder .colRight {
	margin-left: 19px;
}
.testimonialsHolder .thumbHolder {
	max-width: 100px;
}
.testimonialsHolder .thumbHolder, .testimonialsHolder .textHolder {
	display: inline-block;
	vertical-align: top;
}
.testimonialsHolder .item {
	margin-top: 32px;
}
.testimonialsHolder .item:first-child {
	margin-top: 0;
}
.testimonialsHolder .thumbHolder img {
	border: 4px solid #fff;
}
.testimonialsHolder .thumbHolder span {
	display: inline-block;
	border: 10px solid #ccc;
	border-radius: 8px;
}
.testimonialsHolder .textHolder {
	margin-left: 17px;
	margin-top: -3px;
	width: 274px;
}
.testimonialsHolder .item i {
	font-weight: bold;
}

.partnersList {
	margin-top: 35px;
	text-align: center;
}
.partnersList li {
	display: inline-block;
	max-width: 190px;
	margin-left: 11px;
	opacity: .6;
}
.partnersList li:first-child {
	margin-left: 0;
}
.partnersList li:hover {
	opacity: 1;
}

.content .contentBox.socialMedia {
	padding: 30px 52px 40px;
	text-align: center;
}
.socialMedia .icons-holder {
	margin-left: 54px;
}
.socialMedia .icons-holder div {
	float: left;
	width: 260px;
	font-weight: bold;
	color: #666;
	margin-left: 10px;
}
.socialMedia .icons-holder div:first-child {
	margin-left: 0;
}
.contentBox.socialMedia .description {
	line-height: 24px;
	margin: 22px 12px 30px;
	padding: 26px 0;
	border: 1px solid #ccc;
	border-left: none;
	border-right: none;
}
.uiButtonNew.small {
	font-size: 21px;
	font-weight: normal;
	letter-spacing: -1px;
	padding: 20px 10px;
	width: auto;
}
.uiButtonNew.home {
	font-size: 21px;
	font-weight: normal;
	letter-spacing: -1px;
	padding: 20px 26px;
	width: auto;
}

.contentBox.contentType {
	text-align: center;
}
.contentBox.contentType .description {
	line-height: 24px;
}
.contentBox.contentType .description a {
	text-decoration: underline;
}
.contentBox.contentType .fbPostsHolder {
	border-top: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	margin: 30px 0;
	padding: 30px 0;
	min-height: 800px;
}
.fbPostsHolder h3 {
	margin-bottom: 18px;
}
.contentBox.contentType .fbPosts {
	margin-left: -10px;
	margin-bottom: 17px;
}
.contentBox.contentType .fbEmbedPost {
	width: 100% !important;
	max-width: 395px !important;
	float: left;
	margin: 10px;
}
.contentBox .fbPostsAfterText {
	font: normal 24px 'mayfield';
	color: #666;
	padding: 0 120px;
	position: relative;
}

.arrowsAbove {
	position: relative;
}
.arrowsAbove:before, .arrowsAbove:after{
	position: absolute;
	content: " ";
	width: 42px;
	height: 36px;
	top: 0;
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAAkCAMAAAAn1TMnAAAARVBMVEUAAAAzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzP1DjgGAAAAFnRSTlMAoUCVD2+xBR++Xy3zT4I1eNzuFtLlc2pDjQAAAS5JREFUOMuVlOmSgyAQhJEb4hE1fu//qOsVN7UCS/qHR1VX0zPTg7igRS0eYCqprgUqlT1ArBSegPCFBx5VXAnAy9VwG3b0NRagmtwdFgDr/+NasPDoAZpupfshR9VgI3RCtgCUmm1AHkPWPSeaDPfF5BzY7XtWbalKD0qIwHT+D0YpNae5T5Drs2puI7g9lc9SaN1+5JmahbE02FauL3UWYmAoDWtV8u9mzmDzsptDNwHHYFW+ugCL22XD1buQD0HcIza989JDl/XgNttGX9FcaHVa937ikCtv4ZlKkUmnYEgW0SVT4BLbFEDeg5iOqB7vOY8gRQrz+NezbnnlNipA8B+2bE52hWuAq+tzYHWbhzkWenBeKmB0xes48IugRRmy4cASK245HY1S8aOqHzBtF1vvHHP9AAAAAElFTkSuQmCC) no-repeat 0 0;
	-moz-transform: scaleY(-1);
	-o-transform: scaleY(-1);
	-webkit-transform: scaleY(-1);
	transform: scaleY(-1);
	filter: FlipV;
	-ms-filter: "FlipV";
}


.arrowsAbove:after {
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAkCAMAAADIF1gZAAAASFBMVEUAAAAzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzNmfWtGAAAAF3RSTlMAm8BfDxqoBfFwfLctP4s1Sdwi0VPj57F1+RYAAAEuSURBVDjLjZQJroMwDETJVidA2Onc/6bfgQh9lRgyqqpafWB77KR5lW1qFYC+jrQAPFWhBqylCtVM+iqyR20BtCIpVqADDql3skXWWwVLx9CMpE1iRjbHbR+w2h7o+CNbmeVjMwGGHxKmq5DV2nOujIentv3gUsBvJPJYy6gblApjDjz0kWepmVbbNDF9s6h/drZnBNg5iB4fGZ2x56UZUwBMosNAyJ0OnEKace7fZau5MfoCndzUcJpybq2BWGwHuOsXuD0RnbJHLOeB1J5QgPX40hWE7fBul9KPd0dcgQyFJaHi4kzFFhTWEqnpjkbA3Nd7Lq7zCm9/65ydZGD3L5vTnN3Ks9buchCAEo8zzenvuNB4HtTQyLKc85J+vlDJ7Bn8VNxQ0fDxM/J9/gdiIhgdnoVi2QAAAABJRU5ErkJggg==) no-repeat 0 0;
	right: 71px;
}

.arrowsAside:before, .arrowsAside:after {
	position: absolute;
	content: " ";
	width: 42px;
	height: 36px;
	top: 0;
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAAkCAMAAAAn1TMnAAAARVBMVEUAAAAzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzP1DjgGAAAAFnRSTlMAoUCVD2+xBR++Xy3zT4I1eNzuFtLlc2pDjQAAAS5JREFUOMuVlOmSgyAQhJEb4hE1fu//qOsVN7UCS/qHR1VX0zPTg7igRS0eYCqprgUqlT1ArBSegPCFBx5VXAnAy9VwG3b0NRagmtwdFgDr/+NasPDoAZpupfshR9VgI3RCtgCUmm1AHkPWPSeaDPfF5BzY7XtWbalKD0qIwHT+D0YpNae5T5Drs2puI7g9lc9SaN1+5JmahbE02FauL3UWYmAoDWtV8u9mzmDzsptDNwHHYFW+ugCL22XD1buQD0HcIza989JDl/XgNttGX9FcaHVa937ikCtv4ZlKkUmnYEgW0SVT4BLbFEDeg5iOqB7vOY8gRQrz+NezbnnlNipA8B+2bE52hWuAq+tzYHWbhzkWenBeKmB0xes48IugRRmy4cASK245HY1S8aOqHzBtF1vvHHP9AAAAAElFTkSuQmCC) no-repeat 0 0;		
}
.fbPostsAfterText:before {
	left: 71px;
}
.arrowsAside:after {
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAkCAMAAADIF1gZAAAASFBMVEUAAAAzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzNmfWtGAAAAF3RSTlMAm8BfDxqoBfFwfLctP4s1Sdwi0VPj57F1+RYAAAEuSURBVDjLjZQJroMwDETJVidA2Onc/6bfgQh9lRgyqqpafWB77KR5lW1qFYC+jrQAPFWhBqylCtVM+iqyR20BtCIpVqADDql3skXWWwVLx9CMpE1iRjbHbR+w2h7o+CNbmeVjMwGGHxKmq5DV2nOujIentv3gUsBvJPJYy6gblApjDjz0kWepmVbbNDF9s6h/drZnBNg5iB4fGZ2x56UZUwBMosNAyJ0OnEKace7fZau5MfoCndzUcJpybq2BWGwHuOsXuD0RnbJHLOeB1J5QgPX40hWE7fBul9KPd0dcgQyFJaHi4kzFFhTWEqnpjkbA3Nd7Lq7zCm9/65ydZGD3L5vTnN3Ks9buchCAEo8zzenvuNB4HtTQyLKc85J+vlDJ7Bn8VNxQ0fDxM/J9/gdiIhgdnoVi2QAAAABJRU5ErkJggg==) no-repeat 0 0;
	right: 71px;
}
.contentBox.contentType .uiButtonNew {
	margin: 48px 0 0;
	padding: 20px 26px;
}
.frequencyList {
	margin: 20px 0 27px;
	text-align: center;
}
.frequencyList li {
	display: inline-block;
	margin: 15px 0 0 3px;
	width: 103px;
	border: 4px solid #ccc;
	cursor: pointer;
	color: #b2b2b2;
}
.frequencyList li:first-child {
	margin-left: 0;
}
.frequencyList div {
	background: #f0f0f0;
	border: 3px solid #fff;
}
.frequencyList .day {
	margin: 0 0 4px;
}
.frequencyList .number {
	color: #666;
	font-size: 24px;
	margin: 0 0 6px;
}

.content .contentBox.freeTrial {
	text-align: center;
	padding: 30px 60px 40px;
	line-height: 24px;
}
.freeTrial .icons-holder {
	margin: 30px 0;
	background: url(../images/steps-bg.png) no-repeat center 40px;
	padding-bottom: 27px;
	border-bottom: 1px solid #ccc;
}
.freeTrial .icons-holder div {
	float: left;
	margin-left: 70px;
	width: 175px;
}
.freeTrial .icons-holder .item-2 {
	margin-left: 102px;
	width: 186px;
}
.freeTrial .icons-holder .item-3 {
	margin-left: 80px;
	width: 221px;
}
.freeTrial .icons-holder p {
	font-weight: bold;
	color: #666;
}
.freeTrial .icons-holder .step {
	color: #333;
	font-size: 23px;
	margin: 26px 0 0;
}
.contentBox.freeTrial .description {
	font-weight: normal;
	margin: 12px 0 0;
}

.contentBox.questions {
	text-align: center;
}
.questions .teamHolder {
	margin: 30px 0 45px;
}
.content .teamAfterText {
	font: normal 24px/26px 'mayfield';
	color: #666;
	padding: 0 230px;
	position: relative;
}
.content .teamAfterText:before {
	left: 171px;
}
.content .teamAfterText:after {
	right: 171px;
}

#we-are-hiring-tab {
	position: absolute;
	margin: 30px 0 0 -56px;
	display: block;
	width: 56px;
	height: 210px;
	background-image: url(../images/we-are-hiring.png);
	background-repeat: no-repeat;
}
.content {
	margin-top: 30px;
}
.content span.bold {
	font-weight: bold;
}
.content .contentBox {
	background: url(/images/new-images/opac80-white.png);
	padding: 30px 90px 40px;
	margin-bottom: 30px;
	position: relative;
	z-index: 1;
}
.content .questionContainer {
	padding-bottom: 110px;
}
.listingBox {
	background: url(/images/new-images/opac80-white.png);
	padding: 30px;
	margin-bottom: 45px;
	position: relative;
	z-index: 1;
}
.sliderContainer {
	display: none;
}
.content .contentBox .contentBoxTitle {
	border-bottom: 1px solid #e1e1e1;
	margin-bottom: 35px;
	padding-top: 30px;
	padding-bottom: 42px;
	text-align: center;
	clear: both;
}
.content .contentBox .contentBoxTitle:first-child {
	padding-top: 0;
}
.content .contentBox .contentBoxTitle h1 {
	font-weight: normal;
	line-height: 1.3em;
	margin: 0 -30px;
}
.stepBox {
	position: relative;
}
.stepBox .opacLayer {
	background-color: #fff;
	display: none;
	height: 100%;
	left: 0;
	opacity: .5;
	position: absolute;
	top: 0;
	width: 100%;
	z-index: 999;
}
.stepBox.opac .opacLayer {
	display: block;
}
.boxBottomArrow {
	position: absolute;
	bottom: -31px;
	left: 830px;
	display: none;
}
.pulsatingArrow {
	bottom: -22px;
	display: none;
	left: 833px;
	position: absolute;
}
.stepBox.complete .pulsatingArrow {
	display: inline-block;
}
.stepBox .stepTitle {
	font-size: 48px;
	font-weight: bold;
}
.stepBox .stepTitle .titleNumber {
	float: left;
	position: relative;
	margin-right: 25px;
}
.stepBox .stepTitle .titleNumber span {
	font-size: 48px;
	font-weight: bold;
	position: absolute;
	line-height: 1em;
	top: 26px;
	left: 36px;
}
.stepBox .stepTitle .titleNumber i {
	font-size: 48px;
	font-weight: bold;
	position: absolute;
	line-height: 1em;
	top: 26px;
	left: 26px;
}
.stepBox .stepTitle h2 {
	padding-bottom: 25px;
	padding-top: 25px;
	color: #333;
}
.stepBox > p {
	margin-left: 130px;
}
.stepBox .stepContent {
	color: #333;
}
.stepContent .wantUsToPostWrapper {
	/* overflow: hidden; */
}
.fb_iframe_widget,
.fb_iframe_widget span[style],
.fb_iframe_widget iframe[style] {
	width: 100% !important;
}
.postNumberContainer {
	width: 360px;
}
.postNumberWrapper {
	/*
	border-bottom: 1px solid #e1e1e1;
	margin-bottom: 30px;
	padding-bottom: 30px;
	*/
}
.postNumberContainer .postNumbers {
	margin-top: 20px;
}
.postNumberContainer .postNumbers li {
	background: url(/images/new-images/post-number-line.png) center bottom no-repeat;
	float: left;
	font-weight: bold;
	margin-left: 50.5px;
	padding-bottom: 10px;
}
.postNumberContainer .postNumbers li:first-child {
	margin-left: 0;
}
.postNumberContainer .postNumberSliderWrapper {
	border: 1px solid #ddd;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	height: 6px;
	margin: 3px 2px 0 6px;
	padding: 1px 0;
	position: relative;
}
.postNumberContainer .postNumberSlider {
	border: none;
	height: 6px;
	margin: 0 1px;
}
.wantUsToPostWrapper .selectCity .selectWrapper {
	margin-top: 10px;
	width: 100%;
	height: 30px;
}
.wantUsToPostWrapper .selectCity .selectWrapper select {
	height: 30px;
	line-height: 30px;
}
.stepContent .noteContainer {
	background: url(/images/new-images/opac7.png);
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	box-shadow: inset 2px 2px 8px -3px #000;
	-moz-box-shadow: inset 2px 2px 8px -3px #000;
	-webkit-box-shadow: inset 2px 2px 8px -3px #000;
	margin-top: 25px;
	padding: 10px;
	position: relative;
	width: 271px;
}
.stepContent .noteContainer p {
	margin-bottom: 0;
}
.step2 .stepContent .noteContainer {
	box-shadow: inset 0 2px 8px -3px #000;
	-moz-box-shadow: inset 0 2px 8px -3px #000;
	-webkit-box-shadow: inset 0 2px 8px -3px #000;
	width: 280px;
	margin: 3px -20px 0 30px;
	position: relative;
	font-weight: normal;
}
.step3 .stepContent .noteContainer {
	margin-left: 130px;
}
.step4 .stepContent .noteContainer > img {
	position: absolute;
	top: 15px;
	right: -10px;
}
.step2 .stepContent .noteContainer > img {
	position: absolute;
	bottom: 15px;
	left: -10px;
}
.step1 .stepContent .ladyCartoon {
	margin-left: 15px;
	margin-right: 22px;
}
.step2 .stepContent .ladyCartoon {
	margin-left: 15px;
	margin-right: 22px;
}
.step3 .stepContent .ladyCartoon {
	margin-left: 15px;
	margin-right: 10px;
}
.stepContent .settings {
	margin-left: 0;
}
ul.settings p {
	font-size: 13px;
}
.stepContent .settings > li {
	width: 210px;
	margin-left: 30px;
}
.stepContent .settings > li:first-child {
	margin-left: 0;
}
.stepContent .settings .unstyled {
	margin-top: 10px;
}
.stepContent .settings .unstyled li {
	border-top: 1px solid #b9b9b9;
	float: none;
	margin-left: 0;
	padding: 10px 0 9px;
	width: 100%;
}
.stepContent .settings .unstyled li:first-child {
	border-top: 0;
}
.stepContent .settings .unstyled li input {
	margin: 4px 2px 0 -2px;
	vertical-align: top;
}
.stepContent .settings .unstyled li label {
	background: url(/images/new-images/unselected.png) 2px top no-repeat #fafbfc;
	color: #333;
	display: inline-block;
	font-size: 12px;
	margin-bottom: 0;
	margin-left: -18px;
	padding-left: 25px;
	vertical-align: top;
	zoom: 1;
	width: 80%;
}
.stepContent .settings .unstyled li.selected label {
	background: url(/images/new-images/selected.png) 2px top no-repeat #fafbfc;
}
.stepContent .settings .samplecontent {
	background: url(/images/new-images/search-icon.png) no-repeat;
	float: right;
	height: 14px;
	margin-right: 5px;
	margin-top: 3px;
	position: relative;
}
.stepContent .settings .samplecontent span {
	background: url(/images/new-images/tooltip-bg.png) no-repeat;
	color: #fff;
	display: none;
	font-weight: 600;
	left: -70px;
	padding: 12px 0 32px;
	position: absolute;
	text-align: center;
	text-transform: uppercase;
	top: -64px;
	width: 155px;
}
.stepContent .settings .samplecontent:hover {
	background: url(/images/new-images/search-icon-on.png) no-repeat;
}
.stepContent .settings .samplecontent:hover span {
	display: block;
}
.stepContent .btnContainer {
	margin-top: 16px;
}
.stepContent .btnContainer h4 {
	margin-bottom: 15px;
	letter-spacing: -1px;
}
.banner {
	background: url(/images/new-images/banner-bg.png) no-repeat;
	background-size: 100% 100%;
	color: #fff;
}
.banner h2 {
	line-height: 50px;
	margin: 30px 0;
	padding-right: 10px;
}
.banner h5 {
	font-weight: normal;
}
.banner h6 {
	font-size: 12px;
	font-weight: normal;
}
.banner .agent {
	position: relative;
}
.banner .info {
	position: absolute;
	top: 148px;
	left: 250px;
}
.banner .info h5 {
	font-weight: bold;
	line-height: 1.2em;
}
.banner .info .other h5 {
	font-size: 13px;
}
.banner .info h6 {
	color: #e5e5e5;
	line-height: 1.2em;
	margin-bottom: 10px;
}
.banner .tryDemo {
	margin-left: 18px;
}
.strategyContainer {
	border-bottom: 1px solid #e5e5e5;
	margin-bottom: 45px;
	padding-bottom: 30px;
	position: relative;
}
.strategyContainer .bottomArrow {
	bottom: -13px;
	left: 38px;
	position: absolute;
}
.strategyContainer .media {
	margin-top: 0;
}
.strategyContainer .media .pull-left {
	margin-right: 30px;
}
.strategyContainer .media .media-body {
	color: #666;
	font-size: 18px;
	font-weight: normal;
}
.strategyContainer .media .media-body h3 {
	color: #333;
	font-weight: normal;
}
.tryDemoTxt h3 {
	color: #000;
	font-size: 26px;
}
.tryDemoTxt h4 {
	font-weight: normal;
}
.planContainer,
.mobilePlanContianer {
	color: #797979;
}
.mobilePlanContainer {
	display: none;
}
.planContainer > li {
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	box-shadow: 1px 1px 9px -1px #666;
	-moz-box-shadow: 1px 1px 9px -1px #666;
	-webkit-box-shadow: 1px 1px 9px -1px #666;
	margin-left: 20px;
	margin-top: 15px;
	padding: 1px;
	position: relative;
	width: 228px;
	behavior: url(/css/PIE.htc);
}
.mobilePlanContainer > li {
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	-webkit-box-shadow: 1px 1px 9px -1px #000;
	-moz-box-shadow: 1px 1px 9px -1px #000;
	box-shadow: 1px 1px 9px -1px #000;
	color: #797979;
	margin: 0 auto 20px;
	padding: 1px;
	position: relative;
	width: 476px;
	behavior: url(/css/PIE.htc);
}
.mobilePlanContainer > li > div {
	border-radius: 5px 0 0 5px;
	-moz-border-radius: 5px 0 0 5px;
	-webkit-border-radius: 5px 0 0 5px;
	margin-right: -15px;
	width: 253px;
}
.mobilePlanContainer > li > .right {
	background-color: #f6f6f6;
	border-radius: 0 5px 5px 0;
	-moz-border-radius: 0 5px 5px 0;
	-webkit-border-radius: 0 5px 5px 0;
	width: 238px;
}
.mobilePlanContainer > li.popular > .right {
	background-color: #f0fdea;
}
.planContainer li:first-child {
	margin-left: 0;
}
.planContainer > li.popular {
	margin-top: 0;
	width: 258px;
}
.planContainer .title {
	padding: 11px 0;
	text-align: center;
}
.mobilePlanContainer .title {
	text-align: center;
	text-shadow: 0 -2px 0 #3a3a3a;
	height: 48px;
	padding-top: 15px;
}
.planContainer .popular .title {
	color: #fff;
	height: 101px;
	position: relative;
	text-shadow: 0 -1px 0 #666;
	z-index: 1;
}
.mobilePlanContainer .popular .title {
	color: #fff;
	position: relative;
	text-shadow: 0 -1px 0 #666;
	z-index: 1;
}
.planContainer .popular .title img {
	left: 0;
	position: relative;
	top: -10px;
	z-index: -1;
}
.mobilePlanContainer .popular .title img {
	position: relative;
	z-index: -1;
	margin-top: -15px;
}
.planContainer .title h3,
.mobilePlanContainer .title h3 {
	font-weight: 600;
	text-transform: uppercase;
}
.mobilePlanContainer .title h3 {
	line-height: 1em;
}
.planContainer .title h6,
.mobilePlanContainer .title h6 {
	font-weight: 600;
}
.planContainer .popular .title h3 {
	font-size: 36px;
	margin-top: -105px;
}
.mobilePlanContainer .popular .title h3 {
	margin-top: -55px;
}
.planContainer .popular .title h6,
.mobilePlanContainer .popular .tile h6 {
	font-size: 18px;
}
.planContainer .priceContainer,
.mobilePlanContainer .priceContainer {
	background-color: #f6f6f6;
	font-weight: bold;
	padding: 20px 0 20px 35px;
	position: relative;
	text-transform: uppercase;
	text-shadow: 0 1px 0 #fff;
}
.mobilePlanContainer .priceContainer {
	background-color: transparent;
	padding-left: 20px;
}
.planContainer .popular .priceContainer {
	background-color: #f0fdea;
	margin-top: -20px;
	padding-left: 20px;
}
.planContainer .priceContainer span,
.mobilePlanContainer .priceContainer span {
	display: block;
}
.planContainer .priceContainer span.dollars,
.mobilePlanContainer .priceContainer span.dollars {
	display: inline;
}
.planContainer .priceContainer .price,
.mobilePlanContainer .priceContainer .price {
	font-size: 44px;
	line-height: 1em;
	text-shadow: 1px 4px 6px #DFDFDF, 0 0 0 #999;
	color: rgba(0,0,0,.3);
}
.mobilePlanContainer .priceContainer .price {
	color: rgba(0,0,0,.7);
	font-size: 54px;
	text-shadow: 1px 4px 6px #DFDFDF, 0 0 0 #404040;
}
.planContainer .popular .priceContainer .price {
	font-size: 58px;
	text-shadow: 1px 4px 6px #DFDFDF, 0 0 0 #404040;
	color: rgba(0,0,0,.5);
}
.mobilePlanContainer .priceContainer .price sup,
.mobilePlanContainer .priceContainer .price sub {
	color: rgba(0,0,0,.7);
	text-shadow: 1px 4px 6px #DFDFDF, 0 0 0 #404040;
}
.planContainer .priceContainer .price sup,
.mobilePlanContainer .priceContainer .price sup {
	font-size: 20px;
	position: static;
	vertical-align: super;
}
.mobilePlanContainer .priceContainer .price sup {
	font-size: 24px;
}
.mobilePlanContainer .priceContainer .price sup sup {
	font-size: 18px;
	text-shadow: none;
}
.planContainer .popular .priceContainer .price sup {
	font-size: 25px;
}
.planContainer .priceContainer .price sub,
.mobilePlanContainer .priceContainer .price sub {
	font-size: 12px;
	margin-left: -22px;
	position: static;
}
.mobilePlanContainer .priceContainer .price sub {
	margin-left: -33px;
}
.planContainer .popular .priceContainer .price sub {
	font-size: 14px;
	margin-left: -29px;
}
.planContainer .priceContainer .timePeriod,
.mobilePlanContainer .priceContainer .timePeriod {
	padding-left: 15px;
}
.planContainer .popular .priceContainer .timePeriod {
	padding-left: 20px;
}
.planContainer .priceContainer .join,
.mobilePlanContainer .priceContainer .join {
	background: url(/images/new-images/join-tag-bg-blue.png) no-repeat;
	color: #fff;
	font-size: 13px;
	padding: 6px 21px 14px 17px;
	position: absolute;
	right: -9px;
	text-shadow: 0 1px 0 #3a3a3a;
	top: 29%;
}
.planContainer .popular .priceContainer .join,
.mobilePlanContainer .popular .priceContainer .join {
	background: url(/images/new-images/join-tag-bg-green.png) no-repeat;
	font-size: 18px;
	padding: 10px 21px 19px 24px;
	right: -12px;
}
.planContainer .specification {
	padding: 0 35px;
}
.mobilePlanContainer .specification {
	margin-top: 8px;
	padding: 0 28px 0 42px;
}
.planContainer .specification li,
.mobilePlanContainer .specification li {
	border-top: 1px solid #f2f2f2;
	background: url(/images/new-images/specification-icons.png) no-repeat;
	padding: 5px 0 5px 25px;
}
.planContainer .specification li:first-child,
.mobilePlanContainer .specification li:first-child {
	border-top: none;
}
.planContainer .specification li.techSupport,
.mobilePlanContainer .specification li.techSupport {
	background-position: 1px 7px;
}
.planContainer .specification li.advOptions,
.mobilePlanContainer .specification li.advOptions {
	background-position: 1px -29px;
}
.planContainer .specification li.storage,
.mobilePlanContainer .specification li.storage {
	background-position: 1px -64px;
}
.planContainer .specification li.bandwidth,
.mobilePlanContainer .specification li.bandwidth {
	background-position: 1px -96px;
}
.planContainer .uiButtonNew {
	background-color: #16adfb;
	font-size: 22px;
	margin: 10px 0 20px 19px;
	padding: 18px 0;
	width: 190px;
}
.mobilePlanContainer .uiButtonNew {
	background-color: #16adfb;
	font-size: 24px;
	margin: 10px 0 20px 20px;
	padding: 13px 0;
	width: 200px;
}
.mobilePlanContainer .popular .uiButtonNew {
	background-color: #6fad51;
}
.planContainer .popular .uiButtonNew {
	background-color: #6fad51;
	font-size: 28px;
	padding: 18px 0;
	width: 220px;
}
.aboutPlanNote {
	color: #999;
	font-size: 12px;
	margin-top: 15px;
	text-align: center;
}
.quoteContainer {
	border-bottom: 1px solid #e1e1e1;
	margin-bottom: 30px;
	padding-bottom: 30px;
	text-align: center;
}
.aboutPlans .span6,
.quoteContainer .span6 {
	width: 47.617949%;
}
.aboutPlans .span4 {
	width: 30.123932%;
}
.aboutPlans [class*="span"],
.quoteContainer [class*="span"] {
	margin-left: 4.764103%;
}
.aboutPlans [class*="span"]:first-child,
.quoteContainer [class*="span"]:first-child {
	margin-left: 0;
}
.whoIsUsingCB h3,
.faqs h3 {
	font-size: 21px;
	font-weight: 600;
	line-height: 1em;
	margin-bottom: 30px;
}
.whoIsUsingCB .row-fluid {
	margin-bottom: 30px;
}
.securityNote {
	background: url(/images/new-images/opac7.png);
	box-shadow: inset 1px 1px 8px -2px #000;
	-moz-box-shadow: inset 1px 1px 8px -2px #000;
	-webkit-box-shadow: inset 1px 1px 8px -2px #000;
	border-radius: 5px;
	padding: 20px;
}
.securityNote img {
	margin: 20px 5px 30px 15px;
}
.faqs h5 {
	margin-bottom: 10px;
}
.listing {
	background: url(/images/new-images/header-bg.png) center center #e3e6e8;
	margin-bottom: 30px;
}
.listing .span8 {
	background: url(/images/new-images/opac80-white.png);
}
.listing .slider{
	background: url(/images/new-images/opac3.png);
}
.listing .propertyInfo {
	padding: 15px 20px;
}
.listing .propertyInfo .valueContainer {
	padding-bottom: 15px;
	border-bottom: 1px solid #e1e1e1;
}
.listing .propertyInfo .valueContainer .address {
	color: #666;
	font-size: 21px;
	font-weight: bold;
}
.listing .propertyInfo p {
	font-weight: 400;
	margin: 25px 0;
}
.listing .map {
	margin: 0 -20px 25px;
}
.listing .notUsingCBAgent{
	background: url(/images/new-images/opac3.png);
	padding: 30px;
	text-align: center;
}
.listing .span8 .agentDetail {
	display: none;
}
.listing .agentDetail,
.listing .propertyDetail,
.listing .suggestions {
	margin-bottom: 15px;
	text-shadow: 1px 1px 0 #fff;
}
.listing .agentDetail h3,
.listing .propertyDetail h3,
.listing .suggestions h3 {
	font-size: 21px;
	letter-spacing: -1px;
}
.listing .agentDetail .dl-horizontal dt {
	width: 68px;
	text-align: left;
}
.listing .agentDetail .dl-horizontal dd {
	margin-left: 80px;
}
.listing .agentDetail .uiButtonNew {
	font-size: 21px;
	padding: 13px 0;
	width: 205px;
}
.listing .propertyDetail .address {
	font-size: 16px;
	font-weight: 600;
	letter-spacing: -1px;
	word-spacing: 2px;
}
.listing .propertyDetail table {
	width: 94%;
}
.listing .propertyDetail td {
	border-bottom: 1px solid #bababa;
	font-weight: 600;
}
.listing .propertyDetail td.title {
	font-weight: bold;
	padding: 5px 30px 5px 0;
	width: 92px;
}
.listing .propertyDetail .note {
	color: #999;
	font-size: 9px;
	line-height: 1em;
	margin-top: 5px;
	padding-right: 18px;
	text-shadow: none;
}
.listing .propertyDetail .brokerInfo p {
	color: #808080;
	font-size: 11px;
	line-height: 1.3em;
	margin-bottom: 0;
	text-shadow: none;
}
.listing .suggestions {
	display: block;
}
.listing .suggestions span {
	color: #333;
	display: block;
	font-weight: 600;
	margin-bottom: 25px;
}
.thumbnail_img_container > div {
	margin: 0 75px 0 95px;
	padding: 17px 0 18px;
	position: relative;
}
.listing_pic {
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	border-radius: 8px;
	background-color: #f8f8f8;
	border: solid 1px #e5e5e5;
	padding: 4px;
	margin: 0 20px 0 0;
	float: left;
	position: relative;
	behavior: url(/css/PIE.htc);
}
.listing_pic a {
	display: block;
	height: 65px;
	width: 65px;
}
.listing_pic img {
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
	background-position: center center;
	position: relative;
	behavior: url(/css/PIE.htc);
}
.thumbnail_img_container span.prevSlide,
.thumbnail_img_container span.nextSlide {
	position: absolute;
	width: 56px;
	height: 46px;
	display: block;
	cursor: pointer;
	top: 37.5px;
}
.thumbnail_img_container span.nextSlide .bx-next,
.thumbnail_img_container span.prevSlide .bx-prev {
	display: block;
	height: 100%;
}
.thumbnail_img_container span.prevSlide {
	background: url(../images/arrow-left.png) no-repeat;
	left: -80px;
}
.thumbnail_img_container span.nextSlide {
	background: url(../images/arrow-right.png) no-repeat;
	right: -80px;
}
.socialShare {
	float: right;
	margin-top: -35px;
}
.socialShare .facebookShare,
.socialShare .twitter,
.socialShare .mail {
	float: left;
	opacity: .5;
	filter: alpha(opacity = 50);
	margin-right: 5px;
}
.mail {
	margin-right: 10px;
}
.facebookShare:hover,
.twitter:hover,
.mail:hover {
	opacity: 1;
	filter: alpha(opacity = 100);
}
.facebookShare a#button,
.twitter .tweet,
.social-mail {
	background: url(/images/facebook.png) no-repeat transparent;
	display: block;
	float: left;
	height: 23px;
	margin: 0;
	text-decoration: none;
	width: 23px;
}
.facebookShare .counter,
.twitter .count,
.mail .count {
	border-width: 1px 1px 1px 0;
	border-style: solid;
	border-color: #ccc;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	color: #333;
	cursor: default;
	font: 12px/16px "lucida grande",tahoma,verdana,arial,sans-serif;
	font-weight: bold;
	filter: alpha(opacity = 50);
	float: left;
	height: 17px;
	margin-left: 1px;
	max-height: 23px;
	max-width: 100px;
	min-width: 20px;
	margin-left: 4px;
	padding: 2px 4px 2px 5px;
	position: relative;
	text-align: center;
}
.facebookShare .counter img,
.twitter .count img,
.mail .count img {
	position: absolute;
	top: -1px;
	left: -4px;
}
.facebookShare:hover .counter #fbcount,
.twitter:hover .count .twittercount,
.mail:hover .count .mailcount {
	filter: alpha(opacity = 100);
}
.twitter .tweet {
	background-image: url(/images/twitter.png);
}
.mail .social-mail {
	background-image: url(/images/mail.png);
}
.fb-comments, .fb-comments span[style], .fb-comments iframe[style] {
	width: 100% !important;
}
.registration .paymentInfo {
	background-color: #d7d7d7;
	border-left: 1px solid #babcbd;
	border-right: 1px solid #babcbd;
	padding: 10px;
}
.registration .title {
	background: #636363;
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#636363), to(#3b3b3b));
	background: -webkit-linear-gradient(#636363, #3b3b3b);
	background: -moz-linear-gradient(#636363, #3b3b3b);
	background: -ms-linear-gradient(#636363, #3b3b3b);
	background: -o-linear-gradient(#636363, #3b3b3b);
	background: linear-gradient(#636363, #3b3b3b);
	-pie-background: linear-gradient(#636363, #3b3b3b);
	behavior: url(/pie/PIE.htc);
	color: #fff;
	text-shadow: 0 0 0 #000;
	font-size: 18px;
	font-weight: bold;
	padding: 13px 0 14px 15px;
	border-radius: 3px 3px 0 0;
}
.registration span.info {
	color: #969696;
	text-shadow: 0 2px 0 #eeebe9;
}
.registration .title span {
	height: 19px;
	width: 15px;
	display: inline-block;
	background: url(/images/sec-lock.png) no-repeat;
	margin-right: 10px;
}
.registration .selectWrapper,
.registration input[type="text"] {
	background-color: #eee;
}
.registration .selectWrapper select {
	min-height: 20px;
}
.registration .selectWrapper.active {
	color: #000;
	background-color: #FFFFD6;
}
.registration input.active {
	background-color: #fff;
}
.registration .paymentInfo label {
	text-transform: uppercase;
	color: #545454;
	text-shadow: 0 2px 0 #fff;
	font-size: 16px;
	font-weight: bold;
}
.registration .paymentInfo div.fieldContainer {
	margin-bottom: 30px !important;
	margin-top: 10px;
}
.registration .paymentInfo .codeContainer .securityCode input {
	font-size: 14px;
	width: 35px;
}
.registration {
	text-align: center;
	margin-bottom: 20px;
}
.discountOffer {
	border: 1px solid #a8aaab;
	border-radius: 3px;
	position: relative;
}
.discountOffer > .arrowImg {
	position: absolute;
	left: -15px;
	bottom: 80px;
}
.discountOffer .title {
	color: #626262;
	font-size: 20px;
	margin: 0;
	padding: 15px 0 15px 30px;
	background: #f7f7f7;
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#f7f7f7), to(#eaeaea));
	background: -webkit-linear-gradient(#f7f7f7, #eaeaea);
	background: -moz-linear-gradient(#f7f7f7, #eaeaea);
	background: -ms-linear-gradient(#f7f7f7, #eaeaea);
	background: -o-linear-gradient(#f7f7f7, #eaeaea);
	background: linear-gradient(#f7f7f7, #eaeaea);
	-pie-background: linear-gradient(#f7f7f7, #eaeaea);
	behavior: url(/pie/PIE.htc);
}
.discountOffer .title span {
	background: url(/images/comment-icon.png) no-repeat;
	height: 14px;
	width: 16px;
	display: inline-block;
	margin-right: 5px;
}
.discountOffer .content {
	padding: 20px 28px 30px;
	margin-top: 0;
}
.discountOffer .content p {
	margin-top: 0;
}
.discountOffer .content .specialPrice,
.discountOffer .content .code {
	background: #f9f9f9;
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#f9f9f9), to(#EEE));
	background: -webkit-linear-gradient(#f9f9f9, #EEE);
	background: -moz-linear-gradient(#f9f9f9, #EEE);
	background: -ms-linear-gradient(#f9f9f9, #EEE);
	background: -o-linear-gradient(#f9f9f9, #EEE);
	background: linear-gradient(#f9f9f9, #EEE);
	-pie-background: linear-gradient(#f9f9f9, #EEE);
	behavior: url(/pie/PIE.htc);
	text-align: center;
	border-radius: 3px;
	box-shadow: 0 1px 7px -2px #000;
}
.discountOffer .content .specialPrice {
	margin-bottom: 25px;
}
.discountOffer .content .specialPrice h5,
.discountOffer .content .code h5 {
	text-transform: uppercase;
	padding: 8px 0;
	border-bottom: 1px solid #e8e8e8;
	color: #626262;
	margin: 0;
}
.discountOffer .content .specialPrice div,
.discountOffer .content .code div {
	font-size: 36px;
	font-weight: bold;
	color: #d82008;
	padding: 12px 0;
	font-style: italic;
}
.discountOffer .content .code div {
	color: #626262;
	text-transform: uppercase;
	font-size: 28px;
}
.discountOffer .content .specialPrice sup {
	font-size: 16px;
	vertical-align: middle;
}
.discountOffer .content .specialPrice .period {
	font-size: 14px;
	color: #7f7f7f;
}
.contactToHelp {
	margin-top: 20px;
}
.contactToHelp p {
	margin: 0 0 10px;
	font-size: 18px;
	font-weight: bold;
	text-align: center;
	color: #5e5e5e;
	line-height: 18px;
}
.contactToHelp p.duration {
	font-size: 14px;
	line-height: 14px;
	font-weight: normal;
	color: #474747;
}
.questionWrapper,
.questionWrapper .question,
.questionWrapper .answer {
	border-radius: 7px;
	-webkit-border-radius: 7px;
	-ms-border-radius: 7px;
	-moz-border-radius: 7px;
	behavior: url(/css/PIE.htc);
}
.questionWrapper {
	margin-bottom: 8px;
}
.questionWrapper .question {
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#f8f8f8), to(#e9e9e9));
	background: -webkit-linear-gradient(#f8f8f8, #e9e9e9);
	background: -moz-linear-gradient(#f8f8f8, #e9e9e9);
	background: -ms-linear-gradient(#f8f8f8, #e9e9e9);
	background: -o-linear-gradient(#f8f8f8, #e9e9e9);
	background: linear-gradient(#f8f8f8, #e9e9e9);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f8f8f8', endColorstr='#e9e9e9',GradientType=0 );
	-pie-background: linear-gradient(#f8f8f8, #e9e9e9);
	border-top: 1px solid #f2f2f2;
	box-shadow: 0 1px 0 1px #cfcfcf;
	-webkit-box-shadow: 0 1px 0 1px #cfcfcf;
	-moz-box-shadow: 0 1px 0 1px #cfcfcf;
	-ms-box-shadow: 0 1px 0 1px #cfcfcf;
	padding: 16px 18px 17px 20px;
	position: relative;
	display: block;
	text-decoration: none;
	outline: none;
	margin: 0 1px;
	behavior: url(/css/PIE.htc);
}
.questionWrapper .question h5 {
	color: #333;
	max-width: 95%;
	font-weight: normal;
}
.questionWrapper .question img {
	background-image: url(/images/new-images/expand.png);
	width: 32px;
	position: absolute;
	right: 18px;
	top: 50%;
	margin-top: -16px;
}
.questionWrapper .answer {
	border-top-left-radius: 0;
	border-top-right-radius: 0;
	box-shadow: inset 4px 0 3px -3px #aaa,inset 0 0 5px -1px #aaa;
	-moz-box-shadow: inset 4px 0 3px -3px #aaa,inset 0 0 5px -1px #aaa;
	-webkit-box-shadow: inset 4px 0 3px -3px #aaa,inset 0 0 5px -1px #aaa;
	-ms-box-shadow: inset 4px 0 3px -3px #aaa,inset 0 0 5px -1px #aaa;
	color: #656565;
	font-weight: 600;
	display: none;
	padding: 20px 18px 15px 20px;
	margin-bottom: 0;
	margin-top: -5px;
}
.questionWrapper.expand .question img {
	background-image: url(/images/new-images/collapse.png);
}

.content .testimonialsContainer {
	padding-top: 34px !important;
	padding-bottom: 20px !important;
	position: relative;
}
.content .testimonialsContainer .testimonialsBottomArrow {
	position: absolute;
	bottom: -30px;
	right: 115px;
}
.content .testimonialsContainer .testimonialsDesc {
	position: relative;
}
.content .testimonialsDesc .openQutote,
.content .testimonialsDesc .closeQutote {
	display: inline-block;
	color: #e6e6b8;
	font-size: 100px;
	font-family: 'Helvetica Neue';
	font-weight: bold;
	position: absolute;
}
.content .testimonialsDesc p {
	color: #666;
	font-size: 18px;
	margin-bottom: 21px;
	line-height: 1.3;
	word-spacing: -.9px;
}
.content .testimonialsDesc .openQutote {
	left: -55px;
	top: 5px;
}
.content .testimonialsDesc .closeQutote {
	right: -60px;
	bottom: -30px;
}
.content .memberInfo {
	padding-left: 80px;
}
.content .memberInfo .memberImg {
	display: inline-block;
	padding: 2px;
	background: #fff;
	border: 1px solid #e6e6e6;
	border-radius: 4px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	-ms-border-radius: 4px;
	margin-right: 11px;
	behavior: url(/css/PIE.htc); 
}
.content .memberInfo .memberImg img {
	border-radius: 2px;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	-ms-border-radius: 2px;
	behavior: url(/css/PIE.htc);
}
.content .memberInfo .media {
	margin-right: 19px;
	min-width: 250px;
	max-width: 250px;
}
.content .memberInfo .media-heading {
	margin-top: 18px;
	margin-bottom: 2px;
	word-spacing: -2.5px;
}
.content .memberInfo .media-body span {
	color: #666;
	font-size: 13px;
	word-spacing: -1.5px;
}
.content .memberInfo .trialInfo {
	border-left: 1px solid #d2d2cf;
	margin-top: 10px;
	padding: 18px 0 17px 19px;
	max-width: 425px;
	word-spacing: -1px;
	overflow: hidden;
}
/*** CONTENT STYLE ENDS ***/

/*** OVERRIDEN SLIDER CSS STYLE STARTS ***/

.ui-widget-header {
	background: #19affe;
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#19affe), to(#0199e7));
	background: -webkit-linear-gradient(#19affe, #0199e7);
	background: -moz-linear-gradient(#19affe, #0199e7);
	background: -ms-linear-gradient(#19affe, #0199e7);
	background: -o-linear-gradient(#19affe, #0199e7);
	background: linear-gradient(#19affe, #0199e7);
	-pie-background: linear-gradient(#19affe, #0199e7);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.ui-slider-horizontal .ui-slider-range {
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	position: relative;
}
.ui-slider .ui-slider-handle {
	background: url(/images/new-images/slider-handle.png) no-repeat;
	border: none;
	outline: none;
}

/*** OVERRIDEN SLIDER CSS STYLE ENDS ***/

/*** HOW BLASTING WORKS STYLE STARTS ***/
.content .blastListingsWrapper,
.content .whyBlastWrapper,
.content .howitworksWrapper {
	padding-left: 98px;
	padding-right: 98px;
}
.content .blastListingsWrapper {
	padding-top: 46px;
	padding-bottom: 52px;
}
.blastListingsWrapper #blast_video_container {
	position: relative;
	margin-bottom: 2px;
}
.blastListingsWrapper #blast_video_container .whenBlastContainer {
	color: #fff;
	left: 50px;
	position: absolute;
	top: 45px;
	width: 275px;
}
.blastListingsWrapper #blast_video_container .whenBlastContainer h3 {
	font-size: 20px;
	line-height: 1.3;
}
.blastListingsWrapper #blast_video_container .whenBlastContainer p {
	font-size: 16px;
	line-height: 1.45;
}
.blastListingsWrapper #blast_video_container #blasting_video_btn {
	background: #e11e25;
	border-radius: 4px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	-ms-border-radius: 4px;
	border-bottom: 1px solid #003d20;
	bottom: 41px;
	color: #fff;
	font-size: 24px;
	font-style: italic;
	text-align: center;
	padding: 17px 0;
	position: absolute;
	right: 58px;
	text-decoration: none;
	width: 230px;
	text-shadow: 0 1px #003d20;
	behavior: url(/css/PIE.htc);
}
.blastListingsWrapper #blast_video_container #blasting_video_btn img {
	margin-left: 5px;
	margin-top: -3px;
}
.blastListingsWrapper .keyFeatures {
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#f2f2f2), to(#fff));
	background: -webkit-linear-gradient(#f2f2f2, #fff);
	background: -moz-linear-gradient(#f2f2f2, #fff);
	background: -ms-linear-gradient(#f2f2f2, #fff);
	background: -o-linear-gradient(#f2f2f2, #fff);
	background: linear-gradient(#f2f2f2, #fff);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f2f2', endColorstr='#fff',GradientType=0 );
	-pie-background: linear-gradient(#f2f2f2, #fff);
	padding: 30px 0 35px;
	border-bottom: 1px solid #e5e5e5;
	border-radius: 4px 4px 0 0; 
	-webkit-border-radius: 4px 4px 0 0; 
	-ms-border-radius: 4px 4px 0 0; 
	-moz-border-radius: 4px 4px 0 0; 
	behavior: url(/css/PIE.htc);
}
.blastListingsWrapper .keyFeatures .keyFeature {
	width: 27%;
	margin-left: 45px;
}
.blastListingsWrapper .keyFeatures .keyFeature:FIRST-CHILD {
	margin-left: 30px;
}
.blastListingsWrapper .keyFeatures .keyFeature .keyTitle {
	font-size: 30px;
	color: #000;
	font-weight: bolder;
	width: auto;
	overflow: hidden;
	font-style: italic;
	padding-left: 10px;
	margin-top: 13px;
	line-height: 1;
}
.blastListingsWrapper .keyFeatures .keyFeature h3 {
	font-size: 16px;
	line-height: 1;
	margin-top: 24px;
	margin-bottom: 10px;
}
.blastListingsWrapper .keyFeatures .keyFeature p {
	font-size: 14px;
	color: #666;
	word-spacing: .5px;
	line-height: 1.5;
	letter-spacing: -.2px;
}
.blastListingsWrapper .keyFeatures .winContainer h3,
.blastListingsWrapper .keyFeatures .winContainer p {
	color: #000;
}
.blastListingsWrapper .keyFeatures .winContainer p {
	color: #666;
}
.how-blasting-works .composeBlastContainer {
	padding-top: 32px;
	border-top: 1px solid #eeeeee;
	margin-top: 30px;
}
.how-blasting-works .composeBlastContainer h1 {
	font-weight: 600;
	font-size: 42px;
}
.how-blasting-works .composeBlastContainer h3 {
	font-size: 22px;
	font-weight: 500;
	color: #666;
}
.how-blasting-works .composeBlastContainer a {
	background-color: #6fad51;
	border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	color: #fff;
	display: inline-block;
	font-size: 30px;
	font-weight: 600;
	line-height: 1em;
	outline: none;
	padding: 25px 10px;
	position: relative;
	text-decoration: none;
	text-align: center;
	width: 321px;
	behavior: url(/css/PIE.htc);
}
.content .whyBlastWrapper {
	padding-bottom: 54px;
	padding-top: 19px;
}
.whyBlastWrapper .contentBoxTitle,
.howitworksWrapper .contentBoxTitle {
	font-size: 36px;
	border-bottom: 1px solid #e1e1e1;
	margin-bottom: 35px;
	padding-top: 30px;
	padding-bottom: 42px;
	line-height: 1em;
}
.content .whyBlastWrapper .contentBoxTitle:FIRST-CHILD,
.content .howitworksWrapper .contentBoxTitle:FIRST-CHILD {
	padding-top: inherit;
}
.whyBlastWrapper p, .contentBox.wondering .description {
	font-size: 18px;
	color: #666;
	margin-bottom: 30px;
	letter-spacing: -.3px;
	word-spacing: .4px;
	line-height: 1.3;
	font-weight: 500;
}
.whyBlastWrapper .whyBlastContainer img {
	margin-right: 70px;
}
.whyBlastWrapper .whyBlastContainer .whyBlast {
	color: #333;
	font-size: 16px;
	font-weight: bold;
	list-style-position: inside;
	margin-top: 12px;
	width: 79%;
}
.whyBlastWrapper .whyBlastContainer .whyBlast li {
	padding: 10px 0;
	border-bottom: 1px solid #e1e1e1;
	word-spacing: -.5px;
	letter-spacing: -.2px;
}
.whyBlastWrapper .whyBlastContainer .whyBlast li img {
	float: left;
	margin-right: 12px;
	margin-top: 3px;
}
.whyBlastWrapper .whyBlastContainer .whyBlast li span {
	display: block;
	overflow: hidden;
	width: auto;
}
.why-blast h2 {
	padding: 30px 0 5px 250px;
	text-align: left;
}
.why-blast p {
	padding: 15px 0 0 250px;
}
.why-blast p.small {
	font-size: 17px;
}
.why-blast ul {
	padding: 15px 0 0 250px;
}
.why-blast ul li {
	padding: 10px 0 5px 30px;
	background: url(../images/new-images/list-style-icon.png) no-repeat left center;
}
.content .howitworksWrapper .contentBoxTitle {
	margin-bottom: 25px;
}
.content .howitworksWrapper {
	padding-bottom: 60px;
	padding-top: 22px;
}
.howitworksWrapper .stepContainer {
	width: 53%;
	margin-top: 20px;
}
.howitworksWrapper .stepContainer .step .media-heading {
	color: #e11e25;
	font-size: 24px;
	margin-bottom: 10px;
}
.howitworksWrapper .stepContainer .step .media-object {
	margin-right: 30px;
}
.howitworksWrapper .stepContainer .step p {
	font-size: 14px;
	color: #666;
	line-height: 1.4;
	word-spacing: -.7px;
	letter-spacing: -.3px;
}
.howitworksWrapper .testimonialsWrapper {
	border-top: 1px solid #e4e4e4;
	margin-top: 30px;
	padding-top: 30px;
}
.howitworksWrapper .testimonialsWrapper .testTitle {
	font-size: 22px;
	margin: 8px 0 0 100px;
	color: #666;
}
.howitworksWrapper .testimonialsWrapper .testPrevNextContainer a {
	background-image: url(/images/new-images/testimonials-prev-next-arrow-sprite.png);
	border-radius: 50%;
	-webkit-border-radius: 50%;
	-ms-border-radius: 50%;
	-moz-border-radius: 50%;
	display: inline-block;
	height: 34px;
	outline: none;
	width: 34px;
}
.howitworksWrapper .testimonialsWrapper .testPrevNextContainer .next {
	background-position: -34px;
}
.howitworksWrapper .testimonialsWrapper .testPrevNextContainer .prev:HOVER {
	background-position: 68px;
}
.howitworksWrapper .testimonialsWrapper .testPrevNextContainer .next:HOVER {
	background-position: 34px;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer {
	padding: 0 !important;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .testimonials {
	background-color: #f1f1f1;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	-ms-border-radius: 5px;
	padding: 30px;
	margin-top: 23px;
	margin-left: 100px;

}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .testimonials p {
	line-height: 1.4;
	color: #666;
	font-size: 16px;
	letter-spacing: -.1px;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .testimonials p.signature {
	line-height: 1.4;
	color: #666;
	font-size: 12px;
	letter-spacing: -.1px;
	margin-bottom: 0;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .testimonials img {
	position: absolute;
	bottom: -10px;
	right: 20px;
}
.testimonialsContainer .arrow {
	background-image: url(../images/testimonial-arrow.png);
	background-repeat: no-repeat;
	width: 12px;
	height: 21px;
	position: absolute;
	margin: 60px 0 0 89px;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .media {
	position: absolute;
	margin: 50px 0 0 5px;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .media-object {
	background: url(/images/new-images/testimonial-user-img.png) -11px 0 no-repeat;
	border-radius: 50%;
	-webkit-border-radius: 50%;
	-ms-border-radius: 50%;
	-moz-border-radius: 50%;
	behavior: url(/css/PIE.htc);
	overflow: hidden;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .media-body p {
	margin-top: -3px;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .media-body p.text-right {
	line-height: 1.1em;
	margin-top: 1px;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .media-body {
	color: #666;
	margin-top: 4px;
}
.howitworksWrapper .testimonialsWrapper .testimonialsContainer .media-heading {
	font-size: 18px;
	line-height: 1;
	margin-bottom: 0;
}
.contentBox.wondering .fbEmbedPost {
	margin: 34px auto;
}
.contentBox.wondering .fbPostBefore, .contentBox.wondering .fbPostAfter p {
	line-height: 38px;
}
.contentBox.wondering .fbPostAfter {
	width: 150px;
	padding-left: 48px;
	right: -207px;
	top: 171px;
	text-align: center;
}
.contentBox.wondering .bottom {
	border-top: 1px solid #ccc;
	margin-left: -90px;
	padding: 53px 90px 0;
	width: 100%;
}
.contentBox.wondering h3 {
	float: left;
	font-size: 34px;
	font-weight: normal;
	margin: 0 0 0 9px;
	width: 505px;
}
.contentBox.wondering .uiButtonNew {
	float: right;
	margin: 7px 7px 0 0;
	font-size: 31px;
	padding: 24px 0;
}
.bottomContacts {
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAADCAYAAABWKLW/AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABdJREFUeNpiZGBgkGKAAiYGJIDCAQgwAAQqACB/hf4PAAAAAElFTkSuQmCC) repeat 0 0;
	border-top: 1px solid #a7a8aa;
	padding: 20px 28px 7px 30px;
	font-weight: bold;
	text-shadow: 1px 1px 0 rgba(255,255,255,.4);
}
.bottomContacts .pull-left {
	margin-top: -0;
	font-size: 16px;
}
.bottomContacts .phone {
	padding-bottom: 6px;
}
.bottomContacts .pull-right {
	font-size: 12px;
	color: #666;
}

/*** HOW BLASTING WORKS STYLE ENDS ***/

/*** SEMINAR PAGE STYLE STARTS ***/
.seminar.bannerContainer {
	color: #fff;
	margin-bottom: 50px;
}
.seminar.bannerContainer:after {
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAgBAMAAAC8xgahAAAAJ1BMVEUAAABEibBEibBEibBEibBEibBEibBEibBEibBEibBEibBEibBEibBfdP99AAAADHRSTlMAHEYw4Q1ffvHNnLYrJP5SAAAAvklEQVQoz33SrRHCQBDF8ZdkSGaCiUAgKSCCAmIQOAQCg0sBCAqIoQgcggIQFIAKmUCAovgc9vZu9372P889nB4eHfq+PEVvp9cmAxZ63gAIZlq9ZngZavmMt0DLY3wUcq3xFcl5hJ/cNwZCKZf4W7m1BYndvIahsusFpkQckwOvN3Apz3tw/BZ32JZmPsLCbtHN4RhQ3sJFt2icMbvFBJKIPiQq6ICSkD4kyumAklgb0y1q6BI6oKhq4ZOWYJ5yQIJUhlhROwAAAABJRU5ErkJggg==) no-repeat 0 0;
	bottom: -32px;
	content: " ";
	height: 32px;
	position: absolute;
	width: 59px;
	left: 50%;
	margin-left: -30px;
}
.seminar.bannerContainer .bannerTitle {
	font-size: 50px;
	font-weight: bold;
	margin-top: 24px;
	/*margin-bottom: 27px;*/
	text-align: center;
	line-height: 60px;
	top: 25px;
}
.seminar.bannerContainer .bannerTitle > p {
	margin: 0 auto;
	width: 600px;
	line-height: 52px;
}
.seminar.bannerContainer .bannerTitle > p:first-child {
	width: 800px;
	font-size: 40px;
	line-height: 61px;
}
.seminar.bannerContainer .bannerTitle span {
	font-size: 40px;
	font-weight: normal;
}
.seminar.bannerContainer .bannerTitle, .seminar.bannerContainer .bannerBottom {
	text-shadow: 0 2px 1px rgba(0,0,0,.8);
}
.seminar.bannerContainer .bannerTitle, .seminar.bannerContainer .bannerBottomHolder {
	position: absolute;
	z-index: 2;
	width: 100%;
	/*position: relative;
	z-index: 1;*/
}
.seminar.bannerContainer .bannerBottomHolder {
	bottom: 89px;
}
.bannerContainer.no-margin {
	margin-bottom: 0;
}
.seminar.bannerContainer .bannerBottom {
	color: #fff;
	font-size: 30px;
	width: 445px;
	margin: 0 auto;
	display: block;
	line-height: 25px;
	position: static;
}
.contentBox.webinarDescover {
	padding: 50px 20px 40px;
}
.webinarDescover .paddingHolder {
	padding: 0 16px;
}
.webinarDescover p {
	font-size: 16px;
}
.webinarDescover .paddingHolder {
	font-size: 24px;
	padding: 0 17px 0 21px;
	text-align: center;
	line-height: 29px;
}
.webinarList {
	margin: 48px 0;
}
.webinarList, .webinarList li {
	text-align: center;
}
.webinarList [class*="icon"], .webinarList li {
	display: inline-block;
}
.webinarList [class*="icon"] {
	background: url(../images/new-images/webinar-icons.png) no-repeat 0 0;
	width: 103px;
	height: 104px;
	margin-bottom: 6px;
}
.webinarList li {
	border-left: 1px solid #eee;
	color: #2b3b42;
	width: 176px;
	padding: 0 6px;
	vertical-align: top;
}
.webinarList li:first-child {
	border: none;
}
.webinarList .icon2 {
	background-position: 0 -104px;
}
.webinarList .icon3 {
	background-position: 0 -208px;
}
.webinarList .icon4 {
	background-position: 0 -416px;
}
.webinarList .icon5 {
	background-position: 0 -312px;
}
.webinarList .title {
	font-size: 24px;
	font-weight: bold;
	margin-bottom: 7px;
}
.webinarList .subtitle {
	font-size: 14px;
	color: #666;
}
.webinarDescover .description {
	font-size: 24px;
	text-align: center;
	color: #333;
}
.webinarDescover .description span {
	text-decoration: underline;
	font-weight: bold;
}
.contentBox.webinarDescover h3 {
	font-size: 37px;
	text-align: center;
}
.upcomingSeminarsHolder {
	border-top: 1px solid #ccc;
	margin-top: 60px;
	padding-top: 40px;
}
.upcomingSeminars {
	text-align: center;
	margin-top: 34px;
	text-transform: uppercase;
}
.upcomingSeminars li {
	display: inline-block;
	background: #0097dc;
	border-radius: 10px;
	color: #fff;
	width: 204px;
	padding: 52px 15px 15px;
	margin-left: 35px;
}
.upcomingSeminars li:first-child {
	margin-left: 0;
}
.upcomingSeminars .dateHolder {
	background: #fff;
	margin-bottom: 15px;
	padding-bottom: 7px;
}
.upcomingSeminars .date {
	border-radius: 50%;
	background: #0d3952;
	border: 6px solid #fff;
	padding-top: 22px;
	width: 128px;
	position: relative;
	bottom: 26px;
	height: 106px;
	margin: 0 auto;
	font-weight: bold;
}
.upcomingSeminars .month {
	font-size: 22px;
}
.upcomingSeminars .day {
	font-size: 64px;
	line-height: 1;
}
.upcomingSeminars .time {
	color: #656565;
	margin-top: -22px;
}
.upcomingSeminars .selectLink {
	background: #ff7f00;
	color: #fff;
	display: block;
	border-radius: 5px;
	font-weight: bold;
	font-size: 25px;
	padding: 19px 0 30px;
}
.upcomingSeminars .selectLink:hover, .upcomingSeminars .selectLink:focus {
	text-decoration: none;
}
.upcomingSeminars .selectLink span {
	font-size: 45px;
	display: block;
	margin-top: 18px;
}
.contentBox.watchSeminar {
	padding: 50px 40px 40px;
}
.watchSeminar .videosHolder {
	margin-bottom: 27px;
}
.videosHolder .player {
	background: #e1e1e1;
	height: 375px; /*temporary*/
	width: 638px;
	float: left;
}
.watchSeminar .videosListHolder {
	float: right;
	width: 261px;
	border: 1px solid #ebecec;
	border-left: none;
	color: #333;
	font-size: 18px;
	padding: 10px;
	border-radius: 0 4px 4px 0;
	position: relative;
}
.watchSeminar .customScrollbar {
	height: 353px;
	overflow: hidden;
}
.watchSeminar .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar, .watchSeminar .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar {
	background: #19affe;
	background: -moz-linear-gradient(top,  #19affe 0%, #1c9bdc 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#19affe), color-stop(100%,#1c9bdc));
	background: -webkit-linear-gradient(top,  #19affe 0%,#1c9bdc 100%);
	background: -o-linear-gradient(top,  #19affe 0%,#1c9bdc 100%);
	background: -ms-linear-gradient(top,  #19affe 0%,#1c9bdc 100%);
	background: linear-gradient(to bottom,  #19affe 0%,#1c9bdc 100%);
	border: 1px solid #0981b6;
	border-radius: 8px;
}
.watchSeminar .mCSB_container {
	margin-right: 15px;
}
.watchSeminar .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar {
	width: 8px;
}
.watchSeminar .mCSB_scrollTools .mCSB_dragger {
	right: 2px;
}
.mCSB_dragger_bar:before {
	border-radius: 50%;
	border-top: 1px solid transparent;
	box-shadow: 0 2px 5px 1px #FFF;
	content: " ";
	left: 0;
	position: absolute;
	top: -1px;
	width: 100%;
}
.watchSeminar .videosList li {
	background: #f5f5f5;
	margin-top: 11px;
	padding: 13px 10px 15px 15px;
	cursor: pointer;
	border-radius: 4px;
}
.watchSeminar .videosList li:first-child {
	margin-top: 0;
}
.watchSeminar .videosList li:hover,
.watchSeminar .videosList li.active {
	background: #8dccec;
}
.videosList li img {
	float: left;
	margin-right: 17px;
}
.contentBox.watchSeminar h3 {
	text-align: center;
	font-size: 24px;
}
.watchSeminar .description {
	text-align: center;
	font-size: 14px;
}
.contentBox.learnBlog {
	padding: 30px 60px 40px;
}
.learnBlog h2 {
	font-size: 37px;
}
.learnBlog .postsList {
	text-align: center;
}
.learnBlog .postsList li {
	width: 250px;
	padding: 5px;
	display: inline-block;
	border: 4px solid #dedede;
	text-align: left;
	font-weight: bold;
	margin-left: 28px;
}
.learnBlog .postsList li:first-child {
	margin-left: 0;
}
.learnBlog .postsList .paddingWrapper {
	padding-left: 20px;
}
.postsList .category {
	color: #999;
	text-transform: uppercase;
	display: block;
	margin: 17px 0 10px 0;
	font-size: 12px;
}
.postsList .postText {
	line-height: 23px;
}
.postsList .bottom {
	border-top: 1px solid #e5e5e5;
	color: #808080;
	font-weight: bold;
	margin-right: -5px;
	margin-top: 34px;
	padding-top: 5px;
}
.postsList .bottom .date {
	font-weight: normal;
	float: right;
	font-size: 12px;
	margin-right: 5px;
}

/*** SEMINAR PAGE STYLE STARTS ***/

.sitemap > li {
	float: left;
	width: 260px;
	margin-right: 10px;
}

/* FOOTER MAP STYLE STARTS  */
.footerMap {
	margin-bottom: 30px;
}
.footerMap .contact {
	background: url(/images/new-images/opac10.png);
	padding: 19px 31px 19px 29px;
	position: relative;
}
.footerMap .contactTopBorder {
	background: url(/images/new-images/opac30.png);
	height: 1px;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
}
.footerMap .contact div.pull-left {
	color: #666;
	font-size: 16px;
	font-weight: bold;
	text-shadow: 1px 0 0 #fff;
}
.footerMap .contact .phone {
	background: url(/images/new-images/phone-icon.png) no-repeat;
	color: #000;
	padding-left: 33px;
	padding-bottom: 3px;
	text-shadow: none;
}
.footerMap .contact div.pull-right span {
	color: #666;
	font-weight: 600;
	word-spacing: 1px;
}
.footerMap .contact div.pull-right a {
	color: #0098dc;
}
.footerMap .map {
	background: #fff;
	padding: 7px 11px;
}
/* FOOTER MAP STYLE ENDS */

/*** FOOTER STYLE STARTS ***/
.footer {
	background: url(/images/new-images/footer-bg.png) center center #d9dbde;
	padding: 20px 0;
	margin-bottom: 10px;
}
.footer a {
	text-decoration: none;
}
.footer .leftContent,
.footer .rightContent {
	padding: 0 10px;
}
.footer .nav {
	border-bottom: 1px solid #b4b4b4;
	color: #666;
	font-size: 14px;
	font-weight: bold;
	margin: 0;
}
.footer .nav a {
	color: #666;
}
.footer .leftContent p {
	border-top: 1px solid #e1e1e1;
	color: #999;
	font-size: 12px;
	padding-top: 5px;
	text-align: center;
}
.footer .rightContent {
	text-align: center;
}
.footer .rightContent p {
	color: #666;
	margin-bottom: 0;
}
.footer .quote {
	border-bottom: 1px solid #333;
	color: #333;
	display: inline-block;
	font-size: 16px;
	font-weight: bold;
	margin-top: 5px;
}
.wePostContainer {
	padding: 30px 20px;
}
.wePostContainer .wePost li {
	display: inline-block;
}
.wePostContainer .wePost li a {
	color: #000;
	font-size: 18px;
	font-style: italic;
	font-weight: bold;
	margin-right: 40px;
	text-decoration: none;
}
.wePostContainer .wePostWeb {
	background: url(/images/new-images/webPostSocialIcon.png) no-repeat 0 center;
	color: #000;
	font-size: 12px;
	font-weight: bold;
	padding-left: 18px;
	text-decoration: none;
}
/*** FOOTER STYLE ENDS ***/

.termsBox {
	text-align: center;
	padding: 5px 10px;
	border-radius: 4px;
	background: #ebebeb;
	margin: 10px 0;
	line-height: 18px;
}
.termsBox input {
	line-height: 18px;
	vertical-align: top;
	margin-right: 5px;
}
.promoBox {
	border-radius: 5px;
	background: #e7fae0;
	padding: 20px;
	font-size: 16px;
	margin: 30px 0;
}
.promoBox > div:first-child {
	margin-right: 20px;
}
.promoBox p {
	color: #666;
	margin: 10px 0 0;
}
.promoBox > div:last-child {
	border-radius: 3px;
	padding: 10px;
	background: #d7e9cf;
	-webkit-box-shadow: inset 1px 1px 6px 0 #a1bb96, 1px 1px 0 #fff;
	box-shadow: inset 1px 1px 6px 0 #a1bb96, 1px 1px 0 #fff;
	text-align: center;
}
.promoBox > div:last-child h5 {
	color: #70a458;
	line-height: 26px;
}
.promoBox > div:last-child .promoCode {
	color: #285712;
	font-size: 24px;
	font-weight: 600;
	line-height: 26px;
}
.listing .notUsingCBAgent h5 br {
	display: none;
}

.contentBox.experts {
	padding: 30px 50px 40px;
}

.contentBox.experts .social-experts {
	border-top: 1px solid #eee;
	margin: 30px 40px 0 40px;
	padding: 40px 0;
}

.contentBox.experts .step {
	text-align: center;
	width: 195px;
	float: left;
	margin-right: 10px;
	padding: 60px 50px 0 0;
	background: url(../images/left-to-right-arrow.png) no-repeat right center;
}

.contentBox.experts .step h3 {
	color: #333;
	font-size: 18px;
}

.contentBox.experts .step p {
	color: #666;
	font-size: 13px;
}

.contentBox.why-experts .fbPostsAfterText {
	padding: 0 40px;
}
.contentBox.why-experts .fbPostsAfterText:before {
	left: 20px;
}
.contentBox.why-experts .fbPostsAfterText:after {
	right: 20px;
}
.arrowsDown {
	font: normal 24px 'mayfield';
	color: #666;
	position: relative;
	margin: 40px 0 0 0;
}
.arrowsDown:before, .arrowsDown:after {
	position: absolute;
	content: " ";
	width: 40px;
	height: 47px;
	top: 0;
	background: url(../images/arrow-down-left.png) no-repeat 0 0;
}
.arrowsDown:after {
	background: url(../images/arrow-down-right.png) no-repeat 0 0;
}

.greatest {
	width: 500px;
}
.greatest.thanks {
	width: auto;
}
.greatest h3 {
	font-weight: normal;
	color: #666666;
	font-size: 24px;
	text-align: center;
	margin-top: 20px;
}
.greatest h1 {
	font-weight: bold;
	font-size: 56px;
	color: #000;
	line-height: 1.1em;
	border-bottom: 1px solid #e2e2e3;
	padding: 10px 0 20px 0;
	text-align: center;
	margin-bottom: 30px;
}
img.book {
	float:right;
	margin-top:30px
}
img.book.mobile {
	display: none;
}
.greatest p {
	color: #333333;
	font-size: 16px;
	margin: 25px 20px 25px 15px;
}
.greatest-form {
	border-top: 1px solid #e2e2e3;
}
.greatest-form p {
	margin: 25px 20px 5px 15px;
	color: #99999e;
	font-size: 18px;
}
.greatest-form label {
	background-image: url(../images/new-images/get_now_arrow.png);
	background-repeat: no-repeat;
	text-transform: uppercase;
	color: #fff;
	font-size: 14px;
	width: 215px;
	height: 56px;
	padding: 20px 0 0 8px;
	margin-left: 15px;
	display: inline-block;
}
.greatest-form #email {
	width: 300px;
	height: 30px;
}
.greatest-form .uiInput {
	height: 38px !important;
	margin-top: 8px !important;
}
.greatest-form .validation {
	color: red;
	margin-left: 245px;
}

/*** MEDIA QUERIES STARTS ***/

@media (min-width : 1024px) {
	h3{
		font-size: 26px;
	}
	.uiButtonNew{
		width: 281px;
	}
	.container{
		width: 1000px;
	}
	ul.settings p {
		font-size: 14px;
	}
	.stepContent .settings .unstyled li label {
		font-size: 14px;
	}
	.tryDemoTxt h3 {
		font-size: 30px;
	}
	.planContainer > li {
		width: 248px;
	}
	.planContainer > li.popular{
		width: 278px;
	}
	.planContainer .priceContainer .price {
		font-size: 54px;
	}
	.planContainer .priceContainer .price sup {
		font-size: 24px;
	}
	.planContainer .popular .priceContainer .price {
		font-size: 65px;
	}
	.planContainer .popular .priceContainer .price sup {
		font-size: 29px;
	}
	.planContainer .priceContainer .price sub {
		margin-left: -28px;
	}
	.planContainer .popular .priceContainer .price sub {
		margin-left: -33px;
	}
	.planContainer .uiButtonNew {
		width: 208px;
	}
	.planContainer .popular .uiButtonNew {
		width: 238px;
	}
	.promoBox > div:first-child {
		width: 580px;
	}
	.promoBox > div:last-child {
		width: 160px;
	}
	.listing .agentDetail .uiButtonNew {
		font-size: 24px;
		font-size: 21px\0/;
		width: 221px;
	}
}
@media (max-width: 1023px) {
	.testimonialsHolder .colLeft, .testimonialsHolder .colRight {
		width: auto;
		float: none;
		margin-left: 0;
	}
	.testimonialsHolder .thumbHolder {
		float: left;
		text-align: center;
	}
	.testimonialsHolder .textHolder {
		overflow: hidden;
		width: auto;
		margin: 0 0 0 117px;
		display: block;
	}
	.socialMedia .icons-holder {
		margin-left: 0;
	}
	.contentBox.contentType .fbEmbedPost {
		float: none;
		margin: 30px auto 0;
	}
	.contentBox.contentType .fbEmbedPost:first-child {
		margin-top: 0;
	}
	.freeTrial .icons-holder {
		background: transparent;
	}
	.freeTrial .icons-holder [class*="item"] {
		margin-left: 75px;
	}
	.content .teamAfterText {
		padding: 0 110px;
	}
	.content .teamAfterText:before {
		left: 50px;
	}
	.content .teamAfterText:after {
		right: 50px;
	}
	.webinarList li {
		border: none;
		margin-bottom: 15px;
	}
	.webinarList li:last-child {
		margin-bottom: 0;
	}
	.upcomingSeminars li {
		margin-left: 25px;
	}
	.videosHolder .player {
		width: 578px;
	}
	.contentBox.why-experts .case {
		width: 210px;
	}
	.contentBox.experts .step {
		width: 320px;
	}
	.contentBox.experts .result {
		text-align: center;
		width: 750px;
	}
	.how-blasting-works .composeBlastContainer a {
		float: none;
	}
	.contentBox.wondering .uiButtonNew {
		float: none;
	}
	#getfreebook [type="submit"] {
		font-size: 16px;
		width: 260px;
	}
}
@media (min-width : 980px) and (max-width : 1023px) {
	h2 {
		font-size: 35px;
	}
	.navigation .nav-pills li a {
		padding-left: 15px;
		padding-right: 15px;
	}
	.stepContent .settings > li {
		width: 190px;
	}
	.postNumberContainer {
		width: 320px;
	}
	.postNumberContainer .postNumbers li {
		margin-left: 45px;
	}
	.stepContent .btnContainer {
		width: 260px;
	}
	.footer .nav a {
		font-size: 13px;
	}
	.footer .rightContent p {
		font-size: 12px;
	}
	.promoBox > div:first-child {
		width: 520px;
		font-size: 14px;
	}
	.promoBox > div:last-child {
		width: 160px;
	}

	.contentBox .arrow-step {
		width: 300px;
	}
	.contentBox .arrow-step:nth-child(2) {
		clear: right;
	}
	.contentBox .arrow-step.last {
		clear: left;
		float: none;
		margin-right: auto;
		margin-left: auto
	}
}

@media (max-width: 979px) and (min-width: 768px) {
	h1 {
		font-size: 30px;
	}
	h2 {
		font-size: 25px;
	}
	h5 {
		font-size: 15px;
	}
	.bannerContainer span.bannerInfo {
		padding-top: 20px;
	}
	.bannerContainer .bannerTitle,
	.bannerContainer.howBlastingWorks .bannerTitle {
		font-size: 28px;
		margin-bottom: 10px;
		width: 400px;
	}
	.bannerContainer .bannerInfo .bannerSubTitle {
		font-size: 17px;
	}
	.bannerContainer.howBlastingWorks .bannerInfo .bannerSubTitle {
		width: 480px;
	}
	.banner .info {
		top: 83px;
		left: 190px;
	}
	.banner .info h5 {
		font-size: 12px;
	}
	.banner .info .other h5 {
		font-size: 10px;
	}
	.banner h2 {
		line-height: 30px;
		padding-right: 30px;
	}
	.banner h6 {
		font-size: 9px;
	}
	.tryDemo {
		font-size: 28px;
		padding: 16px 0;
		width: 175px;
	}
	.banner .tryDemo {
		margin-left: 5px;
	}
	.navigation .nav-pills {
		float: left;
		margin: auto;
	}
	.stepBox .stepTitle h2 {
		line-height: 1em;
	}
	.stepBox.step1 .stepTitle h2 {
		padding-top: 22px;
		padding-bottom: 23px;
	}
	.stepBox .stepTitle .titleNumber {
		height: 70px;
		width: 70px;
	}
	.stepBox .stepTitle .titleNumber span {
		font-size: 33px;
		top: 19px;
		left: 26px;
	}
	.step1 .stepContent .ladyCartoon {
		margin-left: 0;
		float: none;
		text-align: center;
	}
	.stepContent .settings > li {
		width: 168px;
		margin-left: 10px;
	}
	.step2 .stepContent .ladyCartoon {
		float: none;
		margin: 0 0 10px 0;
		text-align: center;
	}
	.postNumberContainer {
		width: 280px;
	}
	.postNumberContainer .postNumbers li {
		margin-left: 37px;
	}
	.stepContent .settings .unstyled li label {
		padding-left: 20px;
		font-size: 11px;
	}
	.step2 .stepContent .noteContainer {
		width: 220px;
		margin-left: 24px;
	}
	.step3 .stepContent .noteContainer {
		margin-left: 65px;
	}
	.stepContent .btnContainer {
		float: none;
		text-align: center;
	}
	.stepBox > p {
		margin-left: 92px;
	}
	ul.settings p {
		font-size: 11px;
		line-height: 1.2em;
	}
	ul.settings label {
		font-size: 11px;
	}
	.strategyContainer .media .media-body h3{
		font-size: 20px;
		line-height: 25px;
	}
	.strategyContainer .media .media-body {
		font-size: 13px;
	}
	.tryDemoTxt h3 {
		font-size: 20px;
		line-height: 1.4em;
	}
	.tryDemoTxt h4 {
		font-size: 15px;
	}
	ul.planContainer {
		margin-left: -70px;
		margin-right: -70px;
	}
	.planContainer > li {
		width: 208px;
	}
	.planContainer > li.popular {
		width: 222px;
	}
	.planContainer .priceContainer {
		padding-left: 20px;
	}
	.planContainer .uiButtonNew {
		font-size: 20px;
		padding: 14px 0;
		width: 170px;
	}
	.planContainer .popular .uiButtonNew {
		font-size: 23px;
		padding: 14px 0;
		width: 185px;
	}
	.planContainer .priceContainer .price {
		font-size: 44px;
	}
	.planContainer .popular .priceContainer .price {
		font-size: 47px;
	}
	.planContainer .priceContainer .price sup,
	.planContainer .popular .priceContainer .price sup {
		font-size: 20px;
	}
	.planContainer .priceContainer .price sub {
		font-size: 10px;
		margin-left: -23px;
	}
	.planContainer .popular .priceContainer .price sub {
		font-size: 12px;
		margin-left: -23px;
	}
	.listing .agentDetail .media a.pull-left {
		float: none;
	}
	.slider .bx-viewport {
		height: 60px;
	}
	.listing_pic a {
		height: 50px;
		width: 50px;
	}
	.thumbnail_img_container span.prevSlide,
	.thumbnail_img_container span.nextSlide {
		top: 24.5px
	}
	.footer .nav a {
		padding-left: 5px;
		padding-right: 5px;
		font-size: 12px;
	}
	.footer .rightContent p {
		font-size: 10px;
		line-height: 1.2em;
	}
	.promoBox > div:first-child {
		width: 340px;
		font-size: 12px;
	}
	.promoBox > div:last-child {
		width: 120px;
	}
	.questionWrapper .question h5 {
		max-width: 92%;
	}
	.fbPostBefore, .fbPostAfter {
		position: static;
		margin: 0 auto;
		background: transparent;
		padding: 0;
	}
	.fbPostBefore {
		width: 113px;
		margin-bottom: 25px;
	}
	.fbPostAfter {
		margin-top: 25px;
	}
	.fbPostAfter .last {
		background-position: 0 0;
		padding-top: 75px;
	}
	.fbEmbedPost .fb-post {
		min-height: 200px;
	}
	.socialMedia .icons-holder div {
		width: 197px;
	}
	.contentBox.why-experts .case {
		width: 230px;
		margin-right: 20px;
		margin-left: 20px;
	}
	.contentBox.why-experts .arrowsAbove {
		margin: 40px 40px 0 40px;
	}
	.contentBox.experts .step {
		width: 220px;
	}
	.contentBox.experts .result {
		width: 600px;
	}
	.contentBox.experts .pull-left {
		width: 350px;
	}

	.contentBox .arrow-step {
		width: 200px;
	}
	.contentBox .arrow-step:nth-child(2) {
		clear: right;
	}
	.contentBox .arrow-step.last {
		clear: left;
		float: none;
		margin-right: auto;
		margin-left: auto
	}
}

@media (max-width: 979px) {
	.navigation .nav-pills {
		float: left;
		margin: auto;
	}
	.navigation .nav-pills li a {
		font-size: 13px;
		padding-left: 9px;
		padding-right: 8px;
	}
	.content .testimonialsContainer .testimonialsBottomArrow {
		display: none;
	}
	.content .memberInfo {
		padding-left: 30px;
	}
	.content .memberInfo .trialInfo {
		margin-top: 5px;
		padding-top: 10px;
		padding-bottom: 10px;
		max-width: 200px;
	}
	.wePostContainer .wePost li a {
		margin-right: 20px;
	}
	.content .blastListingsWrapper,
	.content .whyBlastWrapper,
	.content .howitworksWrapper {
		padding-left: 50px;
		padding-right: 50px;
	}
	.blastListingsWrapper .keyFeatures .keyFeature {
		width: 40%;
		margin-bottom: 20px;
	}
	.blastListingsWrapper .keyFeatures .winContainer {
		float: none;
		margin: auto;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer {
		left: 30px;
		top: 35px;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer h3 {
		font-size: 18px;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer p {
		font-size: 14px;
	}
	.blastListingsWrapper .composeBlastContainer h3 {
		font-size: 17px;
		line-height: 1.2;
	}
	.blastListingsWrapper .composeBlastContainer a {
		width: 270px;
		font-size: 26px;
		padding: 20px 0;
	}
	.whyBlastWrapper p{
		font-size: 16px;
	}
	.whyBlastWrapper .whyBlastContainer img {
		margin-right: 38px;
	}
	.whyBlastWrapper .whyBlastContainer .whyBlast {
		font-size: 12px;
	}
	.whyBlastWrapper .whyBlastContainer .whyBlast li {
		padding: 7px 0;
		word-spacing: 0;
	}
	.howitworksWrapper .stepContainer .step .media-heading {
		font-size: 23px;
	}
	.howitworksWrapper .stepContainer .step p {
		font-size: 13px;
	}
	.howitworksWrapper .stepContainer .step .media-object {
		margin-right: 23px;
	}
	.howitworksWrapper .testimonialsWrapper .testimonialsContainer .testimonials p {
		font-size: 13px;
	}
	.bannerContainer .playButton {
		padding: 11px 25px 11px 28px;
	}
	.bannerBottom h3 {
		font-size: 17px;
		line-height: 17px;
	}
	.bannerBottom p {
		font-size: 14px;
		margin-bottom: 8px;
	}
	.freeTrial .icons-holder [class*="item"] {
		margin-left: 15px;
		width: 185px;
	}
	.contentBox.wondering .fbPostBefore, .contentBox.wondering .fbPostAfter, 
	.contentBox.wondering .bottom h3 {
		width: auto;
	}
	.contentBox.wondering .fbPostBefore, .contentBox.wondering .fbPostAfter {
		text-align: center;
		padding: 0;
	}
	.contentBox.wondering .bottom h3, .contentBox.wondering .bottom .uiButtonNew {
		float: none;
	}
	.contentBox.wondering .bottom h3 {
		text-align: center;
	}
	.contentBox.wondering .bottom .uiButtonNew {
		display: block;
		margin: 30px auto 0;
	}
	.seminar.bannerContainer .bannerTitle{
		font-size: 26px;
		line-height: 43px;
		margin-left: -20px;
		top: 0;
	}
	.seminar.bannerContainer .bannerTitle > p, .seminar.bannerContainer .bannerTitle > p:first-child {
		width: 555px;
	}
	.seminar.bannerContainer .bannerTitle span {
		font-size: 26px;
	}
	.seminar.bannerContainer .bannerBottomHolder {
		bottom: 20px;
	}
	.upcomingSeminars li {
		margin-bottom: 25px;
	}
	.upcomingSeminars li:last-child {
		margin-bottom: 0;
	}
	.videosHolder .player, .watchSeminar .videosListHolder {
		float: none;
		width: auto;
	}
	.watchSeminar .videosListHolder {
		border: 1px solid #ebecec;
		border-radius: 0 0 4px 4px;
	}
	.learnBlog .postsList li {
		margin-bottom: 28px;
	}
	.whyCB .thinking {
		display: none;
	}
	.contentBox.whyCB .features {
		margin-left: 0;
	}
	.bannerContainer .bannerBottomBlack h3 {
		font-size: 18px;
		line-height: 1.3em;
	}
	.bannerContainer .bannerBottomBlack p {
		font-size: 14px;
		margin-bottom: 10px;
	}
	.contentBox.why-blast > img {
		display: none
	}
	.why-blast h2, .why-blast p, .why-blast ul {
		padding-left: 0
	}
	img.book {
		margin: 30px auto 0;
		display: block;
		float: none;
		display: none;
	}
	img.book.mobile {
		display: block;
	}
	.greatest {
		width: auto;
	}
	.greatest h1 {
		font-size: 36px;
	}
	#getfreebook [type="submit"] {
		margin: 10px auto 0;
		display: block;
	}
}

@media (max-width: 767px) {
	body {
		padding: 0;
	}
	h1 {
		font-size: 30px;
	}
	h2 {
		font-size: 25px;
	}
	h5 {
		font-size: 15px;
	}
	.stepBox .stepTitle h2 {
		line-height: 1em;
	}
	.stepBox.step1 .stepTitle h2 {
		padding-top: 22px;
		padding-bottom: 23px;
	}
	.stepBox .stepTitle .titleNumber {
		height: 70px;
		width: 70px;
	}
	.stepBox .stepTitle .titleNumber span {
		font-size: 33px;
		top: 19px;
		left: 26px;
	}
	.content .contentBox {
		padding: 30px 30px 40px;
	}
	.stepBox > p {
		margin-top: 10px;
		margin-left: 30px;
	}
	.step1 .stepContent .ladyCartoon,
	.step3 .stepContent .ladyCartoon {
		float: none;
		margin-left: 0;
		text-align: center;
	}
	.submitContainer .ladyCartoon {
		float: none;
		text-align: center;
	}
	.postNumberContainer {
		float: none;
		width: 270px;
	}
	.postNumberContainer .postNumbers li {
		margin-left: 35px;
	}
	.postNumberContainer .postNumbers li:last-child {
		margin-left: 32px;
	}
	.step2 .stepContent .noteContainer {
		box-shadow: inset 2px -1px 8px -3px #000;
		-moz-box-shadow: inset 2px -1px 8px -3px #000;
		-webkit-box-shadow: inset 2px -1px 8px -3px #000;
		float: none;
		margin: 25px 0 0;
		width: 250px;
	}
	.step2 .stepContent .noteContainer > img {
		bottom: auto;
		left: 134px;
		top: -18px;
		-webkit-transform: rotate(90deg);
	}
	.step3 .stepContent .noteContainer {
		float: none;
		margin: 0 auto 25px;
	}
	.step3 .stepContent .noteContainer > img {
		bottom: -14px;
		right: 140px;
		top: auto;
		-webkit-transform: rotate(90deg);
	}
	.stepContent .btnContainer {
		float: none;
		text-align: center;
	}
	.stepContent .settings {
		float: none;
		margin-left: 0;
	}
	.stepContent .settings > li {
		float: none;
		margin: 0;
		width: auto;
	}
	.bannerContainer span.bannerInfo {
		padding-top: 20px;
	}
	.bannerContainer .bannerTitle,
	.bannerContainer.howBlastingWorks .bannerTitle {
		font-size: 28px;
		margin-bottom: 10px;
		width: 380px;
	}
	.bannerContainer .bannerInfo .bannerSubTitle {
		font-size: 17px;
		margin-bottom: 15px;
	}
	.bannerContainer.howBlastingWorks .bannerInfo .bannerSubTitle {
		width: 380px;
	}
	.bannerContainer .playButton {
		padding: 11px 25px 11px 28px;
	}
	.bannerContainer .playButton span {
		background: none;
		padding-right: 0;
	}
	.banner .agent {
		text-align: center;
	}
	.banner .info {
		background: url(/images/new-images/opac80-white.png);
		color: #333;
		position: static;
		padding-top: 10px;
		text-align: left;
	}
	.banner .info:after {
		content: '';
		display: block;
		clear: both;
	}
	.banner .info .name,
	.banner .info .other {
		float: left;
		margin-left: 10px;
	}
	.banner .info h6 {
		color: #333;
	}
	.banner h2 {
		font-size: 19px;
		padding-left: 10px;
		line-height: 1.3em;
	}
	.banner .row-fluid {
		text-align: center;
	}
	.banner .row-fluid h5 {
		padding: 0 10px;
	}
	.banner .tryDemo {
		margin: 10px 0;
	}
	.home .bannerBottom {
		background: #009C52;
	}
	.bannerBottom {
		position: static;
		display: inline-block;
	}
	.strategyContainer .media .media-body h3 {
		line-height: 1em;
	}
	.tryDemoTxt h3 {
		font-size: 19px;
		line-height: 1.2em;
	}
	.tryDemoTxt h4 {
		font-size: 15px;
	}
	.planContainer > li {
		float: none;
		margin: auto;
		width: 248px;
	}
	.planContainer > li:first-child {
		margin: auto;
	}
	.planContainer > li.popular {
		margin: 25px auto;
		width: 278px;
	}
	.planContainer .uiButtonNew {
		font-size: 24px;
		padding: 22px 0;
		width: 210px;
	}
	.planContainer .popular .uiButtonNew {
		font-size: 30px;
		padding: 22px 0;
		width: 240px;
	}
	.aboutPlans .span6,
	.quoteContainer .span6 {
		width: 100%;
		margin-left: 0;
		margin-top: 20px;
	}
	.aboutPlans .span6:first-child,
	.quoteContainer .span6:first-child {
		margin-top: 0;
	}
	.whoIsUsingCB .span4 {
		float: left;
		text-align: center;
	}
	.whoIsUsingCB .span4:first-child {
		text-align: left;
	}
	.whoIsUsingCB .span4:nth-child(3) {
		text-align: right
	}
	.listing .agentDetail,
	.listing .propertyDetail,
	.listing .suggestions {
		padding: 0 15px;
	}
	.listing .agentDetail .dl-horizontal dt {
		float: left;
	}
	.footerMap .contact div.pull-left,
	.footerMap .contact div.pull-right {
		float: none !important;
		text-align: center;
	}
	.footerMap .contact div.pull-right {
		margin-top: 15px;
	}
	.footer .nav > li {
		float: none;
	}
	.footer .nav a {
		text-align: center;
	}
	.footer .rightContent p {
		font-size: 11px;
		line-height: 1.2em;
	}
	.promoBox > div:first-child {
		width: auto;
		font-size: 12px;
	}
	.promoBox > div:last-child {
		width: 120px;
	}
	.questionWrapper .question h5 {
		max-width: 92%;
	}
	.content .testimonialsDesc p {
		padding-left: 25px;
		padding-right: 25px;
		font-size: 16px;
	}
	.content .testimonialsDesc .openQutote {
		left: -25px;
	}
	.content .testimonialsDesc .closeQutote {
		right: -25px;
	}
	.content .testimonialsDesc .openQutote,
	.content .testimonialsDesc .closeQutote {
		font-size: 90px;
	}
	.wePostContainer .wePost li a {
		font-size: 16px;
		margin-right: 15px;
	}
	.wePostContainer .wePostWeb {
		font-size: 11px;
	}
	.content .blastListingsWrapper,
	.content .whyBlastWrapper,
	.content .howitworksWrapper {
		padding-left: 30px;
		padding-right: 30px;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer {
		width: 230px;
		left: 20px;
		top: 25px;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer h3 {
		font-size: 16px;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer p {
		font-size: 12px;
	}
	.blastListingsWrapper #blast_video_container #blasting_video_btn {
		right: 35px;
		bottom: 27px;
		width: 200px;
		background-position: 160px;
		font-size: 22px;
		padding: 15px 0;
	}
	.whyBlastWrapper .whyBlastContainer img {
		width: 80px;
	}
	.whyBlastWrapper .whyBlastContainer .whyBlast li img {
		width: auto;
	}
	.whyBlastWrapper .whyBlastContainer .whyBlast {
		font-size: 11px;
	}
	.whyBlastWrapper p {
		font-size: 14px;
	}
	.content .howitworksWrapper{
		padding-bottom: 40px;
	}
	.howitworksWrapper .stepContainer,
	.howitworksWrapper .testimonialsWrapper{
		float: none;
		width: auto;
	}
	.howitworksWrapper .testimonialsWrapper {
		margin-top: 50px;
	}
	.fbPostBefore, .fbPostAfter {
		position: static;
		margin: 0 auto;
		background: transparent;
		padding: 0;
	}
	.fbPostBefore {
		width: 113px;
		margin-bottom: 25px;
	}
	.fbPostAfter {
		margin-top: 25px;
	}
	.fbPostAfter .last {
		background-position: 0 0;
		padding-top: 75px;
	}
	.fbEmbedPost .fb-post {
		min-height: 200px;
	}
	.socialMedia .icons-holder div {
		width: 29%;
	}
	.freeTrial .icons-holder [class*="item"] {
		float: none;
		margin: 0 auto 25px;
		width: 225px;
	}
	.seminar.bannerContainer .bannerTitle {
		font-size: 24px;
		line-height: 30px;
	}
	.seminar.bannerContainer .bannerTitle > p, .seminar.bannerContainer .bannerTitle > p:first-child {
		width: auto;
		font-size: 21px;
		line-height: 24px;
	}
	.seminar.bannerContainer .bannerTitle span {
		font-size: 18px;
	}
	.seminar.bannerContainer .bannerBottom {
		width: 380px;
		font-size: 16px;
	}
	.contentBox.experts .step {
		margin-left: 20px;
		width: 230px;
	}
	.contentBox.experts .result {
		width: 650px;
	}
	.tryDemo {
		font-size: 28px;
		padding: 16px 0;
		width: 175px;
	}
	.contentBox.why-experts .case {
		margin-left: 50px;
		width: 200px;
	}
	.tryDemoTxt h3 {
		font-size: 24px;
	}
	.tryDemoTxt h4 {
		font-size: 14px;
	}

	.contentBox .arrow-step,
	.contentBox .arrow-step.last {
		width: 300px;
		float: none;
		margin-right: auto;
		margin-left: auto;
		padding-right: 0;
		background: none
	}
	.greatest-form #email {
		width: 270px;
	}
	.getfreebook [type="submit"] {
		margin: 10px auto 0;
		display: block;
	}
}

@media (max-width: 767px) and (min-width: 555px) {
	.planContainer {
		display: none;
	}
	.mobilePlanContainer {
		display: block;
	}
}

@media (max-width: 570px) {
	.contentBox.wondering h3 {
		width: auto;
		float: none;
	}
}

@media (max-width: 680px) {
	.contentBox.why-experts .case {
		margin: 0 auto;
		width: 250px;
		float: none;
	}
	.contentBox.why-experts .arrowsAbove {
		margin: 40px 20px 0 20px;
	}
}

@media (max-width: 640px) and (min-width: 480px) {
	.bannerContainer .bannerTitle,
	.bannerContainer.howBlastingWorks .bannerTitle {
		font-size: 20px;
		width: 280px;
	}
	.bannerContainer .bannerInfo .bannerSubTitle {
		font-size: 14px;
	}
	.bannerContainer.howBlastingWorks .bannerInfo .bannerSubTitle {
		width: 280px;
	}
	.bannerContainer .playButton {
		padding: 6px 15px 6px 18px;
	}
	.bannerContainer .playButton span {
		font-size: 17px;
	}
	.questionWrapper .question h5 {
	max-width: 90%;
	}
	.bannerTitle {
		margin: 0;
	}
}
@media (max-width: 640px) {
	.upcomingSeminars li {
		margin-left: 0;
	}
	#getfreebook [type="submit"] {
		display: block;
		margin: 0 auto;
	}
	#getfreebook label {
		background: #73AD42;
		border-radius: 3px;
		display: block;
		height: auto;
		margin: 28px auto;
		padding: 12px 10px;
		text-align: center;
		width: 240px;
	}
	.greatest-form #email {
		margin: 0 auto 10px;
		display: block;
		width: 240px;
	}

}
@media (max-width: 628px) {
	.learnBlog .postsList li {
		margin-left: 0;
	}
	.learnBlog .postsList li:last-child {
		margin-bottom: 0;
	}
}

@media (max-width: 710px) {
	.social-experts .pull-right {
		float: none;
	}
	.social-experts .tryDemo {
		width: 250px;
		margin-top: 20px;
	}
}

@media (max-width: 767px) {
	.navigation span.pull-right {
		font-size: 13px;
		margin-top: 4px;
	}
	.bottomContacts .pull-left, .bottomContacts .pull-right {
		float: none;
		text-align: center;
	}
	.bottomContacts .pull-right {
		margin-top: 20px;
	}
	.contentBox.experts .step {
		float: none;
		margin: 0 auto;
	}
	.contentBox.experts .result {
		float: none;
		margin: 30px auto 0 auto;
		width: 100%;
	}
}
@media (max-width: 640px) {
	.content .memberInfo div:FIRST-CHILD {
		float: none;
		max-width: 250px;
		margin: auto;
	}
	.content .memberInfo .trialInfo {
		float: none;
		max-width: 100%;
		text-align: center;
		border: none;
		padding: 0;
		margin-top: 20px;
	}
	.content .memberInfo {
		padding-left: 0;
	}
	.wePostContainer .wePost li {
		display: block;
		margin-top: 7px;
	}
	.wePostContainer .wePost li:FIRST-CHILD {
		margin-top: 0;
	}
	.wePostContainer .wePost li a {
		font-size: 18px;
	}
	.wePostContainer .wePostWeb {
		font-size: 12px;
	}
	.blastListingsWrapper .keyFeatures .keyFeature {
		float: none;
		width: auto;
	}
	.blastListingsWrapper .keyFeatures .keyFeature {
		margin: 20px 25px;
	}
	.blastListingsWrapper .keyFeatures .keyFeature:FIRST-CHILD {
		margin-left: 25px;
	}
	.whyBlastWrapper .whyBlastContainer {
		text-align: center;
	}
	.whyBlastWrapper .whyBlastContainer img,
	.whyBlastWrapper .whyBlastContainer .whyBlast {
		float: none;
		margin: auto;
	}
	.whyBlastWrapper .whyBlastContainer .whyBlast {
		text-align: left;
		font-size: 10px;
		margin-top: 15px;
		width: auto;
	}
	.whyBlastWrapper .whyBlastContainer .whyBlast li {
		padding: 8px 0;
	}
	.blastListingsWrapper .composeBlastContainer {
		text-align: center;
	}
	.blastListingsWrapper .composeBlastContainer div,
	.blastListingsWrapper .composeBlastContainer a {
		float: none;
	}
	.blastListingsWrapper .composeBlastContainer a {
		margin-top: 20px;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer {
		width: 220px;
		left: auto;
		top: 20px;
		right: 20px;
		text-shadow: 0 1px #003d20;
	}
	.blastListingsWrapper #blast_video_container #blasting_video_btn {
		right: 20px;
		bottom: 20px;
		width: 145px;
		font-size: 20px;
		padding: 10px 0;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer h3 {
		font-size: 14px;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer p {
		font-size: 12px;
	}
	.navigation span.pull-right {
		font-size: 13px;
		margin-top: 4px;
	}
	.bannerContainer .bannerBottomBlack h3 {
		font-size: 16px;
		line-height: 1.3em;
	}
	.bannerContainer .bannerBottomBlack p {
		font-size: 12px;
		margin-bottom: 10px;
	}
}
@media (max-width: 480px) {
	h1 {
		font-size: 20px;
	}
	h2 {
		font-size: 15px;
	}
	.navigation span.pull-right {
		font-size: 12px;
		margin-top: 4px;
	}
	/*  THIS DOESN'T WORK
		.pageTop{
			background: url("/images/new-images/opac63.png");
			display: block;
			padding: 8px 14px 9px 13px;
		}
	*/
	.pageTop .sign li {
		float: left;
	}
	.pageTop .sign li a {
		border-left: 1px solid #cecfcf;
		color: #fff;
		display: inline-block;
		font-weight: bold;
		padding: 0 5px;
		text-decoration: none;
		line-height: 1em;
	}
	.pageTop .sign li:FIRST-CHILD a {
		border-left: none; 
	}
	.pageTop .pull-right {
		display: inline-block;
	}
	.pageTop + .logoHeader {
		border-bottom: none;
		padding-bottom: 30px;
		margin-bottom: -30px;
	}
	#header_wrapper div.phone {
		color: #666;
		display: block;
		font-size: 16px;
		font-weight: bold;
		text-shadow: 1px 0 0 #fff;
		text-align: center;
		margin-top: 33px;
	}
	#header_wrapper div.phone span {
		background: url(/images/new-images/phone-icon.png) no-repeat;
		color: #000;
		padding-left: 33px;
		padding-bottom: 3px;
		text-shadow: none;
	}
	#logo {
		width: 127px;
		margin-bottom: 8px;
		margin-top: -8px;
	}
	#user_nav {
		margin-right: 13px;
		margin-top: -8px;
	}
	#open_login .name {
		font-size: 12px;
		padding: 0 10px;
	}
	.content img[style = 'position: absolute; z-index: 1;']{
		display: none;
	}
	.showfull {
		display: none;
	}
	.sliderContainer {
		background: #f8f9f9;
		display: block;
		padding: 0 7px;
		position: relative;
	}
	.sliderContainer .sliderPrev,
	.sliderContainer .sliderNext {
		position: absolute;
		display: inline-block;
		top: 36%;
	}
	.sliderContainer .sliderPrev {
		left: 14px;
	}
	.sliderContainer .sliderNext {
		right: 14px;
	}
	.listing {
		margin-bottom: 0;
	}
	.listing .propertyInfo {
		background: #f8f9f9;
		padding-bottom: 0;
	}
	.listing .propertyInfo .valueContainer {
		border-bottom: none;
		padding-bottom: 0;
	}
	.listing .propertyInfo .valueContainer h2 {
		line-height: 1em;
		margin-top: 19px;
		font-weight: bolder;
	}
	.listing .propertyInfo .valueContainer .address {
		font-weight: 600;
		display: inline-block;
		margin-top: 5px;
	}
	.listing .map {
		background: url(/images/new-images/header-bg.png) center center #f0f2f5;
		margin: 0 -20px;
		padding: 0 20px 49px;
	}
	.listing .map #map_canvas{
		border: 1px solid #b2b2b2;
	}
	.listing  .fb_iframe_widget {
		display: none;
	}
	.listing .span8 .agentDetail .uiButtonNew {
		margin-top: 18px !important;
	}
	.listing .span4 .propertyDetail {
		display: none;
	}
	.listing .span8 .propertyDetail {
		background: url(/images/new-images/header-bg.png) center center #f0f2f5;
		border-top: 1px solid #b2b2b2;
		display: block;
		padding: 40px 20px 21px;
		margin: 0 -20px;
	}
	.listing .span8 .propertyDetail .address {
		color: #666;
		font-weight: bold;
		letter-spacing: 0;
		margin-top: 3px;
		margin-bottom: 15px;
		word-spacing: 1px;
	}
	.listing .span8 .propertyDetail td {
		font-weight: bold;
		padding: 11px 0;
	}
	.listing .span8 .propertyDetail td.title + td {
		text-align: right;
	}
	.listing .span8 .propertyDetail .note {
		color: #666;
		font-size: 12px;
		margin-top: 15px;
		margin-bottom: 10px;
	}
	.listing .span8 .propertyDetail .brokerInfo p {
		font-size: 12px;
		color: #666;
		margin: 10px 0;
	}
	.listing .notUsingCBAgent {
		background: #f8f9f9;
		border-top: 1px solid #b2b2b2;
		border-bottom: 1px solid #b2b2b2;
		padding: 46px 20px 34px;
	}
	.listing .notUsingCBAgent h5 br {
		display: block;
	}
	.listing .notUsingCBAgent h5 {
		font-size: 18px;
		margin-bottom: 15px;
		line-height: 1.3em;
	}
	.listing .notUsingCBAgent p a {
		color: #0088cc;
	}
	.listing .propertyDetail + .suggestions {
	 	padding: 40px 20px 48px 20px;
		margin-bottom: 0;
		background: url(/images/new-images/header-bg.png) center center #f0f2f5;
	}
	.listing .propertyDetail + .suggestions h3 {
		margin-bottom: 16px;
	}
	.listing .propertyDetail + .suggestions + .notUsingCBAgent + .suggestions {
		background: url(/images/new-images/header-bg.png) center center #f0f2f5;
		padding: 0 20px;
		margin-bottom: 0;
		padding-bottom: 50px;
	}
	.listing .suggestions span {
		color: #0088cc;
	}
	.listing .suggestions h4 {
		font-size: 21px;
		margin-bottom: 20px;
	}
	.listing .propertyDetail .brokerInfo p i {
		font-style: normal;
	}
	.listing .propertyInfo {
	padding-top: 0;
	}
	.listing .propertyInfo .valueContainer h2 {
		margin-top: 0;
	}
	.uiButtonNew {
		width: 243px;
	}
	.bannerContainer span.bannerInfo {
		padding-top: 10px;
	}
	.bannerContainer .bannerTitle,
	.bannerContainer.howBlastingWorks .bannerTitle {
		font-size: 14px;
		margin-bottom: 5px;
		width: 210px;
	}
	.bannerContainer .bannerInfo .bannerSubTitle {
		font-size: 10px;
		margin-bottom: 8px;
	}
	.bannerContainer.howBlastingWorks .bannerInfo .bannerSubTitle {
		width: 180px;
	}
	.bannerContainer .playButton {
		padding: 0 10px;
	}
	.bannerContainer .playButton span {
		font-size: 11px;
	}
	.content .contentBox h1 {
		line-height: 1.2em;
	}
	.stepBox .stepTitle .titleNumber {
		width: 50px;
		height: 50px;
		margin-right: 10px;
	}
	.stepBox .stepTitle .titleNumber span {
		font-size: 25px;
		top: 12px;
		left: 18px;
	}
	.stepBox .stepTitle h2 {
		padding: 5px 0;
	}
	.stepBox.step1 .stepTitle h2 {
		padding: 17px 0 18px;	
	}
	.step2 .stepContent .ladyCartoon {
		float: none;
		margin: 0;
		text-align: center;
	}
	.postNumberContainer {
		margin: auto;
		width: 243px;
	}
	.postNumberContainer .postNumbers li {
		margin-left: 31px;
	}
	.postNumberContainer .postNumbers li:last-child {
		margin-left: 25px;
	}
	.step2 .stepContent .noteContainer {
		margin: 25px auto 0;
		width: 223px;
	}
	.step2 .stepContent .noteContainer > img {
		left: 116px;
	}
	.step3 .stepContent .noteContainer {
		width: 223px;
	}
	.step3 .stepContent .noteContainer > img {
		right: 116px;
	}
	.stepContent .settings > li {
		margin-top: 10px;
	}
	.stepContent .settings > li:first-child {
		margin-top: 0;
	}
	.planContainer > li {
		width: 227px;
	}
	.planContainer > li.popular {
		width: 241px;
	}
	.planContainer .priceContainer {
		padding-left: 20px;
	}
	.planContainer .popular .priceContainer {
		margin-top: -35px;
	}
	.planContainer .popular .priceContainer .price {
		font-size: 44px;
	}
	.planContainer .popular .priceContainer .price sup {
		font-size: 20px;
	}
	.planContainer .popular .priceContainer .price sub {
		margin-left: -22px;
	}
	.planContainer .uiButtonNew {
		font-size: 22px;
		padding: 16px 0;
		width: 190px;
	}
	.planContainer .popular .uiButtonNew {
		font-size: 26px;
		padding: 14px 0;
		width: 204px;
	}
	.promoBox > div:first-child {
		width: auto;
	}
	.socialShare {
		float: none;
		margin-top: 15px;
		width: 204px;
		border-bottom: 1px solid #e1e1e1;
		padding-bottom: 15px;
	}
	.footer-stats {
		background: transparent url(/images/new-images/opac12.png) center center !important; 
		border: none !important;
		position: relative;
		padding: 28px 20px !important;
	}
	.footer-stats .topBorder {
		background: url(/images/new-images/opac30.png);
		height: 1px;
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
	}
	.footer-copyright {
		padding-top: 21px !important;
		padding-bottom: 25px !important;
	}
	.footer-copyright .span8 {
		font-size: 14px;
		width: 60%;
		float: left;
	}
	.footer-copyright .span4 {
		font-size: 16px;
		width: 40%;
		float: left;
	}
	.questionWrapper .question h5 {
		max-width: 83%;
	}
	.content .testimonialsDesc p {
		font-size: 14px;
	}
	.content .memberInfo .memberImg {
		float: left;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer {
		width: 175px;
		top : 10px;
		right: 20px;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer h3 {
		font-size: 12px;
	}
	.blastListingsWrapper #blast_video_container .whenBlastContainer p {
		font-size: 10px;
		line-height: 1.3;
		margin-top: 3px;
	}
	.blastListingsWrapper #blast_video_container #blasting_video_btn {
		right: 20px;
		bottom: 15px;
		width: 145px;
		font-size: 18px;
		padding: 8px 0;
	}
	.content .blastListingsWrapper,
	.content .whyBlastWrapper,
	.content .howitworksWrapper {
		padding-left: 12px;
		padding-right: 12px;
	}
	.blastListingsWrapper .composeBlastContainer h1 {
		font-size: 30px;
	}
	.howitworksWrapper .testimonialsWrapper .testimonialsContainer .media-object {
		float: right;
	}
	.howitworksWrapper .stepContainer .step .media-object {
		float: left;
	}
	.whyBlastWrapper .contentBoxTitle,
	.howitworksWrapper .contentBoxTitle {
		font-size: 25px;
		margin-bottom: 30px;
		padding-bottom: 30px;
	}
	.whyBlastWrapper p {
		font-size: 13px;
	}
	.whyBlastWrapper .whyBlastContainer .whyBlast li {
		line-height: 1.3;
		font-size: 11px;
	}
	.blastListingsWrapper .composeBlastContainer h1 {
		font-size: 29px;
	}
	.blastListingsWrapper .composeBlastContainer h3 {
		font-size: 15px;
	}
	.blastListingsWrapper .composeBlastContainer a {
		width: 270px;
		font-size: 25px;
		padding: 18px 0;
	}
	.bannerBottom h3 {
		font-size: 14px;
	}
	.bannerBottom p {
		font-size: 11px;
	}
	.socialMedia .icons-holder div {
		float: none;
		width: auto;
		margin-bottom: 30px;
	}
	.socialMedia .icons-holder img {
		margin-bottom: 15px;
	}
	.content .teamAfterText {
		padding: 0 55px;
	}
	.content .teamAfterText:before, .content .teamAfterText:after {
		background-size: 70%;
	}
	.content .teamAfterText:before {
		left: 0;
	}
	.content .teamAfterText:after {
		right: 0;
	}
	.contentBox.wondering .fbEmbedPost {
		width: auto;
	}
	.seminar.bannerContainer {
		padding: 0;
	}
	.seminar.bannerContainer:after {
		display: none;
	}
	.seminar.bannerContainer .bannerTitle {
		font-size: 17px;
		line-height: 21px;
		margin:0;
		top: 15px;
	}
	.seminar.bannerContainer .bannerTitle > p {
		width: 295px;
	}
	.seminar.bannerContainer .bannerBottomHolder {
		bottom: 5px;
	}
	.seminar.bannerContainer .bannerBottom {
		width: 287px;
	}
	.contentBox.why-experts .arrowsAbove {
		margin: 40px 0 20px 0;
	}
}
@media (max-width: 530px) {
	.bannerContainer .bannerBottomBlack h3 {
		font-size: 14px;
		line-height: 1.3em;
		margin-bottom: 0;
	}
	.bannerContainer .bannerBottomBlack p {
		font-size: 10px;
		margin-bottom: 10px;
		line-height: 1.1em;
	}
}
.key_features {
	width: 100%;
	margin: 0;
	border-top: solid 1px #fff;
	position: relative;
}
.key_feature {
	position: relative;
	width: 28%;
	margin-left: 9px;
	float: left;
	background-color: #e5e5e5;
	padding: 20px;
	border-radius: 5px;
	min-height: 197px
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	behavior: url(/css/PIE.htc);
}
.key_feature:first-child {
	margin-left: 0;
}
.key_feature h3 {
	font-size: 14px;
	margin: 0 0 15px 0;
	line-height: 18px;
	border: none;
	padding: 4px 0 0 65px;
	min-height: 22px;
}
.key_feature h3.image{
	background: url(/images/clientfinder-circle-image-icon.png) no-repeat;
}
.key_feature h3.commissions{
	background: url(/images/clientfinder-circle-commissions-icon.png) no-repeat;
}
.key_feature h3.maintenance{
	background: url(/images/clientfinder-circle-maintenance-icon.png) no-repeat;
}
.key_feature p {
	font-size: 14px;
	margin: 0 0 15px 0;
	line-height: 15px;
	letter-spacing: -.01em;
}
.key_feature p:last-child{
	margin-bottom: 0;
}

.uiForm .fieldname {
	color: #333;
	font-family: myriad-pro,Helvetica,Arial,Verdana,sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 12px 20px 3px;

}

.uiForm .field {
	padding: 2px 20px 10px;
	font-weight: bold;

}
.uiInput, .uiTextarea {
	background-color: #EEE !important;
	border: 1px solid #C9C9C9 !important;
	border-radius: 4px 4px 4px 4px !important;
	box-shadow: 1px 1px 3px #E5E5E5 inset !important;
	font-size: 16px !important;
	font-weight: normal !important;
	height: 24px !important;
	outline: medium none !important;
	padding: 4px 9px 0 !important;
	position: relative !important;
	z-index: 2 !important;
	width: 100%;
}

.fieldhelp {
	font-size: 12px !important;
	font-weight: normal !important;
	color: #666;
	margin-left: 10px;
}

.payment_hint {
	font-size: 12px;
	margin-left: 8px;
	font-weight: normal !important;
}

.icon {
	position: absolute;
	top: 0;
	left: 0;
	width: 50px;
	height: 50px;
	background-image: url(/images/clientfinder_feature_icons.png);
}
.icon.image {
	background-position: 0 0;
}
.icon.commissions {
	background-position: -50px 0;
}
.icon.maintenance {
	background-position: -100px 0;
}
.icon.impress {
	background-position: -150px 0;
}
.icon.sell {
	background-position: -200px 0;
}
.icon.win {
	background-position: -250px 0;
}
.icon.leila {
	background-position: 0 -50px;
}
.icon.shaun {
	background-position: -50px -50px;
}
.icon.luke {
	background-position: -100px -50px;
}
.icon.inesa {
	background-position: -150px -50px;
}

.key_feature .icon {
	top: 20px;
	left : 20px;
}

.video_btn {
	float: left;
	margin-right: 20px;
	background-image: url(/images/clientfinder_lp_btn_bgs.png);
	background-position: 0 0;
}
.video_btn:hover {
	background-position: 0 -60px;
}
.connect_fb_btn {
	float: left;
	background-image: url(/images/clientfinder_lp_btn_bgs.png);
	background-position: -90px 0;
}
.connect_fb_btn:hover {
	background-position: -90px -60px;
}
.compose_blast_btn {
	float: left;
	background-image: url(/images/clientfinder_lp_btn_bgs.png);
	background-position: -370px 0;
}
.compose_blast_btn:hover {
	background-position: -370px -60px;
}
.key_features {
	width: 987px;
	margin: 0 -40px;
	padding: 20px 6px 20px 7px;
	border-top: solid 1px #fff;
	background-color: #fff;
	background-image: -moz-linear-gradient(top, #f2f2f2, #fff); /* FF3.6 */
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f2f2f2),color-stop(1, #fff)); /* Saf4+, Chrome */
	background: linear-gradient(#f2f2f2, #fff);
	-pie-background: linear-gradient(#f2f2f2, #fff);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.key_feature {
	position: relative;
	width: 283px;
	margin-left: 9px;
	float: left;
	background-color: #e5e5e5;
	padding: 20px;
	border-radius: 5px;
	min-height: 194px;
}
.key_feature:first-child {
	margin-left: 0;
}
.key_feature h3 {
	font-size: 14px;
	margin: 0 0 15px 0;
	line-height: 18px;
	border: none;
	padding: 13px 0 13px 65px;
}
.key_feature h3.image{
	background: url(/images/clientfinder-circle-image-icon.png) no-repeat;
}
.key_feature h3.commissions{
	background: url(/images/clientfinder-circle-commissions-icon.png) no-repeat;
}
.key_feature h3.maintenance{
	background: url(/images/clientfinder-circle-maintenance-icon.png) no-repeat;
}
.key_feature p {
	font-size: 14px;
	margin: 0 0 15px 0;
	line-height: 15px;
	letter-spacing: -.01em;
}
.key_feature p:last-child{
	margin-bottom: 0;
}
.action_heading {
	font-size: 21px;
	display: block;
	width: 1000px;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-weight: 700;
	background-color: #f0f0f0;
	border-top: solid 1px #d9d9d9;
	border-bottom: solid 1px #d9d9d9;
	margin: 0 -40px!important;
	line-height: 48px;
	text-align: center;
	text-shadow: 1px 1px 0 #fff;
}
.action_heading.bottom {
	border-bottom: none;
	margin: 0 -40px -20px!important;
	padding: 20px 0;
	-moz-border-radius: 0 0 8px 8px;
	-webkit-border-radius: 0 0 8px 8px;
	border-radius: 0 0 8px 8px;
	position: relative;
	behavior: url(/css/PIE.htc);
}

.photo_upload_thumb .progress {
	background-image: -moz-linear-gradient(top, #333, #555);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#333), to(#555));
	background-image: -webkit-linear-gradient(top, #333, #555);
	background-image: -o-linear-gradient(top, #333, #555);
	background-image: linear-gradient(to bottom, #333, #555);
	margin-bottom: 0 !important;
	width: 104px;
}

.uploadifyQueueItem {
	display: none;
	width: auto !important;
}

.uploadstatus {
	font-size: .9em;
	font-weight: normal;
}

.search_bar {
	margin-bottom: 10px;
	margin-left: 0;
	margin-right: 0;
	border-top: 1px solid #B2B2B2;
	border-bottom: 1px solid #B2B2B2;
}

.search_title {
	background-color: #4D4D4D;
	background: url(/images/accent2-bg.png) repeat scroll 0 0 transparent;
	padding: 5px 10px 5px 13px;
	position: relative;
	z-index: 1;
	font-family: "myriad-pro-n4", Helvetica, Arial, Verdana, sans-serif;
	color: #333;
	font-weight: bold; 
}

#char_counter {
	color: #333;
	font-weight: bold;
	font-size: 14px;
	padding: 0 10px;
	line-height: 38px;
	background-color: #fff;
	border: solid 1px #ccc;
	width: 100px;
	text-align: center;
	z-index: 2;
	white-space: nowrap;
}
.char_number {
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-weight: 600;
	line-height: 30px;
	font-size: 24px;
}
.char_text {
	color: #009900;
	display: block;
	padding-top: 3px;
	margin-top: 3px;
	border-top: solid 1px #e5e5e5;
}
.char_text.error {
	color: #cc0000;
}

/* ----------------------------
		PAGINATION
------------------------------ */

#pagination li { border:0; margin:0 0 20px 0; padding:0; font-size:11px; list-style:none; /* savers */ float:left; }
#pagination a {
	border:1px solid #aaa;
	background-color: #f0f0f0;
	margin-right:2px;
	color: #1B1B1B;
}
#pagination .previous-off,
#pagination .next-off {
	border:solid 1px #DEDEDE;
	color:#888;
	display:block;
	float:left;
	font-weight:bold;
	margin-right:2px;
	padding-top: 6px;
	padding-right: 8px;
	padding-bottom: 6px;
	padding-left: 8px;
}
#pagination .next a,
#pagination .previous a { font-weight:bold; }
#pagination .active {
	color:#FFF;
	font-weight:bold;
	display:block;
	float:left; /* savers */
	margin-right:2px;
	background-color: #ccc;
	padding-top: 7px;
	padding-right: 8px;
	padding-bottom: 7px;
	padding-left: 8px;
}
#pagination a:link, 
#pagination a:visited {
	color:#1B1B1B;
	display:block;
	float:left;
	text-decoration:none;
	padding-top: 6px;
	padding-right: 8px;
	padding-bottom: 6px;
	padding-left: 8px;
}
#pagination a:hover {
	border:1px solid #1B1B1B;
}
.errorBorder
{
	border:1px solid #FF0000;
}

img[src*="gstatic.com/"], img[src*="googleapis.com/"]  {
	max-width: 99999px;
}


#header span.phone{
	background: url(/images/new-images/phone-icon.png) no-repeat scroll 0 0 transparent;
	color: #000;
	display: inline-block;
	height: 22px;
	padding-left: 33px;
	text-shadow: none;
	font-weight: bold;
	margin-top: 24px;
}

.whos_cb_stats_box {
	background-color: #f5f5f5;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px 4px 4px 4px;
	padding: 10px 2%;
	margin: 20px 0;
	font-size: 12px;
}

.docs-data {
	width: 600px;
	border-spacing: 0;
	border-collapse: collapse;
	background: #f6f6f6;
}

.docs-data th {
	background: #666;
	color: #eee;
}

.docs-data tr {
	margin: 0;
}

.docs-data td, .docs-data th {
	vertical-align: top;
	padding: 10px;
	border: 1px solid #aaa !important;
	margin: 0;
}
 .error {
	color: #CC0000!important;
	background-color: #ffe5e5 !important;
	border: solid 1px #ff6666 !important;
}
.uiInput.error, .uiTextarea.error {
	color: #CC0000!important;
	background-color: #ffe5e5 !important;
	border: solid 1px #ff6666 !important;
}
.uiInput.error:focus, .uiTextarea.error:focus {
	-moz-box-shadow: 0 0 10px #ff9999 !important;
	-webkit-box-shadow: 0 0 10px #ff9999 !important;
	box-shadow: 0 0 10px #ff9999 !important;
}

.uiCheckbox.error, .uiRadio.error {
	color: #CC0000 !important;
	background-color: #ffe5e5 !important; /* FF3.5 */
	background-image: -moz-linear-gradient(top, #ffe5e5, #ffe5e5) !important; /* FF3.6 */
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #ffe5e5),color-stop(1, #ffe5e5)) !important; /* Saf4+, Chrome */
	background: linear-gradient(#ffe5e5, #ffe5e5) !important;
	-pie-background: linear-gradient(#ffe5e5, #ffe5e5) !important;
	border: solid 1px #ff6666 !important;
}
.uiSelect.error {
	color: #CC0000 !important;
	background-color: #ffe5e5 !important;
	border: solid 1px #ff6666 !important;
}
.uiSelect.error:focus, .uiSelect.error:focus {
	-moz-box-shadow: 0 0 10 #ff9999 !important;
	-webkit-box-shadow: 0 0 10px #ff9999 !important;
	box-shadow: 0 0 10px #ff9999 !important;
}
.uiFormRow li.error {
	float: left !important;
	font-size: 11px !important;
	padding: 0 0 5px 240px !important;
	width: 380px !important;
	font-weight:bold !important;
	color:#CC0000 !important;
}
.uiFormRow li.error.cc {
	width: 380px !important;
	padding-left: 0 !important;
}

.footer-stats {
	border-top: solid 1px #999;
	border-top: solid 1px rgba(0,0,0,.15);
	border-bottom: solid 1px #adadad;
	border-bottom: solid 1px rgba(0,0,0,.07);
	background: transparent url(/images/bg_5.png) center center;
	padding: 0 20px;
}
.footer-stats .stat {
	font-size: 24px;
	position: relative;
	line-height: 40px;
	padding: 20px 0;
	color: #7C7C7C;
	color: rgba(130, 130, 130, .6);
	text-shadow: 0 1px 0 #c9c9c9, 0 0 0 #505050, 0 1px 0 #c9c9c9;
	vertical-align: middle;
	text-align: center;
}
.footer-stats .stat strong {
	font-size: 30px;
}
.footer-stats .stat:first-child {
	text-align: left;
}
.footer-stats .stat:last-child {
	text-align: right;
}

.footer-menus {
	padding: 30px 20px;
	border-bottom: solid 1px #a4a4a4;
	border-bottom: solid 1px rgba(0,0,0,.13);
	background: transparent url(/images/bg_4.png) center center;
}
.footer-menu {
	padding-left: 40px;
	position: relative;
	z-index: 1;
}
.footer-menu h5 {
	margin: 0;
	line-height: 30px;
}
.footer-menu a {
	float: left;
	clear: left;
	font-size: 12px;
	line-height: 14px;
	padding: 5px 0;
	color: #666;
}
.footer-menu a:hover {
	color: #333;
	text-decoration: none;
}
.footer-menu-icon {
	position: absolute;
	top: 0;
	left: 0;
	width: 30px;
	height: 30px;
	background: transparent url(/images/footer_icons.png) no-repeat;
}
.footer-menu-icon.cityblast {
	background-position: 0 0;
}
.footer-menu-icon.about {
	background-position: -30px 0;
}
.footer-menu-icon.help {
	background-position: -60px 0;
}
.footer-menu-icon.developers {
	background-position: -90px 0;
}
.footer-menu-icon.approved {
	background-position: -120px 0;
}
.footer-social {
	border-top: solid 1px #dbdbdb;
	border-top: solid 1px rgba(255,255,255,.3);
	padding: 30px 20px;
	background: transparent url(/images/bg_4.png) center center;
	color: #666;
	text-shadow: 1px 1px 0 #d9d9d9;
}
.footer-social .pull-right {
	min-width: 33%;
}
.footer-social h3 {
	margin: 0 0 10px;
	display: inline-block;
	border-bottom: solid 2px #000;
	color: #333;
}
.footer-social p {
	margin-bottom: 0;
}
.footer-social-icons {
	text-align: right;
	margin-top: 20px;
}
.footer-social-icon,
.footer-social-icon:focus,
.footer-social-icon:active,
.footer-social-icon:visited {
	display: inline-block;
	width: 40px;
	line-height: 40px;
	text-align: center;
	color: #e5e5e5;
	font-size: 24px;
	background: #4d4d4d;
	-moz-border-radius: 20px;
	-webkit-border-radius: 20px;
	border-radius: 20px 20px 20px 20px;
	-moz-box-shadow: 1px 1px 0 #e5e5e5;
	-ms-box-shadow: 1px 1px 0 #e5e5e5;
	-o-box-shadow: 1px 1px 0 #e5e5e5;
	-webkit-box-shadow: 1px 1px 0 #e5e5e5;
	box-shadow: 1px 1px 0 #e5e5e5;
	margin-left: 10px;
	text-shadow: none!important;
}
.footer-social-icon:hover {
	color: #f8f8f8;
	background: #333;
	text-decoration: none!important;
}
.footer-social-icon .icon {
	position: relative;
}
.footer-copyright {
	/* border-top: solid 1px #a4a4a4;
	border-top: solid 1px rgba(0,0,0,.13); */
	padding: 30px 20px;
	background: transparent url(/images/bg_3.png) center center;
	line-height: 60px;
	color: #999;
	font-size: 12px;
	text-shadow: 1px 1px 0 #f8f8f8;
}
.to-top {
	vertical-align: middle;
	cursor: pointer;
	margin-left: 9px;
}
.listing .span8 .contactMeWrapper {
	display: none;
}
.listing .propertyDetail + .suggestions + .notUsingCBAgent{
	display: none;
}
.footer-social h3 br{
	display: none;
}
@media (max-width: 767px) {
	.listing .span4 .agentDetail{
		display: none;
	}
	 .listing .span8 .agentDetail {
		border-top: 1px solid #d2d2d2;
	}
	.listing .span8 .agentDetail{
		display: block;
		padding: 0;
		text-align: center;
		margin-top: 41px;
		margin-bottom: 0;
		padding-bottom: 48px;
	}
	.footer-stats .stat {
		font-size: 22px;
		line-height: 30px;
		padding: 8px 0 6px;
		text-align: center!important;
		float: left;
		width: 33.3333333%;
	}
	.footer-stats .stat strong {
		font-size: 30px;
	}
	.footer-menus,
	.footer-social {
		padding-left: 20px;
		padding-right: 20px;
	}
	.footer-menu {
		margin-bottom: 20px;
	}
	.footer-menu:last-child {
		margin-bottom: 0;
	}
	.footer-menu a {
		clear: none;
		display: inline-block;
		margin-right: 20px;
	}
	.footer-menu a:last-child {
		margin-right: 0;
	}
	.footer-social h3 {
		font-size: 18px;
	}
	.footer-social p {
		font-size: 13px;
	}
	.footer-social-icons {
		text-align: left;
	}
	.footer-social-icon {
		margin-left: 0;
		margin-right: 10px;
	}
}
@media (max-width: 639px) {
	.footer-stats .stat {
		font-size: 16px;
		line-height: 22px;
		padding: 8px 0 6px;
		text-align: center!important;
		float: left;
		width: 33.3333333%;
	}
	.footer-stats .stat strong {
		font-size: 22px;
	}
	.footer-social h3 {
		font-size: 16px;
	}
	.footer-social p {
		font-size: 12px;
	}
}
@media (max-width: 480px) {
	.footer-stats .stat {
		color: #666;
		font-size: 14px;
		line-height: 20px;
		padding: 0;
		text-align: left!important;
		float: left;
		line-height: 1em;
		width: 33.3333333%;
		padding-left: 18px;
		border-left: 1px solid #b3b4b3; 
	}
	.footer-stats .stat:FIRST-CHILD{
		padding-left: 0;
		border-left: none;
		width: 25%;
	}
	.footer-stats .stat strong {
		color: #666;
		display: block;
		font-size: 18px;
		font-weight: bolder;
		margin-bottom: 4px;
		text-shadow: 0 1px 0 #fff;
	}
	.footer-menus, .footer-social {
		background: transparent url(/images/new-images/opac10.png) center center;
	}
	.footer-menus {
		padding-top: 33px;
		padding-bottom: 46px;
	}
	.footer-menu {
		padding-left: 35px;
		width: 50% !important;
		float: left !important;
		margin-bottom: 36px;
	}
	.footer-menu h5 {
		font-size: 18px;
		word-spacing: -1px;
		margin-bottom: 5px;
		line-height: 1.2em;
	}
	.footer-menu a{
		font-size: 14px;
		font-weight: 600;
		display: block;
		float: none;
		padding: 7px 0;
	}
	.footer-social {
		border: none !important;
		padding-top: 49px;
		padding-bottom: 51px;
		position: relative;
	}
	.footer-social .topBorder {
		background: url(/images/new-images/opac30.png);
		height: 1px;
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
	}
	.footer-social .span8,
	.footer-social .footer-social-icons {
		text-align: center;
	}
	.footer-social h3 {
		border-color: #696d6e;
		font-size: 18px;
		line-height: 1.1em;
		margin-bottom: 16px;
	}
	.footer-social p {
		font-size: 14px;
		font-weight: 600;
	}
	.footer-social .footer-social-icons{
		margin-bottom: 36px;
		margin-bottom: 0;
	}
	.to-top{
		margin-left: 9px;
	}
	.footer-copyright .span8{
		line-height: 1.2em;
		margin-top: 20px;
	}
}
@media (max-width: 320px){
	.sliderContainer .sliderPrev,
	.sliderContainer .sliderNext{
		top: 30%;
	}
	.sliderContainer{
		padding: 0;
	}
	.socialShare{
		width: 100%;
	}
	.listing .propertyDetail + .suggestions{
		padding-top: 0;
	}
	.listing .propertyDetail + .suggestions + .notUsingCBAgent + .suggestions{
		padding-top: 48px;
	}
	.footer-stats .stat{
		float: none;
		padding-left: 0;
		border: none;
		text-align: center !important;
		margin: 20px auto 0 !important;
	}
	.footer-stats .stat:FIRST-CHILD{
		margin-top: 0 !important;
	}
	.footer-menu {
		width: 100% !important;
	}
	.footer-copyright .span8,
	.footer-copyright .span4{
		width: 100%;
		text-align: center !important;
	}
	.footerMap{
		display: none;
	}
	.listing .agentDetail h3{
		line-height: 1.1em;
		padding-top: 48px;
	}
	.listing .propertyInfo + .notUsingCBAgent{
		display: none;
	}
	.listing .propertyDetail + .suggestions + .notUsingCBAgent{
		display: block;	
	}
	.footer-social h3{
		padding: 0 28px;
		border: none;
	}
	.footer-social h3 span{
		border-bottom: 1px solid #000607;
		display: inline-block;
	}
	.footer-social h3 span:FIRST-CHILD{
		border-bottom: 2px solid #696d6e;
	}
	.footer-social p{
		padding: 0 20px;
	}
	.footer-social h3 br{
		display: block;	
	}
	.footer-copyright .span8{
		margin-bottom: 15px;
		margin-top: 0;
	}
	.to-top{
		float: none !important;
		margin: auto;
	}
	.to-top + span{
		display: block;
		float: none;
		text-align: center;
		line-height: 2em;
	}
	.seminar.bannerContainer .bannerTitle, .seminar.bannerContainer .bannerTitle span, .seminar.bannerContainer .bannerBottom {
		font-size: 14px;
		font-weight: normal;
	}
	.seminar.bannerContainer .bannerTitle > p, .seminar.bannerContainer .bannerBottom {
		width: auto;
	}
}
@media (max-width: 240px){
	.listing .propertyInfo{
		padding-top: 0;
	}
	.listing .propertyInfo .valueContainer h2 {
		margin-top: 0;
	}
	.pageTop,
	.listing .propertyInfo,
	.listing .span8 .propertyDetail,
	.listing .map,
	.listing .notUsingCBAgent,
	.listing .propertyDetail + .suggestions,
	.listing .propertyDetail + .suggestions + .suggestions,
	.footer-stats,
	.footer-menus,
	.footer-social,
	.footer-copyright{
		padding-left: 10px !important;
		padding-right: 10px !important;
	}
	.sliderContainer .sliderPrev {
		left: 10px;
	}
	.sliderContainer .sliderNext {
		right: 10px;
	}
	.listing .span8 .propertyDetail{
		margin: 0 -10px;
	}
	.listing .map{
		margin: 0 -10px;
	}
	.listing .span8 .agentDetail{
		padding-bottom: 15px;
	}
	
	.listing .span8 .agentDetail .askQuestion{
		display: none;
	}
	.listing .span8 .contactMeWrapper{
		display: block;
		padding-bottom: 28px;
	}
	.listing .span8 .contactMeWrapper .contactMeTopBorder{
		display: none;
	}
	.listing .span8 .contactMeWrapper .closeBtn{
		margin-top: 15px;
		margin-bottom: 10px;
	}
	.listing .span8 .contactMeWrapper p{
		margin: 10px 0 20px;
		text-shadow: none;
	}
	.listing .span8 .contactMeWrapper .closeBtn a{
		display: inline-block;
		height: 19px;
		width: 19px;
		background: url("/images/new-images/close-icon.png") no-repeat;
	}
	.listing .span8 .contactMeWrapper .emailMe,
	.listing .span8 .contactMeWrapper .callMe{
		color: #fff;
		display: inline-block;
		text-decoration: none;
		background: -moz-linear-gradient(top, #19affe 0%, #039ae8 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#19affe), color-stop(100%,#039ae8));
		background: -o-linear-gradient(top, #19affe 0%,#039ae8 100%);
		background: -ms-linear-gradient(top, #19affe 0%,#039ae8 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#19affe', endColorstr='#039ae8',GradientType=0 );
		width: 145px;
		padding-top: 10px;
		padding-bottom: 10px;
		text-shadow: none;
		font-weight: bold;
		box-shadow: 0 5px 17px 0 #999;
		-webkit-box-shadow: 0 5px 17px 0 #999;
		-moz-box-shadow: 0 5px 17px 0 #999;
		-ms-box-shadow: 0 5px 17px 0 #999;
		border-radius: 2px;
		-webkit-border-radius: 2px;
		-moz-border-radius: 2px;
		-ms-border-radius: 2px;
	}
	.listing .span8 .contactMeWrapper .emailMe{
		margin-bottom: 11px;
	}
	.listing .span8 .contactMeWrapper .emailMe img{
		margin-right: 4px;
	}
	.listing .span8 .contactMeWrapper .callMe img{
		margin-right: 6px;
	}
	.listing .span8 .agentDetail:HOVER .askQuestion{
		display: block;
	}
	.listing .span8 .agentDetail:HOVER .contactMeWrapper .closeBtn{
		display: none;
	}
	.listing .span8 .agentDetail:HOVER .contactMeWrapper{
		background: url('/images/new-images/opac7.png');
		border-top: 1px solid #bbbcbc;
		border-left: 1px solid #bbbcbc;
		border-right: 1px solid #d9dada;
		border-bottom: 1px solid #dbdcdc;
		border-radius: 2px;
		-webkit-border-radius: 2px;
		-moz-border-radius: 2px;
		-ms-border-radius: 2px;
		padding: 34px 9px 16px;
		margin-top: 23px;
		box-shadow: inset 2px 2px 10px -5px #cccdcd;
		-webkit-box-shadow: inset 2px 2px 10px -5px #cccdcd;
		-moz-box-shadow: inset 2px 2px 10px -5px #cccdcd;
		-ms-box-shadow: inset 2px 2px 10px -5px #cccdcd;
		position: relative;
	}
	.listing .span8 .agentDetail:HOVER .contactMeWrapper .contactMeTopBorder{
		display: block;
		position: absolute;
		top: -11px;
		left: 45%;
	}
	.footer-social h3 {
		padding: 0 17px;
	}
	.footer-social p {
		padding: 0 10px;
	}
}
@media (min-width: 768px) {
	.row-fluid .span5th {
		width: 17.79235675%;
		*width: 17.74%;
	}
	.footer-menu h5 {
		font-size: 12px;
	}
	.footer-menu a {
		font-size: 11px;
	}
}
@media (min-width: 980px) {
	.row-fluid .span5th {
		width: 17.94%;
		*width: 17.895%;
	}
	.footer-menu h5 {
		font-size: 14px;
	}
	.footer-menu a {
		font-size: 12px;
	}
}
@media (min-width: 1200px) {
	.row-fluid .span5th {
		width: 17.94%;
		*width: 17.895%;
	}
	.footer-menu h5 {
		font-size: 18px;
	}
	.footer-menu a {
		font-size: 13px;
	}
}
@media (max-width: 1124px) {
	#we-are-hiring-tab {
		margin: 30px 0 0 -22px;
		width: 22px;
		height: 98px;
		background-image: url(../images/we-are-hiring-sm.png);
	}
}
@media (max-width: 767px) {
	#we-are-hiring-tab {
		display: none;
	}
}

.flipH {
	-moz-transform: scaleX(-1);
	-o-transform: scaleX(-1);
	-webkit-transform: scaleX(-1);
	transform: scaleX(-1);
	filter: FlipH;
	-ms-filter: "FlipH";
}
.flipV {
	-moz-transform: scaleY(-1);
	-o-transform: scaleY(-1);
	-webkit-transform: scaleY(-1);
	transform: scaleY(-1);
	filter: FlipV;
	-ms-filter: "FlipV";
}

/* Facebook Block */
.facebook-block {
	border: 1px solid #d3d6db;
	border-bottom: 2px solid #d3d6db;
	font: 14px/18px 'Helvetica Neue', Helvetica, Arial, 'lucida grande',tahoma,verdana,arial,sans-serif;
	color: #333;
	margin: 10px 0;
	background: #fff;
	max-width: 466px;
	width: 100%;
	text-align: left;
}
.facebook-block .fbb-content {
	padding: 8px 12px 12px
}
.facebook-block .fbb-content p {
	margin: 6px 0;
	text-align: left;
	letter-spacing: normal;
	font-size: 14px
}
.facebook-block .fbb-content a {
	color: #2e57aa;
	text-decoration: none;
}
.facebook-block .fbb-content a:hover {
	text-decoration: underline;
}
.facebook-block .fbb-top {
	padding: 4px 0 5px
}
.facebook-block .fbb-top > a {
	float: left;
	margin-right: 10px
}
.facebook-block .fbb-top-title-container {
	overflow: hidden;
}
.facebook-block .fbb-top-button {
	float: right;
}
.facebook-block .fbb-top-button a {
	position: relative;
	top: 11px;
	font-size: 12px;
	line-height: 22px;
	color: #4e5665;
	-webkit-border-radius: 2px;
	border-radius: 2px;
	background-clip: padding-box;
	background-color: #fff;
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#f6f7f8));
	background-image: -webkit-linear-gradient(#fff, #f6f7f8);
	background-image: -moz-linear-gradient(#fff, #f6f7f8);
	background-image: -o-linear-gradient(#fff, #f6f7f8);
	background-image: linear-gradient(#fff, #f6f7f8);
	display: block;
	border: 1px solid #cdced0;
	padding: 0 8px;
	text-shadow: 0 1px 0 #fff;
	text-decoration: none;
	font-weight: bold;
}
.facebook-block .fbb-top-button a i {
	width: 16px;
	height: 16px;
	display: inline-block;
	margin-right: 4px;
	vertical-align: middle;
	background: url('/images/fb-icon.gif') no-repeat
}
.facebook-block .fbb-top-button a:hover {
	text-decoration: none;
}
.facebook-block .fbb-top-title {
	overflow: hidden;
	display: inline-block;
	vertical-align: middle;
}
.facebook-block .fbb-top-title h5 a {
	font-weight: bold;
	text-decoration: none;
	color: #333;
}
.facebook-block .fbb-top-likes {
	color: gray;
	font-size: 11px;
}
.facebook-block .fbb-date {
	margin-top: 10px
}
.facebook-block .fbb-date a {
	color: #999;
	font-size: 11px;
	line-height: 15px;
}
.facebook-block .fbb-date > a {
	display: inline-block;
	width: 10px;
	height: 10px;
	vertical-align: middle;
	background: url('/images/globe.png') no-repeat;
	margin-left: 5px
}
.facebook-block .fbb-footer {
	padding: 0 12px;
	background: #fbfbfb;
	border-top: 1px solid #dee0e3;
	font-size: 12px;
	line-height: 16px;
}
.facebook-block .fbb-footer > a {
	color: #232937;
	display: inline-block;
	margin: 3px 0;
	outline: none;
	padding: 5px 8px;
	text-decoration: none;
	font-weight: bold;
	-webkit-border-radius: 2px;
	border-radius: 2px;
	background-clip: padding-box;
}
.facebook-block .fbb-footer a:hover {
	text-decoration: none;
	background: #f0f2f2
}
.facebook-block .fbb-footer a i {
	display: inline-block;
	width: 16px;
	height: 12px;
	margin-right: 4px;
	background: url('/images/fb-arrow.png') no-repeat;
	vertical-align: middle;
}
.facebook-block .fbb-footer a.like i {
	background: url('/images/fb-like.png') no-repeat;
	background-position: 1px -2px;
}
.facebook-block .fbb-footer a.comment i {
	background: url('/images/fb-comment.png') no-repeat;
}
.facebook-block .fbb-footer a.shares {
	float: right;
	font-weight: normal;
}
.facebook-block .fbb-footer .people-likes {
	border-top: 1px solid #dedede;
	padding: 8px 0;
	color: #6a7180;
}
.facebook-block .fbb-footer .people-likes a {
	color: #6a7180;
	font-weight: bold;
}
.facebook-block .fbb-footer .people-likes a:hover {
	background-color: transparent;
	text-decoration: underline;
}
.facebook-block .inner-content-container {
	background: #fff;
	-webkit-box-shadow: 0 0 0 1px rgba(0, 0, 0, .15) inset, 0 1px 4px rgba(0, 0, 0, .1);
	box-shadow: 0 0 0 1px rgba(0, 0, 0, .15) inset, 0 1px 4px rgba(0, 0, 0, .1);
	position: relative;
}
.facebook-block .icc-image {
	float: left;
	line-height: 1px
}
.facebook-block .icc-text {
	overflow: hidden;
	font-size: 12px;
	height: 132px;
	padding: 10px 12px 13px;
	position: relative;
}
.facebook-block .icc-text-title {
	font-family: Georgia, 'lucida grande',tahoma,verdana,arial,sans-serif;
	font-size: 18px;
	font-weight: 500;
	line-height: 22px;
	max-height: 110px;
	overflow: hidden;
	word-wrap: break-word;
	margin: 0 2px 6px 0
}
.facebook-block .icc-text-title a {
	color: #232b37;
}
.facebook-block .icc-text-title a:hover {
	color: #2e57aa;
	text-decoration: none
}
.facebook-block .icc-text-text {
	font-family: Georgia, 'lucida grande',tahoma,verdana,arial,sans-serif;
	line-height: 16px;
	max-height: 80px;
	overflow: hidden;
	font-size: 12px
}
.facebook-block .icc-text-link {
	bottom: 12px;
	left: 12px;
	position: absolute;
	right: 0;
	font-size: 11px;
	line-height: 11px;
	text-transform: uppercase;
	color: #adb2bb;
}
.facebook-block .full-link {
	bottom: 0;
	left: 0;
	position: absolute;
	right: 0;
	top: 0;
}
.facebook-block .full-link:hover {
	-webkit-box-shadow: 0 0 0 1px rgba(0, 0, 0, .15) inset, 0 1px 4px rgba(0, 0, 0, .1);
    box-shadow: 0 0 0 1px rgba(0, 0, 0, .15) inset, 0 1px 4px rgba(0, 0, 0, .1);
}
.facebook-block .fbb-footer ul {
	padding: 0 12px
}
.facebook-block .fbb-footer li {
	color: #4e5665;
	border-top: 1px solid #ebebeb;
	margin-top: 0;
	padding: 8px 0 7px;
}
.facebook-block .fbb-footer li a {
	color: #666;
	font-weight: bold;
	text-decoration: none
}
.facebook-block .fbb-footer li a:hover {
	text-decoration: underline;
}

/* Twitter Block */
.twitter-block {
	margin: 20px 0;
	-webkit-border-radius: 5px;
    border-radius: 5px;
	background-clip: padding-box;
	border: 1px solid rgb(221, 221, 221);
	border-color-top: rgb(238, 238, 238);
	border-color-bottom: rgb(187, 187, 187);
	-webkit-box-shadow: rgba(0, 0, 0, 0.14902) 0px 1px 3px;
	box-shadow: rgba(0, 0, 0, 0.14902) 0px 1px 3px;
	color: #292f33;
	font: 12px/16px "Helvetica Neue",Arial,sans-serif;
	padding: 16px 16px 8px;
	background: #fff;
	-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
    box-sizing: border-box;
    width: 100%;
    max-width: 468px;
}
.twitter-block a {
	text-decoration: none;
	color: #707070;
}
.twitter-block .tb-icon {
	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALEAAAC/CAYAAACxIz21AAAKRGlDQ1BJQ0MgUHJvZmlsZQAASA2dlndUFNcXx9/MbC+0XZYiZem9twWkLr1IlSYKy+4CS1nWZRewN0QFIoqICFYkKGLAaCgSK6JYCAgW7AEJIkoMRhEVlczGHPX3Oyf5/U7eH3c+8333nnfn3vvOGQAoASECYQ6sAEC2UCKO9PdmxsUnMPG9AAZEgAM2AHC4uaLQKL9ogK5AXzYzF3WS8V8LAuD1LYBaAK5bBIQzmX/p/+9DkSsSSwCAwtEAOx4/l4tyIcpZ+RKRTJ9EmZ6SKWMYI2MxmiDKqjJO+8Tmf/p8Yk8Z87KFPNRHlrOIl82TcRfKG/OkfJSREJSL8gT8fJRvoKyfJc0WoPwGZXo2n5MLAIYi0yV8bjrK1ihTxNGRbJTnAkCgpH3FKV+xhF+A5gkAO0e0RCxIS5cwjbkmTBtnZxYzgJ+fxZdILMI53EyOmMdk52SLOMIlAHz6ZlkUUJLVlokW2dHG2dHRwtYSLf/n9Y+bn73+GWS9/eTxMuLPnkGMni/al9gvWk4tAKwptDZbvmgpOwFoWw+A6t0vmv4+AOQLAWjt++p7GLJ5SZdIRC5WVvn5+ZYCPtdSVtDP6386fPb8e/jqPEvZeZ9rx/Thp3KkWRKmrKjcnKwcqZiZK+Jw+UyL/x7ifx34VVpf5WEeyU/li/lC9KgYdMoEwjS03UKeQCLIETIFwr/r8L8M+yoHGX6aaxRodR8BPckSKPTRAfJrD8DQyABJ3IPuQJ/7FkKMAbKbF6s99mnuUUb3/7T/YeAy9BXOFaQxZTI7MprJlYrzZIzeCZnBAhKQB3SgBrSAHjAGFsAWOAFX4Al8QRAIA9EgHiwCXJAOsoEY5IPlYA0oAiVgC9gOqsFeUAcaQBM4BtrASXAOXARXwTVwE9wDQ2AUPAOT4DWYgSAID1EhGqQGaUMGkBlkC7Egd8gXCoEioXgoGUqDhJAUWg6tg0qgcqga2g81QN9DJ6Bz0GWoH7oDDUPj0O/QOxiBKTAd1oQNYSuYBXvBwXA0vBBOgxfDS+FCeDNcBdfCR+BW+Bx8Fb4JD8HP4CkEIGSEgeggFggLYSNhSAKSioiRlUgxUonUIk1IB9KNXEeGkAnkLQaHoWGYGAuMKyYAMx/DxSzGrMSUYqoxhzCtmC7MdcwwZhLzEUvFamDNsC7YQGwcNg2bjy3CVmLrsS3YC9ib2FHsaxwOx8AZ4ZxwAbh4XAZuGa4UtxvXjDuL68eN4KbweLwa3gzvhg/Dc/ASfBF+J/4I/gx+AD+Kf0MgE7QJtgQ/QgJBSFhLqCQcJpwmDBDGCDNEBaIB0YUYRuQRlxDLiHXEDmIfcZQ4Q1IkGZHcSNGkDNIaUhWpiXSBdJ/0kkwm65KdyRFkAXk1uYp8lHyJPEx+S1GimFLYlESKlLKZcpBylnKH8pJKpRpSPakJVAl1M7WBep76kPpGjiZnKRcox5NbJVcj1yo3IPdcnihvIO8lv0h+qXyl/HH5PvkJBaKCoQJbgaOwUqFG4YTCoMKUIk3RRjFMMVuxVPGw4mXFJ0p4JUMlXyWeUqHSAaXzSiM0hKZHY9O4tHW0OtoF2igdRzeiB9Iz6CX07+i99EllJWV75RjlAuUa5VPKQwyEYcgIZGQxyhjHGLcY71Q0VbxU+CqbVJpUBlSmVeeoeqryVYtVm1Vvqr5TY6r5qmWqbVVrU3ugjlE3VY9Qz1ffo35BfWIOfY7rHO6c4jnH5tzVgDVMNSI1lmkc0OjRmNLU0vTXFGnu1DyvOaHF0PLUytCq0DqtNa5N03bXFmhXaJ/RfspUZnoxs5hVzC7mpI6GToCOVGe/Tq/OjK6R7nzdtbrNug/0SHosvVS9Cr1OvUl9bf1Q/eX6jfp3DYgGLIN0gx0G3QbThkaGsYYbDNsMnxipGgUaLTVqNLpvTDX2MF5sXGt8wwRnwjLJNNltcs0UNnUwTTetMe0zg80czQRmu836zbHmzuZC81rzQQuKhZdFnkWjxbAlwzLEcq1lm+VzK32rBKutVt1WH60drLOs66zv2SjZBNmstemw+d3W1JZrW2N7w45q52e3yq7d7oW9mT3ffo/9bQeaQ6jDBodOhw+OTo5ixybHcSd9p2SnXU6DLDornFXKuuSMdfZ2XuV80vmti6OLxOWYy2+uFq6Zroddn8w1msufWzd3xE3XjeO2323Ineme7L7PfchDx4PjUevxyFPPk+dZ7znmZeKV4XXE67m3tbfYu8V7mu3CXsE+64P4+PsU+/T6KvnO9632fein65fm1+g36e/gv8z/bAA2IDhga8BgoGYgN7AhcDLIKWhFUFcwJTgquDr4UYhpiDikIxQODQrdFnp/nsE84by2MBAWGLYt7EG4Ufji8B8jcBHhETURjyNtIpdHdkfRopKiDke9jvaOLou+N994vnR+Z4x8TGJMQ8x0rE9seexQnFXcirir8erxgvj2BHxCTEJ9wtQC3wXbF4wmOiQWJd5aaLSwYOHlReqLshadSpJP4iQdT8YmxyYfTn7PCePUcqZSAlN2pUxy2dwd3Gc8T14Fb5zvxi/nj6W6pZanPklzS9uWNp7ukV6ZPiFgC6oFLzICMvZmTGeGZR7MnM2KzWrOJmQnZ58QKgkzhV05WjkFOf0iM1GRaGixy+LtiyfFweL6XCh3YW67hI7+TPVIjaXrpcN57nk1eW/yY/KPFygWCAt6lpgu2bRkbKnf0m+XYZZxl3Uu11m+ZvnwCq8V+1dCK1NWdq7SW1W4anS1/+pDa0hrMtf8tNZ6bfnaV+ti13UUahauLhxZ77++sUiuSFw0uMF1w96NmI2Cjb2b7Dbt3PSxmFd8pcS6pLLkfSm39Mo3Nt9UfTO7OXVzb5lj2Z4tuC3CLbe2emw9VK5YvrR8ZFvottYKZkVxxavtSdsvV9pX7t1B2iHdMVQVUtW+U3/nlp3vq9Orb9Z41zTv0ti1adf0bt7ugT2ee5r2au4t2ftun2Df7f3++1trDWsrD+AO5B14XBdT1/0t69uGevX6kvoPB4UHhw5FHupqcGpoOKxxuKwRbpQ2jh9JPHLtO5/v2pssmvY3M5pLjoKj0qNPv0/+/tax4GOdx1nHm34w+GFXC62luBVqXdI62ZbeNtQe395/IuhEZ4drR8uPlj8ePKlzsuaU8qmy06TThadnzyw9M3VWdHbiXNq5kc6kznvn487f6Iro6r0QfOHSRb+L57u9us9ccrt08rLL5RNXWFfarjpebe1x6Gn5yeGnll7H3tY+p772a87XOvrn9p8e8Bg4d93n+sUbgTeu3px3s//W/Fu3BxMHh27zbj+5k3Xnxd28uzP3Vt/H3i9+oPCg8qHGw9qfTX5uHnIcOjXsM9zzKOrRvRHuyLNfcn95P1r4mPq4ckx7rOGJ7ZOT437j154ueDr6TPRsZqLoV8Vfdz03fv7Db56/9UzGTY6+EL+Y/b30pdrLg6/sX3VOhU89fJ39ema6+I3am0NvWW+738W+G5vJf49/X/XB5EPHx+CP92ezZ2f/AAOY8/wRDtFgAAAX+ElEQVR42u2dC3QUVZrHG4zMwLjSPhciYaOjw5DoTvM6K8IOObPr6p51ndYzKOI4ZlwcHUc8WfcIjB43ERXF0e2ZdWeR15QZkJCGGCDiQgTL5/BcW3EEdBY6iAFJ0MZEHkGYb+/X1I2VSz1uVd3uJN3f/5z/Safq1pd6/Pqr795b6Q6FSCQSKVNau3YtzJ07t9P4O52VHJCu64DO9ePUNK0LwNy43EuclStXWsaxM7bPiRO4fPnyHnsg/GRnChI/wDQ0NLjGwjZ+MjDPvlbLZM+X1/Pr547x/PPPw8KFC6Gurg4yff2lL64KSMwQYPa0urheszDfLmg2lvnbMm1kAPYKsh2sZpC9HqfM/nmF+NVXX7WNFY/HoVsSmF+4nLKcG8jZzJiy8bxcUHE7uzerl/Pq1LYnQewWT0UNz5OE67Fj+WC1E0uWLIEgADtB5ueiBgHDTwb2A7GKO08mMrHq9osWLXJ9U2B54RVYZM7pDWa5f3YbBelMyEDq52IEBVgmM6mC2A7m7qqJ7YDwm4nnz5/vyk11dbWyrO47E4tZlSDODsQqRycyBfHLL78MKu+QgTKxl5PtZdTCLo5sudHbIVbRkVV5vLKxvfwNp5Ji1apVvuph8Tila+JMjE5YdXjcOn5ut1dzjc7fsV47D7IQ+31zqejY2ZV6fvoomcrEdscfdJxZWadWxTix1UkSQZa9NTq183phVQ2tZXKITeWIUSYhXr9+fXq7ZcuWKRtOkylDQ/ku1UN2mZjssCrz/I6LZ3Kyw+8xkfJQfsoIrryddiaRSCQSiUQikUgkEolEIpFI0gISqZeLICYRxCQSQUwiEcQkgphEIohJJIKYRCKIc1RJ5qgwph81lpN6OcTlhnMd4LDN5FTYI8jjPE5+jc8FiPEERXrqxTWd7KAZKSxxQcMScVLMZRKxyoy2MooK2bfeYpncBfY4K+tjFncl8w3M5zL3Zx7F/BRze0aAtdg/cUE9v7gB/1bCACBsvE5nUAXT3uWm7cpVnwyfF7TMQ5Yrk903G1ijXs8bbyv7CILHa3KbQ6wS5hbVALtBXKHomYqEOcu5gZzFjCkdz8sFtdiuPOizKqa29WKS6UEQPyER74YAHOnMlTxJuEGcLh9sdqIY4fZ46w7LQObhYtRLnKz6TGRgnxDzkqxcAcTKMnEG2g+SuC79PfzpSuNOVez0BrODuFgy02kqUr8PiFExh32L+blIMtkoAMRgB3N31cR2QATIxP0kuLnAKy9u18UO4oRDJg4Lv+vdBHHK4eBSOQixstGJDEJ8rQR492YrE/MettPJThl/JKIA4rCPmlZzOFFaD4Q4UDnBL6rN9pUqygMFEO90KinYugnMRwJ03jzVxF1gcRmdSPi9PZhGE5w6fo63V6NG1wwXe721eoFYtsOYiY6dScUWfRQIeryKIEY9ZTPOPFf1CIQXiDlkgceJbU5SucMQnGNn0ab21bxeWIVDaxkbYjP30P2Wc1mCeLLR/grmbZkcRvMyTpwPUj1kl4nJjtPGxv2Oi2d4suMHzI8xd3TnBaVnJ3qHigNs63XaeVxvOzkEManXiyAmEcQkEkFMIhHEJIKYRCKISaRuhphEIpFI3a1Ro0YBmdybTRCTCWIymSAmkwliMkFMJ4FMEJPJBDGZTBDnqJPMUWFMP2osp/PTmyEeOXJkOTrXAY5EIpb/C4jLvYA8YsSIsV4mv7B9LkCcHDNmTKSnXlxTVgqUkewgsQDGLVZq9OjRrv8oim2wreT+RYXsW2+xTO4Ce5yV9dqeJZSVzDeMHTv2XOb+7PUodqxPsXXtGQHWYv/EBfX84gb8YwmMY0CQ4Bk06LS3OUbQbCzzt2XayAAsgCy1bzawRr2eN95W9hEEL7HZ8dzmkNFLWJsW1QA7QsygqFD0TEXCnOXcQM5ixpSO5+WCitvZvVm9nFdT23oxyfQgiF0/FRMzdACOdLZ9JU8SbhCnywcbOIoRbi+3bitIrCDzcDHqJU5+fSYysB+IedkT5M6TiUysuj1jxvVTMbG88HCXTQOLzDm9wSwhttvIAkJNRer3ATHetmIOt+iYn4skk40CQAx2MHdXTWwHhN9MXFpa2k+ik3iBV17crotdJk44ZOKwEEjvDoixQ+RwcKkchFjZ6ESmIGbHda1EH+DerGRiDonLycb1lV5GLez+oGy5IVhzgE7rgRAng3Zk8Xzb1JmVKsoDBTXxTqeSgq2bwNocCdB581QTd4HFZXQi4ff2YBpNsO34ud1e8V1qQKvxd6yXW6sXiGU7jJno2Jne8MViH8XvKIzqTGyUeU9ZxBjPls9VPQLhBWLg5YWq4RAnkCUBTmdvm9pX83phVQ2tZXKIzdxD91vOZQNi5slG+yuYt2VyGM3LOHHOW/WQXYYmO04bG/c7Lp7JyQ62Tz9gfoy97qBpZ7JrWeF323yddiaTCWIymSAmkwliMkFMJhPEZDJBTCYHmlgjkUgkUlZFH9FMH+xMEBPEJIKYICYRxAQxQUwiiAniXFK9YWUQz5s3LzJ37txEJq7dAw88EJk+fXqCICZxpZjDhlMqIGbwVjCnmJVn7mnTplUwgFPMdFdwuVYJ5kiWINKM/dFMyyJZfBNWmmJWBoFY07QwA7ce4eVWdb0qKirCDNx6hJebILZXDLNSlrK1JuyTZsqOkSxAnOLHasR0zcZ2+zNnzpwyBm3SDLAqiGfMmFHGoE2aASaIrSHAixfNYsmh2eyXGeTiDENcaRG30ivEDNYqEV7Byeeee05D0H0AXCXCKxjh1hD0fIdYtwIm2wBbgGwLkApgPZyrCqt9YFAWY+fNBeAuZh0+HTt9EvAWY+fNBWDROnb68hHiSpd2xczlBuhKRgIk969eMcQpmVrbYj8i5hJDyMBJLwALMJc7/V2r8kHW7A1Qni8QJ71eVNa+zOj0ZWTcVWVbGyXNNbDE8YaNbUBVJpYF2Wcmzi+QnepNiW21XgoxH3kJSwKcUFATV5mNtbE5g7uVFk41Ma4zG2tjcwbP+dLCTybOBMguYPouPVwUk4gZ8zE6kZIdnUB4sT7GITnJ0YmU7OiEMRmi45Bc3tfERk/d7HJzBldRWkhkVy0DbyblEPsdJ8aJkUyNE+PESL6NToQ9jE4kjPo4mqlnEYQOlab4blAhcX4qaMYuP8aJY5l8oMYJ5IDlTJnFCEyxRSeWnp2gGbvgD9QI09+ainpc6LzFrI7d7vhDpF4BcbafneiWxymN+j5lc0cqJ4h7P8Qkep6YICaISQQxQUwiiLMPMU4ji9PJMtPSfq4fTiuL08sy09QEMQncABafizAmQBIqIeYAi89JGBMiCYKY5BliEWAbkJMqIBYBtgE5mRcQk9TIDmC3J9X8QGwHsNuTawQxyVYMwKjkY5fRoBBPmzYtKvMIJrYjiElBoPby4E+gjp0XMAliEkHcU0Uf0qz+g52dwAxSengFM0jpQRATxI7Z1W8n0E929dsJJIgJYjtAEzjMJjMc5wdiwQkcZpMZjiOICWIvEDuC7BVgF4gdQc6pfyAlILMOMZgfkreaolYIMZgfmreaoiaIc8/1hukbewjiXulUJBIJo/E1QZw7ECfGjBkTyRJEmrE/Gl+Gfztbb8KRI0d2/rc3viaIcwDi0aNHxzArZSlba8I+cZBTbiCrzMI8pkw2JnJ6NsR48aJZLDk0m/3STIAVZxJicxaWzcZETs+FWLcCJtsAW4BsC5AKYGXFtq0giHswxG4XF+FmbcoRdFUjAZL7V68SYpkSxUpG3yDlFWJjmjmq8prhNHGvnypWDHHS60Vl9XIZdvoyNe6qsq2Nk+Ya2E1GjZz0Wk4YD8HjJwCl+ARHUBkPu+Mn/6T4REbey6nelHgDaL0U4vTIiwzIBsAJPzWx8CmZVSqul/nTMfE1EewzE2cCZBcwfZceTsYRGIm7TsxPx86UhTnEgbOxKQvz2TjKxrI1Ma4TXG7O4CpKC4nsqql+M2USYpvPKq5SlYUpGzuPToQ9jE4kjPo4mqlnEYQOlabyboCjDV5GJOz2V+ILZ5wckwXWg2P5DLGvcWK7bKXqgRonkIOUM/gGFPsHYh/BeJM67q/Mv+O7PdFmUzr4+ZqDRN6VGD1kxs719ixMf2sq6nFz5838RjQfu93xi/tnfG9HygPAKdzGpXwotvpkeAencJt8Lye689mJbnmc0hj7tppiTuE6WYhRxmcRy4Cckvn6L5TxGcQyIKfy6mu/FA1R0fPE1h26CgmIK7xcI+NT4d3+T64ilK8iIAligpggFiGO+R2RsBOONtCIBEGcNYiNr/Pq8n3O4uevYRuPEOsW3+Msfs6aThCTVWXizs6buWwwf5OS1w9KMXfezGWD+RuU8vpTfQhI9RDjt4Vajf/iMuObRP1ArFmN/xrjyRp9NBWJRCKRSCQSiZTHSumh8NE3+uhHX+8Dvsy2xRg8Xnl9Inzny9v0n63eBn6M22IMHi9WkQj/990b9d/evRH8GLfFGDyeruvhV155RWcGn9YxBpHTg3T0tTN0ZgjozjHKO1/apt/50nsQzNs64/3XXRt0ZgjozniNjY06MwR0Z7z2+8Y+3H7fVSeZwaNP4rZEoAqIXy0AFebxpqx6F7waJS7j8Z792R9AhXm8NWvWgArzeG1TrzzJDD59kghUAfG6b4CVnWTVnse7Y+U74MVc4nIe7z/vfBus3P55h62t2vN4q1evBrO5xN9FidvxeId+MQYOVYyDE7vehaMv/gbSvxtuf3wStM+a3GUZtsG2X1SMT/9OBCrQscb+YGUnWbX/uibeCnZ+8s0PYcWOfbCjtS0dB5dxiW15vF//y1tgZSeIrdrzeA0NDcAtym65eR13Z5/i5yPhyNInoWPXe9By3zg42jAHcNmh+66E49s3wvEdm9KvcdnRl+ZCy71XptseWfZ0ehkRqALiNWeBm0+D2KINj/eT5ZuBu357M+xoaYPWw8cswcA25tdmd3bs7ngT3Nz++bEutmrD461YsQLQduLrxXbich7vs7v+Go6zzNry6M1wePkzcJQBiss6/rexc1t8jcuO7doGh1/8NRyo+hEcT76fXkYEqoB49UBwsmUmtmjH4/04vhG43WRuY94OzeM9U/46OLmNQSvaqh2PV1dXB2Zzicvd1vN4rVNK4OSRNvj0zivgSPJD2P/EbdB69/fgzye++vpAT56Ag3dHYP/jt8Lh5E44MOVytk074LZEoAqIV50Hdu4Crul3q7Y83uSlfwDuuj/uhe0HvoCWL60zMbYxvzabx/vV7a+Bnds+O9Zp8+9WbXm8eDwOZnOJy93W83if/vQyOHn0MODPEwzmlkduhNYZV592rAcfvAZaqm5Iw5ve5nBb+icRqEAdKy4EK5slLrNqz+NNWvIWiOaaue59WP7+x/DBgUPp383rxG14vNm36WBlM8DiMqv2PF5NTQ2YzWW3XBRfz+Ptu/1i6Ph4B7RM/zto0R6GAwziL16MnbbdF+kyIgotv38EDjwwATr2fgi4LRGoAuK6wSC6C8AWy6224fFuWvQGiObyso7He+LW9SDaDLDVcqtteLzFixeD2VayW2fejsf75MdF0Dz7dmi+dzQ0TR4Ce24tgiPb3jh9ROeDt9PrkqxN8y9GQfNT5YDbEoEqIF42BER3wmqxzs483o+qXwPRXFbr+HpxGY83a/I6EM1htVpnZx6vuroazLaTuF7cjsf7ePJg2H3LYNjDfn5s+E+Ti2D//Olw/MAeON6yF/YvmAEfsWV8/R5jG3xNBKqAuHYoqDCPd6P2Kqgwj/fYpFdAhXk8TdNAhXm8PTdfeJIZRO+aeCE0zbgGmn75j+nXVm1wWyJQgY7XXKwzQ0B3TsNGf7dOjy5cB4HMYvB4j97cqDNDQHfGW7Bggc4MAd0ZL3nTuZXJm847yQyi/2/iKVutYz6B2xKBCgRacfj4EgbyC5eAL7NtMUYnxJoe/uGCV3Rm8GkdY/B4VVE9PHNioz7zprXgy2xbjGHKxGH8Hzdm8Gld1ce0kkgkEolEIpFIJBKJRCKRclaFW2BY0SZ4s2gzHMaf+LuX7b9bkyx2bBCHM4bHmx4qqU02M0PJ0mQT+3lfafyDs0qW7p44vHb394Ut+no8BPz71zH/hPk25n9gHmRafxHzQF8npyoWtl6uhUMPLasI/XtdVejhuvLOtnbtSepVvBkGFW2Bfx26GR5h8H7EDCZ/lF7O1mM7pzjD4k0Xl9YmD5XW7hln16ZkadOzaXhP90nmDSU1yb8RNpnA/JeShzKS+VrcFQPWIcyXM/8zM745biwoKJgQj8f7+TlP/WaubOiE1KQ+j//Pe6H/eAtCv9kEoaffgNCsRr3PzNV7Bs5afRfRlQVdtAnOY6A2C+DauRnb22bheHKCAeSxktqmfyvT9QLz+tJ4chBb92cbiJmb2ofHkzcLYb/H/E94k3A5FAR9nNFONAI9cuDAgd++//77i/r161fq51x9Z8GWpjSkM1/S0tk3nXFXR0K/fQdCi/ZA6IVPIFSdhNCCndB/7rsdT29suocIy4KGbIFKSYDTxqxsF2t4XdNglolPdEK5NPlRaXz3vZcv2Z3OpFgq2AOcBLbtLULIAiObFhkgR+zKiwEDBoy2Adjsv2KZ+PtVVVXn+zlXD21sXhv63UeQhvbJdYdCj72kFc7ZoIWqd0No2WcQWv75qZ81+2H+jtQWALiUCMuCWJkwxwnaK98FqNxjWsba28VidW0/BmLMEtKlyV3s5yYniIfFd3epvxkEfYqKiszZdDzzD5kvEWE+55xzLneDGGNt3boVnyst8HOuLlzwznuhF5ohtGQfpGF+bhuE5u9IQ5sGmHvxXnz4bTzRlQVdshUGMjB1J4D3Gv+YUfU1yDpuZxXv8ppkGYN4JwPyuBOstpmYde7EmFOmTBkiwIjZ7Rrmm43y4TLka8iQIZe6QTx+/PihfgFGNez6fPG3avd9lc62yw5CaOmnp4yvOcBs3XeWN7UTXVkSKw0Wm6HFjDv2vdMB/qTj6+VGSbHIsoOoJb/JYNznB2DmhEXIglmzZl0wYsQIKyiHMmMJ8fdGdh7mBnEkErksyPnC8uDnG1o+/FZd61ddMq/ZtQegcvOB14mu7EHcxsHETItCcCfusAfYgLjNpXOX8gHxaY8mjho16kwcUTj77LMvk6h3XX3VVVd9O+g5e2LL/icvXbn/iy7Z15yFVzS3s9N2LdGVrXp4M3zCwURQ9wr/02kFsOFPnOKW1Db9CkcaPAB8GDuFFpmvz9ixY7EWvlgFxBMnTrwo6Dlj+xRZ+3Hbi2XrW1pPKyMaWtr3fdlRTWRlc2RiE0wzw2kG2QFgwO2cIU7+0ksWHl6btI337LPPFqoAeMCAAYXxePws3ycLh9Qq67XQ7PWp9OjE75sgFG81AD4I1792cD8CzE7dWURWtkHeDA0iyBva7AFmXiUTd/jS3VMYoO9KdPLqcRbPIfN9o6amJg1hEIivu+66i1isvgEyMH6SzPTXmtten/r2/uTUjQeTU7emkk//sW3H9kMdG3AdAdxddfEmqFI1TmzWpfE/XVBa2/ToqYkP28mNZTgs5xbrlltuOX/q1KlDgoA8e/bsQhXnCzt3+MlbJl/LPIhI6kapnLFjZUG0ZGlSGx5Pvu2SgQ8y382I6COzj/fcc89ZZ5555pXGiIRngK+//vpCBlo/uto5rPSzE5vgfgbpTKtnJ5gfxfVuz06cGiveNRonPJi3MlC/NIG7B0sHBvlPrcaDXbJfX78lxfnnn1+4du3aC+kq55HST7FthreYj+DP4q3w3Z6wX1gbb9++ffAdd9xRaJrFczXCz7Y9g64sKdRDQC548MEHBw8dOvQSmdGI6upq7Mx9k84cqTs10DA+QITPRuBDQDg75zj5gZm6sbERM3B/OoWkblVpaem5BQUFf8teXh869VzxcLfsO23atMKWlpZBWH7QGST1hPLhTObzdu7cWVhVVVV49dVXpztqvIOHP4cNG1Y4adKkwoULFxa2trZi9j07yHgwiZSxWpj5LxBoHJNlxscpC42f+Lmz5zAPAMkhO1Lu6yHmJ5kJCFLvBDgWi7WzW/Phvn37Pk6ng9QrAeZPsM2bN++wkZVJpN4HMBcuI5BJvRZgApmUEwATyKQer/b29hhICtvSGSP1ODE2w+g1a9aU2sFrrEu3ozNG6rF65plniuwgxnV0hki9JSPbiTIwqcfq/wHw7v6KYvzhdAAAAABJRU5ErkJggg==);
	background-size:177px 191px;
}
.twitter-block a:hover {
	color: #66757f;
}
.twitter-block .tb-header {
	position: relative;
	min-height: 48px;
	padding: 3px 0 4px 57px;
}
.twitter-block .tb-header.tb-small {
	min-height: 36px;
	padding: 0 0 0 47px;
	margin-right: -16px;
	margin-left: -16px;
}
.twitter-block .tb-h-card a {
	font-size: 16px;
	color: #292f33;
	font-weight: bold;
}
.twitter-block .tb-small .tb-h-card a {
	font-size: 12px;
	line-height: 18px;
}
.twitter-block .tb-h-card a:hover b {
	color: #292f33;
	text-decoration: underline;
}
.twitter-block .tb-h-card a span {
	font-size: 14px;
	font-weight: normal;
	color: #707070;
}
.twitter-block .tb-small .tb-h-card a span {
	font-size: 12px;
}
.twitter-block .tb-h-card a:hover span {
	color: #66757f;
}
.twitter-block .tb-h-card a img {
	position: absolute;
	top: 0;
	left: 0;
	width: 48px;
	height: 48px;
	background: #fff;
	border-radius: 4px;
}
.twitter-block .tb-small .tb-h-card a img {
	top: 4px;
	left: 10px;
	width: 28px;
	height: 28px;
}
.twitter-block a.tb-follow-button {
	display: inline-block;
	padding: 0 5px 0 3px;
	font: bold 11px/18px 'Helvetica Neue',Arial,sans-serif;
	color: #333;
	text-decoration: none;
	text-shadow: 0 1px 0 rgba(255,255,255,0.5);
	white-space: nowrap;
	background-color: #eee;
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#dedede));
	background-image: -webkit-linear-gradient(#fff, #dedede);
	background-image: -moz-linear-gradient(#fff, #dedede);
	background-image: -o-linear-gradient(#fff, #dedede);
	background-image: linear-gradient(#fff, #dedede);
	border: 1px solid #ccc;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	background-clip: padding-box;
	position: absolute;
	top: 0;
	right: 8px;
	color: #333;
}
.twitter-block a.tb-follow-button:hover {
	color: #333;
	text-decoration: none;
	background-color: #d9d9d9;
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f8f8f8), to(#d9d9d9));
	background-image: -webkit-linear-gradient(#f8f8f8, #d9d9d9);
	background-image: -moz-linear-gradient(#f8f8f8, #d9d9d9);
	background-image: -o-linear-gradient(#f8f8f8, #d9d9d9);
	background-image: linear-gradient(#f8f8f8, #d9d9d9);
	border-color: #bbb;
}
.twitter-block a.tb-follow-button i {
	width: 16px;
	height: 16px;
	margin: 0 3px 0 0;
	background-position: -73px -160px;
	display: inline-block;
	vertical-align: middle;
}
.twitter-block .tb-content {
	font: 18px/24px Georgia,"Times New Roman",Palatino,serif;
}
.twitter-block .tb-content.tb-small {
	margin-left: -4px;
	margin-right: -4px
}
.twitter-block .tb-content p {
	margin: 0 5px 0 0;
	font-size: 18px;
	letter-spacing: normal;
	text-align: left
}
.twitter-block .tb-content p a {
	color: #0084b4;
}
.twitter-block .tb-content p a:hover {
	text-decoration: underline;
}
.twitter-block .tb-dateline {
	margin-top: 6px;
	font: 12px/16px "Helvetica Neue",Arial,sans-serif;
}
.twitter-block .tb-dateline a:hover {
	text-decoration: underline;
	color: #0084b4;
}
.twitter-block .tb-footer {
	margin-top: 5px;
	padding-top: 11px;
	border-top: 1px solid #e8e8e8;
	min-height: 16px;
	overflow: hidden;
}
.twitter-block .tb-footer > a {
	float: left;
	text-transform: uppercase;
	font-size: 10px;
	margin-right: 4px
}
.twitter-block .tb-footer > a:hover {
	text-decoration: underline;
	color: #707070;
}
.twitter-block .tb-footer > a strong {
	font-size: 12px
}
.twitter-block .tb-tweet-actions {
	float: right;
	margin-bottom: 4px;
}
.twitter-block .tb-tweet-actions li {
	float: left;
}
.twitter-block .tb-tweet-actions li i {
	margin: 1px 0 0 8px;
	overflow: hidden;
	background-color: #999;
}
.twitter-block .tb-tweet-actions li a:hover i {
	background-color: #0084b4;
}
.twitter-block .tb-tweet-actions li i.tb-ic-reply {
	width: 19px;
	height: 15px;
	background-position: 0 -87px;
	display: inline-block;
}
.twitter-block .tb-tweet-actions li i.tb-ic-retweet {
	width: 22px;
	height: 15px;
	background-position: -31px -87px;
	display: inline-block;
}
.twitter-block .tb-tweet-actions li i.tb-ic-fav {
	width: 16px;
	height: 15px;
	background-position: -60px -87px;
	display: inline-block;
}
.twitter-block .tb-tweet-actions li b {
	position: absolute;
	top: 0;
	left: 0;
	clip: rect(0,0,0,0);
}
.twitter-block .tb-media {
	margin: -16px -16px 8px;
	line-height: 1px
}
.twitter-block .tb-media img {
	width: 100%;
	-webkit-border-radius: 5px 5px 0 0;
	border-radius: 5px 5px 0 0;
	background-clip: padding-box;
}

.like-bar {
    background: url("/images/bg_3.png") repeat scroll center center rgba(0, 0, 0, 0);
    border-radius: 5px;
    line-height: 24px;
    margin: 50px 0;
    padding: 8px 0;
    text-align: center;
}
		.clearfix:before,
		.clearfix:after {
			content: " ";
			display: table
		}
		.clearfix:after {
			clear: both
		}
		.cb-widget {
			min-width: 300px;
			background: #fff;
		}
		.cb-widget .widget-top {
			padding: 19px 19px 13px 22px
		}
		.cb-widget .widget-top .left-col {
			width: 70%;
			float: left
		}
		.cb-widget .widget-top .right-col {
			float: right;
			width: 16%;
			margin-top: 3px
		}
		.cb-widget .widget-top span {
			display: block
		}
		.cb-widget .price {
			font-weight: bold;
			font-size: 26px
		}
		.cb-widget .address {
			color: #666;
			font-weight: bold;
			font-size: 16px
		}
		.cb-widget .image img {
			margin: 0 auto;
			display: block
		}
		.cb-widget img {
			max-width: 100%;
			height: auto
		}
		.cb-widget .widget-bottom {
			padding: 17px 17px 19px 22px
		}
		.cb-widget .more a{
			float: right;
			font-size: 16px;
			color: #fff;
			background: #939292;
			border-radius: 4px;
			text-decoration: none;
			margin-top: 22px;
			padding: 10px 14px
		}
	</style>
	<script>
		function c(p) {console.log(p)}
		window.onload = function() {
			var widget = document.getElementById('cb-widget');
			window.parent.postMessage('cityblast_listing_message|<?= $this->block_id ?>|' + widget.offsetHeight, "*");
		}
	</script>
	<base target="_top">
</head>
<body>
	<div class="cb-widget" id="cb-widget">
		<div class="widget-top clearfix">
			<div class="left-col">
				<span class="price">$<?= number_format($this->listing->price, 0, '.', ',') ?></span>
				<span class="address"><?= $this->listing->street ?></span>
			</div>
			<div class="right-col">
				<img src="<?= $this->profilePic('small', $this->member) ?>" alt="<?= $this->member->first_name ?> <?= $this->member->last_name ?>">
			</div>
		</div>
		<? if (!empty($this->listing->image)) : ?>
		<div class="image"><img src="<?= $this->listing->image ?>" alt="<?= $this->listing->street ?>"></div>
		<? endif; ?>
		<div class="widget-bottom">
			<p class="text"><?= $this->listing->message ?></p>
			<div class="more clearfix"><a href="/listing/view/<?= $this->listing->id ?>" title="Show more">Show more</a></div>
		</div>
	</div>
</body>
</html>