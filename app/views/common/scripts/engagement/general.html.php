<?php
$pagingParams = array('extraParams' => array(
	'group_by' => $this->group_by,
	'sort_by' => $this->sort_by,
	'sort_dir' => $this->sort_dir,
	'date_from' => $this->date_from,
	'date_to' => $this->date_to
));

$date_title = 'Date?';
$date_format = 'Y-m-d ?';
switch ($this->group_by) {
	case 'date':
		$date_title = 'Date';
		$date_format = 'Y-m-d';
	break;
	case 'week':
		$date_title = 'Week';
		$date_format = 'Y, W';
	break;
	case 'month':
		$date_title = 'Month';
		$date_format = 'Y, M';
	break;
	case 'year':
		$date_title = 'Month';
		$date_format = 'Y';
	break;
}
?>

<div class="container_12 clearfix">

	<form method="POST" action="/engagement/general">



	<div class="grid_12" style="margin-top: 8px; margin-bottom: 10px;">
																	
		<h1 style="margin: 0px;">Engagement</h1>
		
		<div style="float: right;">
			<input name="date_from" class="uiInput" value="<?= $this->date_from?>" style="margin-right: 5px; width: 85px;">
			<div style="float:left; margin-right: 5px; margin-top: 7px;">to</div>
			<input name="date_to"  class="uiInput" value="<?=$this->date_to?>" style="margin-right: 5px; width: 85px;">
		</div>	
		
		<div style="float: right; width: 100%;">
			<div style="float: right; margin: 20px 0 0 0;">
				<select name="group_by" id="group_by" style="float: left; font-size: 22px;  margin-right: 10px;">
				<?php
					$values = array(
						'date'     => 'Group by day', 
						'week' => 'Group by week',  
						'month' => 'Group by month', 
						'year' => 'Group by year',
					);
				?>
				<?php foreach ($values as $value => $label) :?>
					<option value="<?= $value ?>"<?= ($this->group_by == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
				<?php endforeach; ?>
				</select>
				<select name="sort_by" id="sort_by" style="float: left; font-size: 22px; bold; margin-right: 10px;">
				<?php
					$values = array(
						'date'     => 'Sort by date', 
						'score' => 'Sort by score',  
						'score_perpost' => 'Sort by score weighted', 
						'posts'     => 'Sort by posts',
					);
				?>
				<?php foreach ($values as $value => $label) :?>
					<option value="<?= $value ?>"<?= ($this->sort_by == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
				<?php endforeach; ?>
				</select>
				<select name="sort_dir" id="sort_dir" style="float: left; font-size: 22px; bold; margin-right: 10px;">
				<?php
					$values = array(
						'DESC' => 'Descending', 
						'ASC' => 'Ascending'
					);
				?>
				<?php foreach ($values as $value => $label) :?>
					<option value="<?= $value ?>"<?= ($this->sort_dir == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
				<?php endforeach; ?>
				</select>
				
				<div class="clearfix" style="float:right;"><input type="submit" class="uiButton" value="Update"/></div>	
			</div>
		</div>

	</div>
	</form>

	<table style="width: 100%;" class="uiDataTable lineheight">
		<tr>
			<th><?=$date_title?></th>
			<th>Clicks</th>
			<th>Likes</th>
			<th>Comments</th>
			<th>Shares</th>
			<th>Score</th>
			<th>Score Weighted</th>
			<th>Posts</th>
		</tr>
		<?php foreach ($this->paginator as $stat): ?>
		<?php
		switch ($this->group_by) {
			case 'date':
				$from_date = $to_date = $stat->date->format('Y-m-d');
				break;
			case 'week':
				$from_date = $stat->date->format('Y-m-d');
				$to_date = date('Y-m-d', strtotime('+6 day', $stat->date->getTimestamp()));
				break;
			case 'month':
				$from_date = $stat->date->format('Y-m-d');
				$to_date = date('Y-m-t', $stat->date->getTimestamp());
				break;
			case 'year':
				$from_date = $stat->date->format('Y-m-d');
				$to_date = date('Y-12-t', $stat->date->getTimestamp());
				break;
		}
		?>
		<tr>
			<td><a href="/engagement/listing?date_from=<?= $from_date ?>&date_to=<?= $to_date ?>&blast_type=CONTENT&group_by=<?=$this->group_by?>&sort_by=score_perpost&sort_dir=DESC"><?=$stat->date->format($date_format)?></a></td>
			<td style="text-align:right;"><?=number_format($stat->clicks)?></td>
			<td style="text-align:right;"><?=number_format($stat->likes)?></td>
			<td style="text-align:right;"><?=number_format($stat->comments)?></td>
			<td style="text-align:right;"><?=number_format($stat->shares)?></td>
			<td style="text-align:right;"><?=number_format($stat->score)?></td>
			<td style="text-align:right;"><?=number_format($stat->score_perpost, 3)?></td>
			<td style="text-align:right;"><a href="/engagement/listing?date_from=<?= $from_date ?>&date_to=<?= $to_date ?>&blast_type=LISTING&group_by=date&sort_by=score_perpost&sort_dir=DESC"><?=number_format($stat->posts)?></a></td>
		</tr>
		<?php endforeach; ?>
	</table>

	<div class="grid_6" style="margin-bottom: 80px; float: right;">
		<div style="float: right;" class="clearfix"><?= ($this->paginator) ? $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $pagingParams) : ''; ?></div>
	</div>
		
</div>

<script type="text/javascript">

	$('input[name=date_from], input[name=date_to]').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true
	});

</script>