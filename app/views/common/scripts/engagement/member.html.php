<?php
$pagingParams = array('extraParams' => array(
	'blast_type' => $this->blast_type,
	'group_by' => $this->group_by,
	'sort_by' => $this->sort_by,
	'sort_dir' => $this->sort_dir,
	'date_from' => $this->date_from,
	'date_to' => $this->date_to
));

$date_title = 'Date?';
$date_format = 'Y-m-d ?';
switch ($this->group_by) {
	case 'date':
		$date_title = 'Date';
		$date_format = 'Y-m-d';
	break;
	case 'week':
		$date_title = 'Week';
		$date_format = 'Y, W';
	break;
	case 'month':
		$date_title = 'Month';
		$date_format = 'Y, M';
	break;
	case 'year':
		$date_title = 'Month';
		$date_format = 'Y';
	break;
}

$member_types = MemberType::all();
?>

<div class="container_12 clearfix">

	<form method="POST" action="/engagement/member">
	<div class="grid_12" style="margin-top: 8px; margin-bottom: 10px;">
																	
		<h1 style="margin: 0px;">Member Engagement</h1>
		
		<div style="float: right;">
			<input name="date_from" class="uiInput" value="<?= $this->date_from?>" style="margin-right: 5px; width: 85px;">
			<div style="float:left; margin-right: 5px; margin-top: 7px;">to</div>
			<input name="date_to"  class="uiInput" value="<?=$this->date_to?>" style="margin-right: 5px; width: 85px;">
		</div>	
		
		<div style="float: right; width: 100%;">
			<div style="float: right; margin: 20px 0 0 0;">
				<select name="type_id" id="type_id" style="float: left; font-size: 22px;  margin-right: 10px;">
				<?php
					$values = array('' => 'Any type');
					if (!empty($member_types)) {
						foreach ($member_types as $type) {
							$values[$type->id] = $type->name;
						}
					}
				?>
				<?php foreach ($values as $value => $label) :?>
					<option value="<?= $value ?>"<?= ($this->type_id == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
				<?php endforeach; ?>
				</select>

				<select name="group_by" id="group_by" style="float: left; font-size: 22px;  margin-right: 10px;">
				<?php
					$values = array(
						'date'     => 'Group by day', 
						'week' => 'Group by week',  
						'month' => 'Group by month', 
						'year' => 'Group by year',
					);
				?>
				<?php foreach ($values as $value => $label) :?>
					<option value="<?= $value ?>"<?= ($this->group_by == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
				<?php endforeach; ?>
				</select>
				<select name="sort_by" id="sort_by" style="float: left; font-size: 22px; bold; margin-right: 10px;">
				<?php
					$values = array(
						'date'     => 'Sort by date', 
						'score' => 'Sort by score',  
						'score_perpost' => 'Sort by score weighted', 
						'posts'     => 'Sort by posts',
					);
				?>
				<?php foreach ($values as $value => $label) :?>
					<option value="<?= $value ?>"<?= ($this->sort_by == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
				<?php endforeach; ?>
				</select>
				<select name="sort_dir" id="sort_dir" style="float: left; font-size: 22px; bold; margin-right: 10px;">
				<?php
					$values = array(
						'DESC' => 'Descending', 
						'ASC' => 'Ascending'
					);
				?>
				<?php foreach ($values as $value => $label) :?>
					<option value="<?= $value ?>"<?= ($this->sort_dir == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
				<?php endforeach; ?>
				</select>
				
				<div class="clearfix" style="float:right;"><input type="submit" class="uiButton" value="Update"/></div>
			</div>
		</div>
	</div>
	</form>
	
	<table style="width: 100%;" class="uiDataTable lineheight">
		<tr>
			<th><?=$date_title?></th>
			<th>Member</th>
			<th>Type</th>
			<th>Clicks</th>
			<th>Likes</th>
			<th>Comments</th>
			<th>Shares</th>
			<th>Score</th>
			<th>Score Weighted</th>
			<th>Posts</th>
		</tr>
		<?php foreach ($this->paginator as $stat): ?>
		<tr>
			<td><?=$stat->date->format($date_format)?></td>
			<td>
				<a href="/admin/member/<?=$stat->member_id?>"><?=$stat->first_name . ' ' . $stat->last_name?></a>
				(<a href="/admin/member/<?= $stat->member_id?>"><?= $stat->member_id?></a>)
			</td>
			<td style="text-align:right;"><?=$stat->type_name?></td>
			<td style="text-align:right;"><?=number_format($stat->clicks)?></td>
			<td style="text-align:right;"><?=number_format($stat->likes)?></td>
			<td style="text-align:right;"><?=number_format($stat->comments)?></td>
			<td style="text-align:right;"><?=number_format($stat->shares)?></td>
			<td style="text-align:right;"><?=number_format($stat->score)?></td>
			<td style="text-align:right;"><?=number_format($stat->score_perpost, 3)?></td>
			<td style="text-align:right;"><?=number_format($stat->posts)?></td>
		</tr>
		<?php endforeach; ?>
	</table>

	<div class="grid_6" style="margin-bottom: 80px; float: right;">
		<div style="float: right;" class="clearfix"><?= ($this->paginator) ? $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $pagingParams) : ''; ?></div>
	</div>
		
</div>

<script type="text/javascript">

	$('input[name=date_from], input[name=date_to]').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true
	});

</script>