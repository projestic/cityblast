
	<link rel="stylesheet" type="text/css" href="/css/facepile.css" />
	<div class="mvs">
		<span style="padding-left: 19px;" class="uiIconText">
			<i class="img sp_favicon sx_favicon_fav" style="top: 0px;"></i>
			<span class="fsl">
				<span>
					<a href="https://www.facebook.com/<? echo $this->member_list[0]->uid; ?>" target="_blank"><? echo $this->member_list[0]->full_name; ?></a>, 
					<a href="https://www.facebook.com/<? echo $this->member_list[1]->uid; ?>" target="_blank"><? echo $this->member_list[1]->full_name; ?></a> 
					and <a href="https://www.facebook.com/<? echo $this->member_list[2]->uid; ?>" target="_blank"><? echo $this->member_list[2]->full_name; ?></a>
					use <a href="<?=APP_URL;?>" target="_blank"><?=COMPANY_WEBSITE;?></a>.
				</span>
			</span>
		</span>
	</div>
	
	<div class="uiFacepile uiFacepileLarge">
		<ul class="uiList uiFacepileList _4ki clearfix">
			<? foreach ($this->member_list as $m) {?>
			<li class="uiFacepileItem uiListItem" style="margin: 0 0 4px 4px; float: left;">
				<a class="link" title="<?=$m->full_name?>" href="https://facebook.com/<?=$m->uid?>" data-jsid="anchor" target="_blank">
					<img class="_s0 _rw img" src="https://graph.facebook.com/<?=$m->uid?>/picture?return_ssl_resources=1" alt="<?=$m->full_name;?>" data-jsid="img" />
				</a>
			</li>
			<? } ?>
		</ul>
	</div>

