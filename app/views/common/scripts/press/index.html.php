<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>

<div class="contentBox">
		

	<div class="contentBoxTitle">
		<h1>Press Releases</h1>
	</div>

	
	<div class="clearfix" style="float: right; font-size: 10px; font-weight: normal; color: #888">January 13th, 2014</div>
	<div class="clearfix"></div>
	<h5 style="margin-top: 10px;">Record breaking $55 million listing confirms CityBlast as leading website for Real Estate Agents in North America.</h5>
	<p>CityBlast has cemented its status as the largest social media site in the world for real estate agents with a record-breaking $55 million property listing. The ultra luxurious property is the 
		most expensive to ever be Blasted through CityBlast, marking an exciting milestone for the social media listing service. <a href="/press/fiftyfivemillion">Read more</a>.</p>
	
		
	<div class="clearfix" style="float: right; font-size: 10px; font-weight: normal; color: #888">December 10th, 2013</div>
	<div class="clearfix"></div>
	<h5 style="margin-top: 10px;">CityBlast experiences explosive 43% growth in engagement on Facebook with Story Bumping.</h5>	
	<p>Facebook's newest update to its algorithm "puts a greater emphasis on content quality" putting CityBlast updates in the spotlight. <a href="/press/storybump">Read more</a>.</p>	


	<div class="clearfix" style="float: right; font-size: 10px; font-weight: normal; color: #888">November 8th, 2012</div>
	<div class="clearfix"></div>
	<h5 style="margin-top: 10px;">CityBlast Social Media Listing Service Secures Investment from Leading Real Estate Solutions Provider.</h5>
	<p>CityBlast, North American’s leading social media marketing agency for Real Estate Agents and Brokerages has just confirmed 
		an initial round of funding from Lone Wolf Real Estate Technologies of Cambridge, Ontario. <a href="/press/lonewolf">Read more</a>.</p>
	
	
	<?/*

	<div class="clearfix" style="float: right; font-size: 10px; font-weight: normal;">November 8th, 2012</div>
	<div class="clearfix"></div>
	<h3 style="margin-top: 10px;">Lone Wolf announces their investment in CityBlast.</h3>	
	<p>Facebook's newest update to its algorithm "puts a greater emphasis on content quality" putting CityBlast updates in the spotlight. <a href="/press/lonewolf">Read more</a>.</p>	
	
	*/ ?>	
	
</div>