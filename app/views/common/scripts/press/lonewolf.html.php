<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>

<div class="contentBox">
		
	<div class="contentBoxTitle">
		<h1><?=COMPANY_NAME;?> Social Media Listing Service Secures Investment from Leading Real Estate Solutions Provider</h1>
	</div>
	

	<div class="strategyContainer">
		
		<p><?=COMPANY_NAME;?>, North American’s leading social media marketing agency for Real Estate Agents and Brokerages has just confirmed an initial round of funding from Lone Wolf Real Estate Technologies of Cambridge, Ontario.</p>

		<p>Studies have shown that 90% of property searches now begin online, putting <?=COMPANY_NAME;?> is in a prime position to solve the challenges that many realtors face as the industry moves towards the digital sphere. 
			The new round of investment from Lone Wolf, who are looking to enter into the agent-centric real estate space, will add further tools and offerings to agents, as well as building 
			client bases and ensuring profitability in this expanding online market.</p>
		
		<p>Shaun Nilsson, co-founder of <a href="/index/faq" title="FAQ"><?=COMPANY_NAME;?></a>, says, "In addition to working capital, we plan on reaching out to the 9,000 brokerages and 250,000 agents that already call Lone Wolf a trusted partner, 
			in order to help those agents and brokers maximize their earning potential through Facebook, Twitter and LinkedIn as well. Lone Wolf’s commitment to creating industry-leading software and their 
			celebrated service makes them a natural fit for CityBlast. We are excited about taking this next step together."</p>
		
		<p><?=COMPANY_NAME;?> provides unique services for agents to share real estate content and online real estate listings with social media audiences, using platforms like Facebook, Twitter and LinkedIn, 
			and has several pending patents on their revolutionary marketing practices. The robust offerings by Lone Wolf and their Complete Enterprise Solution 
			form the ideal digital solution for leading brokerages across North America.</p>

		<p>Lorne C. Wallace, CEO of Lone Wolf, said, "<?=COMPANY_NAME;?> solves the challenge many agents face today: being limited for time in making the efforts required 
			for an effective social media marketing strategy. Lone Wolf will be supporting <?=COMPANY_NAME;?> as it helps agents engage clients across different platforms, 
			in keeping with the current trends we are seeing across the web."</p>

		<p><?=COMPANY_NAME;?> users benefit from a significant marketing edge over their competitors, and this will be made even sharper and more pronounced by the investment from Lone Wolf. The marketing strategies integrate everything 
			from engaging content and social media technology to cutting-edge web 2.0 and know-how from the real estate gurus at <?=COMPANY_NAME;?>.</p>

		<p>To find out more visit <a href="<?=APP_URL;?>"><?=APP_URL;?></a></p>

		<h3>About CityBlast</h3>
		<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is the largest social media website for real estate agents in the world. Currently, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reaches over 3.5 Million potential new homebuyers every day through Facebook, Twitter and LinkedIn. 
		<a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> has helped thousands of agents and brokerages across North America to improve their social media presence by revamping their social media accounts with consistent and expert real estate content.</p> 

		<h3>Contact</h3>
		<p>Grace Carroll, Media Relations<br/>
			642 King Street West<br/>
			Toronto, ON M5V 1M7<br/>
			Office: 1-888-712-7888<br/>
			info@cityblast.com<br/>
		</p>	
	
	</div>	
	
			
		
</div>