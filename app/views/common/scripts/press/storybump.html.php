<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>

<div class="contentBox">
		
	<div class="contentBoxTitle">
		<h1>CityBlast experiences EXPLOSIVE 43% growth in engagement on Facebook with Story Bumping.</h1>
	</div>
	

	<div class="strategyContainer">

		<p><i>Facebook's newest update to its algorithm "puts a greater emphasis on content quality" putting CityBlast updates in the spotlight.</i></p>
		


		<p><b>Toronto ON, -</b> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?>'s</a> engagement recently experienced a huge jump across the board thanks to 
			Facebook's newest update to its algorithm. The Story Bumping algorithm now "puts a greater 
		emphasis on content quality" meaning <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?>'s</a> top-quality-content status updates are now in the spotlight of the newsfeed.</p>

		
		
		<p>Just as with Google, Facebook changes their Newsfeed algorithm from time to time. Edge Rank is out and Story Bumping is now in, and Facebook's new emphasis on content 
		has allowed <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> to reap the rewards. The social media app's real estate-focused status updates -- with links to articles from 
		top publications such as The Globe & Mail and The New York Times - are exactly what Facebook now wants.</p>
		
		
		
		<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> clients experienced a massive spike in their clicks, likes, and shares this week on Facebook. Meaning that Facebook is giving more credit to the links that provide more engagement 
		from Facebook users. "Surveys show that on average people prefer links to high quality articles about current events, their favorite sports team or shared interests, [compared] to the latest 
		meme," says Facebook software engineer Varun Kacholia. The new Facebook Newsfeed algorithm is rewarding CityBlast (and its users) for the exceptionally high quality of content it is sharing.</p>


		<h3>About CityBlast</h3>
		<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is the largest social media website for real estate agents in the world. Currently, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reaches over 3.5 Million potential new homebuyers every day through Facebook, Twitter and LinkedIn. 
		<a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> has helped thousands of agents and brokerages across North America to improve their social media presence by revamping their social media accounts with consistent and expert real estate content.</p> 
		

		<h3>Contact</h3>
		<p>Grace Carroll, Media Relations<br/>
			642 King Street West<br/>
			Toronto, ON M5V 1M7<br/>
			Office: 1-888-712-7888<br/>
			info@cityblast.com<br/>
		</p>


	</div>	
	
	

			
		
</div>