<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>

<div class="contentBox">
		
	<div class="contentBoxTitle">
		<h1>Record Breaking $55 Million Listing Confirms <?=COMPANY_NAME;?> as Leading Website for Real Estate Agents in North America.</h1>
	</div>
	

	<div class="strategyContainer">
		
		<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> has cemented its status as the largest social media site in the world for real estate agents with a record-breaking $55 million property listing. The ultra luxurious property is the most expensive to ever be Blasted through <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>, marking an exciting milestone for the social media listing service.</p>
		
		<p>The Fendi-designed ultra luxurious $55 million property was listed with <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> by the Engel and Volkers USA realtor Giancarlo Cuffia. With six 
			bedrooms and 8.5 bathrooms the condo is 16,000 sq ft of sublime, sumptuous comfort. Part of the world renowned Acqualina Resort and Spa, the lavishly appointed residence couples 
			unparalleled amenities with world class service to provide a level of opulence that simply redefines the meaning of luxury.</p>
		

		<p>Described by the agent as a mansion-in-the-sky, the <a href="<?=APP_URL;?>/listing/view/19129">17749 Collings Ave, Sunny Isles Beach, Florida</a> penthouse is billed as 
			one of the world's finest residences with a pristine private white-sand beach and enviable sea views.</p>
		
		<p>Built by a stellar team of handpicked architects, interior designers and skilled craftsmen from every corner of the globe, the multi-million dollar property uses an exemplary collection of the most 
			luxurious materials throughout, from the hardwood floors to private outside terrace and via a rich palette of choices such as Shagreen Leather for the entry ways, vein cut Travetine Marble, 
			rare Havana Onyx with Kozmus Slate floors and molten glass mosaic walls. Outside, the development offers every indulgence, with two pools, cascading waterfalls, lush landscaping, oceanfront 
			dining, poolside cabanas and lounges, private cinema, children's center, state-of-the-art fitness center and Hammam Spa.</p>
		
		<p>As the largest social media website in the world for real estate agents, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is the natural choice for the record breaking property listing. 
			<a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reaches millions of potential homebuyers every day through Facebook, Twitter and Linkedin, and will blast 
			<a href="<?=APP_URL;?>/listing/view/19129">the 17749 Collings Ave</a> listing information through all <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> channels on behalf of Engel and Volkers. 
			The expert team will harness the cumulative power of its extensive network to generate maximum exposure and interest in the stunning residence.</p>
		
		<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> helps agents to stay on top of their social media marketing by offering an outsourced solution for updating their social media status with real estate listings and articles on a consistent basis. <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>'s powerful and patented Blasting tool allows for <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> agents to share their listings between their networks in order to market their properties to more people faster. Engel and Volkers USA's listing Blast will benefit from a significant marketing edge that encompasses the best systems, most engaging content, ground breaking web 2.0 and social media technology - all handled effortlessly by the <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> online real estate gurus.</p>
		  
		
		<p>Shaun Nilsson, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>'s Co-Founder and CEO said, "We are delighted that Engel and Volkers USA have chosen to Blast their $55 million property 
			listing using <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> unique sharing tool. 
			This is a new level of luxury for us and the most expensive property to use our service to date. We're very excited and honoured to have been selected and look forward to 
		wielding our substantial marketing muscles to deliver a listing edge for this sublime residence."</p> 
		
		<p>All Blasts include a short property description, several photos and details such as price and address. Realtors choosing to Blast their listings with <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> then enjoy a range of
		 benefits that are all designed to sell properties faster. Each blast reaches tens of thousands of local buyers, by advertising the listing on other local agents' Facebook, Twitter 
		 and LinkedIn accounts, thus tapping into networks of potential buyers that would otherwise be unreachable. Together, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> helps real estate professionals to secure top dollar 
		sales quickly. Realtors get an edge in each listing presentation Blasted with <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> thanks to a powerful online marketing pitch and unprecedented access to savvy social media experts.</p>
			
		<h3>About CityBlast</h3>
		<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is the largest social media website for real estate agents in the world. Currently, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reaches over 3.5 Million potential new homebuyers every day through Facebook, Twitter and LinkedIn. 
		<a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> has helped thousands of agents and brokerages across North America to improve their social media presence by revamping their social media accounts with consistent and expert real estate content.</p> 


		<h3>Contact</h3>
		<p>Grace Carroll, Media Relations<br/>
			642 King Street West<br/>
			Toronto, ON M5V 1M7<br/>
			Office: 1-888-712-7888<br/>
			info@cityblast.com<br/>
		</p>	
	
	</div>	
	
			
		
</div>