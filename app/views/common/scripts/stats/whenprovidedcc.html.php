<div class="container_12 clearfix">


  <div class="grid_12" style="margin-bottom: 10px;">

    <h1>When CC is provided</h1>
    <span style="display: inline-block; color: gray; margin-left: 10px; background-color: #f7f7f7; padding: 5px;">
      What is the time difference between when people signup and when they give us their CC
    </span>
    <div name="_delimiter" style="">&nbsp;</div>

    <table style="" class="uiDataTable">
      <tr>
        <th style='text-align: center; width: 60px;'>Days</th>
        <th style='text-align: center; width: 100px;'>Member Count</th>
        <th style='text-align: center; width: 100px;'>Member Count %</th>
      </tr>

      <?php

      $countMembersTotal = 0;
      foreach($this->statRecs as $bucketName => $countMembers)
      {
        $countMembersTotal += $countMembers;
      }

      foreach($this->statRecs as $bucketName => $countMembers)
      {
        $countMembersPercentFromTotal = number_format($countMembers * 100 / $countMembersTotal, 2);
        echo "<tr>";

        echo "<td style='border-right: 1px solid #e0e0e0;'>" . $bucketName . "</td>";
        echo "<td style='text-align: right;'>" . $countMembers . "</td>";
        echo "<td style='text-align: right;'>" . $countMembersPercentFromTotal . "</td>";

        echo "</tr>";
      }
      ?>
      <tr>
        <td></td>
        <td style="text-align: right; font-weight: bold;">Total: <?= $countMembersTotal ?></td>
        <td ></td>
      </tr>

    </table>
  </div>
</div>