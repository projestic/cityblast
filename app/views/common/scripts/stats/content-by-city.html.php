
<div class="container_12 clearfix">
	<div class="grid_12" style="padding-top: 30px;"><h1>Content Stats</h1></div>
	<div class="grid_12" style="margin-bottom: 80px;">
		<table width="100%;" class="uiDataTable lineheight">

			<tr>
				<th></th>
				<?php foreach ($this->content_new_days as $day): ?>
				<th><?php echo  $day;?></th>
				<?php endforeach; ?>
				<th>Total</th>
			</tr> 

			<?php foreach ($this->content_by_city as $data): ?>
			<tr>
				<td style="width: 25%;">
					<?php echo $data['name']; ?>
				</td>
				<?php $total = ""; ?>
				<?php foreach ($data['stats'] as $date => $count): ?>
					<td style="text-align: right;"><?php if($count) echo number_format($count); ?></td>
					<?php $total += $count; ?>
				<?php endforeach; ?>
				<td style="text-align: right;"><?php if($count) echo number_format($total); ?></td>
			</tr>
			<?php endforeach; ?>

			<tr>
				<td style="width: 25%; font-size: 14px;">
					<strong>Total</strong>
				</td>
				<?php $total = ""; ?>
				<?php foreach ($this->content_new as $date => $count): ?>
					<td style="text-align: right; font-size: 14px;"><?php echo number_format($count); ?></td>
					<?php $total += $count; ?>
				<?php endforeach; ?>
				<td style="text-align: right; font-size: 14px;"><?php echo number_format($total); ?></td>
			</tr>

		</table>
	</div>
</div>	