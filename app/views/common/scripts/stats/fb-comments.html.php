<div class="container_12 clearfix">

	<?php if(!empty($this->message)): ?>
		<div class="grid_12">
			<div class="notice">
				<h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
		</div>
	<?php endif; ?>



	<div class="grid_12" style="padding-top: 30px;">
		<h1>Facebook Comments</h1>
	</div>


	<div class="grid_12" style="padding-bottom: 80px;">

		<table style="width: 100%;" class="uiDataTable lifetimeTable">
			<thead>
			<tr>
				<th><a href="/stats/fb_comments/sort/member/by/<?php echo $this->sort == 'member' ? $this->by : 'desc';  ?>">From Member</a></th>
				<th>Member email</th>
				<th>Message</th>
				<th><a href="/stats/fb_comments/sort/likes/by/<?php echo $this->sort == 'likes' ? $this->by : 'desc';  ?>">Likes</a></th>
				<th style="min-width: 110px;"><a href="/stats/fb_comments/sort/date/by/<?php echo $this->sort == 'date' ? $this->by : 'desc';  ?>">Created Time</a></th>
			</tr>
			</thead>

			<tbody>
			<?php foreach($this->paginator as $message) { ?>
				<?php
					$post_date = $message->created_time->format('Y-m-d H:i:s');
					if($message->member_id) {
						$member_link = "<a href='/admin/member/".$message->member_id."'>".$message->from_name."</a>";
					} else {
						$member_link = $message->from_name;
					}

				?>
				<tr>
					<td style='vertical-align: top;'><?php echo $member_link ?></td>
					<td style='vertical-align: top;'><?php echo $message->email ?></td>
					<td style='vertical-align: top;'><?php echo $message->message ?></td>
					<td style='vertical-align: top;'><?php echo $message->likes ?></td>
					<td style='vertical-align: top;'><?php echo $post_date ?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		<div class="grid_12" style="margin-top: 0px;">
			<div style="margin-top: 0px; text-align: right; float: right;">
				<?php
				if (count($this->paginator) > 0) {
					$paginationParams = array('extraParams'=>array());
					echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $paginationParams);
				}
				?>
			</div>
		</div>
	</div>
</div>