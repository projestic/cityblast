<?
	$stats = $this->stats;
?>
<div class="container_12 clearfix">
	<div class="grid_12" style="padding-top: 30px;"><h1>Financial Stats</h1></div>
	<div class="grid_12" style="margin-bottom: 80px;">
		<table width="100%;" class="uiDataTable lineheight">
			<tr>
				<th colspan="3">Actual Revenue</th>
			</tr> 
			<tr>
				<td style="width: 25%;">
					<strong>This Month</strong> 
					<span style="font-style: italic; font-size: 10px; color: #b9b9b9;">(Calendar month)</span>
				</td>
				<td style="text-align: right;">$<?=number_format($stats['actual_revenue_current_month'], 2)?></td>
				<?/*<td></td>*/?>
			</tr>
			<tr>
				<td>
					<strong>This Year</strong> 
					<span style="font-style: italic; font-size: 10px; color: #b9b9b9;">(Since Jan 1st)</span>
				</td>
				<td style="text-align: right;">$<?=number_format($stats['actual_revenue_current_annual'], 2)?></td>
				<?/*<td></td>*/?>
			</tr>
		</table>

		<table width="100%;" class="uiDataTable lineheight">
			<tr>
				<th colspan="4">Current Month Revenue Projections <div style="font-size: 10px; color: #b9b9b9; font-weight: normal;">(recieved + expected)</div></th>
			</tr>
			<tr>
				<td style="width: 25%;"></td>
				<td style="width: 25%;">
					<strong>Paid Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected current calendar month revenue for paid members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>CC Trial Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected current calendar month revenue for CC trial members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>Total</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected current calendar month revenue
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<strong>Monthly billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected current calendar month revenue for members with recurring monthly billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_paid_member_month'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_cctrial_member_month'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_month'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Biannual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected current calendar month revenue for members with recurring biannual billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_paid_member_biannual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_cctrial_member_biannual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_biannual'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Annual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected current calendar month revenue for members with recurring annual billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_paid_member_annual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_cctrial_member_annual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_annual'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Total</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected current calendar month revenue
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_paid_member'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_cctrial_member'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue'], 2)?></td>
			</tr>
		</table>
		
		<table width="100%;" class="uiDataTable lineheight">
			<tr>
				<th colspan="4">30 Day Rolling Revenue Projections <div style="font-size: 10px; color: #b9b9b9; font-weight: normal;">(recieved + expected)</div></th>
			</tr>
			<tr>
				<td style="width: 25%;"></td>
				<td style="width: 25%;">
					<strong>Paid Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected 30 day revenue for paid members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>CC Trial Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected 30 day revenue for CC trial members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>Total</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected 30 day revenue
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<strong>Monthly billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected 30 day month revenue for members with recurring monthly billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_paid_member_month'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_cctrial_member_month'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_month'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Biannual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected 30 day revenue for members with recurring biannual billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_paid_member_biannual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_cctrial_member_biannual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_biannual'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Annual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected 30 day revenue for members with recurring annual billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_paid_member_annual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_cctrial_member_annual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_annual'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Total</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Projected 30 day revenue
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_paid_member'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30_cctrial_member'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_revenue_30'], 2)?></td>
			</tr>
		</table>
		
		<table width="100%;" class="uiDataTable lineheight">
			<tr>
				<th colspan="4">Monthly Average Price <div style="font-size: 10px; color: #b9b9b9; font-weight: normal;">(SUM(member.price_month)/COUNT(member))</div></th>
			</tr>
			<tr>
				<td style="width: 25%;"></td>
				<td style="width: 25%;">
					<strong>Paid Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly equivalent price for paid members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>CC Trial Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly equivalent price for CC trial members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>All</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly equivalent price 
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<strong>Monthly billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly equivalent price for members with recurring monthly billing
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_paid_member_month'], 2)?> 
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['paid_member_count_month'])?>)
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_cctrial_member_month'], 2)?>
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['cctrial_member_count_month'])?>)
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_month'], 2)?>
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['member_count_month'])?>)
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<strong>Biannual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly equivalent price for members with recurring biannual billing
					</div>
				</td>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_paid_member_biannual'], 2)?> 
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['paid_member_count_biannual'])?>)
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_cctrial_member_biannual'], 2)?>
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['cctrial_member_count_biannual'])?>)
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_biannual'], 2)?>
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['member_count_biannual'])?>)
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<strong>Annual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly equivalent price for members with recurring annual billing
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_paid_member_annual'], 2)?> 
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['paid_member_count_annual'])?>)
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_cctrial_member_annual'], 2)?>
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['cctrial_member_count_annual'])?>)
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_annual'], 2)?>
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['member_count_annual'])?>)
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<strong>All</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly equivalent price
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_paid_member'], 2)?> 
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['paid_member_count'])?>)
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price_cctrial_member'], 2)?>
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['cctrial_member_count'])?>)
					</div>
				</td>
				<td style="text-align: right;">
					<?=number_format($stats['projected_monthly_average_price'], 2)?>
					<div style="font-size: 10px; color: #b9b9b9; line-height: 10px;">
					(<?=number_format($stats['member_count'])?>)
					</div>
				</td>
			</tr>
		</table>
		
		
		<? /*
		
		Kevin, I don't understand what these numbers represent.
		
		<table width="100%;" class="uiDataTable lineheight">
			<tr>
				<th colspan="4">Monthly Average Revenue Projections <div style="font-size: 10px; color: #b9b9b9; font-weight: normal;">(monthly value of members)</div></th>
			</tr>
			<tr>
				<td style="width: 25%;"></td>
				<td style="width: 25%;">
					<strong>Paid Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly revenue projection for paid members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>CC Trial Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly revenue projection for CC trial members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>Total</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly revenue projection
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<strong>Monthly billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly revenue projection for members with recurring monthly billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_paid_member_month'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_cctrial_member_month'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_month'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Biannual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly revenue projection for members with recurring biannual billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_paid_member_biannual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_cctrial_member_biannual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_biannual'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Annual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly revenue projection for members with recurring annual billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_paid_member_annual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_cctrial_member_annual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_annual'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Total</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Average monthly revenue projection
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_paid_member'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue_cctrial_member'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_monthly_average_revenue'], 2)?></td>
			</tr>
		</table>
		*/ ?>
		
		
		<table width="100%;" class="uiDataTable lineheight">
			<tr>
				<th colspan="4">Annual Projections <div style="font-size: 10px; color: #b9b9b9; font-weight: normal;">((monthly value of members) x 12 months)</div></th>
			</tr>
			<tr>
				<td style="width: 25%;"></td>
				<td style="width: 25%;">
					<strong>Paid Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Annual revenue projection for paid members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>CC Trial Members</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Annual revenue projection for CC trial members
					</div>
				</td>
				<td style="width: 25%;">
					<strong>Total</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Annual revenue projection
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<strong>Monthly billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Annual revenue projection for members with recurring monthly billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_paid_member_month'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_cctrial_member_month'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_month'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Biannual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Annual revenue projection for members with recurring biannual billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_paid_member_biannual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_cctrial_member_biannual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_biannual'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Annual billing cycle</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Annual revenue projection for members with recurring annual billing
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_paid_member_annual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_cctrial_member_annual'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_annual'], 2)?></td>
			</tr>
			<tr>
				<td>
					<strong>Total</strong>
					<div style="font-style: italic; font-size: 10px; color: #b9b9b9; line-height: 10px;">
					Annual revenue projection
					</div>
				</td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_paid_member'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue_cctrial_member'], 2)?></td>
				<td style="text-align: right;"><?=number_format($stats['projected_annual_revenue'], 2)?></td>
			</tr>
		</table>
	</div>
</div>	