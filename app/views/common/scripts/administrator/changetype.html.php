<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">

	<div class="grid_12 member_update">

		<div class="box form clearfix">
			<div class="box_section">
				<h1>Add administrator</h1>
			</div>

			<form id="extraForm" action="/administrator/save" method="POST">

			<ul class="uiForm clearfix">
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Member:</li>
						<li class="field">
						    	<input type="hidden" name="member_id" id="member_id" value="<?=$this->member->id;?>"/>
							<b><?php echo $this->member->first_name . " " . $this->member->last_name;?></b>
						</li>
					</ul>
					<ul>
						<li class="fieldname">Set member type to:</li>
						<li class="field">
							<span class="uiSelectWrapper"><span class="uiSelectInner">
								<select name="type_id" class="uiSelect">
								<?php foreach (MemberType::all() as $type) : ?>
									<option value="<?= $type->id ?>" <?php if ($this->member->type_id == $type->id) : ?>selected="1"<?php endif; ?>><?= $type->name ?></option>;
								<?php endforeach; ?>
								</select>
							</span></span>
						</li>
					</ul>
				</li>
			</ul>

	
		</div>

		<input type="submit" name="submitted" class="uiButton large right" value="Change">
		
	</form>
	
</div>