<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">

	<div class="grid_12 member_update">

		<div class="box form clearfix">
			<div class="box_section">
				<h1>Search users</h1>
			</div>

			<form id="extraForm" action="/administrator/list_results" method="POST">

			<ul class="uiForm clearfix">
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">First name:</li>
						<li class="field">
						    	<input type="text" name="first_name" id="first_name" class="uiInput" style="width: 200px" />
						</li>
					</ul>
					<ul>
						<li class="fieldname">Last name:</li>
						<li class="field">
						    	<input type="text" name="last_name" id="last_name" class="uiInput" style="width: 200px" />
						</li>
					</ul>
					<ul>
						<li class="fieldname">Email:</li>
						<li class="field">
						    	<input type="text" name="email" id="email" class="uiInput" style="width: 200px" />
						</li>
					</ul>
				</li>
			</ul>

	
		</div>


		<input type="submit" name="submitted" class="uiButton large right" value="Search">

		
	</form>
	
</div>