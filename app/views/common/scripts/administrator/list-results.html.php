<div class="container_12 clearfix">
	
	<div class="grid_12" style="padding-top: 30px;"><h1>Results <a href="/administrator/search" class="uiButton"  style="float: right; margin-top: 0px; padding-top:0px; margin-right: 0px;">Search again</a></h1></div>
	
	<div class="grid_12" style="margin-bottom: 80px;">

		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Email</th>					
				<th>Current type</th>					

				<th style="text-align: center;">Actions</th>
			</tr>

			<? 
			
			$row=true;
			foreach($this->results as $r)
			{
				echo "<tr ";
					if($row == true)
					{						
						$row = false;
						echo "class='odd' ";	
					}
					else
					{
						$row = true;	
						echo "class='' ";	
					}		

				echo ">";
				
				echo "<td style='vertical-align: top;'>". $r->id."</td>";
				echo "<td style='vertical-align: top;'>". $r->first_name . " " . $r->last_name ."</td>";
				echo "<td style='vertical-align: top;'>". $r->email ."</td>";
				echo "<td style='vertical-align: top;'>". $r->type->name ."</td>";
				
				echo "<td><a href='/administrator/changetype/id/".($r->id)."'>Change type</a></td>";


				echo "</tr>";
					
			}

		?>
		
		
		
				
		</table>




	</div>



</div>	