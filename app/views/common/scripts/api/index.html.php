<style>
h3
{
	margin-top: 30px;	
}	
h5
{
	margin-top: 20px;	
	margin-bottom: 10px;	
}	
p
{
	margin-top: 10px;	
}
code
{
	margin-top: 10px;	
	margin-bottom: 10px;	
	clear: both;
	display: inline;
}
</style>

<div class="contentBox">
	
	<div class="contentBoxTitle">
		<h1>Developer API</h1>
	</div>
	

	<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
		<p style="text-align: right;"><a href="/api/listing">API: Posting a listing</a></p>
	<? endif; ?>	

	
	<h3>User Authorization</h3>
	
	<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> offers simple, standard OAuth2 services for user-friendly authorization of third-party operations. Your users will need to go through the OAuth2 flow before your site can publish listings via <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>.</p>
	
	<p>Before you can begin, you will need to have <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> support provide you with a unique API key and secret. Once you receive those, uou'll be able to implement the <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> API in your website.</p>
	
	<h5>API keys</h5>
	
	<p>In this documentation, we'll use the following test account:</p>
	
	<table class="docs-data">
		<tr>
			<td>API Key:</td>
			<td><code>0cbc6611f5540bd0809a388dc95a615b</code></td>
		</tr>
		<tr>
			<td>API Secret:</td>
			<td><code>1e6947ac7fb3a9529a9726eb692c8cc5</code></td>
		</tr>
	</table>
	
	
	<p><em>Note: These keys are not valid and cannot be used in your code.</em>
	<p><em>Note: You should protect your key and secret like you would a password, particularly the API secret.</em></p>
	
	<h5>OAuth2 endpoints:</h5>
	
	<table class="docs-data">
		<tr>
			<td>Authorization:</td>
			<td><code>http://www.<?=strtolower(COMPANY_NAME);?>.com/rest/authorize</code></td>
		</tr>
		<tr>
			<td>Access token:</td>
			<td><code>http://www.<?=strtolower(COMPANY_NAME);?>.com/rest/token</code></td>
		</tr>
	</table>
	
	<h3>OAuth2 Implementation Overview</h3>
	
	<p>To provide a general understanding of how OAuth2 works, here's an outline of how your site and the <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> API interact together to authorize your site to post listings on behalf of your user:</p>
	
	<p>Your site generates a random "state" string and redirects the user to the <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> authorization endpoint, with client_id (your API key) and 
		state (random generated string) as GET arguments. Your site should also store the state string in the current session.</p>
	
	<p>Example redirect URL:</p>
	
	<p><code>http://www.<?=strtolower(COMPANY_NAME);?>.com/rest/authorize?client_id=0cbc6611f5540bd0809a388dc95a615b&state=0ed8eb67d887c67f953359d7cb2ac6a0</code></p>
	
	
	<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> asks the user if they want to allow your site to access their <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> account. User clicks yes or no.</p>
	
	<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> redirects the user back to your site's preconfigured redirect URL with an authorization token appended (in a 'code' GET parameter) and the state string (in a 'state' GET argument).</p>
	
	<p>Example redirect URL:</p>
	<p><code>http://www.yoursite.com/oauth_callback?code=0ed3f45743dfebb64cdca6c15011e391701e9340&state=0ed8eb67d887c67f953359d7cb2ac6a0</code></p>
	
	<p>In this example, the authorization token is:</p> 
	<p><code>0ed3f45743dfebb64cdca6c15011e391701e9340</code>.</p>
	
	<p>Your server checks that the 'state' GET argument matches the string stored in the session (in step 1).</p>
	
	<p>Your server then makes an HTTP-authenticated POST request to the <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> access token endpoint, posting the authorization token.</p>
	
	<p>Example POST request:<br/>
	<table class="docs-data">
		<tr>
			<td>URL:</td><td><code>http://www.<?=strtolower(COMPANY_NAME);?>.com/rest/token</code></td>
		</tr>
		<tr>
			<td>Username:</td><td><code>0cbc6611f5540bd0809a388dc95a615b</code></td>
		</tr>
		<tr>
			<td>Password:</td><td><code>1e6947ac7fb3a9529a9726eb692c8cc5</code></td>
		</tr>
		<tr>
			<td>POST field <code>grant_type</code>:&nbsp;&nbsp;</td><td><code>authorization_code</code></td>
		</tr>
		<tr>
		    <td>POST field <code>code</code>:</td><td><code>0ed3f45743dfebb64cdca6c15011e391701e9340</code><br/><em>(authorization token from URL in step 3)</em></td>
		</tr>
	</table>
	</pre></p>
	
	<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> responds with a JSON-encoded string containing an access token, an access token expiration, and a refresh token. All three of these 
		items should be persisted in your site's database for future use. Each of your users who authorize you to access their <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> account will 
		have their own unique tokens, so you should be sure to include a reference to the current user (generally a user_id column). The state and 
		authorization token are no longer useful and do not need to be stored.</p>
	
	<p>Example JSON response:</p>
	
	<pre>
	{
	    "access_token": "e62f1900c6b12c875fc64fa915309b09c2517530",
	    "refresh_token": " 977b97fec11b71360f2fc08e69bac1899568e4c9",
	    "expires_in": 315576000
	}</pre>
	
	<p>You might also receive an error, and your application should be prepared to handle it gracefully.</p>
	
	<p>Example error JSON response:</p>
	
	<pre>
	{ 
	    "error": "invalid_grant",
	    "error_description": "Authorization code doesn't exist or is invalid for the client"
	}
	</pre>
	
	<p><em>Note: the expires_in value is the number of seconds until the access token expires. To get a date and time of expiry, add the expires_in value to the current UNIX timestamp.</em></p></li>
	

	
	<h3>OAuth2 Tokens</h3>
	
	Tokens are semi-random strings of characters that function as unique identifiers. Each type of token has a different function:
	
	<h5>Access token</h3>
	
	<p>The access token is your key to the user's <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> account. You will need to provide this key each time you take an action (e.g. publish a listing) to the user's account. Access tokens are currently long-lived: they're good for 10 years. It's still a good idea to store the access token expiration though, as this may change in the future.</p>
	
	Access tokens should be protected and never displayed or otherwise exposed on your website.</p>
	
	<h5>Refresh token</h5>
	
	<p>Once an access token expires, your site can use the refresh token to obtain a new one. In the future, should we reduce the lifetime of access tokens, we'll provide documentation on how to use refresh tokens.</p>
	
	<h5>Authorization token</h5>
	
	<p>This temporary token (issued in step 3 above) is short-lived and single-use. Its only purpose is to retrieve an access token. Once it's used, it expires. Your site should use it to request the access token immediately (in step 5 above), because they expire in 10 seconds.</p>


	<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
		<p style="text-align: right; margin-top: 30px;"><a href="/api/listing">API: Posting a listing</a></p>
	<? endif; ?>	

	
	


</div>