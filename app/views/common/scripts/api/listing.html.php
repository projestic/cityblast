<style>
h3
{
	margin-top: 30px;	
}	
h5
{
	margin-top: 20px;	
	margin-bottom: 10px;	
}	
p
{
	margin-top: 10px;	
}
code
{
	margin-top: 10px;	
	margin-bottom: 10px;	
	clear: both;
	display: inline;
}
</style>

<div class="contentBox">
	
	<div class="contentBoxTitle">
		<h1>API: Listing Post</h1>
	</div>
	
	
	<p style="text-align: right;"><a href="/api">API: User authorization</a></p>

	
	
	<h3>API operations: posting a listing to <?=(COMPANY_NAME);?></h3>
	
	<p>Once you have obtained a valid access token for a user, your site can publish listings to that user's account via the <?=(COMPANY_NAME);?> API.</p>
	
	<p>Publishing listings is just a matter of POSTing the listing data and the access token to the listing endpoint URL.</p>
	
	<p>Endpoint: <code>http://www.<?=strtolower(COMPANY_NAME);?>.com/rest/listing</code></p>
	
	<p>Request type: <code>POST</code></p>
	
	<h5>POST data:</h5>
	
	<table class="docs-data" style="width: 100%">
		<thead>
		<tr>
			<th>
				Field name
			</th>
			<th>
				Data type		
			</th>
			<th>
				Description
			</th>
			<th>
				Required?
			</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>
				<code>access_token</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The access token obtained through the OAuth2 process for <?=(COMPANY_NAME);?> user you want to post a listing on behalf of.
			</td>
			<td>
				Required
			</td>
		</tr>
		<tr>
			<td>
				<code>message</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The message text that will appear with this listing.
			</td>
			<td>
				Required
			</td>
		</tr>
		<tr>
			<td>
				<code>street_number</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The numeric portion of the property address (<strong>123</strong> Main St)
			</td>
			<td>
				Required
			</td>
		</tr>
		<tr>
			<td>
				<code>street</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The street or highway portion of the property address (123 <strong>Main</strong> St)
			</td>
			<td>
				Required
			</td>
		</tr>
		<tr>
			<td>
				<code>apartment</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The apartment or unit number of the property address.
			</td>
			<td>
				Optional
			</td>
		</tr>
		<tr>
			<td>
				<code>city</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The city where the property is located.
			</td>
			<td>
				Required
			</td>
		</tr>
		<tr>
			<td>
				<code>city</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The postal code where the property is located.
			</td>
			<td>
				Required
			</td>
		</tr>
		<tr>
			<td>
				<code>price</code>
			</td>
			<td>
				<code>decimal</code>		
			</td>
			<td>
				The price of the property.
			</td>
			<td>
				Required
			</td>
		</tr>
		<tr>
			<td>
				<code>mls</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The MLS number of the property.
			</td>
			<td>
				Optional
			</td>
		</tr>
		<tr>
			<td>
				<code>square_footage</code>
			</td>
			<td>
				<code>integer</code>		
			</td>
			<td>
				The size of the property in square feet.
			</td>
			<td>
				Optional
			</td>
		</tr>
		<tr>
			<td>
				<code>bedrooms</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The property's number of bedrooms.
			</td>
			<td>
				Optional
			</td>
		</tr>
		<tr>
			<td>
				<code>extra_bedrooms</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The property's number of extra bedrooms.
			</td>
			<td>
				Optional
			</td>
		</tr>
		<tr>
			<td>
				<code>bathrooms</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The property's number of bathrooms.
			</td>
			<td>
				Optional
			</td>
		</tr>
		<tr>
			<td>
				<code>parking_spaces</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The property's number of parking spaces.
			</td>
			<td>
				Optional
			</td>
		</tr>
		<tr>
			<td>
				<code>maintenance_fee</code>
			</td>
			<td>
				<code>string</code>		
			</td>
			<td>
				The property's maintenance fees, if any.
			</td>
			<td>
				Optional
			</td>
		</tr>
<!--
Thumbnails disabled - Kfoster 2014-01-24
		<tr>
			<td>
				<code>thumbnail</code>
			</td>
			<td>
				<code>binary data</code>		
			</td>
			<td>
				The photo thumbnail data.
			</td>
			<td>
				Required
			</td>
		</tr>
-->
		<tr>
			<td>
				<code>photos[]</code>
			</td>
			<td>
				<code>binary data</code>		
			</td>
			<td>
				Array of photo data. Each photo be posted in one or multiple fields, all named "photos[]".
			</td>
			<td>
				Required
			</td>
		</tr>
		<tr>
			<td>
				<code>property_type</code>
			</td>
			<td>
				<code>integer</code>		
			</td>
			<td>
				The property type, one of:
				<ol>
					<li>Detached</li>
					<li>Semi-detached</li>
					<li>Freehold Townhouse</li>
					<li>Condo Townhouse</li>
					<li>Condo Apartment</li>
					<li>Coop/Common Elements</li>
					<li>Attached/Townhouse</li>
				</ol>
			</td>
			<td>
				Required
			</td>
		</tr>
		</tbody>
	</table>
	
	<p>Upon posting, <?=(COMPANY_NAME);?> will return a JSON response.</p>
	
	<p>Example JSON response when listing has been created:</p>
	
	<pre>
	{
	    "created": "true",
	    "listing_id": "123456"
	}</pre>
	
	<p>Example JSON response when an error has occurred:</p>
	
	<pre>
	{
	    "created": "false",
	    "errors": ["A listing with this information already exists."]
	}</pre>
	
	
	<p style="text-align: right; margin-top: 30px;"><a href="/api/index">API: User authorization</a></p>
	


</div>