// Adding event listening on comment
FB.Event.subscribe('comment.create', function(response){

    var commentQuery = FB.Data.query("SELECT object_id,id,text, fromid FROM comment WHERE post_fbid='"+response.commentID+"' AND object_id IN (SELECT comments_fbid FROM link_stat WHERE url='"+response.href+"')");
    var userQuery = FB.Data.query("SELECT name FROM user WHERE uid in (select fromid from {0})", commentQuery);


    FB.Data.waitOn([commentQuery, userQuery], function() 
    {
		var commentRow = commentQuery.value[0];
		var userRow = userQuery.value[0];

		var commentId   =	commentRow.id;
		var name	 	=	userRow.name;
		var fuid		=	commentRow.fromid;
		var text	 	=	commentRow.text;
		var mid		=	<?php echo (empty($_GET['mid'])) ? 0 : intval($_GET['mid']);?>;
		var url		=	'/comments/broadcast';
		var data	 	=	'name='+name+'&fuid='+fuid+'&text='+text+'&mid='+mid+'&referring_url=<? echo urlencode("/".Zend_Controller_Front::getInstance()->getRequest()->getControllerName() . "/" . Zend_Controller_Front::getInstance()->getRequest()->getActionName());?>';


		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			success: function(response){
				// do nothing
			},
			dataType: 'json'
		});

    });
});