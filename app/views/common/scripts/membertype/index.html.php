<div class="container_12 clearfix">
	
	<div class="grid_12" style="padding-top: 30px;">
		<h1>
			Member Types 
			<a href="/membertype/add" class="uiButton"  style="float: right; margin-top: 0px; padding-top:0px; margin-right: 0px;">Add Member Type</a>
		</h1>
	</div>

	<div class="grid_12" style="margin-bottom: 80px;">
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<th>Code</th>
				<th>Name</th>
				<th>Is Default</th>
				<th>Price Month</th>
				<th>Price Biannual</th>
				<th>Price Annual</th>
				<th>Promo Allowed</th>
				<th style="text-align: center;">Actions</th>
			</tr>
<? 
		foreach($this->paginator as $promo)
		{
?>
			<tr>
				<td><?=$this->escape($promo->id)?></td>
				<td><?=$this->escape($promo->code)?></td>
				<td><?=$this->escape($promo->name)?></td>
				<td><?=($promo->is_default) ? 'Yes' : 'No'?></td>
				<td><?=$this->escape(sprintf('%.2f', $promo->price_month))?></td>
				<td><?=$this->escape(sprintf('%.2f', $promo->price_biannual))?></td>
				<td><?=$this->escape(sprintf('%.2f', $promo->price_annual))?></td>
				<td><?=($promo->promo_codes_allowed) ? 'Yes' : 'No'?></td>
				<td style="text-align: center;">
					<a href='/membertype/update/id/<?=$this->escape($promo->id)?>'>edit</a> | 
					<a class="delete_link" href='/membertype/delete/id/<?=$this->escape($promo->id)?>'>delete</a>
				</td>
			</tr>
<?
		}
?>

		</table>
		<div class="grid_10" style="margin-top: 0px;">
			<div style="margin-top: 0px; text-align: right; float: right;">
				<?php 
				if(count($this->paginator) > 0) 
				{
					echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); 
				}
				?>
			</div>
		</div>
	</div>
</div>	

<script type="text/javascript">
	$(document).ready(function()
	{
		$(".delete_link").click(function()
		{
			var result = window.confirm('Are you sure you want to delete this member type record?');
			result = (result) ? window.confirm('Last chance! Are you really sure you want to delete this member type record?') : result;
			
			return result;
		});
		
	});
</script>