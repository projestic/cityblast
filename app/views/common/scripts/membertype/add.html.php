<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">

	<div class="grid_12 member_update">

		<div class="box form clearfix">
			<div class="box_section">
				<h1>Member Type</h1>
			</div>

			<form id="extraForm" action="/membertype/save" method="POST">
			<input type="hidden" name="id" value="<?php if(isset($this->member_type->id)) echo $this->member_type->id;?>" />

			<ul class="uiForm clearfix">
				<li class="uiFormRowHeader">Member Type Details</li>
				
				<? if(isset($this->member_type) && ($this->member_type->id)) :?>
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Code</li>
					    <li class="field" style="padding-top: 20px;">
							<? if(isset($this->member_type->code)) echo $this->member_type->code;?>
					    </li>
					</ul>
				</li>
				<? endif; ?>
				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Name</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="name" id="name" value="<? if(isset($this->member_type->name)) echo $this->member_type->name;?>" class="uiInput" style="width: 200px" />
					    </li>
					</ul>
				</li>

				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Is Default</li>
					    <li class="field" style="padding-top: 20px;">

						    <input type="radio" id="is_default" name="is_default" value="1" <? if(isset($this->member_type) && $this->member_type->is_default == 1) echo "checked" ?>>&nbsp;Yes &nbsp;&nbsp;
						    <input type="radio" id="is_default" name="is_default" value="0" <? if(isset($this->member_type) && $this->member_type->is_default == 0) echo "checked" ?>>&nbsp;No &nbsp;&nbsp;
					    </li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Price Month</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="price_month" id="price_month" value="<? if(isset($this->member_type->price_month)) echo sprintf('%.2f', $this->member_type->price_month);?>" class="uiInput" style="width: 200px" />
					    </li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Price Biannual</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="price_biannual" id="price_biannual" value="<? if(isset($this->member_type->price_biannual)) echo sprintf('%.2f', $this->member_type->price_biannual);?>" class="uiInput" style="width: 200px" />
					    </li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Price Annual</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="price_annual" id="price_annual" value="<? if(isset($this->member_type->price_annual)) echo sprintf('%.2f', $this->member_type->price_annual);?>" class="uiInput" style="width: 200px" />
					    </li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Promos Allowed</li>
					    <li class="field" style="padding-top: 20px;">

						    <input type="radio" id="promo_codes_allowed" name="promo_codes_allowed" value="1" <? if(isset($this->member_type) && $this->member_type->promo_codes_allowed == 1) echo "checked" ?>>&nbsp;Yes &nbsp;&nbsp;
						    <input type="radio" id="promo_codes_allowed" name="promo_codes_allowed" value="0" <? if(isset($this->member_type) && $this->member_type->promo_codes_allowed == 0) echo "checked" ?>>&nbsp;No &nbsp;&nbsp;
					    </li>
					</ul>
				</li>


				<li class="fieldname"></li>
				<li class="field">
					<? if(isset($this->member_type) && ($this->member_type->id)) :?>
						<input type="submit" name="submitted" class="uiButton large right" value="Save" style="margin-right: 15px margin-top: 15px;">
					<? else : ?>
						<input type="submit" name="submitted" class="uiButton large right" value="Add" style="margin-right: 15px margin-top: 15px;">
					<? endif; ?>
				</li>
			</ul>
			</form>
		</div>
	</div>
	
</div>