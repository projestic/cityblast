<div class="container_12 member-update">
	

	
	<div class="grid_12 member-update">
		

		<?php if(!empty($this->message)): ?>
			
			<div class="box white80 clearfix" style="text-align: center;">
			
				<h3 style="color: #005826;"><?php echo $this->message;?></h3>
				
			</div>
			
		<?php endif; ?>
	
	</div>

	<div class="grid_12" style="margin-top: 30px; margin-bottom: 15px; font-size: 26px; font-weight: bold;">
		<?php echo $this->member->first_name.' '.$this->member->last_name; ?>
		<span style="float: right;"><a id="deletefanpage" href="/fanpage/delete/id/<?php echo $this->member->id;?>" class="uiButton" style="float: right;">Delete fanpage</a></span>
	</div>
	

	

	
	<div class="grid_12 member-update">
		
		<div class="box gray clearfix">

			<form action="/fanpage/save/" method="POST">
				<input type="hidden" name="member_id" value="<?php echo $this->member->id;?>" />
				<input type="hidden" name="id" value="<?php if(isset($this->fanpage->id)) echo $this->fanpage->id;?>" />
				
				<ul class="uiForm clearfix">

					<li class="uiFormRowHeader">Fanpage</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Fanpage</li>
							<li class="field">
								<input type="text" name="fanpage" id="fanpage" value="<?php if(isset($this->fanpage->fanpage)) echo $this->fanpage->fanpage;?>" class="uiInput validateNotempty <?=($this->fields && in_array("fanpage", $this->fields)?" error":"")?>" style="width: 600px !important;" />
							</li>
						</ul>
					</li>


					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Active</li>
							<li class="field">
								

								<select name="active" style="width: 200px; border: 1px solid #000;">
									<option></option>
									<option value="yes" <?php if(isset($this->fanpage->active) && $this->fanpage->active == "yes") echo 'selected="selected"';?>>YES</option>
									<option value="no" <?php if(isset($this->fanpage->active) && $this->fanpage->active == "no") echo 'selected="selected"';?>>NO</option>
								</select>
							</li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">

						<ul>
							<li class="fieldname">Fanpage ID</li>
							<li class="field">
								<input type="text" name="fanpage_id" id="fanpage_id" value="<?php if(isset($this->fanpage->fanpage_id))  echo intval($this->fanpage->fanpage_id);?>" class="uiInput validateNotempty <?=($this->fields && in_array("fanpage_id", $this->fields)?" error":"")?>" style="width: 600px !important;" />
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Likes</li>
							<li class="field">
								<?php if(isset($this->fanpage->likes))  echo intval($this->fanpage->likes);?>
							</li>
						</ul>
					</li>					
					
				</ul>					


	

				<ul class="uiForm clearfix">
					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<input type="submit" value="Update" class="uiButton right" id="send_id" />
							</li>
						</ul>
					</li>
				</ul>
				
			</form>
				
		</div>
		
	</div>
</div>