<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		<? if(isset($this->notification)) : ?>
			<p style="color: #790000; font-style: italic;"><?=$this->notification; ?></p>
		<? endif; ?>

		<h1>Fanpages</h1>		
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<th>Member</th>
					
				<th>Fanpage</th>
				<th>Fanpage ID</th>
				
				<th>Active</th>
				
				<th>Likes</th>
			
				<th>Action</th>
				
			</tr>
			<?
				$row=true;
				$total_likes=0;
				foreach($this->fanpages as $fanpage)
				{
					

						
					echo "<tr ";
						if($row == true)
						{						
							$row = false;
							echo "class='odd' ";	
						}
						else
						{
							$row = true;	
							echo "class='' ";	
						}		
	
						//if(strlen($listing->message) > 140) echo " style='color: #790000; font-weight: bold; background-color: #FFE6E6' ";
						
					echo ">";
										
					echo "<td style='vertical-align: top;'><a href='/fanpage/update/".$fanpage->id."' style='text-decoration: none;'>". $fanpage->id."</a></td>";
					
					?>
					<td>
					<?php if ($fanpage->member instanceOf Member) : ?><a href="/admin/member/<?php echo $fanpage->member->id ?>"><?php echo $fanpage->member->name() ?></a><?php endif; ?>
					</td>
					<td>
					<?php if ($fanpage->member instanceOf Member) : ?><a href="<?php echo $fanpage->fanpage; ?>" target='_new'><?php echo $fanpage->fanpage; ?></a><?php endif; ?>
					</td>
					
					<?php
					
					
					echo "<td><a href='https://www.facebook.com/". $fanpage->fanpage_id."' target='_new''>". $fanpage->fanpage_id."</a></td>";	
					echo "<td>". strtoupper($fanpage->active)."</td>";	
					echo "<td>". $fanpage->likes."</td>";	
					
					$total_likes+=$fanpage->likes;
					
						
					echo "<td style='text-align: center;' nowrap>";
					
					if ($fanpage->member instanceOf Member)
					{
	
						//THIS IS A DANGEROUS FUNCTION THAT ONLY ALEN SHOULD HAVE ACCESS TO...
						if($_SESSION['member']->first_name == "Alen" && $_SESSION['member']->last_name == "Bubich")
						{						
							echo "<a href='/fanpage/contentfill/".$fanpage->member->id."/".$fanpage->id."'>content fill</a> | ";	
						}
						
						echo "<a href='/fanpage/testblast/".$fanpage->member->id."/".$fanpage->id."'>test publish</a> | ";
						echo "<a href='/fanpage/update/".$fanpage->member->id."/".$fanpage->id."'>edit</a> | ";
					}
					

					
					echo "<a href='/fanpage/delete/".($fanpage->id)."' onclick='return window.confirm(\"Delete the current fanpage?\");'>delete</a>";
	
					echo "</td>";
	
		
					echo "</tr>";
				}		
			?>				
			
			<tr><td colspan="5" style="text-align: right;">Total:</td><td><?=$total_likes;?></td><td></td></tr>
			
		</table>
		<div class="grid_6" style="margin-bottom: 80px; float: right;">
			<div style="float: right;" class="clearfix"><?= ($this->fanpages) ? $this->paginationControl($this->fanpages, 'Elastic', '/common/pagination.phtml') : ''; ?></div>
		</div>
	</div>
</div>