<div class="container_12 clearfix page_Grid_Wider">
    <!--
     old uploader
     <script type="text/javascript" src="/js/swfobject.js"></script>
    <script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script> -->

    <script src="/js/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/js/uploadify/uploadify.css">

    <div class="grid_12 member_update page_Grid_Wider">
        
        <table style="width: 100%">
            <tr>

                <td style="font-size: 22px; font-weight: bold; vertical-align: top;">
                    <?php echo $this->member->first_name.' '.$this->member->last_name; ?><br/>
                <td>
                

            </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="0">
            <tr>
                <td valign="top" bgcolor="#ffffee">
                    <div style="min-height: 200px;">
                        <table width="100%" cellpadding="2" cellspacing="1">
                            <tr>
                                <td bgcolor="#ffffff">
                                    <?=$this->member->testimonials->testimonial?>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff">
                                    <input type="text" name="testimonial_link" id="l<?=$this->member->testimonials->id?>" value="<?=$this->member->testimonials->link?>" size="60" />&nbsp;<a href="/testimonial/updateTestimonialLink" class="testimonialLinkAction uiButton" data-testim_id='<?=$this->member->testimonials->id?>' style="float: none;">Save link</a>
                                    <? if (count($this->member->fanpages) > 0): ?>
                                    <ul>
                                        <? foreach ($this->member->fanpages as $fanp): ?>
                                            <?php
                                            $pos = stripos($fanp->fanpage, 'http');
                                            if ($pos !== false): ?>
                                                <li><a href='<?=$fanp->fanpage?>'><?=$fanp->fanpage?></a></li>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </ul>
                                    <? endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff">
                                    <input type="hidden" name="testimonial_image" class="testimonial_image" id="i<?=$this->member->testimonials->id?>" data-testim_id="<?=$this->member->testimonials->id?>" />
                                </td>
                            </tr>
                            <? if (!empty($this->member->testimonials->image)): ?>
                            <tr>
                                <td bgcolor="#ffffff"><img src="<?=$this->member->testimonials->image?>" style="max-width: 400px;"></td>
                            </tr>
                            <? endif; ?>
                        </table>
                    </div>
                </td>
                <td width="150" valign="top" bgcolor="#f0f0f0">
                    <div style="min-height: 200px; padding-top: 20px">
                        <? $label = $this->member->testimonials->approved == 1 ? 'Unapprove' : 'Approve'; ?>
                        <?="<a href='/testimonial/updateTestimonial' id='id{$this->member->testimonials->id}'  class='testimonialAction uiButton' data-testim_id='{$this->member->testimonials->id}'>{$label}</a>";?>
                    </div>
                </td>

            </tr>
        </table>
    </div>

</div>

<div class="clearfix" style="height: 50px;"></div>

<script type="text/javascript">


    $(document).ready(function() {

        $(".testimonial_image").each(function() {
            var testim_id = $(this).data('testim_id');
            var uploader_id = $(this).attr('id');
            $('#'+uploader_id).uploadify({
                height        : 30,
                swf           : '/js/uploadify/upload.swf',
                //uploader      : '/uploadify/uploadify.php',
                uploader      : '/testimonial/testimonialImage?testim_id='+testim_id,
                width         : 120,
                'onUploadSuccess' : function(file, data, response) {
                    if(!alert('The file ' + file.name + ' was uploaded with a response of :' + data)){window.location.reload();}
                }
            });

        });

        $('.testimonialAction').click(function(e){
            e.preventDefault();
            var testim_id = $(this).data('testim_id');
            var link_id = $(this).attr('id');
            $.post( $(this).attr('href'), { testim_id: testim_id } ).done(function( data ) {
                $('#'+link_id).text(data);
                //alert('Status updated');
            });
        });

        $('.testimonialLinkAction').click(function(e){
            e.preventDefault();
            var testim_id = $(this).data('testim_id');
            $.post( $(this).attr('href'), { testim_id: testim_id, link : $('#l'+testim_id).val() } ).done(function( data ) {
                alert('Link has been updated successfully.');
            });
        });


    });
</script>
