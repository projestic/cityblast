<?php

$year_from  = null;
$year_to    = null;
$day_from   = null;
$day_to     = null;
$month_from = null;
$month_to   = null;

if ($this->to_date)
{
	$to_arr = explode("-",$this->to_date);
	$day_to	= $to_arr[2];
	$month_to	= $to_arr[1];
	$year_to	= $to_arr[0];
}

if ($this->from_date)
{
	$from_arr = explode("-",$this->from_date);
	
	$day_from		= $from_arr[2];
	$month_from	= $from_arr[1];
	$year_from	= $from_arr[0];
}

?>



<?php $allowStatus = in_array($this->selected_status, array('threestrikes', 'fbpword', 'twpword', 'expiredtoken')); ?>
<?php $affiliate_array = $this->affiliate_array; ?>

<div class="container_12 clearfix">

	<? echo $this->render('common/member.dropdown.html.php'); ?>

	<form name="3strikes-form" action="/admin/allmembers" method="POST" id="3strikesForm">

	<?
		if($this->selected_status == "expiredtoken")
		{
			echo "<input type='hidden' name='expiredtoken' value='1'/>";
		}
	?>
	<div class="grid_12">

		<?php if (isset($this->strikes)) : ?>
		<h4 align="center" style="color: green;">3 Strikes email has been sent for <?php echo $this->strikes ?> member<?php echo ($this->strikes == 1) ? '' : 's' ?></h4>
		<?php endif ?>


		<?php if (isset($this->expires)) : ?>
		<h4 align="center" style="color: green;">3 Expired Token email has been sent for <?php echo $this->expires ?> member<?php echo ($this->expires == 1) ? '' : 's' ?></h4>
		<?php endif ?>



		<h1>
			
			<?php 
			
				if(count($this->paginator) > 0)
				{
                    echo number_format($this->paginator->getTotalItemCount()). " Member testimonials ";
				}
					
				if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo "(".$this->selectedCityObject->name.")";
			?>
		</h1>

		
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<? if($allowStatus) : ?><th></th><? endif; ?>
				<th>ID</th>

				<th>Name</th>
			

				<th>Email</th>

                <th>Testimonial</th>
                <th>Action</th>
			</tr>

		<?
			$total_reach = 0;
			$total_recur_revenue = 0;
			$row=true;

			foreach($this->paginator as $member)
			{

				if( $member->id == 1182 or $member->id == 1183 or $member->id == 1184 )
				{
					continue;
				}

				$is_publisher = false;

				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}
				echo ">\n";


				if($allowStatus) echo "<td><input type='checkbox' name='3strikes[]' value='". $member->id ."' /></td>\n";

				echo "<td><a href='/testimonial/view/id/".$member->id."'>".$member->id."</a></td>\n";


				echo "<td style='line-height: 15px; width: 100px;'><a href='/testimonial/view/id/".$member->id."'>".$member->first_name." ".$member->last_name."</a></td>\n";

				echo "<td>";
					
					if($member->email)
					{
						echo "<a href='mailto:".$member->email."'>";
						$newstring = substr($member->email,0,30);
						echo $newstring;
						echo "</a>";
					}
					
				echo "</td>\n";





                    echo "<td>";

                        echo $member->testimonials->testimonial ;

                    echo "</td>\n";

                    echo "<td nowrap style='line-height: 15px; width: 100px;'>";
					switch ($member->testimonials->approved) {
						case null:
							$admin_id = $_SESSION['member']->id;
							echo "<div id='action{$member->testimonials->id}'><a href='/testimonial/updateTestimonial' id='id{$member->testimonials->id}' class='testimonialAction' data-testim_id='{$member->testimonials->id}' data-status='1'>Approve</a><br/><a href='/testimonial/updateTestimonial' id='id{$member->testimonials->id}' class='testimonialAction' data-testim_id='{$member->testimonials->id}' data-status='0' data-amember_id='{$admin_id}'>Unapprove</a></div>";
							break;
						case 0:
							echo 'Unapproved';
							break;
						case 1;
							echo 'Approved';
							break;
					}
                    echo "</td>\n";
			}

		?>

		<?
			if($allowStatus)
			{
				$subtotal_colspan = 7;
				$total_colspan = 5;
			}
			else
			{
				$subtotal_colspan = 6;
				$total_colspan = 6;
			}

		?>

		</table>

	</div>

    <script>

        $(document).ready(function(){
            $('.testimonialAction').click(function(e){
                e.preventDefault();
                var testim_id = $(this).data('testim_id');
				var amember_id = $(this).data('amember_id');
				var status = $(this).data('status');
                var link_id = $(this).attr('id');
                $.post($(this).attr('href'), {testim_id: testim_id, status: status, admin_id: amember_id})
					.done(function(data) {
						var status_text = null;
						var data_obj = $.parseJSON(data);
						if (data_obj.status == 'SUCCESS') {
							switch (status) {
								case 0:
									status_text = 'Unapproved';
									break;
								case 1:
									status_text = 'Approved';
									break;
							}
							$('#action'+testim_id).html(status_text);
						} else {
                    		alert(data_obj.message);
						}
                });
            });
        });

    </script>


	<div class="grid_2" style="margin-top: 20px;">
		<? if($allowStatus) : ?><input type="submit" value="Send Email" class="uiButton" name="submitted" id="mail-btn"><? endif; ?>
	</div>

	
	<div class="grid_10" style="margin-top: 0px;">
		<div style="margin-top: 0px; text-align: right; float: right;">
			<?php 
			if(count($this->paginator) > 0) 
			{
				$paginationParams = array('extraParams'=>array());
		
				if(!empty($this->to_date))
					$paginationParams['extraParams']['to'] = $this->to_date;
		
				if(!empty($this->from_date))
					$paginationParams['extraParams']['from'] = $this->from_date;
		
				echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $paginationParams); 
			}
			?>
		</div>
	</div>
	
	
	</form>

	<div class="grid_12" style="margin-bottom: 80px;">&nbsp;</div>

</div>


<script type="text/javascript">
$(document).ready(function()
{
	
	// TABS
	$(".uiTabset").each( function(){
		
		var tabset	= $(this);
		//console.log(tabset);
		var tabs		= $(this).find('.uiTab');
		//console.log(tabs);
		var content	= $(this).children('.uiTabContent');
		//console.log(content);
		
		// SET LOAD EVENTS ON EACH TAB
		(tabs).click( function(){
			
			var tab = $(this);
			var url = tab.attr('rel');
			tabs.removeClass('active');
			tab.addClass('active');
			loadTabContent(content, url);
			
		});
		
		var firstTab;
		if(window.location.hash){
			firstTab = tabs.filter('[href="' + window.location.hash + '"]')[0];	
		} else {
			firstTab = tabs[0];
		}

		// INITIALIZE FIRST TAB
		$(firstTab).addClass('active');
		loadTabContent(content, $(firstTab).attr('rel'));
		
	});

	$('.filter_open').click(function(e){
		var $this = $(this);
		var $AdvanceSearch = $('#advancedSearch');

		if($this.hasClass('close')){
			$AdvanceSearch.hide();
			$this.removeClass('close');
			$this.parent().addClass('roundit');
		}else{
			$AdvanceSearch.show();
			$this.addClass('close');
			$this.parent().removeClass('roundit');
		}
	});
	
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/index/";
		var data    =	{"hasmember":"1","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});


	$('#mail-btn').click(function(event) {

		event.preventDefault();

		//alert($('input[name*="3strikes"]:checked').length);
		if ($('input[name*="3strikes"]:checked').length > 0) {

			$('#3strikesForm').submit();
		} else {

			alert("Please select at least one member.")
		}
	})
});


</script>

<style type="text/css">

/* UI Form */
ul.uiForm, 
ul.uiForm li, 
ul.uiForm li ul, 
ul.uiForm li ul li {
	padding: none!important;
	margin: none!important;
	list-style-type: none!important;
}
ul.uiForm {
	margin: 0 -20px;
	width: 1000px;
}
ul.uiForm.small {
	width: 660px;
}
.uiForm ul, .uiForm li {
	float: left;
	margin: 0px;
}

.uiForm > li{
	float: none;
}

ul.uiForm li ul li ul {
	margin-left: 2em;
	margin-bottom: 1.5em;
}
ul.uiForm li ul li ul li {
	float: none;
	list-style-type: square!important;
	margin-bottom: .5em;
}

.uiFormRowHeader {
	font-size: 21px;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-weight: 600;
	line-height: 26px;
	text-shadow: 1px 1px 0px #fff;
	color: #000;
	margin: 0 0 -1px 0!important;
	padding: 20px;
	width: 960px;
	border-top: solid 1px #fff;
	border-bottom: solid 1px #e5e5e5;
	background-color: #f2f2f2;
	background-image: -moz-linear-gradient(top, #d9d9d9, #f2f2f2); /* FF3.6 */
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #d9d9d9),color-stop(1, #f2f2f2)); /* Saf4+, Chrome */
	background: linear-gradient(#d9d9d9, #f2f2f2);
	-pie-background: linear-gradient(#d9d9d9, #f2f2f2);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm.small .uiFormRowHeader {
	padding: 20px;
	width: 620px;
}
.uiForm .uiFormRow {
	width: 1000px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .uiFormRow {
	width: 660px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm .uiFormRow.last {
	-moz-border-radius: 0 0 8px 8px;
	-webkit-border-radius: 0 0 8px 8px;
	border-radius: 0 0 8px 8px;
	margin-bottom: -20px;
}
.uiForm .info {
	line-height: 20px;
	width: 1000px;
	padding: 10px;
	float: left;
}
.uiForm.small .info {
	width: 660px;
}
.uiForm .buttons {
	width: 960px;
	padding: 10px 20px;
	float: left;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .buttons {
	width: 620px;
}
.uiForm .buttons .uiButton {
	margin-left: 5px;
}
.uiForm .fieldname {
	width: 310px;
	padding: 12px 20px 10px;
	float: left;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-size: 16px;
	font-weight: 400;
	color: #333333;
}
.uiForm.small .fieldname {
	width: 210px;
}
.uiForm .field {
	width: 610px;
	padding: 12px 20px 10px;
	float: left;
}
.uiForm.small .field {
	width: 370px;
}
.uiForm .field span.label {
	float: left;
	margin: 0 20px 0 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiForm .fieldhelp {
	font-size: 12px!important;
	font-weight: normal!important;
	line-height: 18px!important;
	display: block!important;
	margin-top: 4px;
	color: #666666;
}
.uiFormFooterMessage {
	float: left;
	margin-top: 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiFormFooterMessage strong {
	color: #333;
}

ul.normal, ol.normal {
	margin: 0px!important;
}
ul.normal li, ol.normal li {
	line-height: 24px!important;
	margin-left: 10px!important;
}

.uiForm .upsell {
	width: 960px;
	padding: 20px;
	background-color: #252525;
	border-top: solid 1px #fff;
	background-image: url(/images/upsell_new_bg.png);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm .listonly {
	width: 960px;
	padding: 0 20px;
	height: 60px;
	background: #f2f2f2;
	border-top: solid 1px #999;
	-moz-box-shadow: 0 0 20px #cccccc inset;
	-webkit-box-shadow: 0 0 20px #cccccc inset;
	box-shadow: 0 0 20px #cccccc inset;
	position: relative;
	behavior: url(/css/PIE.htc);
}

/* Search Box Styles starts*/

.search_title{
	padding: 5px 10px 5px 13px;
	margin: 0 0px 0px 0px;
	background-color: #4D4D4D;
	background-image: -moz-linear-gradient(top, #666, #333);
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #666),color-stop(1, #333));
	background: linear-gradient(#666, #333);
	-pie-background: linear-gradient(#666666, #333333);
	position: relative;
	behavior: url(/css/PIE.htc);
	-moz-box-shadow: 0 1px 0 #000;
	-webkit-box-shadow: 0 1px 0 #000;
	box-shadow: 0 1px 0 #000;
	-moz-border-radius: 8px 8px 0 0 ;
	-webkit-border-radius: 8px 8px 0 0 ;
	border-radius: 8px 8px 0 0;
	position: relative;
	behavior: url(/css/PIE.htc);
	z-index: 1;
	font-family: Helvetica, Arial, Verdana, sans-serif;
	color: #FFF;
	font-weight: normal;
}

.search_title.roundit{
	border-radius: 8px 8px 8px 8px;
	margin-bottom: 20px;
}

.filter_open{
	background: url(/images/filter_opener.png) no-repeat;
	position: absolute;
	top: 1px;
	right: 10px;
	display: block;
	height: 30px;
	width: 30px;
	cursor: pointer;
}

.filter_open.close{
	background-position: 0 -30px;
}

#advancedSearch{
	display: none;
}

#advancedSearch ul.uiForm{
	margin: 0px;
}

<? /*
#advancedSearch ul.uiForm, #advancedSearch .uiForm .uiFormRow{
	width: 918px;
} */ ?>

#advancedSearch .uiForm > li{
	float: left;
}

#advancedSearch .uiFormRow, #advancedSearch .form {
	background: none !important;
}

#advancedSearch .uiFormRow{
	border-bottom: solid 1px #EDEDED;
}
#advancedSearch .fieldname {
	padding: 18px 20px 19px !important;
	background-color: #FBFBFB;
	border-right: solid 1px #EDEDED;
}
	
#advancedSearch .li_235 {
	width: 235px !important;
}

#advancedSearch .li_180 {
	width: 180px !important;
}

#advancedSearch .li_160 {
	width: 160px !important;
}

#advancedSearch .uiSelectWrapper{
	padding-bottom: 8px;
}

.uiFormRow li.field{
	padding: 10px 0px 5px 10px;
}

.uiFormRow li.fieldname{
	line-height: inherit;
}

</style>