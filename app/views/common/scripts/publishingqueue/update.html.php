<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 30px; padding-bottom: 60px;">
	<div class="grid_12">
		<div class="box form">
			<div class="box_section">
				<?php
					echo "<table width='100%' style='margin: 0px;'><tr><td style='border: none;'>";
					echo "<h2>".$this->publish->publish_city->name . " - Listing ID #".$this->publish->listing->id . "</h2>";		
					echo "</td><td style='text-align:right; font-size: 22px; font-weight: bold; border: none;' nowrap>Current Count: ".$this->publish->current_count;
					if($this->click_stat->total_clicks):
						echo " | Click Count: ".$this->click_stat->total_clicks;
					endif;
					echo "</td>";
					echo "</tr></table>";
				?>
			</div>
				<?php
					echo "<p>".$this->publish->listing->message."</p>";
				?>
			
			<form action="/publishingqueue/save" method="POST" class="clearfix">
				<input type="hidden" name="id" value="<?=$this->publish->id;?>" />
			
				<ul class="uiForm full">
					<li class="uiFormRow clearfix" style="border-radius: 0">
						<ul>
							<li class="fieldname cc">City</li>
							<li class="field cc" style="padding-top: 20px;">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="city_id" id="publish_city_id" class="uiSelect <?=($this->fields && in_array("city_id", $this->fields)?" error":"")?>" style="width: 200px;">
										
																																																		
											<option value="" <?php echo (empty($this->publish->city_id)) ? "selected" : "";?>>All Cities</option>
											<?php foreach($this->cities as $city): ?>
												<option value="<?php echo $city->id;?>" <?php if (!empty($this->publish->city_id) && $this->publish->city_id == $city->id) echo "selected";?>>
												<?php 
													echo $city->name;
													if(isset($city->state) && !empty($city->state)) echo ", " . $city->state;
												?>
													
												</option>
											<?php endforeach; ?>
				
										</select>
									</span>
								</span>
							</li>
							<li class="fieldname cc">Hopper ID</li>
							<li class="field cc" style="padding-top: 20px;">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select id="hopper_id" name="hopper_id" class="uiSelect" style="width: 200px;">
											<option value="">Select A Hopper</option>					    		
											<?
												for($i = 1; $i <= $this->publish->publish_city->number_of_hoppers; $i++)
												{		    
													echo '<option value="'. $i .'" ';
													if($this->publish->hopper_id == $i) echo " selected ";
													echo '>Hopper #'.$i.'</option>';
												}
											
											?>
										</select>
									</span>
								</span>
							</li>
							
							<?/*
							<li class="fieldname cc">Minimum Inventory Threshold</li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="min_inventory_threshold" id="name" value="<? if(isset($this->publish->min_inventory_threshold)) echo $this->publish->min_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("min_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
							*/ ?>
							
							<li class="fieldname cc">Maximum Inventory Threshold</li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="max_inventory_threshold" id="name" value="<? if(isset($this->publish->max_inventory_threshold)) echo $this->publish->max_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("max_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
						</ul>
					</li>	  
		
									
		
					<?/*
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Inventory Unit ID</li>
							<li class="field cc" style="padding-top: 20px;">
							<select name="inventory_id" id="inventory_id"  class="uiInput">
								<option value="">Select An Inventory Unit</option>			
								<? 
									for($i=1; $i <= $this->publish->publish_city->number_of_inventory_units; $i++) 
									{	
										echo "<option value='".$i."' ";
										if($this->publish->inventory_id == $i) echo " selected ";
										echo ">Inventory Unit #".$alpha_array[$i]."</option>";
									}
								?>
							</select>
							</li>
						</ul>
					</li>
					*/?>
					<? /*
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Hopper ID</li>
							<li class="field cc" style="padding-top: 20px;">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select id="hopper_id" name="hopper_id" class="uiSelect" style="width: 200px;">
											<option value="">Select A Hopper</option>					    		
											<?
												for($i = 1; $i <= $this->publish->publish_city->number_of_hoppers; $i++)
												{		    
													echo '<option value="'. $i .'" ';
													if($this->publish->hopper_id == $i) echo " selected ";
													echo '>Hopper #'.$i.'</option>';
												}
											
											?>
										</select>
									</span>
								</span>
							</li>
						</ul>
					</li>	  
					*/ ?>
					
					<?/*
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Priority</li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="priority" id="name" value="<? if(isset($this->publish->priority)) echo $this->publish->priority;?>" class="uiInput<?=($this->fields && in_array("priority", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
						</ul>
					</li>
					*/?>
			
					<? /*
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Minimum Inventory Threshold</li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="min_inventory_threshold" id="name" value="<? if(isset($this->publish->min_inventory_threshold)) echo $this->publish->min_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("min_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
						</ul>
					</li>
			
			
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Maximum Inventory Threshold</li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="max_inventory_threshold" id="name" value="<? if(isset($this->publish->max_inventory_threshold)) echo $this->publish->max_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("max_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
						</ul>
					</li>		    		
					*/?>
				</ul>    
			
			<input type="submit" name="submitted" class="uiButton large right" value="Save" style="margin-top: 20px;">
			
			</form>
		
		</div>
	</div>
	
</div>	
<script language="javascript">

$(document).ready(function(){


	// when ochange the publish city in the publishing assignment section
	$("#publish_city_id").change(function () {
		
		//alert('I chagneed!');
		
		$.post("/member/changepublishingcity", {"publish_city_id": $(this).val()}, function (response) {
			if ( response.error !== false ) {
				return;
			}
			if ( response.options ) {
				$("#hopper_id").html(response.options);
			}
		}, "json");
		return true;
	});
});
</script>	