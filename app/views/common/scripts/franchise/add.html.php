<!-- LOAD UPLOADIFY -->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>


<form action="/franchise/add" method="POST" class="clearfix">
<input type="hidden" name="id" value="<?=is_object($this->franchise) ? $this->franchise->id:'';?>" />
				

<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">
	<div class="grid_12">
		<div class="box form">
			<div class="box_section">
				<h1>Franchise</h1>
			</div>

			<?php if(isset($this->updated) && $this->updated === true): ?>
			
					<div class="grid_12"><h3>Your Franchise was successfully added!</h3>
					<p><a href="/franchise/index/">Return to franchise list.</a></p>
				</div>
			
				<div class="clearfix">&nbsp;</div>	
			<?php endif; ?>


			<?php if( !empty($this->error) ) : ?>
				<div class="grid_12 error">
					<h3><?php echo $this->error;?></h3>
				</div>
			
				<div class="clearfix">&nbsp;</div>
			<?php endif; ?>



			
			<ul class="clearfix uiForm full">
		
				<li class="uiFormRow clearfix">
					<ul class="clearfix">
						<li class="fieldname">Name <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="padding-top: 20px;">
							<input type="text" name="name" id="name" value="<? if(isset($this->franchise->name)) echo $this->franchise->name;?>" class="uiInput<?=($this->fields && in_array("name", $this->fields)?" error":"")?>" style="width: 500px" />
						</li>
						<li class="fieldname">Branded <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="padding-top: 20px;">
							<input type="checkbox" name="is_branded" id="is_enabled" value="1" class="uiInput" <? if (!empty($this->franchise) && !empty($this->franchise->is_branded)){ echo ' checked="checked"'; } ?> />
						</li>
						<li class="fieldname">Enabled <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="padding-top: 20px;">
							<input type="checkbox" name="is_enabled" id="is_enabled" value="1" class="uiInput" <? if (!empty($this->franchise) && !empty($this->franchise->is_enabled)){ echo ' checked="checked"'; } ?> />
						</li>
					</ul>
				</li>

			</ul>


		</div>


		<? if(isset($this->franchise->id) && ($this->franchise->id)) :?>
			<input type="submit" name="submitted" class="uiButton large right"  value="Save">
		<? else : ?>
			<input type="submit" name="submitted" class="uiButton large right" value="Add">
		<? endif; ?>		
		
	</div>
</div>



</form>



<script type="text/javascript">

var no_flash = false;

$(document).ready(function() {

	$('#file_upload').uploadify({
		'uploader'  : '/js/uploadify/uploadify.swf',
		'script'    : '/blast/uploadimage',
		'cancelImg' : '/js/uploadify/cancel.png',
	    	'folder'    : '/images/',
		'auto'      : true,
		'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
		'fileDesc'  : 'Image Files',
		'buttonImg' : '/images/browse_button.png',
		'removeCompleted' : false,
		'height' : 30,
		'width' : 106,
		'name': 'fileUploaded',
		'onComplete': function(event, ID, fileObj, response, data)
		{
			/*alert('RESPONSE: ' +response);*/
			$('#fileUploaded').val(response);

			if ($('#imageerror'))
			{
				$('#imageerror').remove();
			}

			//$("#file_uploadQueue").hide();
			$("#file_uploadStatus").show();
			$("#file_uploadStatus").html("<span style='display: block; text-align: center;'>Upload Complete</span>");
			$('#main_photo').attr('src', response);

			$('#main_photo_progress').css('display', 'none');

		},

  		'onError'     : function (event,ID,fileObj,errorObj) {
      		alert(errorObj.type + ' Error: ' + errorObj.info);
    		},

		'onProgress'  : function(event,ID,fileObj,data)
		{
			$('#main_photo_progress').css('display', 'block');
			$('#main_photo_progress').css('height', Math.round(data.percentage/100 * 106) + 'px');
			return false;
		}

	});
});


</script>