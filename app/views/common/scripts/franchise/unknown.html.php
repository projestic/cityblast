<div style="padding-top: 30px;">&nbsp;</div>

	
<div class="container_12 clearfix">
	<div class="grid_12">
		<h1>Franchise Unknown Members</h1>
	</div>
	<div class="grid_12">
		<?php
			if(!empty($this->message)):
			?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
			<?php
			endif;
		?>
	</div>
	
	<div class="grid_12" style="padding-bottom: 80px;">

		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>Member Id</th>
				<th>Name</th>
				<th>Brokerage</th>
				<th>Created At</th>
				<th></th>
			</tr>
<?php
			$row=true;
			foreach($this->paginator as $franchise_unknown_member)
			{
				$member = $franchise_unknown_member->member;
?>
			<tr>
				<td><a href="/admin/member/<?=$member->id?>"><?=$member->id?></a></td>
				<td><a href="/admin/member/<?=$member->id?>"><?=$member->first_name?> <?=$member->last_name?></a></td>
				<td><?=$member->brokerage?></td>
				<td><?=$member->created_at->format('Y-m-d H:i:s');?></td>
				<td><a href="/franchise/unknowndelete/<?=$franchise_unknown_member->id?>" onclick="return confirm('Are you sure?');">delete</a></td>
			</tr>
<?php
			}
?>
		</table>
		<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />
	</div>
</div>