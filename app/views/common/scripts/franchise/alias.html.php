<div style="padding-top: 30px;">&nbsp;</div>

<div class="container_12 clearfix">


	<div class="grid_12">


		<?php
			if(!empty($this->message)):
			?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
			<?php
			endif;
		?>

	</div>


    	<div class="grid_12"><h1>Franchise Aliases <a href="/franchise/addalias/" class="uiButton"  style="float: right; margin-top: 0px; padding-top:0px; margin-right: 0px;">Add Alias</a></h1></div>


	<div class="grid_12" style="padding-bottom: 80px;">

		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Franchise</th>
				<th>Created At</th>

                	<th>Actions</th>
			</tr>

		<?php
			$row=true;
			if (!empty($this->alias_list)) {
				foreach($this->alias_list as $alias)
				{
					echo "<tr ";
						if($row == true)
						{
							$row = false;
							echo "class='odd' ";
						}
						else
						{
							$row = true;
							echo "class='' ";
						}

					echo ">";

						echo "<td style='vertical-align: top;'><a href='/franchise/addalias/". $alias->id."'>". $alias->id."</a></td>";
						echo "<td style='vertical-align: top;'><a href='/franchise/addalias/". $alias->id."'>". $alias->name . "</a></td>";
						echo "<td style='vertical-align: top;'><a href='/franchise/addalias/". $alias->id."'>". $alias->franchise->name . "</a></td>";


						echo "<td style='vertical-align: top;'>".$alias->created_at->format("Y-M-d")."</td>";





							echo '<td style="vertical-align: top;">';
							echo '<a href="/franchise/addalias/' . $alias->id.'" >edit</a> | ';
							echo '<a href="/franchise/deletealias/' . $alias->id.'" onclick="return confirm(\'Are you sure?\');">delete</a></td>';
							
					echo "</tr>";

				}
			}
		?>
		</table>
	</div>
</div>					