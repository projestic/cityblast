<!-- LOAD UPLOADIFY -->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>


<form action="/franchise/addalias" method="POST" class="clearfix">
<input type="hidden" name="id" value="<?=is_object($this->alias) ? $this->alias->id:'';?>" />
				

<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">
	<div class="grid_12">
		<div class="box form">
			<div class="box_section">
				<h1>Franchise Alias</h1>
			</div>
			<?php if(!empty($this->error)) : ?>
				<div class="grid_12 error">
					<h3 style="color:red;"><?php echo $this->error;?></h3>
				</div>
			
				<div class="clearfix">&nbsp;</div>
			<?php endif; ?>
			<ul class="clearfix uiForm full">
				<li class="uiFormRow clearfix">
					<ul class="clearfix">
						<li class="fieldname cc">Name <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field cc" style="padding-top: 20px;">
							<input type="text" name="name" id="name" value="<? if(isset($this->alias->name)) echo $this->alias->name;?>" class="uiInput<?=($this->fields && in_array("name", $this->fields)?" error":"")?>" style="width: 500px" />
						</li>

						<li class="fieldname">Franchise <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field">
							
							<span class="uiSelectWrapper"><span class="uiSelectInner">
							<select name="franchise_id" class="uiSelect">
								<option value=""></option>
								<?php 																
									foreach ($this->franchises as $franchise) {
										echo "<option value='".$franchise->id."' ";
										if(isset($this->alias) && $this->alias->franchise_id == $franchise->id) echo " selected ";
										echo ">" . $franchise->name . "</option>";
									}
								?>
							</select>
							</span></span>
							
						</li>

					</ul>
				</li>
			</ul>
		</div>
		<? if(isset($this->alias->id) && ($this->alias->id)) :?>
			<input type="submit" name="submitted" class="uiButton large right"  value="Save">
		<? else : ?>
			<input type="submit" name="submitted" class="uiButton large right" value="Add">
		<? endif; ?>
	</div>
</div>
</form>



