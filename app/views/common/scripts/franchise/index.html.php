<div style="padding-top: 30px;">&nbsp;</div>

<div class="container_12 clearfix">


	<div class="grid_12">


		<?php
			if(!empty($this->message)):
			?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
			<?php
			endif;
		?>

	</div>


    	<div class="grid_12"><h1>Franchises <a href="/franchise/add/" class="uiButton"  style="float: right; margin-top: 0px; padding-top:0px; margin-right: 0px;">Add Franchise</a></h1></div>


	<div class="grid_12" style="padding-bottom: 80px;">

		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Enabled</th>
				<th>Created At</th>

				<th style="text-align: right">Active Agents</th>
				<th style="text-align: right">Inactive Agents</th>

				<th style="text-align: right">Approx Reach</th>

				<? /*<th>Active</th>*/ ?>
				<? /*<th>Blasting</th>*/ ?>
                	<th>Actions</th>
			</tr>

		<?
			$row=true;
			foreach($this->franchises as $franchise)
			{
				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}

				echo ">";

					echo "<td style='vertical-align: top;'><a href='/franchise/add/". $franchise->id."'>". $franchise->id."</a></td>";
					echo "<td style='vertical-align: top;'><a href='/franchise/add/". $franchise->id."'>". $franchise->name . "</a></td>";
					echo "<td style='vertical-align: top;'>". (($franchise->is_enabled) ? 'Yes' : 'No') . "</td>";


					echo "<td style='vertical-align: top;'>".$franchise->created_at->format("Y-M-d")."</td>";


					echo "<td style='vertical-align: top; top; text-align: right'>";
					
						if(!empty($franchise->current_agents)) echo "<span style='font-weight: bold;'><a href='/admin/allmembers?franchise_id=" . $franchise->id . "'>" . number_format($franchise->current_agents) . "</a></span>";
						else echo "";

					echo "</td>";
					echo "<td style='vertical-align: top; top; text-align: right'>";
						
						if ($franchise->current_agents) echo "<span style='font-weight: bold;'><a href='/admin/allmembers?franchise_id=" . $franchise->id . "'>" . number_format($franchise->all_agents - $franchise->current_agents) . "</a></span>";
						else echo "";

					echo "</td>";

					echo "<td style='vertical-align: top; text-align: right'>";
					
						if(!empty($franchise->reach)) echo number_format($franchise->reach);
						else echo "";

					echo "</td>";


	                	echo '<td style="vertical-align: top;">';
	                	echo '<a href="/franchise/add/' . $franchise->id.'" >edit</a> | ';
	                	echo '<a href="/franchise/delete/' . $franchise->id.'" onclick="return confirm(\'Are you sure?\');">delete</a></td>';
	                	
				echo "</tr>";

			}

		?>
		</table>
	</div>
</div>					