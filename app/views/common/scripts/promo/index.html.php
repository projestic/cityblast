<div class="container_12 clearfix">
	
	<div class="grid_12" style="padding-top: 30px;"><h1>Promo Codes <a href="/promo/add" class="uiButton"  style="float: right; margin-top: 0px; padding-top:0px; margin-right: 0px;">Add Promo Code</a></h1></div>

	
	<div class="grid_12" style="margin-bottom: 80px;">


		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<th>Promo Code</th>
				<th>Discount Type</th>
				<th>Amount</th>					
				<th>Promo Link</th>	
				<th>Franchise</th>
				<th>Affiliate ID</th>
				

				<th style="text-align: center;">Actions</th>
			</tr>

			

			<? 

			$row=true;
			foreach($this->paginator as $promo)
			{
				echo "<tr ";
					if($row == true)
					{						
						$row = false;
						echo "class='odd' ";	
					}
					else
					{
						$row = true;	
						echo "class='' ";	
					}		

				echo ">";
				
				
				echo "<td style='vertical-align: top;'><a href='/promo/update/".$promo->id."' style='text-decoration: none;'>". $promo->id."</a></td>";
				echo "<td style='vertical-align: top;'><a href='/promo/update/".$promo->id."' style='text-decoration: none;'>". $promo->promo_code."</a></td>";


				echo "<td>". $promo->type."</td>";
				
				echo "<td>";
				if($promo->type == "FIXED_AMOUNT") echo "$";
				echo $promo->amount;
				if($promo->type == "PERCENT") echo "%";
				echo "</td>";

				echo "<td>" . APP_URL . "/promo/code/". $promo->promo_code."</td>";



				echo "<td>";
				if(!empty($promo->franchise_id))
				{ 
					$franchise = $promo->franchise;

					echo "<a href='/promo/add/".$franchise->id."'>" . $franchise->name . "</a>";
					
				}
				else echo "&nbsp;";
				echo "</td>\n";
				
				

				echo "<td>";
				if(!empty($promo->affiliate_id))
				{ 
					$affiliate = $promo->affiliate;
					if ($affiliate) {
						echo "<a href='/admin/member/".$affiliate->owner->id."'>";
						if ($affiliate->owner) 
						{
							echo $affiliate->owner->first_name. " " . $affiliate->owner->last_name;
						}
						else
						{
							echo $promo->affiliate_id;
						}
						echo "</a>";
					} 
				}
				else echo "&nbsp;";
				echo "</td>\n";




				echo "<td><a href='/admin/allmembers?promo_id=".$promo->id."'>members</a> | ";
				echo "<a href='/promo/update/".$promo->id."'>edit</a> | ";
				echo "<a href='/promo/delete/".($promo->id)."' onclick='return window.confirm(\"Delete the current promo?\");'>delete</a>";

				//else echo " ---- ";
				echo "</td>";



				echo "</tr>";
					
			}

		?>
		
		
		
				
		</table>

		<div class="grid_10" style="margin-top: 0px;">
			<div style="margin-top: 0px; text-align: right; float: right;">
				<?php 
				if(count($this->paginator) > 0) 
				{
					echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); 
				}
				?>
			</div>
		</div>



	</div>



</div>	