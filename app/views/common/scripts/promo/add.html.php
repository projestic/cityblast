<?
$price_data = $this->standard_price_data;
?>

<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">

	<div class="grid_12 member_update">

		<div class="box form clearfix">
			<div class="box_section">
				<h1>Promo Code</h1>
			</div>

			<form id="extraForm" action="/promo/save" method="POST">
			<input type="hidden" name="id" value="<?php if(isset($this->promo->id)) echo $this->promo->id;?>" />

			<ul class="uiForm clearfix">
				<li class="uiFormRowHeader">Promo Details</li>
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Promo Code</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="promo_code" id="promo_code" value="<? if(isset($this->promo->promo_code)) echo $this->promo->promo_code;?>" class="uiInput" style="width: 200px" />
					    </li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Standard Prices</li>
					    <li class="field" style="padding-top: 20px;">
							<table>
								<tr>
									<td>MemberType</td>
									<td>Monthly</td>
									<td>Biannual</td>
									<td>Annual</td>
								</tr>
								<? foreach ($price_data as $price_data_row): ?>
								<tr>
									<td><?=$this->escape($price_data_row['member_type']->name)?></td>
									<td><?=$this->escape($price_data_row['prices']['month'])?></td>
									<td><?=$this->escape($price_data_row['prices']['biannual'])?></td>
									<td><?=$this->escape($price_data_row['prices']['annual'])?></td>
								</tr>
								<? endforeach; ?>
							</table>
					    </li>
					</ul>
				</li>				
				
				

				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Discount Type</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="radio" id="fixed_amount" name="type" value="FIXED_AMOUNT" <? if(isset($this->promo->type) && $this->promo->type == 'FIXED_AMOUNT') echo "checked" ?>>&nbsp;Fixed Amount &nbsp;&nbsp;
						    <input type="radio" id="percent" name="type" value="PERCENT" <? if(isset($this->promo->type) && $this->promo->type == 'PERCENT') echo "checked" ?>>&nbsp;Percent &nbsp;&nbsp;
					    </li>
					</ul>
				</li>		

				
					
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Amount of Discount</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="amount" id="amount" class="uiInput" value="<? if(isset($this->promo->amount)) echo $this->promo->amount;?>" style="width: 200px" />
						    
					    </li>
					</ul>
				</li>					

				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Discounted Prices</li>
					    <li class="field" style="padding-top: 20px;">

							<div id="typical_price">
								<table id="discounted_prices">
									<tr>
										<td>MemberType</td>
										<td>Monthly</td>
										<td>Biannual</td>
										<td>Annual</td>
									</tr>
									<? foreach ($price_data as $price_data_row): ?>
									<tr class="member_type_<?=$this->escape($price_data_row['member_type']->id)?>">
										<td><?=$this->escape($price_data_row['member_type']->name)?></td>
										<td class="month"><?=$this->escape($price_data_row['prices']['month'])?></td>
										<td class="biannual"><?=$this->escape($price_data_row['prices']['biannual'])?></td>
										<td class="annual"><?=$this->escape($price_data_row['prices']['annual'])?></td>
									</tr>
									<? endforeach; ?>
								</table>
						    </div>
					    </li>
					</ul>
				</li>					
			</ul>
			

			<ul class="uiForm clearfix">

				<li class="uiFormRowHeader">Affiliate Owner</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Pay Commission To:</li>
						<li class="field">


							
							
							<span class="uiSelectWrapper">
								<span class="uiSelectInner">
									
									
									<select name="affiliate_id" class="uiSelect">
										
										<option value=""></option>
										<? 																
											foreach($this->affiliates as $affiliate) 
											{
												$member = $affiliate->owner;
												if (!$member) {
													continue;
												}
												echo "<option value='".$affiliate->id."' ";
												if(isset($this->promo) && $this->promo->affiliate_id == $affiliate->id) echo " selected ";
												echo ">" . $member->first_name." ".$member->last_name."</option>";
											}
										?>
											
									</select>
								</span>
							</span>
							
							

						</li>
				
						
								

						
					</ul>
				</li>
			</ul>



			<ul class="uiForm clearfix">

				<li class="uiFormRowHeader">Franchise</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Franchise Franchise:</li>
						<li class="field">

							<span class="uiSelectWrapper"><span class="uiSelectInner">
							<select name="franchise_id" class="uiSelect">
								<option value=""></option>
								<?php 																
									foreach ($this->franchises as $franchise) {
										echo "<option value='".$franchise->id."' ";
										if(isset($this->promo->franchise_id) && $this->promo->franchise_id == $franchise->id) echo " selected ";
										echo ">" . $franchise->name . "</option>";
									}
								?>
							</select>
							</span></span>
								
						</li>
						
						
						<li class="fieldname"></li>
						<li class="field">
		
								<? if(isset($this->promo->id) && ($this->promo->id)) :?>
									<input type="submit" name="submitted" class="uiButton large right" value="Save" style="margin-right: 15px margin-top: 15px;">
								<? else : ?>
									<input type="submit" name="submitted" class="uiButton large right" value="Add" style="margin-right: 15px margin-top: 15px;">
								<? endif; ?>
					
								
						</li>

						
						
					</ul>
				</li>
			</ul>




			</form>
		</div>
	</div>
	
</div>

<script type="text/javascript">
$(document).ready(function()
{
	// Init type checkbox if not no option is checked
	if (0 == $('input[name="type"]:checked').length) {
		$('input[name="type"]').last().attr('checked','checked');
	}
	
	function calcDiscountPrices()
	{
		var type = $('input[name="type"]:checked').val();
		var amount = $('input[name="amount"]').val();

		var jsonUrl = "/promo/calculate_prices/type/"+encodeURIComponent(type)+"/amount/"+encodeURIComponent(amount);  
		
		if (!type || !amount) {
			return;
		}
		
		$.getJSON(  
			jsonUrl,   
			function(result) 
			{  
				if(result['error'])
				{
					/*alert('there was an error: ' + value);*/
					$.colorbox({width:"550px", height: "220px", html:"<h3 style='margin-top: 0px; padding-top: 0px;'>Error.</h3><p style='font-size: 130%; width: 480px;'>"+result['error']+"</p>"});
				} 
				else 
				{
					renderDiscountPrices(result);
				}  
			}  
		); 
	}
	
	function renderDiscountPrices(data)
	{
		$.each(data['prices'], function(member_type_id, billing_cycle_data){
			$.each(billing_cycle_data, function(billing_cycle, price){
				$('#discounted_prices .member_type_' + member_type_id + ' .' + billing_cycle).html(price);
			});
		});
	}
	
	$('#amount, input[name="type"]').change(function()
	{
		calcDiscountPrices();
		
	});
	
	calcDiscountPrices();
});
</script>


