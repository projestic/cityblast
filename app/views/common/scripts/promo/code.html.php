<div class="grid_12">
	<div class="box clearfix" style="padding-top: 30px">
		<div class="grid_7">

			<div class="clearfix">


				<? /* IN ORDER FOR STRIPES TO WORK PROPERLY, YOU CAN'T HAVE ANY ACTION IN THE FORM */ ?>
				<form action="" method="POST" id="payment-form">

					<?= $this->render('common/plan-and-payment.html.php'); ?>

				</form>

			</div>

		</div>


		<div class="grid_4">
			<?php if (!$this->member->isPaying()) : ?>
			<div class="discountOffer">
				<img class="arrowImg" src="/images/discount-offer-arrow.png" />
				<h4 class="title"><span></span>Special Discount</h4>
				<div class="content">
					<p>For a limited time, CityBlast is offering the following discount to new members. Include the Promo Code below, and secure your discounted rate.</p>
					<p><strong>You will not be billed until your free trial expires.</strong></p>
					<div class="specialPrice">
						<h5>Special Price</h5>
						<div>

							<sup>$</sup><?=$this->member->price;?>

							<span class="period">month</span>
						</div>
					</div>
					<div class="code">
						<h5>Promo Code</h5>
						<div><?=$this->promo->promo_code;?></div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<div class="contactToHelp">
				<p>Need help or have questions?</p>
				<p>Call us at 1-888-712-7888</p>
				<p class="duration">(*9:00 am to 5:00 pm EST)</p>
			</div>
		</div>
	</div>
</div>



<div style="display: none;">
	
	<div id="login_popup" style="width: 540px; height: 250px;">
	    	   		
		<h1>Start Saving 20% Today!</h1>
		<p>That's right, you can start saving a whopping 20% for the lifetime of your membership!<p>
		<p>Wait, there's more! We're also going to give you 60 days FREE.</p> 
		<p style="margin-bottom: 10px;">All you have to do is ACTIVATE your account today. It's as simple as clicking a button!</p>
		
		
		<input type="button" class="uiButton right facebook-login" style="width: 395px;" value="Yes, I want to start my 60 day free trial" />
		<?/*<p style="text-align: right;"><input type="button" class="uiButton facebook-login">Yes, I want to start my 60 day free trial</a></p>*/?>

	</div>
	
</div>



<? if(empty($_SESSION['member'])) : ?>

<script type="text/javascript">
	function showLoginButton() {
		$.colorbox({width:"660px", height: "400px", inline:true, href:"#login_popup",
		onClosed: function() {		
			showLoginButton();
		}});
	}
	
	$(document).ready(function () {
		showLoginButton();
	});
</script>

<? endif; ?>