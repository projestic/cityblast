<div class="row-fluid" style="<?=$this->bgcolor;?> padding-top: 10px; padding-bottom: 10px;">
	<link rel="stylesheet" type="text/css" href="/css/facepile.css" />
	<h4 style="color: #808080; margin-left: 10px; margin-top: 0;">Here Are Just Some of the Thousands of Agents We Help Every Day!</h4>
	<div class="mvs">
		<span style="padding-left: 19px;" class="uiIconText">
			<i class="img sp_favicon sx_favicon_fav" style="top: 0px;"></i>
			<span class="fsl">
				<span style="color: #b2b2b2;">
					<a href="https://www.facebook.com/<? echo $this->member_list[0]->uid; ?>" target="_blank" style="color: #fff;"><? echo $this->member_list[0]->full_name; ?></a>, 
					<a href="https://www.facebook.com/<? echo $this->member_list[1]->uid; ?>" target="_blank" style="color: #fff;"><? echo $this->member_list[1]->full_name; ?></a> 
					and <a href="https://www.facebook.com/<? echo $this->member_list[2]->uid; ?>" target="_blank" style="color: #fff;"><? echo $this->member_list[2]->full_name; ?></a>
					use <a href="<?=APP_URL;?>" target="_blank" style="color: #fff;"><?=COMPANY_WEBSITE;?></a>.
				</span>
			</span>
		</span>
	</div>
	
	<div class="uiFacepile uiFacepileLarge" style="margin-left: 10px;">
		<ul class="uiList uiFacepileList _4ki clearfix ">
			<? foreach ($this->member_list as $m) {?>
			<li class="uiFacepileItem uiListItem" style="margin-right: 5px;">
				<a class="link" title="<?=$m->full_name?>" href="https://facebook.com/<?=$m->uid?>" data-jsid="anchor" target="_blank">
					<img class="_s0 _rw img" src="https://graph.facebook.com/<?=$m->uid?>/picture?return_ssl_resources=1" alt="<?=$m->full_name;?>" data-jsid="img" />
				</a>
			</li>
			<? } ?>
		</ul>
	</div>

</div>
