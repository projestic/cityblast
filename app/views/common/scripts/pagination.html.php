<?php
/**
 * this is a dynamic view script for any pagination
 */

/**
 * we need the route on which paging urls will be build
 */
if (Zend_Registry::isRegistered('pagingUrlOptions')) :
    $pagingUrlOptions   = Zend_Registry::get('pagingUrlOptions');
    var_dump($pagingUrlOptions);
    /**
     * every pagination should have route and so registry variable should contains a route name
     */
    $pagingRoute    = $pagingUrlOptions['route'];
    /**
     * pagination route may have one or more dynamic parameters rather than only "page"
     * in such case other parameters (except "page" which will be sort out by paginator itself) must have passed as an associative
     * array under "urlOptions" index of registry variable
     */
    $urlOptions     = isset($pagingUrlOptions['urlOptions']) ? (array) $pagingUrlOptions['urlOptions'] : array();
?>
    <?php if ($this->pageCount > 1): ?>
	<link type="text/css" rel="stylesheet" href="/css/pagination.css" />
    <center>
        <div class="pagination">

            <ul id="pagination-entry-results">

                <li class="literal"><?php echo $this->firstItemNumber?> - <?php echo $this->lastItemNumber?>
                of <?php echo $this->itemCount?></li>

                <?php if (!empty($this->previous)): ?>
                    <?php $urlOptions['page'] = $this->first ?>
                <li><a href="<?php echo $this->url($urlOptions, $pagingRoute)?>">First</a></li>
                <?php else: ?>  <li class="off">First</li><?php endif ?>

                <?php if (!empty($this->previous)): ?>
                    <?php $urlOptions['page'] = $this->previous ?>
                <li class="previous"><a href="<?php echo $this->url($urlOptions, $pagingRoute)?>">&lt; Previous</a></li>
                <?php else: ?>  <li class="previous-off">&lt; Previous</li><?php endif ?>


                <?php foreach ($this->pagesInRange as $page): ?>
                    <?php if ($page != $this->current): ?>
                        <?php $urlOptions['page'] = $page ?>
                    <li><a href="<?php echo $this->url($urlOptions, $pagingRoute); ?>"><?php echo $page?></a></li>  <?php else: ?>
                    <li class="active"><?php echo $page ?></li>
                    <?php endif ?>
                <?php endforeach ?>

                <?php if (!empty($this->next)): ?>
                    <?php $urlOptions['page'] = $this->next ?>
                <li class="next"><a href="<?php echo $this->url($urlOptions, $pagingRoute)?>">Next &gt;</a></li>
                <?php else: ?>  <li class="next-off">Next &gt;</li><?php endif ?>

                <?php if (!empty($this->next)): ?>
                    <?php $urlOptions['page'] = $this->last ?>
                <li><a href="<?php echo $this->url($urlOptions, $pagingRoute)?>">Last</a></li>
                <?php else: ?>  <li class="off">Last</li><?php endif ?>

            </ul>
        </div>
    </center>
    <?php endif ?>
<?php else : ?>
    <h4><center>Paging Error: Undefined registry variable 'pagingRoute'!</center></h4>
<?php endif ?>
