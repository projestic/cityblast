<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"; "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
  <title>Zend Framework Default Application</title> 
</head> 
<body> 
  <h1>An error occurred</h1> 
  <h2><?= $this->message ?></h2> 
  
  <h3>Exception information:</h3> 
  <p> 
      <b>Message:</b> <?= get_class($this->exception) . ':' . (method_exists($this->exception, 'getCode') ?  $this->exception->getCode() : '' ) . ' ' . (method_exists($this->exception, 'getSubcode') ?  $this->exception->getSubcode() : '' ) . ' ' . $this->exception->getMessage()?> 
  </p> 

  <h3>Stack trace:</h3> 
  <pre><?= $this->exception->getTraceAsString() ?> 
  </pre>
  
  <? if(isset($this->sql)): ?>
  <h3>SQL:</h3> 
  <pre><?=($this->sql); ?> 
  </pre>
  
  <h3>SQL Args:</h3> 
  <pre><?=(print_r($this->sql_args, true)); ?> 
  </pre>
  <? endif; ?>
  
  <h3>Request Parameters:</h3> 
  <pre><? var_dump($this->request->getParams()) ?> 
  </pre>
  
  <h3>Server</h3>
  <pre><? var_dump($_SERVER) ?>
  </pre>
  
</body> 
</html>
