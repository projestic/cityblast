<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Oh Dear, There Was a Problem.</h1>
		</div>
				
		<p>Unfortunately we couldn't find the page you are looking for. Why not head back to the <a href="/">home page</a> and try again?</p>


		<a href="/" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" id="submitBtn"><?=COMPANY_NAME;?> Home</a>
		
		<div class="clearfix"></div>

	</div>
</div>