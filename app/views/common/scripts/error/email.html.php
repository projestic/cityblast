<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=640, user-scalable=yes">
    <style>
      * {
  margin: 0;
  padding: 0;
  border: none;
}
body {
  font-family: Trebuchet MS, Arial, sans-serif;
  font-size: 95%;
  background-color: #333;
  margin: 1em;
}
.header, .section, .footer {
  -webkit-border-radius: 0.5em;
  margin-bottom: 1em;
}
.header {
  display: table;
  border-collapse: collapse;
  position: relative;
  width: 100%;
}
.header #application_name {
  vertical-align: middle;
  text-align: center;
  font-size: 2em;
  font-weight: bold;
  border-left: 0.1em solid #333;
  white-space: nowrap;
  color: white;
  padding: 0.55em;
  background-color: #333;
}
.header #summary_table {
  background-color: #EFBE20;
}
.header #summary {
  padding-left: 1em;
  padding: 1em;
  font-size: 1.25em;
  width: 100%;
  background: -webkit-gradient(linear, left top, left bottom, from(#FCD41E), to(#EF9520));
  background-color: #EFBE20;
  -webkit-border-radius: 0.5em;
}
.header #summary p.message {
  font-weight: bold;
  padding-top: 1em;
  padding-left: 2em;
}
.section {
  display: block;
  background-color: white;
  padding: 1em;
  color: #555;
}
.section h1 {
  font-size: 1.25em;
  border-bottom: 1px solid #ccc;
  padding-bottom: 0.75em;
  margin-bottom: 0.75em;
}
.section#request .method {
  font-size: 0.6em;
  background-color: #333;
  padding: 0.3em 0.6em;
  -webkit-border-radius: 0.5em;
  color: white;
}
.backtrace {
  font-family: Courier, monospace;
  line-height: 1.75em;
  font-size: 1.25em;
}
table {
  border-collapse: collapse;
  width: 100%;
}
table tr:nth-child(odd) {
  background-color: white;
}
table tr:nth-child(even) {
  background-color: #eee;
}
table th,
table td {
  padding: 0.5em;
  vertical-align: middle;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  max-width: 1em;
}
table th {
  text-align: right;
  width: 125px;
}

.section#source li {
  list-style: none;
  white-space: pre;
  line-height: 1.75em;
  text-overflow: ellipsis;
  overflow: hidden;
  font-family: Courier;
}
.section#source li:before {
  content: attr(line_number);
  -webkit-user-select: none;
  padding-left: 0.75em;
  padding-right: 1.5em;
}
.section#source li.error_line {
  background: -webkit-gradient(linear, left top, left bottom, from(#FCD41E), to(#EF9520));
  background-color: #EFBE20;
  -webkit-border-radius: 0.5em;
}
.section#source span.attribute { color: #009900; }
.section#source span.char { color: #F00; }
.section#source span.class { color: #A020F0; font-weight: bold; }
.section#source span.comment { color: #0000FF; }
.section#source span.constant { color: #008B8B; }
.section#source span.escape { color: #6A5ACD; }
.section#source span.expr { color: #2222CC; }
.section#source span.global { color: #11AA44; }
.section#source span.ident { color: #000000; }
.section#source span.keyword { color: #A52A2A; font-weight: bold; }
.section#source span.method { color: #008B8B; }
.section#source span.module { color: #A020F0; font-weight: bold; }
.section#source span.number { color: #DD00DD; }
.section#source span.punct { color: #6A5ACD; }
.section#source span.regex { color: #DD00DD; }
.section#source span.string { color: #DD00DD; }
.section#source span.symbol { color: #008B8B; }
    </style>
  </head>
  <body>
    <div class="header">
      <table>
        <tr>
          <td width="80%" id="summary_table">
          <div id="summary">
            <p>
              Our beloved application needs a little attention
            </p>
            <p class="message">
            	<? if(isset($this->exception)) :?>
              		<?= get_class($this->exception) . ": " . $this->exception->getMessage() ?>
              	<? endif; ?>
            </p>
          </div>
          </td>
          <td id="application_name">
                <?=COMPANY_WEBSITE;?>
          </td>
        </tr>
      </table>
    </div>
    <div class="section" id="backtrace">
      <h1>
        Backtrace
      </h1>
      <table>
        <tbody>
          <tr>
            <td class="backtrace">
      <? 
        $trace = $this->exception->getTrace();
        foreach($trace as $line)
          echo $line['file']  . ":" . $line['line'] . " in <strong>" . $line['function'] . "()" . "</strong><br />";
      ?>
                            
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <? if(isset($this->sql)): ?>
    <div class="section" id="sql">
      <h1>
        SQL
      </h1>
      <table>
        <tbody>
          <tr>
            <th title="SQL">SQL</th>
            <td><?= $this->sql; ?></td>
          </tr>
          <tr>
            <th title="SQL Args">SQL Args</th>
            <td><?= print_r($this->sql_args, true); ?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <? endif; ?>
    <div class="section" id="params">
      <h1>
        Request Params
      </h1>
      <table>
        <tbody>
          <? foreach($this->request->getParams() as $key => $value): ?>

          
          	<? if(is_array($value)) : ?>
          		
          		<tr><th title="<?=$key ?>"><?=$key ?></th>
          			<td>Values:<br/>
          			
          			<table>
          			<? foreach($value as $sub_key => $sub_value): ?>
				             <tr>
				              <th title="<?=$sub_key ?>"><?=$sub_key ?></th>
				              <td><?=$sub_value ?></td>
				            </tr>
				          <? endforeach; ?>         			
          			
          			</table>
          			
          			</td>	
          	<? else : ?>
	            <tr>
	              <th title="<?=$key ?>"><?=$key ?></th>
	              <td><?=$value ?></td>
	            </tr>
	        <? endif; ?>
	        
          <? endforeach; ?>
        </tbody>
      </table>
    </div>
    <div class="section" id="environment">
      <h1>
        Environment
      </h1>
      <table>
        <tbody>
          <? foreach($_SERVER as $key => $value): ?>
            <tr>
              <th title="<?=$key ?>"><?=$key ?></th>
              <td><?=$value ?></td>
            </tr>
          <? endforeach; ?>
        </tbody>
      </table>
    </div>
    <? if(isset($_SESSION)): ?>
    <div class="section" id="session">
      <h1>
        Session
      </h1>
      <table>
        <tbody>
          <? foreach($_SESSION as $key => $value): ?>
            <tr>
              <th title="<?=$key ?>"><?=$key ?></th>
              <td><?=var_dump($value); ?></td>
            </tr>
          <? endforeach; ?>
        </tbody>
      </table>
    </div>
    <? endif; ?>
    <? if(isset($_COOKIE)): ?>
    <div class="section" id="cookie">
      <h1>
        Cookie
      </h1>
      <table>
        <tbody>
          <? foreach($_COOKIE as $key => $value): ?>
            <tr>
              <th title="<?=$key ?>"><?=$key ?></th>
              <td><?=$value ?></td>
            </tr>
          <? endforeach; ?>
        </tbody>
      </table>
    </div>
    <? endif; ?>
  </body> 
</html>
