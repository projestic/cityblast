<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Oh Dear, There Was a Problem.</h1>
		</div>
				
				
		<p>There was an error processing your request. Our development staff has already been notified and we're working hard to resolve the issue.</p>

		<p>While we fix it, why head back to the <a href="/">home page</a> and try something different?</p>

		<a href="/" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;" id="submitBtn"><?=COMPANY_NAME;?> Home</a>
		
		<div class="clearfix"></div>

	</div>
</div>