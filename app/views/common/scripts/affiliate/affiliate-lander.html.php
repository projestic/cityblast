<?php if(!empty($this->logo)): ?>
<div class="row-fluid">
	<div class="contentBox" style="text-align: center; font-size: 48px; font-weight: bold;">		
		<?= $this->logo; ?> + <img src="/images/<?= strtolower(COMPANY_NAME) ?>_logo_plain.png" style="padding-top: 10px;" alt="City Blast" /> = <img src="/images/heart.png" alt="Love" />		
	</div>
</div>
<?php elseif ($this->affiliate) : ?>
<div class="row-fluid">
	<div class="contentBox" style="text-align: center; font-size: 48px; font-weight: bold;">
		<?php if ($this->affiliate->logo): ?>
			<img src="<?=CDN_URL . $this->affiliate->logo?>" style="padding-top: 10px;" />
		<?php else: ?>
			<?= "{$this->affiliate->owner->first_name} {$this->affiliate->owner->last_name}" ?> 
		<?php endif; ?>
		+ <img src="/images/<?= strtolower(COMPANY_NAME) ?>_logo_plain.png" style="padding-top: 10px;" alt="City Blast" /> = <img src="/images/heart.png" alt="Love" />		
	</div>
</div>
<?php endif; ?>

<div class="row-fluid">
	<div class="bannerContainer">
		
		<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
			<a href="//www.youtube.com/embed/Ysi4-i_gOJc?rel=0&autoplay=1" id="explain_video" class="cboxElement">
		<? else : ?>
			<a href="//www.youtube.com/embed/OCXo__6Q0Xw?rel=0&autoplay=1" id="explain_video" class="cboxElement">
		<? endif; ?>
			<img src="/images/cityblast-banner-bg.jpg" />
			<span class="bannerInfo">
				<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
					<span class="bannerTitle">Are You Losing Valuable Real Estate Commissions?</span>
				<? else : ?>
					<span class="bannerTitle">Are You Losing Valuable Mortgage Commissions?</span>
				<? endif;?>					
				
				<span class="bannerSubTitle">A <?=COMPANY_NAME;?> Social Media Expert can help, see how.</span>
				<span class="playButton"><span>Play Video</span></span>
			</span>
		</a>
	</div>
	
	<?/*<iframe class="videoPlayer" src="//www.youtube.com/embed/8T6V3PZi8G0?rel=0" frameborder="0" allowfullscreen></iframe>*/?>

	<?/*
	<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
		<iframe class="videoPlayer" src="//www.youtube.com/embed/Ysi4-i_gOJc?rel=0" frameborder="0" allowfullscreen></iframe>
	<? else: ?>
		<iframe class="videoPlayer" id="viddler-f74c47ef" src="//www.viddler.com/embed/f74c47ef/?f=1&autoplay=0&player=full&secret=22300716&loop=false&nologo=false&hd=false" frameborder="0" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
	<? endif; ?>
	*/?>

</div>



	


<? echo $this->render('member/index.html.php'); ?>
