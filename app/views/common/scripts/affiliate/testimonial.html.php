<div class="contentBox testimonialsContainer">
	
	<div class="testimonialsDesc">
		<span class="openQutote">&ldquo;</span>

		<?php if ($this->member->testimonials->testimonial) : ?>
			<p class="text-center"><?= $this->member->testimonials->testimonial ?></p>
		<?php else: ?>
			<p class="text-center">My friends noticed my Facebook updates right away. &nbsp;In only my first week using <?=COMPANY_NAME;?>, I received a call from a family friend who said she'd seen my Facebook and wanted to talk to me
			about how I could help! Social Media has never been so easy. I just told my <i>Social Expert</i> what type of posts I wanted and <strong><?=COMPANY_NAME;?> did the rest!</strong></p>		
		<?php endif; ?>
		<span class="closeQutote">&rdquo;</span>
	</div>

	<div class="clearfix memberInfo">
		<div class="pull-left">
			<div class="media">

				<div class="pull-left memberImg">
					<img class="media-object" src="<? 
						
						//Do not use the profilePic here pls -- it's not accurate
						//$this->profilePic($this->member) 
						
						if ($this->member->photo) echo CDN_URL . $this->member->photo;
						else echo "https://graph.facebook.com/". $this->member->uid . "/picture?type=small&redirect=true";
						
						?>" >
				</div>
				<div class="media-body">
					<h5 class="media-heading"><?= $this->member->name() ?></h5>
					<span><?= $this->member->brokerage ?></span>

				</div>
			</div>
		</div>
		<h5 class="pull-left trialInfo">A free 14-Day Trial awaits you below, it's as east as 1-2-3!</h5>
	</div>

	<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" style="display: inline;" />			

</div>