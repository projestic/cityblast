<form name="upfrontsettings" id="upfrontsettings" onsubmit="return false;" method="POST" style="margin: 0px !important; padding: 0px !important;">
<input type="hidden" name="locality" value="" id="locality" /><? /* locality == city */ ?>
<input type="hidden" name="administrative_area_level_1" value="" id="administrative_area_level_1" /><? /* administrative_area_level_1 == state/province */ ?>
<input type="hidden" name="country" value="" id="country" />
	

<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>


<div class="contentBox">
	
	<div class="contentBoxTitle">
		<h1>Are You Ready To Turbo-Charge Your Earnings?</h1>
	</div>


	<div class="strategyContainer">
		<p>Our <a href="/"><?=COMPANY_NAME;?></a> affliate program is very simple and takes only a moment to set up. You'll be underway in no time, and can begin referring your clients and customers to <a href="/"><?=COMPANY_NAME;?></a>. Our largest current affiliates
		earn thousands of dollars every month in recurring commissions, and get nothing but positive praise back from their clients and friends regarding the service. If you'd like a testimonial, let us know!</p>
	
	
		<h3 style="margin-top: 20px;">How it works</h3>
	
		<p>For every member you refer to <a href="/"><?=COMPANY_NAME;?></a>, we pay you monthly commissions for the duration of that member's tenure. Commission amounts are calculated based on the total charges paid by your referred members.</p>
			
		<p><span  style="background-color: #ffff00;">We pay a <u><i>recurring</i></u> commission to our affiliates for the <u><i>lifetime</i></u> of the membership!</span></p>
			
		<p>Your commissions are calculated every month, and paid the following month. Easy!</p>
	
		<h3 style="margin-top: 20px;">How we track</h3>
		
		<p>We track your referrals 2 ways:
			
			<ol style="margin-bottom: 18px; margin-bottom: 1.5em; margin-left: 5.2em;">
				<li style="margin-bottom: 10px;list-style-type: decimal;">You get your own custom landing page and URL on our site. Any new members that sign up via the aliate landing page URL will be automatically tracked to you and will be commissionable.</li>
				<li style="margin-bottom: 10px;list-style-type: decimal;">Additionally, you are provided with a unique promotional discount code that can be offered to prospects. Any new member registering via your landing page OR using your promo code will automatically count towards your commissions.</li>
			</ol>
		</p>	

		<p>We also offer a generous <u>60 day tracking cookie</u> on all leads generated through your Affiliate link. No more losing leads!</p>

	</div>


	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 28px;">You're one click away.</h3>
			<h4>Get your <?=COMPANY_NAME;?> affiliate link and start earning now.</h4>
		</div>
		<div class="pull-right">
			<?/*<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>*/?>
			<a href="javascript:void(0);" class="uiButtonNew tryDemo facebook-signup-affiliate" id="tryusnow_step1_btn">Earn Now!</a>
		</div>
	</div>



	
	<div class="clearfix"></div>
	
</div>
	




	<?/***
	<div class="contentBox " id="step1">

		
		<div class="stepBox step1">
			<div class="stepTitle">
			</div>
			<div class="stepContent clearfix" style="margin-top: 30px;">
				<div class="ladyCartoon pull-left">
					<img src="/images/new-images/step2-lady-cartoon.png">
				</div>
				<div style="overflow: hidden;">
					<ul class="uiForm full">
						<li class="clearfix">
							<ul>
								<li class="fieldname cc">
									<h2>Become a CityBlast affiliate</h2><br/>
									Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.
								</li>
							</ul>
						</li>
						<li class="clearfix">
							<div class="map_canvas"></div>
						</li>
						<li class="uiFormRow clearfix">
							<ul class="clearfix">
								<li class="clearfix">
									<hr />
									<a href="javascript:void(0);" class="btn btn-primary btn-large pull-right facebook-signup-affiliate" id="tryusnow_step1_btn">Sign up with Facebook</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		

	</div>
	***/ ?>
	
	
<script type="text/javascript">


		$('.facebook-signup-affiliate').click(function(event){

		   FB.login(function(response) 
		   {

				if (response.authResponse)
				{
						window.location.href="/affiliate/signup/?access_token=" + response.authResponse.accessToken + "&member_type=affiliate";
				} else {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>Either your login failed, or you didn't authorize us to access your Facebook account. Please be sure to click \"Yes\" in the Facebook popup in order to proceed.</p><p><a href=\"#\" class=\"btn facebook-retry\">Retry</a></p>"});
					bindFacebookRetry();
				}

			}, {scope: '<?php echo FB_SCOPE;?>'});		              
		
		});

</script>



	<div class="grid12" style="text-align: center; margin-bottom: 10px;">
		<a href="http://www.affiliateranker.com" style="font-size: 10px;">Affiliate Ranker</a>&nbsp;&nbsp;&nbsp;
		<a href="http://www.affiliateguide.com" style="font-size: 10px;">Affiliate Directory and Guide</a>&nbsp;&nbsp;&nbsp; 
		<a href="http://www.twistdirectory.com" style="font-size: 10px;">Twist Directory</a>
	</div>

	<? echo $this->render('logos-responsive-small.html.php');	?>
	<div style="margin-bottom: 25px;">&nbsp;</div>

	
</form>

