<?/*
<div class="grid_12">
    <div style="padding: 0;" class="box white clearfix">
        <h1 style="text-align: center; margin: 0; line-height: 60px; color: #666666">Need assistance? Call us at <span style="color: #333333;">1-888-712-7888</span>
        	<?/* or <a href="#">Live Chat Now!</a>*?></h1>
    </div>
</div>
*/?>

<script type="text/javascript">
var fb_param = {};
fb_param.pixel_id = '6006773476030';
fb_param.value = '0.00';
(function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = '//connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
})();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6006773476030&amp;value=0" /></noscript>


<form action="" method="POST" id="infoform">
        		
        		

<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
        		
            	<h1>Account Information.</h1>


            	
        	</div>


			<p>Please pick your custom affiliate link. It can be whatever you like as long as it hasn't been used by another affiliate already.</p> 
			
			<p>We also need your best business email and phone number so that we can stay in touch!</p>


        		<input type="hidden" name="first_name" value="<?= $this->member->first_name;?>"  />
			<input type="hidden" name="last_name" value="<?= $this->member->last_name;?>" />
        	

   		        		
			<ul class="uiForm full" style="margin-top: 15px; padding: 20px;">
			
			
				<li class="clearfix">

					<input type="hidden" name="slug-valid" value="" />

					<ul style="margin-top: 15px;">
						<li class="fieldname cc"><span style="color: #990000;">*</span>Your Custom Signup Page URL <small id="slug-message" style="float:right; font-style:italic; font-size: 11px;">Must be at least three characters. Only alphanumeric digits and dashes allowed.</small>
						<li class="field cc clearfix" style="position: relative">
							<i id="slug-ok" class="icon-ok" style="display: none; float: left; margin-left: -25px; margin-top: 7px; font-size: 18px; color: #090"></i> 
							<i id="slug-invalid" class="icon-ban-circle" style="display: none; float: left; margin-left: -25px; margin-top: 7px; font-size: 18px; color: #900"></i> 
							<span style="position: absolute; font-size: 16px; font-weight: normal; top: 9px; left: 30px; z-index: 10; color: #999">http://www.cityblast.com/</span><input type="text" name="slug" id="slug" value="<?php if ($this->member->affiliate_account) : ?><?= $this->member->affiliate_account->slug ?><?php endif; ?>" class="uiInput<?php if (in_array('slug', $this->error_fields)) : ?> error<?php endif; ?>" style="padding-left: 195px !important; width: 560px;" />
						</li>
					</ul>

															
					<ul>
						<li class="fieldname cc"><span style="color: #990000;">*</span>Business Email Address
							<?/*<span class="fieldhelp">The best email address to send leads to.</span>*/?>
						</li>
						<li class="field cc clearfix">
							<input type="text" name="email" id="email" value="<?= $this->member->email;?>" class="uiInput <?php if (in_array('email', $this->error_fields)) : ?> error<?php endif; ?>" />
						</li>
					</ul>
					
					<ul style="margin-top: 15px;">
						<li class="fieldname cc"><span style="color: #990000;">*</span>Phone Number
						<?/*<span class="fieldhelp">A phone number for interested buyers.</span>*/?></li>
						<li class="field cc clearfix">
							<input type="text" name="phone" id="phone" value="<?= $this->member->phone;?>" class="uiInput<?php if (in_array('phone', $this->error_fields)) : ?> error<?php endif; ?>" />
						</li>

					</ul>

					<ul style="margin-top: 15px;">
						<li class="fieldname cc"><span style="color: #990000;">*</span>Testimonial<small id="testimonial-message" style="float:right; font-style:italic; font-size: 11px;">100-500 characters. Tell your referrals why you love CityBlast!</small>
						<li class="field cc clearfix">
							<textarea maxlength="500" name="testimonial" class="uiTextarea<?php if (in_array('testimonial', $this->error_fields)) : ?> error<?php endif; ?>" style="height: 100px !important"><?= ($this->member ? $this->member->testimonials->testimonial : '') ?></textarea>
						</li>
					</ul>

					<ul style="margin-top: 15px;">
						<li class="fieldname cc"><span style="color: #990000;">*</span>Your logo (optional, maximum 335px wide, 55px high)
						<li class="field cc clearfix">
							<div class="photo_upload_thumb" style="width: 345px; height: 65px;">
								<div id="logo_upload_progress" class="progress"></div>
								<?php if ($this->member->affiliate_account->logo) : ?>
								<img src="<?= CDN_URL . $this->member->affiliate_account->logo ?>" id="logo_upload_image" />
								<?php else : ?>
								<img src="" id="logo_upload_image" style="display: none;"/>
								<?php endif; ?>
							</div>
							<input id="logo_upload" name="logo_upload" type="file" width="335" height="104" />
							<span id="logo_status" class="uploadstatus" style="float: left;"></span>
							<input type="hidden" name="logo_uploaded" id="logo_uploaded" />

							<script type="text/javascript">
								$(document).ready(function() {
									$('#logo_upload').uploadify({
										'uploader'  : '/js/uploadify/uploadify.swf',
										'script'    : '/affiliate/uploadlogo',
										'cancelImg' : '/js/uploadify/cancel.png',
										'folder'    : '/images/',
										'auto'      : true,
										'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
										'fileDesc'  : 'Image Files',
										'buttonImg' : '/images/browse_button.png',
										'removeCompleted' : false,
										'height' : 30,
										'width' : 106,
										'name': 'logo_uploaded',
										'onComplete': function(event, ID, fileObj, response, data)
										{
											if (response.indexOf('/tmp/') == -1) {
												$.colorbox({href:false, innerWidth:420, initialWidth:450, initialHeight:200, html: response});
											} else {
												$('#logo_uploaded').val(response);
												$("#logo_status").show();
												$("#logo_status").html("<span style='display: block; text-align: center;'>Upload Complete</span>");
												$('#logo_upload_image').attr('src', response).show();

												$('#logo_upload_progress').css('display', 'none');
											}
										},

										'onError'     : function (event, ID, fileObj, errorObj) {
											$.colorbox({href:false, innerWidth:420, initialWidth:450, initialHeight:200, html: errorObj.info});
										},

										'onProgress'  : function(event,ID,fileObj,data)
										{
											$('#logo_upload_image').attr('src', '').hide();
											$("#logo_upload_status").html("<span style='display: block; text-align: center;'>Uploaded " + data.percentage + "% &hellip;</span>");
											$('#logo_upload_progress').css('display', 'block');
											$('#logo_upload_progress').css('width', 335).css('height', Math.round(data.percentage/100 * 65) + 'px');
											return false;
										}

									});
								});
							</script>
						</li>
					</ul>

				</li>
				
				
				<li class="uiFormRow clearfix">
					<ul class="clearfix">
						<li class="clearfix span12">
							<input type="button" class="btn btn-primary btn-large pull-right" id="submitform" style="width: 195px;" id="member_info_save_btn" value="Save Info" />
						</li>
					</ul>
				</li>
			</ul>
   		</ul>
   			
    </div>

</div>

</form>





<div style="display: none;">
	
	<div id="help_video_popup" style="width: 640px; height: 480px;">
	    	   		
		<iframe width="640" height="480" src="//www.youtube.com/embed/0hskCF0TWPo?rel=0" frameborder="0" allowfullscreen></iframe>	

	</div>
	
</div>




<script type="text/javascript">
<!--

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

</script>
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

		var slug_validate;

		$('#slug').bind('keyup', validate_slug)
				  .bind('blur', validate_slug);

		function validate_slug() {
			
			var slug = $(this).val();
	
			$(this).val( $(this).val().replace(/[^A-Za-z0-9\-]/, '') );
	
			if (slug_validate) clearTimeout(slug_validate);
	
			slug_validate = setTimeout( validate, 100 );
	
			function validate() {
				$('#slug-valid').val('');
				$('#slug-ok, #slug-invalid').hide();
				$('#slug-message').html('');
				$.get('/member/validate_affiliate_slug?slug=' + slug, function (response) {
					if ($('#slug').val() == response.slug) {
						if (response.allowed) {
							$('#slug-valid').val(1);
							$('#slug-ok').show();
							$('#slug-message').css('color', '#090');
							$('#slug-valid').hide();
						} else {
							$('#slug-valid').val(0);
							$('#slug-invalid').show();
							$('#slug-message').css('color', '#900');
						}
						$('#slug-message').html(response.message);
					}
				}, 'json');
			}

		}

		$( "#submitform" ).click(function( event ) 
		{			
			event.preventDefault();

			var phone = $("#phone").val();
			var email = $("#email").val();
			var slug  = $("#slug").val();
			var slug_valid  = $("#slug_valid").val();
			
			if (validateEmail(email) == false)
			{
				$("#email").addClass('error');
				
				var htmldata = '<div><div class="clearfix"><h3>Oops!</h3><p style="margin-top: 15px;">A valid <b>email address</b> is required.</p><p>It will be used to connect you with other agents who have interested buyers for your listings. It will also be used by buyers who are interested in listings that appear on your wall.</p><a href="#" class="btn btn-warning pull-right" onclick="$.colorbox.close();">Enter your email address now</a></p></div></div>';
				$.colorbox({width:"650px", height: "290px", html:htmldata});
				
				return false;
			}
			
			if (phone.length < 4){
				var htmldata = '<div><div class="clearfix"><h3>Oops!</h3><p style="margin-top: 15px;">Your <b>phone number</b> is required!</p><p>It will be used to connect you with other agents who have interested buyers for your listings. It will also be used by buyers who are interested in listings that appear on your wall.</p><a href="#" class="btn btn-warning pull-right" onclick="$.colorbox.close();">Enter your phone number now</a></p></div></div>';
				$.colorbox({width:"650px", height: "290px", html:htmldata});
				$("#phone").addClass('error');
				return false;
			}			

			if (slug.length < 3 || slug_valid == 0){
				var htmldata = '<div><div class="clearfix"><h3>Oops!</h3><p style="margin-top: 15px;">You must choose a valid <b>signup URL</b>!</p><p>You will give out your signup URL to people you want to refer to CityBlast. URLs may only include letters, numbers and dashes.</p><a href="#" class="btn btn-warning pull-right" onclick="$.colorbox.close();">Enter a signup URL</a></p></div></div>';
				$.colorbox({width:"650px", height: "290px", html:htmldata});
				$("#phone").addClass('error');
				return false;
			}			
			
			$("#infoform").submit();

		});


        
    });
</script>