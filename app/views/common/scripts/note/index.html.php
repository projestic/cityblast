<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		<table style="width: 100%; border-bottom: 0px;">
		<tr>
			<td style="border-bottom: 0px;">
				<h1>
					Notes
				</h1>
			</td>

			<td style="text-align: right;border-bottom: 0px;">
				<a href="/note/add/id/<?=$this->member->id;?>/" class="uiButton" style="float: right;">Add Note</a>
			</td>
			
		</tr>
		</table>
		
	
		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>ID</th>
				<th>Member</th>
				<th>Admin</th>
				<th>Note</th>
				<th>Created</th>
				<th style="text-align: center;">Actions</th>
			</tr>
			<? 

			$row = true;
			$row_class = 'odd';
			
			foreach($this->paginator as $note)
			{
				$row_class = empty($row_class) ? 'odd' : '';
			
?>

			<tr class="<?=$row_class?>">
				<td><?=$note->id?></td>
				<td><a href="/admin/member/<?=$note->member_id?>" style="color: #3D6DCC;"><? 
					if(!empty($note->mfirst)) echo $note->mfirst . " ";
					if(!empty($note->mlast)) echo $note->mlast;
				?>
				</td>
				<td>
				<? if($note->admin_id): ?>
					<a href="/admin/member/<?=$note->admin_id?>" style="color: #3D6DCC;"><? 
					if(!empty($note->afirst)) echo $note->afirst . " ";
					if(!empty($note->alast)) echo $note->alast;
					?>
				<? else: ?>
				-
				<? endif;?>
				<td><?=$note->note?></td>
				<td><?=$note->created_at->format('Y-M-d H:i:s')?></td>

				<td style='text-align: right;' nowrap>
					<a href='/note/delete/id/<?=$note->id;?>' onclick='return window.confirm("Delete this note?");'>delete</a>
				</td>
				
			</tr>
<?php
			}

		?>
				
		</table>


		<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />


	</div>



</div>