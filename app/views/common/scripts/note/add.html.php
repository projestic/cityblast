<link href="/css/forms.css" rel="stylesheet" type="text/css" />

<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">
	
	<div class="grid_12 member_update">
		<table style="width: 100%; border-bottom: 0px;">
		<tr>
			<td style="border-bottom: 0px;">
				<h1><?php echo $this->member->first_name.' '.$this->member->last_name; ?></h1>
			</td>

			<td style="text-align: right;border-bottom: 0px;">
				<a href="/admin/member/<?=$this->member->id;?>#notes" class="uiButton" style="float: right;">Notes</a>
			</td>
			
		</tr>
		</table>
		
		<? if(!empty($this->error_messages)): ?>
			<? foreach($this->error_messages as $error_message): ?>
				<div style="color:red;"><?=$error_message?></div>
			<? endforeach; ?>
			<br/>
		<? endif; ?>
		
		<div class="box form clearfix">
			
			<div class="box_section">
				<h1> Add Note</h1>
			</div>

			<form id="noteForm" action="/note/add/id/<?php echo $this->member->id;?>" method="POST">
				<input type="hidden" name="member_id" value="<?php echo $this->member->id;?>" />

				<ul class="uiForm clearfix">
					<li class="uiFormRow clearfix">
						<ul>
						    <li class="fieldname">Note</li>
						    <li class="field" style="padding-top: 20px;">
						    	<textarea name="note" id="note" class="uiTextarea validateNotempty" rows="4" cols="50" style="height: 72px;"></textarea>
						    </li>
						</ul>
					</li>					
					
				</ul>
				
				<input type="submit" name="submitted" class="uiButton large right" value="Add" id="submitBtn">
			</form>
				
		</div>
		
	</div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    $("#submitBtn").click(function(event){
        	event.preventDefault();
	    	var errors = 0;
		
		$(".validateNotempty").each( function()
		{
			$(this).removeClass('error');

			if($(this).val() == '') {
				errors = errors + 1;
				$(this).addClass('error');
			}
		});
		
		if(errors == 0)
		{
			$("#noteForm").submit();
		}
    });
});
</script>
