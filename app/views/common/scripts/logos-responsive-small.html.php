<? if(!stristr(APPLICATION_ENV, "myeventvoice")) : ?>
<div class="row-fluid logos-slider">
	<div class="span12 logo_span">
		<a href="http://www.royallepage.ca/" target="_blank"><img src="/images/lp_logos_tight/royal_lepage.png" width="120px" /></a>
		
		<a href="http://blog.lwolf.com/partners/news/lone-wolf-invests-in-cityblast-81759/" target="_blank"><img src="/images/lp_logos_tight/lone_wolf.png" width="120px" /></a>

		<a href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/" target="_blank"><img src="/images/lp_logos_tight/real_estate_talk_show.png" width="90px" /></a>

		<a href="http://www.seevirtual360.com/" target="_blank"><img src="/images/lp_logos_tight/see_virtual.png" width="120px"  /></a>
	
		<a href="http://www.techvibes.com/blog/cityblast-enables-canadian-real-estate-agents-to-build-their-business-through-social-media-2012-08-03" target="_blank"><img src="/images/lp_logos_tight/techvibes.png" width="120px" /></a>
	
		<a href="http://www.canadianrealestatemagazine.ca" target="_blank"><img src="/images/lp_logos_tight/real_estate_wealth.png" width="140px" /></a>
	
		<a href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/" target="_blank"><img src="/images/lp_logos_tight/globe_and_mail.png" width="120px" /></a>

		<a href="http://retechulous.com/" target="_blank"><img src="/images/lp_logos_tight/retechulous.png" width="120px" /></a>

		<a href="" target="_blank"><img src="/images/lp_logos_tight/inman_news.png" width="120px" /></a>

		<a href=" http://activerain.trulia.com/blogsview/4374325/how-one-agent-generated-24-deals-in-12-months-using-facebook" target="_blank"><img src="/images/lp_logos_tight/active_rain.png" width="120px" /></a>

		<a href="http://www.clareitystore.com/how-it-works/store-vendors" target="_blank"><img src="/images/lp_logos_tight/clareity_store.png" width="120px" /></a>
	</div>
	
</div>
<? endif; ?>


<script src="/js/jquery.bxslider.min.js"></script>
<script type="text/javascript">

	$(document).ready(function(){
		
		$('.logo_span img').each(function(){
			
			$(this).css('opacity', .6);
			$(this).mouseover(function(){ $(this).css('opacity', 1); });
			$(this).mouseout(function(){ $(this).css('opacity', .6); });
			
		});

	});
	
</script>