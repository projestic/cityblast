<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		<table style="width: 100%; border-bottom: 0px;">
		<tr>
			<td style="border-bottom: 0px;">
				<h1>
					Cash Transactions (total <?=number_format($this->paginator->getTotalItemCount());?>)
				</h1>
			</td>

			<td style="text-align: right;border-bottom: 0px;">
				<a id="addcash" href="/cashtrans/add/id/<?=$this->member->id;?>/" class="uiButton" style="float: right;">Add Transaction</a>
			</td>
			
		</tr>
		</table>
		
	
		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>ID</th>
				<th>Payment</th>
				<th>Member</th>
				<th>Created By</th>
				<th>Credit</th>
				<th>Debit</th>
				<th>Balance</th>
				<th>Notes</th>
				<th>Created</th>
			</tr>
			<? 

			$row = true;
			$row_class = 'odd';
			
			foreach($this->paginator as $trans)
			{
				$row_class = empty($row_class) ? 'odd' : '';
			
?>

			<tr class="<?=$row_class?>">
				<td><?=$trans->id?></td>
				<td>
					<? if($trans->payment_id): ?>
						<a href="/admin/payment/<?=$trans->payment_id?>" style="color: #3D6DCC;"><?=$trans->payment_id?></a>
					<? else: ?>-<? endif; ?>
				</td>
				<td><a href="/admin/member/<?=$trans->member_id?>" style="color: #3D6DCC;"><? 
					if(!empty($trans->mfirst)) echo $trans->mfirst . " ";
					if(!empty($trans->mlast)) echo $trans->mlast;
				?>
				</td>
				<td>
				<? if($trans->created_by): ?>
					<a href="/admin/member/<?=$trans->created_by?>" style="color: #3D6DCC;"><? 
					if(!empty($trans->mcfirst)) echo $trans->mcfirst . " ";
					if(!empty($trans->mclast)) echo $trans->mclast;
					?>
				<? else: ?>
				-
				<? endif;?>
				<td><?=$trans->isCredit() ? sprintf('%.2f', $trans->getCreditAmount()) : '-'?></td>
				<td><?=$trans->isDebit() ? sprintf('%.2f', $trans->getDebitAmount()) : '-'?></td>
				<td><?=sprintf('%.2f', $trans->balance)?></td>
				<td><?=$trans->note?></td>
				<td><?=$trans->created_at->format('Y-M-d H:i:s')?></td>
			</tr>
<?php
			}

		?>
				
		</table>


		<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />


	</div>



</div>