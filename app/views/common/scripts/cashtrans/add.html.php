<link href="/css/forms.css" rel="stylesheet" type="text/css" />

<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">
	
	<div class="grid_12 member_update">
		<table style="width: 100%; border-bottom: 0px;">
		<tr>
			<td style="border-bottom: 0px;">
				<h1><?php echo $this->member->first_name.' '.$this->member->last_name; ?></h1>
			</td>

			<td style="text-align: right;border-bottom: 0px;">
				<a id="addcash" href="/admin/member/<?=$this->member->id;?>#cash" class="uiButton" style="float: right;">Transactions</a>
			</td>
			
		</tr>
		</table>
		
<? if(!empty($this->error_messages)): ?>
	<? foreach($this->error_messages as $error_message): ?>
		<div style="color:red;"><?=$error_message?></div>
	<? endforeach; ?>
	<br/>
<? endif; ?>
		
		<div class="box form clearfix">
			
			<div class="box_section">
				<h1> Add Cash Transaction</h1>
			</div>

			<form id="cashForm" action="/cashtrans/add/id/<?php echo $this->member->id;?>" method="POST">
				<input type="hidden" name="member_id" value="<?php echo $this->member->id;?>" />

				<ul class="uiForm clearfix">
					<li class="uiFormRow clearfix">
						<ul>
						    <li class="fieldname">Type</li>
						    <li class="field" style="padding-top: 20px;">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="type" class="uiSelect validateNotZero">
											<option value="<?=CashTrans::TYPE_CREDIT?>">Credit</option>
											<option value="<?=CashTrans::TYPE_DEBIT?>">Debit</option>
										</select>
									</span>
								</span>
						    </li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
						    <li class="fieldname">Amount</li>
						    <li class="field" style="padding-top: 20px;">
							    <input type="text" name="amount" id="amount" class="uiInput validateNotempty" style="width: 200px" />
						    </li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
						    <li class="fieldname">Notes</li>
						    <li class="field" style="padding-top: 20px;">
							    <input type="text" name="note" id="note" class="uiInput validateNotempty" style="width: 200px" />
						    </li>
						</ul>
					</li>					
					
				</ul>
				
				<input type="submit" name="submitted" class="uiButton large right" value="Add" id="submitBtn">
			</form>
				
		</div>
		
	</div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    $("#submitBtn").click(function(event){
        	event.preventDefault();
	    	var errors = 0;
		
		$(".validateNotempty").each( function()
		{
			$(this).removeClass('error');

			if($(this).val() == '') {
				errors = errors + 1;
				$(this).addClass('error');
			}
		});
		
		if(errors == 0)
		{
			$("#cashForm").submit();
		}
    });
});
</script>
