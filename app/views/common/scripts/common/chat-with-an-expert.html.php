<div style="display: none;">
	<div id="callus" style="width: 500px; text-align: center; padding-top: 150px;">
		<h3>Our Experts Are Standing By!</h3>
		<p style="font-size: 20px; font-weight: bold;"><?=TOLL_FREE?></p>
	</div>
</div>


<div class="contentBox questions">
	
	<div style="margin-bottom: 30px; border-bottom: 1px solid #CCCCCC; padding-bottom: 27px;">
	<h2>Do You Have More Questions?</h2>
		<p>Our <i>Social Experts</i> are on call. Feel free to call our 
		<?php if (Blastit_Http_UserAgentIdentifier::isSmartphone()): ?>
			<a href="tel:<?=TOLL_FREE?>">800 number</a>,
		<?php else: ?>
			<a href="#-" class="calluspopup">800 number</a>,
		<?php endif; ?>
		<a href="#-" class="chatbutton">Live Chat</a>
			 with us or drop us <a href="mailto: <?=HELP_EMAIL;?>">an Email</a>. Our social media marketing team is here to make your life easier.</p>
		<div class="teamHolder">
			<img src="/images/team.png" alt="">
		</div>
		<p class="arrowsAside teamAfterText"><?=COMPANY_NAME;?>'s <u>Real</u> Social Experts are always standing by ready to help you.</p>
	</div>
	
	<a href="#-" class="uiButtonNew home chatbutton">Chat With An Expert</a>
	&nbsp;&nbsp;&nbsp;- OR -&nbsp;&nbsp;&nbsp;
	<a href="mailto: <?=HELP_EMAIL;?>" class="uiButtonNew home">Send Us An Email</a>
	&nbsp;&nbsp;&nbsp;- OR -&nbsp;&nbsp;&nbsp;
	
	<?php if (Blastit_Http_UserAgentIdentifier::isSmartphone()): ?>
		<a href="tel:+18887128888" class="uiButtonNew home"><?=TOLL_FREE?></a>
	<?php else: ?>
		<a href="#-" class="uiButtonNew home calluspopup"><?=TOLL_FREE?></a>
	<?php endif; ?>
</div>


<script type="text/javascript">
	$(document).ready(function() 
	{
		$(".calluspopup").click(function(e)
		{
			e.preventDefault();
			$.colorbox({width: "600px", height: "400px", inline:true, href:"#callus",onClosed:function(){}});
		});
	});
</script>