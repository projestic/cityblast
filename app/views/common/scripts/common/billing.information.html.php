<!-- Step 2 -->
<li class="uiFormRowHeader"><span class="step">2</span><span class="step_text">Account Info</span></li>
<li class="uiFormRow first clearfix">
	<ul>
		<li class="fieldname">Company Name</li>
		<li class="field">
			<input type="text" name="company_name" value="<?=$this->company_name;?>" class="uiInput" style="width: 372px">
		</li>
	</ul>
	<ul>
		<li class="info">* Optional</li>
	</ul>
</li>
<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">Company Website</li>
		<li class="field">
			<input type="text" name="company_link" value="<?=$this->company_link;?>" class="uiInput" style="width: 372px">
		</li>
	</ul>
	<ul>
		<li class="info">* Optional</li>
	</ul>
</li>
<li class="uiFormRow last clearfix">
	<ul>
		<li class="fieldname">Phone Number</li>
		<li class="field">
			<input type="text" name="phone" value="<?=$this->member->phone;?>" class="uiInput" style="width: 372px">
		</li>
	</ul>
</li>