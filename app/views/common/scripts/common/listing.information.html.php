<li class="uiFormRowHeader"><span class="step">1</span><span class="step_text">Property Information</span></li>

<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">List Price</li>
		<li class="field">
			<input type="text" class="uiInput" style="width: 372px" name="post[list_price]" value="<?=$this->list_price;?>">
		</li>
	</ul>
</li>
<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">Number of Bedrooms</li>
		<li class="field">
			<select name="post[bedrooms]" class="uiSelect" style="width: 380px">
				<option value="1" <? if(isset($this->bedrooms) && $this->bedrooms == "1") echo " selected "; ?>>1 Bedroom</option>	
				<option value="2" <? if(isset($this->bedrooms) && $this->bedrooms == "2") echo " selected "; ?>>2 Bedrooms</option>
				<option value="3" <? if(isset($this->bedrooms) && $this->bedrooms == "3") echo " selected "; ?>>3 Bedrooms</option>
				<option value="4" <? if(isset($this->bedrooms) && $this->bedrooms == "4") echo " selected "; ?>>4 Bedrooms</option>
				<option value="5" <? if(isset($this->bedrooms) && $this->bedrooms == "5") echo " selected "; ?>>5 or More Bedrooms</option>
			</select>
		</li>
	</ul>
</li>
<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">Number of Bathrooms</li>
		<li class="field">
			<select name="post[bathrooms]" class="uiSelect" style="width: 380px">
				<option value="1" <? if(isset($this->bathrooms) && $this->bathrooms == "1") echo " selected "; ?>>1 Bathroom</option>	
				<option value="2" <? if(isset($this->bathrooms) && $this->bathrooms == "2") echo " selected "; ?>>2 Bathrooms</option>
				<option value="3" <? if(isset($this->bathrooms) && $this->bathrooms == "3") echo " selected "; ?>>3 Bathrooms</option>
				<option value="4" <? if(isset($this->bathrooms) && $this->bathrooms == "4") echo " selected "; ?>>4 Bathrooms</option>
				<option value="5" <? if(isset($this->bathrooms) && $this->bathrooms == "5") echo " selected "; ?>>5 or More Bathrooms</option>
			</select>
		</li>
	</ul>
	<? /* <label>MLS Number</label><input type="text" name="post[mls_number]"><br /> */ ?>
	<? /* <label>Link</label><input type="text" name="post[link]"><br /> */ ?>
</li>
<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">Image</li>
		<li class="field">
			<input id="file_upload" name="post[image]" type="file" />
		</li>
	</ul>
</li>
<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">Address</li>
		<li class="field">
			<input type="text" name="post[address]" value="<?=$this->post_address;?>" class="uiInput" style="width: 372px">
		</li>
	</ul>
</li>
<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">Address 2</li>
		<li class="field">
			<input type="text" name="post[address2]" value="<?=$this->post_address2;?>" class="uiInput" style="width: 372px">
		</li>
	</ul>
</li>
<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">Zip/Postal Code</li>
		<li class="field">
			<input type="text" name="post[zip]" value="<?=$this->post_zip;?>" class="uiInput" style="width: 100px">
		</li>
	</ul>
</li>
<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">City</li>
		<li class="field">
			<input type="text" name="post[city]" value="<?=$this->post_city;?>" class="uiInput" style="width: 372px">
		</li>
	</ul>
</li>
<li class="uiFormRow clearfix">
	<ul>
		<li class="fieldname">State/Province</li>
		<li class="field">
			<select name="post[state]" class="uiSelect" style="width: 380px">
				<option value="">-- Select One --</option>
				<optgroup label="States">
					<option <? if(isset($this->post_state) && $this->post_state == "Alaska") echo " selected "; ?> value="Alaska">Alaska</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Alabama") echo " selected "; ?> value="Alabama">Alabama</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Arkansas") echo " selected "; ?> value="Arkansas">Arkansas</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Arizona") echo " selected "; ?> value="Arizona">Arizona</option>
					<option <? if(isset($this->post_state) && $this->post_state == "California") echo " selected "; ?> value="California">California</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Colorado") echo " selected "; ?> value="Colorado">Colorado</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Connecticut") echo " selected "; ?> value="Connecticut">Connecticut</option>
					<option <? if(isset($this->post_state) && $this->post_state == "District of Columbia") echo " selected "; ?> value="District of Columbia">District of Columbia</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Delaware") echo " selected "; ?> value="Delaware">Delaware</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Florida") echo " selected "; ?> value="Florida">Florida</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Georgia") echo " selected "; ?> value="Georgia">Georgia</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Hawaii") echo " selected "; ?> value="Hawaii">Hawaii</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Iowa") echo " selected "; ?> value="Iowa">Iowa</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Idaho") echo " selected "; ?> value="Idaho">Idaho</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Illinois") echo " selected "; ?> value="Illinois">Illinois</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Indiana") echo " selected "; ?> value="Indiana">Indiana</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Kansas") echo " selected "; ?> value="Kansas">Kansas</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Kentucky") echo " selected "; ?> value="Kentucky">Kentucky</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Louisiana") echo " selected "; ?> value="Louisiana">Louisiana</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Massachusetts") echo " selected "; ?> value="Massachusetts">Massachusetts</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Maryland") echo " selected "; ?> value="Maryland">Maryland</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Maine") echo " selected "; ?> value="Maine">Maine</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Michigan") echo " selected "; ?> value="Michigan">Michigan</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Minnesota") echo " selected "; ?> value="Minnesota">Minnesota</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Missouri") echo " selected "; ?> value="Missouri">Missouri</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Mississippi") echo " selected "; ?> value="Mississippi">Mississippi</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Montana") echo " selected "; ?> value="Montana">Montana</option>
					<option <? if(isset($this->post_state) && $this->post_state == "North Carolina") echo " selected "; ?> value="North Carolina">North Carolina</option>
					<option <? if(isset($this->post_state) && $this->post_state == "North Dakota") echo " selected "; ?> value="North Dakota">North Dakota</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Nebraska") echo " selected "; ?> value="Nebraska">Nebraska</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Nevada") echo " selected "; ?> value="Nevada">Nevada</option>
					<option <? if(isset($this->post_state) && $this->post_state == "New Hampshire") echo " selected "; ?> value="New Hampshire">New Hampshire</option>
					<option <? if(isset($this->post_state) && $this->post_state == "New Jersey") echo " selected "; ?> value="New Jersey">New Jersey</option>
					<option <? if(isset($this->post_state) && $this->post_state == "New Mexico") echo " selected "; ?> value="New Mexico">New Mexico</option>
					<option <? if(isset($this->post_state) && $this->post_state == "New York") echo " selected "; ?> value="New York">New York</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Ohio") echo " selected "; ?> value="Ohio">Ohio</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Oklahoma") echo " selected "; ?> value="Oklahoma">Oklahoma</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Oregon") echo " selected "; ?> value="Oregon">Oregon</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Pennsylvania") echo " selected "; ?> value="Pennsylvania">Pennsylvania</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Rhode Island") echo " selected "; ?> value="Rhode Island">Rhode Island</option>
					<option <? if(isset($this->post_state) && $this->post_state == "South Carolina") echo " selected "; ?> value="South Carolina">South Carolina</option>
					<option <? if(isset($this->post_state) && $this->post_state == "South Dakota") echo " selected "; ?> value="South Dakota">South Dakota</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Tennessee") echo " selected "; ?> value="Tennessee">Tennessee</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Texas") echo " selected "; ?> value="Texas">Texas</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Utah") echo " selected "; ?> value="Utah">Utah</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Viginia") echo " selected "; ?> value="Viginia">Virginia</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Vermont") echo " selected "; ?> value="Vermont">Vermont</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Washington") echo " selected "; ?> value="Washington">Washington</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Wisconsin") echo " selected "; ?> value="Wisconsin">Wisconsin</option>
					<option <? if(isset($this->post_state) && $this->post_state == "West Virginia") echo " selected "; ?> value="West Virginia">West Virginia</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Wyoming") echo " selected "; ?> value="Wyoming">Wyoming</option>
				</optgroup>
				<optgroup label="Provinces">
					<option <? if(isset($this->post_state) && $this->post_state == "Alberta") echo " selected "; ?> value="Alberta">Alberta</option>
					<option <? if(isset($this->post_state) && $this->post_state == "British Columbia") echo " selected "; ?> value="British Columbia">British Columbia</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Manitoba") echo " selected "; ?> value="Manitoba">Manitoba</option>
					<option <? if(isset($this->post_state) && $this->post_state == "New Brunswisk") echo " selected "; ?> value="New Brunswisk">New Brunswick</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Newfoundland &amp;Labrador") echo " selected "; ?> value="Newfoundland &amp;Labrador">Newfoundland & Labrador</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Northwest Territories") echo " selected "; ?> value="Northwest Territories">Northwest Territories</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Nova Scotia") echo " selected "; ?> value="Nova Scotia">Nova Scotia</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Nunavut") echo " selected "; ?> value="Nunavut">Nunavut</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Ontario") echo " selected "; ?> value="Ontario">Ontario</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Prince Edward Island") echo " selected "; ?> value="Prince Edward Island">Prince Edward Island</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Quebec") echo " selected "; ?> value="Quebec">Quebec</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Saskatchewan") echo " selected "; ?> value="Saskatchewan">Saskatchewan</option>
					<option <? if(isset($this->post_state) && $this->post_state == "Yukon Territory") echo " selected "; ?> value="Yukon Territory">Yukon Territory</option>
				</optgroup>
			</select>
		</li>
	</ul>
</li>
<li class="uiFormRow last clearfix">
	<ul>
		<li class="fieldname">Country</li>
		<li class="field">
			<select name="post[country]" class="uiSelect" style="width: 380px">
				<option value="CA" <? if(isset($this->post_country) && $this->post_country == "CA") echo " selected "; ?>>Canada</option>
				<option value="US" <? if(isset($this->post_country) && $this->post_country == "US") echo " selected "; ?>>United States</option>
			</select>
		</li>
	</ul>
</li>