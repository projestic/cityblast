<script type="text/javascript" src="https://js.stripe.com/v1/"></script>

<script type="text/javascript">

    // this identifies your website in the createToken call below
    Stripe.setPublishableKey('<?=STRIPE_PUBLISH;?>');

	function stripeResponseHandler(status, response) 
	{
		if (response.error) 
		{
			// re-enable the submit button
			$('.submit-button').removeAttr("disabled");

			$("#submit_btn").removeClass("cancel");
			$("#submit_btn").val("Submit");
		
			
			$("#payment-errors-wrapper").show();
		
			//alert('Error!');
			
			// show the errors on the form
			$("#payment-errors").html(response.error.message);
		} else {

			var form$ = $("#payment-form");
			
			// token contains id, last4, and card type
			var token = response['id'];
			
			// insert the token into the form so it gets submitted to the server
			form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
			
			// and submit
			form$.get(0).submit();
		}
	}
	
	$(document).ready(function()
	{
		$("#payment-form").submit(function(event) 
		{
			event.preventDefault();
			
			
			// disable the submit button to prevent repeated clicks
			$('.submit-button').attr("disabled", "disabled");
		
			// createToken returns immediately - the supplied callback submits the form if there are no errors
			Stripe.createToken({
		   		number: $('.card-number').val(),
		    		cvc: $('.card-cvc').val(),
		    		exp_month: $('.card-expiry-month').val(),
		    		exp_year: $('.card-expiry-year').val()
			}, stripeResponseHandler);
			
			return false; // submit from callback
		});
	});
	
</script>


<?
	$city = null;
	if(isset($_SESSION['member'])) $city = $_SESSION['member']->default_city;


	
	function check_state($city, $STATE)
	{
		if(	isset($_REQUEST['state']) && strtoupper($_REQUEST['state']) == strtoupper($STATE)		) echo " selected ";
		if(	isset($city) && !empty($city->state) && strtoupper($city->state) == strtoupper($STATE)	) echo " selected ";
	}
	function check_country($city, $SHORT_CODE, $COUNTRY)
	{
		if(isset($_REQUEST['cc_country']) && $_REQUEST['cc_country'] == $SHORT_CODE) echo " selected ";
		if(	isset($city) && !empty($city->country) && strtoupper($city->country) == strtoupper($COUNTRY)	) echo " selected ";
	}

?>

<noscript>
<h1 style="color: #990000;">ERROR: This page MUST have JavaScript activated in order to work properly!</h1> 
</noscript>


<ul class="uiForm small clearfix">
	<li class="uiFormRowHeader">Personal Information</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname">Cost</li>
			<li class="field">
				<span class="label">Only 
					<?
						if($this->member->price == 0) echo "Free!";

						elseif($this->member->price == 15) echo "$14.99";
						elseif($this->member->price == 20) echo "$19.99";		
						elseif($this->member->price == 25) echo "$24.99";
						elseif($this->member->price == 50) echo "$49.99";
						
						else echo "$".$this->member->price;
					?>
					
					<i style="color: #808080">&nbsp;&nbsp;*(after your 14 day trial)</i>
					
				</span>
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">First Name <span style="font-weight: bold; color: #990000">*</span></li>
			<li class="field cc">
				<input type="text" name="fname" id="fname" value="<?
					if(isset($_REQUEST['fname'])) echo $_REQUEST['fname'];
					elseif(isset($_SESSION['member'])) echo $_SESSION['member']->first_name;
					?>" class="uiInput<?=($this->fields && in_array("fname", $this->fields)?" error":"")?>" style="width: 350px" />
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Last Name <span style="font-weight: bold; color: #990000">*</span></li>
			<li class="field cc">
				<input type="text" name="lname" id="lname" value="<?
					if(isset($_REQUEST['lname'])) echo $_REQUEST['lname'];
					elseif(isset($_SESSION['member'])) echo $_SESSION['member']->last_name;
					?>" class="uiInput<?=($this->fields && in_array("lname", $this->fields)?" error":"")?>" style="width: 350px" />
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Address</li>
			<li class="field cc">
				<input type="text" name="address" id="address" value="<? if(isset($_REQUEST['address'])) echo $_REQUEST['address'];?>" class="uiInput<?=($this->fields && in_array("address", $this->fields)?" error":"")?>" style="width: 350px">
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Zip/Postal Code</li>
			<li class="field cc">
				<input type="text" name="zip" id="zip" value="<? if(isset($_REQUEST['zip'])) echo $_REQUEST['zip'];?>" class="uiInput<?=($this->fields && in_array("zip", $this->fields)?" error":"")?>" style="width: 100px">
			</li>
		</ul>
	</li>


	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">City</li>
			<li class="field cc">
				<input type="text" name="city" id="city" value="<? if(isset($_REQUEST['city'])) echo $_REQUEST['city']; elseif ($_SESSION['selectedCityName']) echo $_SESSION['selectedCityName'];?>" class="uiInput<?=($this->fields && in_array("city", $this->fields)?" error":"")?>" style="width: 350px">
			</li>
		</ul>
	</li>
	
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">State/Province</li>
			<li class="field cc">
				<span class="uiSelectWrapper">
					<span class="uiSelectInner">
						<select name="state" id="state" class="uiSelect<?=($this->fields && in_array("state", $this->fields)?" error":"")?>" style="width: 350px">
							<option value="">-- Select One --</option>
							<optgroup label="States">
								<option <? check_state($city, "Alaska"); ?> value="Alaska">Alaska</option>
								<option <? check_state($city, "Alabama"); ?> value="Alabama">Alabama</option>
								<option <? check_state($city, "Arkansas"); ?> value="Arkansas">Arkansas</option>
								<option <? check_state($city, "Arizona"); ?> value="Arizona">Arizona</option>
								<option <? check_state($city, "California"); ?> value="California">California</option>
								<option <? check_state($city, "Colorado"); ?> value="Colorado">Colorado</option>
								<option <? check_state($city, "Connecticut"); ?> value="Connecticut">Connecticut</option>
								<option <? check_state($city, "District of Columbia"); ?> value="District of Columbia">District of Columbia</option>
								<option <? check_state($city, "Delaware"); ?> value="Delaware">Delaware</option>
								<option <? check_state($city, "Florida"); ?> value="Florida">Florida</option>
								<option <? check_state($city, "Georgia"); ?> value="Georgia">Georgia</option>
								<option <? check_state($city, "Hawaii"); ?> value="Hawaii">Hawaii</option>
								<option <? check_state($city, "Iowa"); ?> value="Iowa">Iowa</option>
								<option <? check_state($city, "Idaho"); ?> value="Idaho">Idaho</option>
								<option <? check_state($city, "Illinois"); ?> value="Illinois">Illinois</option>
								<option <? check_state($city, "Indiana"); ?> value="Indiana">Indiana</option>
								<option <? check_state($city, "Kansas"); ?> value="Kansas">Kansas</option>
								<option <? check_state($city, "Kentucky"); ?> value="Kentucky">Kentucky</option>
								<option <? check_state($city, "Louisiana"); ?> value="Louisiana">Louisiana</option>
								<option <? check_state($city, "Massachusetts"); ?> value="Massachusetts">Massachusetts</option>
								<option <? check_state($city, "Maryland"); ?> value="Maryland">Maryland</option>
								<option <? check_state($city, "Maine"); ?> value="Maine">Maine</option>
								<option <? check_state($city, "Michigan"); ?> value="Michigan">Michigan</option>
								<option <? check_state($city, "Minnesota"); ?> value="Minnesota">Minnesota</option>
								<option <? check_state($city, "Missouri"); ?> value="Missouri">Missouri</option>
								<option <? check_state($city, "Mississippi"); ?> value="Mississippi">Mississippi</option>
								<option <? check_state($city, "Montana"); ?> value="Montana">Montana</option>
								<option <? check_state($city, "North Carolina"); ?> value="North Carolina">North Carolina</option>
								<option <? check_state($city, "North Dakota"); ?> value="North Dakota">North Dakota</option>
								<option <? check_state($city, "Nebraska"); ?> value="Nebraska">Nebraska</option>
								<option <? check_state($city, "Nevada"); ?> value="Nevada">Nevada</option>
								<option <? check_state($city, "New Hampshire"); ?> value="New Hampshire">New Hampshire</option>
								<option <? check_state($city, "New Jersey"); ?> value="New Jersey">New Jersey</option>
								<option <? check_state($city, "New Mexico"); ?> value="New Mexico">New Mexico</option>
								<option <? check_state($city, "New York"); ?> value="New York">New York</option>
								<option <? check_state($city, "Ohio"); ?> value="Ohio">Ohio</option>
								<option <? check_state($city, "Oklahoma"); ?> value="Oklahoma">Oklahoma</option>
								<option <? check_state($city, "Oregon"); ?> value="Oregon">Oregon</option>
								<option <? check_state($city, "Pennsylvania"); ?> value="Pennsylvania">Pennsylvania</option>
								<option <? check_state($city, "Rhode Island"); ?> value="Rhode Island">Rhode Island</option>
								<option <? check_state($city, "South Carolina"); ?> value="South Carolina">South Carolina</option>
								<option <? check_state($city, "South Dakota"); ?> value="South Dakota">South Dakota</option>
								<option <? check_state($city, "Tennessee"); ?> value="Tennessee">Tennessee</option>
								<option <? check_state($city, "Texas"); ?> value="Texas">Texas</option>
								<option <? check_state($city, "Utah"); ?> value="Utah">Utah</option>
								<option <? check_state($city, "Viginia"); ?> value="Viginia">Virginia</option>
								<option <? check_state($city, "Vermont"); ?> value="Vermont">Vermont</option>
								<option <? check_state($city, "Washington"); ?> value="Washington">Washington</option>
								<option <? check_state($city, "Wisconsin"); ?> value="Wisconsin">Wisconsin</option>
								<option <? check_state($city, "West Virginia"); ?> value="West Virginia">West Virginia</option>
								<option <? check_state($city, "Wyoming"); ?> value="Wyoming">Wyoming</option>
							</optgroup>
							<optgroup label="Provinces">
								<option <? check_state($city, "Alberta"); ?> value="Alberta">Alberta</option>
								<option <? check_state($city, "British Columbia"); ?> value="British Columbia">British Columbia</option>
								<option <? check_state($city, "Manitoba"); ?> value="Manitoba">Manitoba</option>
								<option <? check_state($city, "New Brunswisk"); ?> value="New Brunswisk">New Brunswick</option>
								<option <? check_state($city, "Newfoundland &amp;Labrador"); ?> value="Newfoundland &amp;Labrador">Newfoundland & Labrador</option>
								<option <? check_state($city, "Northwest Territories"); ?> value="Northwest Territories">Northwest Territories</option>
								<option <? check_state($city, "Nova Scotia"); ?> value="Nova Scotia">Nova Scotia</option>
								<option <? check_state($city, "Nunavut"); ?> value="Nunavut">Nunavut</option>
								<option <? check_state($city, "Ontario"); ?> value="Ontario">Ontario</option>
								<option <? check_state($city, "Prince Edward Island"); ?> value="Prince Edward Island">Prince Edward Island</option>
								<option <? check_state($city, "Quebec"); ?> value="Quebec">Quebec</option>
								<option <? check_state($city, "Saskatchewan"); ?> value="Saskatchewan">Saskatchewan</option>
								<option <? check_state($city, "Yukon Territory"); ?> value="Yukon Territory">Yukon Territory</option>
							</optgroup>
						</select>
					</span>
				</span>
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Country</li>
			<li class="field cc">
				<span class="uiSelectWrapper">
					<span class="uiSelectInner">
						<select name="cc_country" id="cc_country" class="uiSelect<?=($this->fields && in_array("cc_country", $this->fields)?" error":"")?>" style="width: 350px">
							<option value="">-- Select One --</option>
							<option value="CA" <? check_country($city, "CA", "CANADA"); ?>>Canada</option>
							<option value="US" <? check_country($city, "US", "UNITED STATES"); ?>>United States</option>
						</select>
					</span>
				</span>
			</li>
		</ul>
	</li>
	

	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc"></li>
			<li class="field cc" style="text-align: right;">
				<a href="#" id="promo_code">Promo Code</a>
			</li>
		</ul>
	</li>

	<li class="uiFormRow clearfix" id="promo_code_div" style="display: none;">
		<ul>
			<li class="fieldname cc">Promo Code</li>
			<li class="field cc">
				<input type="text" name="promo_code" id="promo_code" value="<? if(isset($_REQUEST['promo_code'])) echo $_REQUEST['promo_code'];?>" class="uiInput" style="width: 350px">
			</li>
		</ul>
	</li>
	
</ul>


<ul class="uiForm small clearfix">
	<li class="uiFormRowHeader">Credit Card Information</li>


	<li class="uiFormRow clearfix" style="background-color: #FFE6E6;">
		<div id="payment-errors-wrapper" <? if(!$this->payment_error) : ?>style="display: none;"<? endif; ?>>

			<div style="margin-left: 30px; margin-bottom: 15px;">
				<h4 style="color: #cc0000;">Oh Dear, Something Went Wrong!</h4>
				<div id="payment-errors"><?=$this->payment_error;?></div>
			</div>
			
		</div>
	</li>


	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc"></li>
			<li class="field cc">
				<img src="/images/cc_types.png" />
			</li>
		</ul>
	</li>


	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Card Number</li>
			<li class="field cc">
				<input type="text" size="20" autocomplete="off" class="card-number uiInput" style="width: 350px" />
			</li>
		</ul>
	</li>

	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">CVV2</li>
			<li class="field cc">
				<input type=text class="card-cvc uiInput" style="width: 60px" />
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Expiry Month/Year</li>
			<li class="field cc">
				<span class="uiSelectWrapper" style="margin-right: 5px;">
					<span class="uiSelectInner">
						<select id="exp_month"  class="card-expiry-month uiSelect" style="width: 70px">
							<option value="">MM</option>
							<option value="01" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "01") echo "selected"; ?>>01</option>
							<option value="02" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "02") echo "selected"; ?>>02</option>
							<option value="03" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "03") echo "selected"; ?>>03</option>
							<option value="04" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "04") echo "selected"; ?>>04</option>
							<option value="05" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "05") echo "selected"; ?>>05</option>
							<option value="06" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "06") echo "selected"; ?>>06</option>
							<option value="07" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "07") echo "selected"; ?>>07</option>
							<option value="08" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "08") echo "selected"; ?>>08</option>
							<option value="09" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "09") echo "selected"; ?>>09</option>
							<option value="10" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "10") echo "selected"; ?>>10</option>
							<option value="11" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "11") echo "selected"; ?>>11</option>
							<option value="12" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "12") echo "selected"; ?>>12</option>
						</select>
					</span>
				</span>

				<span class="uiSelectWrapper">
					<span class="uiSelectInner">
						<select id="exp_year" class="card-expiry-year uiSelect" style="width: 70px; margin-left: 5px">
							<option value="">YY</option>
							<?
								$minyear = date("Y");
								$maxyear = date("Y") + 10;
								for($year=$minyear;$year<=$maxyear;$year++)
								{
									echo "<option value='$year'";
									if(isset($_REQUEST['exp_year']) && $_REQUEST['exp_year'] == $year) echo " selected ";
									echo ">$year</option>";
								}
							?>
						</select>
					</span>
				</span>
			</li>
		</ul>
	</li>




	<li class="uiFormRow clearfix">
		<ul>
			<li class="buttons">
				<input type="submit" name="submitted" class="uiButton right submit-button" value="Submit" id="submit_btn">
				<input type="reset" name="submitted" class="uiButton cancel right" value="Reset">
				<span style="float: left; line-height: 40px; font-size: 12px;"><span style="font-weight: bold; color: #990000;">*</span> As it appears on your credit card</span>
			</li>
		</ul>
	</li>
</ul>

<script type="text/javascript">
$(document).ready(function(){
	$("#submit_btn").click(function(e){
		$(this).addClass("cancel");
		$(this).val("Processing");
	});	
	$("#promo_code").click(function(e){
		e.preventDefault();
		$("#promo_code_div").show();
	});		
});
</script>