<ul class="uiForm small clearfix">
	<li class="uiFormRowHeader">Credit Card Information</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname">Cost</li>
			<li class="field">
				<span class="label">Only 
					<?
						if($this->member->price == 0) echo "Free!";

						elseif($this->member->price == 15) echo "$14.99";
						elseif($this->member->price == 20) echo "$19.99";		
						elseif($this->member->price == 25) echo "$24.99";
						elseif($this->member->price == 50) echo "$49.99";
						
						else echo "$".$this->member->price;
					?>
					
					<i style="color: #808080">&nbsp;&nbsp;*(after your 30 day trial)</i>
					
				</span>
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">First Name <span style="font-weight: bold; color: #990000">*</span></li>
			<li class="field cc">
				<input type="text" name="fname" id="fname" value="<?
					if(isset($_REQUEST['fname'])) echo $_REQUEST['fname'];
					elseif(isset($_SESSION['member'])) echo $_SESSION['member']->first_name;
					?>" class="uiInput<?=($this->fields && in_array("fname", $this->fields)?" error":"")?>" style="width: 350px" />
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Last Name <span style="font-weight: bold; color: #990000">*</span></li>
			<li class="field cc">
				<input type="text" name="lname" id="lname" value="<?
					if(isset($_REQUEST['lname'])) echo $_REQUEST['lname'];
					elseif(isset($_SESSION['member'])) echo $_SESSION['member']->last_name;
					?>" class="uiInput<?=($this->fields && in_array("lname", $this->fields)?" error":"")?>" style="width: 350px" />
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Address</li>
			<li class="field cc">
				<input type="text" name="address" id="address" value="<? if(isset($_REQUEST['address'])) echo $_REQUEST['address'];?>" class="uiInput<?=($this->fields && in_array("address", $this->fields)?" error":"")?>" style="width: 350px">
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Zip/Postal Code</li>
			<li class="field cc">
				<input type="text" name="zip" id="zip" value="<? if(isset($_REQUEST['zip'])) echo $_REQUEST['zip'];?>" class="uiInput<?=($this->fields && in_array("zip", $this->fields)?" error":"")?>" style="width: 100px">
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">City</li>
			<li class="field cc">
				<input type="text" name="city" id="city" value="<? if(isset($_REQUEST['city'])) echo $_REQUEST['city'];?>" class="uiInput<?=($this->fields && in_array("city", $this->fields)?" error":"")?>" style="width: 350px">
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">State/Province</li>
			<li class="field cc">
				<span class="uiSelectWrapper">
					<span class="uiSelectInner">
						<select name="state" id="state" class="uiSelect<?=($this->fields && in_array("state", $this->fields)?" error":"")?>" style="width: 350px">
							<option value="">-- Select One --</option>
							<optgroup label="States">
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Alaska") echo " selected "; ?> value="Alaska">Alaska</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Alabama") echo " selected "; ?> value="Alabama">Alabama</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Arkansas") echo " selected "; ?> value="Arkansas">Arkansas</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Arizona") echo " selected "; ?> value="Arizona">Arizona</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "California") echo " selected "; ?> value="California">California</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Colorado") echo " selected "; ?> value="Colorado">Colorado</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Connecticut") echo " selected "; ?> value="Connecticut">Connecticut</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "District of Columbia") echo " selected "; ?> value="District of Columbia">District of Columbia</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Delaware") echo " selected "; ?> value="Delaware">Delaware</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Florida") echo " selected "; ?> value="Florida">Florida</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Georgia") echo " selected "; ?> value="Georgia">Georgia</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Hawaii") echo " selected "; ?> value="Hawaii">Hawaii</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Iowa") echo " selected "; ?> value="Iowa">Iowa</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Idaho") echo " selected "; ?> value="Idaho">Idaho</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Illinois") echo " selected "; ?> value="Illinois">Illinois</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Indiana") echo " selected "; ?> value="Indiana">Indiana</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Kansas") echo " selected "; ?> value="Kansas">Kansas</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Kentucky") echo " selected "; ?> value="Kentucky">Kentucky</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Louisiana") echo " selected "; ?> value="Louisiana">Louisiana</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Massachusetts") echo " selected "; ?> value="Massachusetts">Massachusetts</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Maryland") echo " selected "; ?> value="Maryland">Maryland</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Maine") echo " selected "; ?> value="Maine">Maine</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Michigan") echo " selected "; ?> value="Michigan">Michigan</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Minnesota") echo " selected "; ?> value="Minnesota">Minnesota</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Missouri") echo " selected "; ?> value="Missouri">Missouri</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Mississippi") echo " selected "; ?> value="Mississippi">Mississippi</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Montana") echo " selected "; ?> value="Montana">Montana</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "North Carolina") echo " selected "; ?> value="North Carolina">North Carolina</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "North Dakota") echo " selected "; ?> value="North Dakota">North Dakota</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Nebraska") echo " selected "; ?> value="Nebraska">Nebraska</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Nevada") echo " selected "; ?> value="Nevada">Nevada</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "New Hampshire") echo " selected "; ?> value="New Hampshire">New Hampshire</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "New Jersey") echo " selected "; ?> value="New Jersey">New Jersey</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "New Mexico") echo " selected "; ?> value="New Mexico">New Mexico</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "New York") echo " selected "; ?> value="New York">New York</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Ohio") echo " selected "; ?> value="Ohio">Ohio</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Oklahoma") echo " selected "; ?> value="Oklahoma">Oklahoma</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Oregon") echo " selected "; ?> value="Oregon">Oregon</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Pennsylvania") echo " selected "; ?> value="Pennsylvania">Pennsylvania</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Rhode Island") echo " selected "; ?> value="Rhode Island">Rhode Island</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "South Carolina") echo " selected "; ?> value="South Carolina">South Carolina</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "South Dakota") echo " selected "; ?> value="South Dakota">South Dakota</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Tennessee") echo " selected "; ?> value="Tennessee">Tennessee</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Texas") echo " selected "; ?> value="Texas">Texas</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Utah") echo " selected "; ?> value="Utah">Utah</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Viginia") echo " selected "; ?> value="Viginia">Virginia</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Vermont") echo " selected "; ?> value="Vermont">Vermont</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Washington") echo " selected "; ?> value="Washington">Washington</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Wisconsin") echo " selected "; ?> value="Wisconsin">Wisconsin</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "West Virginia") echo " selected "; ?> value="West Virginia">West Virginia</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Wyoming") echo " selected "; ?> value="Wyoming">Wyoming</option>
							</optgroup>
							<optgroup label="Provinces">
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Alberta") echo " selected "; ?> value="Alberta">Alberta</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "British Columbia") echo " selected "; ?> value="British Columbia">British Columbia</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Manitoba") echo " selected "; ?> value="Manitoba">Manitoba</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "New Brunswisk") echo " selected "; ?> value="New Brunswisk">New Brunswick</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Newfoundland &amp;Labrador") echo " selected "; ?> value="Newfoundland &amp;Labrador">Newfoundland & Labrador</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Northwest Territories") echo " selected "; ?> value="Northwest Territories">Northwest Territories</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Nova Scotia") echo " selected "; ?> value="Nova Scotia">Nova Scotia</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Nunavut") echo " selected "; ?> value="Nunavut">Nunavut</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Ontario" || !isset($_REQUEST['state']) || empty($_REQUEST['state'])	) echo " selected "; ?> value="Ontario">Ontario</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Prince Edward Island") echo " selected "; ?> value="Prince Edward Island">Prince Edward Island</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Quebec") echo " selected "; ?> value="Quebec">Quebec</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Saskatchewan") echo " selected "; ?> value="Saskatchewan">Saskatchewan</option>
								<option <? if(isset($_REQUEST['state']) && $_REQUEST['state'] == "Yukon Territory") echo " selected "; ?> value="Yukon Territory">Yukon Territory</option>
							</optgroup>
						</select>
					</span>
				</span>
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Country</li>
			<li class="field cc">
				<span class="uiSelectWrapper">
					<span class="uiSelectInner">
						<select name="cc_country" id="cc_country" class="uiSelect<?=($this->fields && in_array("cc_country", $this->fields)?" error":"")?>" style="width: 350px">
							<option value="">-- Select One --</option>
							<option value="CA" <? if(isset($_REQUEST['cc_country']) && $_REQUEST['cc_country'] == "CA" || !isset($_REQUEST['cc_country']) || empty($_REQUEST['cc_country'])	) echo " selected "; ?>>Canada</option>
							<option value="US" <? if(isset($_REQUEST['cc_country']) && $_REQUEST['cc_country'] == "US") echo " selected "; ?>>United States</option>
						</select>
					</span>
				</span>
			</li>
		</ul>
	</li>

	
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Credit Card Type</li>
			<li class="field cc">
				<span class="uiSelectWrapper">
					<span class="uiSelectInner">
						<select name="cc_type" id="cc_type" class="uiSelect<?=($this->fields && in_array("cc_type", $this->fields)?" error":"")?>" style="width: 350px">
							<option value="">-- Select One --</option>
							<option value="Visa" <? if (isset($_REQUEST['cc_type']) && $_REQUEST['cc_type'] == "Visa") echo "selected"; ?>>Visa</option>
							<option value="MasterCard" <? if (isset($_REQUEST['cc_type']) && $_REQUEST['cc_type'] == "MasterCard") echo "selected"; ?>>Master Card</option>
                            <option value="AmericanExpress" <? if (isset($_REQUEST['cc_type']) && $_REQUEST['cc_type'] == "AmericanExpress") echo "selected"; ?>>American Express</option>

							<!--
							<option value="Amex" <? if ($_REQUEST['cc_type'] == "Amex") echo "selected"; ?>>American Express</option>
							<option value="Discover" <? if ($_REQUEST['cc_type'] == "Discover") echo "selected"; ?>>Discover</option>
							<option value="Maestro" <? if ($_REQUEST['cc_type'] == "Maestro") echo "selected"; ?>>Maestro</option>
							<option value="Solo" <? if ($_REQUEST['cc_type'] == "Solo") echo "selected"; ?>>Solo</option>
							--->
						</select>
					</span>
				</span>
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Credit Card Number</li>
			<li class="field cc">
				<input type=text name="cc_number" id="cc_number" value="<? if(isset($_REQUEST['cc_number'])) echo $_REQUEST['cc_number']?>" class="uiInput<?=($this->fields && in_array("cc_number", $this->fields)?" error":"")?>" style="width: 350px" />
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">CVV2</li>
			<li class="field cc">
				<input type=text name="cv2" id="cv2" value="<? if(isset($_REQUEST['cv2'])) echo $_REQUEST['cv2']?>" class="uiInput<?=($this->fields && in_array("cv2", $this->fields)?" error":"")?>" style="width: 60px" />
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="fieldname cc">Expiry Month/Year</li>
			<li class="field cc">
				<span class="uiSelectWrapper" style="margin-right: 5px;">
					<span class="uiSelectInner">
						<select name="exp_month" id="exp_month" class="uiSelect<?=($this->fields && in_array("exp_month", $this->fields)?" error":"")?>" style="width: 70px">
							<option value="">MM</option>
							<option value="01" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "01") echo "selected"; ?>>01</option>
							<option value="02" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "02") echo "selected"; ?>>02</option>
							<option value="03" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "03") echo "selected"; ?>>03</option>
							<option value="04" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "04") echo "selected"; ?>>04</option>
							<option value="05" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "05") echo "selected"; ?>>05</option>
							<option value="06" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "06") echo "selected"; ?>>06</option>
							<option value="07" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "07") echo "selected"; ?>>07</option>
							<option value="08" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "08") echo "selected"; ?>>08</option>
							<option value="09" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "09") echo "selected"; ?>>09</option>
							<option value="10" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "10") echo "selected"; ?>>10</option>
							<option value="11" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "11") echo "selected"; ?>>11</option>
							<option value="12" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "12") echo "selected"; ?>>12</option>
						</select>
					</span>
				</span>

				<span class="uiSelectWrapper">
					<span class="uiSelectInner">
						<select name="exp_year" id="exp_year" class="uiSelect<?=($this->fields && in_array("exp_year", $this->fields)?" error":"")?>" style="width: 70px; margin-left: 5px">
							<option value="">YY</option>
							<?
								$minyear = date("Y");
								$maxyear = date("Y") + 10;
								for($year=$minyear;$year<=$maxyear;$year++)
								{
									echo "<option value='$year'";
									if(isset($_REQUEST['exp_year']) && $_REQUEST['exp_year'] == $year) echo " selected ";
									echo ">$year</option>";
								}
							?>
						</select>
					</span>
				</span>
			</li>
		</ul>
	</li>
	<li class="uiFormRow clearfix">
		<ul>
			<li class="buttons">
				<input type="submit" name="submitted" class="uiButton right" value="Submit">
				<input type="reset" name="submitted" class="uiButton cancel right" value="Reset">
				<span style="float: left; line-height: 40px; font-size: 12px;"><span style="font-weight: bold; color: #990000;">*</span> As it appears on your credit card</span>
			</li>
		</ul>
	</li>
</ul>

<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $("#cc_country")
                .change(function () {
                    var option = $("#cc_type").find("option[value='AmericanExpress']");
                    if ( $(this).val() == "US" ) {
                        $(option).show();
                    } else {
                        if ( $("#cc_type").val() == "AmericanExpress" ) {
                            $("#cc_type").val("");
                        }
                        $(option).hide();
                    }
                })
                .trigger("change");
        });
    })(jQuery);
</script>