<? /****************************************************************
<div class="grid_12" style="margin-top: 30px; text-align: right">

	<select name="member_status" id="member_status" onchange="window.location.replace('/admin/setmemberstatus/'+this.value);" style="font-size: 22px; font-weight: bold;">
		<option value="all" <?php if($this->selected_status === "all") echo 'selected="selected"';?>>All Members</option>
		<option value="active" <?php if($this->selected_status === "active") echo 'selected="selected"';?>>Active Members</option>
		<option value="threestrikes" <?php if($this->selected_status === "threestrikes") echo 'selected="selected"';?>>3 Strikes Members</option>
		<option value="inactive" <?php if($this->selected_status === "inactive") echo 'selected="selected"';?>>Inactive Members</option>
		<option value="publishing" <?php if($this->selected_status === "publishing") echo 'selected="selected"';?>>Publishing Assignments</option>
		<option value="publishsettings" <?php if($this->selected_status === "publishsettings") echo 'selected="selected"';?>>Publish Settings</option>
		<option value="fbpword" <?php if($this->selected_status === "fbpword") echo 'selected="selected"';?>>FB Pword Change</option>
		<option value="twpword" <?php if($this->selected_status === "twpword") echo 'selected="selected"';?>>TW Pword Change</option>
		<option value="reach-by-city" <?php if($this->selected_status === "reach-by-city") echo 'selected="selected"';?>>Reach By City</option>
		<option value="deleted" <?php if($this->selected_status === "deleted") echo 'selected="selected"';?>>Deleted Members</option>
		<option value="growth" <?php if($this->selected_status === "growth") echo 'selected="selected"';?>>Growth</option>

	</select>

</div>
*************************************/?>