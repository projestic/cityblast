<?
	$member = $this->member;
	
	$tax_data = $member->getTaxData();
	
	//$selected_billing_cycle = isset($_REQUEST['billing_cycle']) ? $_REQUEST['billing_cycle'] : null;
	
	$selected_country = isset($_REQUEST['country']) ? $_REQUEST['country'] : null;
	$selected_country = empty($selected_country) && !empty($tax_data['country']) ? $tax_data['country'] : $selected_country;
	
	$selected_state = isset($_REQUEST['state']) ? $_REQUEST['state'] : null;
	$selected_state = empty($selected_state) && !empty($tax_data['state']) ? $tax_data['state'] : $selected_state;

?>





<!--[if lte IE 10]>
<style type="text/css">
	.registration .selectWrapper select{
		padding-right: 0;
	}
</style>
<![endif]-->

	<noscript>
	<h1 style="color: #990000;">ERROR: This page MUST have JavaScript activated in order to work properly!</h1> 
	</noscript>
	
	
	<div id="payment-errors-wrapper" <? if(!$this->payment_error) : ?>style="display: none;"<? endif; ?>>
	
		<div>
			<h1 style="color: #cc0000;">Oh Dear, Something Went Wrong!</h1>
			<div id="payment-errors"><?=$this->payment_error;?></div>
		</div>
		
	</div>



<div class="pull-left clearfix">

	<?= $this->partial('common/stripe-min.creditcard.html.php', array('member' => $this->member, 'states' => $this->states, 'show_promo_code' => false ) ); ?>
		
	<div class="clearfix">
		<input type="submit" name="submitted" style="margin-top: 25px;" class="pull-right btn btn-large btn-primary submit-button" id="submit_btn" value="Submit" />
	</div>


</div>

