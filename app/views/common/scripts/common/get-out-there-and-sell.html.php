<div class="contentBox experts">
	<div style="margin: 0 40px">
		<h2>CityBlast's Social Media Experts let you get back out there and SELL!</h2>
		<p class="center"><strong>That's how <?=COMPANY_NAME;?>'s founder, Shaun Nilsson, was able to make over $165,000 in commissions through posting on his Facebook alone - in his FIRST YEAR AS AN AGENT!</strong></p>
		<p class="center">Facebook is where your real estate business really needs to shine in order for you to be successful in today's market... and CityBlast gives you the professional presence you need, with none of the daily hassle.</p>
		<p class="center" style="font-size: 20px; margin: 40px 0;"><strong>But how does it work?</strong> It's pretty simple, actually.</p>
	</div>
	<div class="step">
		<img src="/images/icon-pencil.png">
		<h3>Sign Up</h3>
		<p>When you first sign up (PS - we offer a 14-day totally risk-free trial) just tell your <i>Social Expert</i> a exactly how you want them to post.</p>
	</div>
	<div class="step">
		<img src="/images/icon-experts.png">
		<h3>And we do the rest!</h3>
		<p>Our <i>Social Experts</i> will then harness the power of  "<a href="/blog/how-to-use-inbound-marketing-to-generate-real-estate-leads/" title="Inbound Marketing">Inbound Marketing</a>" to convert your friends, fans and followers into leads.</p>
	</div>
	<div class="result" style="float:left">
		<img src="/images/satisfaction.png" alt="Satisfaction">
	</div>
	<div class="clear"></div>
	<div class="social-experts">
		<div class="pull-left tryDemoTxt">
			<h3>Our <i>Social Experts</i> can help.</h3>
			<h4>Start your Free 14-Day Trial of <?=COMPANY_NAME;?> <i>Social Experts</i> now.</h4>
		</div>
		<div class="pull-right">
			<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>
		</div>
	</div>
	<div class="clear"></div>
</div>