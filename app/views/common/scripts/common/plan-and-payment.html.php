<?php
$member = $this->member;

$tax_data = $member->getTaxData();

$selected_country = isset($_REQUEST['country']) ? $_REQUEST['country'] : null;
$selected_country = empty($selected_country) && !empty($tax_data['country']) ? $tax_data['country'] : $selected_country;

$selected_state = isset($_REQUEST['state']) ? $_REQUEST['state'] : null;
$selected_state = empty($selected_state) && !empty($tax_data['state']) ? $tax_data['state'] : $selected_state;
?>


<input type="hidden" name="rebill" value="1" />


<!--[if lte IE 10]>
<style type="text/css">
	.registration .selectWrapper select{
		padding-right: 0;
	}
</style>
<![endif]-->

	<noscript>
	<h1 style="color: #990000;">ERROR: This page MUST have JavaScript activated in order to work properly!</h1> 
	</noscript>
	

	<div class="contentBoxTitle" id="payment-errors-wrapper" style="<? if(!$this->payment_error) : ?>display: none;<? endif; ?>background-color: #ffd6d8; padding-top: 10px; padding-bottom: 10px; " >
	
		<div>
			<h1 style="color: #cc0000;">Oh Dear, Something Went Wrong!</h1>
			<div id="payment-errors" style="margin-top: 10px; font-size: 16px; color: #cc0000;" ><?=$this->payment_error;?></div>
		</div>
		
	</div>


	
	<div class="contentBoxTitle">
		<h1><i class="icon-lock icon-8" style="color: #e0e0e0;"></i>&nbsp;&nbsp;Your Credit Card Information.</h1>
		<?php if (!$this->rebill): ?>
		<h3 style="color: #888">Cancel Your <u>FREE</u> 14 Day Trial Anytime: Lock in Your Price <u><i>Today</i></u>.</h3>
		<?php endif; ?>


		<?	
			/**********			
			if(isset($_COOKIE['promo_code']))
			{
				if($promo = Promo::find_by_promo_code($_COOKIE['promo_code']))
				{
					echo "<h3 style='color: #5fb004'>You Save An Additional ";
					if($promo->type == "FIXED_AMOUNT") echo "$".number_format($promo->amount, 2);
					else echo number_format($promo->amount) . "%";
					
					echo " Promo Code: <u><i>" . (isset($_COOKIE['promo_code']) ? $_COOKIE['promo_code'] : '') ."</i></u></h3>";	
				}
			} *********/
			
		?>
		
	</div>






<div class="pull-left clearfix">


	<?= $this->partial('common/stripe-min.creditcard.html.php', array('member' => $this->member, 'states' => $this->states ) ); ?>
		
	<div class="clearfix" style="margin-left: 18px;">
<?php
if ($this->rebill) {
	$button_label = 'Submit';
} else {
	$button_label = 'Start Your 14 Day FREE Trial';
}
?>
		<input type="submit" name="submitted" style="margin-top: 25px; width: 100%" class="pull-right btn btn-large btn-primary submit-button" id="free_trial_submit_btn" value="<?=$button_label?>" />
	</div>


</div>

<div class="pull-right" style="width: 280px">

	<div style="margin-top: 30px; margin-bottom: 30px;">
		<img src="/images/master-cards.png" />
	</div>


	<div style="margin-bottom: 30px;">
		

		
		<div id="savings"><h2 style='color: #888'>No Contract</h2></div>
		
		<div id="promo_code_div" style="margin-top: 5px; margin-bottom: 5px; display: none; font-size: 22px; color: #5fb004"></div>
		
		<?/*<div id="original_price" style="text-decoration: line-through; font-size: 20px; color: #ff0000;">59.99</div>*/?>
		
		<h3 id="price_desc">$59.99 x 1 month</h3>
		

		<div style="font-weight: bold; border-top: 1px solid #b9b9b9;" class="clearfix">
			<div style="float: left;">Total:</div>
			<div style="float: right;">$<span id="total_price"><span class="dollars" style="font-size: 18px"></span>.<span class="cents" style="font-size: 10px;">99</span></span></div>
		</div>
		
		<?/*<h3>Your Price: </h3>*/?>

	
	</div>
	
	<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
		<div id="member_type_switch">
			<div class="aboutPlanNote" style="<?=($this->member->type->code == 'BROKER') ? 'display:none' : '';?>">Are you planning on using <a href="#" data-member_type_id="2"><?=COMPANY_NAME;?> for your Brokerage</a>?</div>			
			<div class="aboutPlanNote" style="<?=($this->member->type->code == 'AGENT') ? 'display:none' : '';?>">Are you an <a href="#"  data-member_type_id="1">individual looking to use <?=COMPANY_NAME;?></a>?</div>
		</div>
	<? endif; ?>
</div>	



<div class="clearfix">&nbsp;</div>



<script type="text/javascript">
$(document).ready(function()
{	
	var price_manager = {
		prices: {},
		updatePrices: function(onSuccess)
		{
			var member_id = '<?=$member->id?>';
			var type_id = $("#type_id").val();
			var billing_cycle = $("#billing_cycle").val();
			var promo_code = $("#promo_code").val();
			
			var jsonUrl = "/member/get_prices/member_id/"+member_id+"/type_id/"+type_id+"/billing_cycle/"+billing_cycle+"/promo_code/"+promo_code;  

	
			$.getJSON(  
				jsonUrl,   
				function(result) 
				{  
					if(result['error'])
					{
						$.colorbox({width:"550px", height: "220px", html:"<h3 style='margin-top: 0px; padding-top: 0px;'>Error.</h3><p style='font-size: 130%; width: 480px;'>"+result['error']+"</p>"});
					} 
					else if(!result['error'] && onSuccess)
					{	
						price_manager.prices = result;
						onSuccess(result);
					}  
				}  
			); 
		},
		renderPriceOptions: function(type_id)
		{			
			// Set default prices
			var price_data = price_manager.prices;
			$(".plan_month .timePeriod .price_pay").html(price_data[type_id]['month']['price']);
			$(".plan_month .dollars").html(price_data[type_id]['month']['price_month_dollars']);
			$(".plan_month .cents").html(price_data[type_id]['month']['price_month_cents']);
			$(".plan_biannual .timePeriod .price_pay").html(price_data[type_id]['biannual']['price']);
			$(".plan_biannual .dollars").html(price_data[type_id]['biannual']['price_month_dollars']);
			$(".plan_biannual .cents").html(price_data[type_id]['biannual']['price_month_cents']);
			$(".plan_annual .timePeriod .price_pay").html(price_data[type_id]['annual']['price']);
			$(".plan_annual .dollars").html(price_data[type_id]['annual']['price_month_dollars']);
			$(".plan_annual .cents").html(price_data[type_id]['annual']['price_month_cents']);
			
			if (type_id == price_data[type_id]['type_id']) {
				$(".plan_" + price_data[type_id]['billing_cycle'] + " .timePeriod .price_pay").html(price_data[type_id]['price']);
				$(".plan_" + price_data[type_id]['billing_cycle'] + " .dollars").html(price_data[type_id]['price_month_dollars']);
				$(".plan_" + price_data[type_id]['billing_cycle'] + " .cents").html(price_data[type_id]['price_month_cents']);
			}
		},
		renderPriceTotals: function(type_id, billing_cycle)
		{
			var price_data = price_manager.prices;
			var prices = price_data[type_id][billing_cycle];

			//alert('Billing Cycle:'+billing_cycle);
			
			if(billing_cycle == "month")
			{
				$("#savings").slideUp(function(){
					$("#savings").html("<h2 style='color: #888'>No Contract</h2>");
					$("#savings").slideDown();				
				});


			}
			if(billing_cycle == "biannual")
			{
				$("#savings").slideUp(function(){
					$("#savings").html("<h2 style='color: #ff0000;'>Save 15%</h2>");
					$("#savings").slideDown();				
				});	
	
			}
			if(billing_cycle == "annual")
			{
				$("#savings").slideUp(function(){
					$("#savings").html("<h2 style='color: #ff0000;'>Save 33%</h2>");
					$("#savings").slideDown();					
				});	
			}
						
			
			//$("#total_price").html(price_data[type_id][billing_cycle]['price']); 
			$("#total_price .dollars").html(prices['price_dollars']); 
			$("#total_price .cents").html(prices['price_cents']); 
			var price_desc = price_manager.getPriceTotalDesc(prices['billing_cycle_months'], prices['price_month']);
			$("#price_desc").html(price_desc); 
		},
		getPriceTotalDesc: function(billing_cycle_months, price_month)
		{
			if(billing_cycle_months == "month") var price_desc =  '$' + price_month + ' x ' + billing_cycle_months + ' month';
			else var price_desc =  '$' + price_month + ' x ' + billing_cycle_months + ' months';
			return price_desc;
		},
		renderPricePromo: function(billing_cycle)
		{
			var price_data = price_manager.prices;
			if (price_data['promo_code'] && !price_data['error'])
			{
				if (price_data['promo_code_valid']) 
				{
					$.colorbox({width:"575px", height: "270px", html:"<h3 style='margin-top: 0px; padding-top: 0px;'><img src='/images/checkmark1.png' style='width: 30px; margin: 4px 10px 0 0;' />Success!</h3><p style='font-size: 130%; width: 480px; margin-top: 8px;'>The Promotional Code <b>" + $("#promo_code").val() + "</b> has been applied to your account!</p><a href='#' onclick='$.colorbox.close();' class='btn btn-primary btn-large pull-right' style='width: 195px; margin-top: 10px;'>Continue</a>"});

					$("#promo_code_div").slideDown();
					$("#promo_code_div").html("*You Save An Additional: <b>" + price_data['promo_code_discount'] + "</b>");  
				} else {
					$.colorbox({width:"575px", height: "270px", html:"<h3 style='margin-top: 0px; padding-top: 0px;'><img src='/images/cross.jpg' style='width: 30px; margin: 4px 10px 0 0;' />Error!</h3><p style='font-size: 130%; width: 480px; margin-top: 8px;'>The Promotional Code you provided is not valid.</p><a href='#' onclick='$.colorbox.close();' class='btn btn-primary btn-large pull-right' style='width: 195px; margin-top: 10px;'>Continue</a>"});
					$("#promo_code_div").html('');  
					$("#promo_code_div").slideUp();
				}
			}
		},
		selectedBillingCycle: function(type_id, billing_cycle)
		{
			$.each(['month', 'biannual', 'annual'], function(index, bc) 
			{
				if (bc == billing_cycle) 
				{
					$(".plan_" + bc).addClass('popular');
					$(".plan_" + bc + ' .select_button').html("Selected.");
					$(".planContainer .plan_" + bc + " .plan_title").html('<img src="/images/new-images/plan-title-green-bg.png" />');
					$(".mobilePlanContainer .plan_" + bc + " .plan_title").html('<img src="/images/new-images/small-plan-title-green-bg.png" />');
				} else {
					$(".plan_" + bc).removeClass('popular');
					$(".plan_" + bc + ' .select_button').html("Select.");
					$(".planContainer .plan_" + bc + " .plan_title").html('');
					$(".mobilePlanContainer .plan_" + bc + " .plan_title").html('');
				}
			});
			
			$("#billing_cycle").val(billing_cycle);
			price_manager.renderPriceTotals(type_id, billing_cycle);
		},
		updateRendering: function () {
			var type_id = $("#type_id").val();
			var billing_cycle = $("#billing_cycle").val();
			price_manager.renderPriceOptions(type_id);
			price_manager.renderPriceTotals(type_id, billing_cycle);
			price_manager.selectedBillingCycle(type_id, billing_cycle);
		}
	};
	
	
	// Init billing cycle selection
	price_manager.updatePrices(price_manager.updateRendering);
	
	$("#free_trial_submit_btn").click(function(e)
	{
		$(this).addClass("cancel");
		$(this).val("Processing");
	});	


	$("#billing_cycle").bind("change", function() 
	{ 
		var type_id = $("#type_id").val();
		price_manager.updateRendering();
	});	
	
	/********************
	$('.plan_month .select_button').click(function(e)
	{
		var type_id = $("#type_id").val();
		price_manager.selectedBillingCycle(type_id, 'month');
	});

	$('.plan_biannual .select_button').click(function(e)
	{
		var type_id = $("#type_id").val();
		price_manager.selectedBillingCycle(type_id, 'biannual');
	});

	$('.plan_annual .select_button').click(function(e)
	{
		var type_id = $("#type_id").val();
		price_manager.selectedBillingCycle(type_id, 'annual');
	});
	*******************/
	
	$('#member_type_switch a').click(function(e)
	{
		var member_type_id = $(e.target).attr('data-member_type_id');
		$("#type_id").val(member_type_id);
		
		price_manager.updatePrices(function(price_data)
		{
			var type_id = $("#type_id").val();


			if(type_id == 2)
			{
				$(".price").css('padding-left','0px');
			}
			else
			{
				$(".price").css('padding-left','30px');
			}
			
			var billing_cycle = $("#billing_cycle").val();
			price_manager.updateRendering();
			
			$('#member_type_switch a').each(function()
			{
				var elemnt_member_type_id = $(this).attr('data-member_type_id');
				if (elemnt_member_type_id != member_type_id) 
				{
					$(this).parent('.aboutPlanNote').show();
				} else {
					$(this).parent('.aboutPlanNote').hide();
				}
			});
			
			$("#promo_code").val('');
		});
		
		e.preventDefault();
		return false;
	});
	
	// Prevent scroll to top of page on click of '#' URL
	$(".select_button").click(function(e)
	{
		e.preventDefault();
		return false;
	});
	
	$("#promo_btn").click(function(e)
	{
		price_manager.updatePrices(function(price_data){
			var type_id = $("#type_id").val();
			var billing_cycle = $("#billing_cycle").val();
			price_manager.renderPriceOptions(type_id);
			price_manager.renderPriceTotals(type_id, billing_cycle);
			price_manager.renderPricePromo(billing_cycle);
		});
		e.preventDefault();
	});
	
	<? if(isset($_COOKIE['promo_code'])) : ?>
	
		price_manager.updatePrices(function(price_data){

			var type_id = $("#type_id").val();
			var billing_cycle = $("#billing_cycle").val();
			price_manager.renderPriceOptions(type_id);
			price_manager.renderPriceTotals(type_id, billing_cycle);
			price_manager.renderPricePromo(billing_cycle);
		});	
	<? endif; ?>

});
</script>