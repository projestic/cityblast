<?php
$tax_data = $this->member->getTaxData();

$selected_billing_cycle = isset($_REQUEST['billing_cycle']) ? $_REQUEST['billing_cycle'] : null;

$selected_country = isset($_REQUEST['country']) ? $_REQUEST['country'] : null;
$selected_country = empty($selected_country) && !empty($tax_data['country']) ? $tax_data['country'] : $selected_country;

$selected_state = isset($_REQUEST['state']) ? $_REQUEST['state'] : null;
$selected_state = empty($selected_state) && !empty($tax_data['state']) ? $tax_data['state'] : $selected_state;
?>

<script type="text/javascript" src="https://js.stripe.com/v1/"></script>

<script type="text/javascript">

    // this identifies your website in the createToken call below
    Stripe.setPublishableKey('<?=STRIPE_PUBLISH;?>');

	function stripeResponseHandler(status, response) 
	{
		if (response.error) 
		{
			// re-enable the submit button
			$('.submit-button').removeAttr("disabled");

			$("#submit_btn").removeClass("cancel");
			$("#submit_btn").val("Submit");
		
			
			$("#payment-errors-wrapper").show();
		
			//alert('Error!');
			
			// show the errors on the form
			$("#payment-errors").html(response.error.message);
		} else {

			var form$ = $("#payment-form");
			
			// token contains id, last4, and card type
			var token = response['id'];
			
			// insert the token into the form so it gets submitted to the server
			form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
			
			// and submit
			form$.get(0).submit();
		}
	}
	
	function countrySelectedUpdate()
	{
		var country_code = $('#country').val();
		
		var state_options = $('#state > option');
		
		var state_init_selected = state_options.filter(':selected');
		
		var x = 0;
		state_options.each(function(){
			var element = $(this);
			var val = element.val();
			if (element.hasClass('country_code_' + country_code)) {
				// This is an option for this country
				x++;
				element.removeAttr('disabled');
				element.removeAttr('selected');
				if (x == 1) {
					element.attr('selected','selected');
				}
				element.show();
			} else {
				// This is an option for a different country
				element.removeAttr('selected');
				element.attr('disabled','disabled');
				element.hide();
			}
		});
		
		// if initialy selected state is in selected country, re-selected it
		if (state_init_selected.hasClass('country_code_' + country_code)) {
			state_init_selected.attr('selected','selected');
		}
		
		if (x == 0) {
			var default_option = $('#state > option[value=""]');
			//default_option.attr('selected','selected');
			default_option.removeAttr('disabled');
			default_option.show();
		}
	}
	
	$(document).ready(function()
	{
		$("#payment-form").submit(function(event) 
		{
			event.preventDefault();
			
			
			// disable the submit button to prevent repeated clicks
			$('.submit-button').attr("disabled", "disabled");
		
			// createToken returns immediately - the supplied callback submits the form if there are no errors
			Stripe.createToken({
		   		number: $('.card-number').val(),
		    		cvc: $('.card-cvc').val(),
		    		exp_month: $('.card-expiry-month').val(),
		    		exp_year: $('.card-expiry-year').val()
			}, stripeResponseHandler);
			
			return false; // submit from callback
		});
		
		
		countrySelectedUpdate();
		$('#country').change(function(event){
			countrySelectedUpdate();
		});
	});
	
</script>


<?
	/***************
	$city = null;
	if(isset($_SESSION['member'])) $city = $_SESSION['member']->default_city;


	
	function check_state($city, $STATE)
	{
		if(	isset($_REQUEST['state']) && strtoupper($_REQUEST['state']) == strtoupper($STATE)		) echo " selected ";
		if(	isset($city) && !empty($city->state) && strtoupper($city->state) == strtoupper($STATE)	) echo " selected ";
	}
	function check_country($city, $SHORT_CODE, $COUNTRY)
	{
		if(isset($_REQUEST['cc_country']) && $_REQUEST['cc_country'] == $SHORT_CODE) echo " selected ";
		if(	isset($city) && !empty($city->country) && strtoupper($city->country) == strtoupper($COUNTRY)	) echo " selected ";
	}
	**********/
?>

<!--[if lte IE 10]>
<style type="text/css">
	.registration .selectWrapper select{
		padding-right: 0;
	}
</style>
<![endif]-->

<noscript>
<h1 style="color: #990000;">ERROR: This page MUST have JavaScript activated in order to work properly!</h1> 
</noscript>

<div class="registration">
	<input type="hidden" name="billing_cycle" value="<?=$selected_billing_cycle?>">
	<div class="title"><span></span>Secure Your Discount and Complete Registration.</div>

	<div class="paymentInfo">
		
		<div id="payment-errors-wrapper" <? if(!$this->payment_error) : ?>style="display: none;"<? endif; ?>>

			<div>
				<h4 style="color: #cc0000;">Oh Dear, Something Went Wrong!</h4>
				<div id="payment-errors"><?=$this->payment_error;?></div>
			</div>
			
		</div>

		<div style="margin-bottom: 30px;">
			<img src="/images/master-cards.png" />
		</div>
		
		
		
					
		<div class="clearfix">
			<label for="country" class="pull-left">Billing State/Province</label>
		</div>
		<div class="clearfix fieldContainer">
			<span class="selectWrapper span4">
				<select id="country" name="country" class="span12">
					<option value="">Country</option>
					<option value="CA" <? if ($selected_country == "CA") echo 'selected="selected"'; ?>>Canada</option>
					<option value="US" <? if ($selected_country == "US") echo 'selected="selected"'; ?>>United States</option>
				</select>
			</span>
			<span class="selectWrapper span6">
				<select id="state" name="state" class="span12">
					<option value="">State/Province</option>
					<?
						if (!empty($this->states)) {
							foreach ($this->states as $state) {
								?>
								<option value="<?=$state->code?>" <? if ($selected_state == $state->code) echo 'selected="selected"'; ?> class="country_code_<?=$state->country_code?>" style="display: none;" disabled="disabled"><?=$state->name?></option>
								<?
							}
						}
					?>
				</select>
			</span>
		</div>
		
		<div class="clearfix">
			<label for="cardnumber" class="pull-left">Card Number</label>
			<span class="info pull-right">The 16 digits on the front of your card</span>
		</div>
		<div class="fieldContainer">
			<input type="text" id="cardnumber" name="cardnumber" class="span12 card-number" />
		</div>
		
		<div class="clearfix">
			<label for="expmonth" class="pull-left">Expiration Date</label>
		</div>
		<div class="clearfix fieldContainer">
			<span class="selectWrapper span3">
				<select id="expmonth" name="expmonth" class="card-expiry-month span12">
					<option value="">Month</option>
					<option value="01" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "01") echo "selected"; ?>>01</option>
					<option value="02" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "02") echo "selected"; ?>>02</option>
					<option value="03" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "03") echo "selected"; ?>>03</option>
					<option value="04" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "04") echo "selected"; ?>>04</option>
					<option value="05" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "05") echo "selected"; ?>>05</option>
					<option value="06" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "06") echo "selected"; ?>>06</option>
					<option value="07" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "07") echo "selected"; ?>>07</option>
					<option value="08" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "08") echo "selected"; ?>>08</option>
					<option value="09" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "09") echo "selected"; ?>>09</option>
					<option value="10" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "10") echo "selected"; ?>>10</option>
					<option value="11" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "11") echo "selected"; ?>>11</option>
					<option value="12" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "12") echo "selected"; ?>>12</option>
				</select>
			</span>
			<span class="selectWrapper span3">
				<select id="expyear" name="expyear" class="card-expiry-year span12">
					<option>Year</option>
					<?
						$minyear = date("Y");
						$maxyear = date("Y") + 10;
						for($year=$minyear;$year<=$maxyear;$year++)
						{
							echo "<option value='$year'";
							if(isset($_REQUEST['exp_year']) && $_REQUEST['exp_year'] == $year) echo " selected ";
							echo ">$year</option>";
						}
					?>
				</select>
			</span>
		</div>
		
		<div class="codeContainer clearfix">
			<div class="securityCode">
				<div class="clearfix">
					<label for="securitycode" class="pull-left">Security Code</label>
				</div>
				<div>
					<span class="info">Visa/MasterCard: 3 digits on back; Amex: 4 on front</span>
				</div>
				<div class="fieldContainer">
					<input type="text" id="securitycode" name="securitycode" class="card-cvc" />
					<img src="/images/visa-master-sec-code.png" alter="Visa/MasterCard" style="margin: 0 5px 10px 10px; vertical-align: bottom;" />
					<img src="/images/amex-sec-code.png" alter="Amex" style="vertical-align: bottom; margin-bottom: 10px;" />
				</div>
			</div>
		</div>
		

		<div class="clearfix">
			<label for="promo_code" class="pull-left">Enter Promo Code</label>
			<div>
				<span class="info">(Optional)</span>
			</div>

		</div>
		<div class="fieldContainer">
			
			<input type="text" name="promo_code" id="promo_code" style="width: 155px;" value="<?=($this->promo) ? $this->promo->promo_code : ''?>" />
			<? if(!isset($this->redeem_btn_inactive)) :?>
				<a href="#" id="promo_btn" style="background-color: #959595; border-radius: 3px 3px 3px 3px; color: #FFFFFF;font-size: 15px;font-weight: normal;line-height: 10px;padding: 10px 6px 10px;vertical-align:bottom;text-shadow: none;display: inline-block;margin-left: 5px;margin-bottom: 10px;">Redeem Code</a>
			<? endif; ?>					
		</div>
		<div class="clearfix">
			<input type="submit" name="submitted" class="pull-right btn btn-large btn-danger submit-button" id="submit_btn" value="Complete Registration" />
		</div>
	</div>
	<div class="sponsors">
		<img src="/images/sponsors.png" usemap="sponsors" />
		<map name="sponsors">
			<area shape="circle" coords="170 31 30" target="_blank" href="https://www.paypal.com/us/verified/pal=crowonder%40yahoo%2ecom"/>
		</map>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function()
{
	var member_id = "<?=$this->member->id;?>";
	var current_price = "<?=$this->member->price;?>";
	
	$("#submit_btn").click(function(e)
	{
		$(this).addClass("cancel");
		$(this).val("Processing");
	});	
	$("#promo_btn").click(function(e)
	{
		e.preventDefault();

		var jsonUrl = "/promo/get_price/"+member_id+"/"+$("#promo_code").val();  
		
		
		$.getJSON(  
			 jsonUrl,   
			 function(result) 
			 {  
			 	$.each(result, function(key, value)
			 	{
			 		 if(key == "error")
			 		 {
			 		 	/*alert('there was an error: ' + value);*/
			 		 	$.colorbox({width:"550px", height: "220px", html:"<h1 style='margin-top: 0px; padding-top: 0px;'>Error!</h1><p style='font-size: 130%; width: 480px;'>"+value+"</p>"});
			 		 }
			 		 			 		 
			 		 if(key == "promo_code")
			 		 {
			 		 	$("#promo_code_div").html(value);  
			 		 }
			 		 			 		 
			 		 if(key == "new_price")
			 		 {
			 		 	$.colorbox({width:"550px", height: "220px", html:"<h1 style='margin-top: 0px; padding-top: 0px;'><img src='/images/checkmark1.png' style='width: 30px; margin: 4px 10px 0 0;' />Success!</h1><p style='font-size: 130%; width: 480px;'>The Promo Code has been applied to your account!</p>"});
			 		 	$("#discount_price").html(value);  
			 		 }
			 	});	
			     
 
			 }  
		);  


		
	});



			
});
</script>