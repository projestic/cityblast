<?php
	$member = $this->member;
	
	$tax_data = $member->getTaxData();
	
	//$selected_billing_cycle = isset($_REQUEST['billing_cycle']) ? $_REQUEST['billing_cycle'] : null;
	
	$selected_country = isset($_REQUEST['country']) ? $_REQUEST['country'] : null;
	$selected_country = empty($selected_country) && !empty($tax_data['country']) ? $tax_data['country'] : $selected_country;
	
	$selected_state = isset($_REQUEST['state']) ? $_REQUEST['state'] : null;
	$selected_state = empty($selected_state) && !empty($tax_data['state']) ? $tax_data['state'] : $selected_state;

?>
	<ul class="uiForm clearfix">

		<li class="uiFormRow clearfix">

			<? if(isset($this->rebill) && $this->rebill) : ?>
			
				<input type="hidden" id="billing_cycle" name="billing_cycle" value="<?=$member->billing_cycle?>"/>			
				
			<? else: ?>
			
				<ul>
					<input type="hidden" id="type_id" name="type_id" value="<?=$member->type_id?>"/>
	
					<li class="fieldname cc"><span style="color: #990000;">*</span>Choose A Plan</li>
					<li class="field cc">		
	
						<select id="billing_cycle" name="billing_cycle" value="<?=$member->billing_cycle;?>" class="span12">
							<option value="month" <? if ($member->billing_cycle == "month") echo 'selected="selected"'; ?>>Monthly - No contract, cancel any time.</option>
							<option value="biannual" <? if ($member->billing_cycle == "biannual") echo 'selected="selected"'; ?>>Bi-Annual - Pay 6 months in advance, save 15%</option>
							<option value="annual" <? if ($member->billing_cycle == "annual") echo 'selected="selected"'; ?>>Annual - Pay 12 months in advance, save 33%</option>
						</select>
	
					</li>
				</ul>
			
			<? endif; ?>
			

			
			<ul>

				<li class="fieldname"><span style="color: #990000;">*</span>Country / State</li>
				<li class="field">		
					<select id="country" name="country" class="span4">
						<option value="">Select</option>
						<option value="CA" <?php if ($selected_country == "CA") echo 'selected="selected"'; ?>>Canada</option>
						<option value="US" <?php if ($selected_country == "US") echo 'selected="selected"'; ?>>United States</option>
					</select>

					<select id="state" name="state" class="span6">
						<option value="">State/Province</option>
						<?php
							if (!empty($this->states)) 
							{
								foreach ($this->states as $state) 
								{
									?>
									<option value="<?=$state->code;?>" <? if ($selected_state == $state->code) echo 'selected="selected"'; ?> 
										class="country_code_<?=$state->country_code;?>"><?=$state->name;?></option>
									<?
								}
							}
						?>
					</select>
				</li>
				
				
				<li class="fieldname cc"><span style="color: #990000;">*</span>Card Number <span class="payment_hint pull-right">The 16 digits on the front of your card</span></li>
				<li class="field cc">											
					<input type="text" id="cardnumber" class="uiInput card-number" />
				</li>




				<li class="fieldname cc"><span style="color: #990000;">*</span>Expiration Date</li>
				<li class="field cc">		
					

					<select id="expmonth" class="card-expiry-month span3">
						<option value="">Month</option>
						<option value="01" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "01") echo "selected"; ?>>01</option>
						<option value="02" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "02") echo "selected"; ?>>02</option>
						<option value="03" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "03") echo "selected"; ?>>03</option>
						<option value="04" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "04") echo "selected"; ?>>04</option>
						<option value="05" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "05") echo "selected"; ?>>05</option>
						<option value="06" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "06") echo "selected"; ?>>06</option>
						<option value="07" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "07") echo "selected"; ?>>07</option>
						<option value="08" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "08") echo "selected"; ?>>08</option>
						<option value="09" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "09") echo "selected"; ?>>09</option>
						<option value="10" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "10") echo "selected"; ?>>10</option>
						<option value="11" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "11") echo "selected"; ?>>11</option>
						<option value="12" <? if (isset($_REQUEST['exp_month']) && $_REQUEST['exp_month'] == "12") echo "selected"; ?>>12</option>
					</select>
	

	
					<select id="expyear" class="card-expiry-year span3">
						<option>Year</option>
						<?
							$minyear = date("Y");
							$maxyear = date("Y") + 10;
							for($year=$minyear;$year<=$maxyear;$year++)
							{
								echo "<option value='$year'";
								if(isset($_REQUEST['exp_year']) && $_REQUEST['exp_year'] == $year) echo " selected ";
								echo ">$year</option>";
							}
						?>
					</select>


				</li>


				<li class="fieldname cc"><span style="color: #990000;">*</span>CVV2 / Security Code</li>
				<li class="field cc">	

					<input type="text" id="securitycode" class="uiInput card-cvc" style="width: 50px;"/>
					<img src="/images/visa-master-sec-code.png" alter="Visa/MasterCard" style="margin: 0 5px 10px 10px; vertical-align: bottom;" />
					<img src="/images/amex-sec-code.png" alter="Amex" style="vertical-align: bottom; margin-bottom: 10px;" />
					
					<span class="payment_hint">Visa/MasterCard: 3 digits on back / Amex: 4 on front</span>
				</li>
					

				<?php if (!isset($this->show_promo_code) || ($this->show_promo_code == true)) : ?>
				
					<li class="fieldname cc">Promo Code</li>
					<li class="field cc">	
	
						<input type="text" name="promo_code" id="promo_code" class="uiInput span6" value="<?=(isset($_COOKIE['promo_code'])) ? $_COOKIE['promo_code'] : ''?>" />
						<? if(!isset($this->redeem_btn_inactive)) :?>
							<a href="#" id="promo_btn" style="background-color: #959595; border-radius: 3px 3px 3px 3px; color: #FFFFFF;font-size: 15px;font-weight: normal;line-height: 10px;padding: 10px 6px 10px;vertical-align:bottom;text-shadow: none;display: inline-block;margin-left: 5px;margin-bottom: 10px;">Redeem Code</a>
						<? endif; ?>	
				
					</li>
					
				<?php endif; ?>
			</ul>
		
		
		</li>	
	</ul>


<script type="text/javascript" src="https://js.stripe.com/v1/"></script>

<script type="text/javascript">

    // this identifies your website in the createToken call below
    Stripe.setPublishableKey('<?=STRIPE_PUBLISH;?>');

	function stripeResponseHandler(status, response) 
	{
		if (response.error) 
		{
			// re-enable the submit button
			$('.submit-button').removeAttr("disabled");

			$("#submit_btn").removeClass("cancel");
			$("#submit_btn").val("Submit");
		
			
			$("#payment-errors-wrapper").show();
		
			//alert('Error!');
			
			// show the errors on the form
			$("#payment-errors").html(response.error.message);
			
		} else {

			var form$ = $("#payment-form");
			
			// token contains id, last4, and card type
			var token = response['id'];
			
			// insert the token into the form so it gets submitted to the server
			form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
			
			// and submit
			form$.get(0).submit();
		}
	}
	
	function provinceSelectedUpdate()
	{
		var country_code = $('#country').val();
		

		$("#state").empty();
		var options = '';

		$.getJSON ("/country/index", {id: $('#country').val(), ajax: 'true'}, 
			function(data) {
  				$.each(data, function(key, val) {
   					$("#state").append('<option value="'+key+'">'+val+'</option>')
  				});
		});

	}
	
	$(document).ready(function()
	{
		$("#payment-form").submit(function(event) 
		{
			event.preventDefault();
			
			// disable the submit button to prevent repeated clicks
			$('.submit-button').attr("disabled", "disabled");
		
			// createToken returns immediately - the supplied callback submits the form if there are no errors
			Stripe.createToken({
		   		number: $('#cardnumber').val(),
				cvc: $('#securitycode').val(),
				exp_month: $('#expmonth').val(),
				exp_year: $('#expyear').val()
			}, stripeResponseHandler);
			
			return false; // submit from callback
		});
		
		//provinceSelectedUpdate();		
		$('#country').change(function(event)
		{		
			provinceSelectedUpdate();
		});
		
	});

</script>		