<div class="container_12 clearfix">



	<div class="grid_12" style="margin-top: 30px;">
	
		
		<h1>Referral Credits<a href="/admin/add_manual_referral" class="uiButton"  style="float: right; margin-top: 0px; padding-top:0px; margin-right: 0px;">Add Manual Referral</a></h1>
		
		
		<table style="width: 100%" class="uiDataTable">
			<tr>
				<th style="padding:4px;">Member Id</th>
				<th style="padding:4px;">Member Name</th>
				<th style="padding:4px;">Member Email</th>
				<th style="padding:4px;">Referred Member Id</th>				
				<th style="padding:4px;">Referred Member Name</th>				
				<th style="padding:4px;">Referred Member Email</th>	
				<th style="padding:4px;">Payment ID</th>	
				<th style="padding:4px;">Date</th>	
			</tr>
			
		<?
			$row = true;
			if(iterator_count($this->paginator)):
				foreach($this->paginator as $referral) { 
				 	$class = ($row) ? 'odd' : ''; 
				 	$row   = !$row; 
			?>
					<tr class="<?php echo $class; ?>">
	
						<td style="vertical-align: top;" nowrap><a href="/admin/member/<?php echo $referral->member->id; ?>"><?php echo $referral->member->id; ?></a></td>
						<td style="vertical-align: top;" nowrap><?php echo $referral->member->first_name.' '.$referral->member->last_name; ?></td>
						<td style="vertical-align: top;"><?php echo $referral->member->email;?></td>
						<td style="vertical-align: top;" nowrap><a href="/admin/member/<?php echo $referral->referred_member->id; ?>"><?php echo $referral->referred_member->id; ?></a></td>
						<td style="vertical-align: top;" nowrap><?php echo $referral->referred_member->first_name.' '.$referral->referred_member->last_name; ?></td>
						<td style="vertical-align: top;"><?php echo $referral->referred_member->email;?></td>
						<td style="vertical-align: top;"><a href="/admin/payment/<?php echo $referral->payment_id;?>"><?php echo $referral->payment_id;?></a></td>
						<td style="vertical-align: top;"><?php echo $referral->created_at->format("Y-m-d H:i:s");?></td>
					</tr>
				<?php } ?>
			<? endif; ?>
				<?php
	
	?>
			
		</table>
	</div>
	
	
	<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />	
<div class="grid_12">
		<div style="height: 60px;">&nbsp;</div>
	</div>
	
</div>
