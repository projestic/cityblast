<?php
$date_range_years = 3;
$date_range_month = $date_range_years * 12;

?>

<div class="container_12 clearfix">

	<div class="grid_12" style="margin-bottom: 80px; margin-top: 30px;">

		<form method="POST">
	
			<div style="float: right;">
				<select id="sales_person_id" name="sales_person_id" style="font-size: 30px; font-weight: bold; width: 300px; margin-bottom: 5px;">
					<option value="">Sales Person Name</option>
					<?php
						foreach($this->list_members as $member) {
							if($this->sales_person_id == $member->id) {
								echo "<option selected=selected value='{$member->id}'>$member->first_name $member->last_name</option>";
							} else {
								echo "<option value='{$member->id}'>$member->first_name $member->last_name</option>";
							}
						}
					?>
				</select>
			</div>
			<div class="clearfix"></div>
			
			<h1 style="margin-top: 2px;">Staff Sales Leader Board
		
				<span style="float: right;">
					<input name="date_from" class="uiInput" value="<?=$this->date_from?>" style="margin-right: 5px; width: 85px;">
					<div style="float:left; margin-right: 5px; font-size: 14px; font-weight: normal;">to</div>
					<input name="date_to"  class="uiInput" value="<?=$this->date_to?>" style="margin-right: 5px; width: 85px;">
					<input type="submit" class="uiButton" value="Update"/>		
				</span>
				
			</h1>
		
		</form>
		


	<table style="width: 100%;" class="uiDataTable lineheight">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th style="text-align: right;" title="Members refferred that are currently trial status">Trial (Current)</th>
			<th style="text-align: right;" title="Members refferred that are currently paid status">Paid (Current)</th>
			<th style="text-align: right;" title="Number agents referred within specified dates">Agent</th>
			<th style="text-align: right;" title="Number brokers referred within specified dates">Broker</th>
			<th style="text-align: right;" title="Number brokers referred within specified dates">#Sales</th>
			<th style="text-align: right;" title="Current recur revenue by referred members">Revenue Recur Monthly</th>
			<th style="text-align: right;" title="Revenue from referred members within specified dates">Revenue</th>
			<th style="text-align: right;" title="Commission from referred members within specified dates">Commission</th>
		</tr>
		<?php if(!empty($this->members)): ?>
			<?php foreach($this->members as $member): ?>
		<tr>
			<td><a href="/admin/member/<?=$this->escape($member->id)?>"><?=$this->escape($member->id)?></a></td>
			<td><a href="/admin/member/<?=$this->escape($member->id)?>"><?=$this->escape($member->first_name)?> <?=$this->escape($member->last_name)?></a></td>
			<td style="text-align: right;"><a href="/admin/allmembers/affiliate_id/<?=$this->escape($member->id)?>/payment_status/signup"><?=$this->escape(number_format($member->trial_current))?></a></td>
			<td style="text-align: right;"><a href="/admin/allmembers/affiliate_id/<?=$this->escape($member->id)?>/payment_status/paid"><?=$this->escape(number_format($member->paid_current))?></a></td>
			<td style="text-align: right;"><a href="/admin/allmembers/affiliate_id/<?=$this->escape($member->id)?>/pay_date_from/<?=$this->date_from?>/pay_date_to/<?=$this->date_to?>/membership_type/1"><?=$this->escape(number_format($member->agent))?></a></td>
			<td style="text-align: right;"><a href="/admin/allmembers/affiliate_id/<?=$this->escape($member->id)?>/pay_date_from/<?=$this->date_from?>/pay_date_to/<?=$this->date_to?>/membership_type/2"><?=$this->escape(number_format($member->broker))?></a></td>
			<td style="text-align: right;"><?=$this->escape(number_format($member->count))?></td>
			<td style="text-align: right;"><a href="/admin/allmembers/affiliate_id/<?=$this->escape($member->id)?>/billing_cycle/month/payment_status/paid/show_revenue_recur_monthly/1"><?=$this->escape(number_format($member->revenue_recur_monthly, 2))?></a></td>
			<td style="text-align: right;"><a href="/admin/payments/from/<?=$this->date_from?>/to/<?=$this->date_to?>/affiliate_id/<?=$this->escape($member->id)?>"><?=$this->escape(number_format($member->revenue, 2))?></a></td>
			<td style="text-align: right;"><?=$this->escape(number_format($member->commission, 2))?></td>
		</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</table>
</div>
</div>

<script type="text/javascript">

	$('input[name=date_from], input[name=date_to]').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true
	});

</script>