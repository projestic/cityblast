<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">

	<div class="grid_12 member_update">
		
		<form id="extraForm" action="/admin/save_tag" method="POST">
		<input type="hidden" name="id" value="<?php if(isset($this->tag->id)) echo $this->escape($this->tag->id);?>" />
		
		<div class="box form clearfix">

			

			<ul class="uiForm clearfix">
				<li class="uiFormRowHeader">Tag</li>
				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Name</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="name" id="name" value="<? if(isset($this->tag->name)) echo $this->escape($this->tag->name);?>" class="uiInput" style="width: 200px" />
					    </li>
					</ul>
				</li>	
				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Reference Code</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="name" id="name" disabled="disabled" value="<? if(isset($this->tag->ref_code)) echo $this->escape($this->tag->ref_code);?>" class="uiInput" style="width: 200px" />
						    <p style="float: right; width: 380px;margin: 00 0 10px;">This is used internaly to associate special functionality. Think carefully before changing the name of a tag that has a reference code.</p>
					    </li>
					</ul>
				</li>

				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Description</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="description" id="description" value="<? if(isset($this->tag->description)) echo $this->escape($this->tag->description);?>" class="uiInput" style="width: 500px" />
					    </li>
					</ul>
				</li>

				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Group</li>
						<li class="field">
							
							<span class="uiSelectWrapper"><span class="uiSelectInner">
							<select name="tag_group_id" class="uiSelect">
								<option value=""></option>
								<?php 
									if ($this->tag_groups) {
										foreach ($this->tag_groups as $group) {
											echo "<option value='".$this->escape($group->id)."' ";
											if($this->tag && $this->tag->tag_group_id == $group->id) echo " selected ";
											echo ">" . $this->escape($group->name) . "</option>";
										}
									}
								?>
							</select>
						</li>
					</ul>
				</li>	
			</ul>
		</div>
		
		<? foreach ($this->member_types as $member_type): ?>
<?php
	$member_type_settings = !empty($this->member_type_settings[$member_type->id]) ? $this->member_type_settings[$member_type->id] : null;
	$available = $member_type_settings && $member_type_settings->available ? 1 : 0;
	$default = $member_type_settings && $member_type_settings->default_value ? 1 : 0;
?>
			<div class="box form clearfix">
				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader"><?=$member_type->name?> Settings</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Available</li>
							<li class="field" style="padding-top: 20px;">
								<input type="radio" name="member_type_settings[<?=$member_type->id?>][available]" value="1"  <? if($available) echo 'checked="checked"';?>  /> On
								<input type="radio" name="member_type_settings[<?=$member_type->id?>][available]" value="0"  <? if(!$available) echo 'checked="checked"';?>  /> Off
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Default</li>
							<li class="field" style="padding-top: 20px;">
								<input type="radio" name="member_type_settings[<?=$member_type->id?>][default_value]" value="1"  <? if($default) echo 'checked="checked"';?>  /> On
								<input type="radio" name="member_type_settings[<?=$member_type->id?>][default_value]" value="0"  <? if(!$default) echo 'checked="checked"';?>  /> Off
							</li>
						</ul>
					</li>	
				</ul>
			</div>
		<? endforeach; ?>

		<input type="submit" name="submitted" class="uiButton large right" value="Save">

		
	</form>
	
</div>