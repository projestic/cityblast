<form method="post" action="<?php echo $this->target ?>" target="_top">
	
	<h3>Delete <?php echo $this->type ?></h3>
	<p>Please enter a reason for deleting this <?php echo $this->type ?>:</p>
	<p><textarea name="reason" style="width: 100%; height: 120px"></textarea></p>
    <p>
        Send these comments to listing creator? 
        <input name="notify_creator" type="radio" value="1"/> Yes 
        <input name="notify_creator" type="radio" checked="checked" value="0"/> No
    </p>
	<p><input type="submit" name="submit" class="uiButton" value="Delete <?php echo $this->type ?>"></p>
	
</form>