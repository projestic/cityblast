<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<form action="/listing/approve/<?=$this->listing->id;?><?php if (isset($_GET['redirect'])) : ?>?redirect=<?= $_GET['redirect'] ?><?php endif; ?>" method="POST">
	<input type="hidden" name="id" value="<?=$this->listing->id;?>" />
	



<div class="container_12 clearfix admin-update-listing">
	
	<div class="grid_12">
		
		<?
			echo "<table width='100%' style='margin-top: 30px;'><tr><td>";
			echo "<h2>Publish ".$this->listing->list_city->name . " - Listing ID #".$this->listing->id . "</h2></td>";
			//echo "<td style='text-align:right; font-size: 22px; font-weight: bold;' nowrap>Current Count: 0</td>";
			echo "</tr></table>";	
		?>
	

		
		<div style="margin: 20px auto; width: 460px; clear:both">
			<h4 style="margin-top: 0px; margin-bottom: 10px;">Facebook Blast Preview</h4>
			<div class="facebook_preview clearfix">
				<div class="facebook_preview_inner clearfix">
					<? if(isset($_SESSION['member'])):?>
					<img src="https://graph.facebook.com/<?=$_SESSION['member']->uid ?>/picture" class="facebook_userpic" id="facebook_userpic" />
					<? else: ?>
					<img src="/images/blank.gif" style="background-color: #f2f2f2;" class="facebook_userpic" id="facebook_userpic" />
					<? endif; ?>
					<div class="facebook_member" id="label_member"><?=$_SESSION['member']->first_name;?> <?=$_SESSION['member']->last_name;?></div>
					<div id="label_message" class="facebook_message">
						<?=$this->listing->message;?>
					</div>
					<div class="facebook_postpic_wrapper" id="facebook_postpic_wrapper">
						<div>
							<? if(isset($this->listing->image) && !empty($this->listing->image)) { ?>
							<img src='<?=$this->listing->getSizedImage(76, 76);?>' width='75' height='75' style='float: left; padding: 6px;' />
							<? } else { ?>
							<img src="/images/blank.gif" width="90" style="background-color: #f2f2f2;" class="facebook_postpic" id="facebook_postpic" />
							<?php } ?>
						</div>
					</div>
					<div class="facebook_postinfo clearfix">
						<div class="facebook_linktext" id="facebook_linktext"> <span id="label_street">
							<?=$this->listing->street;?>
						</span> <span id="label_city">
							<?=$this->listing->city;?>
						</span> <span id="label_city">
							<?=$this->listing->city;?>
						</span> : <?=COMPANY_NAME;?> </div>
						<div class="facebook_baseurl" id="facebook_baseurl"><?=COMPANY_WEBSITE;?></div>
						<div class="facebook_description" id="facebook_description">List Price: $<span id="label_price"><? if(isset($this->listing->price)) echo number_format((int)$this->listing->price);?></span>
							<span id="label_street">
								<?=$this->listing->street;?>
							</span> <span id="label_city">
								<?=$this->listing->city;?>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>

		
		<div class="box form clearfix">
			<ul class="clearfix uiForm full" style="padding-top: 20px;">

				<li class="uiFormRow clearfix" style="border-radius: 0;">
					<ul>
						<li class="fieldname cc">City</li>
						<li class="field cc" style="padding-top: 20px;">
							<span class="uiSelectWrapper">
								<span class="uiSelectInner">
									<select name="city_id" id="publish_city_id" class="uiSelect <?=($this->fields && in_array("city_id", $this->fields)?" error":"")?>" style="width: 200px;">
										<option value="" <?php echo (empty($this->publish->city_id)) ? "selected" : "";?>>All Cities</option>
										<?php foreach($this->cities as $city): ?>
											<option value="<?php echo $city->id;?>" <?php if (!empty($this->listing->city_id) && $this->listing->city_id == $city->id) echo "selected";?>>
											<?php 
												echo $city->name;
												if(isset($city->state) && !empty($city->state)) echo ", " . $city->state;
											?>
												
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</span>
						</li>
						<li class="fieldname cc">Hopper ID</li>
						<li class="field cc" style="padding-top: 20px;">
							<span class="uiSelectWrapper">
								<span class="uiSelectInner">
									<select id="hopper_id" name="hopper_id" class="uiSelect" style="width: 200px;">
										<option value="">Select A Hopper</option>					    		
										<?
											for($i = 1; $i <= $this->listing->list_city->number_of_hoppers; $i++)
											{		    
												echo '<option value="'. $i .'" ';
												if($i == 3) echo " selected ";
												echo '>Hopper #'.$i.' ';
												
												if($i == 1) echo " - National";
												if($i == 2) echo " - Regional";
												if($i == 3) echo " - Local";
												if($i == 4) echo " - Franchise";
												if($i == 5) echo " - Broker";
												
												echo '</option>';
											}
										
										?>
									</select>
								</span>
							</span>
						</li>
						<li class="fieldname cc">Minimum Inventory Threshold</li>
						<li class="field cc" style="padding-top: 20px;">
							<input type="text" name="min_inventory_threshold" id="name" value="<? if(isset($this->listing->list_city->min_inventory_threshold)) echo $this->listing->list_city->min_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("min_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
						</li>
						<li class="fieldname cc">Maximum Inventory Threshold</li>
						<li class="field cc" style="padding-top: 20px;">
							<input type="text" name="max_inventory_threshold" id="name" value="<? if(isset($this->listing->list_city->max_inventory_threshold)) echo $this->listing->list_city->max_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("max_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
						</li>
					</ul>
				</li>	  

								

				<?/*
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname cc">Hopper ID</li>
						<li class="field cc" style="padding-top: 20px;">
								<select id="hopper_id" name="hopper_id" class="uiInput" style="width: 200px;">
							<option value="">Select A Hopper</option>					    		
							<?
								for($i = 1; $i <= $this->listing->list_city->number_of_hoppers; $i++)
								{		    
									echo '<option value="'. $i .'" ';
									if($i == 3) echo " selected ";
									echo '>Hopper #'.$i.'</option>';
								}
							
							?>
							</select>
						</li>
					</ul>
				</li>	  

		
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname cc">Minimum Inventory Threshold</li>
						<li class="field cc" style="padding-top: 20px;">
							<input type="text" name="min_inventory_threshold" id="name" value="<? if(isset($this->listing->list_city->min_inventory_threshold)) echo $this->listing->list_city->min_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("min_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
						</li>
					</ul>
				</li>
		
		
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname cc">Maximum Inventory Threshold</li>
						<li class="field cc" style="padding-top: 20px;">
							<input type="text" name="max_inventory_threshold" id="name" value="<? if(isset($this->listing->list_city->max_inventory_threshold)) echo $this->listing->list_city->max_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("max_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
						</li>
					</ul>
				</li>*/ ?>
					
			</ul>    
			
			<input type="submit" name="submitted" class="uiButton large right" value="Save" style="margin-top: 20px;">
		</div>
		</form>
		
		
	</div>
	
	<div class="grid_12" style="padding-bottom: 60px;">&nbsp;</div>
</div>	
<script language="javascript">

$(document).ready(function(){


	// when ochange the publish city in the publishing assignment section
	$("#publish_city_id").change(function () {
		
		//alert('I chagneed!');
		
		$.post("/member/changepublishingcity", {"publish_city_id": $(this).val()}, function (response) {
			if ( response.error !== false ) {
				return;
			}
			if ( response.options ) {
				$("#hopper_id").html(response.options);
			}
		}, "json");
		return true;
	});
});
</script>	




</form>