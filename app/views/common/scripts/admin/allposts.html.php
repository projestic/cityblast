<?php

$sel_day	= $sel_month	= $sel_year	= null;
if ($this->selected_day)
{
	$sel_day		= substr($this->selected_day, 8);
	$sel_month	= substr($this->selected_day, 5, 2);
	$sel_year		= substr($this->selected_day, 0, 4);
}
?>
<div class="container_12 clearfix">

	<div class="grid_12" style="text-align: right; margin-top: 20px;">

		<select name="month" id="month" class="type_status" style="font-size: 18px; font-weight: bold;">
			<option value="">Select Month</option>
			<?
				$months	= array(
					'01' => 'January',
					'02' => 'February',
					'03' => 'March',
					'04' => 'April',
					'05' => 'May',
					'06' => 'June',
					'07' => 'July',
					'08' => 'August',
					'09' => 'September',
					'10' => 'October',
					'11' => 'November',
					'12' => 'December',
				);

				foreach ($months as $key => $name) 
				{
					echo '<option value="'. $key .'"'. ($key == $sel_month ? ' selected="true"' : '') .'>'. $name .'</option>';
				}
			?>
		</select>

		<select name="day" id="day" class="type_status" style="font-size: 18px; font-weight: bold; width: 75px">
			<option value="">Select Day</option>
			<?php

				$days	= range(1, 31);

				foreach ($days as $name) {

					$key	= str_pad($name, 2, '0', STR_PAD_LEFT);

					echo '<option value="'. $key .'"'. ($key == $sel_day ? ' selected="true"' : '') .'>'. $name .'</option>';
				}
			?>
		</select>

		<select name="year" id="year" class="type_status" style="font-size: 18px; font-weight: bold;">
			<option value="">Select Year</option>
			<?
				$current_year	= intval(date('Y'));

				$years	= range($current_year, $current_year - 25);

				foreach ($years as $name) {

					echo '<option value="'. $name .'"'. ($name == $sel_year ? ' selected="true"' : '') .'>'. $name .'</option>';
				}
			?>
		</select>

			
		&nbsp;<input type="button" name="go" id="select_date" value="Go" class="uiButton right" />
		
	</div>


	<div class="grid_12">

		<div style="float: right; margin: 45px 0 0 0"><a href="/admin/allposts/<?php if ($this->selected_year) : ?>-<?= $this->selected_year ?>-<?= $this->selected_month ?>-<?= $this->selected_day ?><?php endif; ?><?php if (!$this->deleted) : ?>deleted/1<?php endif; ?>">Show <?php if ($this->deleted) : ?>un<?php endif; ?>deleted</a></div>
		<h1>Publishing Log</h1>
	</div>

	
	<div class="grid_12">

		
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>Post ID</th>
				<th>Date</th>				
				<th>Member</th>
				<th>Content ID</th>
				<th>Result</th>								
				<th>Hopper ID</th>
				<th>FB</th>
				<th>TW</th>
				<th>LN</th>
				<th>Manual Publish</th>
				<th>Actions</th>
			</tr>

		<?
			$row=true;
			$fb_total=0;
			$tw_total=0;
			$ln_total=0;

			$fb_fail=0;
			$tw_fail=0;
			$ln_fail=0;

			$all_pub = array();
			$success_pub = array();

			$fb_fail_array = array();
			$tw_fail_array = array();
			$ln_fail_array = array();


			if($this->total_posts)
			{
				foreach($this->paginator as $post)
				{

					$city_id 	= isset($post->member->city_id) ? $post->member->city_id : null;

					echo "<tr ";
						if($row == true)
						{
							$row = false;
							echo "class='odd' ";
						}
						else
						{
							$row = true;
							echo "class='' ";
						}

					echo ">";


					echo "<td style='vertical-align: top !important;'>". $post->id."</td>";

					echo "<td style='vertical-align: top !important;' nowrap>". $post->created_at->format('Y-M-d H:i')."</td>";

					if(isset($post->member_id) && !empty($post->member_id))
					{
						echo "<td style='vertical-align: top !important;' nowrap>";

						if($this->view_type == "MEMBER") echo "<a href='/admin/member/".$post->member_id."'>";
						else  echo "<a href='/admin/memberposts/member/".$post->member_id."'>";


						if(isset($post->member->first_name)) echo $post->member->first_name . " ";
						if(isset($post->member->last_name)) echo $post->member->last_name;
						echo "</a></td>";
					}

					
					echo "<td style='vertical-align: top !important;'><a href='/listing/view/".$post->listing_id."' target='_new'>". $post->listing_id."</a></td>";
					echo "<td style='vertical-align: top !important;'>". $post->message."</td>";

					echo "<td style='vertical-align: top !important;'>". $post->hopper_id."</td>";



					if(!empty($post->fb_post_id))
					{
						if($post->fb_post_id == "EMPTY")
						{
							echo "<td style='color: #ff0000;text-align: right;vertical-align: top !important;'>";

							if (isset($post->member->facebook_friend_count))
							{
								$fb_fail+=$post->member->facebook_friend_count;
							}
							$fb_fail_array[] = $post->member_id;
						}
						else
						{
							echo "<td style='text-align: right;vertical-align: top !important;'>";

							if (isset($post->member->facebook_friend_count))
							{
								$fb_total	+= $post->member->facebook_friend_count;
							}

							if(isset($success_pub[$post->member_id])) $success_pub[$post->member_id] +=1;
							else $success_pub[$post->member_id] = 1;
						}
						echo (isset($post->member->facebook_friend_count) ? number_format($post->member->facebook_friend_count) : '') . "</td>";
					}
					else
					{
						echo "<td></td>";
					}

					if(stristr($post->message,"TWITTER"))
					{
						if($post->twitter_post_id)
						{
							echo "<td style='text-align: right;vertical-align: top !important;'>";

							if (isset($post->member->twitter_followers))
							{
								$tw_total+=$post->member->twitter_followers;
							}

							if(isset($success_pub[$post->member_id])) $success_pub[$post->member_id] +=1;
							else $success_pub[$post->member_id] = 1;
						}
						else
						{
							echo "<td style='color: #ff0000;text-align: right;vertical-align: top !important;'>";

							if (isset($post->member->twitter_followers))
							{
								$tw_fail+=$post->member->twitter_followers;
							}

							$tw_fail_array[] = $post->member_id;

						}
						echo (isset($post->member->twitter_followers) ? number_format($post->member->twitter_followers) : '') . "</td>";
					}
					else
					{
						echo "<td></td>";
					}

					if(stristr($post->message,"LINKEDIN"))
					{
						if(stristr($post->message,"SUCCESS"))
						{
							echo "<td style='text-align: right;vertical-align: top !important;'>";

							if (isset($post->member->linkedin_friends_count))
							{
								$ln_total+=$post->member->linkedin_friends_count;
							}

							if(isset($success_pub[$post->member_id])) $success_pub[$post->member_id] +=1;
							else $success_pub[$post->member_id] = 1;
						}
						else
						{
							echo "<td style='color: #ff0000;text-align: right;vertical-align: top !important;'>";

							if (isset($post->member->linkedin_friends_count))
							{
								$ln_fail+=$post->member->linkedin_friends_count;
							}

							$ln_fail_array[] = $post->member_id;

						}
						echo (isset($post->member->linkedin_friends_count) ? number_format($post->member->linkedin_friends_count) : '') . "</td>";

					}
					else
					{
						echo "<td></td>";
					}


					echo "<td nowrap style='vertical-align: top !important;'>";
					if(isset($post->member->facebook_publish_flag) && $post->member->facebook_publish_flag) echo "<a href='/publish/facebook/".$post->id."/".$post->member_id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Facebook?\");' style='color: #0072BC;'>facebook</a> | ";
					else echo "<span style='color: #b9b9b9;'>facebook | </span>";

					if(isset($post->member->twitter_publish_flag) && $post->member->twitter_publish_flag && $post->member->twitter_access_token) echo "<a href='/publish/twitter/".$post->id."/".$post->member_id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Twitter?\");' style='color: #0072BC;'>twitter</a> | ";
					else echo "<span style='color: #b9b9b9;'>twitter | </span>";

					if(isset($post->member->linkedin_publish_flag) && $post->member->linkedin_publish_flag && $post->member->linkedin_access_token) echo "<a href='/publish/linkedin/". $post->id."/".$post->member_id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to LinkedIn?\");' style='color: #0072BC;'>linkedin</a>";
					else echo "<span style='color: #b9b9b9;'>linkedin</span>";

					echo "</td>";
					echo "<td style=\"vertical-align: top !important;\">";
					if ($post->status == 'deleted') {
						echo "<a href=\"/admin/allposts/undelete/{$post->id}\" class=\"undelete-post\">undelete</a>";
					} else {
						echo "<a href=\"/admin/allposts/delete/{$post->id}\" class=\"delete-post\">delete</a>";
					}
					echo "</td>";

					echo "</tr>";

					if(isset($all_pub[$post->member_id])) $all_pub[$post->member_id] +=1;
					else $all_pub[$post->member_id] = 1;
					if (($post->status == 'deleted') && ($action = $post->delete_action)) {
					?><tr><td colspan="13" style="text-align: right"><span class="gray"><em>Deleted by <?= $action->administrator->member->name() ?> on <?= $action->created_at->format('F j, Y') ?> at <?= $action->created_at->format('g:ia') ?>. Reason: <?= $action->reason ?></em></span></td></tr><?php
					}
				}

				echo "<tr><td colspan='6'></td>";
				echo "<td nowrap style='text-align: right;'><span style='color: #ff0000'>".number_format($fb_fail)."</span><br/>".number_format($fb_total)."</td>";
				echo "<td nowrap style='text-align: right;'><span style='color: #ff0000'>".number_format($tw_fail)."</span><br/>".number_format($tw_total)."</td>";
				echo "<td nowrap style='text-align: right;'><span style='color: #ff0000'>".number_format($ln_fail)."</span><br/>".number_format($ln_total)."</td>";
				echo '<td></td><td></td>';
				echo "</tr>";

				echo "<tr>
						<td colspan='10' style='text-align: right;'><span style='color: #ff0000'>".number_format($fb_fail + $tw_fail + $ln_fail)."</span><br/>".number_format($fb_total + $tw_total + $ln_total)."</td>
						<td></td>
					</tr>";

			}
			else
			{
				echo "<tr><td colspan='9'>No Posts on record ". ($this->selected_day ? "for ". $this->selected_day : "") ."</td></tr>";
			}


		?>
		</table>
		<?php if ($this->need_page) : ?>
		<div style="float: right;"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div>
		<div style="clear: both; margin: 10px 0 20px;" class="clearfix"></div>
		<?php endif ?>
	</div>




	<div class="grid_12">
		<div style="height: 60px;">&nbsp;</div>
	</div>	

</div>



<script type="text/javascript">
	
	$(document).ready( function () {

		$('.delete-post').click(function(event)
		{
			$.colorbox({width:"650px", height: "380px;",  href:"/admin/deleteconfirm/type/post/target/?target=" + encodeURIComponent( $(this).attr('href') ) });
			event.preventDefault();
		});

		$('#select_date').click(function() {

			if (!$('#month').val()) {
				alert('Please select a month');
				return false;
			}

			if (!$('#day').val()) {
				alert('Please select a day');
				return false;
			}

			if (!$('#year').val()) {
				alert('Please select a year');
				return false;
			}

			location.href	= '/admin/allposts/for/'+$('#year').val()+'-'+$('#month').val()+'-'+$('#day').val();
		});
	});
	
</script>

