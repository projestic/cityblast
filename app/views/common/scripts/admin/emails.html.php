<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		<h1>
		<? if(!empty($this->filterbytype)) : ?>
		 <strong><?php echo $this->filterbytype;?></strong>
		<?php endif; ?>

		    Emails Sent
		<? if(!empty($this->filterbymember)) : ?>
		for <strong><?php echo $this->filterbymember->first_name.' '.$this->filterbymember->last_name;?></strong>
		<?php endif; ?>


		</h1>

		<table style="width: 100%;" class="uiDataTable lineheight">
		    <tr>	
		    	
		    		<th>ID</th>
		    		<th>Date</th>
				<th>To Name</th>
				<th>To Email</th>
				<th>Type</th>
				<th>Subject</th>

			</tr>

			<?php $row = true; ?>
			<? if(iterator_count($this->paginator)): ?>
				<?php foreach($this->paginator as $email) { ?>
	
					<?php $class = ($row) ? 'odd' : ''; ?>
					<?php $row   = !$row; ?>
	
					<tr class="<?php echo $class; ?>">
	
						<td style="vertical-align: top;" nowrap><?php echo $email->id; ?></td>
						<td style="vertical-align: top;" nowrap><?php echo $email->created_at->format("Y-m-d H:i"); ?></td>
						<td style="vertical-align: top;"><?php echo isset($email->member->first_name) ? $email->member->first_name.' '.$email->member->last_name : ''; ?></td>
						<td style="vertical-align: top;"><?php echo $email->email;?></td>
						<td style="vertical-align: top;"><a href='/admin/emails/bytype/<?php echo $email->type; ?>'><?php echo $email->type; ?></a></td>
						<td style="vertical-align: top;"><?php echo $email->subject; ?></td>
						<? /* <td style="vertical-align: top;"><?php echo $email->fb_uid; ?></td> */ ?>
					</tr>
				<?php } ?>
			<? endif; ?>
		</table>
	    	<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />

	</div>
</div>
