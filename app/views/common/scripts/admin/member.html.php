<div class="container_12 clearfix page_Grid_Wider">
	
		
	<div class="grid_12 member_update page_Grid_Wider">
		
		<table style="width: 100%">
			<tr>
				<td style="vertical-align: top;">
					<a href="http://www.facebook.com/profile.php?id=<?php echo $this->member->uid ?>"  target="_new">
						<img src="<?php echo empty($this->member->photo) ? 'https://graph.facebook.com/'. $this->member->uid .'/picture' : CDN_URL . $this->member->photo ?>" alt="<?php echo $this->member->first_name.' '.$this->member->last_name; ?>" style="width: 100px; height: 100px;"/>
					</a>
				</td>
				
				<td style="font-size: 22px; font-weight: bold; vertical-align: top;">
					<?php echo $this->member->first_name.' '.$this->member->last_name; ?><br/>					
					
					<div class="payment_status" style="margin-bottom: 8px;">
						
						<a href="#" style="color: #0080FF;" id="pay_status">
						<? if(strtolower($this->member->payment_status) == "cancelled" || strtolower($this->member->payment_status) == "payment_failed")  : ?><span style="color: #990000;"><? endif; ?>
						<? if(strtolower($this->member->payment_status) == "paid")  : ?><span style="color: #2ea265;"><? endif; ?>						
							<?=  str_replace("_", " ", ucfirst($this->member->payment_status));?>
							
						<? if(strtolower($this->member->payment_status) == "cancelled" || strtolower($this->member->payment_status) == "payment_failed")  : ?></span><? endif; ?>	
						</a>
					
						<ul class="payment_status_popup">
							<li>
								<a href="/admin/memberstatuschange/signup" rel="signup">Signup</a>
							</li>
							<li>
								<a href="/admin/memberstatuschange/signup_expired" rel="signup_expired">Signup Expired</a>
							</li>
							<li>
								<a href="/admin/memberstatuschange/signup_expired" rel="signup_expired">Payment Failed</a>
							</li>
							<li>
								<a href="/admin/memberstatuschange/paid" rel="paid">Paid</a>
							</li>
						</ul>
												
					</div>



<script type="text/javascript">
	function documentClickHandler(e)
	{
		if($(e.target).closest('.on').length == 0)
		{
			$('.payment_status, .actions, #top_menu').removeClass('on');
			$(document).unbind('click', documentClickHandler);
		}
	}
	
	$(document).ready(function(){
		$('.payment_status > a, .actions > a').click(function(e){
			$(document).bind('click', documentClickHandler);
			e.preventDefault();
			var $this = $(this);
			$this.parent().toggleClass('on');
			if($this.parent().hasClass('payment_status')){
				$('.actions, #top_menu').removeClass('on');
			}
			else if($this.parent().hasClass('actions')){
				$('.payment_status, #top_menu').removeClass('on');
			}
		});

		$('.payment_status_popup > li > a').click(function(e){
			e.preventDefault();
			var con = confirm("Are you sure to change payement status?");
			if(!con)
				return;
			var $this = $(this);
			var status = $this.attr("rel"); 
			$("#pay_status").html("<b>Updating...</b>");
			documentClickHandler(e);
			$.post("/member/memberstatuschange", { status: status, userid: <?php echo $this->member->id;?> },
				function(data) {
					status = status.replace('_',' ');
					firstChar = status.substring( 0, 1 ).toUpperCase();
					tail = status.substring( 1 ); 
					status = firstChar + tail; 
					
					$("#pay_status").html(status);
				});
		});
	});
</script>	
					
					<table style="padding:1px; margin:0px; border: 0px;font-size: 14px; color: #808080;">
						
					<? if(strtolower($this->member->payment_status) == "cancelled") : ?>	
						
						<?
							if(isset($this->member->cancelled_by_id) && !empty($this->member->cancelled_by_id)) :
							
								$admin = Member::find($this->member->cancelled_by_id);														
							?>
			
							<tr><td style="border: 0px; padding: 0px;">Cancelled By:</td><td style="border: 0px; padding: 0px; padding-left: 4px;"><span style="color:#0080FF"><?=$admin->first_name . " " . $admin->last_name;?></span></td></tr>
							<tr><td style="border: 0px; padding: 0px;">Cancelled On:</td><td style="border: 0px; padding: 0px; padding-left: 4px;"><span style="color:#000"><?=$this->member->cancelled_date->format('Y-M-d H:i');?></span></td></tr>
						
						<? endif; ?>
						
					<? else : ?>


						<? if(strtolower($this->member->payment_status) == "extended_trial") : ?>
						
							<tr><td style="border: 0px; padding: 0px;">Trial Ends:</td>
								<td style="border: 0px; padding: 0px;color:#000; padding-left: 4px;">
									<? if(isset($this->member->trial_extended) && !empty($this->member->trial_extended)) echo $this->member->trial_extended->format('Y-M-d');?>
								</td>
							</tr>
						
						<? endif; ?>
						
						<tr><td style="border: 0px; padding: 0px;">Since:</td><td style="border: 0px; padding: 0px;color:#000; padding-left: 4px;"><?=$this->member->created_at->format('Y-M-d');?></td></tr>
						<tr><td style="border: 0px; padding: 0px;">Days on Site:</td><td style="border: 0px; padding: 0px;color:#000; padding-left: 4px;"><?=$this->days_on_site[0]->number_of_days; ?></td></tr>
						<tr><td style="border: 0px; padding: 0px;">Next Payment:</td><td style="border: 0px; padding: 0px;color:#000; padding-left: 4px;">
							<?php
							$date = '';
							if(isset($this->member->next_payment_date) && !empty($this->member->next_payment_date) ) {
								$date = $this->member->next_payment_date->format('Y-M-d');
							}
							?>
							 <input id="next_payment" type="text" name="next_payment" 
							 	value="<?php echo $date; ?>" style="color: #0080FF; text-decoration: underline; cursor: pointer; border: 1px solid #F7F7F7; background-color: #F7F7F7; font-weight: bold; font-size: 14px; font-family: Arial,Helvetica,sans-serif;"/> <span id="next_payment_msg"></span>
							 <script>
								$(document).ready(
									function(){
										$( "#next_payment" ).datepicker({
											onSelect: function(dateText, inst) {
												$("#next_payment_msg").html("<b>Updating...</b>");
												$.post("/member/update_next_payment", { next_payment: dateText, id: <?php echo $this->member->id;?> },
													function(data) {
														$("#next_payment_msg").html("");
													});
											},
											minDate: 0,
											dateFormat: 'yy-mm-dd',
										});
									}
								);
							</script>
							</td>
						</tr>
						<tr><td style="border: 0px; padding: 0px;">Lifetime Value:</td><td style="border: 0px; padding: 0px;color:#000; padding-left: 4px;">$<?=$this->total_payments[0]->total_amount;?></td></tr>
						
					<? endif; ?>
					
					
					</table>	

				<td>
				
				<td style="vertical-align: top !important; width: 80px;">	
					

					<div class="actions" style="float: right; padding: 0px; margin: 0px;">
						<a href="#" class="uiButton">+ Actions</a>
						<ul class="actions_popup">
							<li>
								<a href="#" id="extend_trial_btn" onclick="javascript: extendTrial();">Extend Trial</a>
							</li>
							<li>
								<a href="/admin/newcreditcard/<?php echo $this->member->id;?>">New Payment Info</a>
							</li>
							<?php if ($this->member->type->promo_codes_allowed) : ?>
							<li>
								<a href="/admin/applypromo/<?php echo $this->member->id;?>">Apply Promo Code</a>
							</li>
							<?php endif; ?>
							<? if(strtolower($this->member->payment_status) != "cancelled") : ?>
							<li>
								<a href="#" id="cancel_account">Cancel Account</a>
							</li>
							<? endif; ?>
							
							<? if($_SESSION['member']->id == 35): ?>
							<li>
								<a href="/member/delete/id/<?php echo $this->member->id;?>">Delete Account</a>
							</li>
							<? endif; ?>
							
						</ul>
					</div>
					
					

					<script type="text/javascript">
					function extendTrial(){
						$.colorbox({width:"350px", height: "420px", inline:true, href:"#extend_trail"});
					}
					$(document).ready(
						function(){
							$( "#extend_trail_input" ).datepicker({
								minDate: 0,
								dateFormat: 'yy-mm-dd',
							});
						}
					);
					function saveTrailDate(ths){
						var trail_period = $("#extend_trail_input").val();
						if(trail_period==''){
							alert("Please select the date.");
							return false;
						}
						$(ths).html("<b>Please wait...</b>");
						$.post("/member/update_trail_period", { trail_period: trail_period, id: <?php echo $this->member->id;?> },
							function(data) {
								window.location.reload();
							});
					}
					</script>
					<div style="display: none;">
						<div id="extend_trail">
							<div class="box clearfix">
								<h3>Extend Trial</h3>
								
									
								<div id="extend_trail_input"></div>

								
								<div style="width: 240px;"><a href="#" class="uiButton large" style="float: right;"onClick="saveTrailDate(this);">Save</a></div>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
		
	</div>

	<div class="grid_12" style="padding-top: 10px;">
		<b>Facebook UID:</b> <?=$this->member->uid; ?><br/>
		<b>Facebook Access Token:</b><br/>
		<? 	
			if(isset($this->member->access_token) && !empty($this->member->access_token)) 
			{
				//echo $this->member->access_token; 
				
				echo "<span style='font-size: 6px;'>";
				$split = chunk_split($this->member->access_token, 140, "<br>"); 
				echo $split;
				//echo $this->member->access_token;
				echo "</span>";
				
			}
			else
			{ 
				echo "<b>MISSING</b>";
			}
			?>	
	</div>
		
	
	<div class="grid_12 page_Grid_Wider" style="margin-top: 30px; padding-bottom: 50px; margin-bottom: 25px;">
		
			<div class="uiTabset">
	
				<div class="uiTabs">
	
					<a class="uiTab tabs-1" href="#notes" rel="/note/index/id/<?=$this->member->id;?>">Notes</a>
					
					<a class="uiTab tabs-2" href="#events" rel="/eventlog/membertab/id/<?=$this->member->id;?>">Events</a>
					
					<a class="uiTab tabs-3" href="#update" rel="/admin/update_member_details/<?=$this->member->id;?>">Details</a>
	
					<a class="uiTab tabs-4" href="#posts" rel="/admin/memberposts/member/<?=$this->member->id;?>"><?=count($this->member->posts);?> Posts</a>
					
					<a class="uiTab tabs-5" href="#listings" rel="/admin/memberlistings/member/<?=$this->member->id;?>"><?=count($this->member->listings);?> Listings</a>
					
					<a class="uiTab tabs-6" href="#fanpages" rel="/member/fanpages/<?=$this->member->id;?>"><?=count($this->member->fanpages);?> FanPages</a>
					
					<a class="uiTab tabs-7" href="#payments" rel="/admin/mpayments/<?=$this->member->id;?>"><?=count($this->member->payments);?> Payments</a>
	
					<a class="uiTab tabs-8" href="#pubsettings" rel="/publishsettings/member/<?=$this->member->id;?>">Publish Settings</a>
					
					<a class="uiTab tabs-9" href="#email" rel="/admin/memberemails/<?=$this->member->id;?>">Emails</a>
					
					<a class="uiTab tabs-10" href="#cash" rel="/cashtrans/index/id/<?=$this->member->id;?>">Cash Trans</a>
					
					<a class="uiTab tabs-11" href="#stats" rel="/member/get_stats/id/<?=$this->member->id;?>">Stats</a>

					<a class="uiTab tabs-3" href="#referrals" rel="/admin/member_referrals/id/<?=$this->member->id;?>">Referrals (<?= $this->referralSatellitesCount ?>)</a>
					
					<a class="uiTab tabs-12" href="#refunds" rel="/refund/index/id/<?=$this->member->id;?>">Refunds</a>
					
				</div>
	
				<div id="tabcontent" class="uiTabContent"></div>
	
			</div>
	
	
	</div>
</div>

<div class="clearfix" style="height: 50px;"></div>



<script language="javascript">

	$(document).ready(function(){

		function loadTabContent(content, url){
		
		 	content.text('Loading...');
			content.load(url)		 
		}

		$(".uiTabset").each( function(){
			
			var tabset	= $(this);
			//console.log(tabset);
			var tabs		= $(this).find('.uiTab');
			//console.log(tabs);
			var content	= $(this).children('.uiTabContent');
			//console.log(content);
			
			// SET LOAD EVENTS ON EACH TAB
			(tabs).click( function(){
				
				var tab = $(this);
				var url = tab.attr('rel');
				tabs.removeClass('active');
				tab.addClass('active');
				loadTabContent(content, url);
				
			});
			
			var firstTab;
			if(window.location.hash){
				firstTab = tabs.filter('[href="' + window.location.hash + '"]')[0];	
			} else {
				firstTab = tabs[0];
			}
	
			// INITIALIZE FIRST TAB
			$(firstTab).addClass('active');
			loadTabContent(content, $(firstTab).attr('rel'));
			
		});




	});


	// Delete member
	$("#deletemember").bind("click",function(ev){
		ev.preventDefault();
		var message = "Are you sure you want to delete this user ? This operation is irreversible.";
		<?php if( count($this->member->listings) > 0 ) : ?>
		message	= "This member has attached listings, do you want to delete the member and all his listings from the system ? This operation is irreversible."
		<?endif;?>

		$.colorbox({
			href: false,
			innerWidth:420,
			initialWidth:450,
			initialHeight:200,
			html:"<h3>Confirmation</h3><p style='margin-bottom: 30px;'><strong>" + message + "</strong></p>"
				+"<p><input type='button' class='uiButton cancel' value='Cancel' onclick='$.colorbox.close();' /> "
				+"<input type='button' class='uiButton' id='delete-member-yes' value='Delete' style='margin-left: 30px;' onclick='$.colorbox.close();' /></p>"
		});
	});

	$("#delete-member-yes").live("click", function () {
		window.location = $("#deletemember").attr("href");
	});
	// end

	var firstErrOb	= 20;

	// Cancel the account
	$("#cancel_account").bind("click", function(e){
		e.preventDefault();
		var message = "Are you sure you want to cancel this account? This operation is irreversible.";
		$.colorbox({
			href: false,
			innerWidth:420,
			initialWidth:450,
			initialHeight:200,
			html:"<h3>Confirmation</h3><p style='margin-bottom: 30px;'><strong>" + message + "</strong></p>"
				+"<p><input type='button' class='uiButton cancel' value='No' onclick='$.colorbox.close();' /> "
				+"<input type='button' class='uiButton' id='cancel-account-yes' value='Yes' style='margin-left: 30px;' onclick='$.colorbox.close();' /></p>"
		});
	});

	$("#cancel-account-yes").live("click", function () {
		window.location = "/admin/cancelaccount/<?= $this->member->id; ?>";
	});
	// end



	function validateForm()
	{

		var errors  =	0;


		$("#extraForm .validateNotempty").each( function()
		{
			$(this).removeClass('error');

			if($(this).val() == '') {
				errors = errors + 1;
				$(this).addClass('error');

				updateErrIndex($(this));
			}
		});
		
		$("#extraForm .validatePositiveNumber").each( function()
		{
			$(this).removeClass('error');

			if($(this).val() <= 0) {
				errors = errors + 1;
				$(this).addClass('error');

				updateErrIndex($(this));
			}
		});

		$("#extraForm .validateEmail").each( function()
		{
			$(this).removeClass('error');
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

			if(!filter.test($(this).val())) {

				errors = errors + 1;
				$(this).addClass('error');

				updateErrIndex($(this));
			}
		});


		if(errors == 0)
		{
			$("#extraForm").submit();
		}
		else
		{
			$("#update").attr('disabled', false);

			if (firstErrOb) {
				if (firstErrOb > 1) {
					$('[tabindex='+(firstErrOb - 1)+']').focus().blur();
					if (firstErrOb != 9) {

						$('[tabindex='+firstErrOb+']').focus();
					}
				} else {
					$('[tabindex=1]').focus();
				}
			}
		}

	}


	function updateErrIndex(ob)
	{
		if (firstErrOb > parseInt(ob.attr('tabindex'))) {
			firstErrOb	= parseInt(ob.attr('tabindex'));
		}
	}


</script>

<style type="text/css">
	.container_12 .page_Grid_Wider {
		width: 1060px;
	}
	.grid_12 .page_Grid_Wider {
		width: 1060px;
	}
	.page_Grid_Wider {
		width: 1060px;
	}
</style>