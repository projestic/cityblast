<div class="container_12 clearfix">

	<?php if(!empty($this->message)): ?>
		<div class="grid_12">
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
		</div>		
	<?php endif; ?>



    	<div class="grid_12" style="padding-top: 30px;">
    		<h1>Group List <span style="display:inline; float: right"><a href="/admin/groupadd/" class="uiButton"  style="margin-top: 0px; padding-top:0px; margin-right: 0px;">Add Group</a> <a href="/admin/group_city_download" class="uiButton" style="margin-top: 0px; padding-top:0px; margin-right: 0px;">Download</a></span></h1>
    	</div>


	<div class="grid_12" style="padding-bottom: 80px;">

		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>ID</th>
				<th>Group Name</th>
				
				<th>Number of Cities</th>

                	<th style="text-align: center">Action</th>
			</tr>

		<?
			$row=true;
			foreach($this->paginator as $group)
			{
				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='' ";
					}
					else
					{
						$row = true;
						echo "class='even' ";
					}

				echo ">";

					echo "<td style='vertical-align: top;'><a href='/admin/groupadd/". $group->id."'>". $group->id."</a></td>";
					echo "<td style='vertical-align: top;'><a href='/admin/groupadd/". $group->id."'>". $group->name . "</a></td>";

	
	
					echo "<td style='vertical-align: top;'><a href='/admin/groupadd/". $group->id."'>". count($group->cities) . "</a></td>";
	


	                	echo '<td style="vertical-align: top; text-align: center">';
	                	echo '<a href="/admin/groupadd/' . $group->id.'" >edit</a> | ';
	                	echo '<a href="/admin/groupdelete/id/' . $group->id.'" onclick="return confirm(\'Are you sure?\');">delete</a></td>';

	                	
				echo "</tr>";

			}

		?>
		</table>
		<div class="grid_12" style="margin-top: 0px;">
			<div style="margin-top: 0px; text-align: right; float: right;">
				<?php 
				if(count($this->paginator) > 0) 
				{
					$paginationParams = array('extraParams'=>array());
			
					echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $paginationParams); 
				}
				?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/select/";
		var data    =	{"country":selected};
		$.post(url, data, function(response){
			//$('#cityselection').html(response);
			window.location.reload();
		});
	});
});
</script>