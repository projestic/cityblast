<?php
/**
 * Date UI component.
 * @author konstv
 * @date 2013dec10
 */

// expected vars
/** @var string $id */
$id = $this->id;

/** @var string $label */
$label = $this->label;

/** @var $date_chosen_str */
$date_chosen_str = $this->date_chosen_str;

/** @var $month_chosen */
/** @var $day_chosen */
/** @var $year_chosen */

if (isset($date_chosen_str) && $date_chosen_str) {
	$arrExploded = explode("-", $date_chosen_str);
	$day_chosen = $arrExploded[2];
	$month_chosen = $arrExploded[1];
	$year_chosen = $arrExploded[0];
}
$date_chosen_str = $year_chosen . '-' . $month_chosen . '-' . $day_chosen ;
?>
<b><?= $label ?> :</b>
<select id="<?= $id ?>_month" class="type_status" style="font-size: 18px; font-weight: bold;">
	<option value="">Sel Month</option>
	<?php
	$months	= array(
		'01' => 'Jan',
		'02' => 'Feb',
		'03' => 'Mar',
		'04' => 'Apr',
		'05' => 'May',
		'06' => 'Jun',
		'07' => 'Jul',
		'08' => 'Aug',
		'09' => 'Sep',
		'10' => 'Oct',
		'11' => 'Nov',
		'12' => 'Dec',
	);

	foreach ($months as $key => $name) {

		echo '<option value="'. $key .'"'. ($key == $month_chosen ? ' selected="true"' : '') .'>'. $name .'</option>';
	}
	?>
</select>

<select id="<?= $id ?>_day" class="type_status" style="font-size: 18px; font-weight: bold; width: 75px">
	<option value="">Day</option>
	<?php

	$days	= range(1, 31);

	foreach ($days as $name) {

		$key	= str_pad($name, 2, '0', STR_PAD_LEFT);

		echo '<option value="'. $key .'"'. ($key == $day_chosen ? ' selected="true"' : '') .'>'. $name .'</option>';
	}
	?>
</select>

<select id="<?= $id ?>_year" class="type_status" style="font-size: 18px; font-weight: bold;">
	<option value="">Sel Year</option>
	<?
	$current_year	= intval(date('Y'));

	$years	= range($current_year, $current_year - 25);

	foreach ($years as $name) {

		echo '<option value="'. $name .'"'. ($name == $year_chosen ? ' selected="true"' : '') .'>'. $name .'</option>';
	}
	?>
</select>
<input type="hidden" id="<?= $id ?>_resultValue" value="<?= $date_chosen_str ?>">
<input type="hidden" id="<?= $id ?>" name="<?= $id ?>" value="<?= $date_chosen_str ?>">

<script type="text/javascript">
	if (typeof Ui_Component_Date === 'undefined') {
		Ui_Component_Date = function() {
		};
		Ui_Component_Date.bindEvents = function(elNameGlobal) {
			var thisRef = this;
			var monthName = this.fullElName(elNameGlobal, 'month');
			var dayName = this.fullElName(elNameGlobal, 'day');
			var yearName = this.fullElName(elNameGlobal, 'year');
			$('#' + monthName).on("change", function(event) {
				thisRef.buildResultValue(elNameGlobal);
			});
			$('#' + dayName).on("change", function(event) {
				thisRef.buildResultValue(elNameGlobal);
			});
			$('#' + yearName).on("change", function(event) {
				thisRef.buildResultValue(elNameGlobal);
			});
		};

		Ui_Component_Date.fullElName = function(elNameGlobal, elName) {
			return elNameGlobal + '_' + elName;
		};

		Ui_Component_Date.buildResultValue = function(elNameGlobal) {
			var resultValuePureName = elNameGlobal;
			var resultValueName = this.fullElName(elNameGlobal, 'resultValue');
			var monthName = this.fullElName(elNameGlobal, 'month');
			var dayName = this.fullElName(elNameGlobal, 'day');
			var yearName = this.fullElName(elNameGlobal, 'year');
			var val = $('#' + yearName).val() + '-' + $('#' + monthName).val() + '-' + $('#' + dayName).val();
			$('#' + resultValuePureName).val(val);
			$('#' + resultValueName).val(val); // just a duplicate
		};
	}

	$(document).ready(function()
	{
		Ui_Component_Date.bindEvents("<?= $id ?>");
	});
</script>