<?php
/** @var int $weekChosen */
// format: "Y.m-W"
$weekChosenFull = $this->weekChosen;

/** @var int $urlBase */
$urlBase = $this->urlBase;

$weekAndYearExploded = explode('.', $weekChosenFull);
$yearChosenNumber = $weekAndYearExploded[0];
$monthAndWeek = $weekAndYearExploded[1];
$monthAndWeekExploded = explode('-', $monthAndWeek);
$monthChosenNumber = $monthAndWeekExploded[0];
$weekChosenNumber = $monthAndWeekExploded[1];

/** @var int $urlBase */
$urlBase = $this->urlBase;
?>
<select onchange="window.location.replace('<?php echo $urlBase . '/'; ?>' + this.value)" style="font-size: 22px; font-weight: bold;">
	<option value='0'>
		Select
	</option>
	<?
		$dt = new DateTime();
		$dt->setTime(0, 0, 0);
		$i = 0;
		$monthPrev = null;
		$optionChosenMarker = 'selected="selected"';
		$yearCurr = date('Y');
		while($i < 52) {
			//$dt->modify('-1 year');
			$dt->modify('-1 week');
			$monthCurr = $dt->format('M');
			$weekNumber = $dt->format('W');
			$yearNumber = $dt->format('Y');
			$monthNumber = $dt->format('m');
			$yearAndWeek = $yearNumber . '.' . $weekNumber;
			$yearAndMonthAndWeek = $yearNumber . '.' . $monthNumber . '-' . $weekNumber;
			$optionTitle = null;
			if ($yearCurr == $yearNumber) {
				$optionTitle = $weekNumber;
			} else {
				$optionTitle = $yearAndWeek;
			}
			if ($monthCurr != $monthPrev) {
				$optionTitle .= ' (' . $monthCurr . ')';
			}
			$isOptionChosenStr = null;
			if ($weekNumber == $weekChosenNumber
				&&
				$yearNumber == $yearChosenNumber
			) {
				$isOptionChosenStr = $optionChosenMarker;
			}
			$monthPrev = $dt->format('M');
			$i++;
			?>
			<option value='<?= $yearAndMonthAndWeek ?>' <?= $isOptionChosenStr ?>>
				<?= $optionTitle ?>
			</option>
			<?
		}
	?>
</select>