<?php
/** @var int $monthChosen */
// format: "Y.m"
$monthChosenFull = $this->monthChosen;

/** @var int $urlBase */
$urlBase = $this->urlBase;

$yearAndMonth2Exploded = explode('.', $monthChosenFull);
$yearChosenNumber = $yearAndMonth2Exploded[0];
$monthChosenNumber = $yearAndMonth2Exploded[1];

/** @var int $urlBase */
$urlBase = $this->urlBase;
?>
<select onchange="window.location.replace('<?php echo $urlBase . '/'; ?>' + this.value)" style="font-size: 22px; font-weight: bold;">
	<option value='0'>
		Select
	</option>
	<?
		$dt = new DateTime();
		$dt->setTime(0, 0, 0);
		$i = 0;
		$optionChosenMarker = 'selected="selected"';
		$yearCurr = date('Y');
		while($i < 12) {
			//$dt->modify('-1 year');
			$dt->modify('-1 month');
			$monthCurrName = $dt->format('M');
			$yearNumber = $dt->format('Y');
			$monthNumber = $dt->format('m');
			$yearAndMonth = $yearNumber . '.' . $monthNumber;
			$optionTitle = null;
			if ($yearCurr == $yearNumber) {
				$optionTitle = $monthNumber;
			} else {
				$optionTitle = $yearAndMonth;
			}
			$isOptionChosenStr = null;
			if ($monthNumber == $monthChosenNumber
				&&
				$yearNumber == $yearChosenNumber
			) {
				$isOptionChosenStr = $optionChosenMarker;
			}
			$i++;
			?>
			<option value='<?= $yearAndMonth ?>' <?= $isOptionChosenStr ?>>
				<?= $optionTitle ?>
			</option>
			<?
		}
	?>
</select>