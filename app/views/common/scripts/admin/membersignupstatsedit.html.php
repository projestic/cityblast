<div class="clearfix" style="height: 50px;"></div>
<div class="container_12 clearfix">
	<h1>Member Stats <?=$this->date?></h1>
	<div class="grid_12 member_update">
		<?php if(!empty($this->message)): ?>
			<div class="box white80 clearfix" style="text-align: center;">
				<h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
		<?php endif; ?>
	</div>

	<div class="grid_12 member_update">
		<div class="box gray clearfix">
			<form id="extraForm" action="" method="POST" style="margin: 0 -20px;">
				<ul>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Ad Spend</li>
							<li class="field">
								<input type="text" name="ad_spend" value="<?=!empty($this->stats)?$this->stats->ad_spend:0?>"/>
							</li>
						</ul>
					</li>
				</ul>
			

		</div>
		<input type="submit" value="Save" id="save" class="uiButton large right" />
		</form>
	</div>
</div>
<div class="clearfix" style="height: 50px;"></div>