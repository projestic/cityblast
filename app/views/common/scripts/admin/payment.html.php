<?php $payment	= $this->payment; ?>
<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 60px;">
		<div class="box form clearfix">
			<div class="box_section">
				<h1>Invoice #<?=$payment->id;?></h1>
			</div>
			<div class="clearfix" style="padding: 0 0 20px">
				<ul class="uiForm full clearfix">
					<li class="uiFormRow clearfix" style="width: auto; background: none; border-top-color: #e5e5e5; border-radius: 0px;">
						<ul>
							<li class="fieldname">Transaction ID:</li>
							<li class="field">
								<?php 
						
									if($payment->payment_gateway && $payment->payment_gateway->code == "PAYPAL") echo $payment->paypal_confirmation_number;
									else echo $payment->stripe_payment_id;
								
								?>
							</li>
							<li class="fieldname">Transaction Date:</li>
							<li class="field"><?php echo date('Y-M-d H:i:s', $payment->created_at->getTimestamp())  ?></li>
							<li class="fieldname">Name:</li>
							<li class="field"><a href="/admin/member/<?=$payment->member_id;?>"><?php echo $payment->first_name .' '. $payment->last_name ?></a></li>
							<li class="fieldname">Address1:</li>
							<li class="field"><?php echo $payment->address1 ? $payment->address1 : "&nbsp;" ?></li>
							<li class="fieldname">Address2:</li>
							<li class="field"><?php echo $payment->address2 ? $payment->address2 : "&nbsp;" ?></li>
							<li class="fieldname">City:</li>
							<li class="field"><?php echo ucfirst($payment->city) ?></li>
							<li class="fieldname">State:</li>
							<li class="field"><?php echo ucfirst($payment->state) ?></li>
							<li class="fieldname">Country:</li>
							<li class="field"><?php echo ucfirst($payment->country) ?></li>
							<li class="fieldname">Zip:</li>
							<li class="field"><?php echo strtoupper($payment->zip) ?></li>
							<li class="fieldname">Taxes:</li>
							<li class="field"><?php echo money_format('%!i', $payment->taxes) ?></li>
							<li class="fieldname">Amount:</li>
							<li class="field">$ <?php echo money_format('%!i', $payment->amount) ?></li>
							<li class="fieldname">Cash Balance Debit:</li>
							<li class="field">
								<? if ($payment->cash_trans_debit): ?>
									$<?=money_format('%!i', $payment->cash_trans_debit->amount)?>
								<? else: ?>
									$<?=money_format('%!i', 0)?>
								<? endif; ?>
							</li>
							<li class="fieldname">Expiration Date:</li>
							<li class="field"><?php if(isset($payment->paypal_transaction_expire_date) && !empty($payment->paypal_transaction_expire_date)) echo date('Y-M-d H:i:s', $payment->paypal_transaction_expire_date->getTimestamp()); ?></li>
							
						</ul>
					</li>
				</ul>
			</div>
			<? /*
			<table class="payment_details" width="100%;">
				<tr>
					<td>Transaction ID:</td>
					<td>
						<?php 
						
							if($payment->payment_gateway && $payment->payment_gateway->code == "PAYPAL") echo $payment->paypal_confirmation_number;
							else echo $payment->stripe_payment_id;
						
						?></td>
				</tr>
				<tr class="even">
					<td>Transaction Date:</td>
					<td><?php echo date('Y-M-d H:i:s', $payment->created_at->getTimestamp())  ?></td>
				</tr>
				<tr>
					<td>Name:</td>
					<td><a href="/admin/member/<?=$payment->member_id;?>"><?php echo $payment->first_name .' '. $payment->last_name ?></a></td>
				</tr>
				<tr class="even">
					<td>Address1:</td>
					<td><?php echo $payment->address1 ?></td>
				</tr>
				<tr>
					<td>Address2:</td>
					<td><?php echo $payment->address2 ?></td>
				</tr>
				<tr class="even">
					<td>City:</td>
					<td><?php echo ucfirst($payment->city) ?></td>
				</tr>
				<tr>
					<td>State:</td>
					<td><?php echo ucfirst($payment->state) ?></td>
				</tr>
				<tr class="even">
					<td>Country:</td>
					<td><?php echo ucfirst($payment->country) ?></td>
				</tr>
				<tr>
					<td>Zip:</td>
					<td><?php echo strtoupper($payment->zip) ?></td>
				</tr>

				<tr class="even">
					<td>Taxes:</td>
					<td><?php echo money_format('%!i', $payment->taxes) ?></td>
				</tr>
				
				<tr>
					<td>Amount:</td>
					<td>$ <?php echo money_format('%!i', $payment->amount) ?> </td>
				</tr>
				<tr class="even">
					<td>Expiration Date:</td>
					<td><?php if(isset($payment->paypal_transaction_expire_date) && !empty($payment->paypal_transaction_expire_date)) echo date('Y-M-d H:i:s', $payment->paypal_transaction_expire_date->getTimestamp()); ?></td>
				</tr>

				
			</table>
			*/ ?>
			<a href="/admin/payments" class="uiButton" style="float: right;">Back to Payments</a>
		</div>
	</div>
</div>

<style type="text/css">

.payment_details tr td:first-child {
	font-weight: bold;
	width: 130px;
	font-size: 11pt;
}
</style>