<?
$sections = array();

$sections[] = array(
	'name' => 'Paid',
	'totals' => array('paid_new', 'paid_cancel'),
	'fields' => array(
		'paid_total_prev' => 'Total 12:00am',
		'paid_new' => 'New',
		'paid_cancel' => 'Cancel',
		'paid_deleted' => 'Deleted',
		'paid_total' => 'Total 11:59pm',
		'paid_alltime' => 'All time',
	)
);

$sections[] = array(
	'name' => 'Card Trials',
	'totals' => array('cc_trial_new', 'cc_trial_cancel', 'cc_trial_converted'),
	'fields' => array(
		'cc_trial_total_prev' => 'Total 12:00am',
		'cc_trial_new' => 'New',
		'cc_trial_cancel' => 'Cancel',
		'cc_trial_deleted' => 'Deleted',
		'cc_trial_converted' => 'Converted',
		'cc_trial_total' => 'Total 11:59pm',
		'cc_trial_alltime' => 'All time',
	)
);

$sections[] = array(
	'name' => 'No Card Trials',
	'totals' => array('nocc_trial_new', 'nocc_trial_cancel', 'nocc_trial_failed_convert', 'nocc_trial_converted'),
	'fields' => array(
		'nocc_trial_total_prev' => 'Total 12:00am',
		'nocc_trial_new' => 'New',
		'nocc_trial_cancel' => 'Cancel',
		'nocc_trial_failed_convert' => 'Failed Convert',
		'nocc_trial_deleted' => 'Deleted',
		'nocc_trial_converted' => 'Converted',
		'nocc_trial_total' => 'Total 11:59pm',
		'nocc_trial_failed_convert_total' => 'Failed Convert Total',
	)
);

$sections[] = array(
	'name' => 'Free',
	'totals' => array('free_new'),
	'fields' => array(
		'free_new' => 'New',
		'free' => 'Total'
	)
);

$sections[] = array(
	'name' => 'Signup',
	'totals' => array('signup_new'),
	'fields' => array(
		'signup_new' => 'New',
		'signup_alltime' => 'All time'
	)
);

$sections[] = array(
	'name' => 'Cancelled',
	'totals' => array('cancelled_new'),
	'fields' => array(
		'cancelled_new' => 'New',
		'cancelled_total' => 'Total'
	)
);

$sections[] = array(
	'name' => 'Deleted',
	'totals' => array('deleted_new'),
	'fields' => array(
		'deleted_new' => 'New',
		'deleted_total' => 'Total'
	)
);

$sections[] = array(
	'name' => 'Totals',
	'totals' => array(),
	'fields' => array(
		'active_cc' => 'Active CC',
		'total' => 'Total Active',
		'avrg_lifetime_value' => 'Avrg Lifetime Value',
	)
);

$numberFormatPrecisions = array(
	'avrg_lifetime_value' => 2
);
?>


<div class="container_12">
	<div class="grid_12" style="text-align: right;">

		<div id="monthselection" style="float: right;">
		<select name="month" style="margin-top: 25px;width: 200px; font-size: 30px; font-weight: bold; width: 300px;">
			<?php for($i = 0; $i <= 12; $i++): ?>
			<?php 
				$month_timestamp = strtotime('-' . $i . ' months');  
				$month = date('n', $month_timestamp); 
				$year = date('Y', $month_timestamp);
				$name = date('F Y', $month_timestamp);
				$url = $this->url(array('y' => $year, 'm' => $month));
				?>
			<option value="<?=$url?>" <?=($this->year == $year && $this->month == $month) ? 'selected="selected"' : ''?>><?=$name?></option>
			<?php endfor; ?>
		</select>
		</div>
	</div>
</div>
<div class="clearfix"></div>
				
<div class="container_12 clearfix">
	<h1><?=$this->month_name . ' ' . $this->year?></h1>
	<div class="grid_12" style="padding-top: 15px; padding-bottom: 30px;">

		<table class="uiDataTable" style="width:100%;">
			<tr>
				<th></th>
				<?php for ($i = 1; $i <= $this->days_in_month; $i++): ?>
				<th><?=$i?></th>
				<?php endfor; ?>
				<th>Totals</th>
			<tr>
<?
foreach ($sections as $section) {
	$section_data = memberStatsGenSectionData(
		$section['name'], 
		$section['fields'], 
		$section['totals'], 
		$this->stats_by_day_of_month, 
		$this->days_in_month
	);
?>
	<tr>
		<th colspan="<?=$this->days_in_month+2?>"><?=$section_data['section_name']?></th>
	<tr>
	<?php $row = 1; ?>
	<?php foreach ($section_data['fields'] as $field_key => $field_name): ?>
	<?php $numberFormatPrecision = !empty($numberFormatPrecisions[$field_key]) ? $numberFormatPrecisions[$field_key] : 0; ?>
	<tr class="<?=($row % 2 ==0) ? 'even' : 'odd' ?>">
		<td><?=$field_name?></td>
		<?php foreach ($section_data['values'][$field_key] as $value): ?>
		<td><?=($value!==null) ? number_format($value, $numberFormatPrecision) : '-' ?></td>
		<?php endforeach; ?>
		<td>
			<?=(isset($section_data['totals'][$field_key])) ? number_format($section_data['totals'][$field_key]) : ''?>
		</td>
	<tr>
		<?php $row++; ?>
	<?php endforeach; ?>
	<?
	}
	?>
		</table>

	</div>
</div>

		
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="month"]').change(function(){
			$('select[name="month"] option:selected').each(function(){
				window.location.href = $(this).val();
			});
		});
		$('select[name="month"]').blur(function(){
			$(this).change();
		});
	});
</script>

<?php

function memberStatsGenSectionData($name, $fields, $totals, $stats, $days_in_month)
{
	$result = array();
	$result['section_name'] = $name;
	$result['fields'] = $fields;
	$result['values'] = array();
	$result['totals'] = array();
	$result['days_in_month'] = $days_in_month;
	if (!empty($fields)) {
		foreach ($fields as $field_key => $field_name) {
			for ($i = 1; $i <= $days_in_month; $i++) {
				if (!isset($result['values'])) {
					$result['values'] = array();
				}
				if (!isset($result['values'][$field_key])) {
					$result['values'][$field_key] = array();
				}
				$value = isset($stats['day' . $i]) 
						? $stats['day' . $i]->{$field_key}
						: null;
				$result['values'][$field_key][$i] = $value;
				
				if (in_array($field_key, $totals)) {
					if (!isset($result['totals'][$field_key])) {
						$result['totals'][$field_key] = 0;
					}
					$result['totals'][$field_key] += $value;
				}
			}
		}
	}
	
	return $result;
}
