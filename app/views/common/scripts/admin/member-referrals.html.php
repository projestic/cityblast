<?php
/** @var Zend_Paginator $satellitesByMemberPaginator */
$satellitesByMemberPaginator = $this->satellitesByMemberPaginator;

/** @var Zend_Paginator $satellitesByAffiliatePaginator */
$satellitesByAffiliatePaginator = $this->satellitesByAffiliatePaginator;

/** @var array $referrerTotals */
$referrerTotals = $this->referrerTotals;
?>
<div class="container_12 clearfix">
<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">


	<!-- Satellites -->
	<div>
		<h6>Referrals:	<?= $satellitesByMemberPaginator->getTotalItemCount(); ?></h6>
		<table style="width: 100%;" class="uiDataTable lineheight">
		  <tr>
				<th>ID</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Total referral credits</th>
			</tr>

			<?php $row = true; ?>
			<? if(iterator_count($satellitesByMemberPaginator)): ?>
				<?php foreach($satellitesByMemberPaginator as $satellite) { ?>
	
					<?php $class = ($row) ? 'odd' : ''; ?>
					<?php $row   = !$row; ?>
	
					<tr class="<?php echo $class; ?>">
						<td style="vertical-align: top;" nowrap><?= $satellite->id; ?></td>
						<td style="vertical-align: top;" nowrap><?= $satellite->first_name ?></td>
						<td style="vertical-align: top;"><?= $satellite->last_name ?></td>
						<td style="vertical-align: top;"><?= $satellite->total_referral_credits ?></td>
					</tr>
				<?php } ?>
			<? endif; ?>
		</table>
	  <div style="float: right;" class="clearfix"><?= $this->paginationControl($satellitesByMemberPaginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />
		<div style="clear:both"></div>
	</div>

	<!-- Satellites -->
	<?php if (isset($satellitesByAffiliatePaginator)) { ?>
		<div>
			<h6>Referrals by affiliate:	<?= isset($satellitesByAffiliatePaginator) ? $satellitesByAffiliatePaginator->getTotalItemCount() : 0 ?></h6>
			<table style="width: 100%;" class="uiDataTable lineheight">
				<tr>
					<th>ID</th>
					<th>First name</th>
					<th>Last name</th>
					<th>Total referral credits</th>
				</tr>

				<?php $row = true; ?>
					<?php foreach($satellitesByAffiliatePaginator as $satellite) { ?>

						<?php $class = ($row) ? 'odd' : ''; ?>
						<?php $row   = !$row; ?>

						<tr class="<?php echo $class; ?>">
							<td style="vertical-align: top;" nowrap><?= $satellite->id; ?></td>
							<td style="vertical-align: top;" nowrap><?= $satellite->first_name ?></td>
							<td style="vertical-align: top;"><?= $satellite->last_name ?></td>
							<td style="vertical-align: top;"><?= $satellite->total_referral_credits ?></td>
						</tr>
					<?php } ?>
			</table>
			<div style="float: right;" class="clearfix"><?= $this->paginationControl($satellitesByAffiliatePaginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />
			<div style="clear:both"></div>
		<div>
	<?php } ?>

	<div style="clear:both"></div>
		<!-- Totals -->
	<div>
		<h6>Referrer Totals:</h6>
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>Referral blast count</th>
				<th>Referral email count</th>
				<th>Total referrals</th>
				<th>Total referral credits</th>
				<th>Total referral payment amount</th>
			</tr>

			<?php if ($referrerTotals) { ?>
				<tr>
					<td style="vertical-align: top;" nowrap><?= $referrerTotals->referral_blast_count; ?></td>
					<td style="vertical-align: top;" nowrap><?= $referrerTotals->referral_email_count ?></td>
					<td style="vertical-align: top;"><?= $referrerTotals->total_referrals ?></td>
					<td style="vertical-align: top;"><?= $referrerTotals->total_referral_credits ?></td>
					<td style="vertical-align: top;"><?= $referrerTotals->total_referral_payment_amount ?></td>
				</tr>
			<?php } else { ?>
				<tr><td colspan="5">no data</td></tr>
			<?php } ?>
		</table>
	</div>


</div>
</div>