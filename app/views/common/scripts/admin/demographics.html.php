<div class="container_12 clearfix">

	<div class="grid_12" style="margin-bottom: 80px;">		
		
		<h1>User Demographics</h1>

			<br/><br/>
			<table width="100%;">
				<tr>
					<td>Average age</td>
					<td style="text-align: right; font-size: 16; font-weight: bold;"><?=$this->avg_age;?></td>
				</tr>
				<tr>
					<td>Gender</td>
					<td style="text-align: right; font-size: 16; font-weight: bold;"><?= $this->gender_m ?>% male, <?= $this->gender_f ?>% female</td>
				</tr>
			</table>
			


			
			
			<br/><br/>
	
		<h1>Age Bands</h1>
			<table style="width: 100%;" class="uiDataTable lineheight">			
			<tr>
			<th>Age Band</th>
			<th style="text-align: right;">Member Count</th>
			<th style="text-align: right;">Percent</th>
			</tr>							
			
				<?php foreach ($this->age_bands as $age_band) : ?>
					
					<tr>
						<td><?= $age_band->ageband; ?></td>
						<td style="text-align: right;"><?= $age_band->total; ?></td>
						
						<td style="text-align: right;"><?= number_format(	(($age_band->total / $this->member_count) * 100 ),2); ?>%</td>
					</tr>
				
				<? endforeach; ?>
			</table>
			
			
		<h1>Users by Region</h1>			
			<br/><br/>
			<table style="width: 100%;" class="uiDataTable lineheight">			
			<tr>
			<th>Region</th>
			<th>Member Count</th>
			</tr>							
			
				<?php foreach ($this->regions as $region) : ?>
					
					<tr>
						<td><?= $region->current_state ? $region->current_state : '(none)' ?></td>
						<td><?= $region->members ?></td>
					</tr>
				
				<? endforeach; ?>
			</table>
				
	</div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/index/";
		var data    =	{"hasmember":"1","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});
	
	$( "#startDate" ).datepicker({
		defaultDate: "-30d",
		dateFormat: 'yy-mm-dd',
		changeMonth: true
	});
	
	$( "#endDate" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true
	});
});
</script>