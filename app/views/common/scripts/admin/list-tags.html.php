<div class="container_12 clearfix">

	<div class="grid_12" style="padding-top: 30px;"><h1>Tags <a href="/admin/add_tag" class="uiButton"  style="float: right; margin-top: 0px; padding-top:0px; margin-right: 0px;">Add Tag</a></h1></div>


	<div class="grid_12" style="margin-bottom: 80px;">


		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<th>Tag</th>
				<th>Group</th>
				<th>Content count</th>
				<th style="text-align: center;">Actions</th>
			</tr>



			<?

			$row=true;
			foreach($this->paginator as $tag)
			{
				$tag_group = ($tag->tag_group) ? $this->escape($tag->tag_group->name) : '-';

				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}

				echo ">";


				echo "<td style='vertical-align: top;'><a href='/admin/update_tag/id/" . $this->escape($tag->id) ."' style='text-decoration: none;'>" . $this->escape($tag->id) . "</a></td>";
				echo "<td style='vertical-align: top;'> " . $this->escape($tag->name) . "</td>";
				echo "<td style='vertical-align: top;'>" . $this->escape($tag_group) . "</td>";

				echo "<td>". $this->escape($tag->content_count) . "</td>";


				echo "<td><a href='/admin/update_tag/id/" . $this->escape($tag->id) . "'>edit</a> | ";
				echo "<a href='/admin/delete_tag/id/" . $this->escape($tag->id) . "' onclick='return window.confirm(\"Delete this tag?\");'>delete</a>";

				echo "</td>";



				echo "</tr>";

			}

		?>




		</table>

		<div class="grid_10" style="margin-top: 0px;">
			<div style="margin-top: 0px; text-align: right; float: right;">
				<?php
				if(count($this->paginator) > 0)
				{
					echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml');
				}
				?>
			</div>
		</div>



	</div>



</div>