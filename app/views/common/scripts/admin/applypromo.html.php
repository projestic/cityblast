<div class="container_12 clearfix">
				
	<div class="grid_9" style="margin-bottom: 40px;">

		<h3>Apply promo code for <?= $this->member->name() ?></h3>
	
		<div class="box clearfix">
	
			<form action="" method="POST">
				<select name="promo_id">
					<option value="">Select a promo code</option>
				<?php foreach ($this->promos as $promo) : ?>
					<option value="<?= $promo->id ?>"><?= ($promo->type == 'PERCENT' ? '' : '$') ?><?= $promo->amount ?><?= ($promo->type == 'PERCENT' ? '%' : '') ?> off - <?= $promo->promo_code ?></option>
				<?php endforeach; ?>
				</select>
			
				<?php if ($this->member->referring_affiliate) : ?>
					<p><em>Only promo codes associated with the <?= $this->member->referring_affiliate->owner->name() ?> affiliate account may be used for this member.</em></p>
				<?php endif; ?>
			
				<p><input type="submit" name="submitted" class="uiButton" value="Apply"></p>
			</form>
			
		</div>
	
	</div>
	
	<div class="grid_3"></div>

</div>