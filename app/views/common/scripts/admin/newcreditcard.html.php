<?
	$_REQUEST['fname'] 		= $this->member->first_name;
	$_REQUEST['lname'] 		= $this->member->last_name;
	
	$_REQUEST['address'] 	= $this->member->address;
	$_REQUEST['state'] 		= $this->member->state;
	$_REQUEST['city'] 		= $this->member->city;
	$_REQUEST['zip'] 		= $this->member->zip;
	$_REQUEST['country'] 	= $this->member->country;
	

?>

<div class="container_12 clearfix">
				
	<div class="grid_9" style="margin-bottom: 40px;">
	
	
	
		<h5 style="margin-bottom: 30px;">Please note, NO CHARGE WILL BE APPLIED. This simply creates a new CC token</h5>
	
	
		<div class="box clearfix">
		
	
			<div class="box_section">
				<h3>New CC Payment Info For <?=$this->member->first_name . " " . $this->member->last_name;?>.</h3>
			</div>
	
			<? /* IN ORDER FOR STRIPES TO WORK PROPERLY, YOU CAN'T HAVE ANY ACTION IN THE FORM */ ?>
			<form action="" method="POST" id="payment-form">
				<?= $this->render('common/payment-admin.html.php'); ?>
	
			</form>
				
		</div>
	
	</div>
	
	<div class="grid_3"></div>

</div>