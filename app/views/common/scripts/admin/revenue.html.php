<div class="container_12 clearfix">


	<div class="grid_12" style="margin-bottom: 10px;">

		<h1>Monthly Revenue/Growth</h1>

		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>Month</th>
				<th style='text-align: center;'>Total Purchases</th>
				<th style='text-align: center;'>Tax Collected</th>
				<th style='text-align: center;'>Total Payments</th>
				<th style='text-align: center;'>Percentage Change</th>
			</tr>


			<?

				$change = array_reverse($this->monthly_revenues);
				$running_total = 0;
				$count = 0;

				//Calculate % change from previous month
				foreach($change as $key => $month)
				{
					if($key == 0)
					{
						$delta_change[$key] = 0;
						$previous_month = $month->running_total;
					}
					else
					{
						$change = $previous_month > 0 ? round((($month->running_total - $previous_month) / $previous_month) * 100) : 0;
						$delta_change[$key] = !$change && $month->running_total ? 100 :  $change;
						$previous_month = $month->running_total;

						$running_total += $delta_change[$key];

						$count++;
					}

				}

				$final_delta = array_reverse($delta_change);


				foreach($this->monthly_revenues as $key => $month)
				{
					echo "<tr>";

					echo "<td>".$month->month_str."</td>";
					echo "<td style='text-align: right;'>".number_format($month->count_purchases)."</td>";
					echo "<td style='text-align: right;'>$".number_format($month->running_tax)."</td>";
					echo "<td style='text-align: right;'>$".number_format($month->running_total)."</td>";

					echo "<td style='text-align: right;'>".$final_delta[$key]."%</td>";

					echo "</tr>";
				}

				echo "<tr><td colspan='5' style='text-align: right;'><b>". round($running_total / $count)."%</b></td></tr>";

			?>

		</table>
	</div>

  <div class="grid_12" style="margin-bottom: 60px;">
    <?php
    echo $this->render('admin/_growth_charts.html.php', array('monthly_revenues' => $this->monthly_revenues));
    ?>
  </div>
</div>