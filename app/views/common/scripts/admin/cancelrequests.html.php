<div class="container_12 clearfix">

	<div class="grid_12" style="padding-bottom: 60px;">
		
		<h1>Cancel Account Request</h1>
		
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
	
				<th>ID</th>				
				<th>Member</th>				
				<th>Status</th>				
				<th>Admin ID</th>	
				<th>Cancel Request Date</th>	
	
			</tr>
		
	
			<?
				$row = true;
				foreach($this->paginator as $cancel) 
				{	
					echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}
					echo ">\n";
					
					echo "<td>".$cancel->id."</td>\n";
					
					
				
					if(isset($cancel->member_id) && !empty($cancel->member_id))
					{ 
						$member = Member::find($cancel->member_id);
						echo "<td><a href='/admin/member/".$cancel->member_id."'>";
							if($member) echo $member->first_name." " .$member->last_name;
							else echo $cancel->member_id;
						echo "</a></td>\n";		
					}
					else
					{ 
						echo "<td></td>";																				
					}

					
					echo "<td>".$cancel->status."</td>\n";
					
					if(isset($cancel->admin_id) && !empty($cancel->admin_id)) echo "<td>".$cancel->admin->first_name." " .$cancel->admin->last_name."</td>\n";			
					else echo "<td></td>";
					
					
					echo "<td nowrap>".$cancel->created_at->format('M-d-y H:i')."</td>\n";
					
					echo "<tr>";
				}
			
			?>
	
		</table>


		<div style="float: right;"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div>
		<div style="clear: both; margin: 10px 0 20px;" class="clearfix"></div>	
	
	</div>

</div>