<div class="container_12 clearfix">

	<?php if(!empty($this->message)): ?>
	<div class="grid_12">
		<div class="notice">
			<h3 style="color: #005826;"><?php echo $this->message;?></h3>
		</div>
	</div>
<?php endif; ?>



<div class="grid_12" style="padding-top: 30px;">
	<h1>Trial Members</h1>
</div>


<div class="grid_12" style="padding-bottom: 80px;">

	<table style="width: 100%;" class="uiDataTable lifetimeTable">
		<thead>
		<tr>
			<th>ID</th>
			<th>Email</th>
			<th>Created at</th>
		</tr>
		</thead>

		<tbody>
		<?php foreach($this->paginator as $member) { ?>
			<tr>
				<td style='vertical-align: top;'><?php echo $member->id ?></td>
				<td style='vertical-align: top;'><?php echo $member->email ?></td>
				<td style='vertical-align: top;'><?php echo $member->created_at->format("F d, Y") ?></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<div class="grid_12" style="margin-top: 0px;">
		<div style="margin-top: 0px; text-align: right; float: right;">
			<?php
			if (count($this->paginator) > 0) {
				$paginationParams = array('extraParams'=>array());
				echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $paginationParams);
			}
			?>
		</div>
	</div>
</div>
</div>