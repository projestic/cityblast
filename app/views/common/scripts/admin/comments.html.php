<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		<h1><?=number_format($this->paginator->getTotalItemCount());?> Comments  <?php if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo "(".$this->selectedCityObject->name.")";?></h1>

		<table style="width: 100%;" class="uiDataTable">
		    	<tr>	
		    		<th>ID</th>
		    		<th>Date</th>
				<th>Listing ID</th>
				<th>Sales Agent</th>
				<th>Listing Agent</th>
				<th>Name</th>
				<th>Text</th>
				<th>FB UID</th>
				<th>Manage</th>
			</tr>

			<?php $row = true; ?>
			<?php foreach($this->paginator as $comment) { ?>

				<?php $class = ($row) ? 'odd' : ''; ?>
				<?php $row   = !$row; ?>


				<tr class="<?php echo $class; ?>">

					<td style="vertical-align: top;"><?=$comment->id;?></td>
					<td style="vertical-align: top;" nowrap><?php if(isset($comment->created_at) && !empty($comment->created_at)) echo $comment->created_at->format("Y-m-d H:i"); ?></td>
					<td style="vertical-align: top;"><a href="/listing/view/<?php echo $comment->listing->id; ?>"><?php echo $comment->listing->id; ?></a></td>
					
					<? 
						$sales_agent = $comment->sales_agent; 
						$listing_agent = $comment->listing->member; 
						
					?>
					<td style="vertical-align: top;"><a href='/admin/member/<?=$sales_agent->id;?>'><?php if($sales_agent) echo $sales_agent->first_name.' '.$sales_agent->last_name;?></a></td>
					<td style="vertical-align: top;"><a href='/admin/member/<?=$listing_agent->id;?>'><?php if($listing_agent) echo $listing_agent->first_name.' '.$listing_agent->last_name; ?></a></td>
					<td style="vertical-align: top;"><a href="http://www.facebook.com/profile.php?id=<?php echo $comment->fb_uid;?>"><?php echo $comment->fb_name; ?></a></td>
					<td style="vertical-align: top;"><?php echo $comment->text; ?></td>
					<td style="vertical-align: top;"><?php echo $comment->fb_uid; ?></td>
					<td style="vertical-align: top;"><a href="/comments/delete/<?php echo $comment->id;?>">Delete</a></td>
				</tr>
			<?php } ?>
		</table>

		<div style="float: right;">
			<?php echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?>
		</div>
		
	</div>
</div>
