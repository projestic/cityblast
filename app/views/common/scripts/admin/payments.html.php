<?php
if ($this->to_date)
{
	$to_arr = explode("-",$this->to_date);
	
	$day_to	= $to_arr[2];
	$month_to	= $to_arr[1];
	$year_to	= $to_arr[0];
}

if ($this->from_date)
{
	$from_arr = explode("-",$this->from_date);
	
	$day_from		= $from_arr[2];
	$month_from	= $from_arr[1];
	$year_from	= $from_arr[0];
}
?>
<div class="container_12 clearfix">

	<div class="grid_8" style="font-size: 16px; margin-top: 30px;">
		
		<table>
			<tr>
				<td>Total Net</td>
				<td style="text-align: right;">$<? echo number_format($this->total_net, 2); ?></td>
			</tr>
			<tr>
				<td>Total Tax</td>
				<td style="text-align: right;">$<? echo number_format($this->total_tax, 2); ?></td>
			</tr>
			<tr>
				<td>Total (Net + Tax)</td>
				<td style="text-align: right;">$<? echo number_format($this->total_gross, 2); ?></td>
			</tr>
			
			<tr>
				<td>Total Gross ClientFinder Revenue:</td>
				<td style="text-align: right;">$<? echo number_format($this->gross_clientfinder_total, 2); ?></td>
			</tr>
		</table>
		
	</div>

	<div class="grid_4" style="margin-top: 30px; text-align: right">

		<select name="display_type" id="display_type" onchange="window.location.replace('/admin/payments/display/'+this.value);" style="font-size: 22px; font-weight: bold;">
			<option value="all" <?php if($this->display === "all") echo 'selected="selected"';?>>All Payments</option>
			<option value="bymember" <?php if($this->display === "bymember") echo 'selected="selected"';?>>Payments By Member</option>
		</select>


		<select name="status_type" id="status_type" onchange="window.location.replace('/admin/payments/status/'+this.value);" style="margin-top: 15px;font-size: 22px; font-weight: bold;">
			<option value="" <?php if($this->status === "") echo 'selected="selected"';?>>All Members</option>
			<option value="signup" <?php if($this->status === "signup") echo 'selected="selected"';?>>New Members Only</option>
		</select>

	</div>




	<div class="grid_12" style="margin-top: 30px;">
	
		<?php
			if(!empty($this->message)):
			?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
			<?php
			endif;
		?>
		
		
		<form name="filter" id="filter" action="" method="post">
		
		<div style="clear: both">
			<h1><?echo $this->payments->getTotalItemCount();?> Payments
				
				<span style="float: right;">
					
					<select name="state" id="state" class="type_status" style="font-size: 18px; font-weight: bold;">
						<option value="">Prov/State</option>
						<?php
							foreach ($this->payment_states as $payment) 
							{
								if (empty($payment->country) || empty($payment->state)) {
									continue;
								}
								$value = $payment->country .'|'. $payment->state;
								echo '<option value="'. $value .'"'. ($value == $this->payment_state ? ' selected="true"' : '') .'>'. $payment->country .', '. $payment->state .'</option>';
							}
						?>
					</select>
				</span>
				
			</h1>
			<?php
			if($this->payments){
			?>
			<div style="float: right; text-align: right; font-size:18px;">
				Net: $
				<? echo number_format($this->monthly_net, 2); ?>
				<br/>
				Tax: $
				<? echo number_format($this->monthly_tax, 2); ?>
				<br/>
				Total (Net + Tax): $
				<? echo number_format($this->monthly_gross, 2); ?>
				<br/>
			</div>
			<br style="clear: both;"/>
			<?php }?>
		</div>
		
		
			
			
			
		<table style="width: 100%; border-bottom: 0px;">
		<tr>
			<td style="text-align: right;border-bottom: 0px;">
			<b>From :</b>
			<select name="month_from" id="month_from" class="type_status" style="font-size: 18px; font-weight: bold;">
				<option value="">Select Month</option>
				<?php
					$months	= array(
						'01' => 'Jan',
						'02' => 'Feb',
						'03' => 'Mar',
						'04' => 'Apr',
						'05' => 'May',
						'06' => 'Jun',
						'07' => 'Jul',
						'08' => 'Aug',
						'09' => 'Sep',
						'10' => 'Oct',
						'11' => 'Nov',
						'12' => 'Dec',
					);

					foreach ($months as $key => $name) {

						echo '<option value="'. $key .'"'. ($key == $month_from ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="day_from" id="day_from" class="type_status" style="font-size: 18px; font-weight: bold; width: 75px">
				<option value="">Select Day</option>
				<?php

					$days	= range(1, 31);

					foreach ($days as $name) {

						$key	= str_pad($name, 2, '0', STR_PAD_LEFT);

						echo '<option value="'. $key .'"'. ($key == $day_from ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="year_from" id="year_from" class="type_status" style="font-size: 18px; font-weight: bold;">
				<option value="">Select Year</option>
				<?
					$current_year	= intval(date('Y'));

					$years	= range($current_year, $current_year - 25);

					foreach ($years as $name) {

						echo '<option value="'. $name .'"'. ($name == $year_from ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

				
			</td>
			
			<td style="text-align: right;border-bottom: 0px;">
			<b>To:</b>
			<select name="month_to" id="month_to" class="type_status" style="font-size: 18px; font-weight: bold;">
				<option value="">Select Month</option>
				<?php
					$months	= array(
						'01' => 'Jan',
						'02' => 'Feb',
						'03' => 'Mar',
						'04' => 'Apr',
						'05' => 'May',
						'06' => 'Jun',
						'07' => 'Jul',
						'08' => 'Aug',
						'09' => 'Sep',
						'10' => 'Oct',
						'11' => 'Nov',
						'12' => 'Dec',
					);

					foreach ($months as $key => $name) {

						echo '<option value="'. $key .'"'. ($key == $month_to ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="day_to" id="day_to" class="type_status" style="font-size: 18px; font-weight: bold; width: 75px">
				<option value="">Select Day</option>
				<?php

					$days	= range(1, 31);

					foreach ($days as $name) {

						$key	= str_pad($name, 2, '0', STR_PAD_LEFT);

						echo '<option value="'. $key .'"'. ($key == $day_to ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="year_to" id="year_to" class="type_status" style="font-size: 18px; font-weight: bold;">
				<option value="">Select Year</option>
				<?
					$current_year	= intval(date('Y'));

					$years	= range($current_year, $current_year - 25);

					foreach ($years as $name) {

						echo '<option value="'. $name .'"'. ($name == $year_to ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

				
			</td>
			<td style="text-align: right;border-bottom: 0px; width: 30px; padding-left: 10px;"><input type="button" name="go" value="Go" class="uiButton right"/></td>
		</tr>
		</table>
		</form>
		
		<?php 
		if($this->payments){
		?>
		<table style="width: 100%" class="uiDataTable lineheight">
			<tr>
				<th style="padding:4px;"><?php echo ($this->display == 'all') ? 'pID' : '<a href="/admin/payments/display/'. $this->display .'/sort/count/by/'. ($this->sort == 'count' ? $this->by : 'desc') .'">Purchase Count</a>' ?></th>
				
				<th style="padding:4px;">mID</th>
				
				<th style="padding:4px;">Member</th>
				<!--<th style="padding:4px;">Type</th>-->
				<th style="padding:4px;">Unit Price</th>
				<!--<th style="border: 1px solid #888;padding:4px;">Listing ID</th>-->
				
				<th style="padding:4px;">Transaction ID</th>				

				<th style="padding:4px;">Date/Time</th>				
				<!--<th>CC Name</th>-->

				<!--<th>Address</th>-->
				<th style="padding:4px;">City</th>
				<th style="padding:4px;">State</th>
																		

				<!--<th>Country</th>-->
				<th style="padding:4px; margin: 0px;">Aff</th>

				<th style="padding:4px;"><?php echo ($this->display == 'all') ? 'Tax' : 'Total Taxes' ?></th>
				<th style="padding:4px;"><?php echo ($this->display == 'all') ? 'Amount' : '<a href="/admin/payments/display/'. $this->display .'/sort/amount/by/'. ($this->sort == 'amount' ? $this->by : 'desc') .'">Total Amount' ?></th>
				<th>Action</th>

			</tr>
			
		<?
			$row				= true;
			$running_total		= 0;
			$running_tax		= 0;
			$count_purchase	= 0;
			$alen_tax			= 0;
			$alen_total		= 0;


	


			foreach($this->payments as $payment)
			{
				if(stristr($payment->paypal_confirmation_number, "Manual blast created"))
					continue;

				echo "<tr ";
					if($row == true)
					{						
						$row = false;
						echo "class='odd' ";	
					}
					else
					{
						$row = true;	
						echo "class='' ";	
					}		
				echo ">";
				
				
				if ($this->display == 'all')
				{
					echo "<td style='padding:4px;'><a href='/admin/payment/".$payment->id."'>".$payment->id."</a></td>";
				}
				else
				{
					echo "<td style='text-align: right;padding:2px'>".$payment->count_purchases." </td>";
				}
				
				echo "<td style='padding:4px;'><a href='/admin/payment/".$payment->id."'>".$payment->member_id."</a></td>";
				
				echo "<td style='padding:4px; line-height: 15px;'>";
				
				if(isset($payment->member->id))
				{
					$firstPayment = "";
					//if(count($payment->member->payments)=='1');
					
					$query   = "select sum(amount) as total_revenue from payment where member_id=".$payment->member_id." AND paypal_confirmation_number!='FREE_REFERRAL_CREDIT'";					
					$revenue = Payment::find_by_sql($query);				

					if(!is_null($revenue[0]->total_revenue) && $revenue[0]->total_revenue == 0) $firstPayment = '<span style="color: #990000"><b>*</b></span>';
					echo $firstPayment . " <a href='/admin/member/".$payment->member->id."'>" .$payment->member->first_name . " " . $payment->member->last_name . "</a>";
				}
				echo "</td>";
				
									
									
				//echo "<td>";
				//	if($payment->payment_type == "blast" && !empty($payment->listing_id))
				//	{
				//		if(isset($payment->listing->id)) echo $payment->listing->id;
				//	}
				//echo "</td>";


				//echo "<td style='padding:4px;'>" . $payment->payment_type . "</td>";

				
				//setlocale(LC_MONETARY, 'en_US');
					
				echo "<td style='padding:4px; line-height: 15px; color: #a0a0a0; text-align: right;'>$" . number_format($payment->member->price, 2) . "</td>";

				
				echo "<td style='padding:4px;' ";
					if(strtoupper($payment->paypal_confirmation_number) == "FREE BLAST") echo " style='color: #acacac;' ";
				echo ">";				
				echo "<a href='/admin/payment/".$payment->id."'>";
				if($payment->payment_gateway == "PAYPAL") echo $payment->paypal_confirmation_number;
				else echo $payment->stripe_payment_id;
				echo "</a></td>";



				if(isset($payment->created_at) && !empty($payment->created_at)) echo "<td nowrap style='padding:4px;'>".$payment->created_at->format('Y-m-d H:i:s')."</td>";	
				else echo "<td style='padding:4px;'></td>";
				
				//if(isset($payment->member_id) && isset($payment->member->first_name) && isset($payment->member->last_name)) echo "<td>".$payment->member->first_name . " " . $payment->member->last_name."</td>";			
				//else echo "<td>&nbsp;</td>";
				
				//echo "<td>".$payment->address1."</td>";

				echo "<td style='padding:4px; line-height: 15px;'>".$payment->city."</td>";				
				echo "<td style='padding:4px; line-height: 15px;'>".$payment->state."</td>";
				//echo "<td>".$payment->country."</td>";				
				echo "<td style='width: 30px; padding:4px; margin: 0px;'>";
				

				
				if($payment->member->referring_member instanceOf Member) 
					echo "<a href='/admin/member/".$payment->member->referring_member_id."'>".$payment->member->referring_member->name() . "</a>";

				elseif($payment->member->referring_affiliate instanceOf Affiliate) 
					echo "<a href='/admin/member/".$payment->member->referring_affiliate->member_id."'>".$payment->member->referring_affiliate->owner->name() . "</a>";
				
				echo "</td>";

				echo "<td style='text-align: right; padding:4px; ";				
				if(strtoupper($payment->paypal_confirmation_number) == "FREE BLAST") echo " color: #acacac; ";											
				echo "'>".money_format('%!i', ($this->display == 'bymember' ? $payment->t_taxes : $payment->taxes))."</td>";
				echo "<td style='text-align: right; padding:4px; ";
				if(strtoupper($payment->paypal_confirmation_number) == "FREE BLAST") echo " color: #acacac; ";
				echo "'>".money_format('%!i', ($this->display == 'bymember' ? $payment->t_amount : $payment->amount))."</td>";

				echo "<td style='padding:4px; text-align: center;'><a href='/admin/payment/".$payment->id."'>view</a></td>";	

				echo "</tr>";
				

				
			}
			echo "</table>";
			
	
	//PAGINATOR...		
	?>
	</div>
	
	
	<div class="grid_6" style="margin-bottom: 80px;"><input type="button" name="export" class="uiButton" style="float:none;" value="Export To CSV" onClick="window.location='/admin/exporttocsv/';"/></div>
	<div class="grid_6" style="margin-bottom: 80px;">
		<div style="float: right;" class="clearfix"><?= ( $this->payments instanceof Zend_Paginator) ? $this->paginationControl($this->payments, 'Elastic', '/common/pagination.phtml') : ''; ?></div>
	</div>
	
	<?php	


/*
		
			echo "<div style='clear: right; width: 100%;'>&nbsp;</div>";
			
			
			echo "
				<table style='float: right;margin-right: 20px;'>
				<tr>
					<td colspan='9' style='padding:4px;'>Subtotals</td>
					<td style='text-align: right;padding:4px;'>".number_format($this->running_tax, 2, '.', ',')."</td>
					<td style='text-align: right;padding:4px;'>".number_format($this->running_total, 2, '.', ',')."</td>
					<td></td>
				</tr>";

			echo "<tr>
					<td colspan='9' style='padding:4px;'>Shaun+Alen Subtotals</td>
					<td style='text-align: right;padding:4px;'>".number_format($this->alen_tax, 2, '.', ',')."</td>
					<td style='text-align: right;padding:4px;'>".number_format($this->alen_total, 2, '.', ',')."</td>
					<td></td>
				</tr>";


			echo "<tr>
					<td colspan='9' style='padding:4px;'>Net</td>
					<td style='text-align: right;padding:4px;'>".number_format($this->running_tax - $this->alen_tax, 2, '.', ',')."</td>
					<td style='text-align: right;padding:4px;'>".number_format($this->running_total - $this->alen_total, 2, '.', ',')."</td>
					<td></td>
				</tr>";
			
			$subtotal = ($this->running_tax + $this->running_total) - ($this->alen_tax + $this->alen_total);
			echo "<tr>
					<td colspan='9' style='padding:4px;'><strong>". $this->count_purchase ." Total Purchases</strong></td>
					
					<td colspan='2' style='text-align: right;padding:4px;'><strong>$".number_format($subtotal, 2, '.', ',')."</strong></td>
					<td></td>
				</tr>";
			
			//echo "<tr><td colspan='11' style='text-align: right;padding:4px;'>Tax</td></tr>";	
			//echo "<tr><td colspan='11' style='text-align: right; padding:4px;font-weight: bold;'>Total</td><td style='text-align: right; font-weight: bold;'>$".money_format('%!i', ($running_tax + $running_total))."</td></tr>";


			echo "</table>";

**/			
			
		?>
		
				
	<?php 
	} 
	else{?>
		<div style="margin-top: 30px; margin-bottom: 80px;" class="grid_12">
		
		<table width="100%" class="uiDataTable">
			<tr>
				<th style="padding:4px;">pID</th>
				
				<th style="padding:4px;">mID</th>
				
				<th style="padding:4px;">Member</th>
				<th style="padding:4px;">Type</th>
				<!--<th style="border: 1px solid #888;padding:4px;">Listing ID</th>-->
				
				<th style="padding:4px;">Transaction ID</th>				

				<th style="padding:4px;">Date/Time</th>				
				<!--<th>CC Name</th>-->

				<!--<th>Address</th>-->
				<th style="padding:4px;">City</th>
				<th style="padding:4px;">State</th>
																		

				<!--<th>Country</th>-->
				<th style="padding:4px; margin: 0px;">Aff</th>

				<th style="padding:4px;">Tax</th>
				<th style="padding:4px;">Amount</th>
				<th>Action</th>

			</tr>
			<tr class=""><td style="padding:4px;" colspan="12">No record found.</td></tr>
		</table>
		</div>
	<?php }?>

	</div>
	

	
</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/index/";
		var data    =	{"blast":"ON","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});
});
</script>

<script type="text/javascript">
$('#filter input[name="go"]').click(function(event) {
		event.preventDefault();
		if (!$('#month_from').val()) {
			alert('Please select month from.');
			return false;
		}

		if (!$('#day_from').val()) {
			alert('Please select day from.');
			return false;
		}

		if (!$('#year_from').val()) {
			alert('Please select year from.');
			return false;
		}

		if (!$('#month_to').val()) {
			alert('Please select month to.');
			return false;
		}

		if (!$('#day_to').val()) {
			alert('Please select day to.');
			return false;
		}

		if (!$('#year_to').val()) {
			alert('Please select year to.');
			return false;
		}
		
		var pathElements = [
			'admin', 'payments', 
			'from', $('#month_from').val()+'-'+$('#day_from').val()+'-'+$('#year_from').val(),
			'to', $('#month_to').val()+'-'+$('#day_to').val()+'-'+$('#year_to').val(),
			'state', $('#state').val()
		];
		$(pathElements).each(function(i){
			pathElements[i] = encodeURIComponent(pathElements[i]);
		});
		
		$('#filter').attr('action', '/' + pathElements.join('/'));
		$('#filter').submit();
	}); 
</script>
