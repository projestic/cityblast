<div class="container_12 clearfix">

	<div class="grid_12" style="margin-bottom: 80px;">

		<div style="float: right; margin-top: 10px;">
			<div>
				<span style="font-size: 22px;">Week</span>
			</div>
		</div>
		<div style="display: inline-block; width: 100%;">
			<span style="float: left;">
				<h1 style="margin: 0px;">Week 1 (2013, Jan)</h1>
			</span>
			<span style="float: left;">
				<span style="font-size: 19pt; font-weight: bold; margin-left: 15px;">Engagement score: 12432423</span>
			</span>
		</div>

		<h1>Top 25 performing listings</h1>
			<table style="width: 100%;" class="uiDataTable lineheight">			
			<tr>
				<th>Rating</th>
				<th>Listing ID</th>
				<th >Clicks</th>
				<th >Likes</th>
				<th >Comments</th>
				<th >Shares</th>
				<th >Engagement Score</th>
			</tr>
			</table>

		<div style="margin-top: 20px;">
			<div style="float: right;">
				See weekly through stats
				<form method="get" action="/admin/engagweekly_thru">
					<input type="submit" value="Stats weekly through" title="Through stats" class="uiButton alt"/>
				</form>
			</div>
		</div>
	</div>
</div>
