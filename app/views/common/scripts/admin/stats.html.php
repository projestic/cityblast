<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; text-align: right;">

			<!--
			history:
			<#? if($this->order_by != 2) { ?>
				<ul style="float: right;">
				echo $this->partial('admin/ui/dateComponent.html.php', array('date_chosen_str' => $endDate, 'label' => 'End Date'))
			-->

			<div class="grid_12" style="float: right;">
				<div>
					<select id="groupByType" style="font-size: 22px; font-weight: bold;">
						<option value='0'>
							Daily
						</option>
						<option value='1' <?php if(1) echo 'selected="selected"'; ?>>
							By Listing Id
						</option>
						<option value='2' <?php if(2) echo 'selected="selected"'; ?>>
							By Member Id
						</option>
					</select>
				</div>
				<div style="margin-top: 5px;">
					<div id="listingBlastTypeContainer" style="<?if (false) echo 'display: none;' ?>">
						<select onchange="window.location.replace('<?php echo APP_URL . '/admin/stats/listingBlastType/'; ?>'+this.value)" style="font-size: 22px; font-weight: bold;">
							<option value='0'>
								Select
							</option>
							<option value='LISTING' <?php if('' === 'LISTING') echo 'selected="selected"'; ?>>
								Listing
							</option>
							<option value='CONTENT' <?php if('' === 'CONTENT') echo 'selected="selected"'; ?>>
								Content
							</option>
							<option value='IDX' <?php if('' === 'IDX') echo 'selected="selected"'; ?>>
								Idx
							</option>
						</select>
					</div>
				</div>
			</div>
	</div>
	
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		<div style="display: inline-block; width: 100%;">
			<span style="float: left;">
				<h1 style="margin: 0px;">Click Stats</h1>
			</span>
			<span style="float: right;">

				<form method="POST" action="/admin/stats/order//listingBlastType/">
					<div>
					<span style="float: left;">
						<?php
						echo $this->partial('admin/ui/dateComponent.html.php', array('id' => 'startDate', 'date_chosen_str' => '2013-11-22', 'label' => 'From'))
						?>
					</span>
					<span style="float: left; margin-left: 10px;">
						<?php
						echo $this->partial('admin/ui/dateComponent.html.php', array('id' => 'endDate', 'date_chosen_str' => '2013-11-22', 'label' => 'To'))
						?>
					</span>
					<span style="float: left; margin-left: 10px;">
						<input type="submit" name="submitted" id="submitted" class="uiButton small right" value="ok">
					</span>
					</div>
				</form>

			</span>
			<span style="clear:both;"></span>
		</div>
		<div name="table-wrapper">





				<table style="width: 100%;" class="uiDataTable lineheight">
				<tr>
					<th>Rank</th>
					<th>Date</th>
					<th>Clicks</th>
					<th class="page_Text_AlignRight">Likes</th>
					<th class="page_Text_AlignRight">Comments</th>
					<th class="page_Text_AlignRight">Shares</th>
					<th class="page_Text_AlignRight">Engagement</th>
				</tr>


						<tr class="">
							<td style="vertical-align: top;"></td>
							<td style="vertical-align: top;"></td>
							<td class="page_Text_AlignTopAndRight"></td>
							<td class="page_Text_AlignTopAndRight"></td>
							<td class="page_Text_AlignTopAndRight"></td>
							<td class="page_Text_AlignTopAndRight"></td>
							<td class="page_Text_AlignTopAndRight"></td>
						</tr>


					<tr><td colspan="3" style="text-align: right; font-weight: bold; font-size: 14px;"></td></tr>

				</table>





		<div class="grid_10" style="margin-top: 0px;">
			<div style="margin-top: 0px; text-align: right; float: right;">
			</div>
		</div>
		</div> <!-- table-wrapper -->
	</div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/index/";
		var data    =	{"hasmember":"1","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});
});

function Page_AdminStat_GroupType() {

};
Page_AdminStat_GroupType.App_Url = "<?= APP_URL ?>";
Page_AdminStat_GroupType.bindEvents = function() {
	var thisRef = this;
	$('#groupByType').on("change", function(event) {
		var el = $(this);
		var val = el.val();
		if (val == 1) {
			thisRef.onByListing(el);
			return;
		}
		window.location.replace(thisRef.App_Url + '/admin/stats/order/' + this.value);
	})
};
Page_AdminStat_GroupType.onByListing = function(el) {
	$('#listingBlastTypeContainer').show();
};

$(function() {
	Page_AdminStat_GroupType.bindEvents();
});

</script>
<style type="text/css">
	.uiDataTable th.page_Text_AlignRight {
		text-align: right;
	}
	.page_Text_AlignTopAndRight {
		vertical-align: top; text-align: right;
	}
</style>