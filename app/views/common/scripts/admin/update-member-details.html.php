<div class="container_12 member_update">

	<div class="grid_12 member_update">
		<?php if(!empty($this->messages)): ?>
			<div class="box white80 clearfix" style="text-align: center;">
				<?php foreach( $this->messages as $message):?>
					<h3 style="color: #005826;"><?php echo $message;?></h3>
				<?php endforeach;?>
			</div>
		<?php endif; ?>

		<?php if(!empty($this->message)): ?>

			<div class="box white80 clearfix" style="text-align: center;">

				<h3 style="color: #005826;"><?php echo $this->message;?></h3>

			</div>

		<?php endif; ?>

	</div>




	<div class="grid_12 member_update">

		<div class="box gray clearfix">



			<form id="extraForm" action="/member/update/<?php echo $this->member->id;?>" method="POST" style="margin: 0 -20px;">

					<ul class="uiForm clearfix">
						<li class="uiFormRowHeader">Account Type</li>
						<li class="uiFormRow clearfix">
							<ul>
								<li class="fieldname">Account Type</li>
								<li class="field">
									<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="type_id" class="uiSelect">
										<option value=""></option>
										<?php foreach (MemberType::all() as $type_option) : ?>
										<option <?php if ($this->member->type_id == $type_option->id) echo 'selected'; ?> value="<?= $type_option->id ?>"><?= $type_option->name ?></option>
										<?php endforeach; ?>
									</select></span></span>
								</li>
							</ul>
						</li>	
					</ul>					
				

				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Contact Details</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">First Name</li>
							<li class="field">
								<input type="text" name="first_name" id="first_name" value="<?php echo $this->member->first_name;?>" class="uiInput" style="width: 500px" />
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Last Name</li>
							<li class="field">
								<input type="text" name="last_name" id="last_name" value="<?php echo $this->member->last_name;?>" class="uiInput" style="width: 500px" />
							</li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Email</li>
							<li class="field" style="font-size: 14px;">
							<?php if ( empty($this->member->email) ) : ?>
								<input type="text" name="email" id="email" value="<?php echo $this->member->email;?>" class="uiInput" style="width: 500px" />
							<?php else : ?>
								<strong><a href="mailto:<?php echo $this->member->email;?>"><?php echo $this->member->email;?></a></strong>
							<?php endif; ?>
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Email Override</li>
							<li class="field">
								<input type="text" name="email_override" id="email_override" value="<?php echo $this->member->email_override;?>" class="uiInput" style="width: 500px" />
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Phone Number</li>
							<li class="field">
								<input type="text" name="phone" id="phone" value="<?php echo $this->member->phone;?>" class="uiInput" style="width: 500px" />
							</li>
						</ul>
					</li>


				</ul>


				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Franchise</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Franchise:</li>
							<li class="field">
								
								<span class="uiSelectWrapper"><span class="uiSelectInner">
								<select name="franchise_id" class="uiSelect">
									<option value=""></option>
									<?php 																
										foreach ($this->franchises as $franchise) {
											echo "<option value='".$franchise->id."' ";
											if($this->member->franchise_id == $franchise->id) echo " selected ";
											echo ">" . $franchise->name . "</option>";
										}
									?>
								</select>
								</span></span>
								
							</li>
						</ul>
					</li>	
					
					<li class="uiFormRow clearfix">

						<ul>
							<li class="fieldname">Broker of Record</li>
							<li class="field">
								<input type="text" name="brokerage" id="brokerage" value="<?php echo $this->member->brokerage;?>" class="uiInput" style="width: 500px" />
							</li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">

						<ul>
							<li class="fieldname">Broker Address</li>
							<li class="field">
								<input type="text" name="broker_address" id="broker_address" value="<?php echo $this->member->broker_address;?>" class="uiInput" style="width: 500px" />
							</li>
						</ul>
					</li>
				</ul>
								
								

				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Affiliate Owner</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Pay Commission To:</li>
							<li class="field">
								
								<span class="uiSelectWrapper"><span class="uiSelectInner">
								<select name="affiliate_id" class="uiSelect">
									<option value=""></option>
									<?php 																
										foreach ($this->affiliates as $affiliate) {
											echo "<option value='".$affiliate->id."' ";
											if($this->member->referring_affiliate_id == $affiliate->id) echo " selected ";
											echo ">" . $affiliate->owner->last_name. ", " .$affiliate->owner->first_name. "</option>";
										}
									?>
								</select>
								</span></span>
								
							</li>
						</ul>
					</li>	

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Promo Code:</li>
							<li class="field" style="color: #000000; font-size: 14px;"><? if ($this->member->promo_code) echo $this->member->promo_code->promo_code;?>

							</li>
						</ul>
					</li>


					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Promo Code Change:</li>
							<li class="field">
								<?php if ($this->member->type->promo_codes_allowed) : ?>
									<a href="/admin/applypromo/<?php echo $this->member->id;?>" class="uiButton">Apply Promo Code</a>
								<?php endif; ?>

							</li>
						</ul>
					</li>
					
					
														
				</ul>

				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Affiliate Member</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Member is affiliate</li>
							<li class="field">
								<?php if ($this->member->isAffiliate() && $this->member->affiliate_account->hasReferredMembers()) : ?>
									Yes, has <?= $this->member->affiliate_account->countReferredMembers() ?> referrals and may not be disabled.
								<?php else : ?>
								<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="is_affiliate_member" class="uiSelect">
									<option <?php echo ($this->member->isAffiliate()) ? 'selected':''; ?> value="1">Yes</option>
									<option <?php echo (!$this->member->isAffiliate()) ? 'selected':''; ?> value="0">No</option>
								</select></span></span>
								<?php endif; ?>
							</li>
						</ul>
						<ul>
							<li class="fieldname">First payout</li>
							<li class="field">
								<input type="text" name="affiliate_first_payout_percent" id="affiliate_first_payout" value="<?= is_object($this->member->affiliate_account) ? $this->member->affiliate_account->first_payout_percent : '' ?>" class="uiInput <?=($this->fields && in_array("affiliate_first_payout_percent ", $this->fields)?" error":"")?>" style="width: 28px" />&nbsp;%
							</li>
						</ul>
						<ul>
							<li class="fieldname">Recurring payout</li>
							<li class="field">
								<input type="text" name="affiliate_recurring_payout_percent" id="affiliate_recurring_payout" value="<?= is_object($this->member->affiliate_account) ? $this->member->affiliate_account->recurring_payout_percent : '' ?>" class="uiInput <?=($this->fields && in_array("affiliate_recurring_payout_percent", $this->fields)?" error":"")?>" style="width: 28px" />&nbsp;%
							</li>
						</ul>
						<ul>
							<li class="fieldname">Override payouts</li>
							<li class="field">
								<span class="uiSelectWrapper"><span class="uiSelectInner">
								<select name="override_type" class="uiSelect" style="width: 450px;">
									<option value="">This affiliate's sales do not generate any overrides.</option>
									<option value="override_affiliate_id" <?php if (is_object($this->member->affiliate_account) && $this->member->affiliate_account->override_affiliate_id) : ?>selected="1"<?php endif; ?>>This affiliate's sales generate overrides for another affiliate.</option>
									<option value="override_payout_percent" <?php if (is_object($this->member->affiliate_account) && $this->member->affiliate_account->override_payout_percent) : ?>selected="1"<?php endif; ?>>This affiliate receives overrides for other affiliates' sales.</option>
								</select>
								</span></span>
							</li>
						</ul>
						<ul id="override_payout_percent" class="override_payout_options" style="display: none">
							<li class="fieldname">Override payout</li>
							<li class="field">
								<input type="text" name="affiliate_override_payout_percent" id="affiliate_override_payout" value="<?= is_object($this->member->affiliate_account) ? $this->member->affiliate_account->override_payout_percent : '' ?>" class="uiInput <?=($this->fields && in_array("affiliate_override_payout_percent", $this->fields)?" error":"")?>" style="width: 28px" />&nbsp;%
							</li>
						</ul>
						<ul id="override_affiliate_id" class="override_payout_options" style="display: none">
							<li class="fieldname">Overrides paid to</li>
							<li class="field">
								<span class="uiSelectWrapper"><span class="uiSelectInner">
								<select name="affiliate_override_affiliate_id" class="uiSelect">
									<option value="">No one</option>
									<?php 
									
									if ($this->member->affiliate_account) {
										$conditions = array('override_payout_percent > 0 AND affiliate.id != ?', $this->member->affiliate_account->id);
									} else {
										$conditions = array('override_payout_percent > 0');
									}
																		
									foreach (Affiliate::all( array('conditions' => $conditions, 'joins' => array('JOIN member ON member_id = member.id'), 'order' => 'last_name ASC, first_name ASC')) as $aff) : ?>
									<option value="<?= $aff->id ?>" <?php if (is_object($this->member->affiliate_account) && $this->member->affiliate_account->override_affiliate_id == $aff->id) : ?>selected="1"<?php endif; ?>><?= $aff->owner->name() ?> (<?= $aff->override_payout_percent ?>% override)</option>
									<?php endforeach; ?>
								</select>
								</span></span>
							</li>
						</ul>
						<ul>
							<li class="fieldname">Public signup page</li>
							<li class="field">
								<div style="float: left;">http://<?= $_SERVER['HTTP_HOST'] ?>/</div><div style="float: left; margin-left: 8px;"><input type="text" name="affiliate_slug" id="affiliate_slug" value="<?= is_object($this->member->affiliate_account) ? $this->member->affiliate_account->slug : '' ?>" class="uiInput <?=($this->fields && in_array("affiliate_recurring_payout_percent", $this->fields)?" error":"")?>" style="width: 260px;" /></div>
							</li>
						</ul>
						<ul>
							<li class="fieldname">Public referral URL</li>
							<li class="field" style="color: #000;">
								<?= is_object($this->member->affiliate_account) ? 'http://' . $_SERVER['HTTP_HOST'] . '/a/i/' . $this->member->affiliate_account->id : '<em>Save to generate URL</em>' ?>
							</li>
						</ul>
						<ul>
							<li class="fieldname">Logo</li>
							<li class="field">
								<div class="photo_upload_thumb" style="width: 345px; height: 65px;">
									<div id="logo_upload_progress" class="progress"></div>
									<?php if ($this->member->affiliate_account && $this->member->affiliate_account->logo) : ?>
									<img src="<?= CDN_URL . $this->member->affiliate_account->logo ?>" id="logo_upload_image" />
									<?php else : ?>
									<img src="" id="logo_upload_image" style="display: none;"/>
									<?php endif; ?>
								</div>
								<input id="logo_upload" name="logo_upload" type="file" width="335" height="104" />
								<span id="logo_status" class="uploadstatus" style="float: left;"></span>
								<input type="hidden" name="logo_uploaded" id="logo_uploaded" />
								<script type="text/javascript" src="/js/swfobject.js"></script>
								<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
								<script type="text/javascript">
									$(document).ready(function() {
										$('#logo_upload').uploadify({
											'uploader'  : '/js/uploadify/uploadify.swf',
											'script'    : '/affiliate/uploadlogo',
											'cancelImg' : '/js/uploadify/cancel.png',
											'folder'    : '/images/',
											'auto'      : true,
											'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
											'fileDesc'  : 'Image Files',
											'buttonImg' : '/images/browse_button.png',
											'removeCompleted' : false,
											'height' : 30,
											'width' : 106,
											'name': 'logo_uploaded',
											'onComplete': function(event, ID, fileObj, response, data)
											{
												if (response.indexOf('/tmp/') == -1) {
													$.colorbox({href:false, innerWidth:420, initialWidth:450, initialHeight:200, html: response});
												} else {
													$('#logo_uploaded').val(response);
													$("#logo_status").show();
													$("#logo_status").html("<span style='display: block; text-align: center;'>Upload Complete</span>");
													$('#logo_upload_image').attr('src', response).show();

													$('#logo_upload_progress').css('display', 'none');
												}
											},

											'onError'     : function (event, ID, fileObj, errorObj) {
												$.colorbox({href:false, innerWidth:420, initialWidth:450, initialHeight:200, html: errorObj.info});
											},

											'onProgress'  : function(event,ID,fileObj,data)
											{
												$('#logo_upload_image').attr('src', '').hide();
												$("#logo_upload_status").html("<span style='display: block; text-align: center;'>Uploaded " + data.percentage + "% &hellip;</span>");
												$('#logo_upload_progress').css('display', 'block');
												$('#logo_upload_progress').css('width', 335).css('height', Math.round(data.percentage/100 * 65) + 'px');
												return false;
											}

										});
									});
								</script>
							</li>
						</ul>
					</li>
				</ul>

<script type="text/javascript">

		$('#slug').bind('keyup', function () {
			
			var slug = $(this).val();
			
			$(this).val( $(this).val().replace(/[^A-Za-z0-9\-]/, '') );
			
			if (slug_validate) clearTimeout(slug_validate);
			
			slug_validate = setTimeout( validate, 100 );
			
			function validate() {
				$('#slug-ok, #slug-invalid').hide();
				$('#slug-message').html('');
				$.get('/member/validate_affiliate_slug?slug=' + slug, function (response) {
					if ($('#slug').val() == response.slug) {
						if (response.allowed) {
							$('#slug-ok').show();
							$('#slug-message').css('color', '#090');
						} else {
							$('#slug-invalid').show();
							$('#slug-message').css('color', '#900');
						}
						$('#slug-message').html(response.message);
					}
				}, 'json');
			}
			
		});

</script>

				<script>
				function randomString(len) {
					   var chrs = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
								  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
								  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
 
					  len = (isNaN(len)) ? 32 : len;
 
					  var tmp, current, top = chrs.length; 
					  if(top)
					  {
						while(--top) 
						{ 
						  current = Math.floor(Math.random() * (top + 1)); 
						  tmp = chrs[current]; 
						  chrs[current] = chrs[top]; 
						  chrs[top] = tmp; 
						}
					  }
 
					  var randomStr = '';
					  for(i=0;i<len;i++) 
					  {
						randomStr = randomStr + chrs[Math.floor(Math.random()*chrs.length)];
					  }
 
					  return randomStr;
					}
				</script>

				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Payment Terms</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Account Type</li>
							<li class="field">
								<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="account_type" class="uiSelect">
									<option <?php echo ($this->member->account_type == 'paid') ? 'selected':''; ?> value="paid">Paying Member</option>
									<option <?php echo ($this->member->account_type == 'free') ? 'selected':''; ?> value="free">Free Member</option>
								</select></span></span>
							</li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Billing Cycle</li>
							<li class="field">
								<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="billing_cycle" id="billing_cycle" class="uiSelect">
									<option <?php echo ($this->member->billing_cycle == 'month') ? 'selected':''; ?> value="month">Monthly</option>
									<option <?php echo ($this->member->billing_cycle == 'biannual') ? 'selected':''; ?> value="biannual">Biannually</option>
									<option <?php echo ($this->member->billing_cycle == 'annual') ? 'selected':''; ?> value="annual">Annually</option>
								</select></span></span>
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Unit Price</li>
							<li class="field">
								<input type="text" name="price" id="price" value="<?php echo $this->member->price;?>" class="uiInput validateNotempty validatePositiveNumber <?=($this->fields && in_array("price", $this->fields)?" error":"")?>" style="width: 100px" />
								<span class="label">Must be a positive number. Use account type 'Free' for free accounts.</span>
							</li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Monthly Price</li>
							<li class="field" id="price_month">
								<?php echo $this->member->price_month;?>
							</li>
						</ul>
					</li>


					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Account Status</li>
							<li class="field" style="font-weight: bold;">
								<?=$this->member->payment_status;?>
							</li>
						</ul>
					</li>




				</ul>


				<? /***
				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Credits</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Credits</li>
							<li class="field">
								<input type="text" name="credits" id="credits" value="<?php echo intval($this->member->credits);?>" class="uiInput validateNotempty <?=($this->fields && in_array("credits", $this->fields)?" error":"")?>" style="width: 100px" />
							</li>
						</ul>
					</li>

				</ul>
				****/ ?>


				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Publishing Assignment</li>


					<?/*				
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Inventory ID</li>
							<li class="field">
								<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="inventory_id" id="inventory_id" style="font-size: 14px; width: 260px;" class="uiSelect" tabindex="5">
									<option value=""></option>
									<?php for($i=1;$i<=$this->member->publish_city->number_of_inventory_units;$i++): ?>
										<option value="<?php echo $i;?>" <?php if (isset($this->member->inventory_id) && ($this->member->inventory_id == $i)) echo "selected"; ?>><?php echo $i;?></option>
									<?php endfor; ?>
								</select></span></span>
							</li>
						</ul>
					</li>

					*/?>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">City:</li>
							<li class="field">
								<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="city_id" id="publish_city_id" style="font-size: 14px; width: 260px;" class="uiSelect">
									<option value=""></option>
								<?php foreach($this->cities as $city): ?>
									<option value="<?php echo $city->id;?>" <?php if (!empty($this->member->city_id) && ($this->member->city_id == $city->id)) echo "selected"; ?>>
										<?php echo $city->name . ", ". $city->state;?>										
										</option>
								<?php endforeach; ?>
								</select></span></span>
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Hopper ID</li>
							<li class="field">
								<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="current_hoper" id="current_hoper" style="font-size: 14px; width: 260px;" class="uiSelect" tabindex="6">
									<option value=""></option>
									<?php for($i=1;$i<=$this->member->default_city->number_of_hoppers;$i++): ?>
										<option value="<?php echo $i;?>" <?php if (isset($this->member->current_hoper) && ($this->member->current_hoper == $i)) echo "selected"; ?>><?php echo $i;?></option>
									<?php endfor; ?>
								</select></span></span>
							</li>
						</ul>
					</li>




				</ul>

				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Publish Settings</li>



					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Frequency</li>
							<li class="field">

								<?
									if(!isset($this->member->frequency_id) or empty($this->member->frequency_id))
									{
										$this->member->frequency_id = 0;
									}
								
								?>
	
								<input type="hidden" name="frequency_id" id="frequency_id" value="<?=$this->member->frequency_id;?>"/>
								<div id="slider" style="width: 200px; float: left; margin-top: 10px;"></div>
								<div style="float: left; margin-left: 10px;">Publish <input type="text" id="amount" style="border: 0; color:#000; font-weight:bold; width: 20px; font-size: 16px; text-align: center;" /> day(s) a week.</div>

								<? /*
								<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="frequency_id" id="frequency_id" style="font-size: 14px; width: 260px;" class="uiSelect">
									<option value=""></option>

									<option value="3" <?php if (!empty($this->member->frequency_id) && ($this->member->frequency_id == 3)) echo "selected"; ?>>Business</option>
									<option value="2" <?php if (!empty($this->member->frequency_id) && ($this->member->frequency_id == 2)) echo "selected"; ?>>Professional</option>
									<option value="1" <?php if (!empty($this->member->frequency_id) && ($this->member->frequency_id == 1)) echo "selected"; ?>>Personal</option>


								</select>	</span></span>
								*/ ?>
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Auto-like posts</li>
							<li class="field">
								<input class="uiRadio" type="radio" name="auto-like" value="1" <?=($this->member->auto_like)?'checked="checked"':''?>>
								<span class="label">On</span>
								<input class="uiRadio" type="radio" name="auto-like" value="0" <?=(!$this->member->auto_like) ?'checked="checked"':''?>>
								<span class="label">Off</span>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Facebook Publishing</li>
							<li class="field">
								<input type="radio" class="uiRadio" name="facebookpublish" value="1" <?php if($this->member->facebook_publish_flag==1) echo 'checked';?>> <span class="label">On</span>
								<input type="radio" class="uiRadio" name="facebookpublish" value="0" <?php if($this->member->facebook_publish_flag==0) echo 'checked';?>> <span class="label">Off</span>
								<hr />
								<input type="checkbox" class="uiCheckbox" name="resetfbcredentials" id="resetfbcredentials" /> <span class="label">Reset Facebook credentials</span>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Twitter Publishing</li>
							<li class="field">
								<?php if( empty($this->member->twitter_request_token) || empty($this->member->twitter_access_token) ): echo '<i>(Empty)</i>';?>
								<?php else : ?>
									<input type="radio" class="uiRadio" name="twitterpublish" value="1" <?php if($this->member->twitter_publish_flag==1) echo 'checked';?>> <span class="label">On</span>
									<input type="radio" class="uiRadio" name="twitterpublish" value="0" <?php if($this->member->twitter_publish_flag==0) echo 'checked';?>> <span class="label">Off</span>
								<? endif; ?>
								<hr />
								<input type="checkbox" class="uiCheckbox" name="resettwittercredentials" id="redettwittercredentials" /> <span class="label">Reset Twitter credentials</span>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">LinkedIn Publishing</li>
							<li class="field">
								<?php if( empty($this->member->linkedin_request_token) || empty($this->member->linkedin_access_token) ): echo '<i>(Empty)</i>';?>
								<?php else : ?>
									<input type="radio" class="uiRadio" name="linkedinpublish" value="1" <?php if($this->member->linkedin_publish_flag==1) echo 'checked';?>> <span class="label">On</span>
									<input type="radio" class="uiRadio" name="linkedinpublish" value="0" <?php if($this->member->linkedin_publish_flag==0) echo 'checked';?>> <span class="label">Off</span>
								<? endif; ?>
								<hr />
								<input type="checkbox" class="uiCheckbox" name="resetlinkedincredentials" id="redetlinkdincredentials" /> <span class="label">Reset LinkedIn credentials</span>
							</li>
						</ul>
					</li>


				</ul>

				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">REST API</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Member is API user</li>
							<li class="field">
								<?php if ($this->member->isOAuthClient() && $this->member->oauth_client->hasTokens()) : ?>
									Yes, has created <?= $this->member->oauth_client->countTokens() ?> tokens and may not be disabled.
								<?php else : ?>
								<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="is_oauth_client" class="uiSelect">
									<option <?php echo ($this->member->isOAuthClient()) ? 'selected':''; ?> value="1">Yes</option>
									<option <?php echo (!$this->member->isOAuthClient()) ? 'selected':''; ?> value="0">No</option>
								</select></span></span>
								<?php endif; ?>
							</li>
						</ul>

						<ul>
							<li class="fieldname">Client API Key</li>
							<li class="field">
								<?php if ($this->member->oauth_client) : ?><?= $this->member->oauth_client->client_id; ?><?php else : ?><?= $this->tmp_api_key ?><input type="hidden" name="tmp_api_key" value="<?= $this->tmp_api_key ?>"/> (must submit form to be activated)<?php endif; ?>			
							</li>
						</ul>

						<ul>
							<li class="fieldname">Client API Secret</li>
							<li class="field">
								<?php if ($this->member->oauth_client) : ?><?= $this->member->oauth_client->client_secret; ?><?php else : ?><?= $this->tmp_api_secret ?><input type="hidden" name="tmp_api_secret" value="<?= $this->tmp_api_secret ?>"/> (must submit form to be activated)<?php endif; ?>			
							</li>
						</ul>

						<ul>
							<li class="fieldname">Client Organization Name</li>
							<li class="field">
								<input type="text" name="oauth_client_name" id="oauth_client_name" value="<?= is_object($this->member->oauth_client) ? $this->member->oauth_client->client_name : '' ?>" class="uiInput <?=($this->fields && in_array("oauth_client_name", $this->fields)?" error":"")?>" />
								&nbsp;&nbsp;<em>Shown publicly when members are asked to grant access to this organization.</em>
							</li>
						</ul>

						<ul>
							<li class="fieldname">Client Organization Website</li>
							<li class="field">
								<input type="text" name="oauth_client_site" id="oauth_client_site" value="<?= is_object($this->member->oauth_client) ? $this->member->oauth_client->client_name : '' ?>" class="uiInput <?=($this->fields && in_array("oauth_client_site", $this->fields)?" error":"")?>" />
								&nbsp;&nbsp;<em>Shown publicly when members are asked to grant access to this organization.</em>
							</li>
						</ul>

						<ul>
							<li class="fieldname">Client OAuth Redirect URL</li>
							<li class="field">
								<input type="text" name="oauth_client_name" id="oauth_client_name" value="<?= is_object($this->member->oauth_client) ? $this->member->oauth_client->client_name : '' ?>" class="uiInput <?=($this->fields && in_array("oauth_client_name", $this->fields)?" error":"")?>" />
								&nbsp;&nbsp;<em>Dependent on client configuration. Do not change without verifying.</em>
							</li>
						</ul>
					</li>
					
				</ul>				
				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Content Settings</li>
					<li class="uiFormRow clearfix">
						<?php if(isset($this->content_preference) && is_array($this->content_preference)):?>
						<ul>
							<?php foreach($this->content_preference as $preference):?>
							<li class="fieldname">
								<?php echo $preference->name;?>
							</li>
							<li class="field">
								<input class="uiRadio" type="radio" name="preferences[<?php echo $preference->id;?>]" value="1" <?php
									echo $preference->pref === "1" ? 'checked="checked"' : '';
								?>>
								<span class="label">On</span>
								<input class="uiRadio" type="radio" name="preferences[<?php echo $preference->id;?>]" value="0" <?php
									echo $preference->pref !== "1" ? 'checked="checked"' : '';
								?>>
								<span class="label">Off</span>
							</li>
							<?php endforeach;?>
						</ul>
						<?php endif;?>
					</li>
				</ul>

			</form>

		</div>

		<input type="button" value="Update" id="update" class="uiButton large right" />

	</div>
</div>


<script language="javascript">

$(document).ready(function(){


	// when ochange the publish city in the publishing assignment section
	$("#publish_city_id").change(function () {
		
		$.post("/member/changepublishingcity", {"publish_city_id": $(this).val()}, function (response) {
			if ( response.error !== false ) {
				return;
			}
			if ( response.options ) {
				$("#current_hoper").html(response.options);
			}
		}, "json");
		return true;
	});

	$("#update").click(function(event){
	
		//alert('I am updating!');
		
		$("#update").attr('disabled', true);
		event.preventDefault();
		validateForm();
	});
	
	$("select[name=is_affiliate_member], select[name=is_oauth_client]").change( function(event) {
		if ($(this).val() == 1) {
			$(this).parents('.uiFormRow').find('ul').show();	
		} else {
			$(this).parents('.uiFormRow').find('ul').hide();
			$(this).parents('ul').show();
		} 
		$("select[name=override_type]").trigger('change');
	}).trigger('change');

	$("select[name=override_type]").bind('change', function (e) {
		var id = $(this).val();
		var $options = $('.override_payout_options');
		if (id) {
			$options = $options.not('#' + id);
		}
		$options.hide().find('select, input').val('');
		$('#' + id).show();
	}).trigger('change');

	$("#price, #billing_cycle").change(function(event){
		var orig_val = $("#price").val();
		switch ($("#billing_cycle").val()){
			case 'month':
				var new_val = new Number(orig_val);
				new_val = new_val.toFixed(2);
				$("#price_month").html(new_val);
			break;
			case 'biannual':
				var new_val = orig_val / 6;
				new_val = new_val.toFixed(2);
				$("#price_month").html(new_val);
			break;
			case 'annual':
				var new_val = orig_val / 12;
				new_val = new_val.toFixed(2);
				$("#price_month").html(new_val);
			break;
		}
	});
	
	$( "#slider" ).slider({
		value:<?=$this->member->frequency_id;?>,
		min: 0,
		max: 7,
		step: 1,
		slide: function( event, ui )
		{
			$("#amount").val(ui.value);
		},
		change: function( event, ui )
		{
			$('#frequency_id').val(ui.value);
		}
	});
	$("#amount").val($( "#slider" ).slider( "value" ) );	
	
});
</script>
