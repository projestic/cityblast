<link href="/js/multi-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">


	<?php if(isset($this->updated) && $this->updated === true): ?>
	
	    	<div class="grid_12"><h3>Your Group was successfully added!</h3>
			<p><a href="/admin/groups">Return to your group list.</a></p>
		</div>
	
		<div class="clearfix">&nbsp;</div>	
	<?php endif; ?>


	<?php if( !empty($this->error) ) : ?>
		<div class="grid_12 error">
		    <h3><?php echo $this->error;?></h3>
		</div>
	
		<div class="clearfix">&nbsp;</div>
	<?php endif; ?>


	<div class="clearfix">&nbsp;</div>
	<div class="grid_12">
		<form action="/admin/groupadd" method="POST">
			<input type="hidden" name="id" value="<?=!empty($this->group->id)?$this->group->id:'';?>" />
			<div class="box form">
				<div class="box_section">
					<h1>Group</h1>
				</div>
				<ul class="uiForm full clearfix">
				
					<li class="uiFormRow clearfix" style="border-radius: 0;">
						<ul>
							<li class="fieldname cc">Name <span style="font-weight: bold; color: #790000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="name" id="name" value="<? if(isset($this->group->name)) echo $this->group->name;?>" class="uiInput<?=($this->fields && in_array("name", $this->fields)?" error":"")?>" style="width: 590px" />
							</li>
						</ul>
					</li>
			
				</ul>
			</div>
		</div>
		<div class="grid_12" style="padding-bottom: 80px; padding-top: 40px;">
			<div class="box form">
				<div class="box_section">
					<h1>Cities</h1>
				</div>

				<table style="width: 100%;">

					<?
						$row=true;
						$counter = 0;
						
						echo "<tr>";

					?>

					<select name="city[]" id="citylist" multiple="multiple">
					
						<option value=""></option>
						<?php foreach($this->cities as $city): ?>
							<option value="<?php echo $city->id;?>" <?php if (in_array($city->id, $this->group_cities)) echo "selected";?>>
							<?php 
								echo $city->name;
								if(isset($city->state) && !empty($city->state)) echo ", " . $city->state;
							?>
							</option>
						<?php endforeach; ?>
				
					</select>

				<?echo "</tr></table>";?>
			</div>
			<div class="clearfix">
				<? if(isset($this->group->id) && ($this->group->id)) :?>
					<input type="submit" name="submitted" class="uiButton large right" value="Save">
				<? else : ?>
					<input type="submit" name="submitted" class="uiButton large right" value="Add">
				<? endif; ?>
			</div>
		</form>
	</div>

<style type="text/css">
	.ms-container {
		background: transparent url('/js/multi-select/img/switch.png') no-repeat 470px 110px;
		padding-left: 100px;
		padding-top: 30px;
	}

	/* multiselect plugin error selector */
	.ms-container .ms-list.invalid {
		border-color: rgba(255, 89, 112, 0.8);
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 89, 112, 0.6);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 89, 112, 0.6);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 89, 112, 0.6);
	}
	
	.uiInput.invalid {
		border-color: #FF9F99;
		-moz-box-shadow: 0px 0px 10px #FF9F99;
		-webkit-box-shadow: 0px 0px 10px #FF9F99;
		box-shadow: 0px 0px 10px #FF9F99;
	}
</style>
			
<script src="/js/multi-select/js/jquery.multi-select.js" type="text/javascript"></script>

<script language="javascript">
$(document).ready(function(){
	$('#citylist').multiSelect();
	$('#grouplist').multiSelect();
	
	// Validate required fields before submit
	$('input[name=submitted]').click(function () {
		var formValid = true;

		// Check the group name
		if ($.trim($('#name').val()) == '') {
			
			// Outline the input field and focus on it
			$('#name').addClass('invalid').trigger('select', 'focus');
			formValid = false;
		}
		
		// Validate city list block
		try {
			$('#citylist').val().length;
			$('#ms-citylist .ms-selection .ms-list').removeClass('invalid');
		} catch (e) {
			formValid = false;
			$('#ms-citylist .ms-selection .ms-list').addClass('invalid');
		}
		
		if (!formValid) {

			// Reset red borders after 3 seconds
			setTimeout(function () {
				$('#ms-citylist .ms-selection .ms-list, #name').removeClass('invalid');
			}, 3000);
		}
		
		return formValid;
	});
});
</script>	
