<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">
		
		<?php if(!empty($this->message)): ?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
		<?php endif; ?>

		<div class="clear"></div>

		<span style="float: right;">
			<a class="uiButton" href="/content/edit/0" style="float: right; color: #FFFFFF" id="new_manual_blast">New Content Blast</a>
		</span>
		
		<table style="width: 100%; border-bottom: 0px;">
		<tr>
			<td style="border-bottom: 0px;">
				<h1>
					<?= ucfirst($this->view_type) ?> <?php if ($this->type == Listing::TYPE_LISTING) : ?>listings<?php elseif ($this->type == Listing::TYPE_CONTENT) : ?>content<?php else : ?>listings and content<?php endif; ?> (<?=number_format($this->paginator->getTotalItemCount());?>)
				</h1>
			</td>
			<td style="text-align: right;border-bottom: 0px;">			
				<select name="type" id="type" class="type_status" style="font-size: 26px; font-weight: bold; margin-right: 10px;">
					<?php
						$values = array(
							'ALL'     => 'ALL', 
							Listing::TYPE_LISTING => Listing::TYPE_LISTING, 
							Listing::TYPE_CONTENT => Listing::TYPE_CONTENT,
							Listing::TYPE_IDX => Listing::TYPE_IDX,
						);
					?>
					<?php foreach ($values as $value => $label) :?>
							<option value="<?= $value ?>"<?= ($this->type == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		</table>
	
		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>ID</th>
				<th>Created</th>
				<?php if ($this->memberIs('superadmin')) : ?><th>Member</th><?php endif; ?>
				<th>PubCity</th>
				<th>Address</th>
				<th>Image</th>				
				<th>Message</th>
				<th style="text-align: center;">Actions</th>
			</tr>

			<? 

			$row=true;
			foreach($this->paginator as $listing)
			{
				echo "<tr ";
					if($row == true)
					{						
						$row = false;
						echo "class='odd' ";	
					}
					else
					{
						$row = true;	
						echo "class='' ";	
					}		

				echo ">";
				
				
				echo "<td style='vertical-align: top;'><a href='/listing/update/".$listing->id."' style='text-decoration: none;'>". $listing->id."</a></td>";

				if(isset($listing->created_at) && !empty($listing->created_at)) echo "<td style='vertical-align: top; color: #a2a2a2'>". $listing->created_at->format('M-d-Y H:i')."</td>";
				else echo "<td style='vertical-align: top; color: #a2a2a2'>&nbsp;</td>";
			
				if ($this->memberIs('superadmin')) {
					if ($listing->member)
					{ 			
						$member = $listing->member;
						echo "<td style='vertical-align: top;'><a href='/admin/member/".$listing->member_id."' style='color: #3D6DCC;'>";
						if(isset($member->first_name)) echo $member->first_name . " ";
						if(isset($member->last_name)) echo $member->last_name;
						echo "</a></td>";
					}
					else echo "<td><a href='/listing/set_member/".$listing->id."'>Set member</a></td>";
				}
				
				echo "<td style='vertical-align: top;'>";
				
					if($listing->isContent())
					{
						//SEARCH for all the CITIES that this content is PUBLISHED in
					}
					else
					{
						if(isset($listing->city_id) && !empty($listing->city_id) && isset($this->cities_array[$listing->city_id]) && ($this->cities_array[$listing->city_id])) echo "<a href='/listing/update/".$listing->id."' style='text-decoration: none;'>".$this->cities_array[$listing->city_id]."</a>";
						else echo "<a href='/listing/update/".$listing->id."' style='color: #790000;'>Unknown City</a>";
					}
					
				echo "</td>";

				echo "<td style='vertical-align: top;'><a href='/listing/update/".$listing->id."' style='text-decoration: none;'>";
				if(!empty($listing->street)) echo $listing->street . "<br />";
				if(!empty($listing->city)) echo $listing->city . " ";
				if(!empty($listing->postal_code)) echo $listing->postal_code . " ";

				if(!empty($listing->price)) echo "<br>PRICE:". $listing->price;

				echo "</a></td>";


				echo "<td style='vertical-align: top;'>";
				if(!empty($listing->image))
				{
					echo "<a href='/listing/update/".$listing->id."' style='text-decoration: none;'><img src='".$listing->getSizedImage(76, 76)."' width='50' height='50' border='0'/></a>";
				}
				echo "</td>";
				
				$message = null;
				$message = stripslashes(strip_tags($listing->message));
				
				
				//Need to add a "on mouse over" that pops the full message here
				if($listing->isContent())
				{
					echo "<td style='vertical-align: top; font-size: 9px;'><a href='/content/edit/".$listing->id."' style='text-decoration: none; color: #a2a2a2'>" . substr($message,0,50) . " ...</a></td>";
				}
				else
				{
					echo "<td style='vertical-align: top; width: 250px; font-size: 9px;'><a href='/listing/update/".$listing->id."' style='text-decoration: none; color: #a2a2a2'>" . substr($message,0,50) . " ...</a></td>";
				}
				
				echo '</td><td>';
				
				if ($this->memberIs('superadmin')) {
					echo '<a href="/listing/update/' . $listing->id . '">Edit</a> | ';
					echo '<a href="/admin/gethopper/' . $listing->id . '?redirect=/admin/pending">Approve</a>';
				} elseif ($this->view_type == 'pending') {
					echo '<a href="/listing/update/' . $listing->id . '">Edit</a> | ';
					echo '<a href="/listing/approve/' . $listing->id . '?redirect=/admin/pending">Approve</a>';
				} elseif ($this->view_type == 'approved') {
					echo '<a href="/listing/unapprove/' . $listing->id . '?redirect=/admin/approved">Unapprove</a>';
				}
				echo '</td></tr>';

			}

		?>
		
		
		
				
		</table>


		<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />


	</div>



</div>





		


<script type="text/javascript">
$(document).ready(function()
{
	$('.approve-blast').click(function(event)
	{
		event.preventDefault();

		$.colorbox({width:"550px", height: "300px", inline:true, href:"#hopers-popup"});

		var buttonHref	=   $(this).attr("href");
		$("#hopers-popup button").bind("click",function(ev)
		{
			window.location =	buttonHref+"/"+$("#inventory_unit_id option:selected").val()+"/"+$("#hopper_id option:selected").val();
		});
	});

	$('.delete-listing').click(function(event)
	{
		$.colorbox({width:"650px", height: "380px;",  href:"/admin/deleteconfirm/type/listing/?target=" + encodeURIComponent( $(this).attr('href') ) });
		event.preventDefault();
	});

	$("#status").change(function (eventObject)
	{
		window.location.href = "/admin/listingstatus/" + $("#status option:selected").val();
	});

	$("#type").change(function (eventObject)
	{
		window.location.href = "/admin/listingtype/"+$("#type option:selected").val();
	});


	$('.filter_open').click(function(e){
		var $this = $(this);
		var $AdvanceSearch = $('#advancedSearch');

		if($this.hasClass('close')){
			$AdvanceSearch.hide();
			$this.removeClass('close');
			$this.parent().addClass('roundit');
		}else{
			$AdvanceSearch.show();
			$this.addClass('close');
			$this.parent().removeClass('roundit');
		}
	});



	//autocomplete
	$('#search_city').autocomplete({
		//appendTo: '#navigation_autocomplete',
		source: "/city/autocomplete",
		minLength: 2,

		select: function( event, ui ) {
			if(ui.item){//alert(ui.item.toSource())
				$('#city_id').val(ui.item.id);
			}
		},
		change:function(event, ui)
		{
			if(!ui.item){
				$('#city_id').val('');
				$(this).val('');
			}
		}

	}).focus(function() {
		if ($(this).val().search(/Select Your City/) != -1) {
			$(this).val('');
		}
	}).blur(function() {
		if ($(this).val() == '') {
			//$(this).val('Select Your City');
		}
	});

});
</script>

<style type="text/css">

/* UI Form */
ul.uiForm, 
ul.uiForm li, 
ul.uiForm li ul, 
ul.uiForm li ul li {
	padding: none!important;
	margin: none!important;
	list-style-type: none!important;
}
ul.uiForm {
	margin: 0 -20px;
	width: 1000px;
}
ul.uiForm.small {
	width: 660px;
}
.uiForm ul, .uiForm li {
	float: left;
	margin: 0px;
}

.uiForm > li{
	float: none;
}

ul.uiForm li ul li ul {
	margin-left: 2em;
	margin-bottom: 1.5em;
}
ul.uiForm li ul li ul li {
	float: none;
	list-style-type: square!important;
	margin-bottom: .5em;
}

.uiFormRowHeader {
	font-size: 21px;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-weight: 600;
	line-height: 26px;
	text-shadow: 1px 1px 0px #fff;
	color: #000;
	margin: 0 0 -1px 0!important;
	padding: 20px;
	width: 960px;
	border-top: solid 1px #fff;
	border-bottom: solid 1px #e5e5e5;
	background-color: #f2f2f2;
	background-image: -moz-linear-gradient(top, #d9d9d9, #f2f2f2); /* FF3.6 */
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #d9d9d9),color-stop(1, #f2f2f2)); /* Saf4+, Chrome */
	background: linear-gradient(#d9d9d9, #f2f2f2);
	-pie-background: linear-gradient(#d9d9d9, #f2f2f2);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm.small .uiFormRowHeader {
	padding: 20px;
	width: 620px;
}
.uiForm .uiFormRow {
	width: 1000px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .uiFormRow {
	width: 660px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm .uiFormRow.last {
	-moz-border-radius: 0 0 8px 8px;
	-webkit-border-radius: 0 0 8px 8px;
	border-radius: 0 0 8px 8px;
	margin-bottom: -20px;
}
.uiForm .info {
	line-height: 20px;
	width: 1000px;
	padding: 10px;
	float: left;
}
.uiForm.small .info {
	width: 660px;
}
.uiForm .buttons {
	width: 960px;
	padding: 10px 20px;
	float: left;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .buttons {
	width: 620px;
}
.uiForm .buttons .uiButton {
	margin-left: 5px;
}
.uiForm .fieldname {
	width: 310px;
	padding: 12px 20px 10px;
	float: left;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-size: 16px;
	font-weight: 400;
	color: #333333;
}
.uiForm.small .fieldname {
	width: 210px;
}
.uiForm .field {
	width: 610px;
	padding: 12px 20px 10px;
	float: left;
}
.uiForm.small .field {
	width: 370px;
}
.uiForm .field span.label {
	float: left;
	margin: 0 20px 0 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiForm .fieldhelp {
	font-size: 12px!important;
	font-weight: normal!important;
	line-height: 18px!important;
	display: block!important;
	margin-top: 4px;
	color: #666666;
}
.uiFormFooterMessage {
	float: left;
	margin-top: 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiFormFooterMessage strong {
	color: #333;
}

ul.normal, ol.normal {
	margin: 0px!important;
}
ul.normal li, ol.normal li {
	line-height: 24px!important;
	margin-left: 10px!important;
}

.uiForm .upsell {
	width: 960px;
	padding: 20px;
	background-color: #252525;
	border-top: solid 1px #fff;
	background-image: url(/images/upsell_new_bg.png);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm .listonly {
	width: 960px;
	padding: 0 20px;
	height: 60px;
	background: #f2f2f2;
	border-top: solid 1px #999;
	-moz-box-shadow: 0 0 20px #cccccc inset;
	-webkit-box-shadow: 0 0 20px #cccccc inset;
	box-shadow: 0 0 20px #cccccc inset;
	position: relative;
	behavior: url(/css/PIE.htc);
}

/* Search Box Styles starts*/

.search_title{
	padding: 5px 10px 5px 13px;
	margin: 0 0px 0px 0px;
	background-color: #4D4D4D;
	background-image: -moz-linear-gradient(top, #666, #333);
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #666),color-stop(1, #333));
	background: linear-gradient(#666, #333);
	-pie-background: linear-gradient(#666666, #333333);
	position: relative;
	behavior: url(/css/PIE.htc);
	-moz-box-shadow: 0 1px 0 #000;
	-webkit-box-shadow: 0 1px 0 #000;
	box-shadow: 0 1px 0 #000;
	-moz-border-radius: 8px 8px 0 0 ;
	-webkit-border-radius: 8px 8px 0 0 ;
	border-radius: 8px 8px 0 0;
	position: relative;
	behavior: url(/css/PIE.htc);
	z-index: 1;
	font-family: Helvetica, Arial, Verdana, sans-serif;
	color: #FFF;
	font-weight: normal;
}

.search_title.roundit{
	border-radius: 8px 8px 8px 8px;
	margin-bottom: 20px;
}

.filter_open{
	background: url(/images/filter_opener.png) no-repeat;
	position: absolute;
	top: 1px;
	right: 10px;
	display: block;
	height: 30px;
	width: 30px;
	cursor: pointer;
}

.filter_open.close{
	background-position: 0 -30px;
}

#advancedSearch{
	display: none;
}

#advancedSearch ul.uiForm{
	margin: 0px;
}
<? /*
#advancedSearch ul.uiForm, #advancedSearch .uiForm .uiFormRow{
	width: 918px;
} */ ?>

#advancedSearch .uiForm > li{
	float: left;
}

#advancedSearch .uiFormRow, #advancedSearch .form {
	background: none !important;
}

#advancedSearch .uiFormRow{
	border-bottom: solid 1px #EDEDED;
}
#advancedSearch .fieldname {
	padding: 18px 20px 19px !important;
	background-color: #FBFBFB;
	border-right: solid 1px #EDEDED;
}
	
#advancedSearch .li_235 {
	width: 235px !important;
}

#advancedSearch .li_180 {
	width: 180px !important;
}

#advancedSearch .li_160 {
	width: 160px !important;
}

#advancedSearch .uiSelectWrapper{
	padding-bottom: 8px;
}

.uiFormRow li.field{
	padding: 10px 0px 5px 10px;
}

.uiFormRow li.fieldname{
	line-height: inherit;
}

</style>
