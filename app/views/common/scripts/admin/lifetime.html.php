<div class="container_12 clearfix">

	<?php if(!empty($this->message)): ?>
		<div class="grid_12">
			<div class="notice">
				<h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
		</div>
	<?php endif; ?>



	<div class="grid_12" style="padding-top: 30px;">
		<h1>Lifetime Value<span style="float:right"><?php echo "&#36;".$this->avg_amount.' / '.$this->avg_days.' days'; ?></span></h1>
	</div>


	<div class="grid_12" style="padding-bottom: 80px;">

		<table style="width: 100%;" class="uiDataTable lifetimeTable">
			<thead>
				<tr>
					<th>ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th><a href="/admin/lifetime/sort/total_payment/by/<?php echo $this->sort == 'total_payment' ? $this->by : 'desc';  ?>">Total payment</a></th>
					<th><a href="/admin/lifetime/sort/days_as_member/by/<?php echo $this->sort == 'days_as_member' ? $this->by : 'desc';  ?>">Days as member</a></th>
					<th><a href="/admin/lifetime/sort/days_as_customer/by/<?php echo $this->sort == 'days_as_customer' ? $this->by : 'desc';  ?>">Days as customer</a></th>
				</tr>
			</thead>

			<tbody>
			<?php foreach($this->paginator as $group) { ?>
			<tr>
				<td style='vertical-align: top;'><?php echo $group->id ?></td>
				<td style='vertical-align: top;'><?php echo $group->first_name ?></td>
				<td style='vertical-align: top;'><?php echo $group->last_name ?></td>
				<td style='vertical-align: top;'><?php echo $group->email ?></td>
				<td style='vertical-align: top;'><?php echo $group->amount ?></td>
				<td style='vertical-align: top;'><?php echo $group->as_member ?></td>
				<td style='vertical-align: top;'><?php echo $group->as_customer ?></td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
		<div class="grid_12" style="margin-top: 0px;">
			<div style="margin-top: 0px; text-align: right; float: right;">
				<?php
				if (count($this->paginator) > 0) {
					$paginationParams = array('extraParams'=>array());
					echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $paginationParams);
				}
				?>
			</div>
		</div>
	</div>
</div>