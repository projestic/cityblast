<div class="container_12 clearfix">
	
		
	<?php if(!empty($this->message)): ?>
		<div class="grid_12" style="border: 1px solid #790000; background-color: #f9ad81; margin-top: 22px; margin-bottom: 22px;">
		   	<?php echo $this->message;?>
		</div>
	<?php endif; ?>
	
	
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 10px; text-align: right; ">
	<?/*<div class="grid_12" style="margin-top: 25px; margin-bottom: 30px; border: 1px solid #ff00ff; ">*/?>
		
		<?php
				
			if(isset($this->selectedCityObject) || !empty($this->selectedCityObject))
			{
				$num_hoppers = $this->selectedCityObject->number_of_hoppers;
			}
			else{
				$num_hoppers = NUMBER_OF_HOPPERS;
			}			
		?>
		<select style="width: 350px; font-size: 30px; font-weight: bold;" id="hopper" name="hopper" onChange="changeHopper(this.value);">
			<?php
			$cuHopper = $this->currenthopper;
			for($i=1;$i<=$num_hoppers;$i++)
			{
			?>
				<option value="<?php echo $i;?>" <?php echo ($cuHopper==$i) ? 'selected' : ''; ?>>
					
					Hopper (<?php echo $i;?>)  
					
					<?
						if($i == 1) echo " National";
						elseif ($i == 2) echo " Regional";
						elseif ($i == 3) echo " Local";
						elseif ($i == 4) echo " Franchise";
						elseif ($i == 5) echo " Broker";
					?>					
					
				</option>
			<?php
			}
			?>
		</select>
		
	</div>
	
		
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 10px;">
		<h4 class="search_title roundit">
			Search
			<span class="filter_open"></span>
		</h4>
		<div id="advancedSearch" class="form clearfix" style="margin-bottom: 25px;">
			

			<form style="border: 1px solid #ccc; border-radius:0 0 10px 10px;" action="" method="POST" id="extraForm" name="extraForm">

				<input type="hidden" name="blast_type" id="blast_type" />

				<ul class="uiForm full clearfix">
					

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">ID</li>
							<li class="field">
								<input type="text" name="id" id="id" tabindex="2" value="<?php echo isset($this->id) ? $this->id : '' ?>" class="uiInput" style="width: 120px" />
							</li>

						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Listing ID</li>
							<li class="field">
								<input type="text" name="listing_id" id="listing_id" tabindex="2" value="<?php echo isset($this->listing_id) ? $this->listing_id : '' ?>" class="uiInput" style="width: 120px" />
							</li>

						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Message</li>
							<li class="field">
								<input type="text" name="listing_message" id="listing_message" tabindex="2"  value="<?php echo isset($this->listing_message) ? $this->listing_message : '' ?>" class="uiInput" style="width: 560px" />
							</li>

						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">URL</li>
							<li class="field">
								<input type="text" name="content_link" id="content_link" tabindex="2"  value="<?php echo isset($this->content_link) ? $this->content_link : '' ?>" class="uiInput" style="width: 560px" />
							</li>

						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180" style="border-top-left-radius: 10px;">City</li>

							<li class="field">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select class="uiSelect" name="city_id" id="city_id">
											<option value="">All Cities</option>
											<?php foreach($this->cities as $city): ?>
												<option value="<?php echo $city->id;?>" <?php echo (!empty($this->city_id) && $this->city_id == $city->id) ? "selected" : "";?>><?php echo $city->name . ", " . $city->state;?></option>
											<?php endforeach; ?>
										</select>
									</span>
								</span>
							</li>
						</ul>
					</li>


					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Street Address</li>
							<li class="field li_235">
								<input type="text" name="street" id="street" tabindex="2" value="<?php echo isset($this->street) ? $this->street : '' ?>" class="uiInput validateNotempty" style="width: 220px" />
							</li>
							<li class="fieldname right li_160">Postal/Zip Code</li>
							<li class="field li_160">
								<input type="text" name="postal" id="postal" tabindex="3" value="<?php echo isset($this->postal) ? $this->postal : '' ?>" class="uiInput validateNotempty" style="width: 92px" />
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Price Range</li>
							<li class="field li_235">
								<span style="float: left; line-height: 30px; margin-top: 2px; width: 18px; font-weight: bold; font-size: 16px;">$</span>
								<input type="text" name="price_range_low" id="price" value="<?php if(isset($this->price_range_low)) echo $this->price_range_low ?>" class="uiInput validateCurrency" style="width: 157px" />
							</li>
							<li class="fieldname right" style="width: 10px; background: none; border: none;">To</li>
							<li class="field li_235">
								<span style="float: left; line-height: 30px; margin-top: 2px; width: 18px; font-weight: bold; font-size: 16px;">$</span>
								<input type="text" name="price_range_high" id="price" value="<?php if(isset($this->price_range_high)) echo $this->price_range_high ?>" class="uiInput validateCurrency" style="width: 157px" />
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Property Type</li>
							<li class="field li_235">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="property_type_id" id="property_type_id" class="uiSelect validateNotZero" style="width: 190px" tabindex="5">
											<option value="0">Please select...</option>
											<option value="1" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 1) ? 'selected="true"' : '' ?>>Detached</option>
											<option value="2" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 2) ? 'selected="true"' : '' ?>>Semi-detached</option>
											<option value="3" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 3) ? 'selected="true"' : '' ?>>Freehold Townhouse</option>
											<option value="4" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 4) ? 'selected="true"' : '' ?>>Condo Townhouse</option>
											<option value="5" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 5) ? 'selected="true"' : '' ?>>Condo Apartment</option>
											<option value="6" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 6) ? 'selected="true"' : '' ?>>Coop/Common Elements</option>
											<option value="7" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 7) ? 'selected="true"' : '' ?>>Attached/Townhouse</option>
										</select>
									</span>
								</span>
							</li>
							<li class="fieldname right li_160">Number of Bedrooms</li>
							<li class="field li_235">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="number_of_bedrooms" id="number_of_bedrooms" class="uiSelect validateNotempty" style="width: 190px" tabindex="6">
											<option value="">Please select...</option>
											<?php
												$bedroom_options	= array("None", 1, 2, 3, 4, "5+");
												$this->number_of_bedrooms	= ($this->number_of_bedrooms > count($bedroom_options)) ? count($bedroom_options) : $this->number_of_bedrooms;
											?>
											<?php foreach ($bedroom_options as $key => $value): ?>
											<option value="<?php echo $key ?>" <?php echo (!empty($this->number_of_bedrooms) && $this->number_of_bedrooms == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
											<?php endforeach ?>
										</select>
									</span>
								</span>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Number of Bathrooms</li>
							<li class="field li_235">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="number_of_bathrooms" id="number_of_bathrooms"  class="uiSelect validateNotempty" style="width: 190px" tabindex="7">
											<option value="">Please select...</option>
											<?php
												$bathroom_options	= array("None", 1, 2, 3, 4, "5+");
												$this->number_of_bathrooms	= ($this->number_of_bathrooms > count($bathroom_options)) ? count($bathroom_options) : $this->number_of_bathrooms;
											?>
											<?php foreach ($bathroom_options as $key => $value): ?>
											<option value="<?php echo $key ?>" <?php echo (!empty($this->number_of_bathrooms) && $this->number_of_bathrooms == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
											<?php endforeach ?>
										</select>
									</span>
								</span>
							</li>
							<li class="fieldname right li_160">Number of Parking</li>
							<li class="field li_235">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="number_of_parking" id="number_of_parking" class="uiSelect validateNotempty" style="width: 190px" tabindex="8">
											<option value="">Please select...</option>
											<?php
												$parking_options	= array("None", 1, 2, "3+");
												$this->number_of_parking	= ($this->number_of_parking > count($parking_options)) ? count($parking_options) : $this->number_of_parking;
											?>
											<?php foreach ($parking_options as $key => $value): ?>
											<option value="<?php echo $key ?>" <?php echo (!empty($this->number_of_parking) && $this->number_of_parking == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
											<?php endforeach ?>

										</select>
									</span>
								</span>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
						<ul>
							<li class="fieldname li_180" style="border-bottom-left-radius: 10px;">Blast Type</li>
							<li class="field li_235">
								<!--<span class="uiSelectWrapper">
									<span class="uiSelectInner">-->
								<select name="listing_type" id="listing_type" tabindex="9" class="uiInput">

									<option value="">ALL</option>

									<option value="BLAST"<?php echo ($this->listing_type == Listing::TYPE_LISTING) ? " selected " : "" ?>>BLAST</option>

									<option value="MANUAL"<?php echo ($this->listing_type == Listing::TYPE_CONTENT) ? " selected " : "" ?>>MANUAL</option>

									<option value="SMLS"<?php echo ($this->listing_type == Listing::TYPE_IDX) ? " selected " : "" ?>>SMLS</option>

								</select>
									<!--</span>
								</span>-->
							</li>
							<li class="fieldname right li_160">MLS Number</li>
							<li class="field li_235">
								<input type="text" name="mls_number" tabindex="10" value="<? if(isset($this->mls_number)) echo $this->mls_number; elseif(isset($this->mls_number)) echo $this->mls_number;?>" class="uiInput validateNotempty <?=($this->fields && in_array("franchise", $this->fields)?" error":"")?>" style="width: 115px" />
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
						<ul>
							<li class="fieldname li_180" style="border-bottom-left-radius: 10px;">Tags</li>
							<li class="field li_550">
								<table border="0">
										<tr>
										<? 	
											$i = 0;
											foreach ($this->tags_all as $t) {
												$i++;		

												$checked = "";
													foreach ($this->selectedTags as $tag_id)
													{
														if ($tag_id == $t->id)
														{
															$checked = "checked";
															break;
														}
													}
												
												echo "<td><input type='checkbox' class='tagList' name='tags[]' value='{$t->id}' {$checked} /> {$t->name}</td>";
												if ($i == 3)
												{
													$i = 0;
													echo "</tr>\n<tr>";
												}
											} 
										?>
										</tr>
									</table>
							</li>

						</ul>
					</li>
				</ul>
				<input type="submit" name="list_button" id="list_button" class="uiButton right cancel" style="width: 130px; margin-top: 10px; font-size: 26px; height: auto; padding: 5px 15px;" value="Search" />
			</form>


		</div>
	</div>
	

	<div class="grid_12">
	
	
		<table style="width: 100%;">
			<tr>
				<td style='border-bottom: 0px;padding-bottom: 0px; margin-bottom: 0px;'>
					<h2>
						Hopper <?=$this->currenthopper;?>
		
						(<?=number_format($this->publish_queue->getTotalItemCount());?>)
		
						<span style="color: #b7b7b7;">
						- <?php if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo $this->selectedCityObject->name; else echo "ALL CITIES"; ?> 	
						</span>																													
		
						<span style="float: right;">
							<a class="uiButton" href="/content/edit/0" style="float: right; color: #FFFFFF" id="new_manual_blast">New Content Blast</a>
						</span>

					</h2>
										
				</td>
	
			</tr>
		</table>
			
		
		
		<form name="act" id="act" method="post" action="">
		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>&nbsp;</th>
				<th>Pub ID</th>	
				<th>Listing ID</th>						
				<th>City</th>						
				<th>Hopper ID</th>
				<th>Message</th>		

				<th>Max</th>
				
				
				<th>Current</th>						
				
				<? /*
				<th>Post Results</th>
				<th>Click Count</th>
				*/ ?>
				
				<th style="text-align: center" nowrap>Actions</th>
			</tr>
					
				<?php
					$row=true;
					$counter=1;
					$max_priority = 0;	
					$i = $this->currenthopper;
					if(count($this->publish_queue))
					{
						foreach($this->publish_queue as $blast)
						{
							echo "<tr ";
								if($row == true)
								{						
									$row = false;
									echo "class='odd' ";	
								}
								else
								{
									$row = true;	
									echo "class='' ";	
								}		
				
								//if(strlen($blast->listing->message) > 140) echo " style='color: #790000; font-weight: bold; background-color: #FFE6E6' ";
								if(isset($this->hopper_count[$i]) && $counter == $this->hopper_count[$i]) echo " style='background-color: #fefdef; font-weight: bold;' ";
									
								echo ">";
								
								echo "<td><input type='checkbox' class='retire' name='retire[]' value='".$blast->id."'/></td>";
								
								echo "<td><a href='/publishingqueue/update/".$blast->id."'>".$blast->id."</a></td>";
								
								echo "<td>";
								if(isset($this->hopper_count[$i]) && $counter == $this->hopper_count[$i]) echo "*";
								echo $blast->listing_id;
								echo "</td>";
			
								if(!empty($blast->city_id))
								{ 
									echo "<td><a href='/publishingqueue/update/".$blast->id."' style='text-decoration: none;'>";
									
									if(isset($this->cities_array[$blast->city_id]))
									{
										echo $this->cities_array[$blast->city_id];
									}
									else
									{
										echo "<span style='color: #990000;'>unknown</span>";	
									}
									
									echo "</a></td>";
								}
								else  echo "<td><a href='/publishingqueue/update/".$blast->id."' style='text-decoration: none;'>no assigned city</a></td>";
								
	
								echo "<td><a href='/publishingqueue/update/".$blast->id."' style='text-decoration: none;'>".$blast->hopper_id."</a></td>";
			
								
	
								echo "<td style='width: 350px;'>";
							

									$listing = $blast->listing;
									if(!empty($blast) && isset($blast->listing_id) && !empty($listing) && $listing->isContent())
									{
										echo "<a href='/content/edit/".$blast->listing_id."' style='text-decoration: none;'>";
									}
									else
									{
										echo "<a href='/listing/update/".$blast->listing_id."' style='text-decoration: none;'>";
									}
								
									
									if(isset($blast->listing_id) && !empty($blast->listing->image))
									{
										echo "<img src='".$blast->listing->getSizedImage(76,76)."' width='30' height='30' style='float: left; padding: 6px;'/>";
									}
									if(isset($blast->listing_id) && !empty($blast->listing->message)) echo $blast->listing->message;
									
								echo "</a></td>";


								echo "<td>". number_format($blast->max_inventory_threshold)."</td>";
								
								
								if($blast->current_count) echo "<td><a href='/admin/posts/".$blast->listing_id."/".$blast->id."'>".number_format($blast->current_count)."</a></td>";
								else echo "<td>". number_format($blast->current_count)."</td>";													
								

								
								//Post count
								/***********************************************
								echo "<td style='text-align: center;'>";
	
								
									/**********************************
									if(isset($this->click_post_counts[$blast->listing->id]))
									{
										echo $this->click_post_counts[$blast->listing->id]['total_post'];
									}
									else{
										echo "0";
									} ********************************
									
	
								echo "</td>";
								
								//Click count
								echo "<td style='text-align: center;'>";
	
								
									/************************************
									if(isset($this->click_post_counts[$blast->listing->id]))
									{
										echo $this->click_post_counts[$blast->listing->id]['total_clicks'];
									}
									else
									{
										echo "0";
									}**********************************
								
	
								echo "</td>";
								*************************************/
								
								echo "<td style='text-align: right;' nowrap>";				
												
								//echo "<a href='/listing/moveup/".$blast->listing->id."/".$blast->hopper_id."/'>mv up</a> | ";
								//echo "<a href='/listing/movedown/".$blast->listing->id."/".$blast->hopper_id."/'>down</a> | ";
				
								//echo "<a href='/listing/createmanualblast/".$listing->hoper_id."/".($listing->hoper_priority+2)."'>Insert After</a> | ";
								if(isset($blast->listing_id) && isset($blast->listing)) echo "<a href='/listing/view/".$blast->listing->id."' target='_new'>preview</a> | ";
			
								echo "<a href='/publishingqueue/update/".$blast->id."'>edit</a> | ";
								echo "<a href='/publishingqueue/delete/".($blast->id)."' onclick='return window.confirm(\"Are you sure you want to remove this listing from the blast queue?\");'>remove</a> | ";
								echo "<a href='/publishingqueue/retire/".($blast->id)."' onclick='return window.confirm(\"Are you sure you want to retire this listing from the blast queue?\");'>retire</a>";
				
				
								echo "<div style='color: #b9b9b9; font-weight: normal;'>";
								echo "testing: ";
								echo "<a href='/test/facebook/".($blast->id)."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Facebook?\");' style='color: #0072BC;'>facebook</a> | ";
								echo "<a href='/test/twitter/".($blast->id)."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Twitter?\");' style='color: #0072BC;'>twitter</a> | ";
								echo "<a href='/test/linkedin/".($blast->id)."' onclick='return window.confirm(\"Are you sure you want test publish this listing to LinkedIn?\");' style='color: #0072BC;'>linkedin</a>";
								echo "</div>";
				
				
								//else echo " ---- ";
								echo "</td>";
				
					
								echo "</tr>\n";
								
								if($blast->priority > $max_priority) $max_priority = $blast->priority;
								$counter++;
						}
					}
					else{
						echo "<tr><td colspan='7'>No scheduled Blasts</td></tr>";
					}
				?>
				
		</table>
		</form>
		
	</div>			
			
	
	<div class="grid_4"><a id="new_manual_blast" style="color: #FFFFFF" href="#" class="uiButton" onClick="checkIfChecked();">Retire All</a></div>
		
	<div class="grid_8"><div  style="float: right;"><?= $this->paginationControl($this->publish_queue, 'Elastic', '/common/pagination.phtml'); ?></div></div>
	



	<div class="grid_12 clearfix" style='padding-bottom: 80px;'>&nbsp;</div>
		

</div>


<script>
function checkIfChecked(){
	var cnt = 0;
	$(".retire").each(
		function(i){
			if($(this).attr("checked"))
				cnt++;
		}
	);

	if(cnt==0){
		alert("Please select any record.")
	}
	else{
		document.getElementById('act').action = '/publishingqueue/retire/';
		document.getElementById('act').submit();
	}
}

function changeHopper(val)
{
	var url = window.location.protocol+"//"+window.location.host + "/admin/index/hopper/" + val;
	window.location = url; 
}

</script>

<script type="text/javascript">
$(document).ready(function()
{
	$('.approve-blast').click(function(event)
	{
		event.preventDefault();

		$.colorbox({width:"550px", height: "300px", inline:true, href:"#hopers-popup"});

		var buttonHref	=   $(this).attr("href");
		$("#hopers-popup button").bind("click",function(ev)
		{
			window.location =	buttonHref+"/"+$("#inventory_unit_id option:selected").val()+"/"+$("#hopper_id option:selected").val();
		});
	});

	$('.delete-listing').click(function(event)
	{
		$.colorbox({width:"650px", height: "380px;",  href:"/admin/deleteconfirm/type/listing/?target=" + encodeURIComponent( $(this).attr('href') ) });
		event.preventDefault();
	});

	$("#status").change(function (eventObject)
	{
		window.location.href = "/admin/listingstatus/" + $("#status option:selected").val();
	});

	$("#type").change(function (eventObject)
	{
		window.location.href = "/admin/listingtype/"+$("#type option:selected").val();
	});


	$('.filter_open').click(function(e){
		var $this = $(this);
		var $AdvanceSearch = $('#advancedSearch');

		if($this.hasClass('close')){
			$AdvanceSearch.hide();
			$this.removeClass('close');
			$this.parent().addClass('roundit');
		}else{
			$AdvanceSearch.show();
			$this.addClass('close');
			$this.parent().removeClass('roundit');
		}
	});



	//autocomplete
	$('#search_city').autocomplete({
		//appendTo: '#navigation_autocomplete',
		source: "/city/autocomplete",
		minLength: 2,

		select: function( event, ui ) {
			if(ui.item){//alert(ui.item.toSource())
				$('#city_id').val(ui.item.id);
			}
		},
		change:function(event, ui)
		{
			if(!ui.item){
				$('#city_id').val('');
				$(this).val('');
			}
		}

	}).focus(function() {
		if ($(this).val().search(/Select Your City/) != -1) {
			$(this).val('');
		}
	}).blur(function() {
		if ($(this).val() == '') {
			//$(this).val('Select Your City');
		}
	});

});
</script>

<style type="text/css">

/* UI Form */
ul.uiForm, 
ul.uiForm li, 
ul.uiForm li ul, 
ul.uiForm li ul li {
	padding: none!important;
	margin: none!important;
	list-style-type: none!important;
}
ul.uiForm {
	margin: 0 -20px;
	width: 1000px;
}
ul.uiForm.small {
	width: 660px;
}
.uiForm ul, .uiForm li {
	float: left;
	margin: 0px;
}

.uiForm > li{
	float: none;
}

ul.uiForm li ul li ul {
	margin-left: 2em;
	margin-bottom: 1.5em;
}
ul.uiForm li ul li ul li {
	float: none;
	list-style-type: square!important;
	margin-bottom: .5em;
}

.uiFormRowHeader {
	font-size: 21px;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-weight: 600;
	line-height: 26px;
	text-shadow: 1px 1px 0px #fff;
	color: #000;
	margin: 0 0 -1px 0!important;
	padding: 20px;
	width: 960px;
	border-top: solid 1px #fff;
	border-bottom: solid 1px #e5e5e5;
	background-color: #f2f2f2;
	background-image: -moz-linear-gradient(top, #d9d9d9, #f2f2f2); /* FF3.6 */
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #d9d9d9),color-stop(1, #f2f2f2)); /* Saf4+, Chrome */
	background: linear-gradient(#d9d9d9, #f2f2f2);
	-pie-background: linear-gradient(#d9d9d9, #f2f2f2);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm.small .uiFormRowHeader {
	padding: 20px;
	width: 620px;
}
.uiForm .uiFormRow {
	width: 1000px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .uiFormRow {
	width: 660px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm .uiFormRow.last {
	-moz-border-radius: 0 0 8px 8px;
	-webkit-border-radius: 0 0 8px 8px;
	border-radius: 0 0 8px 8px;
	margin-bottom: -20px;
}
.uiForm .info {
	line-height: 20px;
	width: 1000px;
	padding: 10px;
	float: left;
}
.uiForm.small .info {
	width: 660px;
}
.uiForm .buttons {
	width: 960px;
	padding: 10px 20px;
	float: left;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .buttons {
	width: 620px;
}
.uiForm .buttons .uiButton {
	margin-left: 5px;
}
.uiForm .fieldname {
	width: 310px;
	padding: 12px 20px 10px;
	float: left;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-size: 16px;
	font-weight: 400;
	color: #333333;
}
.uiForm.small .fieldname {
	width: 210px;
}
.uiForm .field {
	width: 610px;
	padding: 12px 20px 10px;
	float: left;
}
.uiForm.small .field {
	width: 370px;
}
.uiForm .field span.label {
	float: left;
	margin: 0 20px 0 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiForm .fieldhelp {
	font-size: 12px!important;
	font-weight: normal!important;
	line-height: 18px!important;
	display: block!important;
	margin-top: 4px;
	color: #666666;
}
.uiFormFooterMessage {
	float: left;
	margin-top: 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiFormFooterMessage strong {
	color: #333;
}

ul.normal, ol.normal {
	margin: 0px!important;
}
ul.normal li, ol.normal li {
	line-height: 24px!important;
	margin-left: 10px!important;
}

.uiForm .upsell {
	width: 960px;
	padding: 20px;
	background-color: #252525;
	border-top: solid 1px #fff;
	background-image: url(/images/upsell_new_bg.png);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm .listonly {
	width: 960px;
	padding: 0 20px;
	height: 60px;
	background: #f2f2f2;
	border-top: solid 1px #999;
	-moz-box-shadow: 0 0 20px #cccccc inset;
	-webkit-box-shadow: 0 0 20px #cccccc inset;
	box-shadow: 0 0 20px #cccccc inset;
	position: relative;
	behavior: url(/css/PIE.htc);
}

/* Search Box Styles starts*/

.search_title{
	padding: 5px 10px 5px 13px;
	margin: 0 0px 0px 0px;
	background-color: #4D4D4D;
	background-image: -moz-linear-gradient(top, #666, #333);
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #666),color-stop(1, #333));
	background: linear-gradient(#666, #333);
	-pie-background: linear-gradient(#666666, #333333);
	position: relative;
	behavior: url(/css/PIE.htc);
	-moz-box-shadow: 0 1px 0 #000;
	-webkit-box-shadow: 0 1px 0 #000;
	box-shadow: 0 1px 0 #000;
	-moz-border-radius: 8px 8px 0 0 ;
	-webkit-border-radius: 8px 8px 0 0 ;
	border-radius: 8px 8px 0 0;
	position: relative;
	behavior: url(/css/PIE.htc);
	z-index: 1;
	font-family: Helvetica, Arial, Verdana, sans-serif;
	color: #FFF;
	font-weight: normal;
}

.search_title.roundit{
	border-radius: 8px 8px 8px 8px;
	margin-bottom: 20px;
}

.filter_open{
	background: url(/images/filter_opener.png) no-repeat;
	position: absolute;
	top: 1px;
	right: 10px;
	display: block;
	height: 30px;
	width: 30px;
	cursor: pointer;
}

.filter_open.close{
	background-position: 0 -30px;
}

#advancedSearch{
	display: none;
}

#advancedSearch ul.uiForm{
	margin: 0px;
}
<? /*
#advancedSearch ul.uiForm, #advancedSearch .uiForm .uiFormRow{
	width: 918px;
} */ ?>

#advancedSearch .uiForm > li{
	float: left;
}

#advancedSearch .uiFormRow, #advancedSearch .form {
	background: none !important;
}

#advancedSearch .uiFormRow{
	border-bottom: solid 1px #EDEDED;
}
#advancedSearch .fieldname {
	padding: 18px 20px 19px !important;
	background-color: #FBFBFB;
	border-right: solid 1px #EDEDED;
}
	
#advancedSearch .li_235 {
	width: 235px !important;
}

#advancedSearch .li_180 {
	width: 180px !important;
}

#advancedSearch .li_160 {
	width: 160px !important;
}

#advancedSearch .uiSelectWrapper{
	padding-bottom: 8px;
}

.uiFormRow li.field{
	padding: 10px 0px 5px 10px;
}

.uiFormRow li.fieldname{
	line-height: inherit;
}

</style>
