<div class="container_12">

	<? echo $this->render('common/member.dropdown.html.php'); ?>
	

	
	<div class="grid_12" style="margin-bottom: 20px;">	
		<h1><?=count($this->members);?> Members   <?php if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo "(".$this->selectedCityObject->name.")";?></h1>
		<h3>Publishing Assignments for Active Members</h3>
		<table style="width: 960px;">
			<tr>
				<th>Member ID</th>
				<th>Photo/Link</th>
				<th>First Name</th>
				<th>Last Name</th>

				<th>Email</th>
				
				<th>Freq</th>

				<th>Hopper ID</th>
				
				
				<!--<th>TW Enabled</th>-->
				<th>TW Followers</th>

				<!--<th>LIn Enabled</th>-->
				<th>LIn Followers</th>

				<!--<th>FB Enabled</th>-->
				<th>FB Friends</th>

				<th>Total Inv</th>
					
				<th>Actions</th>
			</tr>
			
		<?
			$li_total=0;
			$twitter_total=0;
			$fb_total=0;			
			$row=true;
			
			
			$total_publishers = 0;
			
			$twitter_publishers = 0;
			$facebook_publishers = 0;
			$linkedin_publishers = 0;
			
			//$total_twitter_friends = 0;
			//$total_linkedin_friends = 0;
			//$total_facebook_friends = 0;

			$inventory_array = array();
			$inv_array_type = array();
			$hopper_array = array();
			
			$frequency_array = array();
			$frequency_total = array();
			
			$inv_total = array();
			$hoppers_total = array();
			
			
			
			foreach($this->members as $member)
			{
				$is_publisher = false;
				$member_inv = 0;


				if(!isset($hoppers_total[$member->current_hoper])) $hoppers_total[$member->current_hoper] = 0;
				if(!isset($hopper_array[$member->current_hoper])) $hopper_array[$member->current_hoper] = 0;
				

				if(!isset($inv_array_type['FACEBOOK'])) $inv_array_type['FACEBOOK'] = 0;				
				if(!isset($inv_array_type['TWITTER'])) $inv_array_type['TWITTER'] = 0;	
				if(!isset($inv_array_type['LINKEDIN'])) $inv_array_type['LINKEDIN'] = 0;	
				
				
				if(!isset($frequency_array[$member->frequency_id])) $frequency_array[$member->frequency_id] = array();
				if(!isset($frequency_array[$member->frequency_id]['FACEBOOK'])) $frequency_array[$member->frequency_id]['FACEBOOK'] = 0;				
				if(!isset($frequency_array[$member->frequency_id]['TWITTER'])) $frequency_array[$member->frequency_id]['TWITTER'] = 0;	
				if(!isset($frequency_array[$member->frequency_id]['LINKEDIN'])) $frequency_array[$member->frequency_id]['LINKEDIN'] = 0;	
				
				if(!isset($frequency_total[$member->frequency_id])) $frequency_total[$member->frequency_id] = 0;
				
				$frequency_total[$member->frequency_id]++;
				
				
				echo "<tr ";
					if($row == true)
					{						
						$row = false;
						echo "class='' ";	
					}
					else
					{
						$row = true;	
						echo "class='even' ";	
					}		
				echo ">";
				
				
				echo "<td>".$member->id."</td>";
				
				echo '<td><a href="http://www.facebook.com/profile.php?id='.$member->uid.'" target="_new"><img src="'. ( empty($member->photo) ? 'https://graph.facebook.com/'. $member->uid .'/picture' : CDN_URL . $member->photo ) .'" style="width: 30px; height: 30px;"/></a></td>';
				
				echo "<td>".$member->first_name."</td>";				
				echo "<td>".$member->last_name."</td>";
				
				
				echo "<td><a href='mailto:".$member->email."'>";
				$newstring = substr($member->email,0,30);
				echo $newstring;
				echo "</a></td>\n";
				
				
				echo "<td nowrap>".$member->frequency_id."</td>";
				echo "<td nowrap>".$member->inventory_id."</td>";
				echo "<td nowrap>".$member->current_hoper."</td>";				
				

				
				
				echo "<td style='text-align: right; ";
				if(!$member->twitter_publish_flag || $member->twitter_auto_disabled)
				{
					echo " color: #acacac; ";

					if($this->display_type == "INACTIVE")
					{ 
						$twitter_total+=$member->twitter_followers;	
						$member_inv+=$member->twitter_followers;					
					}
				}
				else
				{
					if($this->display_type == "ACTIVE")
					{ 					
						$twitter_publishers++;
						$is_publisher = true;										
						$twitter_total+=$member->twitter_followers;	
						$member_inv+=$member->twitter_followers;
						
						$inv_array_type['TWITTER'] += $member->twitter_followers;	
						$frequency_array[$member->frequency_id]['TWITTER'] += $member->twitter_followers; 
					}
				}
				echo "'>". number_format($member->twitter_followers). "</td>";



				
				echo "<td style='text-align: right;";
				if(!$member->linkedin_publish_flag || $member->linkedin_auto_disabled)
				{ 
					echo " color: #acacac; ";
					if($this->display_type == "INACTIVE")
					{ 
						$li_total+=$member->linkedin_friends_count;
					}					
				}
				else
				{
					$linkedin_publishers++;
					if($this->display_type == "ACTIVE")
					{ 										
						$linkedin_publishers++;	
						$is_publisher = true;				
						$li_total+=$member->linkedin_friends_count;	
						$member_inv+=$member->linkedin_friends_count;

						$inv_array_type['LINKEDIN'] += $member->linkedin_friends_count;
						$frequency_array[$member->frequency_id]['LINKEDIN'] += $member->linkedin_friends_count; 					
					}
				}
				

				echo "'>".number_format($member->linkedin_friends_count) ."</td>";
			


				
				echo "<td style='text-align: right;";
				if(!$member->facebook_publish_flag || $member->facebook_auto_disabled)
				{ 
					echo " color: #acacac; ";
					if($this->display_type == "INACTIVE")
					{ 
						$fb_total+=$member->facebook_friend_count;
					}
				}
				else
				{
					if($this->display_type == "ACTIVE")
					{ 					
						$facebook_publishers++;	
						$is_publisher = true;				
						$fb_total+=$member->facebook_friend_count;	
						$member_inv+=$member->facebook_friend_count;
						
						$inv_array_type['FACEBOOK'] += $member->facebook_friend_count;
						$frequency_array[$member->frequency_id]['FACEBOOK'] += $member->facebook_friend_count;			
					}
				}
												
				echo "'>".number_format($member->facebook_friend_count) ."</td>";

				//echo "<td>".$member->referring_affiliate_id."</td>";
																
				echo "<td nowrap>";				
				echo "<a href='/admin/member/".$member->id."'>Edit</a> ";
				echo "| <a href='/admin/memberposts/".$member->id."'>Posts</a> ";
				echo "| <a href='/admin/emails/bymember/".$member->id."'>Emails</a> ";
				echo "</td>";
								
				echo "</tr>";
				

				
				//$inventory_array += $member_inv;
				$hopper_array[$member->current_hoper] += $member_inv;
				
				if($is_publisher)
				{
					$total_publishers++;
					$inv_total++;
					$hoppers_total[$member->current_hoper]++;
				}	
			}
		
		?>
		
		<tr><td colspan="8">Sub Totals:</td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($twitter_total);?></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($li_total);?></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($fb_total);?></td>
			<td></td>
		</tr>
		<tr>
			<?
				$grand_total = $twitter_total + $fb_total + $li_total;
			?>
			<td colspan="10">Total Reach:</td><td style="text-align: right; font-weight: bold;"><?=number_format($grand_total);?></td>
			<td></td>
		</tr>
		
		</table>
	</div>
	
	
	<div class="grid_12" style="margin-bottom: 20px;">
		<h3>Breakdown by Frequency</h3>
		
		<? ksort($frequency_array); ?>
		<table width="100%">
			<tr>
				<th>Freq ID</th>
				<th>Total Members</th>
				<th style='text-align:right;'>Facebook</th>
				<th style='text-align:right;'>Twitter</th>
				<th style='text-align:right;'>Linked In</th>
				<th style='text-align:right;'>Total Amount</th>
			</tr>
		
			<?
				$_fb_total=0;
				$_tw_total=0;
				$_li_total=0;
				$full_total=0;
				
				foreach($frequency_array as $key => $amount_array)
				{
					echo "<tr><td>".$key."</td>";
					echo "<td><a href='/admin/frequency/".$key."'>".$frequency_total[$key]."</a></td>";
					echo "<td style='text-align:right;'>".number_format($amount_array['FACEBOOK'])."</td>";
					echo "<td style='text-align:right;'>".number_format($amount_array['TWITTER'])."</td>";
					echo "<td style='text-align:right;'>".number_format($amount_array['LINKEDIN'])."</td>";
					
					$amount = $amount_array['FACEBOOK'] + $amount_array['TWITTER'] + $amount_array['LINKEDIN'];
					echo "<td style='text-align:right;'>".number_format($amount)."</td></tr>";	
					
					$_fb_total+=$amount_array['FACEBOOK'];
					$_tw_total+=$amount_array['TWITTER'];
					$_li_total+=$amount_array['LINKEDIN'];
					$full_total+=$amount;
				}
				
				echo "<tr><td>Totals:</td><td>".array_sum($inv_total)."</td><td style='text-align:right;'>".number_format($_fb_total)."</td><td style='text-align:right;'>".number_format($_tw_total)."</td>";
				echo "<td style='text-align:right;'>".number_format($_li_total)."</td><td style='text-align:right;'>".number_format($full_total)."</td></tr>";
			?>
		</table>	
		
		
	
	</div>
	
	
	<?/*
	<div class="grid_6">
		
		<h3>Breakdown by Inventory ID</h3>
		<table width="100%">
			<tr>
				<th>Inv ID</th>
				<th>Total Members</th>
				<th>Facebook</th>
				<th>Twitter</th>
				<th>Linked In</th>
				<th>Total Amount</th>
			</tr>
		
			<?
				$_fb_total=0;
				$_tw_total=0;
				$_li_total=0;
				$full_total=0;
				
				foreach($inventory_array as $key => $amount)
				{
					echo "<tr><td>".$key."</td>";
					echo "<td><a href='/admin/inventory/".$key."'>".$inv_total[$key]."</a></td>";
					echo "<td style='text-align:right;'>".number_format($inv_array_type[$key]['FACEBOOK'])."</td>";
					echo "<td style='text-align:right;'>".number_format($inv_array_type[$key]['TWITTER'])."</td>";
					echo "<td style='text-align:right;'>".number_format($inv_array_type[$key]['LINKEDIN'])."</td>";
					
					echo "<td style='text-align:right;'>".number_format($amount)."</td></tr>";	
					
					$_fb_total+=$inv_array_type[$key]['FACEBOOK'];
					$_tw_total+=$inv_array_type[$key]['TWITTER'];
					$_li_total+=$inv_array_type[$key]['LINKEDIN'];
					$full_total+=$amount;
				}
				
				echo "<tr><td>Totals:</td><td>".array_sum($inv_total)."</td><td style='text-align:right;'>".number_format($_fb_total)."</td><td style='text-align:right;'>".number_format($_tw_total)."</td>";
				echo "<td style='text-align:right;'>".number_format($_li_total)."</td>";
				echo "<td style='text-align:right;'>".number_format($full_total)."</td></tr>";
			?>
		</table>
	</div> */?>
	
	
	<div class="grid_12">
		
		<h3>Breakdown By Hopper ID</h3>
		<table width="100%">
			<tr>
				<th>Hopper ID</th>
				<th>Total Members</th>
				<th>Total Inventory</th>
			</tr>
		
			<?
				$members_total=0;
				$full_total=0;

					foreach($hopper_array as $key => $amount)
					{
						//echo "<tr><td>".$inventory_id."</td>";
						echo "<tr><td>".$key."</td>";
						echo "<td><a href='/admin/hopper/".$key."'>".$hoppers_total[$key]."</a></td>";
						echo "<td style='text-align:right;'>".number_format($amount)."</td></tr>";	
						
						$members_total+=$hoppers_total[$key];
						$full_total+=$amount;
					}
				
				echo "<tr><td>Totals:</td><td>".$members_total."</td><td style='text-align:right;'>".number_format($full_total)."</td></tr>";
			?>
			
		</table>
	</div>
	
	<div class="grid_12" style="height: 60px;">&nbsp;</div>

</div>