<div class="container_12 clearfix">
	
		
	<div class="grid_12"> 

		<?
			echo "<table width='100%' style='margin-top: 30px;'><tr><td>";
			echo "<h2>Publish Manual Content ID #".$this->listing->id . "</h2></td>";
			//echo "<td style='text-align:right; font-size: 22px; font-weight: bold;' nowrap>Current Count: 0</td>";
			echo "</tr></table>";
		?>
		


		<link href="/js/multi-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">

		<script src="/js/multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
		
		<script language="javascript">
		$(document).ready(function(){
			$('#citylist').multiSelect();
			$('#grouplist').multiSelect();
		});
		</script>	

		
		<form action="/admin/publish/<?=$this->listing->id;?>" method="POST">
		<input type="hidden" name="id" value="<?=$this->listing->id;?>" />
	
	


		
		<div style="margin: 20px auto; width: 460px; clear:both">
			<h4 style="margin-top: 0px; margin-bottom: 10px;">Facebook Blast Preview</h4>
			<div class="facebook_preview clearfix">
				<div class="facebook_preview_inner clearfix">
					<? if(isset($_SESSION['member'])):?>
					<img src="https://graph.facebook.com/<?=$_SESSION['member']->uid?>/picture" class="facebook_userpic" id="facebook_userpic" />
					<? else: ?>
					<img src="/images/blank.gif" style="background-color: #f2f2f2;" class="facebook_userpic" id="facebook_userpic" />
					<? endif; ?>
					<div class="facebook_member" id="label_member"><?=$_SESSION['member']->first_name;?> <?=$_SESSION['member']->last_name;?></div>
					<div id="label_message" class="facebook_message">
						<?=$this->listing->message;?>
					</div>
					<div class="facebook_postpic_wrapper" id="facebook_postpic_wrapper">
						<div>
							<? if(isset($this->listing->image) && !empty($this->listing->image)) { ?>
							<img src='<?=$this->listing->getSizedImage(76, 76);?>' width='75' height='75' style='float: left; padding: 6px;' />
							<? } else { ?>
							<img src="/images/blank.gif" width="90" style="background-color: #f2f2f2;" class="facebook_postpic" id="facebook_postpic" />
							<?php } ?>
						</div>
					</div>
					<div class="facebook_postinfo clearfix">
						<div class="facebook_linktext" id="facebook_linktext"><?=$this->listing->content_link;?></div>
						<div class="facebook_baseurl" id="facebook_baseurl">www.cityblast.com</div>
						<div class="facebook_description" id="facebook_description"><?=$this->listing->description;?></div>
					</div>
				</div>
			</div>
		</div>
	
			
	
	
		<? /* ------- CITIES AND GROUPS -------- */ ?>
		

		
		<h2>Cities</h2>
				
		<p>		
			<select name="citylist[]" id="citylist" multiple="multiple">
																																									
				<option value=""></option>
				<?php foreach($this->cities as $city): ?>
					<option value="<?php echo $city->id;?>" <?php if (in_array($city->id, $this->pub_city_list)) echo "selected";?>>
					<?php 
						echo $city->name;
						if(isset($city->state) && !empty($city->state)) echo ", " . $city->state;
					?>
					</option>
				<?php endforeach; ?>
		
			</select>
		
		</p>
											

		<h2>Groups</h2>
				
		<p>
		
			<select name="grouplist[]" id="grouplist" multiple="multiple">
																																									
				<option value=""></option>
				<?php foreach($this->group_list as $group): ?>
					<option value="<?php echo $group->id;?>">
					<?php 
						echo $group->name;
					?>
					</option>
				<?php endforeach; ?>
		
			</select>
		
		</p>
	
	
		<ul class="uiForm clearfix">
	
		    	<li class="uiFormRow clearfix">
			    <ul>
				    <li class="fieldname cc">Hopper ID</li>
				    <li class="field cc" style="padding-top: 20px;">
					    	<select id="hopper_id" name="hopper_id" class="uiInput" style="width: 200px;">
						<option value="">Select A Hopper</option>					    		
						<?
							for($i = 1; $i <= NUMBER_OF_HOPPERS; $i++)
							{		    
								echo '<option value="'. $i .'" ';
								if(isset($this->hopper_id) && $this->hopper_id == $i) echo " selected='selected' ";
								echo '>Hopper #'.$i;
				
								if($i == 1) echo " - National";
								if($i == 2) echo " - Regional";
								if($i == 3) echo " - Local";
								if($i == 4) echo " - Franchise";
								if($i == 5) echo " - Broker";								
								
								echo '</option>';
							}
						?>
						</select>
				    </li>
			    </ul>
		    	</li>	  

			

	
		    	<li class="uiFormRow clearfix">
			    <ul>
				    <li class="fieldname cc">Maximum Inventory Threshold</li>
				    <li class="field cc" style="padding-top: 20px;">
					    <input type="text" name="max_inventory_threshold" id="name" value="<? if(isset($this->max_inventory_threshold)) echo $this->max_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("max_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
				    </li>
			    </ul>
		    	</li>		    		
		    
		</ul>    
								
		<input type="submit" name="submitted" class="uiButton large right" value="Publish">



		</form>



						
	</div>
	
	<div class="grid_12" style="padding-bottom: 60px;">&nbsp;</div>
	
</div>