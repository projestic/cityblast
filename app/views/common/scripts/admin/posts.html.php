<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 10px;">
		
		<? if($this->view_type == "MEMBER") : ?>
		
			

			<table style="width: 100%" class="uiDataTable lineheight">
				<tr>
					<td><h3>Member Status</h3></td>
					<td style="text-align: right; font-size: 14px;"><span style="color: #a7a7a7;">Member Since:</span> 
						<? if(isset($this->member->created_at) && !empty($this->member->created_at)) echo $this->member->created_at->format('M-d-Y');?>
					</td>
				</tr>
			</table>

			
			<?

				echo "<table style='width: 100%; margin-bottom: 25px;' class='uiDataTable lineheight'>";
				
				echo "<tr><th>mID</th><th>Name</th><th>Hopper ID</th><th>FB Strikes</th><th>FB Pub Flag</th><th>Last FB Action</th>";
				echo "<th>TW Strikes</th><th>TW Pub Flag</th><th>Last TW Action</th>";
				echo "<th>LN Strikes</th><th>LN Pub Flag</th><th>Last LN Action</th></tr>";
				
	
				$member = $this->member;
				
				$last_fb = PublishSettings::find_by_member_id_and_publish_type($member->id,"FACEBOOK");
				$last_tw = PublishSettings::find_by_member_id_and_publish_type($member->id,"TWITTER");
				$last_ln = PublishSettings::find_by_member_id_and_publish_type($member->id,"LINKEDIN");
	
	

				echo "<tr><td>".$member->id."</td><td><a href='/admin/member/".$member->id."'>".$member->first_name." " .$member->last_name."</a></td>";
				//echo "<td>".$member->inventory_id."</td>";
				echo "<td>".$member->current_hoper."</td>";
				
				//FACEBOOK				
				echo "<td>".$member->facebook_fails."</td>";
	
				if($member->facebook_auto_disabled) echo "<td>Auto Disabled</td>";
				else echo "<td>-</td>";
												
				if(!empty($last_fb)) echo "<td>".$last_fb->changed_to . " " . $last_fb->change_date->format('Y-M-d H:i') ."</td>";
				else echo "<td>-</td>";
	
	
				//TWITTER
				echo "<td>".$member->twitter_fails."</td>";
	
				if($member->twitter_auto_disabled) echo "<td>Auto Disabled</td>";
				else echo "<td>-</td>";
												
				if(!empty($last_tw)) echo "<td>".$last_tw->changed_to . " " . $last_tw->change_date->format('Y-M-d H:i') ."</td>";
				else echo "<td>-</td>";
	
	
				//LINKED IN
				echo "<td>".$member->linkedin_fails."</td>";
	
				if($member->linkedin_auto_disabled) echo "<td>Auto Disabled</td>";
				else echo "<td>-</td>";
												
				if(!empty($last_ln)) echo "<td>".$last_ln->changed_to . " " . $last_ln->change_date->format('Y-M-d H:i') ."</td>";
				else echo "<td>-</td>";
				
				echo "</tr>";
						
					
				echo "</table>";
			?>	
			


			<h1>Postings for Member #<?=$this->member->id;?> <a href="/admin/member/<?=$this->member->id;?>"><?=$this->member->first_name;?> <?=$this->member->last_name;?></a>
				<span style="float; right;"><a style="float: right;" class="uiButton" href="/publish/testblast/<?=$this->member->id;?>" id="testblast">Test Blast</a></span>
				</h1>
			
					
		<? elseif (!empty($this->paginator) && count($this->paginator)) : ?>
			
			<h1><?php echo $this->total_posts ;?> Posts for Listing #<?=$this->listing_id;?></h1>

			<div class="grid_12" style="border-top: 1px solid #333; border-bottom: 1px solid #333; padding-top: 10px; padding-bottom: 10px; margin-bottom: 20px;">
				<h4>Total Unique Members Who Attempted to Publish: <?=$this->total_unique;?></h4>	
			</div>
					
		<? else : ?>
			<h1>No postings available for Listing #<?=$this->listing_id?></h1>

		<? endif; ?>
		
	</div>
	

		
	<div class="grid_12" style=" margin-bottom: 80px;">
				
		<table style="width: 100%;" class='uiDataTable lineheight'>
			<tr>
				<th>Post ID</th>
				<th>Listing ID</th>
				<th>Member</th>
				<th>Date</th>
				<th>Message</th>
			
				<th>Hopper ID</th>
				<th>FB Friends</th>				
				<th>TW Friends</th>	
				<th>LinkedIn Friends</th>
				
				<th>Clicks</th>
				<th>Likes</th>
				<th>Comments</th>
					
				<th>Manual Publish</th>
			</tr>
			
		<?
			$row=true;
			$fb_total=0;
			$tw_total=0;
			$ln_total=0;
			
			$fb_fail=0;
			$tw_fail=0;
			$ln_fail=0;
			
			$all_pub = array();
			$success_pub = array();
			
			$fb_fail_array = array();
			$tw_fail_array = array();
			$ln_fail_array = array();
			
			

			if(isset($this->paginator))
			{

				foreach($this->paginator as $post)
				{
				
					//$inventory_id 	= $post->member->inventory_id;
					$city_id 			= $post->member->city_id;
					
					echo "<tr ";
						if($row == true)
						{						
							$row = false;
							echo "class='' ";	
						}
						else
						{
							$row = true;	
							echo "class='even' ";	
						}		
	
					echo ">";
					
					
					echo "<td style='vertical-align: top;'>". $post->id."</td>";
					echo "<td style='vertical-align: top;'><a href='/listing/view/".$post->listing_id."' target='_new'>". $post->listing_id."</a></td>";
	
					if(isset($post->member_id) && !empty($post->member_id))
					{ 				
						echo "<td style='vertical-align: top;'>";
						
						if($this->view_type == "MEMBER") echo "<a href='/admin/member/".$post->member_id."'>";
						else  echo "<a href='/admin/memberposts/".$post->member_id."'>"; 
						
						
						if(isset($post->member->first_name)) echo $post->member->first_name . " ";
						if(isset($post->member->last_name)) echo $post->member->last_name;
						echo "</a></td>";
					}
		
					echo "<td style='vertical-align: top;'>". $post->created_at->format('Y-M-d H:i')."</td>";
		
					echo "<td style='vertical-align: top;'>";
						
						$parts = null;
						if(!empty($post->fb_post_id) && $post->fb_post_id && $post->fb_post_id != "EMPTY")
						{
							$parts = explode("_", $post->fb_post_id);
							
							//echo $post->fb_post_id . "<BR>";
							
							if(!empty($parts[0]) && !empty($parts[1]))
							{
								echo "<a href='http://www.facebook.com/".$parts[0]."/posts/".$parts[1]."'>view comment</a>";
								echo "<br/>";
							}
						}
						echo $post->message;
					
					echo "</td>";
					//echo "<td style='vertical-align: top;'>". $post->inventory_id."</td>";
					echo "<td style='vertical-align: top;'>". $post->hopper_id."</td>";
					
					/*
					echo "<td style='vertical-align: top;'>";
					if(isset($post->fb_post_id) && !empty($post->fb_post_id)) echo "FB: ".$post->fb_post_id;
					if(isset($post->twitter_post_id) && !empty($post->twitter_post_id)) echo "TW: ".$post->twitter_post_id;
					echo "</td>";
					*/
					
					//echo "<td style='vertical-align: top;'>". $post->twitter_post_id."</td>";
					
					if(!empty($post->fb_post_id))
					{
						if($post->fb_post_id == "EMPTY")
						{
							echo "<td style='color: #ff0000;text-align: right;'>";
							$fb_fail+=$post->member->facebook_friend_count;	
							$fb_fail_array[] = $post->member_id;
						}
						else
						{
							echo "<td style='text-align: right;'>";	
							$fb_total+=$post->member->facebook_friend_count;
							
							if(isset($success_pub[$post->member_id])) $success_pub[$post->member_id] += 1;
							else $success_pub[$post->member_id] = 1;
						}
						echo number_format($post->member->facebook_friend_count) . "</td>";
					}
					else
					{
						echo "<td></td>";
					}
					
					if(stristr($post->message,"TWITTER"))
					{
						if($post->twitter_post_id)
						{
							echo "<td style='text-align: right;'>";
							$tw_total+=$post->member->twitter_followers;
							
							if(isset($success_pub[$post->member_id])) $success_pub[$post->member_id] += 1;
							else $success_pub[$post->member_id] = 1;
						}
						else
						{
							echo "<td style='color: #ff0000;text-align: right;'>";
							$tw_fail+=$post->member->twitter_followers;	
							
							$tw_fail_array[] = $post->member_id;
							
						}
						echo number_format($post->member->twitter_followers) . "</td>";						
					}
					else
					{
						echo "<td></td>";
					}
						
					if(stristr($post->message,"LINKEDIN"))
					{
						if(stristr($post->message,"SUCCESS"))
						{
							echo "<td style='text-align: right;'>";
							$ln_total+=$post->member->linkedin_friends_count;
							
							if(isset($success_pub[$post->member_id])) $success_pub[$post->member_id] += 1;
							else $success_pub[$post->member_id] = 1;
						}
						else
						{
							echo "<td style='color: #ff0000;text-align: right;'>";
							$ln_fail+=$post->member->linkedin_friends_count;	
							
							$ln_fail_array[] = $post->member_id;
							
						}
						echo number_format($post->member->linkedin_friends_count) . "</td>";						
						
					}	
					else
					{
						echo "<td></td>";
					}

				
					echo "<td style='text-align: right;'>";
						
						echo number_format($post->clicks);
						
					echo "</td>";
					
					echo "<td style='text-align: right;'>".$post->facebook_likes."</td>"; 
					echo "<td style='text-align: right;'>".$post->facebook_comments."</td>";
					
					echo "<td nowrap>";
					if($post->member->facebook_publish_flag) echo "<a href='/publish/facebook/".$post->listing_id."/".$post->member_id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Facebook?\");' style='color: #0072BC;'>facebook</a> | ";
					else echo "<span style='color: #b9b9b9;'>facebook | </span>";
					
					if($post->member->twitter_publish_flag && $post->member->twitter_access_token) echo "<a href='/publish/twitter/".$post->listing_id."/".$post->member_id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Twitter?\");' style='color: #0072BC;'>twitter</a> | ";
					else echo "<span style='color: #b9b9b9;'>twitter | </span>";
					
					if($post->member->linkedin_publish_flag && $post->member->linkedin_access_token) echo "<a href='/publish/linkedin/". $post->listing_id."/".$post->member_id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to LinkedIn?\");' style='color: #0072BC;'>linkedin</a>";
					else echo "<span style='color: #b9b9b9;'>linkedin</span>";
					
					echo "</td></tr>";
					
					if(isset($all_pub[$post->member_id])) $all_pub[$post->member_id] +=1;	
					else $all_pub[$post->member_id] = 1;
				}
				
				echo "<tr><td colspan='6'></td>";
				echo "<td nowrap style='text-align: right;'><span style='color: #ff0000'>".number_format($fb_fail)."</span><br/>".number_format($fb_total)."</td>";
				echo "<td nowrap style='text-align: right;'><span style='color: #ff0000'>".number_format($tw_fail)."</span><br/>".number_format($tw_total)."</td>";
				echo "<td nowrap style='text-align: right;'><span style='color: #ff0000'>".number_format($ln_fail)."</span><br/>".number_format($ln_total)."</td>";
				echo "<td colspan='4' />";
				echo "</tr>";
				
				echo "<tr><td colspan='9' style='text-align: right;'><span style='color: #ff0000'>".number_format($fb_fail + $tw_fail + $ln_fail)."</span><br/>".number_format($fb_total + $tw_total + $ln_total)."</td><td colspan='4' /></tr>";




			
			}
			else
			{
				echo "<tr><td colspan='10'>No Posts on record for this member</td></tr>";
			}

			//echo "<h1>".count($success_pub)."</h1>";

		
		?>
		</table>
		<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />
	</div>

	<? if ($this->view_type == "LISTING" && !empty($city_id) && !empty($inventory_id)) : ?>
	
		<div class="grid_12" style="padding-top: 10px; padding-bottom: 10px; margin-bottom: 50px;">
			
			<h1>Inventory Unit <?=$alpha_array[$inventory_id];?></h1>

			<?
				$conditions = "( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) AND status='active' AND city_id='".$city_id ."' AND inventory_id='".$inventory_id."'";
				$active_members = Member::find('all', array('conditions' => $conditions));				
				 


				foreach($active_members as $amember)
				{
					$active_members_array[] = $amember->id;						
				}

		
				$all = array_keys($all_pub);
				sort($all);		
				
				$success = array_keys($success_pub);
				sort($success);
				
				$failed = array_diff($all, $success);
				
				$non_publish = array_diff($active_members_array, $all);
	
			?>
		
			<div style="position: relative; width: 100%">
				<div style="float: left;"><h4>Failed: <?=count($failed);?> / <?=$this->total_unique;?></h4></div>
				<div style="float: right;"><h4>Success: <?=count($success_pub);?> / <?=$this->total_unique;?></h4></div>
			</div>
			<div class="clearfix"></div>
			<?
				echo "<table style='width: 100%'>";
				
				echo "<tr><th>mID</th><th>Name</th><th>Frequency</th>
				<th>Hopper ID</th><th>FB Fail</th><th>FB Strikes</th><th>FB Pub Flag</th><th>Last FB Action</th>";
				echo "<th>TW Fail</th><th>TW Strikes</th><th>TW Pub Flag</th><th>Last TW Action</th>";
				echo "<th>LN Fail</th><th>LN Strikes</th><th>LN Pub Flag</th><th>Last LN Action</th><th>Manual Publish</th></tr>";
				
				foreach($failed as $member_id)
				{
					$member = Member::find_by_id($member_id);
					
					$last_fb = PublishSettings::find_by_member_id_and_publish_type($member->id,"FACEBOOK");
					$last_tw = PublishSettings::find_by_member_id_and_publish_type($member->id,"TWITTER");
					$last_ln = PublishSettings::find_by_member_id_and_publish_type($member->id,"LINKEDIN");
	
		
					echo "<tr><td>".$member->id."</td><td>";

						//if($this->view_type == "MEMBER") 
						//else  echo "<a href='/admin/member/".$post->member_id."'>";
					
					echo "<a href='/admin/memberposts/".$post->member_id."'>";
					echo $member->first_name." " .$member->last_name."</a></td>";	
					
					if($member->frequency_id == 3) 	echo "<td>Bus</td>";
					elseif($member->frequency_id == 2) echo "<td>Pro</td>";
					elseif($member->frequency_id == 1) echo "<td>Per</td>";
					else 						echo "<td>Unknown</td>";
										
					//echo "<td>".$member->inventory_id."</td>";
					echo "<td>".$member->current_hoper."</td>";
					//FACEBOOK
					if(in_array($member->id, $fb_fail_array))
					{
						echo "<td style='color: #ff0000;'>failed</td>";	
					}
					else
					{
						echo "<td>-</td>";	
					}
					
					echo "<td>".$member->facebook_fails."</td>";
	
					if($member->facebook_auto_disabled) echo "<td>Auto Disabled</td>";
					else echo "<td>-</td>";
													
					if(!empty($last_fb)) echo "<td>".$last_fb->changed_to . " On: " . $last_fb->change_date->format('Y-M-d H:i') ."</td>";
					else echo "<td>-</td>";
	
	
					//TWITTER
					if(in_array($member->id, $tw_fail_array))
					{
						echo "<td style='color: #ff0000;'>failed</td>";	
					}
					else
					{
						echo "<td>-</td>";	
					}
					
					echo "<td>".$member->twitter_fails."</td>";
	
					if($member->twitter_auto_disabled) echo "<td>Auto Disabled</td>";
					else echo "<td>-</td>";
													
					if(!empty($last_tw)) echo "<td>".$last_tw->changed_to . " " . $last_tw->change_date->format('Y-M-d H:i') ."</td>";
					else echo "<td>-</td>";
	
	
					//LINKED IN
					if(in_array($member->id, $ln_fail_array))
					{
						echo "<td style='color: #ff0000;'>failed</td>";	
					}
					else
					{
						echo "<td>-</td>";	
					}
					
					echo "<td>".$member->linkedin_fails."</td>";
	
					if($member->linkedin_auto_disabled) echo "<td>Auto Disabled</td>";
					else echo "<td>-</td>";
													
					if(!empty($last_ln)) echo "<td>".$last_ln->changed_to . " " . $last_ln->change_date->format('Y-M-d H:i') ."</td>";
					else echo "<td>-</td>";


					echo "<td nowrap>";
					if($member->facebook_publish_flag) echo "<a href='/publish/facebook/".$this->blast_id."/".$member->id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Facebook?\");' style='color: #0072BC;'>facebook</a> | ";
					else echo "<span style='color: #b9b9b9;'>facebook | </span>";
					
					if($member->twitter_publish_flag && $member->twitter_access_token) echo "<a href='/publish/twitter/".$this->blast_id."/".$member->id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Twitter?\");' style='color: #0072BC;'>twitter</a> | ";
					else echo "<span style='color: #b9b9b9;'>twitter | </span>";
					
					if($member->linkedin_publish_flag && $member->linkedin_access_token) echo "<a href='/publish/linkedin/". $this->blast_id."/".$member->id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to LinkedIn?\");' style='color: #0072BC;'>linkedin</a>";
					else echo "<span style='color: #b9b9b9;'>linkedin</span>";
					
					echo "</td>";
					
					
					echo "</tr>";
						
				}	
				echo "</table>";



				//echo "<pre>";
				echo "<h1 style='margin-top: 30px;'>Total Known Members: ".count($active_members)."</h1>";						
				
				//echo "<pre>";
				//print_r($active_members);
				//echo "</pre>";
				
				
				echo "<h3>".count($non_publish)." Members Did Not Publish</h3>";
			?>


			<div class="clearfix"></div>
			<?
				echo "<table style='width: 100%'>";
				
				echo "<tr><th>mID</th><th>Name</th><th>Frequency</th>
				<th>Hopper ID</th><th>FB Fail</th><th>FB Strikes</th><th>FB Pub Flag</th><th>Last FB Action</th>";
				echo "<th>TW Fail</th><th>TW Strikes</th><th>TW Pub Flag</th><th>Last TW Action</th>";
				echo "<th>LN Fail</th><th>LN Strikes</th><th>LN Pub Flag</th><th>Last LN Action</th><th>Manual Publish</th></tr>";
				
				foreach($non_publish as $member_id)
				{
					$member = Member::find_by_id($member_id);
					
					$last_fb = PublishSettings::find_by_member_id_and_publish_type($member->id,"FACEBOOK");
					$last_tw = PublishSettings::find_by_member_id_and_publish_type($member->id,"TWITTER");
					$last_ln = PublishSettings::find_by_member_id_and_publish_type($member->id,"LINKEDIN");
	
		
					echo "<tr><td>".$member->id."</td><td><a href='/admin/memberposts/".$member->id."'>".$member->first_name." " .$member->last_name."</a></td>";
					
					if($member->frequency_id == 3) 	echo "<td>Bus</td>";
					elseif($member->frequency_id == 2) echo "<td>Pro</td>";
					elseif($member->frequency_id == 1) echo "<td>Per</td>";
					else 						echo "<td>Unknown</td>";
										
					//echo "<td>".$member->inventory_id."</td>";
					echo "<td>".$member->current_hoper."</td>";
					//FACEBOOK
					if(in_array($member->id, $fb_fail_array))
					{
						echo "<td style='color: #ff0000;'>failed</td>";	
					}
					else
					{
						echo "<td>-</td>";	
					}
					
					echo "<td>".$member->facebook_fails."</td>";
	
					if($member->facebook_auto_disabled) echo "<td>Auto Disabled</td>";
					else echo "<td>-</td>";
													
					if(!empty($last_fb)) echo "<td>".$last_fb->changed_to . " " . $last_fb->change_date->format('Y-M-d H:i') ."</td>";
					else echo "<td>-</td>";
	
	
					//TWITTER
					if(in_array($member->id, $tw_fail_array))
					{
						echo "<td style='color: #ff0000;'>failed</td>";	
					}
					else
					{
						echo "<td>-</td>";	
					}
					
					echo "<td>".$member->twitter_fails."</td>";
	
					if($member->twitter_auto_disabled) echo "<td>Auto Disabled</td>";
					else echo "<td>-</td>";
													
					if(!empty($last_tw)) echo "<td>".$last_tw->changed_to . " " . $last_tw->change_date->format('Y-M-d H:i') ."</td>";
					else echo "<td>-</td>";
	
	
					//LINKED IN
					if(in_array($member->id, $ln_fail_array))
					{
						echo "<td style='color: #ff0000;'>failed</td>";	
					}
					else
					{
						echo "<td>-</td>";	
					}
					
					echo "<td>".$member->linkedin_fails."</td>";
	
					if($member->linkedin_auto_disabled) echo "<td>Auto Disabled</td>";
					else echo "<td>-</td>";
													
					if(!empty($last_ln)) echo "<td>".$last_ln->changed_to . " " . $last_ln->change_date->format('Y-M-d H:i') ."</td>";
					else echo "<td>-</td>";


					echo "<td nowrap>";
					if($member->facebook_publish_flag) echo "<a href='/publish/facebook/".$this->blast_id."/".$member->id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Facebook?\");' style='color: #0072BC;'>facebook</a> | ";
					else echo "<span style='color: #b9b9b9;'>facebook | </span>";
					
					if($member->twitter_publish_flag && $member->twitter_access_token) echo "<a href='/publish/twitter/".$this->blast_id."/".$member->id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to Twitter?\");' style='color: #0072BC;'>twitter</a> | ";
					else echo "<span style='color: #b9b9b9;'>twitter | </span>";
					
					if($member->linkedin_publish_flag && $member->linkedin_access_token) echo "<a href='/publish/linkedin/". $this->blast_id."/".$member->id."' onclick='return window.confirm(\"Are you sure you want test publish this listing to LinkedIn?\");' style='color: #0072BC;'>linkedin</a>";
					else echo "<span style='color: #b9b9b9;'>linkedin</span>";
					
					echo "</td>";

					
					echo "</tr>";
						
				}	
				echo "</table>";

				
			?>
		
		</div>
	
	<? else: ?>
		

		
	<? endif; ?>
	<script>
	function loadPost(page){
		var url = $('.tabs-3').attr('rel');
		var content = $("#tabcontent");		
		content.text('Loading...');
		$.post(url, { page: page},
			function(data) {
				content.html(data);
			}
		);
	}
	</script>
</div>