<?php

$year_from  = null;
$year_to    = null;
$day_from   = null;
$day_to     = null;
$month_from = null;
$month_to   = null;

if ($this->to_date)
{
	$to_arr = explode("-",$this->to_date);
	$day_to	= $to_arr[2];
	$month_to	= $to_arr[1];
	$year_to	= $to_arr[0];
}

if ($this->from_date)
{
	$from_arr = explode("-",$this->from_date);
	
	$day_from		= $from_arr[2];
	$month_from	= $from_arr[1];
	$year_from	= $from_arr[0];
}

/**************
$memCount = 0;
foreach($this->paginator as $member)
{
	++$memCount;		
}	
***************/
?>
<div class="container_12 clearfix">
	
	<div class="grid_12" style="margin-top: 25px; margin-bottom: 25px;">	
		
		<h4 class="search_title roundit">
			Search
			<span class="filter_open"></span>
		</h4>
		
		<div id="advancedSearch" class="form clearfix" style="margin-bottom: 25px;">
			
			<form style="border: 1px solid #ccc; border-radius:0 0 10px 10px;" action="/admin/allmembers" method="POST" id="extraForm" name="extraForm">

				<ul class="uiForm full clearfix">					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_160">Member ID</li>
							<li class="field li_235">
								<input type="text" name="member_id" id="member_id" tabindex="2" value="<?php echo isset($this->member_id) ? $this->member_id : '' ?>" class="uiInput validateNotempty" style="width: 220px" />
							</li>

							<li class="fieldname li_160">Email</li>
							<li class="field li_235">
								<input type="text" name="email" id="email" tabindex="3" value="<?php echo isset($this->email) ? $this->email : '' ?>" class="uiInput validateNotempty" style="width: 220px" />
							</li>


						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>

							<li class="fieldname li_160">First name</li>
							<li class="field li_235">
								<input type="text" name="first_name" id="first_name" tabindex="3" value="<?php echo isset($this->first_name) ? $this->first_name : '' ?>" class="uiInput validateNotempty" style="width: 220px" />
							</li>

							<li class="fieldname li_160">Last Name</li>
							<li class="field li_235">
								<input type="text" name="last_name" id="last_name" tabindex="2" value="<?php echo isset($this->last_name) ? $this->last_name : '' ?>" class="uiInput validateNotempty" style="width: 220px" />
							</li>

						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>

							<li class="fieldname li_160">Affiliate</li>
							<li class="field li_235">
												
								<select name="affiliate_id" id="affiliate_id" style="font-size: 18px; font-weight: bold;width: 200px;">
									<option value="">ALL</option>
									<?
									foreach($this->affiliate_array as $affiliate_id => $aff_name)
									{
										echo "<option value='".$affiliate_id."' ";
										if(isset($this->affiliate_id) && $this->affiliate_id == $affiliate_id) echo " selected='selected' ";
										echo ">".$aff_name."</option>";
									}
									
									?>
								</select>
							</li>

							<li class="fieldname li_160">Promo Code</li>
							<li class="field li_235">
								<select name="promo_id" id="promo_id" style="font-size: 18px; font-weight: bold;width: 200px;">
									<option value="">ALL</option>
									<?php
									if (is_array($this->promo)) {
										foreach ($this->promo as $promo) {
											echo "<option value='".$promo->id."' ";
											if (isset($this->promo_id) && $this->promo_id == $promo->id) echo " selected='selected' ";
											echo ">".$promo->promo_code."</option>";
										}
									}
									?>
								</select>	
							</li>

						</ul>
					</li>


					
					<li class="uiFormRow clearfix" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
						<ul>
							<li class="fieldname right li_160">Membership Type</li>
							<li class="field li_235">

								<select name="membership_type" id="membership_type" style="font-size: 18px; font-weight: bold;">
									<option value="">ALL</option>
									<option value="1" <?php echo ($this->membership_type == "1") ? " selected " : "" ?>>Agent</option>
									<option value="2" <?php echo ($this->membership_type == "2") ? " selected " : "" ?>>Brokerage</option>
								</select>
		
							</li>
							
							<li class="fieldname right li_160">Billing Status</li>
							<li class="field li_235">

								<select name="payment_status" id="payment_status" style="font-size: 18px; font-weight: bold;">
									<option value="">ALL</option>
									<?
										$payment_status_array = array('signup','signup_expired','paid','free','payment_failed','cancelled','extended_trial','refund_issued');
										
										foreach($payment_status_array as $status)
										{
											echo "<option value='".$status."' ";
											if(isset($this->payment_status) && $this->payment_status == $status) echo " selected='selected' ";
											echo ">".$status."</option>";
										}										
									?>
								</select>
		
							</li>
						</ul>
					</li>


					<li class="uiFormRow clearfix" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
						<ul>
							<li class="fieldname li_160" style="border-bottom-left-radius: 10px;">Billing Cycle</li>
							<li class="field li_235">

								<select id="billing_cycle" style="font-size: 18px; font-weight: bold;" name="billing_cycle">
									<option value="">ALL</option>
									<option value="month" <?php echo ($this->billing_cycle == "month") ? " selected " : "" ?>>Monthly</option>
									<option value="biannual" <?php echo ($this->billing_cycle == "biannual") ? " selected " : "" ?>>Biannually</option>
									<option value="annual" <?php echo ($this->billing_cycle == "annual") ? " selected " : "" ?>>Annually</option>
								</select>	

							</li>

							<li class="fieldname right li_160">Creative ID</li>
							<li class="field li_235">

								<select name="creative_id" id="creative_id" style="font-size: 18px; font-weight: bold;">
									<option value="">ALL</option>
									<?php
										if (is_array($this->cid_list)) {
											foreach ($this->cid_list as $creative) {
												echo "<option value='".$creative->creative_id."' ";
												if (isset($this->creative_id) && $this->creative_id == $creative->creative_id) echo " selected='selected' ";
												echo ">".$creative->creative_id."</option>";
											}
										}
									?>

								</select>
		
							</li>
							

						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_160">Franchise</li>
							<li class="field li_235">
								<select name="franchise_id" id="promo_id" style="font-size: 18px; font-weight: bold;width: 200px;">
									<option value="">ALL</option>
									<?
									if(is_array($this->franchise))
									{ 
										foreach($this->franchise as $franchise)
										{
											echo "<option value='".$franchise->id."' ";
											if(isset($this->franchise_id) && $this->franchise_id == $franchise->id) echo " selected='selected' ";
											echo ">".$franchise->name."</option>";
										}
									}
									?>
								</select>
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_160">From</li>
							<li class="field li_235">
								<select name="month_from" id="month_from" class="type_status" style="font-size: 18px; font-weight: bold;">
				<option value="">Month</option>
				<?php

					$months	= array(
						'01' => 'Jan',
						'02' => 'Feb',
						'03' => 'Mar',
						'04' => 'Apr',
						'05' => 'May',
						'06' => 'Jun',
						'07' => 'Jul',
						'08' => 'Aug',
						'09' => 'Sep',
						'10' => 'Oct',
						'11' => 'Nov',
						'12' => 'Dec',
					);

					foreach ($months as $key => $name) {

						echo '<option value="'. $key .'"'. ((int)$key == (int)$month_from ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="day_from" id="day_from" class="type_status" style="font-size: 18px; font-weight: bold; width: 75px">
				<option value="">Day</option>
				<?php

					$days	= range(1, 31);

					foreach ($days as $name) {

						$key	= str_pad($name, 2, '0', STR_PAD_LEFT);

						echo '<option value="'. $key .'"'. ((int)$key == (int)$day_from ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="year_from" id="year_from" class="type_status" style="font-size: 18px; font-weight: bold;">
				<option value="">Year</option>
				<?
					$current_year	= intval(date('Y'));

					$years	= range($current_year, $current_year - 25);

					foreach ($years as $name) {

						echo '<option value="'. $name .'"'. ((int)$name == (int)$year_from ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			</li>
			<li class="fieldname right li_160">To</li>
			<li class="field li_235">

			<select name="month_to" id="month_to" class="type_status" style="font-size: 18px; font-weight: bold;">
				<option value="">Month</option>
				<?php
					$months	= array(
						'01' => 'Jan',
						'02' => 'Feb',
						'03' => 'Mar',
						'04' => 'Apr',
						'05' => 'May',
						'06' => 'Jun',
						'07' => 'Jul',
						'08' => 'Aug',
						'09' => 'Sep',
						'10' => 'Oct',
						'11' => 'Nov',
						'12' => 'Dec',
					);

					foreach ($months as $key => $name) {

						echo '<option value="'. $key .'"'. ((int)$key == (int)$month_to ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="day_to" id="day_to" class="type_status" style="font-size: 18px; font-weight: bold; width: 75px">
				<option value="">Day</option>
				<?php

					$days	= range(1, 31);

					foreach ($days as $name) {

						$key	= str_pad($name, 2, '0', STR_PAD_LEFT);

						echo '<option value="'. $key .'"'. ((int)$key == (int)$day_to ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="year_to" id="year_to" class="type_status" style="font-size: 18px; font-weight: bold;">
				<option value="">Year</option>
				<?
					$current_year	= intval(date('Y'));

					$years	= range($current_year, $current_year - 25);

					foreach ($years as $name) {

						echo '<option value="'. $name .'"'. ((int)$name == (int)$year_to ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

							</li>
						</ul>
					</li>

				</ul>
				<input type="submit" name="list_button" id="list_button" class="uiButton right cancel" style="width: 130px; margin-top: 10px; font-size: 26px; height: auto; padding: 5px 15px;" value="Search" />
			</form>
		</div>
		<div class="clear"></div>
	</div>

</div>



<?php $allowStatus = in_array($this->selected_status, array('threestrikes', 'fbpword', 'twpword', 'expiredtoken')); ?>
<?php $affiliate_array = $this->affiliate_array; ?>

<div class="container_12 clearfix">

	<? echo $this->render('common/member.dropdown.html.php'); ?>

	<form name="3strikes-form" action="/admin/allmembers" method="POST" id="3strikesForm">

	<?
		if($this->selected_status == "expiredtoken")
		{
			echo "<input type='hidden' name='expiredtoken' value='1'/>";
		}
	?>
	<div class="grid_12">

		<?php if (isset($this->strikes)) : ?>
		<h4 align="center" style="color: green;">3 Strikes email has been sent for <?php echo $this->strikes ?> member<?php echo ($this->strikes == 1) ? '' : 's' ?></h4>
		<?php endif ?>


		<?php if (isset($this->expires)) : ?>
		<h4 align="center" style="color: green;">3 Expired Token email has been sent for <?php echo $this->expires ?> member<?php echo ($this->expires == 1) ? '' : 's' ?></h4>
		<?php endif ?>



		<h1>
			
			<?php 
			
				if(isset($this->selected_status) && count($this->paginator) > 0)
				{

					switch ($this->selected_status)
					{

						case "expiredtoken":

							echo number_format($this->paginator->getTotalItemCount()) . " Expired Tokens ";
							break;						

						case "growth":

							echo number_format($this->paginator->getTotalItemCount()) . " Growth ";
							break;
						
						case "active":

							echo number_format($this->paginator->getTotalItemCount()) . " Active Members ";
							break;	

						case "free":

							echo number_format($this->paginator->getTotalItemCount()) . " Free Members ";
							break;

						case "paid":

							echo number_format($this->paginator->getTotalItemCount()) . " Paid Members ";
							break;	

						case "failedpayment":

							echo number_format($this->paginator->getTotalItemCount()) . " Failed Payments ";
							break;
													
						case "threestrikes":

							echo number_format($this->paginator->getTotalItemCount()) . ' " 3 Strikes" Members ';
							break;	
													
						case "inactive":

							echo number_format($this->paginator->getTotalItemCount()) . " Inactive Members ";
							break;	
													
						case "publishing":

							echo number_format($this->paginator->getTotalItemCount()). " Members ";
							break;	
													
						case "publishsettings":
							//echo "---";
							break;	
													
						case "fbpword":

							echo number_format($this->paginator->getTotalItemCount()) . " Members Who Changed Their FB Pword ";
							break;	
													
						case "twpword":

							echo number_format($this->paginator->getTotalItemCount()) . " Members Who Changed Their TW Pword ";
							break;	
													
						case "reach-by-city":

							echo number_format($this->paginator->getTotalItemCount()) . " Total Reach By City ";
							break;	
													
						case "deleted":

							echo number_format($this->paginator->getTotalItemCount()) . " Deleted Members ";
							break;	

						case "cityassignment":

							echo number_format($this->paginator->getTotalItemCount()) . " Missing City ";
							break;	
														
						case "all":

							echo number_format($this->paginator->getTotalItemCount()) . " Total Members";
							break;							
					}
					
				}
					
				if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo "(".$this->selectedCityObject->name.")";
			?>
		</h1>



		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<? if($allowStatus) : ?><th></th><? endif; ?>
				<th>ID</th>
				<!--<th>Photo</th>-->
				<th>Name</th>
			

				<th>Email</th>
				<th>City</th>

				<? if(!isset($allowStatus) || (!$allowStatus) ) : ?><th>Member Since</th><? endif; ?>
				
				<? /*<th>Token Expires</th>*/ ?>
				
				<th style="text-align: right">Price</th>
				
				<th>Status</th>
				<? if(!isset($allowStatus) || (!$allowStatus) ) : ?><th>Next Payment</th><? endif; ?>

				<?/*******
				<!--<th>TW Enabled</th>-->
				<th>TW</th>

				<!--<th>LIn Enabled</th>-->
				<th>LIn</th>

				<!--<th>FB Enabled</th>-->
				<th>FB</th>
				****/
				?>
				
				
				<th style="text-align: right;">Reach</th>

				<th>Aff</th>
				<th style="text-align: right;">CID</th>
				<th>Promo</th>

				<th>Actions</th>
			</tr>

		<?
			$total_reach = 0;
			$total_recur_revenue = 0;
			$row=true;
			
			
			/***************
			$li_total=0;
			$twitter_total=0;
			$fb_total=0;
			
			$total_publishers = 0;

			$twitter_publishers = 0;
			$facebook_publishers = 0;
			$linkedin_publishers = 0;

			//$total_twitter_friends = 0;
			//$total_linkedin_friends = 0;
			//$total_facebook_friends = 0;
			******************/

			foreach($this->paginator as $member)
			{
				if($member->id == 1182 or $member->id == 1183 or $member->id == 1184)
				{
					//echo "Im not EQUAL!";
					continue;
				}

				
				$is_publisher = false;

				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}
				echo ">\n";


				if($allowStatus) echo "<td><input type='checkbox' name='3strikes[]' value='". $member->id ."' /></td>\n";

				echo "<td><a href='/admin/member/".$member->id."'>".$member->id."</a></td>\n";

				//echo '<td>';
				//echo '<a href="http://www.facebook.com/profile.php?id='.$member->uid.'" target="_new"><img src="'. (empty($member->photo) ? 'https://graph.facebook.com/'. $member->uid .'/picture' : $member->photo) .'" style="width: 30px; height: 30px;"/></a>';
				//echo '</td>'."\n";

				echo "<td style='line-height: 15px;'><a href='/admin/member/".$member->id."'>".$member->first_name." ".$member->last_name."</a></td>\n";
				//echo "<td>".$member->last_name."</td>\n";

				echo "<td>";
					
					if($member->email)
					{
						echo "<a href='mailto:".$member->email."'>";
						$newstring = substr($member->email,0,30);
						echo $newstring;
						echo "</a>";
					}
					
				echo "</td>\n";

				echo "<td nowrap>"; 
					if(isset($member->city_id) && isset($this->city_array[$member->city_id])) echo $this->city_array[$member->city_id];
				echo "</td>";

				if(	!isset($allowStatus) || (!$allowStatus)	)
				{ 
					echo "<td nowrap>";				
						//echo $member->created_at->format('M-d-y') . "<BR>";				
						if(isset($member->created_at)) echo $member->created_at->format('M-d-y H:i');
					echo "</td>\n";
				}

				echo "<td nowrap style='text-align: right;'>";
				echo number_format($member->price, 2);
				echo "</td>\n";

				$total_recur_revenue += round($member->price, 2);


				echo "<td nowrap>";
				
					if(isset($member->account_type) && $member->account_type == 'free') echo 'free';
					elseif(isset($member->payment_status)) echo $member->payment_status;
				echo "</td>";

				if(	!isset($allowStatus) || (!$allowStatus)	)
				{ 
					echo "<td nowrap>";
						if(isset($member->account_type) && $member->account_type == 'free') echo 'free';
						elseif(isset($member->next_payment_date) && !empty($member->next_payment_date)) echo $member->next_payment_date->format('M-d-y');
					echo "</td>\n";
				}

				/*******************
				echo "<td style='text-align: right; color: #acacac;'>";
				if($member->twitter_publish_flag)
				{
					echo "<img src='/images/right.jpg' height='10' width='10' />";
				}
				else
				{
					//echo "<img src='/images/cross.jpg' height='10' width='10' />";
					echo " - ";
				}
				echo "</td>";

				//echo "</td>";
				*******************/

				$reach = 0;
				if($member->twitter_publish_flag && !$member->twitter_auto_disabled)
				{
					if($allowStatus || $this->display_type == "ACTIVE")
					{					
						$reach+=$member->twitter_followers;
					}
				}
				if($member->linkedin_publish_flag && !$member->linkedin_auto_disabled)
				{
					if($allowStatus || $this->display_type == "ACTIVE")
					{
						$reach+=$member->linkedin_friends_count;
					}	
				}
				if($member->facebook_publish_flag && !$member->facebook_auto_disabled)
				{				
					if($allowStatus || $this->display_type == "ACTIVE")
					{								
						$reach+=$member->facebook_friend_count;				
					}
				}
				
				$total_reach+=$reach;
				
				
				echo "<td style='text-align: right;'>".number_format($reach)."</td>";

				
				
				/*********************
				echo "<td style='text-align: right; ";
				if(!$member->twitter_publish_flag || $member->twitter_auto_disabled)
				{
					echo " color: #acacac; ";

					if($this->display_type == "INACTIVE")
					{
						$twitter_total+=$member->twitter_followers;
					}
				}
				else
				{
					if($this->display_type == "ACTIVE")
					{
						$twitter_publishers++;
						$is_publisher = true;
						$twitter_total+=$member->twitter_followers;
					}
				}
				echo "'>".number_format($member->twitter_followers) ."</td>\n";
				***********/


				/******************
				echo "<td style='text-align: right; color: #acacac;'>";
				if($member->linkedin_publish_flag)
				{
					echo "<img src='/images/right.jpg' height='10' width='10' />";
				}
				else
				{
					//echo "<img src='/images/cross.jpg' height='10' width='10' />";
					echo " - ";
				}
				echo "</td>";
				*********************/

				/*******************
				echo "<td style='text-align: right;";
				if(!$member->linkedin_publish_flag || $member->linkedin_auto_disabled)
				{
					echo " color: #acacac; ";
					if($this->display_type == "INACTIVE")
					{
						$li_total+=$member->linkedin_friends_count;
					}
				}
				else
				{
					$linkedin_publishers++;
					if($this->display_type == "ACTIVE")
					{
						$li_total+=$member->linkedin_friends_count;
					}
				}


				echo "'>".number_format($member->linkedin_friends_count) ."</td>\n";
				***************/

				/***************
				echo "<td style='text-align: right; color: #acacac;'>";
				if($member->facebook_publish_flag)
				{
					echo "<img src='/images/right.jpg' height='10' width='10' />";
				}
				else
				{
					//echo "<img src='/images/cross.jpg' height='10' width='10' />";
					echo " - ";
				}
				echo "</td>";
				**************/

				/*******************
				echo "<td style='text-align: right;";
				if(!$member->facebook_publish_flag || $member->facebook_auto_disabled)
				{
					echo " color: #acacac; ";
					if($this->display_type == "INACTIVE")
					{
						$fb_total+=$member->facebook_friend_count;
					}
				}
				else
				{
					if($this->display_type == "ACTIVE")
					{
						$facebook_publishers++;
						$is_publisher = true;
						$fb_total+=$member->facebook_friend_count;
					}
				}

				echo "'>".number_format($member->facebook_friend_count) ."</td>\n";
 				***************/




				echo "<td>";
				if(isset($member->referring_affiliate_id) && !empty($member->referring_affiliate_id))
				{ 
					if (isset($affiliate_array[$member->referring_affiliate_id])) {
						echo "<a href='/admin/member/".$member->referring_affiliate_id."'>".$affiliate_array[$member->referring_affiliate_id]."</a>";
					} else { 
						$affiliate = $member->referring_affiliate;
						$affiliate_array[$member->referring_affiliate_id] = $affiliate->owner->first_name;					
						if($affiliate) echo "<a href='/admin/member/".$affiliate->member_id."'>".$affiliate->owner->first_name ."</a>";
						else echo "<a href='/admin/member/".$affiliate->member_id."'>".$affiliate->member_id ."</a>";
					}
				} elseif (isset($member->referring_member_id) && !empty($member->referring_member_id)) { 
					if (isset($affiliate_array[$member->referring_affiliate_id])) {
						echo "<a href='/admin/member/".$member->referring_affiliate_id."'>".$affiliate_array[$member->referring_affiliate_id]."</a>";
					} else { 
						$affiliate = $member->referring_member;
						$member_array[$member->referring_member_id] = $member->referring_member->first_name;					
						if($affiliate) echo "<a href='/admin/member/".$member->referring_member_id."'>".$member->referring_member->first_name ."</a>";
						else echo "<a href='/admin/member/".$member->referring_member_id."'>".$member->referring_member_id ."</a>";
					}
				}
				else echo "&nbsp;";
				echo "</td>\n";

				echo "<td>";
				if(isset($member->creative_id)) echo $member->creative_id;
				else echo "&nbsp;";
				echo "</td>\n";
				
				echo "<td nowrap>";
				if($member->promo_id)
				{
					if($promo = Promo::find($member->promo_id))
					{
						 echo $promo->promo_code;
					}
					else
					{
						echo $member->promo_id;
					}
				}
				echo "</td>";



				echo "<td nowrap style='line-height: 15px;'>";

					//Let's figure out the last time we sent this person an PWORD CHANGE EMAIL
					if($allowStatus)
					{
						if($this->selected_status == "expiredtoken")
						{
							$conditions = "member_id=".$member->id." and type='EXPIRED_TOKEN'";
							$email = EmailLog::find('first', array('order' => 'id DESC', 'conditions' => $conditions));
	
							if($email)
							{
								echo "last EXP reminder: <strong>".$email->created_at->format('M-d-y')."</strong><br>\n";
							}
							else
							{
								echo "last EXP reminder: <strong>never sent</strong><br>";
							}	


							$conditions = "member_id=".$member->id." and type='AUTH_REMINDER'";
							$email = EmailLog::find('first', array('order' => 'id DESC', 'conditions' => $conditions));
	
							if($email)
							{
								echo "last AUTH reminder: <strong style='color: #ff0000;'>".$email->created_at->format('M-d-y')."</strong><br>\n";
							}
							else
							{
								echo "last AUTH reminder: <strong>never sent</strong><br>";
							}
													
						}
						else
						{
						
							$conditions = "member_id=".$member->id." and type='AUTH_REMINDER'";
							$email = EmailLog::find('first', array('order' => 'id DESC', 'conditions' => $conditions));
	
							if($email)
							{
								echo "last reminder: <strong>".$email->created_at->format('M-d-y')."</strong><br>\n";
							}
							else
							{
								echo "last reminder: <strong>never sent</strong><br>";
							}

							$conditions = "member_id=".$member->id." and type='EXPIRED_TOKEN'";
							$email = EmailLog::find('first', array('order' => 'id DESC', 'conditions' => $conditions));
	
							if($email)
							{
								echo "last EXP reminder: <strong style='color: #ff0000;'>".$email->created_at->format('M-d-y')."</strong><br>\n";
							}
							else
							{
								echo "last EXP reminder: <strong>never sent</strong><br>";
							}							
						}
					}


				//THIS IS A DANGEROUS FUNCTION THAT ONLY ALEN SHOULD HAVE ACCESS TO...
				if($_SESSION['member']->first_name == "Alen" && $_SESSION['member']->last_name == "Bubich")
				{						
					echo "<a href='/admin/contentfill/".$member->id."'>Content Fill</a> | ";	
				}

				echo "<a href='/admin/member/".$member->id."'>Edit</a>";
				echo " | <a href='/admin/memberposts/".$member->id."'>Posts</a>";
				echo " | <a href='/admin/emails/bymember/".$member->id."'>Emails</a>";

				echo "</td>\n";



				echo "</tr>\n\n";


				if($is_publisher)
				{
					$total_publishers++;
				}
			}

		?>

		<?
			if($allowStatus)
			{
				$subtotal_colspan = 7;
				$total_colspan = 5;
			}
			else
			{
				$subtotal_colspan = 6;
				$total_colspan = 6;
			}

		?>

		<?/*<tr><td colspan="3">Sub Totals:</td><td colspan="<?=$subtotal_colspan;?>" style="text-align: right; font-weight: bold;"><?=number_format($twitter_total);?></td><td colspan="1" style="text-align: right; font-weight: bold;"><?=number_format($li_total);?></td><td colspan="1" style="text-align: right; font-weight: bold;"><?=number_format($fb_total);?></td><td colspan="2"></td></tr>* /?>
		<? if ($this->show_revenue_recur_monthly) : ?>
		<tr><td colspan="3">Total Revenue Recur Monthly:</td><td colspan="<?=$total_colspan-3;?>" style="text-align: right; font-weight: bold;"><?=number_format($total_recur_revenue, 2);?></td><td colspan="3">&nbsp;</td></tr>
		<? else : ?>
		*/?>
		
		<tr><td colspan="5">Totals:</td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($total_recur_revenue, 2);?></td><td colspan="2"></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($total_reach);?></td><td colspan="4">&nbsp;</td>
		</tr>


		</table>

	</div>


	<div class="grid_2" style="margin-top: 20px;">
		<? if($allowStatus) : ?><input type="submit" value="Send Email" class="uiButton" name="submitted" id="mail-btn"><? endif; ?>
	</div>

	
	<div class="grid_10" style="margin-top: 0px;">
		<div style="margin-top: 0px; text-align: right; float: right;">
			<?php 
			if(count($this->paginator) > 0) 
			{
				$paginationParams = array('extraParams'=>array());
		
				if(!empty($this->to_date))
					$paginationParams['extraParams']['to'] = $this->to_date;
		
				if(!empty($this->from_date))
					$paginationParams['extraParams']['from'] = $this->from_date;
		
				echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $paginationParams); 
			}
			?>
		</div>
	</div>
	
	
	</form>
	
	
	
			
	<? /*************************************************
	
	<? $total_friends = $twitter_total + $fb_total + $li_total; ?>
	<? if($this->display_type == "ACTIVE") : ?>

		<div class="grid_12" style="margin-top: 20px; margin-bottom: 80px;">

			<h1>Statistics</h1>

			<table>
				<tr><td>Total Number of Users</td><td style='text-align: right; width: 300px;'><?=$memCount;?></td></tr>
				<tr><td>Total Number of Distributors</td><td style='text-align: right;'><?=$total_publishers;?></td></tr>

				<tr><td>Percentage of Distributors</td><td style='text-align: right;'><? echo ($memCount==0) ? 0 : round(	(	($total_publishers/ $memCount) * 100));?>%</td></tr>

				<tr><td colspan='2' style='border: 0px;'></td></tr>

				<tr><td>Avg Number of Friends per Distributor</td><td style='text-align: right;'><? echo ($total_publishers==0) ? 0 : round(	($total_friends / $total_publishers), 0); ?></td></tr>
				<tr><td>Avg Number of Twitter Friends per Distributor</td><td style='text-align: right;'><? echo ($total_publishers==0) ? 0 : round(	($twitter_total / $total_publishers), 0); ?></td></tr>
				<tr><td>Avg Number of LinkedIn Friends per Distributor</td><td style='text-align: right;'><? echo ($total_publishers==0) ? 0 : round(	($li_total / $total_publishers), 0); ?></td></tr>
				<tr><td>Avg Number of Facebook Friends per Distributor</td><td style='text-align: right;'><? echo ($total_publishers==0) ? 0 : round(	($fb_total / $total_publishers), 0); ?></td></tr>

				<tr><td colspan='2' style='border: 0px;'></td></tr>


				<tr><td>Twitter Percentage of Distribution</td><td style='text-align: right;'><? echo ($total_friends==0) ? 0 :  round(	($twitter_total / $total_friends) * 100, 0); ?>%</td></tr>
				<tr><td>LinkedIn Percentage of Distribution</td><td style='text-align: right;'><? echo ($total_friends==0) ? 0 :round(	($li_total / $total_friends) * 100, 0); ?>%</td></tr>
				<tr><td>Facebook Percentage of Distribution</td><td style='text-align: right;'><? echo ($total_friends==0) ? 0 :round(	($fb_total / $total_friends) * 100, 0); ?>%</td></tr>

			</table>
		</div>

	<? endif; ?>
	*******************************************************/ ?>


	<div class="grid_12" style="margin-bottom: 80px;">&nbsp;</div>

</div>


<script type="text/javascript">
$(document).ready(function()
{
	// control pagination link by binding with sorting paramters and submit the filter form with that generated URL
	$('ul#pagination li a').bind('click', function(event) {
		event.preventDefault();
		current_query_str	= window.location.search;
		pagination_url		= $(this).attr('href');
		target_url			= pagination_url + current_query_str;
		$('#extraForm').attr('action', target_url);
		$('#extraForm').submit();

		return false;
	});
	
	// TABS
	$(".uiTabset").each( function(){
		
		var tabset	= $(this);
		//console.log(tabset);
		var tabs		= $(this).find('.uiTab');
		//console.log(tabs);
		var content	= $(this).children('.uiTabContent');
		//console.log(content);
		
		// SET LOAD EVENTS ON EACH TAB
		(tabs).click( function(){
			
			var tab = $(this);
			var url = tab.attr('rel');
			tabs.removeClass('active');
			tab.addClass('active');
			loadTabContent(content, url);
			
		});
		
		var firstTab;
		if(window.location.hash){
			firstTab = tabs.filter('[href="' + window.location.hash + '"]')[0];	
		} else {
			firstTab = tabs[0];
		}

		// INITIALIZE FIRST TAB
		$(firstTab).addClass('active');
		loadTabContent(content, $(firstTab).attr('rel'));
		
	});

	$('.filter_open').click(function(e){
		var $this = $(this);
		var $AdvanceSearch = $('#advancedSearch');

		if($this.hasClass('close')){
			$AdvanceSearch.hide();
			$this.removeClass('close');
			$this.parent().addClass('roundit');
		}else{
			$AdvanceSearch.show();
			$this.addClass('close');
			$this.parent().removeClass('roundit');
		}
	});
	
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/index/";
		var data    =	{"hasmember":"1","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});


	$('#mail-btn').click(function(event) {

		event.preventDefault();

		//alert($('input[name*="3strikes"]:checked').length);
		if ($('input[name*="3strikes"]:checked').length > 0) {

			$('#3strikesForm').submit();
		} else {

			alert("Please select at least one member.")
		}
	})
});


</script>

<style type="text/css">

/* UI Form */
ul.uiForm, 
ul.uiForm li, 
ul.uiForm li ul, 
ul.uiForm li ul li {
	padding: none!important;
	margin: none!important;
	list-style-type: none!important;
}
ul.uiForm {
	margin: 0 -20px;
	width: 1000px;
}
ul.uiForm.small {
	width: 660px;
}
.uiForm ul, .uiForm li {
	float: left;
	margin: 0px;
}

.uiForm > li{
	float: none;
}

ul.uiForm li ul li ul {
	margin-left: 2em;
	margin-bottom: 1.5em;
}
ul.uiForm li ul li ul li {
	float: none;
	list-style-type: square!important;
	margin-bottom: .5em;
}

.uiFormRowHeader {
	font-size: 21px;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-weight: 600;
	line-height: 26px;
	text-shadow: 1px 1px 0px #fff;
	color: #000;
	margin: 0 0 -1px 0!important;
	padding: 20px;
	width: 960px;
	border-top: solid 1px #fff;
	border-bottom: solid 1px #e5e5e5;
	background-color: #f2f2f2;
	background-image: -moz-linear-gradient(top, #d9d9d9, #f2f2f2); /* FF3.6 */
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #d9d9d9),color-stop(1, #f2f2f2)); /* Saf4+, Chrome */
	background: linear-gradient(#d9d9d9, #f2f2f2);
	-pie-background: linear-gradient(#d9d9d9, #f2f2f2);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm.small .uiFormRowHeader {
	padding: 20px;
	width: 620px;
}
.uiForm .uiFormRow {
	width: 1000px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .uiFormRow {
	width: 660px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm .uiFormRow.last {
	-moz-border-radius: 0 0 8px 8px;
	-webkit-border-radius: 0 0 8px 8px;
	border-radius: 0 0 8px 8px;
	margin-bottom: -20px;
}
.uiForm .info {
	line-height: 20px;
	width: 1000px;
	padding: 10px;
	float: left;
}
.uiForm.small .info {
	width: 660px;
}
.uiForm .buttons {
	width: 960px;
	padding: 10px 20px;
	float: left;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .buttons {
	width: 620px;
}
.uiForm .buttons .uiButton {
	margin-left: 5px;
}
.uiForm .fieldname {
	width: 310px;
	padding: 12px 20px 10px;
	float: left;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-size: 16px;
	font-weight: 400;
	color: #333333;
}
.uiForm.small .fieldname {
	width: 210px;
}
.uiForm .field {
	width: 610px;
	padding: 12px 20px 10px;
	float: left;
}
.uiForm.small .field {
	width: 370px;
}
.uiForm .field span.label {
	float: left;
	margin: 0 20px 0 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiForm .fieldhelp {
	font-size: 12px!important;
	font-weight: normal!important;
	line-height: 18px!important;
	display: block!important;
	margin-top: 4px;
	color: #666666;
}
.uiFormFooterMessage {
	float: left;
	margin-top: 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiFormFooterMessage strong {
	color: #333;
}

ul.normal, ol.normal {
	margin: 0px!important;
}
ul.normal li, ol.normal li {
	line-height: 24px!important;
	margin-left: 10px!important;
}

.uiForm .upsell {
	width: 960px;
	padding: 20px;
	background-color: #252525;
	border-top: solid 1px #fff;
	background-image: url(/images/upsell_new_bg.png);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm .listonly {
	width: 960px;
	padding: 0 20px;
	height: 60px;
	background: #f2f2f2;
	border-top: solid 1px #999;
	-moz-box-shadow: 0 0 20px #cccccc inset;
	-webkit-box-shadow: 0 0 20px #cccccc inset;
	box-shadow: 0 0 20px #cccccc inset;
	position: relative;
	behavior: url(/css/PIE.htc);
}

/* Search Box Styles starts*/

.search_title{
	padding: 5px 10px 5px 13px;
	margin: 0 0px 0px 0px;
	background-color: #4D4D4D;
	background-image: -moz-linear-gradient(top, #666, #333);
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #666),color-stop(1, #333));
	background: linear-gradient(#666, #333);
	-pie-background: linear-gradient(#666666, #333333);
	position: relative;
	behavior: url(/css/PIE.htc);
	-moz-box-shadow: 0 1px 0 #000;
	-webkit-box-shadow: 0 1px 0 #000;
	box-shadow: 0 1px 0 #000;
	-moz-border-radius: 8px 8px 0 0 ;
	-webkit-border-radius: 8px 8px 0 0 ;
	border-radius: 8px 8px 0 0;
	position: relative;
	behavior: url(/css/PIE.htc);
	z-index: 1;
	font-family: Helvetica, Arial, Verdana, sans-serif;
	color: #FFF;
	font-weight: normal;
}

.search_title.roundit{
	border-radius: 8px 8px 8px 8px;
	margin-bottom: 20px;
}

.filter_open{
	background: url(/images/filter_opener.png) no-repeat;
	position: absolute;
	top: 1px;
	right: 10px;
	display: block;
	height: 30px;
	width: 30px;
	cursor: pointer;
}

.filter_open.close{
	background-position: 0 -30px;
}

#advancedSearch{
	display: none;
}

#advancedSearch ul.uiForm{
	margin: 0px;
}

<? /*
#advancedSearch ul.uiForm, #advancedSearch .uiForm .uiFormRow{
	width: 918px;
} */ ?>

#advancedSearch .uiForm > li{
	float: left;
}

#advancedSearch .uiFormRow, #advancedSearch .form {
	background: none !important;
}

#advancedSearch .uiFormRow{
	border-bottom: solid 1px #EDEDED;
}
#advancedSearch .fieldname {
	padding: 18px 20px 19px !important;
	background-color: #FBFBFB;
	border-right: solid 1px #EDEDED;
}
	
#advancedSearch .li_235 {
	width: 235px !important;
}

#advancedSearch .li_180 {
	width: 180px !important;
}

#advancedSearch .li_160 {
	width: 160px !important;
}

#advancedSearch .uiSelectWrapper{
	padding-bottom: 8px;
}

.uiFormRow li.field{
	padding: 10px 0px 5px 10px;
}

.uiFormRow li.fieldname{
	line-height: inherit;
}

</style>