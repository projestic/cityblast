<div class="container_12 clearfix">

	<?php if(!empty($this->message)): ?>
	<div class="grid_12">
		<div class="notice">
			<h3 style="color: #005826;"><?php echo $this->message;?></h3>
		</div>
	</div>
<?php endif; ?>



<div class="grid_12" style="padding-top: 30px;">
	<h1>Referral leader board</h1>
</div>


<div class="grid_12" style="padding-bottom: 80px;">

	<table style="width: 100%;" class="uiDataTable lifetimeTable">
		<thead>
		<tr>
			<th>ID</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th><a href="/admin/referrals/sort/blast/by/<?php echo $this->sort == 'blast' ? $this->by : 'desc';  ?>">Referral blast count</a></th>
			<th><a href="/admin/referrals/sort/email/by/<?php echo $this->sort == 'email' ? $this->by : 'desc';  ?>">Referral email count</a></th>
			<th><a href="/admin/referrals/sort/referrals/by/<?php echo $this->sort == 'referrals' ? $this->by : 'desc';  ?>">Total referrals</a></th>
			<th><a href="/admin/referrals/sort/referral_credits/by/<?php echo $this->sort == 'referral_credits' ? $this->by : 'desc';  ?>">Total referral credits</a></th>
		</tr>
		</thead>

		<tbody>
		<?php foreach($this->paginator as $group) { ?>
			<tr>
				<td style='vertical-align: top;'><a href="/admin/member/<?= $group->id ?>"><?php echo $group->id ?></a></td>
				<td style='vertical-align: top;'><a href="/admin/member/<?= $group->id ?>"><?php echo $group->first_name ?></a></td>
				<td style='vertical-align: top;'><a href="/admin/member/<?= $group->id ?>"><?php echo $group->last_name ?></a></td>
				<td style='vertical-align: top;'><?php echo $group->referral_blast_count ?></td>
				<td style='vertical-align: top;'><?php echo $group->referral_email_count ?></td>
				<td style='vertical-align: top;'><?php echo $group->total_referrals ?></td>
				<td style='vertical-align: top;'><?php echo $group->total_referral_credits ?></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<div class="grid_12" style="margin-top: 0px;">
		<div style="margin-top: 0px; text-align: right; float: right;">
			<?php
			if (count($this->paginator) > 0) {
				$paginationParams = array('extraParams'=>array());
				echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $paginationParams);
			}
			?>
		</div>
	</div>
</div>
</div>