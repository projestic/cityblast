<div class="container_12 clearfix">


	<?/*
	<form action="/admin/payout/" method="POST" name="change_affiliate" id="change_affiliate">

		<div class="grid_12" style="text-align: right;">
			<div style="float: right;">
				<select name="affiliate_id" id="affiliate_id" style="margin-top: 25px;width: 200px; font-size: 30px; font-weight: bold; width: 300px;">
					<option value="*" <?php echo (empty($this->selected_affiliate_id) || $this->selected_affiliate_id == "*") ? "selected" : "";?>>All affiliates</option>
					<?php 
					foreach($this->affiliate_dropdown as $aff): 
					?>
						<option value="<?php echo $aff->id;?>" <?php echo (!empty($this->selected_affiliate_id) && $this->selected_affiliate_id == $aff->id) ? "selected" : "";?>>
							<?php echo ucwords($aff->first_name." ".$aff->last_name); ?>
						</option>
					<?php 
					endforeach; 
					?>
				</select><br/>

			</div>
		</div>
	
	</form>
	*/?>

	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		
		<?php
			if(!empty($this->message)):
			?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
			<?php
			endif;
		?>
	
		<h1>Payables</h1>

		<table style="width: 100%" class="uiDataTable lineheight">
			<tr>
				
				<th style="padding:4px;">ID</th>
				
				<th style="padding:4px;">Member</th>
				<th style="padding:4px;">Affiliate</th>
				<!--<th style="border: 1px solid #888;padding:4px;">Listing ID</th>-->
				
				
				<th style="padding:4px;">Payout Date</th>				
				<th style="padding:4px;">Amount</th>
				
				<?/*<th>Action</th>*/?>

			</tr>
			
			<?php
			if($this->payables){
				$row = true;
				$total = 0;
				foreach($this->payables as $payable)
				{					
					echo "<tr ";
					if($row == true)
					{						
						$row = false;
						echo "class='odd' ";	
					}
					else
					{
						$row = true;	
						echo "class='' ";	
					}		
					echo ">";					
					echo "<td style='padding:4px;'>" . $payable->id."</td>";
					echo "<td style='padding:4px;'><a href='/admin/member/".$payable->member_id."'>".$payable->member->name() . "</a></td>";
					if ($payable->affiliate) {
						echo "<td style='padding:4px;'><a href='/admin/member/".$payable->affiliate->member_id."'>" . $payable->affiliate->owner->name() ."</td>";
					} else {
						echo "<td style='padding:4px;'></td>";
					}
					echo "<td style='padding:4px;'>";
						if(isset($payable->payment->created_at) && !empty($payable->payment->created_at)) echo $payable->payment->created_at->format("Y-M-d");
					echo "</td>";
					echo "<td style='padding:4px; text-align:right'>$".number_format($payable->amount,2)."</td>";
					//echo "<td style='padding:4px;'></td>";
					echo "</tr>";
					
				}
			}
			else{
				echo "<tr><td colspan='5' align='center'>No payable</td></tr>";
			}
			?>
		</table>
	</div>
	<div class="grid_12" style="margin-bottom: 80px;">
		<div style="float: right;" class="clearfix"><?= ( $this->payables instanceof Zend_Paginator) ? $this->paginationControl($this->payables, 'Elastic', '/common/pagination.phtml') : ''; ?></div>
	</div>

</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/index/";
		var data    =	{"blast":"ON","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});
});
</script>