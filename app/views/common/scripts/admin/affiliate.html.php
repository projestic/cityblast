<div class="container_12 clearfix">


	<form action="/admin/affiliate/" method="POST" id="filter">

		<div class="grid_12" style="text-align: right;">
			<div style="float: right;">
				<select name="affiliate_id" id="affiliate_id" style="margin-top: 25px;width: 200px; font-size: 30px; font-weight: bold; width: 300px;">
					<option value="*" <?php echo (empty($this->selected_affiliate_id) || $this->selected_affiliate_id == "*") ? "selected" : "";?>>All affiliates</option>
					<?php 
					foreach($this->affiliate_dropdown as $aff): 
					?>
						<option value="<?php echo $aff->id;?>" <?php echo (!empty($this->selected_affiliate_id) && $this->selected_affiliate_id == $aff->id) ? "selected" : "";?>>
							<?php echo ucwords($aff->first_name." ".$aff->last_name); ?>
						</option>
					<?php 
					endforeach; 
					?>
				</select><br/>
				<select name="referring_member_id" id="referring_member_id" style="margin-top: 25px;width: 200px; font-size: 30px; font-weight: bold; width: 300px;">
					<option value="*" <?php echo (empty($this->selected_referring_member_id) || $this->selected_referring_member_id == "*") ? "selected" : "";?>>All referring members</option>
					<?php 
					foreach($this->referring_member_dropdown as $rf): 
					?>
						<option value="<?php echo $rf->id;?>" <?php echo (!empty($this->selected_referring_member_id) && $this->selected_referring_member_id == $rf->id) ? "selected" : "";?>>
							<?php echo ucwords($rf->first_name." ".$rf->last_name); ?>
						</option>
					<?php 
					endforeach; 
					?>
				</select>
			</div>
		</div>


		<div class="clearfix">
			<?php if (!empty($this->conversionRate)): ?>
				<?
					$signupsExtended  = !empty($this->conversionRate['signup']) ? $this->conversionRate['signup'] : 0;
					$signupsExtended += !empty($this->conversionRate['extended_trial']) ? $this->conversionRate['extended_trial'] : 0;
					$expired          = !empty($this->conversionRate['signup_expired']) ? $this->conversionRate['signup_expired'] : 0;
					$paid             = !empty($this->conversionRate['paid']) ? $this->conversionRate['paid'] : 0;
					$paymentFailed    = !empty($this->conversionRate['payment_failed']) ? $this->conversionRate['payment_failed'] : 0;
					$cancelled        = !empty($this->conversionRate['cancelled']) ? $this->conversionRate['cancelled'] : 0;
					$free             = !empty($this->conversionRate['free']) ? $this->conversionRate['free'] : 0;
				?>
				<h3>Afilliate conversion rates</h3> 
				
				<table>
	
					<tr>
						<td>Paid:</td>
						<td style="text-align: right;"><?=$paid;?></td>
						<td style="text-align: right;">
							<?php ?>
							<?=($this->paginator->getTotalItemCount()) ? number_format(($paid*100)/$this->paginator->getTotalItemCount(), 1) : 0 ?>%
						</td>
					</tr>
	
					
					<tr>
						<td>Signups/Extented Trials:</td>
						<td style="text-align: right;"><?=$signupsExtended;?></td>
						<td style="text-align: right;">
							<?=($this->paginator->getTotalItemCount()) ? number_format(($signupsExtended*100)/$this->paginator->getTotalItemCount(), 1) : 0?>%
						</td>					
					</tr>
					
					<tr>
						<td>Expired:</td>
						<td style="text-align: right;"><?=$expired;?></td>
						<td style="text-align: right;">
							<?=($this->paginator->getTotalItemCount()) ? number_format(($expired*100)/$this->paginator->getTotalItemCount(), 1) : 0?>%
						</td>
					</tr>
	
	
					<tr>
						<td>Payment Failed:</td>
						<td style="text-align: right;"><?=$paymentFailed;?></td>
						<td style="text-align: right;">
							<?=($this->paginator->getTotalItemCount()) ? number_format(($paymentFailed*100)/$this->paginator->getTotalItemCount(), 1) : 0?>%
						</td>
					</tr>
	
					<tr>
						<td>Cancelled:</td>
						<td style="text-align: right;"><?=$cancelled;?></td>
						<td style="text-align: right;">
							<?=($this->paginator->getTotalItemCount()) ? number_format(($cancelled*100)/$this->paginator->getTotalItemCount(), 1) : 0?>%
						</td>
					</tr>
	
					<tr>
						<td>Free:</td>
						<td style="text-align: right;"><?=$free;?></td>
						<td style="text-align: right;">
							<?=($this->paginator->getTotalItemCount()) ? number_format(($free*100)/$this->paginator->getTotalItemCount(), 1) : 0?>%
						</td>
					</tr>

					<tr>
						<td>Total:</td>
						<td style="text-align: right;">
							<?=($this->paginator->getTotalItemCount()) ? number_format($this->paginator->getTotalItemCount()) : 0?>
						</td>
						<td>&nbsp;</td>
					</tr>
					
					
	
					
				</table>
				
			<?php endif; ?>
		</div>

		<?php
		if ($this->to_date)
		{
			$to_arr = explode("-",$this->to_date);
	
			$day_to	= $to_arr[2];
			$month_to	= $to_arr[1];
			$year_to	= $to_arr[0];
		}

		if ($this->from_date)
		{
			$from_arr = explode("-",$this->from_date);
	
			$day_from		= $from_arr[2];
			$month_from	= $from_arr[1];
			$year_from	= $from_arr[0];
		}
		?>

		<table style="width: 100%; border-bottom: 0px; margin-top: 25px">
		<tr>
			<td style="text-align: right;border-bottom: 0px;">
			<b>From :</b>
			<select name="month_from" id="month_from" class="type_status" style="font-size: 18px; font-weight: bold;">
				<?php
					$months	= array(
						'01' => 'Jan',
						'02' => 'Feb',
						'03' => 'Mar',
						'04' => 'Apr',
						'05' => 'May',
						'06' => 'Jun',
						'07' => 'Jul',
						'08' => 'Aug',
						'09' => 'Sep',
						'10' => 'Oct',
						'11' => 'Nov',
						'12' => 'Dec',
					);

					foreach ($months as $key => $name) {

						echo '<option value="'. $key .'"'. ($key == $month_from ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="day_from" id="day_from" class="type_status" style="font-size: 18px; font-weight: bold; width: 75px">
				<?php

					$days	= range(1, 31);

					foreach ($days as $name) {

						$key	= str_pad($name, 2, '0', STR_PAD_LEFT);

						echo '<option value="'. $key .'"'. ($key == $day_from ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="year_from" id="year_from" class="type_status" style="font-size: 18px; font-weight: bold;">
				<?php
					$current_year	= intval(date('Y'));

					$years	= range($current_year, $current_year - 25);

					foreach ($years as $name) {

						echo '<option value="'. $name .'"'. ($name == $year_from ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

				
			</td>
			
			<td style="text-align: right;border-bottom: 0px;">
			<b>To:</b>
			<select name="month_to" id="month_to" class="type_status" style="font-size: 18px; font-weight: bold;">
				<?php
					$months	= array(
						'01' => 'Jan',
						'02' => 'Feb',
						'03' => 'Mar',
						'04' => 'Apr',
						'05' => 'May',
						'06' => 'Jun',
						'07' => 'Jul',
						'08' => 'Aug',
						'09' => 'Sep',
						'10' => 'Oct',
						'11' => 'Nov',
						'12' => 'Dec',
					);

					foreach ($months as $key => $name) {

						echo '<option value="'. $key .'"'. ($key == $month_to ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="day_to" id="day_to" class="type_status" style="font-size: 18px; font-weight: bold; width: 75px">
				<?php

					$days	= range(1, 31);

					foreach ($days as $name) {

						$key	= str_pad($name, 2, '0', STR_PAD_LEFT);

						echo '<option value="'. $key .'"'. ($key == $day_to ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

			<select name="year_to" id="year_to" class="type_status" style="font-size: 18px; font-weight: bold;">
				<?php
					$current_year	= intval(date('Y'));

					$years	= range($current_year, $current_year - 25);

					foreach ($years as $name) {

						echo '<option value="'. $name .'"'. ($name == $year_to ? ' selected="true"' : '') .'>'. $name .'</option>';
					}
				?>
			</select>

				
			</td>
			<td style="text-align: right;border-bottom: 0px; width: 30px; padding-left: 10px;"><input type="submit" name="go" value="Go" class="uiButton right"/></td>
		</tr>
		</table>
		
		<input type="hidden" name="from_date" value="" />
		<input type="hidden" name="to_date" value="" />
		
	</form>
	
	<script type="text/javascript">
	
		$(document).ready( function () {
	
			$('#filter').submit( function(event) {

				if (!$('#month_from').val()) {
					alert('Please select month from.');
					return false;
				}

				if (!$('#day_from').val()) {
					alert('Please select day from.');
					return false;
				}

				if (!$('#year_from').val()) {
					alert('Please select year from.');
					return false;
				}

				if (!$('#month_to').val()) {
					alert('Please select month to.');
					return false;
				}

				if (!$('#day_to').val()) {
					alert('Please select day to.');
					return false;
				}

				if (!$('#year_to').val()) {
					alert('Please select year to.');
					return false;
				}
				
				$('input[name=from_date]').val( $('#year_from').val() + '-' + $('#month_from').val() + '-' + $('#day_from').val() );
				$('input[name=to_date]').val( $('#year_to').val() + '-' + $('#month_to').val() + '-' + $('#day_to').val() );

				$(this).submit();

			});

			$('#affiliate_id, #referring_member_id').change( function() {
				$('#affiliate_id, #referring_member_id').not(this).val('*');
				$("#filter").submit();
			});

		});
	</script>
	<div class="grid_12" style="margin-bottom: 80px;">


		<h1>
			<?php echo number_format($this->paginator->getTotalItemCount()) . " Total Affiliate Sales"; ?>
		</h1>
		
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<?/*<th>Photo</th>*/?>
				<th>Name</th>
				<th>Email</th>
				<th>Member Since</th>				
				<th>Status</th>
				<th>Next Payment</th>
				<th>Referring Affiliate</th>								
				<th>Referring Member</th>
				<th>Actions</th>
			</tr>

		<?php
			$li_total=0;
			$twitter_total=0;
			$fb_total=0;
			$row=true;


			$total_publishers = 0;

			$twitter_publishers = 0;
			$facebook_publishers = 0;
			$linkedin_publishers = 0;

			//$total_twitter_friends = 0;
			//$total_linkedin_friends = 0;
			//$total_facebook_friends = 0;


			//foreach($this->members as $member)
			foreach($this->paginator as $member)
			{
				$is_publisher = false;

				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}
				echo ">\n";




				echo "<td><a href='/admin/member/".$member->id."'>".$member->id."</a></td>\n";

				//echo '<td>';
				//echo '<a href="http://www.facebook.com/profile.php?id='.$member->uid.'" target="_new"><img src="'. (empty($member->photo) ? 'https://graph.facebook.com/'. $member->uid .'/picture' : $member->photo) .'" style="width: 30px; height: 30px;"/></a>';
				//echo '</td>'."\n";

				echo "<td><a href='/admin/member/".$member->id."'>".$member->first_name." " .$member->last_name."</a></td>\n";

				echo "<td><a href='mailto:".$member->email."'>";
				$newstring = substr($member->email,0,30);
				echo $newstring;
				echo "</a></td>\n";

				echo "<td nowrap>".$member->created_at->format('M-d-y')."</td>\n";
	
	
				echo "<td nowrap>";
					echo $member->payment_status;
				echo "</td>";

				echo "<td nowrap>";
					if(isset($member->next_payment_date) && !empty($member->next_payment_date)) echo $member->next_payment_date->format('M-d-y');
				echo "</td>\n";

				echo "<td>";								
				if ($member->referring_affiliate instanceOf Affiliate) echo "<a href='/admin/member/".$member->referring_affiliate->owner->id."' target='_blank'>".$member->referring_affiliate->owner->first_name." " .$member->referring_affiliate->owner->last_name."</a>\n";
				echo "</td>";				
				echo "<td>";				
				if ($member->referring_member instanceOf Member) echo "<a href='/admin/member/".$member->referring_member->id."' target='_blank'>".$member->referring_member->first_name." " .$member->referring_member->last_name."</a>\n";
				echo "</td>";				
				echo "<td nowrap>";
				echo "<a href='/admin/member/".$member->id."'>Edit</a>";
				echo " | <a href='/admin/memberposts/".$member->id."'>Posts</a>";
				echo " | <a href='/admin/emails/bymember/".$member->id."'>Emails</a>";

				echo "</td>\n";



				echo "</tr>\n\n";


				if($is_publisher)
				{
					$total_publishers++;
				}
			}

		?>


		</table>
		
		<div style="float: right;">
			<?php 
			$params = array();
			if (isset($this->params['affiliate_id'])) $params = array('extraParams'=>array('affiliate_id' => $this->params['affiliate_id']));
			echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $params); ?>
		</div>
	</div>




</div>
