<?
	$stats = $this->stats;
?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>


<div class="container_12 clearfix">

	<? echo $this->render('common/member.dropdown.html.php'); ?>


	<div class="grid_12" style="padding-top: 15px; padding-bottom: 30px;">
		
		
		<table width="100%;">
			<tr>
				<td>Lifetime CC Members</td>
				<td style="text-align: right; font-size: 16; font-weight: bold;"><?=number_format($this->all_cc_members);?></td>
			</tr>
			<tr>
				<td>Current Paying Members</td>
				<td style="text-align: right; font-size: 16; font-weight: bold;"><?=number_format($stats['paid_member_count'])?></td>
			</tr>

			<tr>
				<td>14 Day Signups with CC</td>
				<td style="text-align: right; font-size: 16; font-weight: bold;"><?=number_format($stats['cctrial_member_count'])?></td>
			</tr>

			<tr>
				<td>7 Day Trial (no CC)</td>
				<td style="text-align: right; font-size: 16; font-weight: bold;"><?=$this->trial_member;?></td>
			</tr>

			<tr>
				<td>Agents (Paid and Trial)</td>
				<td style="text-align: right; font-size: 16; font-weight: bold;"><?=number_format($stats['agent_count'])?></td>
			</tr>	

			<tr>
				<td>Broker (Paid and Trial)</td>
				<td style="text-align: right; font-size: 16; font-weight: bold;"><?=number_format($stats['broker_count'])?></td>
			</tr>				
			
			<tr>
				<td>All members who've cancelled</td>
				<td style="text-align: right; font-size: 16; font-weight: bold;"><a href="/admin/canceled"><?=$this->dropoff_members;?></a></td>
			</tr>

			<tr>
				<td>Paid cancels</td>
				<td style="text-align: right; font-size: 16; font-weight: bold;"><a href="/admin/paidcanceled"><?=$this->paid_dropoff;?></a></td>
			</tr>

			<tr>
				<td style="border-bottom-width: 0;" colspan="2">
					<h2>Revenue</h2>
				</td>
			</tr>
			<tr>
				<td style="font-size: 36px; font-weight: bold; color: #39aa00; padding-top: 0;">Projected 30 Day Revenue</td>
				<td style="text-align: right; font-size: 36px; font-weight: bold; color: #39aa00; padding-top: 0;">$<?=number_format($stats['projected_revenue_30'], 2)?></td>
			</tr>			
			<tr>
				<td>Actual Revenue MTD<span style="font-style: italic; font-size: 10px; color: #b9b9b9;">Actual payments recieved this month</span></td>
				<td style="text-align: right; font-size: 20px; font-weight: bold;">$<?=number_format($stats['actual_revenue_current_month'], 2)?></td>
			</tr>

			<tr>
				<td style="padding-left: 20px; color: #959595;">Expected Monthlies <span style="font-style: italic; font-size: 10px; color: #b9b9b9;">Payments expected but not yet recieved, from members on monthly billing cycle, for current month</span></td>
				<td style="text-align: right; font-size: 14px; color: #959595; font-weight: bold;">$<?=number_format($stats['expected_revenue_month'], 2)?></td>
			</tr>
 
			<tr>
				<td style="padding-left: 20px; color: #959595;">Expected Semi-Annual <span style="font-style: italic; font-size: 10px; color: #b9b9b9;">Biannual billing cycle</span></td>
				<td style="text-align: right; font-size: 14px; color: #959595;  font-weight: bold;">$<?=number_format($stats['expected_revenue_biannual'], 2)?></td>
			</tr>

			<tr>
				<td style="padding-left: 20px; color: #959595;">Expected Annual <span style="font-style: italic; font-size: 10px; color: #b9b9b9;">Annual billing cycle</span></td>
				<td style="text-align: right; font-size: 14px; color: #959595;  font-weight: bold;">$<?=number_format($stats['expected_revenue_annual'], 2)?></td>
			</tr>  
			
			
			<tr>
				<td>Projected <?=date('F')?> Revenue <span style="font-style: italic; font-size: 10px; color: #b9b9b9;">Expected payments + recieved payments for current month</span></td>
				<td style="text-align: right; font-size: 20px; font-weight: bold;">$<?=number_format($stats['projected_revenue'], 2)?></td>
			</tr>
			
			<tr>
				<td>Average Price Per Member <span style="font-style: italic; font-size: 10px; color: #b9b9b9;">(SUM(member.price) for all current paying members / total number of current paying members)</a></td>
				<td style="text-align: right; font-size: 14px; font-weight: bold;">$<?=number_format($stats['projected_monthly_average_price_paid_member'], 2)?></td>
			</tr>
			
			<tr>
				<td>Average Lifetime Value <span style="font-style: italic; font-size: 10px; color: #b9b9b9;">(SUM(payment.amount)  / all time total number paying members)</a></td>
				<td style="text-align: right; font-size: 14px; font-weight: bold;">$<?=number_format($stats['average_lifetime_value'], 2)?></td>
			</tr>
		</table>
					
	</div>		


	<div class="grid_12" style="margin-bottom: 20px;">
		<h3>Member Signups
			<span style="float: right;">Signups <span style="color: #b9b9b9;">(CC / Total)</span>: <?=number_format($this->member_signup_totals['cc']);?>/<?=number_format($this->member_signup_totals['total']);?></span>
		</h3>
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>Day</th>
				<th style='text-align: right;'>New CC</th>
				<th style='text-align: right;'>Paid Cancel</th>
				<th style='text-align: right;'>Total</th>
				<th style='text-align: right;'>Avrg Conv</th>
				<th style='text-align: right;'>CC</th>
				<th style='text-align: right;'>CC Avrg Conv</th>
				<th style='text-align: right;'>No CC</th>
				<th style='text-align: right;'>No CC Avrg Conv</th>
				<th style='text-align: right;'>Max</th>
				<th style='text-align: right;'>Projected</th>
				<th style='text-align: right;'>Actual</th>
				<th style='text-align: right;'>Spend</th>
				<th style='text-align: right;'>Profit</th>
				<th style='text-align: right;'>Action</th>
			</tr>
		<?php
			$row=true;
			$counter = 0;
			
			
			$isTrialOver = false;
			$trialEndDate = date('Y-m-d', strtotime(date('Y-m-d') . '-14 days'));

			foreach($this->member_signups as $result)
			{				
				$day = $current_day = $result->date;
				$today_time = strtotime($current_day);
				$tomorrow = strtotime("tomorrow", $today_time);
				$yesterday = strtotime("-1day", $today_time);
				$tomorrow_day = date('Y-m-d', $tomorrow);
				$yesterday_day = date('Y-m-d', $yesterday);
				$cur_day = date('Y-m-d', $today_time);
				
																
				$counter++;
				
				$isTrialEndDate = ($trialEndDate == date('Y-m-d', $today_time));
				$isTrialOver = ($isTrialOver || $isTrialEndDate);
				
				$ccNew = !empty($result->ccnew) ? number_format($result->ccnew) : 0;
				$ccPaidCancel = !empty($result->cc_paid_cancel) ? number_format($result->cc_paid_cancel) : 0;
				
				$total = !empty($result->total) ? number_format($result->total) : 0;
				$avConvRate = !empty($result->avconv_rate) ? number_format($result->avconv_rate, 2) : 0;
				
				$cc = !empty($result->cc) ? number_format($result->cc) : 0;
				$ccAvConvRate = number_format($result->cc_avconv_rate, 2);
				
				$noCc = !empty($result->nocc) ? number_format($result->nocc) : 0;
				$noCcAvConvRate = number_format($result->nocc_avconv_rate, 2);
				
				$ccPaid = !empty($result->cc_paid) ? number_format($result->cc_paid) : 0;
				$ccPaid = $ccPaid . '/' . $cc;
				$ccCancel = !empty($result->cc_cancel) ? number_format($result->cc_cancel) : 0;
				$ccCancel = $ccCancel . '/' . $cc;
				$ccFailedPayment = !empty($result->cc_failed_payment) ? number_format($result->cc_failed_payment) : 0;
				$ccFailedPayment = $ccFailedPayment . '/' . $cc;
				$noCcPaid = !empty($result->nocc_paid) ? number_format($result->nocc_paid) : 0;
				$noCcPaid = $noCcPaid . '/' . $noCc;
				$noCcCancel = !empty($result->nocc_cancel) ? number_format($result->nocc_cancel) : 0;
				$noCcCancel = $noCcCancel . '/' . $noCc;
				$noCcFailedPayment = !empty($result->nocc_failed_payment) ? number_format($result->nocc_failed_payment) : 0;
				$noCcFailedPayment = $noCcFailedPayment . '/' . $noCc;
			
				
				
				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}
					
					if($isTrialEndDate) echo ' style="background-color: #FFEF73;" ';
					
					$bgcolor = "";
					if($isTrialEndDate) $bgcolor = ' background-color: #FFEF73; ';
					
				echo ">";

				

				echo "<td nowrap style='" .$bgcolor."'>".date("d, M D", strtotime($day))."</td>";
				
				echo "<td nowrap style='text-align: right; " .$bgcolor." font-size: 14px;'>";
					echo "<a href='/admin/allmembers?cc_not_provided_by=" . $yesterday_day . "&cc_provided_by=" . $cur_day . "'>";
					echo "<b>".$ccNew."</b>";
					echo "</a>";
				echo "</td>";
				
				echo "<td nowrap style='text-align: right; " .$bgcolor." font-size: 14px;'>";
					echo "<a href='/admin/allmembers/cancelled_date/".$cur_day."'><b>".$ccPaidCancel."</b></a>";
				echo "</td>";


				echo "<td nowrap style='text-align: right; " .$bgcolor."'>";
					echo "<a href='/admin/allmembers/from/".$cur_day."/to/".$tomorrow_day."/'>";
					if($isTrialOver) echo ($ccPaid +  $noCcPaid) . "/";
					echo $total;
					echo "</a>";
				echo "</td>";
				
				echo "<td nowrap style='text-align: right; " .$bgcolor."'>";
					echo $avConvRate;
				echo "</td>";

				echo "<td nowrap style='text-align: right;" .$bgcolor."'>";
					echo "<a href='/admin/payments/from/".$cur_day."/to/".$cur_day."/?member_signup_from=" . $cur_day . "&member_signup_to=" . $cur_day . "'>";
					if($isTrialOver) echo $ccPaid; 
					else echo $cc;
					echo "</a>";
				echo "</td>";
				
				echo "<td nowrap style='text-align: right;" .$bgcolor."'>" . $ccAvConvRate . "</td>";

				echo "<td nowrap style='text-align: right;" .$bgcolor."'>";
					echo "<a href='/admin/allmembers/from/".$cur_day."/to/".$tomorrow_day."?cc_not_provided_by=" . $tomorrow_day . "'>";
					if($isTrialOver) echo $noCcPaid;
					else echo $noCc;
					echo "</a>";
				echo "</td>";
				
				echo "<td nowrap style='text-align: right;" .$bgcolor."'>" . $noCcAvConvRate . "</td>";
				
				echo "<td nowrap style='text-align: right;" .$bgcolor."'>";
					if($result->revenue_projected) echo number_format($result->revenue_projected_max);
					else echo "-";
				echo "</td>";
				
				echo "<td nowrap style='text-align: right;" .$bgcolor."'>";
					if($result->revenue_projected) echo number_format($result->revenue_projected);
					else echo "-";
				echo "</td>";
				
				echo "<td nowrap style='text-align: right;" .$bgcolor."'>";
					if($isTrialOver) echo number_format($result->revenue_actual);
					else echo "-";
				echo "</td>";
				
				echo "<td nowrap style='text-align: right; color: #ff0000;" .$bgcolor."'>";
					if($result->ad_spend > 0) echo number_format($result->ad_spend);
					else  echo "-";
				echo "</td>";
				
				echo "<td nowrap style='text-align: right;  font-weight: bold;  font-size: 14px;" .$bgcolor." ";
				if(	isset($result->profit) && $result->profit < 0 ) echo " color: #ff0000; ";
				echo "'>";
			
					if($isTrialOver) 
					{
						if(	!isset($result->profit) || $result->profit == 0 || empty($result->profit) ) echo "-";
						else echo number_format($result->profit);
					}
				echo "</td>";
				
				echo "<td nowrap style='text-align: right;" .$bgcolor."'>";
					echo '<a href="/admin/membersignupstatsedit/date/'.$result->date.'">Edit</a>';
				echo "</td>";
				
				echo "</tr>";
				
				echo "\n";
			}



		?>
		
		<tr>
			<td>Sub Totals:</td>
			<td style="text-align: right; font-weight: bold; font-size: 14px;"><?=number_format($this->member_signup_totals['ccnew']);?></td>
			<td style="text-align: right; font-weight: bold; font-size: 14px;"><?=number_format($this->member_signup_totals['cc_paid_cancel']);?></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($this->member_signup_totals['total']);?></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($this->member_signup_totals['cc']);?></td>
			<td style="text-align: right; font-weight: bold;"></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($this->member_signup_totals['nocc']);?></td>
			<td style="text-align: right; font-weight: bold;"></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($this->member_signup_totals['revenue_projected_max']);?></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($this->member_signup_totals['revenue_projected']);?></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($this->member_signup_totals['revenue_actual']);?></td>
			<td style="text-align: right; font-weight: bold;"><?=number_format($this->member_signup_totals['ad_spend']);?></td>
			<td style="text-align: right; font-weight: bold; font-size: 14px;"><?=number_format($this->member_signup_totals['profit']);?></td>
			<td style="text-align: right; font-weight: bold;"></td>
		</tr>

		</table>
	</div>
	

		<div class="grid_12" style="margin-bottom: 20px;">
		<h3>Monthly Statistics (Last <?=(!empty($this->member_stats_monthly)) ? count($this->member_stats_monthly) : 0 ?> Months)</h3>
		<table width="100%" class="uiDataTable lineheight">
			<tr>
				<th>Month</th>
				<th style='text-align: right;'>Total Signups</th>
				<th style='text-align: right;'>Paid Signups</th>
				<th style='text-align: right;'>Paid Cancels</th>
				<th style='text-align: right;'>% New Signups</th>
				<th style='text-align: right;'>% Paid Signups</th>
				<th style='text-align: right;'>Daily Avg Paid</th>
			</tr>
			<?php if (!empty($this->member_stats_monthly)): ?>
				<?php foreach ($this->member_stats_monthly as $stat): ?>
				<tr>
					<td><?=$stat['month_string']?></td>
					<td style='text-align: right;'><?=$stat['total_signups']?></td>
					<td style='text-align: right;'><?=$stat['paid_signups']?></td>
					<td style='text-align: right;'><?=$stat['paid_cancels']?></td>
					<td style='text-align: right;'><?=$stat['new_signup_percent']?>%</td>
					<td style='text-align: right;'><?=$stat['paid_signup_percent']?>%</td>
					<td style='text-align: right;'><?=$stat['paid_daily_average']?></td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
			<tr>
				<td>Totals</td>
				<td style='text-align: right;font-weight: bold;'><?=$this->member_stats_monthly_totals['total_signups']?></td>
				<td style='text-align: right;font-weight: bold;'><?=$this->member_stats_monthly_totals['paid_signups']?></td>
				<td style='text-align: right;font-weight: bold;'><?=$this->member_stats_monthly_totals['paid_cancels']?></td>
				<td style='text-align: right;font-weight: bold;'><?=$this->member_stats_monthly_totals['new_signup_percent']?></td>
				<td style='text-align: right;font-weight: bold;'><?=$this->member_stats_monthly_totals['paid_signup_percent']?></td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td style='font-weight: bold;'>Averages</td>
				<td style='text-align: right; font-weight: bold;'><?=$this->member_stats_monthly_avrgs['total_signups']?></td>
				<td style='text-align: right; font-weight: bold;'><?=$this->member_stats_monthly_avrgs['paid_signups']?></td>
				<td style='text-align: right; font-weight: bold;'><?=$this->member_stats_monthly_avrgs['paid_cancels']?></td>
				<td style='text-align: right; font-weight: bold;'></td>
				<td></td>
				<td style='text-align: right; font-weight: bold;'><?=$this->member_stats_monthly_avrgs['paid_daily_average']?></td>
			</tr>	
		</table>
		

		

	</div>

	<div class="grid_12" style="height: 60px;">&nbsp;</div>

</div>
