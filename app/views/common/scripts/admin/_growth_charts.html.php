<?php
/**
 * Grows charts
 * @author konstv
 * @date: 2013nov06
 */

/** @var Payment[] $this->monthly_revenues */

// Reverse the array, so that the most oldest values would be at the beginning of the chart
$monthly_revenues = array_reverse($this->monthly_revenues);
?>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<!-- Payments -->
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Year', 'Payments'],
      <?php
       echo Page_Revenue_Grows_Chart_Man::toJsonPayments($monthly_revenues);
      ?>
    ]);

    var options = {
      title: 'Growth Chart: Payments',
      hAxis: {title: 'Month',  titleTextStyle: {color: '#333'}, showTextEvery: 6},
      vAxis: {minValue: 0, },
      series: {
        0: {pointSize: 3}
      },
      chartArea: {
        left: 60
      }
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_div_payments'));
    chart.draw(data, options);
  }
</script>

<!-- Purchases -->
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Year', 'Purchases'],
      <?php
       echo Page_Revenue_Grows_Chart_Man::toJsonPurchases($monthly_revenues);
      ?>
    ]);

    var options = {
      title: 'Growth Chart: Purchases',
      hAxis: {title: 'Month',  titleTextStyle: {color: '#333'}, showTextEvery: 6},
      vAxis: {minValue: 0},
      series: {
        0: {color: '#f88484', pointSize: 3}
      },
      chartArea: {
        left: 60
      }
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_div_purchases'));
    chart.draw(data, options);
  }
</script>

<div>
  <!--Div that will hold the chart-->
  <div id="chart_div_payments"></div>
</div>
<div>
  <!--Div that will hold the chart-->
  <div id="chart_div_purchases"></div>
</div>

<?php
class Page_Revenue_Grows_Chart_Man {
  /**
   * Format:
   * ['month1', value2],
   * @param Payment[] $monthly_revenues
   * @return string JSON string
   */
  public static function toJsonPayments($monthly_revenues) {
    $res = '';
    $needDelimiter = false;
    foreach($monthly_revenues as $month)
    {
      $res .= self::monthRow($month->month, number_format($month->running_total, 2, '.', ''), $needDelimiter);
      if (!$needDelimiter) {
        $needDelimiter = true;
      }
    }
    return $res;
  }

  /**
   * Format:
   * ['month1', value2],
   * @param Payment[] $monthly_revenues
   * @return string JSON string
   */
  public static function toJsonPurchases($monthly_revenues) {
    $res = '';
    $needDelimiter = false;
    foreach($monthly_revenues as $month)
    {
      $res .= self::monthRow($month->month, $month->count_purchases, $needDelimiter);
      if (!$needDelimiter) {
        $needDelimiter = true;
      }
    }
    return $res;
  }

  protected static function monthRow($monthStr, $value2, $needDelimiter) {
    $res = '';
    // format: ['month', value2]
    if ($needDelimiter) {
      $res .= ',';
      $res .= "\n";
    }
    $row = '';
    $row .= "'$monthStr'" . ', ';
    $row .= $value2;
    $res .= "[$row]";

    return $res;
  }
}