<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">
		
		<?php if(!empty($this->message)): ?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
		<?php endif; ?>


		<h4 class="search_title roundit">
			Search
			<span class="filter_open"></span>
		</h4>
		<div id="advancedSearch" class="form clearfix" style="margin-bottom: 25px;">
			

			<form style="border: 1px solid #ccc; border-radius:0 0 10px 10px;" action="/admin/listings" method="POST" id="extraForm" name="extraForm">

				<input type="hidden" name="blast_type" id="blast_type" />

				<ul class="uiForm full clearfix">
					


					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Listing ID</li>
							<li class="field li_235">
								<input type="text" name="listing_id" id="listing_id" tabindex="2" value="<?php echo isset($this->listing_id) ? $this->listing_id : '' ?>" class="uiInput" style="width: 120px" />
							</li>
							<li class="fieldname li_180">Status</li>
							<li class="field li_235">
								<select name="deleted" id="deleted" tabindex="3" class="uiInput">
									<option value="">Active</option>
									<option value="1" <?php echo ($this->deleted) ? " selected='1' " : "" ?>>Deleted</option>
								</select>
							</li>

						</ul>
					</li>						
<?/*
										
					TURN THIS INTO A FORM FIELD BECAUSE THE SELECT * FROM MEMBER IS CAUSING THE CODE TO CRASH
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Member</li>
							<li class="field">
										
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										
										
										
										<select name="member_id" id="member_id" class="uiSelect" style="width: 190px" tabindex="6">
											<option value="">Please select...</option>

											<?php foreach ($this->members as $member): ?>
												<option value="<?php echo $member->id; ?>" 
													<?php echo (!empty($this->member_id) && $this->member_id == $member->id) ? 'selected="true"' : '' ?>><?php echo $member->first_name . " " . $member->last_name; ?></option>
											<?php endforeach; ?>
										</select>
										
									</span>
								</span>
							</li>

						</ul>
					</li>						
*/ ?>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Message</li>
							<li class="field">
								<input type="text" name="listing_message" id="listing_message" tabindex="3"  value="<?php echo isset($this->listing_message) ? $this->listing_message : '' ?>" class="uiInput" style="width: 560px" />
							</li>

						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">URL</li>
							<li class="field">
								<input type="text" name="content_link" id="content_link" tabindex="4"  value="<?php echo isset($this->content_link) ? $this->content_link : '' ?>" class="uiInput" style="width: 560px" />
							</li>

						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180" style="border-top-left-radius: 10px;">City</li>

							<li class="field">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<input type="text" name="search_city" id="search_city" class="uiSelect" style="width: 590px;" value="<?php echo isset($this->search_city) ? $this->search_city : '' ?>" />
										<input type="hidden" name="city_id" id="city_id" value="<?php echo isset($this->city_id) ? $this->city_id : '' ?>" />
									</span>
								</span>
							</li>
						</ul>
					</li>


					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Street Address</li>
							<li class="field li_235">
								<input type="text" name="street" id="street" tabindex="5" value="<?php echo isset($this->street) ? $this->street : '' ?>" class="uiInput validateNotempty" style="width: 220px" />
							</li>
							<li class="fieldname right li_160">Postal/Zip Code</li>
							<li class="field li_160">
								<input type="text" name="postal" id="postal" tabindex="6" value="<?php echo isset($this->postal) ? $this->postal : '' ?>" class="uiInput validateNotempty" style="width: 92px" />
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Price Range</li>
							<li class="field li_235">
								<span style="float: left; line-height: 30px; margin-top: 2px; width: 18px; font-weight: bold; font-size: 16px;">$</span>
								<input type="text" name="price_range_low" id="price" value="<?php if(isset($this->price_range_low)) echo $this->price_range_low ?>" class="uiInput validateCurrency" style="width: 157px" />
							</li>
							<li class="fieldname right" style="width: 10px; background: none; border: none;">To</li>
							<li class="field li_235">
								<span style="float: left; line-height: 30px; margin-top: 2px; width: 18px; font-weight: bold; font-size: 16px;">$</span>
								<input type="text" name="price_range_high" id="price" value="<?php if(isset($this->price_range_high)) echo $this->price_range_high ?>" class="uiInput validateCurrency" style="width: 157px" />
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Property Type</li>
							<li class="field li_235">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="property_type_id" id="property_type_id" class="uiSelect validateNotZero" style="width: 190px" tabindex="7">
											<option value="0">Please select...</option>
											<option value="1" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 1) ? 'selected="true"' : '' ?>>Detached</option>
											<option value="2" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 2) ? 'selected="true"' : '' ?>>Semi-detached</option>
											<option value="3" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 3) ? 'selected="true"' : '' ?>>Freehold Townhouse</option>
											<option value="4" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 4) ? 'selected="true"' : '' ?>>Condo Townhouse</option>
											<option value="5" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 5) ? 'selected="true"' : '' ?>>Condo Apartment</option>
											<option value="6" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 6) ? 'selected="true"' : '' ?>>Coop/Common Elements</option>
											<option value="7" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 7) ? 'selected="true"' : '' ?>>Attached/Townhouse</option>
										</select>
									</span>
								</span>
							</li>
							<li class="fieldname right li_160">Number of Bedrooms</li>
							<li class="field li_235">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="number_of_bedrooms" id="number_of_bedrooms" class="uiSelect validateNotempty" style="width: 190px" tabindex="8">
											<option value="">Please select...</option>
											<?php
												$bedroom_options	= array("None", 1, 2, 3, 4, "5+");
												$this->number_of_bedrooms	= ($this->number_of_bedrooms > count($bedroom_options)) ? count($bedroom_options) : $this->number_of_bedrooms;
											?>
											<?php foreach ($bedroom_options as $key => $value): ?>
											<option value="<?php echo $key ?>" <?php echo (!empty($this->number_of_bedrooms) && $this->number_of_bedrooms == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
											<?php endforeach ?>
										</select>
									</span>
								</span>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180">Number of Bathrooms</li>
							<li class="field li_235">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="number_of_bathrooms" id="number_of_bathrooms"  class="uiSelect validateNotempty" style="width: 190px" tabindex="9">
											<option value="">Please select...</option>
											<?php
												$bathroom_options	= array("None", 1, 2, 3, 4, "5+");
												$this->number_of_bathrooms	= ($this->number_of_bathrooms > count($bathroom_options)) ? count($bathroom_options) : $this->number_of_bathrooms;
											?>
											<?php foreach ($bathroom_options as $key => $value): ?>
											<option value="<?php echo $key ?>" <?php echo (!empty($this->number_of_bathrooms) && $this->number_of_bathrooms == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
											<?php endforeach ?>
										</select>
									</span>
								</span>
							</li>
							<li class="fieldname right li_160">Number of Parking</li>
							<li class="field li_235">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="number_of_parking" id="number_of_parking" class="uiSelect validateNotempty" style="width: 190px" tabindex="10">
											<option value="">Please select...</option>
											<?php
												$parking_options	= array("None", 1, 2, "3+");
												$this->number_of_parking	= ($this->number_of_parking > count($parking_options)) ? count($parking_options) : $this->number_of_parking;
											?>
											<?php foreach ($parking_options as $key => $value): ?>
											<option value="<?php echo $key ?>" <?php echo (!empty($this->number_of_parking) && $this->number_of_parking == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
											<?php endforeach ?>

										</select>
									</span>
								</span>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname li_180" style="border-bottom-left-radius: 10px;">Blast Type</li>
							<li class="field li_235">
								<!--<span class="uiSelectWrapper">
									<span class="uiSelectInner">-->
								<select name="listing_type" id="listing_type" tabindex="11" class="uiInput">

									<option value="">ALL</option>

									<option value="<?=Listing::TYPE_LISTING?>"<?php echo ($this->listing_type == Listing::TYPE_LISTING) ? " selected " : "" ?>>BLAST</option>

									<option value="<?=Listing::TYPE_CONTENT?>"<?php echo ($this->listing_type == Listing::TYPE_CONTENT) ? " selected " : "" ?>>MANUAL</option>

									<option value="<?=Listing::TYPE_IDX?>"<?php echo ($this->listing_type == Listing::TYPE_IDX) ? " selected " : "" ?>>SMLS</option>

								</select>
									<!--</span>
								</span>-->
							</li>
							<li class="fieldname right li_160">MLS Number</li>
							<li class="field li_235">
								<input type="text" name="mls_number" tabindex="12" value="<? if(isset($this->mls_number)) echo $this->mls_number; elseif(isset($this->mls_number)) echo $this->mls_number;?>" class="uiInput validateNotempty <?=($this->fields && in_array("franchise", $this->fields)?" error":"")?>" style="width: 115px" />
							</li>

						</ul>
					</li>



					<li class="uiFormRow clearfix" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
						<ul>
							<li class="fieldname li_180" style="border-bottom-left-radius: 10px;">Image Size</li>
							<li class="field li_235">

								<select name="big_image" id="big_image" tabindex="11" class="uiInput">

									<option value="">ALL</option>

									<option value="1" <?php echo ($this->big_image == 1) ? " selected " : "" ?>>Big Photos Only</option>

									<option value="0" <?php echo ($this->big_image === 0) ? " selected " : "" ?>>Small Photos Only</option>

								</select>

							</li>


						</ul>
					</li>
					
					<li class="uiFormRow clearfix" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
						<ul>							
							<li class="fieldname li_180">Expirey Date</li>
							<li class="field li_235">
								<select name="expiry_date_comparator" tabindex="11" class="uiInput" style="margin: 0 10px 0 0;">
									<option value="">Any</option>
									<option value="=" <?php echo ($this->expiry_date_comparator == '=') ? " selected " : "" ?>>Exactly</option>
									<option value=">=" <?php echo ($this->expiry_date_comparator == '>=') ? " selected " : "" ?>>From</option>
									<option value="<=" <?php echo ($this->expiry_date_comparator == '<=') ? " selected " : "" ?>>To</option>
									<option value="<" <?php echo ($this->expiry_date_comparator == '<') ? " selected " : "" ?>>Before</option>
									<option value=">" <?php echo ($this->expiry_date_comparator == '>') ? " selected " : "" ?>>After</option>
								</select>

								<input name="expiry_date" class="uiInput" value="<?=$this->expiry_date?>" style="margin-right: 5px; width: 85px;">
							</li>

						</ul>
					</li>
					
				</ul>
				<input type="submit" name="list_button" id="list_button" class="uiButton right cancel" style="width: 130px; margin-top: 10px; font-size: 26px; height: auto; padding: 5px 15px;" value="Search" />
			</form>


		</div>




		<div class="clear"></div>
		
		<table style="width: 100%; border-bottom: 0px;">
		<tr>
			<td style="border-bottom: 0px;">
				<h1>
					<?php if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo $this->selectedCityObject->name." - ";?> 
					Content (<?=number_format($this->paginator->getTotalItemCount());?>)
				</h1>
			</td>
	
			<td style="text-align: right;border-bottom: 0px;">			
			
				<select name="type" id="type" class="type_status" style="font-size: 26px; font-weight: bold; margin-right: 10px;">
					<?php
					
						$values = array(
							'ALL'     => 'ALL', 
							Listing::TYPE_LISTING => Listing::TYPE_LISTING, 
							Listing::TYPE_CONTENT => Listing::TYPE_CONTENT,
							Listing::TYPE_IDX => Listing::TYPE_IDX,
						);
						
						foreach ($values as $value => $label) :?>
							<option value="<?= $value ?>"<?= ($this->type == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
						<?php endforeach; ?>
				</select>			
	

				<select name="status" id="status" class="type_status" style="font-size: 26px; font-weight: bold; margin-right: 10px;">
					<?php
					
						$values = array(
											'ALL' => 'ALL', 
											'unpublished' => 'Unpublished', 
											'publishing'  => 'Publishing', 
											'published'   => 'Published',
											'deleted'   => 'Deleted',
											'inaccessible' => 'Pending Retirement',
											'retired-inaccessible' => 'Retired'
										);
						
						foreach ($values as $value => $label) :?>
							<option value="<?= $value ?>"<?= ($this->status == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
						<?php endforeach; ?>
				</select>


		
						<span style="float: right;">
							<a class="uiButton" href="/content/edit/0" style="float: right; color: #FFFFFF" id="new_manual_blast">New Content Blast</a>
						</span>


			</td>			
		</tr>
		</table>
		
	
		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>ID</th>
				<th>Reblast</th>
				<th>Created</th>
					
				<th>Member</th>
				<th>PubCity</th>
				
				<th>Type</th>
			
				<th>Address</th>
				
				<th>Image</th>				

				
				<th>Message</th>

				<th>Publish Status</th>
				<th>Post Result</th>
				<th>Click Count</th>
				<th style="text-align: center;">Actions</th>
			</tr>

			

			<? 

			$row=true;
			foreach($this->paginator as $listing)
			{
				echo "<tr ";
					if($row == true)
					{						
						$row = false;
						echo "class='odd' ";	
					}
					else
					{
						$row = true;	
						echo "class='' ";	
					}		

				echo ">";
				
				
				echo "<td style='vertical-align: top;'><a href='/listing/update/".$listing->id."' style='text-decoration: none;'>". $listing->id."</a></td>";
				if( ($reblast_id =  $listing->reblast_from() ) > 0 )
					echo "<td style='vertical-align: top;'><a href='/listing/update/".$reblast_id."' style='color: #3D6DCC;'>". $reblast_id."</a></td>";
				else
					echo "<td style='vertical-align: top;'></td>";


				if(isset($listing->created_at) && !empty($listing->created_at)) echo "<td style='vertical-align: top; color: #a2a2a2'>". $listing->created_at->format('M-d-Y H:i')."</td>";
				else echo "<td style='vertical-align: top; color: #a2a2a2'>&nbsp;</td>";
			

				if ($listing->member)
				{ 			
					$member = $listing->member;
					echo "<td style='vertical-align: top;'><a href='/admin/member/".$listing->member_id."' style='color: #3D6DCC;'>";
					if(isset($member->first_name)) echo $member->first_name . " ";
					if(isset($member->last_name)) echo $member->last_name;
					echo "</a></td>";
				}
				else echo "<td><a href='/listing/set_member/".$listing->id."'>Set member</a></td>";
				



	
				echo "<td style='vertical-align: top;'>";
				
					if($listing->isContent())
					{
						//SEARCH for all the CITIES that this content is PUBLISHED in
					}
					else
					{
						if(isset($listing->city_id) && !empty($listing->city_id) && isset($this->cities_array[$listing->city_id]) && ($this->cities_array[$listing->city_id])) echo "<a href='/listing/update/".$listing->id."' style='text-decoration: none;'>".$this->cities_array[$listing->city_id]."</a>";
						else echo "<a href='/listing/update/".$listing->id."' style='color: #790000;'>Unknown City</a>";
					}
					
				echo "</td>";

				echo "<td style='vertical-align: top;'>";
				echo $listing->blast_type;
				echo "</td>";


				echo "<td style='vertical-align: top;'><a href='/listing/update/".$listing->id."' style='text-decoration: none;'>";
				if(!empty($listing->street)) echo $listing->street . "<br />";
				if(!empty($listing->city)) echo $listing->city . " ";
				if(!empty($listing->postal_code)) echo $listing->postal_code . " ";

				if(!empty($listing->price)) echo "<br>PRICE:". $listing->price;

				echo "</a></td>";


				echo "<td style='vertical-align: top;'>";
				if(!empty($listing->image))
				{
					echo "<a href='/listing/update/".$listing->id."' style='text-decoration: none;'><img src='".$listing->getSizedImage(76, 76)."' width='50' height='50' border='0'/></a>";
				}
				echo "</td>";
				
				$message = null;
				$message = stripslashes(strip_tags($listing->message));
				
				
				//Need to add a "on mouse over" that pops the full message here
				if($listing->isContent())
				{
					echo "<td style='vertical-align: top; font-size: 9px;'><a href='/content/edit/".$listing->id."' style='text-decoration: none; color: #a2a2a2'>" . substr($message,0,50) . " ...</a></td>";
				}
				else
				{
					echo "<td style='vertical-align: top; width: 250px; font-size: 9px;'><a href='/listing/update/".$listing->id."' style='text-decoration: none; color: #a2a2a2'>" . substr($message,0,50) . " ...</a></td>";
				}
												
				//echo "<td style='vertical-align: top;'>".$listing->payment_id."</td>";

				echo "<td style='text-align: center;'>";
					if(isset($listing->published) && $listing->published)
					{
						echo "<span style='color: #ccc;'>Published</span>";	
					}
					else
					{
						$pub_queue = PublishingQueue::find_by_listing_id($listing->id);
						if(isset($pub_queue) && count($pub_queue)) echo "Publishing";
						else echo "Never Published";
					}

				echo "</td>";
				
				echo "<td style='text-align: center;'>";
			
					/***************
					$posts = Post::find_all_by_listing_id($listing->id);

					echo "<a href='/admin/posts/".$listing->id."'>";
					echo count($posts);
					echo "</a>";	
					***************/
									
				echo "</td>";
				
				// Click counts
				echo "<td style='text-align: center;'></td>";
			
				echo "<td style='text-align: right;' nowrap>";				

				//if(strlen($listing->message) > 140) echo " <span style='color: #790000;'>Too Big</span> | ";

				//if(empty($listing->payment_id)) echo "<a href='/listing/comp/".$listing->id."'>comp</a> | ";
				

				//Commented out for now...
				//if($listing->published != 0) echo "<a href='/listing/republish/".$listing->id."'>republish</a> | ";
				
				//We'll need this one day...
				//echo "<a href='/listing/publish-now/".$listing->id."'>publish-now</a> | ";				



				if($listing->isContent())
				{			    		
		    			echo "<a href='/admin/pubassignment/".$listing->id."'>publish</a> | ";
		    		}
			    	else
			    	{				    		
				    	if($listing->city_id != "")
				    	{ 
				    		echo "<a href='/admin/gethopper/".$listing->id."'>publish</a> | ";			    		
				    	}
				    	else
				    	{ 
				    		echo "<a href='/listing/update/".$listing->id."' style='color: #790000;'>select pub city</a> | ";
				    	}
				}

				

				
				if($listing->isContent())
				{
					echo "<a href='/content/edit/".$listing->id."'>edit</a> | ";
				}
				else
				{
					echo "<a href='/listing/update/".$listing->id."'>edit</a> | ";
				}



	
				
				if ($listing->status != 'deleted') {
					echo "<a href='/listing/delete/id/".($listing->id)."' class=\"delete-listing\">delete</a>";
				} else {
					echo "<a href='/listing/undelete/id/".($listing->id)."');'>undelete</a>";
				}
				//else echo " ---- ";
				
				
				echo "<br>";
				echo "<span style='color: #b9b9b9;'>testing: </span>";
				echo "<a href='/test/facebook_direct/".$_SESSION['member']->id."/".$listing->id."'>facebook</a> | ";
				echo "<a href='/test/fanpage_direct/".$_SESSION['member']->id."/".$listing->id."'>fanpage</a>";
				
				
				echo "</td>";



				echo "</tr>";
				if (($listing->status == 'deleted') && ($action = $listing->delete_action)) {
				?><tr><td colspan="13" style="text-align: right"><span class="gray"><em>Deleted by <?= $action->member->name() ?> on <?= $action->created_at->format('F j, Y') ?> at <?= $action->created_at->format('g:ia') ?>. Reason: <?= $action->reason ?></em></span></td></tr><?php
				}
			}

		?>
		
		
		
				
		</table>


		<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />


	</div>



</div>


<script type="text/javascript">
$(document).ready(function()
{
	$('input[name=expiry_date]').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true
	});
});
</script>


<script type="text/javascript">
$(document).ready(function()
{
	
	$('.approve-blast').click(function(event)
	{
		event.preventDefault();

		$.colorbox({width:"550px", height: "300px", inline:true, href:"#hopers-popup"});

		var buttonHref	=   $(this).attr("href");
		$("#hopers-popup button").bind("click",function(ev)
		{
			window.location =	buttonHref+"/"+$("#inventory_unit_id option:selected").val()+"/"+$("#hopper_id option:selected").val();
		});
	});

	$('.delete-listing').click(function(event)
	{
		$.colorbox({width:"650px", height: "380px;",  href:"/admin/deleteconfirm/type/listing/?target=" + encodeURIComponent( $(this).attr('href') ) });
		event.preventDefault();
	});

	$("#status").change(function (eventObject)
	{
		window.location.href = "/admin/listingstatus/" + $("#status option:selected").val();
	});

	$("#type").change(function (eventObject)
	{
		window.location.href = "/admin/listingtype/"+$("#type option:selected").val();
	});


	$('.filter_open').click(function(e){
		var $this = $(this);
		var $AdvanceSearch = $('#advancedSearch');

		if($this.hasClass('close')){
			$AdvanceSearch.hide();
			$this.removeClass('close');
			$this.parent().addClass('roundit');
		}else{
			$AdvanceSearch.show();
			$this.addClass('close');
			$this.parent().removeClass('roundit');
		}
	});



	//autocomplete
	$('#search_city').autocomplete({
		//appendTo: '#navigation_autocomplete',
		source: "/city/autocomplete",
		minLength: 2,

		select: function( event, ui ) {
			if(ui.item){//alert(ui.item.toSource())
				$('#city_id').val(ui.item.id);
			}
		},
		change:function(event, ui)
		{
			if(!ui.item){
				$('#city_id').val('');
				$(this).val('');
			}
		}

	}).focus(function() {
		if ($(this).val().search(/Select Your City/) != -1) {
			$(this).val('');
		}
	}).blur(function() {
		if ($(this).val() == '') {
			//$(this).val('Select Your City');
		}
	});

});
</script>

<style type="text/css">

/* UI Form */
ul.uiForm, 
ul.uiForm li, 
ul.uiForm li ul, 
ul.uiForm li ul li {
	padding: none!important;
	margin: none!important;
	list-style-type: none!important;
}
ul.uiForm {
	margin: 0 -20px;
	width: 1000px;
}
ul.uiForm.small {
	width: 660px;
}
.uiForm ul, .uiForm li {
	float: left;
	margin: 0px;
}

.uiForm > li{
	float: none;
}

ul.uiForm li ul li ul {
	margin-left: 2em;
	margin-bottom: 1.5em;
}
ul.uiForm li ul li ul li {
	float: none;
	list-style-type: square!important;
	margin-bottom: .5em;
}

.uiFormRowHeader {
	font-size: 21px;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-weight: 600;
	line-height: 26px;
	text-shadow: 1px 1px 0px #fff;
	color: #000;
	margin: 0 0 -1px 0!important;
	padding: 20px;
	width: 960px;
	border-top: solid 1px #fff;
	border-bottom: solid 1px #e5e5e5;
	background-color: #f2f2f2;
	background-image: -moz-linear-gradient(top, #d9d9d9, #f2f2f2); /* FF3.6 */
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #d9d9d9),color-stop(1, #f2f2f2)); /* Saf4+, Chrome */
	background: linear-gradient(#d9d9d9, #f2f2f2);
	-pie-background: linear-gradient(#d9d9d9, #f2f2f2);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm.small .uiFormRowHeader {
	padding: 20px;
	width: 620px;
}
.uiForm .uiFormRow {
	width: 1000px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .uiFormRow {
	width: 660px;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm .uiFormRow.last {
	-moz-border-radius: 0 0 8px 8px;
	-webkit-border-radius: 0 0 8px 8px;
	border-radius: 0 0 8px 8px;
	margin-bottom: -20px;
}
.uiForm .info {
	line-height: 20px;
	width: 1000px;
	padding: 10px;
	float: left;
}
.uiForm.small .info {
	width: 660px;
}
.uiForm .buttons {
	width: 960px;
	padding: 10px 20px;
	float: left;
	background: url(/images/form_divider.png) no-repeat;
}
.uiForm.small .buttons {
	width: 620px;
}
.uiForm .buttons .uiButton {
	margin-left: 5px;
}
.uiForm .fieldname {
	width: 310px;
	padding: 12px 20px 10px;
	float: left;
	font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif; 
	font-size: 16px;
	font-weight: 400;
	color: #333333;
}
.uiForm.small .fieldname {
	width: 210px;
}
.uiForm .field {
	width: 610px;
	padding: 12px 20px 10px;
	float: left;
}
.uiForm.small .field {
	width: 370px;
}
.uiForm .field span.label {
	float: left;
	margin: 0 20px 0 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiForm .fieldhelp {
	font-size: 12px!important;
	font-weight: normal!important;
	line-height: 18px!important;
	display: block!important;
	margin-top: 4px;
	color: #666666;
}
.uiFormFooterMessage {
	float: left;
	margin-top: 10px;
	line-height: 30px;
	font-size: 12px;
}
.uiFormFooterMessage strong {
	color: #333;
}

ul.normal, ol.normal {
	margin: 0px!important;
}
ul.normal li, ol.normal li {
	line-height: 24px!important;
	margin-left: 10px!important;
}

.uiForm .upsell {
	width: 960px;
	padding: 20px;
	background-color: #252525;
	border-top: solid 1px #fff;
	background-image: url(/images/upsell_new_bg.png);
	position: relative;
	behavior: url(/css/PIE.htc);
}
.uiForm .listonly {
	width: 960px;
	padding: 0 20px;
	height: 60px;
	background: #f2f2f2;
	border-top: solid 1px #999;
	-moz-box-shadow: 0 0 20px #cccccc inset;
	-webkit-box-shadow: 0 0 20px #cccccc inset;
	box-shadow: 0 0 20px #cccccc inset;
	position: relative;
	behavior: url(/css/PIE.htc);
}

/* Search Box Styles starts*/

.search_title{
	padding: 5px 10px 5px 13px;
	margin: 0 0px 0px 0px;
	background-color: #4D4D4D;
	background-image: -moz-linear-gradient(top, #666, #333);
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #666),color-stop(1, #333));
	background: linear-gradient(#666, #333);
	-pie-background: linear-gradient(#666666, #333333);
	position: relative;
	behavior: url(/css/PIE.htc);
	-moz-box-shadow: 0 1px 0 #000;
	-webkit-box-shadow: 0 1px 0 #000;
	box-shadow: 0 1px 0 #000;
	-moz-border-radius: 8px 8px 0 0 ;
	-webkit-border-radius: 8px 8px 0 0 ;
	border-radius: 8px 8px 0 0;
	position: relative;
	behavior: url(/css/PIE.htc);
	z-index: 1;
	font-family: Helvetica, Arial, Verdana, sans-serif;
	color: #FFF;
	font-weight: normal;
}

.search_title.roundit{
	border-radius: 8px 8px 8px 8px;
	margin-bottom: 20px;
}

.filter_open{
	background: url(/images/filter_opener.png) no-repeat;
	position: absolute;
	top: 1px;
	right: 10px;
	display: block;
	height: 30px;
	width: 30px;
	cursor: pointer;
}

.filter_open.close{
	background-position: 0 -30px;
}

#advancedSearch{
	display: none;
}

#advancedSearch ul.uiForm{
	margin: 0px;
}
<? /*
#advancedSearch ul.uiForm, #advancedSearch .uiForm .uiFormRow{
	width: 918px;
} */ ?>

#advancedSearch .uiForm > li{
	float: left;
}

#advancedSearch .uiFormRow, #advancedSearch .form {
	background: none !important;
}

#advancedSearch .uiFormRow{
	border-bottom: solid 1px #EDEDED;
}
#advancedSearch .fieldname {
	padding: 18px 20px 19px !important;
	background-color: #FBFBFB;
	border-right: solid 1px #EDEDED;
}
	
#advancedSearch .li_235 {
	width: 235px !important;
}

#advancedSearch .li_180 {
	width: 180px !important;
}

#advancedSearch .li_160 {
	width: 160px !important;
}

#advancedSearch .uiSelectWrapper{
	padding-bottom: 8px;
}

.uiFormRow li.field{
	padding: 10px 0px 5px 10px;
}

.uiFormRow li.fieldname{
	line-height: inherit;
}

</style>
