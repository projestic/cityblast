<div class="container_12 clearfix">




	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		
		<?php
			if(!empty($this->message)):
			?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
			<?php
			endif;
		?>
		
		
		<h1>Payments <span style="float:right;"><a id="paynow" href="#" class="uiButton" style="float: right;">Pay Now</a></span></h1>
		<table style="width: 100%; margin-top: 25px;" class="uiDataTable lineheight">
			<tr>
				<th style="padding:4px;"><?php echo ($this->display == 'all') ? 'Payment ID' : '<a href="/admin/payments/display/'. $this->display .'/sort/count/by/'. ($this->sort == 'count' ? $this->by : 'desc') .'">Purchase Count</a>' ?></th>
				
				<th style="padding:4px;">Member</th>
				<th style="padding:4px;">Type</th>
				<!--<th style="border: 1px solid #888;padding:4px;">Listing ID</th>-->
				
				<th style="padding:4px;">Transaction ID</th>				

				<th style="padding:4px;">Date/Time</th>				
				<th>CC Expires</th>
				<th>CC Last4</th>

				<!--<th>Address</th>-->
				<th style="padding:4px;">City</th>
				<th style="padding:4px;">State</th>
																		

				<!--<th>Country</th>-->
				<th style="width: 30px; padding:4px; margin: 0px;">Phone</th>

				<th style="padding:4px;"><?php echo ($this->display == 'all') ? 'Tax' : 'Total Taxes' ?></th>
				<th style="padding:4px;"><?php echo ($this->display == 'all') ? 'Amount' : '<a href="/admin/payments/display/'. $this->display .'/sort/amount/by/'. ($this->sort == 'amount' ? $this->by : 'desc') .'">Total Amount' ?></th>
				<th>Action</th>

			</tr>
			
		<?
			$row				= true;
			$running_total		= 0;
			$running_tax		= 0;
			$count_purchase	= 0;
			$alen_tax			= 0;
			$alen_total		= 0;

			foreach($this->payments as $payment)
			{
				if(stristr($payment->paypal_confirmation_number, "Manual blast created"))
					continue;

				echo "<tr ";
					if($row == true)
					{						
						$row = false;
						echo "class='odd' ";	
					}
					else
					{
						$row = true;	
						echo "class='' ";	
					}		
				echo ">";
				
				
				if ($this->display == 'all')
				{
					echo "<td style='padding:4px;'><a href='/admin/payment/".$payment->id."'>".$payment->id."</a></td>";
				}
				else
				{
					echo "<td style='text-align: right;padding:2px'>".$payment->count_purchases." </td>";
				}
				
				echo "<td style='padding:4px;'>";
				if(isset($payment->member->id)) echo "<a href='/admin/member/".$payment->member->id."'>".$payment->member->first_name . " " . $payment->member->last_name . "</a>";


				
				echo "</td>";
				
				
				echo "<td style='padding:4px;'>" . $payment->payment_type . "</td>";
					
									
				//echo "<td>";
				//	if($payment->payment_type == "blast" && !empty($payment->listing_id))
				//	{
				//		if(isset($payment->listing->id)) echo $payment->listing->id;
				//	}
				//echo "</td>";
				
				echo "<td style='padding:4px;' ";
				if(strtoupper($payment->paypal_confirmation_number) == "FREE BLAST") echo " style='color: #acacac;' ";
				echo ">";
				echo "<a href='/admin/payment/".$payment->id."'>";
					if($payment->payment_gateway && $payment->payment_gateway->code == "PAYPAL") echo $payment->paypal_confirmation_number;
					else echo $payment->stripe_payment_id;
				echo "</a></td>";

				if(isset($payment->created_at) && $payment->created_at) echo "<td nowrap style='padding:4px;'>".$payment->created_at->format('Y-m-d H:i:s')."</td>";	
				else echo "<td style='padding:4px;'></td>";
				$expire_date = (!empty($payment->cc_expire_date)) ? substr($payment->cc_expire_date,0,2)."/".substr($payment->cc_expire_date,2,6) : '';
				echo "<td style=''>".$expire_date."</td>";
				echo "<td style=''>".$payment->last_four."</td>";
				//if(isset($payment->member_id) && isset($payment->member->first_name) && isset($payment->member->last_name)) echo "<td>".$payment->member->first_name . " " . $payment->member->last_name."</td>";			
				//else echo "<td>&nbsp;</td>";
				
				//echo "<td>".$payment->address1."</td>";

				echo "<td style='padding:4px;'>".$payment->city."</td>";				
				echo "<td style='padding:4px;'>".$payment->state."</td>";
				//echo "<td>".$payment->country."</td>";				
				echo "<td style='width: 30px; padding:4px; margin: 0px;'>".$payment->phone."</td>";



				echo "<td style='text-align: right; padding:4px; ";				
				if(strtoupper($payment->paypal_confirmation_number) == "FREE BLAST") echo " color: #acacac; ";											
				echo "'>".money_format('%!i', ($this->display == 'bymember' ? $payment->t_taxes : $payment->taxes))."</td>";
				echo "<td style='text-align: right; padding:4px; ";
				if(strtoupper($payment->paypal_confirmation_number) == "FREE BLAST") echo " color: #acacac; ";
				echo "'>".money_format('%!i', ($this->display == 'bymember' ? $payment->t_amount : $payment->amount))."</td>";

				echo "<td style='padding:4px; text-align: center;'><a href='/admin/payment/".$payment->id."'>view</a></td>";	

				echo "</tr>";
				

				$running_tax += ($this->display == 'bymember' ? $payment->t_taxes : $payment->taxes);
				$running_total += ($this->display == 'bymember' ? $payment->t_amount : $payment->amount);
				$count_purchase	+= ($this->display == 'bymember' ? $payment->count_purchases : 1);
				
				if($payment->member_id == 40 or $payment->member_id == 35)
				{
					$alen_tax += ($this->display == 'bymember' ? $payment->t_taxes : $payment->taxes);
					$alen_total += ($this->display == 'bymember' ? $payment->t_amount : $payment->amount);						
				}
			}
		
			
			echo "<tr>
					<td colspan='10' style='padding:4px;'>Subtotals</td>
					<td style='text-align: right;padding:4px;'>".number_format($running_tax, 2, '.', ',')."</td>
					<td style='text-align: right;padding:4px;'>".number_format($running_total, 2, '.', ',')."</td>
					<td></td>
				</tr>";

			echo "<tr>
					<td colspan='10' style='padding:4px;'>Shaun+Alen Subtotals</td>
					<td style='text-align: right;padding:4px;'>".number_format($alen_tax, 2, '.', ',')."</td>
					<td style='text-align: right;padding:4px;'>".number_format($alen_total, 2, '.', ',')."</td>
					<td></td>
				</tr>";


			echo "<tr>
					<td colspan='10' style='padding:4px;'>Net</td>
					<td style='text-align: right;padding:4px;'>".number_format($running_tax - $alen_tax, 2, '.', ',')."</td>
					<td style='text-align: right;padding:4px;'>".number_format($running_total - $alen_total, 2, '.', ',')."</td>
					<td></td>
				</tr>";
			
			$subtotal = ($running_tax + $running_total) - ($alen_tax + $alen_total);
			echo "<tr>
					<td colspan='10' style='padding:4px;'><strong>". $count_purchase ." Total Purchases</strong></td>
					
					<td colspan='2' style='text-align: right;padding:4px;'><strong>$".number_format($subtotal, 2, '.', ',')."</strong></td>
					<td></td>
				</tr>";
			
			//echo "<tr><td colspan='11' style='text-align: right;padding:4px;'>Tax</td></tr>";	
			//echo "<tr><td colspan='11' style='text-align: right; padding:4px;font-weight: bold;'>Total</td><td style='text-align: right; font-weight: bold;'>$".money_format('%!i', ($running_tax + $running_total))."</td></tr>";
		?>
		</table>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/index/";
		var data    =	{"blast":"ON","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});

	var processStart = false;

	$("#paynow").click(function(e){
		
		e.preventDefault();

		if(processStart){
			return false;
		}
		
		$("#paynow").addClass("cancel");
		$("#paynow").html("Processing");
		var r = confirm("Make a payment now?")
		if (r == true){
			processStart = true;
			window.location = "/admin/memberpayment/<?php echo $this->member->id;?>";
		}
		else{
			$("#paynow").removeClass("cancel");
			$("#paynow").html("Pay Now");
		}

	});	
	
});
</script>