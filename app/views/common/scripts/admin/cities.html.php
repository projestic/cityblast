<div style="padding-top: 30px;">&nbsp;</div>

<div class="container_12 clearfix">


	<div class="grid_12">


		<?php
			if(!empty($this->message)):
			?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
			<?php
			endif;
		?>

	</div>

<?

	if(!empty($this->unassigned[0]->total)) echo "<div class='grid_12' style='margin-bottom: 6px;'><h3>Members With No City Assignment: ".$this->unassigned[0]->total."</h3></div>";

?>

    	<div class="grid_12"><h1>Cities/Customers List <span style="display: inline; float: right;"><a href="/city/add/" class="uiButton" style="margin-top: 0px; padding-top:0px; margin-right: 0px;">Add City</a></span></h1></div>


	<div class="grid_12" style="padding-bottom: 80px;">

		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>ID</th>
				<th>
				<? if (!empty($this->params['sort']) && $this->params['sort'] != 'nasc')
					echo "<a href='/admin/cities/sort/nasc' style='color: #ffffff;'>";
				else
					echo "<a href='/admin/cities/sort/ndesc' style='color: #ffffff;'>";
					?>
				Name</a></th>
				<th>State/Province</th>
				<th>Country</th>


				<th>Hoppers</th>


				<? /* <th>Type</th>
				<th>Domain</th>
				<th>FB APP ID</th>
				*/ ?>
				
				<th style="text-align: right">
				<? if (!empty($this->params['sort']) && $this->params['sort'] == 'adesc')
					echo "<a href='/admin/cities/sort/aasc' style='color: #ffffff;'>";
				else
					echo "<a href='/admin/cities/sort/adesc'>";
					?>
				Active agents</a></th>
				<th><a href="/admin/cities/sort/ia<?php if (!empty($this->params['sort']) && $this->params['sort'] == 'iaasc') : ?>desc<?php else : ?>asc<?php endif; ?>">Inactive agents</a></td>
				
				<th style="text-align: right">Approx Reach</th>

				<? /*<th>Active</th>*/ ?>
				<? /*<th>Blasting</th>*/ ?>
                	<th>Actions</th>
			</tr>

		<?
			$row=true;
			foreach($this->cities as $city)
			{
				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}

				echo ">";

					echo "<td style='vertical-align: top;'><a href='/city/add/". $city->id."'>". $city->id."</a></td>";
					echo "<td style='vertical-align: top;'><a href='/city/add/". $city->id."'>". $city->name . "</a></td>";
					echo "<td style='vertical-align: top;'><a href='/city/add/". $city->id."'>". $city->state . "</a></td>";
					echo "<td style='vertical-align: top;'><a href='/city/add/". $city->id."'>". $city->country . "</a></td>";
	
	
					echo "<td style='vertical-align: top;'>". $city->number_of_hoppers . "</td>";
	
	
					echo "<td style='vertical-align: top; top; text-align: right'>";
					
						if(!empty($city->current_agents)) echo "<span style='font-weight: bold;'>".$city->current_agents . "</span>";
						else echo "-";

					echo "</td>";
					echo "<td style='vertical-align: top; top; text-align: right'>";
						
						if ($city->inactive_agents > 0) echo $city->inactive_agents;
						else echo "-";

					echo "</td>";

					echo "<td style='vertical-align: top; text-align: right'>";
					
						if(!empty($city->reach)) echo number_format($city->reach);
						else echo "-";

					echo "</td>";

					//echo "<td style='vertical-align: top;'><a href='/city/add/". $city->id."'>";
						//if($city->blasting != "YES") echo $city->blasting; $city->active;
					//echo "</a></td>";
					
					
					//echo "<td style='vertical-align: top;'><a href='/city/add/". $city->id."'>";
						//if($city->blasting != "ON") echo $city->blasting;						
					//echo "</a></td>";
					
	                	echo '<td style="vertical-align: top;">';
	                	echo '<a href="/city/add/' . $city->id.'" >edit</a> | ';
	                	echo '<a href="/city/delete/' . $city->id.'" onclick="return confirm(\'Are you sure?\');">delete</a></td>';
	                	
				echo "</tr>";

			}

		?>
		</table>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/select/";
		var data    =	{"country":selected};
		$.post(url, data, function(response){
			//$('#cityselection').html(response);
			window.location.reload();
		});
	});
});
</script>