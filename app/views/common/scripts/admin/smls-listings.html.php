<div class="container_12">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

		<?php if(!empty($this->message)): ?>
			<div class="notice">
			    <h3 style="color: #005826;"><?php echo $this->message;?></h3>
			</div>
		<?php endif; ?>

	    	<div class="clear"></div>

		<table style="width: 1200px; border-bottom: 0px;">
		<tr>
			<td style="border-bottom: 0px;">
				<h1>
					
					<?php if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo $this->selectedCityObject->name." - ";?> 
					Listings (total <?=number_format($this->paginator->getTotalItemCount());?>)
					
				</h1>
				
				</td>
			<td style="text-align: right;border-bottom: 0px;">

				<select name="type" id="type" class="type_status" style="font-size: 26px; font-weight: bold; margin-right: 30px;">
					<? 
						echo "<option value='ALL' ";
						if($this->type == "ALL") echo " selected ";
						echo ">ALL</option>";
	
						echo "<option value='" . Listing::TYPE_LISTING . "' ";
						if($this->type == Listing::TYPE_LISTING) echo " selected ";
						echo ">BLAST</option>";
	
						echo "<option value='" . Listing::TYPE_CONTENT . "' ";
						if($this->type == Listing::TYPE_CONTENT) echo " selected ";
						echo ">MANUAL</option>";
	
						echo "<option value='" . Listing::TYPE_IDX . "' ";
						if($this->type == Listing::TYPE_IDX) echo " selected ";
						echo ">SMLS</option>";
					?>
				</select>			
	

				<select name="status" id="status" class="type_status" style="font-size: 26px; font-weight: bold;">
					<?
						echo "<option value='ALL' ";
						if($this->status == "ALL" || $this->status == "") echo " selected ";
						echo ">ALL</option>";
	
						echo "<option value='unpublished' ";
						if($this->status == "unpublished") echo " selected ";
						echo ">Unpublished</option>";
	
						echo "<option value='publishing' ";
						if($this->status == "publishing") echo " selected ";
						echo ">Publishing</option>";
	
						echo "<option value='published' ";
						if($this->status == "published") echo " selected ";
						echo ">Published</option>";
					?>
				</select>
			</td>
		</tr>
		</table>


		<table style="width: 1200px;">
			<tr>
				<th>ID</th>

				<th>Created</th>

				<th>PubCity</th>

				<th>Type</th>

				<th>Image</th>

				<th>Address</th>

				<th>Broker</th>

				<th>Agent List</th>

				<th>Agent's Phone</th>

				<th>Contacted?</th>

				<th>Publish Status</th>

				<th style="text-align: center;" width="20%">Actions</th>
			</tr>

		<?
			$row=true;
			foreach($this->paginator as $listing)
			{
				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='' ";
					}
					else
					{
						$row = true;
						echo "class='even' ";
					}

					//if(mb_strlen($listing->message) > 140) echo " style='color: #790000; font-weight: bold; background-color: #FFE6E6' ";

				echo ">";


				echo "<td style='vertical-align: top;'><a href='/listing/update/".$listing->id."' style='text-decoration: none;'>". $listing->id."</a></td>";
				echo "<td style='vertical-align: top; color: #a2a2a2'>". $listing->created_at->format('M-d-Y H:i')."</td>";
				/*
				if(isset($listing->member_id) && !empty($listing->member_id))
				{
					echo "<td style='vertical-align: top;'><a href='/admin/member/".$listing->member_id."' style='color: #3D6DCC;'>";
					if(isset($listing->member->first_name)) echo $listing->member->first_name . " ";
					if(isset($listing->member->last_name)) echo $listing->member->last_name;
					echo "</a></td>";
				}
				else echo "<td><a href='/listing/set_member/".$listing->id."'>Set member</a></td>";
				*/

				echo "<td style='vertical-align: top;'>";
					if(isset($listing->city_id) && !empty($listing->city_id) && ($this->cities_array[$listing->city_id])) echo "<a href='/listing/update/".$listing->id."' style='text-decoration: none;'>".$this->cities_array[$listing->city_id]."</a>";
					else echo "No City Set";

				echo "</td>";

				echo "<td style='vertical-align: top;'>";
				echo $listing->blast_type;
				echo "</td>";

				echo "<td style='vertical-align: top;'>";
				if(!empty($listing->image))
				{
					echo "<a href='/listing/update/".$listing->id."' style='text-decoration: none;'><img src='".$listing->getSizedImage(76, 76)."' width='50' height='50' border='0'/></a>";
				}
				echo "</td>";





				echo "<td style='vertical-align: top;'><a href='/listing/update/".$listing->id."' style='text-decoration: none;'>";
				if(!empty($listing->street)) echo $listing->street . "<br />";
				if(!empty($listing->city)) echo $listing->city . " ";
				if(!empty($listing->postal_code)) echo $listing->postal_code . " ";

				if(!empty($listing->price)) echo "<br>PRICE:". $listing->price;

				echo "</a></td>";


				/*
				echo "<td style='text-align: right;'><a href='/admin/payment/".$listing->payment_id."'>";
				if(isset($listing->payment->amount) && !empty($listing->payment->amount)) echo "$".number_format($listing->payment->amount, 2);
				echo "</a>";
				echo "</td>";
				*/
				echo "<td style='text-align: center;'>". $listing->broker_of_record ."</span></td>";

				echo "<td style='text-align: center;'>". $listing->list_agent ."</span></td>";

				echo "<td style='text-align: center;'>". $listing->list_agent_phone ."</span></td>";

				echo "<td style='text-align: center;'>";
				if($listing->contacted == 'yes')
				{
					echo "<span style='color: #ccc;'>Yes</span>";
				}
				else
				{
					echo "No";
				}
				echo "</td>";


				echo "<td style='text-align: center;'>";
				if($listing->published)
				{
					echo "<span style='color: #ccc;'>Yes</span>";
				}
				else
				{
					if(count($listing->posts)) echo "Republish Pending";
					else echo "Never Published";
				}
				echo "</td>";


				/*
				echo "<td style='text-align: center;'>";

					$posts = Post::find_all_by_listing_id($listing->id);

					echo "<a href='/admin/posts/".$listing->id."'>";
					echo count($posts);
					echo "</a>";

				echo "</td>";
				*/

				echo "<td style='text-align: right; line-height: 1.5;'>";

				if(mb_strlen($listing->message) > 140) echo " <span style='color: #790000;'>Too Big</span> | ";

				if(empty($listing->payment_id)) echo "<a href='/listing/comp/".$listing->id."'>comp</a> | ";


				//Commented out for now...
				//if($listing->published != 0) echo "<a href='/listing/republish/".$listing->id."'>republish</a> | ";

				//We'll need this one day...
				//echo "<a href='/listing/publish-now/".$listing->id."'>publish-now</a> | ";



				if($listing->approval == 0 )
				{

				    	if($this->selectedCityObject != "" && $this->selectedCityObject != "*")
				    	{
				    		echo "<a href='/listing/approve/".$listing->id."' class='approve-blast'>publish</a> | ";
				    	}
				    	else
				    	{
				    		echo "<span style='color: #790000;'>Select Pub City</span> | ";
				    	}
				}




				if($listing->isContent())
				{
					echo "<a href='/content/edit/".$listing->priority."/".$listing->id."'>edit</a> | ";
				}
				else
				{
					echo "<a href='/listing/update/".$listing->id."'>edit</a> | ";
				}





				echo "<a href='/listing/delete/".($listing->id)."' onclick='return window.confirm(\"Deleting the current listing\");'>delete</a>";

				//else echo " ---- ";
				echo "</td>";


				echo "</tr>";

			}

		?>
		</table>
	</div>
</div>


<div style="display: none;">
	<div id="hopers-popup" style="padding: 8px;">

           <div class="text_pop_div">

			<h2>Publish Listing</h2>

			<div style="padding-top: 8px;">
			<select name="inventory_unit_id" id="inventory_unit_id" style="font-size: 20px; width: 400px;">
				<option value="">Select An Inventory Unit</option>
				<?
					for($i=1; $i <= $this->selectedCityObject->number_of_inventory_units; $i++)
					{
						echo "<option value='".$i."' ";
						if($this->selected_inventory_unit_id == $i) echo " selected ";
						echo ">Inventory Unit #".$alpha_array[$i]."</option>";
					}
				?>
			</select>
			</div>

			<div style="padding-top: 8px;">
			<select id="hopper_id" name="hopper_id" style="font-size: 20px; width: 400px;">
			<option value="">Select An Hopper</option>
			<?
				for($i = 1; $i <= $this->selectedCityObject->number_of_hoppers; $i++)
				{
					echo '<option value="'. $i .'">Hopper #'.$i.'</option>';
				}
			?>
			</select>
			</div>
	      </div>

	      <div style="text-align: right; padding-top: 8px;"><button id="submit_hopper" style="font-size: 22px;">Publish Now</button></div>
	</div>
	
	
	
	<div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />


	
</div>


<script type="text/javascript">
$(document).ready(function()
{
	$('.approve-blast').click(function(event)
	{
		event.preventDefault();

		$.colorbox({width:"550px", height: "300px", inline:true, href:"#hopers-popup"});

		var buttonHref	=   $(this).attr("href");
		$("#hopers-popup button").bind("click",function(ev)
		{
			window.location =	buttonHref+"/"+$("#inventory_unit_id option:selected").val()+"/"+$("#hopper_id option:selected").val();
		});
	});


	$("#status").change(function (eventObject)
	{
		window.location.href = "/admin/listingstatus/" + $("#status option:selected").val();
	});

	$("#type").change(function (eventObject)
	{
		window.location.href = "/admin/listingtype/"+$("#type option:selected").val();
	});

});
</script>
