<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">

	<div class="grid_12 member_update">
		
		<form id="extraForm" action="/admin/contentfill/<?=$this->member->id;?>" method="POST">

		
		<div class="box form clearfix">



			<ul class="uiForm clearfix">
				<li class="uiFormRowHeader">Fill Personal Page - <?=$this->member->first_name . " " . $this->member->last_name;?></li>
				
				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Number of Posts</li>
					    <li class="field" style="padding-top: 20px;">
						    <input type="text" name="number_of_posts" id="number_of_posts" value="" class="uiInput" style="width: 250px" />
					    </li>
					</ul>
				</li>				
			

				<li class="uiFormRow clearfix">
					<ul>
					    <li class="fieldname">Content Type</li>
					    <li class="field" style="padding-top: 20px;">
					    		
					    		<span class="uiSelectWrapper"><span class="uiSelectInner">
					    		<select name="content_type" class="uiSelect">
								<option value=""></option>
								<option value="BIG_PHOTO">Large Photo Only</option>
								<option value="SMALL_PHOTO">Small Photo Only</option>
								<option value="">Both</option>
							</select>
					    		</span>
					    </li>
					</ul>
				</li>


			</ul>

		</div>
		
		
		<input type="submit" name="submitted" class="uiButton large right" value="Save">

		
	</form>
	
</div>			