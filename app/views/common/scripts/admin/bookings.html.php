<div class="container_12 clearfix">
	<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">
		
		<h1><?=number_format($this->paginator->getTotalItemCount());?> Bookings  <?php if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo "(".$this->selectedCityObject->name.")";?></h1>

			
		
		<table style="width: 100%;" class="uiDataTable">
			<tr>
				<th>Booking ID</th>
				<th>Created At</th>
				<th>Booking Agent</th>
				<?/*<th>Sent To</th>*/?>
				<th>Listing ID</th>
				<th>Contact Name</th>
				
				<?/*				
				<th>City</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Contact By</th>				
				<th>Best Time</th>
				*/?>
				<th>Message</th>
			</th>
		
			<?php $row = true; ?>
			<?php foreach($this->paginator as $booking) : ?>
			
				<?php $class = ($row) ? 'odd' : ''; ?>
				<?php $row   = !$row; ?>	
				
				<tr class="<?php echo $class; ?>">
					<td style="vertical-align: top;"><?php echo $booking->id; ?></td>
					<td style="vertical-align: top;">
					<?	
						//if(isset($booking->created_at) && !empty($click['created_at'])) echo date('Y-M-d', strtotime($click['created_at']));								
						echo $booking->created_at->format("Y-M-d H:i");
					?>	
					</td>					
					<td style="vertical-align: top;">
						
						<?php if(is_object($booking->booking_agent)) : ?>
							<a href="/admin/member/<?php echo $booking->booking_agent->id; ?>"><?php echo $booking->booking_agent->first_name . '&nbsp;' . $booking->booking_agent->last_name ;?></a>
						<?php endif; ?>

						<?php if(is_object($booking->booking_agent)) : ?>
														
							<? if(isset($booking->booking_agent->email_override) && !empty($booking->booking_agent->email_override)) : ?>
								<br/><a href="mailto:<?php echo $booking->booking_agent->email_override; ?>"><?php echo $booking->booking_agent->email_override ;?></a>
							
							<? else: ?>
								<br/><a href="mailto:<?php echo $booking->booking_agent->email; ?>"><?php echo $booking->booking_agent->email ;?></a>
							<? endif; ?>
						<?php endif; ?>
						
					</td>

					<td style="vertical-align: top;">
						<? if(isset($booking->listing_id) && !empty($booking->listing_id)): ?>
							<a href="/listing/view/<?php echo $booking->listing_id; ?>"><?php echo $booking->listing_id; ?></a>
						<? endif; ?>
					</td>
					<td style="vertical-align: top;"><a href="mailto:<?php echo $booking->email; ?>"><?php echo $booking->name; ?></a></td>
					
					<?/*
					<td style="vertical-align: top;"><?php echo $booking->listing->list_city->name; ?></td>
					<td style="vertical-align: top;"><a href="mailto:<?php echo $booking->email; ?>"><?php echo $booking->email; ?></a></td>
					<td style="vertical-align: top;"><?php echo $booking->phone; ?></td>
					<td style="vertical-align: top;"><?php echo $booking->contact_by; ?></td>					
					<td style="vertical-align: top;"><?php echo $booking->best_time; ?></td>
					*/?>
					<td style="vertical-align: top;"><?php echo strip_tags($booking->message); ?></td>
				</tr>					
			
			<?php endforeach ?>
		
		</table>


		<div style="float: right;">
			<?php echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?>
		</div>
		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/index/";
		var data    =	{"hasmember":"1","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});
});
</script>