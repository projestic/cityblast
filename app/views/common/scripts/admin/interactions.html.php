<div class="container_12 clearfix">
    <div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">
        <h1>
        	
        	<?=number_format($this->paginator->getTotalItemCount());?> Interactions  
        	<?php if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo "(".$this->selectedCityObject->name.")";?>
        	<span style="float: right;">Today's Interactions: <?=$this->interaction_total;?></span>
        	</h1>
        <table style="width: 100%;" class="uiDataTable lineheight">
                <tr>
                    <th>Interaction ID</th>
                    <th>Created At</th>
                    <th>Agent</th>
                    <th>Type</th>
                    <th>User Agent</th>
                    <th>User IP</th>
                    <th>Http Host</th>
                    <th>Listing ID</th>
                </th>

                <?php $row = true; ?>
                <?php foreach($this->paginator as $interaction) {?>

                        <?php $class = ($row) ? 'odd' : ''; ?>
                        <?php $row   = !$row; ?>

                        <tr class="<?php echo $class; ?>">
                                <td style="vertical-align: top;"><?php echo $interaction->id; ?></td>
                                <td style="vertical-align: top;">
                                <?
                                   echo $interaction->created_at->format("Y-M-d H:i");
                                ?>
                                </td>
                                <td style="vertical-align: top;">
                                    <?php if(is_object($interaction->agent)) : ?>
                                            <a href="/admin/member/<?php echo $interaction->agent->id; ?>"><?php echo $interaction->agent->first_name . '&nbsp;' . $interaction->agent->last_name ;?>
                                    <?php endif; ?>
                                </td>
                                <td style="vertical-align: top;"><?php echo ActiveRecord\Utils::human_attribute($interaction->type); ?></td>
                                <td style="vertical-align: top;"><?php echo $interaction->http_user_agent; ?></td>
                                <td style="vertical-align: top;"><?php echo $interaction->remote_addr; ?></td>
                                <td style="vertical-align: top;"><?php echo $interaction->remote_host; ?></td>
                                <td style="vertical-align: top;"><a href="/listing/view/<?php echo $interaction->listing->id; ?>"><?php echo $interaction->listing->id; ?></a></td>
                        </tr>
                <?php } ?>
        </table>


        <div style="float: right;" class="clearfix"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />
    </div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/index/";
		var data    =	{"hasmember":"1","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});
});
</script>