<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">

	<div class="grid_12 member_update">

		<div class="box form clearfix">
			<div class="box_section">
				<h1>Generate affiliate payout report</h1>
			</div>
			<form method="post">
				<input type="hidden" id="page" name="page" value="1">
				<ul class="uiForm clearfix">
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Affiliate</li>
							<li class="field">

								<select name="affiliate_id" id="affiliate_id">
									<?php foreach(Affiliate::all( array('select' => 'affiliate.id, first_name, last_name', 'joins' => 'JOIN member ON member_id = member.id', 'order' => 'first_name ASC, last_name ASC') ) as $affiliate) : ?>
										<option value="<?php echo $affiliate->id;?>" <?php if($affiliate->id == $this->affiliate_id) echo "selected=selected"; ?>>
											<?= $affiliate->first_name ?> <?= $affiliate->last_name; ?>
										</option>
									<?php
									endforeach;
									?>
								</select><br/>

							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">From</li>
							<li class="field">
								<input type="text" name="from" class="uiInput" value="<?= $this->from->format('Y-m-d') ?>" />
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">To</li>
							<li class="field">
								<input type="text" name="to" class="uiInput" value="<?= $this->to->format('Y-m-d') ?>" />
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname"></li>
							<li class="field">
								<input type="submit" name="download" class="uiButton" value="Download" style="margin-right: 20px;">
								<input type="submit" name="email" class="uiButton" value="Email" style="margin-right: 20px;">
								<input type="submit" name="display" class="uiButton" value="Display">
								<br/><br/>
								<em>Emailed reports will be sent to <?= $_SESSION['member']->email ?></em>
							</li>
						</ul>
					</li>
				</ul>
			</form>

		</div>

	</div>
	<?php if($this->display_report == true) {?>
	<div class="grid_12">

		<table class="uiDataTable lifetimeTable">
			<thead>
			<tr>
				<th>Payout amount</th>
				<th>Payout percentage</th>
				<th>Billing cycle</th>
				<th>Payout date</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Email</th>
				<th>Payment status</th>
				<th>Brokerage</th>
				<th>Address</th>
				<th>Phone</th>
				<th>City</th>
				<th>State/Province</th>
			</tr>
			</thead>

			<tbody>
			<?php
			foreach ($this->paginator as $payable) {
				$member = $payable->member;
				$address = $member->address;
				if ($member->address2) $address .= ', ' . $member->address2;
				$city = $member->city;
				$state = $member->state;
				if (count($member->payments)) {
					$payment = $member->payments[0];
					if ($payment->address1 != 'undefined') {
						$address = $payment->address1;
						if ($payment->address2) $address .= ', ' . $payment->address2;
					}
					$city = $payment->city;
					$state = $payment->state;
				}
			?>
				<tr>
					<td style='vertical-align: top;'><?php echo $payable->amount; ?></td>
					<td style='vertical-align: top;'><?php echo round($payable->amount/$payable->payment->amount*100); ?></td>
					<td style='vertical-align: top;'><?php echo $member->billing_cycle; ?></td>
					<td style='vertical-align: top;'><?php echo $payable->created_at->format('Y-m-d'); ?></td>
					<td style='vertical-align: top;'><?php echo $member->first_name; ?></td>
					<td style='vertical-align: top;'><?php echo $member->last_name; ?></td>
					<td style='vertical-align: top;'><?php echo $member->email; ?></td>
					<td style='vertical-align: top;'><?php echo ucfirst(str_replace('_', ' ', $member->payment_status)); ?></td>
					<td style='vertical-align: top;'><?php echo $member->brokerage; ?></td>
					<td style='vertical-align: top;'><?php echo $address; ?></td>
					<td style='vertical-align: top;'><?php echo $member->phone; ?></td>
					<td style='vertical-align: top;'><?php echo $city; ?></td>
					<td style='vertical-align: top;'><?php echo $state ?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		<div class="grid_12" style="margin-top: 0px;">
			<div style="margin-top: 0px; text-align: right; float: right;">
				<?php
				if (count($this->paginator) > 0) {
					$paginationParams = array('extraParams'=>array());
					echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $paginationParams);
				}
				?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>

<script type="text/javascript">

	$( "input[name=to], input[name=from]" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true
	});

	$("a", "#pagination").click(function(e){
		e.preventDefault();
		var $a = $(this);
		var selected_page_num = $a.html();
		$("#page").val(selected_page_num);
		$("input[name='display']").click();
	});

</script>