<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">

	<div class="grid_12 member_update">

		<div class="box form clearfix">
			<div class="box_section">
				<h1>Add Manual Referral</h1>
			</div>
			
			<form id="extraForm" action="/admin/add_manual_referral" method="POST">

			<ul class="uiForm clearfix">

				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Affiliate:</li>
						<li class="field">
							
							<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="referring_member_id" class="uiSelect">
							
							<? 																
								foreach($this->all_users as $user) 
								{
									echo "<option value='".$user->id."' ";
									echo ">".$user->first_name." ".$user->last_name."</option>";
								}
							?>
							
							</select>
							</span></span>
							
							
						</li>
						<li class="fieldname">Referred:</li>
						<li class="field">
							
							<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="referred_member_id" class="uiSelect">
								
							<? 																
								foreach($this->all_users as $user) 
								{
									echo "<option value='".$user->id."' ";
									echo ">".$user->first_name." ".$user->last_name."</option>";
								}
							?>
							
							</select>
							</span></span>
							
							
						</li>
					</ul>
				</li>
			</ul>

	
		</div>

		<input type="submit" name="submitted" class="uiButton large right" value="Add">
		
	</form>
	
</div>