<?
	foreach($this->payments as $key => $payment_count)
	{
		$date = explode("-",$key);

		$year = $date[0];
		$month = $date[1];
		$day = $date[2];
		

		$event_array[] = "{" . "\n". "title: 'Payments: ".$payment_count."', ". "\n" . "start: new Date(".$year.", ".(date('n', strtotime($key))-1).", ".date('j', strtotime($key)).", 14, 30), " . "\n". "allDay: false " . "\n". " }";
	}

	
	$remaining_revenue = 0;
	foreach($this->revenue as $key => $revenue)
	{
		$date = explode("-",$key);

		$year = $date[0];
		$month = $date[1];
		$day = $date[2];

		//echo $revenue . "<BR>";
	
		$remaining_revenue += $revenue;

		$event_array[] = "{" . "\n". "title: 'Expected: $".number_format($revenue)."', ". "\n" . "start: new Date(".$year.", ".(date('n', strtotime($key))-1)  .", ".date('j', strtotime($key)).", 16, 30), " . "\n". "allDay: false " . "\n". " }";
		//$event_array[] = "{ title: 'Expected: $'.$revenue."',start: new Date(".$year.",".$month.",".$day.", 12, 30) }";
	} 

	if(is_array($event_array)) $events = implode(",\n", $event_array);
	else $events = array();

?>

<script type='text/javascript'>

$(document).ready(function() 
{
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

		
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		editable: false,
		events: [
		
			<?=$events;?>
		
		]
	});

});

</script>



<div class="container_12 clearfix">

	<div class="grid_12" style="margin-top: 30px;">

		<link rel='stylesheet' type='text/css' href='/js/calendar/fullcalendar/fullcalendar.css' />
		<script type='text/javascript' src='/js/calendar/fullcalendar/fullcalendar.min.js'></script>
		
		
		<div id='calendar' style='margin:3em 0;font-size:13px'></div>
		
	</div>
	
	<div class="grid_12" style="text-align: right; font-size: 22px; font-weight: bold; margin-bottom: 80px; margin-top: 0px;">Remaining: $<?=number_format($remaining_revenue);?>
</div>