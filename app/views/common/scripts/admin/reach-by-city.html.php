<div class="container_12 clearfix">

	<? echo $this->render('common/member.dropdown.html.php'); ?>


	<div class="grid_12" style="margin-bottom: 80px;">
		<h1><?=count($this->city_reach);?> Cities <?php if(!empty($this->selectedCityObject) && !empty($this->selectedCityObject->name)) echo "(".$this->selectedCityObject->name.")";?></h1>
		<table style="width: 100%;" class="uiDataTable lineheight">
			<tr>
				<th>City ID</th>
				<th>City</th>
				<th>Country</th>
				<th style='text-align: center;'>Total Members</th>
				<th style='text-align: center;'>Total Reach</th>
			</tr>

		<?
			$li_total		= 0;
			$tw_total		= 0;
			$fb_total		= 0;
			$reach_total	= 0;
			$member_total	= 0;
			$row			= true;

			foreach($this->city_reach as $city)
			{
				echo "<tr ";
					if($row == true)
					{
						$row = false;
						echo "class='odd' ";
					}
					else
					{
						$row = true;
						echo "class='' ";
					}
				echo ">\n";

				echo "<td>".$city->id."</td>\n";

				echo "<td>".$city->name."</td>\n";

				echo "<td>".$city->country."</td>\n";

				echo "<td style='text-align: right;'>".number_format($city->total_member)."</td>\n";

				echo "<td style='text-align: right;'>".number_format($city->total_reach)."</td>\n";


				echo "</tr>\n\n";

				#$li_total	+= $city->linkedin_friends_count;
				#$fb_total	+= $city->facebook_friend_count;
				#$tw_total	+= $city->twitter_followers;
				$reach_total	+= $city->total_reach;
				$member_total	+= $city->total_member;
			}

		?>

		<tr><td colspan="3" style="margin-top: 12px;">Totals:</td><td style="text-align: right; font-weight: bold;margin-top: 12px;"><?=number_format($member_total)?></td>
			<td style="text-align: right; font-weight: bold;margin-top: 12px;"><?=number_format($reach_total)?></td></tr>


		</table>
	</div>

</div>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").change(function(e){
		var selected	=   $("#country option:selected").val();
		var url	    =   "/city/changecity/";
		var data    =	{"hasmember":"1","country":selected};
		$.post(url, data, function(response){
			$('#cityselection').html(response);
		});
	});
});
</script>