<?php
$allowStatus = in_array($this->selected_status, array('threestrikes', 'fbpword', 'twpword', 'expiredtoken'));
?>

<div class="container_12">
	<div class="grid_12">
		<h1><?php echo $this->pageHeader; ?></h1>
	
		<table style="width: 960px;">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Email</th>
					<th>City</th>
					<th>Member Since</th>
					<th>Token Expires</th>
					<th>Status</th>
					<th>Next Payment</th>
					<th>TW</th>
					<th>LIn</th>
					<th>FB</th>
					<th>AffID</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($this->paginator as $key => $member) : ?>
				<tr class="<?php echo ($key && $key % 2 == 1) ? 'even': ''; ?>">
					<td><?php echo $member->id; ?></td>
					<td><?php echo $member->first_name . ' ' . $member->last_name; ?></td>
					<td><?php echo $member->email; ?></td>
					<td class="nowrap"><?php if (isset($member->city_id) && isset($this->city_array[$member->city_id])) echo $this->city_array[$member->city_id]; ?></td>
					<td class="nowrap"><?php if (isset($member->created_at)) echo $member->created_at->format('M-d-y'); ?></td>
					<td class="nowrap"><?php if (isset($member->token_expires) && !empty($member->token_expires)) echo date('M-d-y', strtotime($member->token_expires)); ?></td>
					<td class="nowrap">
						<?php
							if (isset($member->account_type) && $member->account_type == 'free') {
								echo 'free';
							} elseif (isset($member->payment_status)) {
								echo $member->payment_status;
							}
						?>
					</td>
					<td class="nowrap">
						<?php
							if (isset($member->account_type) && $member->account_type == 'free') {
								echo 'free';
							} elseif (isset($member->next_payment_date) && !empty($member->next_payment_date)){
								echo date('M-d-y', strtotime($member->next_payment_date));
							}
						?>
					</td>

					<?php
						$addStyle = '';
						if (!$member->twitter_publish_flag || $member->twitter_auto_disabled) {
							$addStyle = 'color: #ACACAC;';

							if($this->display_type == 'INACTIVE') {
								$twitter_total += $member->twitter_followers;
							}
						} else {
							if ($this->display_type == 'ACTIVE') {
								$twitter_publishers++;
								$is_publisher = true;
								$twitter_total += $member->twitter_followers;
							}
						}
					?>
					<td style="text-align: right; <?php echo $addStyle; ?>">
						<?php echo number_format($member->twitter_followers); ?>
					</td>

					<?php
						$addStyle = '';
						if (!$member->linkedin_publish_flag || $member->linkedin_auto_disabled) {
							$addStyle = 'color: #ACACAC;';

							if ($this->display_type == 'INACTIVE') {
								$li_total+=$member->linkedin_friends_count;
							}
						} else {
							$linkedin_publishers++;
							if ($this->display_type == 'ACTIVE') {
								$li_total+=$member->linkedin_friends_count;
							}
						}
					?>
					<td style="text-align: right; <?php echo $addStyle; ?>">
						<?php echo number_format($member->linkedin_friends_count); ?>
					</td>

					<?php
						$addStyle = '';
						if (!$member->facebook_publish_flag || $member->facebook_auto_disabled) {
							$addStyle = 'color: #ACACAC;';

							if ($this->display_type == 'INACTIVE') {
								$fb_total += $member->facebook_friend_count;
							}
						} else {
							if ($this->display_type == 'ACTIVE') {
								$facebook_publishers++;
								$is_publisher = true;
								$fb_total += $member->facebook_friend_count;
							}
						}
					?>
					<td style="text-align: right; <?php echo $addStyle; ?>">
						<?php echo number_format($member->facebook_friend_count); ?>
					</td>
					<td>
						<?php
						if(isset($member->referring_affiliate_id) && !empty($member->referring_affiliate_id)) { 
							if (isset($affiliate_array[$member->id])) {
								echo '<a href="/admin/member/' . $member->referring_affiliate_id . '">' . $affiliate_array[$member->referring_affiliate_id] . '</a>';
							} else {
								echo $member->referring_affiliate_id;
							}
						}
						?>
					</td>
					<td class="nowrap">
						<?php
							if ($allowStatus) {
								if ($this->selected_status == "expiredtoken") {
									$conditions = "member_id=" . $member->id . " and type='EXPIRED_TOKEN'";
									$email = EmailLog::find('first', array('order' => 'id DESC', 'conditions' => $conditions));

									if ($email) {
										echo "last EXP reminder: <strong>" . $email->created_at->format('M-d-y') . "</strong><br>\n";
									} else {
										echo "last EXP reminder: <strong>never sent</strong><br>";
									}


									$conditions = "member_id=" . $member->id . " and type='AUTH_REMINDER'";
									$email = EmailLog::find('first', array('order' => 'id DESC', 'conditions' => $conditions));

									if ($email) {
										echo "last AUTH reminder: <strong style='color: #ff0000;'>" . $email->created_at->format('M-d-y') . "</strong><br>\n";
									} else {
										echo "last AUTH reminder: <strong>never sent</strong><br>";
									}
								} else {
									$conditions = "member_id=" . $member->id . " and type='AUTH_REMINDER'";
									$email = EmailLog::find('first', array('order' => 'id DESC', 'conditions' => $conditions));

									if ($email) {
										echo "last reminder: <strong>" . $email->created_at->format('M-d-y') . "</strong><br>\n";
									} else {
										echo "last reminder: <strong>never sent</strong><br>";
									}

									$conditions = "member_id=" . $member->id . " and type='EXPIRED_TOKEN'";
									$email = EmailLog::find('first', array('order' => 'id DESC', 'conditions' => $conditions));

									if ($email) {
										echo "last EXP reminder: <strong style='color: #ff0000;'>" . $email->created_at->format('M-d-y') . "</strong><br>\n";
									} else {
										echo "last EXP reminder: <strong>never sent</strong><br>";
									}
								}
							}

							echo "<a href='/admin/member/" . $member->id . "'>Edit</a>";
							echo " | <a href='/admin/memberposts/" . $member->id . "'>Posts</a>";
							echo " | <a href='/admin/emails/bymember/" . $member->id . "'>Emails</a>";
						?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<div class="clearfix" style="margin: 5px 0;">
			<?php if (count($this->paginator) > 0) {
				echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); 
			} ?>
		</div>
	</div>
</div>
