<script src="/js/jquery.mCustomScrollbar.min.js"></script>
<link rel="stylesheet" href="/css/jquery.mCustomScrollbar.css">
<script>
	$(function() {
		$('.customScrollbar').mCustomScrollbar({
			mouseWheel:true
		});
	})
</script>
<div class="row-fluid">
	<div class="seminar bannerContainer">
		<img src="/images/new-images/seminar-banner-bg.jpg" alt="">
		<div class="bannerTitle">
			<p>This Week: Attend a 100% FREE Webinar!</p>
			<p>How to Market Yourself on Facebook</p>
		</div>
		<div class="bannerBottomHolder">
			<span class="bannerBottom"><u>WITHOUT</u> annoying your friends</span>
		</div>
	</div>
	<div class="contentBox webinarDescover">
		<h2>Agents</h2>
		<div class="paddingHolder">
			<?/*<p>There’s been a dramatic change in how home buyers research their new hom purchase and over   
			<a href="//www.realtor.org/sites/default/files/Study-Digital-House-Hunt-2013-01_1.pdf">90% of home searches now begin online</a>.</p>
				
			<p>That means the vast majority of home searches are happening <i>without</i> your help.</p>
			
			<p>Where there is a crisis, often times there is also an opportunity. Yes, <a href="//www.realtor.org/sites/default/files/Study-Digital-House-Hunt-2013-01_1.pdf">90% of home searches begin online</a>
			<u>without a realtor</u>.</p> 
				
			<p>But let's repeat that and put the emphasis on another word in sentence: <a href="//www.realtor.org/sites/default/files/Study-Digital-House-Hunt-2013-01_1.pdf">90% of home searches begin online</a> WITHOUT a realtor!</p>
			
			<p>Identifying propective home <u>BUYERS WITHOUT AN AGENT = an massive OPPORTUNITY for you!</u></p>
			

			<p>Our "<a href="/blog/how-to-use-inbound-marketing-to-generate-real-estate-leads/">Inbound social media marketing</a>" webinar teaches you how to take advantage of this paradigm shift.</p>
			<p>Real estate agents are making a <u>FORTUNE</u> using only social media marketing.</p>
			<p>Don't get left behind!</p> */?>
			<p>Learn from special guest Realtor<sup>&reg;</sup> Shaun Nilsson, one of the social media wizards behind CityBlast.com, as he walks you through the time-saving secret of attracting tons of new clients while completely avoiding annoying your social media contacts.</p>
		</div>
		<ul class="webinarList">
			<li>
				<span class="icon1"></span>
				<p class="title">Master</p>
				<p class="subtitle">The secret of INBOUND MARKETING &amp; how to use it to make way more money</p>
			</li>
			<li>
				<span class="icon2"></span>
				<p class="title">Learn</p>
				<p class="subtitle">How a rookie agent used Facebook to close $165,000 in his very first year of business</p>
			</li>
			<li>
				<span class="icon3"></span>
				<p class="title">Discover</p>
				<p class="subtitle">How to systematize your Facebook Page, so you don't have to waste time on it ever again</p>
			</li>
			<li>
				<span class="icon4"></span>
				<p class="title">Understand</p>
				<p class="subtitle">What one major factor makes INBOUND MARKETING especially critical for real estate pros</p>
			</li>
			<li>
				<span class="icon5"></span>
				<p class="title">Bonus</p>
				<p class="subtitle">Live Q&amp;A with the Expert</p>
			</li>
		</ul>
		<p class="description">Reserve your space <span>NOW</span> for this <span>TOTALLY FREE</span> information session</p>
		<div class="upcomingSeminarsHolder">
			<h3>Upcoming FREE seminars</h3>
			<ul class="upcomingSeminars">
				<li>
					<div class="dateHolder">
						<div class="date">
							<div class="month">Jan</div>
							<div class="day">22</div>
						</div>
						<div class="time">10AM PST | 6PM GMT</div>
					</div>
					<a href="" class="selectLink">Click to <span>select</span></a>
				</li>
				<li>
					<div class="dateHolder">
						<div class="date">
							<div class="month">Jan</div>
							<div class="day">22</div>
						</div>
						<div class="time">10AM PST | 6PM GMT</div>
					</div>
					<a href="" class="selectLink">Click to <span>select</span></a>
				</li>
				<li>
					<div class="dateHolder">
						<div class="date">
							<div class="month">Jan</div>
							<div class="day">22</div>
						</div>
						<div class="time">10AM PST | 6PM GMT</div>
					</div>
					<a href="" class="selectLink">Click to <span>select</span></a>
				</li>
			</ul>
		</div>
	</div>
	<div class="contentBox watchSeminar">
		<h2>Take Action: Watch A Pre-Recorded Seminar</h2>
		<p>Do yourself a favor: turn off your cell phone, shut down Facebook and minimize your email client. In the next 45 minutes you're going to learn how to <u>engage more visitors</u>, <u>increase your page 
		likes</u> and <u>meet new clients</u>.</p>
		<p>You owe it to <i>yourself</i> and <i>your business</i> to get your social media marketing right!</p>
		<div class="videosHolder clearfix" style="margin-top: 12px;">
			<div class="player"></div>
			<div class="videosListHolder">
				<div class="customScrollbar">
					<ul class="videosList">
						<li class="clearfix"><img src="/images/new-images/video-icon.png" alt=""><span class="title">Part 1: Quick Tour</span></li>
						<li class="active clearfix"><img src="/images/new-images/video-icon.png" alt=""><span class="title">Part 2: Quick Tour</span></li>
						<li class="clearfix"><img src="/images/new-images/video-icon.png" alt=""><span class="title">Part 3: Quick Tour</span></li>
						<li class="clearfix"><img src="/images/new-images/video-icon.png" alt=""><span class="title">Part 4: Quick Tour</span></li>
						<li class="clearfix"><img src="/images/new-images/video-icon.png" alt=""><span class="title">Part 5: Quick Tour</span></li>
						<li class="clearfix"><img src="/images/new-images/video-icon.png" alt=""><span class="title">Part 6: Quick Tour</span></li>
						<li class="clearfix"><img src="/images/new-images/video-icon.png" alt=""><span class="title">Part 7: Quick Tour</span></li>
					</ul>
				</div>
			</div>
		</div>
		<p class="arrowsAside teamAfterText" style="text-align: center; margin-top: 20px;">Too busy to wait for a LIVE seminar?<br/>Why not check out a previously recorded one?</p>
	</div>

	<?= $this->render('common/14-day-trial.html.php');?>


	<?= $this->render('common/chat-with-an-expert.html.php');?>

	<div class="contentBox learnBlog">
		<h2>Learn more from the <?=COMPANY_NAME;?> blog</h2>
		<ul class="postsList">
			<li>
				<a href="/blog/become-an-irresistible-real-estate-agent-by-sharing-other-peoples-content-2/"><img src="/blog/wp-content/uploads/2013/12/business-woman_332-332x186.jpg" alt="Become An Irresistible Real Estate Agent By Sharing Other People’s Content" border="0"></a>
				<div class="paddingWrapper">
					<span class="category">Uncategorized</span>
					<p class="postText"><a href="/blog/become-an-irresistible-real-estate-agent-by-sharing-other-peoples-content-2/" style="color: #000;">
						Become An Irresistible Real Estate Agent By Sharing Other People’s Content</a></p>
				</div>
				<div class="bottom">
					<div class="paddingWrapper">
						<span>Grace Caroll</span>
						<span class="date">December 10, 2013</span>
					</div>
				</div>
			</li>
			<li>
				<a href= "/blog/how-to-use-inbound-marketing-to-generate-real-estate-leads/"><img src="/blog/wp-content/uploads/2013/05/img10-370x2661-332x186.jpg" alt="How To Use Inbound Marketing To Generate Real Estate Leads" border="0"></a>
				<div class="paddingWrapper">
					<span class="category">Uncategorized</span>
					<p class="postText"><a href= "/blog/how-to-use-inbound-marketing-to-generate-real-estate-leads/" style="color: #000;">
						How To Use Inbound Marketing To Generate Real Estate Leads</a></p>
				</div>
				<div class="bottom">
					<div class="paddingWrapper">
						<span>Grace Caroll</span>
						<span class="date">May 30th, 2013</span>
					</div>
				</div>
			</li>
			<li>
				<a href= "/blog/10-reasons-why-you-can-gain-more-authority-faster-with-social-media/"><img src="/blog/wp-content/uploads/2013/12/carved-chess-pieces_332x186-332x186.jpg" alt="10 Reasons Why You Gain Authority Faster with Social Media" border="0"></a>
				<div class="paddingWrapper">
					<span class="category">Uncategorized</span>
					<p class="postText"><a href= "/blog/10-reasons-why-you-can-gain-more-authority-faster-with-social-media/" style="color: #000;">
						10 Reasons Why You Gain Authority Faster with Social Media</a></p>
				</div>
				<div class="bottom">
					<div class="paddingWrapper">
						<span>Grace Caroll</span>
						<span class="date">December 10, 2013</span>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<?= $this->render('common/get-out-there-and-sell.html.php');?>
</div>