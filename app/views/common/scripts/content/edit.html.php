<!-- LOAD UPLOADIFY -->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script src="/js/jquery-ui.custom.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
  $('#file_upload').uploadify({
    'uploader'  : '/js/uploadify/uploadify.swf',
    'script'    : '/blast/uploadimage',
    'cancelImg' : '/js/uploadify/cancel.png',
    'folder'    : '/images/',
    'auto'      : true,
    'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
    'fileDesc'  : 'Image Files',
    'buttonImg' : '/images/browse-button.png',
    'removeCompleted' : false,
    'height' : 150,
    'width' : 88,
    'onComplete': function(event, ID, fileObj, response, data) 
    {
    		//alert(response);    	
    		$('#fileUploaded').val(response);
    },

    'onAllComplete': function(event, data)
    {
    		//alert(response);
			$('#file_uploadQueue').text("Complete");
    }
  });
});
</script>

<style type="text/css">
	.action-btn, .img-action-btn {
		margin: 15px 0 10px 0;
		clear: both;
	}

	.additional-desc {
		margin: 15px 0 0 0;
		display: inline-block;
		clear: both;
	}
	
	.desc h5{
		float: left;
	}
	
	.desc .counter {
		text-align: right;
		padding-right: 90px;
		color: black;
	}

	.additional-desc .description {
		height: 175px;
		width: 500px;
		margin: 0;
	}

	.action-btn .plus,
	.additional-desc .minus{
		float: right;
	}
	
	.additional-desc .minus{
		margin-top: 10px;
		margin-right: 90px;
	}

	.image-box, .additional-img {
		margin: 0 20px 0 0;
		float: left;
		display: inline-block;
	}

	.additional-img .counter {
		padding: 2px;
		color: black;
		clear: both;
	}

	.img-action-btn .img-plus-btn {
		padding-left: 5px;
		font-size: 13pt;
		font-weight: bold;
		color: black;
		cursor: pointer;
	}

	.additional-img .img-minus-btn {
		padding-left: 5px;
		font-size: 10pt;
		font-weight: bold;
		color: black;
		cursor: pointer;
	}

	.uploadifyQueue {
		background-color: #e5e5e5;
		padding: 0 0 0 10px;
	}

	.uploadifyQueueItem {
		width: 100px !important;
	}
</style>

<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="margin-top: 30px; padding-bottom: 60px;">

	<?php if(!isset($this->updated) || $this->updated === false): ?>
	<div class="grid_12">
		<div class="box form">
			<div class="box_section">
				<h1>
				Content
	
				<?				 
					if(isset($this->listing->id) && !empty($this->listing->id)) echo "#".$this->listing->id; 
								
					echo "<div style='text-align:right; font-size: 22px; font-weight: bold; float:right;'>Click Count: ";
					if(isset($this->click_stat) && isset($this->click_stat->total_clicks)) echo $this->click_stat->total_clicks;
					else echo "0";
					echo "</div>";
				?>
				</h1>
			</div>


		

			
			<?php if($this->listing_only) : ?>
				<form action="/content/saveupdatecontent/" method="POST" id="blastform">
				<input type="hidden" name="id" id="id" value="<?=$this->listing->id;?>" />
			<? else : ?>
				<form action="/content/savecontent/" method="POST" id="blastform">
				<input type="hidden" name="id" id="id" value="<?=!empty($this->listing->id) ? $this->listing->id : '';?>" />
			<? endif; ?>
			
				<input type="hidden" name="fileUploaded" id="fileUploaded" />
				
				<div class="clearfix" style="padding: 0 0 20px;">
					<ul class="uiForm full clearfix">
						<li class="uiFormRow clearfix" style="width: auto; background: none; border-top-color: #e5e5e5; border-radius: 0px;">
							<ul>
							
								<? /***************************************************
								<li class="fieldname">Default City</li>
								<li class="field">


									<span class="uiSelectWrapper"><span class="uiSelectInner">
										<select name="city" id="publish_city_id" class="uiSelect <?=($this->fields && in_array("city", $this->fields)?" error":"")?>" style="font-size: 18px; width: 250px;">
											
									<? if(isset($this->listing) && !empty($this->listing->city_id)) : ?>
																																																										
											<option value="" <?php echo (empty($this->listing->city_id)) ? "selected" : "";?>>All Cities</option>
											<?php foreach($this->cities as $city): ?>
												<option value="<?php echo $city->id;?>" <?php echo (!empty($this->listing->city_id)&& $this->listing->city_id==$city->id) ? "selected" : "";?>>
												<?php 
													echo $city->name;
													if(isset($city->state) && !empty($city->state)) echo ", " . $city->state;
												?>
													
												</option>
											<?php endforeach; ?>

									<? else : ?>
									
											<option value="" <?php echo (empty($this->selectedCityObject->id)) ? "selected" : "";?>>All Cities</option>
											<?php foreach($this->cities as $city): ?>
											<option value="<?php echo $city->id;?>" <?php echo (!empty($this->selectedCityObject->id) && $this->selectedCityObject->id==$city->id) ? "selected" : "";?>>
												<?php 
													echo $city->name;
													if(isset($city->state) && !empty($city->state)) echo ", " . $city->state;
												?>
											</option>
											<?php endforeach; ?>
									<? endif; ?>
											

										</select>
									</span>
											
									


								</li>
								
								
								<? 								
								/**************
								if( (trim($this->admin_selectedCityName) == "CANADA" || trim($this->admin_selectedCityName) == "UNITED STATES")) : ?>
									
									<li class="fieldname">Duplicate</li>
									<li class="field">
										
										<? if ($this->publish->hopper_id == 1) : ?>
											<input type="radio" name="duplicate" value="INTERNATIONAL" /><span style="font-size: 14px; font-weight: bold;">Internationally</span> (all countries)<br/>
											<input type="radio" name="duplicate" value="NATIONAL" checked /><span style="font-size: 14px; font-weight: bold;">Nationally</span> (just this country)<br/>
											<input type="radio" name="duplicate" value="NONE" /><span style="font-size: 14px; font-weight: bold;">Not At All</span>
										<? else : ?>
										
											<input type="hidden" name="duplicate" value="<?=$this->publish->duplicate;?>" />
											<span style="font-size: 14px; font-weight: bold;"><?=$this->publish->duplicate;?></span>
											
										<? endif; ?>
									</li>
								
								<? else: ?>
									<input type="hidden" name="duplicate" value="NONE" />
								<? endif; 
								
								**********************/
								?>
								
								<? /*
								<li class="fieldname">Inventory ID</li>
								<li class="field">
									<span class="uiSelectWrapper"><span class="uiSelectInner"><select name="inventory_id" style="font-size: 18px; width: 250px;" class="uiSelect">
										<?php
										for($unit = 1; $unit <= $this->selectedCityObject->number_of_inventory_units; $unit++):
										?>
										<option value="<?php echo $unit;?>" <?php echo (!empty($this->publish->inventory_id) && $this->publish->inventory_id==$unit) ? 'selected' : '';?>>Inventory Unit <?php echo $alpha_array[$unit];?></option>
										<?php
										endfor;
										?>

									</select></span></span>
								</li>
								*/ ?>
								
																
								<? /**************************
								<li class="fieldname">Hopper ID</li>
								<li class="field">
									<span class="uiSelectWrapper"><span class="uiSelectInner">
										<select name="hopper_id" id="hopper_id" style="font-size: 18px; width: 250px;" class="uiSelect">
											
											<? if(isset($this->listing) && !empty($this->listing->city_id)) : ?>
											
												<?php for($hopper = 1; $hopper <= $this->listing->list_city->number_of_hoppers; $hopper++): ?>
													<option value="<?php echo $hopper;?>" <?php echo (!empty($this->publish->hopper_id) && $this->publish->hopper_id==$hopper) ? 'selected' : '';?>>Hopper #<?php echo $hopper;?></option>
												<?php endfor; ?>
											<? else : ?>
																						
												<?php for($hopper = 1; $hopper <= $this->selectedCityObject->number_of_hoppers; $hopper++): ?>
													<option value="<?php echo $hopper;?>">Hopper #<?php echo $hopper;?></option>
												<?php endfor; ?>	
											<? endif; ?>
										</select>
									</span></span>
								</li>
								********************************/
								?>
								
								<input type="hidden" name="listing_only" value="<?=$this->listing_only;?>" />
								
								<? 
								/*
								if(!$this->listing_only) : ?>
									<li class="fieldname">Priority</li>
									<li class="field">
										<input type="text" name="priority" id="priority" value="<?=$this->publish->priority;?>" class="uiInput<?=($this->fields && in_array("priority", $this->fields)?" error":"")?>" style="width: 50px" />
									</li>

								<? endif; 
								*/
								?>
								
								<?
								/************************
									<li class="fieldname">Minimum Inventory Threshold</li>
									<li class="field"><input type="text" name="min_inventory_threshold" id="min_inventory_threshold" value="<?=$this->publish->min_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("min_inventory_threshold", $this->fields)?" error":"")?>" style="width: 150px" /></li>
									<li class="fieldname">Maximum Inventory Threshold</li>
									<li class="field"><input type="text" name="max_inventory_threshold" id="max_inventory_threshold" value="<?=$this->publish->max_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("max_inventory_threshold", $this->fields)?" error":"")?>" style="width: 150px" /></li>
								**************************/
								?>
								
								<li class="fieldname">Link Title</li>
								<li class="field"><input type="text" name="title" id="title" value="<? if(isset($this->listing->title)) echo $this->listing->title;?>" class="uiInput validateNotempty" style="width: 500px" /></li>
								<li class="fieldname">Link Description</li>
								<li class="field"><textarea name="meta_description" class="uiInput" style="width: 500px; height: 175px"><? if(isset($this->listing->description)) echo $this->listing->description;?></textarea></li>
								<li class="fieldname">Message <span style="font-weight: bold; color: #990000">*</span></li>
								<li class="field">
									<div class="action-btn clearfix">
										<input id="plus-btn" type="button" class="uiButton plus" value="Add Description" />
									</div>
									<div class="desc clearfix">
										<div class="clearfix">
											<h5 style="color: #b9b9b9;">Description #1</h5>
											<div class="counter"><span style="color: black;">0</span> chars</div>
										</div>
										<textarea name="blast" id="message" class="uiInput<?=($this->fields && in_array("blast", $this->fields)?" error":"")?> validateNotempty" style="width: 500px; height: 175px" /><?if(isset($this->listing->message)) echo $this->listing->message;?></textarea>
									</div>
									
										<?php
											
											$count = 2;
										?>
										<?php if ($this->listing && $this->listing->descriptions) : ?>
										
										<div class="clearfix"></div>
										<?php foreach ($this->listing->descriptions as $desc) : ?>
										
										
										<div class="additional-desc desc">
											
											<h5 style="color: #b9b9b9;">Description #<?=$count++;?></h5>
											<div class="counter">
												<span>0</span> chars
											</div>
											<textarea name="description[]" class="uiTextarea description"><?php  if(isset($this->listing->description)) echo $desc->description ?></textarea>
											<input type="button" class="uiButton minus cancel" value="Remove" />
										</div>
										<?php endforeach ?>
									<?php endif ?>
									
									
								</li>
								<li class="fieldname">Link</li>
							
								<li class="field">
																											
									<? if ($this->listing && $this->listing->isContent() && $this->listing->content_link) : ?>									
										<div style="clear:both;">Display URL: <?=$this->listing->content_link;?></div>
										
										<div style="clear:both;">Content Link (fully qualified: http://www.site.com)</div>
										<input type="text" name="content_link" id="content_link" value="<?=($this->listing->url_mapping)?$this->listing->url_mapping->url:'';?>" class="uiInput<?=($this->fields && in_array("content_link", $this->fields)?" error":"")?>" style="width: 450px" />
									<? else: ?>
									
										<div style="clear:both;">Content Link (fully qualified: http://www.site.com)</div>
										<input type="text" name="content_link" id="content_link" value="<? if(isset($this->listing->content_link)) echo $this->listing->content_link;?>" class="uiInput<?=($this->fields && in_array("content_link", $this->fields)?" error":"")?>" style="width: 500px" />
									<? endif; ?>

									
									
								</li>
								<li class="fieldname">Content Expires On:</li>
							
								<li class="field">									

									<input type="text" class="uiInput" name="expiry_date" id="expiry_date" value="<? 
										
											if(isset($this->listing->expiry_date) && !empty($this->listing->expiry_date))
											{
												echo $this->listing->expiry_date->format('Y-m-d');
											}
											
										?>">
								</li>
								
								<input type="hidden" name="big_image" id="big_image" value="<?=($this->listing) ? $this->listing->big_image : '0'?>"/>
								<!--
								<li class="fieldname">Big Image Content:</li>
								<li class="field">									

									<input type="checkbox" class="uiInput" name="big_image" id="big_image" value="1" <? 
										
											if(isset($this->listing->big_image) && !empty($this->listing->big_image))
											{
												echo " checked='checked' ";
											}
											
										?>">
								</li>
								-->

								<li class="fieldname">Email Passthru:</li>
								<li class="field">									

									<input type="checkbox" class="uiInput" name="email_passthru" id="email_passthru" value="1" <? 
										
											if(isset($this->listing->email_passthru) && !empty($this->listing->email_passthru))
											{
												echo " checked='checked' ";
											}
											
										?>">
								</li>
								
								<?php if ($this->copy_checkbox) : ?>
								<li class="fieldname">Copy to MortgageMingler:</li>
								<li class="field">									

									<input type="checkbox" class="uiInput" name="copy_listing" id="copy_listing" value="1" />
								</li>
								<?php endif; ?>
								<?php if ($this->copy_link) : ?>
								<li class="fieldname">Copy to MortgageMingler:</li>
								<li class="field">
									<input type="button" id="copy-to-mm" class="uiButton small" value="Copy" />&nbsp;&nbsp;&nbsp;&nbsp;<em><a href="http://screencast.com/t/KNJ9od8B" target="_blank">How-to video</a></em>
								</li>
								<?php endif; ?>
								
								<li class="fieldname">Image</li>
								<li class="field">
									<div class="image-box">
										<input id="file_upload" name="file_upload" type="file" width="88" height="30" />
									</div>


									<?php if (isset($this->listing->images) && is_array($this->listing->images)) : ?>
										<?php foreach ($this->listing->images as $idx => $img) : ?>
										<div class="additional-img">
											<input name="img_upload[]" id="imgUploaded<?php echo ($idx+1) ?>" type="file" />
											<div class="counter">
												<input type="hidden" name="imgUploaded[]" id="imgUploaded<?php echo ($idx+1) ?>File" value="<?php echo $img->image ?>" />
												<span class="img-minus-btn">[-] remove</span>
											</div>
										</div>
										<?php endforeach ?>
									<?php endif ?>
									<div class="img-action-btn">
										<span id="img-plus-btn" class="img-plus-btn">[+] add another image</span>
									</div>
								</li>
								
								<? if((isset($this->listing->image) && !empty($this->listing->image)) || (isset($this->listing->images[0]) && is_array($this->listing->images))) : ?>
								<li class="fieldname" id="main_image">Current Image</li>
								<li class="field" id="main_image_container">
									<? if(isset($this->listing->image) && !empty($this->listing->image)) : ?>
									<div style="float: left; padding-right: 5px;">
										<img src='<?= CDN_URL . $this->listing->image;?>' width='75' height='75' style='padding: 6px;' /><br/>
									</div>
									<? endif; ?>

									<?php #if (isset($this->listing->images) && is_array($this->listing->images)) : ?>
									<?php foreach ($this->listing->images as $idx => $img) : ?>
									<div style="float: left; padding-right: 5px;" id="image_<?=$img->id ;?>_container">
										<img src='<?= CDN_URL . $img->image;?>' width='75' height='75' style='padding: 6px;' /><br/>
										<input type="button" value="Delete" class="uiButton small" name="delete" onClick="deleteImage(this,'image_<?=$img->id ;?>_container',<?=$img->id ;?>);" style="margin:5px 12px;">
									</div>
									<?php endforeach ?>
									<?php #endif ?>
								</li>							
								<? endif; ?>
								
								<li class="fieldname">Tag Cloud</li>
								<li class="field">
									<div id="tagsError" style="display: none; color: rgb(255, 0, 0);"><b>Please select one or more tags</b></div>
									<table border="0">
										<tr>
										<? 	
											$i = 0;
											$franchise_content = false;
											
											foreach ($this->tags as $t) 
											{
												$i++;		

												$checked = "";
												if (!empty($this->listing->id))
												{													
													foreach ($this->selectedTags as $s)
													{
														if ($s->tag_id == $t->id)
														{
															if($t->ref_code == 'FRANCHISE') $franchise_content = true;
															$checked = "checked";
															break;
														}
													}
												}
												
												echo "<td><input type='checkbox' class='tagList' name='tags[]' id='tag_".$t->ref_code."' value='{$t->id}' {$checked} /> {$t->name}</td>";
												if ($i == 3)
												{
													$i = 0;
													echo "</tr>\n<tr>";
												}
											} 
										?>
										</tr>
									</table>
								</li>
								

								<div id="franchise_div" <? if(!$franchise_content) : ?> style="display: none;"<? endif; ?>>
									<li class="fieldname">Franchise</li>
									<li class="field">
	
										<span class="uiSelectWrapper"><span class="uiSelectInner">
										<select name="franchise_id" class="uiSelect">
											<option value=""></option>
											<?php 																
												foreach ($this->franchises as $franchise) {
													echo "<option value='".$franchise->id."' ";
													if(isset($this->listing->franchise_id) && $this->listing->franchise_id == $franchise->id) echo " selected ";
													echo ">" . $franchise->name . "</option>";
												}
											?>
										</select>
										</span></span>
	
									</li>
								</div>


								
							</ul>
							
							
					</li>




					</ul>



					
				</div>
				
				<div class="clearfix">
					<input type="button" name="submitted" id="submitted" class="uiButton large right" value="Submit">
				</div>
			
			</form>
		</div>
	</div>
</div>



<?php else: ?>
	<div class="grid_12"><h3>Your blast was successfully added!</h3>
	<p><a href="/admin/index">Return to your admin area.</a></p>
	</div>

	<div class="clearfix">&nbsp;</div>	<?php
    endif;?>
</div>

<script type="text/javascript">

var imgStartIdx	= <?php echo (isset($this->listing->images[0]) && is_array($this->listing->images)) ? count($this->listing->images) + 1 : 1 ?>;

$(document).ready(function()
{

	var modified = false;
	var save_action = $('#blastform').attr('action');

	var listing_image = '<?= isset($this->listing) ? $this->listing->image : '' ?>';

	var head_office_tag = "#tag_FRANCHISE";
	$("#tag_FRANCHISE").click(function(event){
		$("#franchise_div").slideDown();
	});

	$("#submitted").click(function(event){
		event.preventDefault();
		var errors = 0;
		
		$("#content_link").each( function()
		{
			if($(this).val() == '') {
				var con = confirm('Hey, you didnt add a link. Are you sure this is something you wanted to do?');
				if(con){
					if (!$("#big_image").is(':checked') && ($('#fileUploaded').val().length > 0  || listing_image.length > 0)) {
						$("#big_image").prop('checked', true);
						var con = confirm('Content without a link must be a big image. The "big image" option has been set for you. If this is not correct please cancel.');
						if (!con) {
							// undo change
							$("#big_image").prop('checked', false);
						}
					}
				} 
				if (!con) {
					errors++;
				}
			}
		});
		
		$(".validateNotempty").each( function()
		{
			$(this).removeClass('error');

			if($(this).val() == '') {
				errors = errors + 1;
				$(this).addClass('error');
			}
		});
		
		$("#tagsError").hide();
		var count_checked = 0;
		
		$(".tagList").each( function()
		{
			if($(this).is(':checked')) {
				count_checked = count_checked + 1;
			}
		});

		if (count_checked == 0)
		{
			$("#tagsError").show();
			errors = errors + 1;
		}
		

		if(errors == 0)
		{
			$("#blastform").submit();
		}
	});

	$('#message').bind('keyup', function() {
		var maxchar = 140;
		var cnt = $(this).val().length;

		//var remainingchar = maxchar - cnt;
		$('span',$(this).prev()).html(cnt);
	});
	
	$('#message').trigger('keyup');

	$( "#expiry_date" ).datepicker({
		defaultDate: "+30d",
		dateFormat: 'yy-mm-dd',
		changeMonth: true
	});

	
	// when ochange the publish city in the publishing assignment section
	$("#publish_city_id").change(function () {
		
		$.post("/member/changepublishingcity", {"publish_city_id": $(this).val()}, function (response) {
			
			if ( response.error !== false ) {
				return;
			}
			if ( response.options ) {
				$("#hopper_id").html(response.options);
			}
		}, "json");
		return true;
	});

	$('#copy-to-mm').click( function (e) {
		var $this = $(this);
		if (modified) {
			if (confirm('This content has been modified, and must be saved prior to copying. OK to save and copy?')) {
				$('#blastform').attr('action', save_action + '?copy_listing=1');
				$("#submitted").trigger('click');
			} else {
				$('#blastform').attr('action', save_action);
			}
			return;
		}
		$this.val('Copying ...');
		$.get('/content/copy-to-mm/<?=($this->listing) ? $this->listing->id : 0 ?>', function (result) {
			$this.val('Copied.')
			$this.unbind('click').css('opacity', .3);
		});
		e.preventDefault();
	});
	
	$('#blastform').change( function () {
		modified = true;
	});

	dynamicDescriptions();
	dynamicImages();
});


function dynamicImages ()
{
	
	/*
	$('#submitted').click(function (e) 
	{	
		e.preventDefault();
		
		if($('#publish_city_id :selected').val() == "")
		{
			alert('YOU MUST SELECT A PUBLISHING CITY!');
		}
		else
		{
			$('#blastform').submit();
		} 
	
	});
	*/
	
	
	
			
		
	$('#img-plus-btn').click(function () 
	{
		$(this).parent().before('<div class="additional-img">'+
									'<input name="img_upload[]" id="imgUploaded'+imgStartIdx+'" type="file" />'+
									'<div class="counter">'+
										'<input type="hidden" name="imgUploaded[]" id="imgUploaded'+imgStartIdx+'File" value="" />'+
										'<span class="img-minus-btn">[-] remove</span>'+
									'</div>'+
								'</div>');//alert(1)

		$('.img-minus-btn', $(this).parent().prev()).bind('click', removeImg);

		bindUploadify(imgStartIdx);
		imgStartIdx++;
	});


	function bindUploadify(id)
	{
		$('#imgUploaded'+id).uploadify({

			'uploader'  : '/js/uploadify/uploadify.swf',
			'script'    : '/blast/uploadimage',
			'cancelImg' : '/js/uploadify/cancel.png',
			'folder'    : '/images/',
			'auto'      : true,
			'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
			'fileDesc'  : 'Image Files',
			'buttonImg' : '/images/browse-button.png',
			'removeCompleted' : false,
			'height' : 150,
			'width' : 88,

			'onComplete': function(event, ID, fileObj, response, data)
			{
				//strs	= event.target.id.split('-');
				//alert(response);
				//alert(response+' --- '+event.target.id);
				$('#'+event.target.id+'File').val(response);
			},

			'onAllComplete': function(event, data)
			{
				//alert(response);
				$('#'+event.target.id+'Queue').text("Complete");
			}
		});
	}

	function removeImg()
	{
		//$('textarea', $(this).parent().parent().prev()).focus();
		$(this).unbind('click');
		$(this).unbind();
		$(this).parent().parent().remove();
	}

	$('.img-minus-btn').bind('click', removeImg);
	//$('.additional-img .description').bind('keyup', charCount);

	for (i = 1; i < imgStartIdx; i++) 
	{
		bindUploadify(i);
	}
	
	$('.additional-img').each(function () 
	{
		//$('.counter span', $(this)).first().text($('textarea', $(this)).val().length);
	})
}


function dynamicDescriptions ()
{
	$('#plus-btn').click(function () 
	{
		$(this).closest('li').append('<div class="additional-desc desc">'+
									'<div class="counter">'+
										'<span>0</span> chars'+
									'</div>'+
									'<textarea name="description[]" class="uiTextarea description"></textarea>'+
									'<input type="button" class="uiButton minus cancel" value="Remove">'+
								'</div>');
		
		$('.uiButton.minus', $(this).closest('li').children().last()).bind('click', removeDesc);
		$('.description', $(this).closest('li').children().last()).bind('keyup', charCount);
		$('textarea', $(this).closest('li').children().last()).focus();
	});


	function charCount()
	{
		$(this).prev().children('span').text($(this).val().length);

		activeTarget	= this;
	}

	function removeDesc()
	{
        activeTarget	= this;
        
        $.colorbox({
			href: false,
			innerWidth:420,
			initialWidth:450,
			initialHeight:200,
			html:"<h3>Confirmation</h3><p style='margin-bottom: 30px;'><strong>Are you sure to remove this additional description?</strong></p>"
				+"<p><input type='button' name='yes' value='Yes' onclick='confirmRemoveDesc()' /> "
				+"<input type='button' name='no' value='No' style='margin-left: 30px;' onclick='closeCBox()' /></p>",

			onComplete: function(e) {}
		});
	}

	$('.uiButton.minus').bind('click', removeDesc);
	$('.additional-desc .description').bind('keyup', charCount);

	$('.additional-desc').each(function () {
		$('.counter span', $(this)).first().text($('textarea', $(this)).val().length);
	})
}

function removeImg(targetEl)
{
	$(targetEl).unbind();
	$(targetEl).parent().parent().remove();
}

function removeDesc(targetEl)
{
	$('textarea', $(targetEl).parent().prev()).focus();
	$(targetEl).unbind();
	$(targetEl).prev().unbind();
	$(targetEl).parent().remove();
}

function confirmRemoveDesc()
{
	$('#cboxClose').trigger('click');//alert($(activeTarget).attr('class'))
	removeDesc(activeTarget);
}

function confirmRemoveImg()
{
	$('#cboxClose').trigger('click');//alert($(activeTarget).attr('class'))
	removeImg(activeTarget);
}

function closeCBox()
{
	$('#cboxClose').trigger('click');
}

function deleteImage(ths,container,image_id){
	var con = confirm("Are you sure you want to delete this image?");
	if(con){
		$(ths).val("Please wait...")
		$.post("/listing/deleteimage", 
			{listing_id: <?php echo (isset($this->listing->id)) ? $this->listing->id : '0';?>, image_id: image_id},
			function(data) {
				if(document.getElementById(container)){
					$('#'+container).remove();
				}
				if(document.getElementById(container+"_container")){
					$('#'+container+"_container").remove();
				}
			}
		);
	}
}
</script>
