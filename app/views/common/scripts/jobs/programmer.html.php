<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>

<div class="contentBox">
		
	<div class="contentBoxTitle">
		<h1>Programmer</h1>
	</div>


	<div class="strategyContainer">
		
		<h3 style="margin-top: 25px;">About Us</h3>
		<p>We're a super tight-knit family of developers (3 in total) and we're looking to add one more to our carefully growing team. We're young, ambitious and changing the face of Social Media marketing. We've 
			developed a new technology called Mass Social Media publishing and we are not only innovative, but we're utilizing leading edge technology. Do not miss this chance to be part of a small but 
			mighty organization!
		</p>
		
		
		<h3 style="margin-top: 25px;">Stuff You'll Be Doing</h3>
		
		<p>This part is a bit dry, but it's exactly what you'll be working on from day-to-day:
			
			<ul>
				<li style="list-style: circle outside; margin-left: 50px;">Construct, develop, code, debug and maintain web site applications.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Conform to in-house software development processes.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Conform to defined software design methodologies for the development and implementation of Internet based application to support all aspects of web site functionalities.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Participate in change analysis and code reviews.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Produce solutions (both design and code) based on client concept documents and discussions.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Apply rigorous testing methodologies and use cases to analyze and verify software programs, algorithms, data transformation, forms, reports and interfaces.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Timely communication of issues and status information to Team Lead concerning system development activities.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Generate application test data as necessary and validate any data conversion requirements for final implementation and production rollout.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Participate in project status review meetings with Team Lead.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Working closely with the project manager and senior staff to ensure stated business objectives are met.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Delivering on the activities required to produce code modules and well tested code.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Perform other duties as may be assigned.</li>
			</ul>	
		</p>
		
		<h3 style="margin-top: 25px;">Intangibles</h3>
		<p>Here's some of the stuff we expect but isn't really a "core" competency:
			
			<ul>
				<li style="list-style: circle outside; margin-left: 50px;">You need to be able to complete your work with NO supervision. There is no hand-holding at <?=COMPANY_NAME;?>. In return, we won't micro-manage you either!</li>
				<li style="list-style: circle outside; margin-left: 50px;">You need to show initiative and leadership skills. If something sucks, fix it. If you're not happy with something (for example a process), provide suggestions for making it better.</li>
				<li style="list-style: circle outside; margin-left: 50px;">No code zealots. If you're fanatical about things being done a certain way, this is NOT the right job for you. This is a collaborative environment where everyone has an equal say. Sometimes you'll
				get your way, sometimes you won't. If you're not comfortable with that, again, this isn't the right role.</li>
				<li style="list-style: circle outside; margin-left: 50px;">Architecture and design should be in your DNA. If you're given vague or minimal requirements, you need to be confident and competent enough to take them and apply best-practices from the
				web to transform those requirements into hard and fast code.</li>
			</ul>
			
		</p>
		
	
		<h3 style="margin-top: 25px;">Technical Requirements</h3>
	
		<p>Here's the nitty-gritty stuff that you'll need to know in order to succeed at <?=COMPANY_NAME;?>:
			
			<ul>
				<li style="list-style: circle outside; margin-left: 50px;">3+ years of production programming experience of which 2+ years of solid PHP design/coding/testing in Open Source environment building both stand-alone and WordPress architected web sites.
				<li style="list-style: circle outside; margin-left: 50px;">Experience working in a formal software development environment
				<li style="list-style: circle outside; margin-left: 50px;">1+ years of Object Orientation software coding experience for web application
				<li style="list-style: circle outside; margin-left: 50px;">Linux, MySQL, PHP 4/5, jQuery, ActiveRecords, Zend Frame Work, Apache technology experience based on LAMP model
				<li style="list-style: circle outside; margin-left: 50px;">Computer Science trained graduate or with equivalent work experience
				<li style="list-style: circle outside; margin-left: 50px;">Responsive screen design experience
				<li style="list-style: circle outside; margin-left: 50px;">HTML, Javascript (jquery/bootstrap), and CSS experience
				<li style="list-style: circle outside; margin-left: 50px;">Experience with SEO friendly web site structures
			</ul>
		</p>
	
	</div>
	
	
	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 28px;">You're one click away.</h3>
			<h4>Join our team today!</h4>
		</div>
		<div class="pull-right">
			<?/*<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>*/?>
			<a href="mailto:<?=HELP_EMAIL;?>?subject=<?=urlencode("Senior PHP Developer.");?>" class="uiButtonNew tryDemo facebook-signup-affiliate" id="tryusnow_step1_btn">Join Now!</a>
		</div>
	</div>

	
</div>