<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>

<div class="contentBox">
		
	<div class="contentBoxTitle">
		<h1>Senior Outside Sales Rep/Sales Manager</h1>
	</div>
	

	<div class="strategyContainer">
	
		<p>Young, super-fun, and firmly established Internet company, having a great time and making lots of cash. Due to our explosive growth, we need to bring on a new Senior Salesperson to handle all the leads we have coming in and more
			importantly, to help manage our new sales staff hires. The perfect candidate will also want to get out there and prospect with us.</p>
		
		<p style="margin-bottom: 20px;">If you are:

			<ul>
				<li style="list-style: circle outside; margin-left: 50px;">A self-starter with proven sales experience under your belt</li>
				<li style="list-style: circle outside; margin-left: 50px;">Hardworking and energetic, with an entrepreneurial bent</li>
				<li style="list-style: circle outside; margin-left: 50px;">Know how to manage a sales team</li>
				<li style="list-style: circle outside; margin-left: 50px;">Ready to make LOTS of money. . .</li>
				<li style="list-style: circle outside; margin-left: 50px;">We've got the gig for you!</li>
			</ul>
			
		</p>
		
		
		<h3 style="margin-top: 25px;">About Us</h3>
		<p><?=COMPANY_NAME;?> is a firmly established Internet company, still young, but currently growing like crazy. A fun, no-pressure atmosphere with a constant stream of warm leads. Looking for someone who can sell, 
		and who's not afraid to go out prospecting when needed.</p>
	
		<h3>The Perks</h3>
		<p>Extremely generous commissions, a guaranteed draw (never go without a paycheck!) and a cool, upbeat work atmosphere you'll love.</p>
	
		<p>Intermediate sales rep positions start at a guaranteed draws of $36,000k - $46,000, depending on experience.</p>
	
		<p>Experience in social media and 
			 
			 <? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>wedding or event planning  
			 <? else: ?> real estate, mortgages or finance/insurance <? endif; ?>
			 is not mandatory, but would be ideal.</p>
		
		<p>If this sounds like you, don't just sit there--email us today! We have part time and full time positions available. Contact us directly at <a href="mailto:sales@<?=APP_DOMAIN;?>">sales@<?=APP_DOMAIN;?></a> and fast-track the start of your new, successful career.</p>
	
	</div>	
	
	
	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 28px;">You're one click away.</h3>
			<h4>Join our team today!</h4>
		</div>
		<div class="pull-right">
			<?/*<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>*/?>
			<a href="mailto:<?=HELP_EMAIL;?>?subject=<?=urlencode("Senior Sales.");?>" class="uiButtonNew tryDemo facebook-signup-affiliate" id="tryusnow_step1_btn">Join Now!</a>
		</div>
	</div>

			
		
</div>