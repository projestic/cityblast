<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>

<div class="contentBox">
		
	<div class="contentBoxTitle">
		<h1>Intermediate Outside Sales Rep</h1>
	</div>
	

	<div class="strategyContainer">
	
		<h4>3 Positions Available</h4>
	
		<p style="margin-top: 20px;">How would you like a fantastic career in sales with warm leads and a guaranteed paycheck?</p>
		
		<p style="margin-bottom: 20px;">If you are a:

			<ul>
				<li style="list-style: circle outside; margin-left: 50px;">A self-starter with proven sales experience under your belt</li>
				<li style="list-style: circle outside; margin-left: 50px;">Hardworking and energetic, with an entrepreneurial bent</li>
				<li style="list-style: circle outside; margin-left: 50px;">Ready to make LOTS of money. . .</li>
				<li style="list-style: circle outside; margin-left: 50px;">We've got the gig for you!</li>
			</ul>
			
		</p>
		
		
		<h3 style="margin-top: 25px;">About Us</h3>
		<p><?=COMPANY_NAME;?> is a firmly established Internet company, still young, but currently growing like crazy. A fun, no-pressure atmosphere with <i>a constant stream of warm leads</i>. Looking for someone who can sell, 
		and who's not afraid to go out prospecting when needed.</p>
	
		<h3>The Perks</h3>
		<p>Extremely generous commissions, <u>a guaranteed draw</u> (never go without a paycheck!) and a cool, upbeat work atmosphere you'll love.</p>
	
		<p><span style="background-color: #ffff00;">Intermediate sales rep positions start at a guaranteed draws of $30,000k - $40,000, depending on experience.</span></p>
	
		<p>Experience in social media and 
			 
			 <? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>wedding or event planning  
			 <? else: ?> real estate, mortgages or finance/insurance <? endif; ?>
			 is not mandatory, but would be ideal.</p>
		
		<p>If this sounds like you, don't just sit there--email us today! We have part time and full time positions available. Contact us directly at <a href="mailto:sales@<?=APP_DOMAIN;?>">sales@<?=APP_DOMAIN;?></a> and fast-track 
			the start of your new, successful career.</p>
	
	</div>	
	
	
	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 28px;">You're one click away.</h3>
			<h4>Join our team today!</h4>
		</div>
		<div class="pull-right">
			<?/*<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>*/?>
			<a href="mailto:<?=HELP_EMAIL;?>?subject=<?=urlencode("Intermediate Sales.");?>" class="uiButtonNew tryDemo facebook-signup-affiliate" id="tryusnow_step1_btn">Join Now!</a>
		</div>
	</div>

			
		
</div>