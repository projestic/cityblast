<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>

<div class="contentBox">
		

	<div class="contentBoxTitle">
		<h1>Join Our Team!</h1>
	</div>

	
	<div class="clearfix" style="margin-top: 30px;">
		<img src="/images/join-our-team.png" />
	</div>

	
	<p style="margin-top: 20px;">Do you want to learn about Social Media from the <i>biggest personal brand management company</i> in the world?</p>
	
	<p>Are you looking for a <u>more entrepreneurial</u> work environment?</p> 
	
	<p>Do you want to work some place where you can see your efforts literally making an impact on our company?</p>
	
	<p>If this sounds like you, <?=COMPANY_NAME;?> is the right place for you.</p> 
	
	<p><span style="background-color: #ffff00;">At <?=COMPANY_NAME;?> we don't just hire people, we hire <u>the best</u> people.</span></p>
	
	<p>Even if you don't see your specific job role listed below, feel free to send us a resume, we're always looking for <u>talented</u>, <u>hard-working</u> entrepreneurial people!</p>
 

	<div class="contentBoxTitle">
		<h1>Current Openings</h1>
	</div>


	<h3 style="margin-top: 25px;">Intermediate Outside Sales Rep(x3)</h3>	
	<p>Calling all salespeople: Your perfect job is here! We've got more leads than we can handle. If you're looking for a fantastic job with warm sales, a great atmosphere, and a guaranteed salary, then read on. <a href="/jobs/sales">Learn More</a>.</p>
	

	<h3 style="margin-top: 25px;">Senior Outside Sales Rep/Sales Manager</h3>	
	<p>The most fun you'll ever have working. Period. Are you ready to join a young, super-fun, and firmly established Internet company AND make lots of cash? Then <?=COMPANY_NAME;?> is probably the right place for you. <a href="/jobs/seniorsales">Find Out More</a>.</p>

	<?/**
	<h3 style="margin-top: 25px;">Affiliate Manager</h3>	
	<p>Are you ready to ski some virgin powder white snow? <?=COMPANY_WEBSITE;?> has <u>never</u> signed up an affiliate. That means you'll be kicking off our affiliate program but more importantly,
		 you'll be able to pick off all the best affiliates because of your first-mover advantage. <a href="/jobs/affiliatemgr">Apply now</a>.</p>
	**/ ?>
	
	<h3 style="margin-top: 25px;">Zend Framework/ActiveRecord Developer</h3>	
	<p>We're a super tight-knit family of developers (3 in total) and we're looking to add one more to our carefully growing team. We're young, ambitious and 
		changing the face of Social Media marketing. <a href="/jobs/programmer">Apply now</a>.</p>
	
	
		
	<?/*	
	<h3 style="margin-top: 25px;">HTML/CSS/JQuery Guru</h3>	
	<p>These are the <?=COMPANY_WEBSITE;?> terms of service. <a href="/jobs/html">Apply now</a>.</p>	
	*/ ?>
	
	
</div>