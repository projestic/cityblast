<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">
	<div class="grid_12">
		<div class="box form">
			<div class="box_section">
				<h1>City</h1>
			</div>

			<?php if(isset($this->updated) && $this->updated === true): ?>
			
					<div class="grid_12"><h3>Your City was successfully added!</h3>
					<p><a href="/admin/cities/">Return to your cities list.</a></p>
				</div>
			
				<div class="clearfix">&nbsp;</div>	
			<?php endif; ?>


			<?php if( !empty($this->error) ) : ?>
				<div class="grid_12 error">
					<h3><?php echo $this->error;?></h3>
				</div>
			
				<div class="clearfix">&nbsp;</div>
			<?php endif; ?>


			<div class="clearfix">&nbsp;</div>

			<form action="/city/add" method="POST" class="clearfix">
				<input type="hidden" name="id" value="<?=is_object($this->city)?$this->city->id:'';?>" />
				<input type="hidden" name="number_of_hoppers" id="number_of_hoppers" value="<? echo isset($this->city->number_of_hoppers) ? $this->city->number_of_hoppers : "3"; ?>" />
				<input type="hidden" name="active" id="active" value="<? echo isset($this->city->active) ? $this->city->active : "NO"; ?>" />
				<input type="hidden" name="blasting" id="blasting" value="<? echo isset($this->city->blasting) ? $this->city->blasting : "ON"; ?>" />

			
				<ul class="clearfix uiForm full" style="margin-bottom: 20px;">
			
					<li class="uiFormRow clearfix">
						<ul class="clearfix">
							<li class="fieldname cc">Name <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="name" id="name" value="<? if(isset($this->city->name)) echo $this->city->name;?>" class="uiInput<?=($this->fields && in_array("name", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
							<li class="fieldname cc">State/Province <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="state" id="state" class="uiSelect" style="width: 350px">
											<option value="">-- Select One --</option>
											<optgroup label="States">
												<option <? if(isset($this->city->state) && $this->city->state == "Alaska") echo " selected "; ?> value="Alaska">Alaska</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Alabama") echo " selected "; ?> value="Alabama">Alabama</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Arkansas") echo " selected "; ?> value="Arkansas">Arkansas</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Arizona") echo " selected "; ?> value="Arizona">Arizona</option>
												<option <? if(isset($this->city->state) && $this->city->state == "California") echo " selected "; ?> value="California">California</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Colorado") echo " selected "; ?> value="Colorado">Colorado</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Connecticut") echo " selected "; ?> value="Connecticut">Connecticut</option>
												<option <? if(isset($this->city->state) && $this->city->state == "District of Columbia") echo " selected "; ?> value="District of Columbia">District of Columbia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Delaware") echo " selected "; ?> value="Delaware">Delaware</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Florida") echo " selected "; ?> value="Florida">Florida</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Georgia") echo " selected "; ?> value="Georgia">Georgia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Hawaii") echo " selected "; ?> value="Hawaii">Hawaii</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Iowa") echo " selected "; ?> value="Iowa">Iowa</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Idaho") echo " selected "; ?> value="Idaho">Idaho</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Illinois") echo " selected "; ?> value="Illinois">Illinois</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Indiana") echo " selected "; ?> value="Indiana">Indiana</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Kansas") echo " selected "; ?> value="Kansas">Kansas</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Kentucky") echo " selected "; ?> value="Kentucky">Kentucky</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Louisiana") echo " selected "; ?> value="Louisiana">Louisiana</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Massachusetts") echo " selected "; ?> value="Massachusetts">Massachusetts</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Maryland") echo " selected "; ?> value="Maryland">Maryland</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Maine") echo " selected "; ?> value="Maine">Maine</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Michigan") echo " selected "; ?> value="Michigan">Michigan</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Minnesota") echo " selected "; ?> value="Minnesota">Minnesota</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Missouri") echo " selected "; ?> value="Missouri">Missouri</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Mississippi") echo " selected "; ?> value="Mississippi">Mississippi</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Montana") echo " selected "; ?> value="Montana">Montana</option>
												<option <? if(isset($this->city->state) && $this->city->state == "North Carolina") echo " selected "; ?> value="North Carolina">North Carolina</option>
												<option <? if(isset($this->city->state) && $this->city->state == "North Dakota") echo " selected "; ?> value="North Dakota">North Dakota</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Nebraska") echo " selected "; ?> value="Nebraska">Nebraska</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Nevada") echo " selected "; ?> value="Nevada">Nevada</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New Hampshire") echo " selected "; ?> value="New Hampshire">New Hampshire</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New Jersey") echo " selected "; ?> value="New Jersey">New Jersey</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New Mexico") echo " selected "; ?> value="New Mexico">New Mexico</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New York") echo " selected "; ?> value="New York">New York</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Ohio") echo " selected "; ?> value="Ohio">Ohio</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Oklahoma") echo " selected "; ?> value="Oklahoma">Oklahoma</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Oregon") echo " selected "; ?> value="Oregon">Oregon</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Pennsylvania") echo " selected "; ?> value="Pennsylvania">Pennsylvania</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Rhode Island") echo " selected "; ?> value="Rhode Island">Rhode Island</option>
												<option <? if(isset($this->city->state) && $this->city->state == "South Carolina") echo " selected "; ?> value="South Carolina">South Carolina</option>
												<option <? if(isset($this->city->state) && $this->city->state == "South Dakota") echo " selected "; ?> value="South Dakota">South Dakota</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Tennessee") echo " selected "; ?> value="Tennessee">Tennessee</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Texas") echo " selected "; ?> value="Texas">Texas</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Utah") echo " selected "; ?> value="Utah">Utah</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Viginia") echo " selected "; ?> value="Viginia">Virginia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Vermont") echo " selected "; ?> value="Vermont">Vermont</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Washington") echo " selected "; ?> value="Washington">Washington</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Wisconsin") echo " selected "; ?> value="Wisconsin">Wisconsin</option>
												<option <? if(isset($this->city->state) && $this->city->state == "West Virginia") echo " selected "; ?> value="West Virginia">West Virginia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Wyoming") echo " selected "; ?> value="Wyoming">Wyoming</option>
											</optgroup>
											<optgroup label="Provinces">
												<option <? if(isset($this->city->state) && $this->city->state == "Alberta") echo " selected "; ?> value="Alberta">Alberta</option>
												<option <? if(isset($this->city->state) && $this->city->state == "British Columbia") echo " selected "; ?> value="British Columbia">British Columbia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Manitoba") echo " selected "; ?> value="Manitoba">Manitoba</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New Brunswisk") echo " selected "; ?> value="New Brunswisk">New Brunswick</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Newfoundland &amp;Labrador") echo " selected "; ?> value="Newfoundland &amp;Labrador">Newfoundland & Labrador</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Northwest Territories") echo " selected "; ?> value="Northwest Territories">Northwest Territories</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Nova Scotia") echo " selected "; ?> value="Nova Scotia">Nova Scotia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Nunavut") echo " selected "; ?> value="Nunavut">Nunavut</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Ontario") echo " selected "; ?> value="Ontario">Ontario</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Prince Edward Island") echo " selected "; ?> value="Prince Edward Island">Prince Edward Island</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Quebec") echo " selected "; ?> value="Quebec">Quebec</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Saskatchewan") echo " selected "; ?> value="Saskatchewan">Saskatchewan</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Yukon Territory") echo " selected "; ?> value="Yukon Territory">Yukon Territory</option>
											</optgroup>
										</select>
									</span>
								</span>
							</li>
							<li class="fieldname cc">Country <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="country" id="country" class="uiSelect" style="width: 350px">
											<option value="">-- Select One --</option>
											<option value="Canada" <? if(isset($this->city->country) && $this->city->country == "Canada" || !isset($this->city->country) || empty($this->city->country)	) echo " selected "; ?>>Canada</option>
											<option value="United States" <? if(isset($this->city->country) && $this->city->country == "United States") echo " selected "; ?>>United States</option>
										</select>
									</span>
								</span>
							</li>
						</ul>
					</li>
					<? /*
					<li class="uiFormRow clearfix">
						<ul class="clearfix">
							<li class="fieldname cc">State/Province <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="state" id="state" class="uiSelect" style="width: 350px">
											<option value="">-- Select One --</option>
											<optgroup label="States">
												<option <? if(isset($this->city->state) && $this->city->state == "Alaska") echo " selected "; ?> value="Alaska">Alaska</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Alabama") echo " selected "; ?> value="Alabama">Alabama</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Arkansas") echo " selected "; ?> value="Arkansas">Arkansas</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Arizona") echo " selected "; ?> value="Arizona">Arizona</option>
												<option <? if(isset($this->city->state) && $this->city->state == "California") echo " selected "; ?> value="California">California</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Colorado") echo " selected "; ?> value="Colorado">Colorado</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Connecticut") echo " selected "; ?> value="Connecticut">Connecticut</option>
												<option <? if(isset($this->city->state) && $this->city->state == "District of Columbia") echo " selected "; ?> value="District of Columbia">District of Columbia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Delaware") echo " selected "; ?> value="Delaware">Delaware</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Florida") echo " selected "; ?> value="Florida">Florida</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Georgia") echo " selected "; ?> value="Georgia">Georgia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Hawaii") echo " selected "; ?> value="Hawaii">Hawaii</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Iowa") echo " selected "; ?> value="Iowa">Iowa</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Idaho") echo " selected "; ?> value="Idaho">Idaho</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Illinois") echo " selected "; ?> value="Illinois">Illinois</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Indiana") echo " selected "; ?> value="Indiana">Indiana</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Kansas") echo " selected "; ?> value="Kansas">Kansas</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Kentucky") echo " selected "; ?> value="Kentucky">Kentucky</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Louisiana") echo " selected "; ?> value="Louisiana">Louisiana</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Massachusetts") echo " selected "; ?> value="Massachusetts">Massachusetts</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Maryland") echo " selected "; ?> value="Maryland">Maryland</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Maine") echo " selected "; ?> value="Maine">Maine</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Michigan") echo " selected "; ?> value="Michigan">Michigan</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Minnesota") echo " selected "; ?> value="Minnesota">Minnesota</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Missouri") echo " selected "; ?> value="Missouri">Missouri</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Mississippi") echo " selected "; ?> value="Mississippi">Mississippi</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Montana") echo " selected "; ?> value="Montana">Montana</option>
												<option <? if(isset($this->city->state) && $this->city->state == "North Carolina") echo " selected "; ?> value="North Carolina">North Carolina</option>
												<option <? if(isset($this->city->state) && $this->city->state == "North Dakota") echo " selected "; ?> value="North Dakota">North Dakota</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Nebraska") echo " selected "; ?> value="Nebraska">Nebraska</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Nevada") echo " selected "; ?> value="Nevada">Nevada</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New Hampshire") echo " selected "; ?> value="New Hampshire">New Hampshire</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New Jersey") echo " selected "; ?> value="New Jersey">New Jersey</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New Mexico") echo " selected "; ?> value="New Mexico">New Mexico</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New York") echo " selected "; ?> value="New York">New York</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Ohio") echo " selected "; ?> value="Ohio">Ohio</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Oklahoma") echo " selected "; ?> value="Oklahoma">Oklahoma</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Oregon") echo " selected "; ?> value="Oregon">Oregon</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Pennsylvania") echo " selected "; ?> value="Pennsylvania">Pennsylvania</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Rhode Island") echo " selected "; ?> value="Rhode Island">Rhode Island</option>
												<option <? if(isset($this->city->state) && $this->city->state == "South Carolina") echo " selected "; ?> value="South Carolina">South Carolina</option>
												<option <? if(isset($this->city->state) && $this->city->state == "South Dakota") echo " selected "; ?> value="South Dakota">South Dakota</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Tennessee") echo " selected "; ?> value="Tennessee">Tennessee</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Texas") echo " selected "; ?> value="Texas">Texas</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Utah") echo " selected "; ?> value="Utah">Utah</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Viginia") echo " selected "; ?> value="Viginia">Virginia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Vermont") echo " selected "; ?> value="Vermont">Vermont</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Washington") echo " selected "; ?> value="Washington">Washington</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Wisconsin") echo " selected "; ?> value="Wisconsin">Wisconsin</option>
												<option <? if(isset($this->city->state) && $this->city->state == "West Virginia") echo " selected "; ?> value="West Virginia">West Virginia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Wyoming") echo " selected "; ?> value="Wyoming">Wyoming</option>
											</optgroup>
											<optgroup label="Provinces">
												<option <? if(isset($this->city->state) && $this->city->state == "Alberta") echo " selected "; ?> value="Alberta">Alberta</option>
												<option <? if(isset($this->city->state) && $this->city->state == "British Columbia") echo " selected "; ?> value="British Columbia">British Columbia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Manitoba") echo " selected "; ?> value="Manitoba">Manitoba</option>
												<option <? if(isset($this->city->state) && $this->city->state == "New Brunswisk") echo " selected "; ?> value="New Brunswisk">New Brunswick</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Newfoundland &amp;Labrador") echo " selected "; ?> value="Newfoundland &amp;Labrador">Newfoundland & Labrador</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Northwest Territories") echo " selected "; ?> value="Northwest Territories">Northwest Territories</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Nova Scotia") echo " selected "; ?> value="Nova Scotia">Nova Scotia</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Nunavut") echo " selected "; ?> value="Nunavut">Nunavut</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Ontario") echo " selected "; ?> value="Ontario">Ontario</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Prince Edward Island") echo " selected "; ?> value="Prince Edward Island">Prince Edward Island</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Quebec") echo " selected "; ?> value="Quebec">Quebec</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Saskatchewan") echo " selected "; ?> value="Saskatchewan">Saskatchewan</option>
												<option <? if(isset($this->city->state) && $this->city->state == "Yukon Territory") echo " selected "; ?> value="Yukon Territory">Yukon Territory</option>
											</optgroup>
										</select>
									</span>
								</span>
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul class="clearfix">
							<li class="fieldname cc">Country <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<span class="uiSelectWrapper">
									<span class="uiSelectInner">
										<select name="country" id="country" class="uiSelect" style="width: 350px">
											<option value="">-- Select One --</option>
											<option value="Canada" <? if(isset($this->city->country) && $this->city->country == "Canada" || !isset($this->city->country) || empty($this->city->country)	) echo " selected "; ?>>Canada</option>
											<option value="United States" <? if(isset($this->city->country) && $this->city->country == "United States") echo " selected "; ?>>United States</option>
										</select>
									</span>
								</span>
							</li>
						</ul>
					</li> */ ?>

					<!--li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Active <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<select name="active" id="active"  class="uiInput" style="width: 200px" >
										<option value="NO" <? if(isset($this->city->active) && $this->city->active == "NO") echo " selected"; ?>>No</option>
										<option value="YES"<? if(isset($this->city->active) && $this->city->active == "YES") echo " selected"; ?>>Yes</option>
									</select>
							</li>
						</ul>
					</li>
			

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Blasting <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								
								<select name="blasting" id="blasting"  class="uiInput" style="width: 200px" >
										<option value="ON" <? if(isset($this->city->blasting) && $this->city->blasting == "ON") echo " selected"; ?>>On</option>
										<option value="OFF"<? if(isset($this->city->blasting) && $this->city->blasting == "OFF") echo " selected"; ?>>Off</option>
									</select>
							</li>
						</ul>
					</li>
			

				
			
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Number of Inventory Units <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="number_of_inventory_units" id="number_of_inventory_units" value="<? if(isset($this->city->number_of_inventory_units)) echo $this->city->number_of_inventory_units;?>" class="uiInput<?=($this->fields && in_array("number_of_inventory_units", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Number of Hoppers <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="number_of_hoppers" id="number_of_hoppers" value="<? if(isset($this->city->number_of_hoppers)) echo $this->city->number_of_hoppers;?>" class="uiInput<?=($this->fields && in_array("number_of_hoppers", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
						</ul>
					</li>
			
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Inventory Unit Size <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="inventory_unit_size" id="inventory_unit_size" value="<? if(isset($this->city->inventory_unit_size)) echo $this->city->inventory_unit_size;?>" class="uiInput<?=($this->fields && in_array("inventory_unit_size", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Minimum Inventory Threshold <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="min_inventory_threshold" id="min_inventory_threshold" value="<? if(isset($this->city->min_inventory_threshold)) echo $this->city->min_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("min_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
						</ul>
					</li>
					
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Maximum Inventory Threshold <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="max_inventory_threshold" id="max_inventory_threshold" value="<? if(isset($this->city->max_inventory_threshold)) echo $this->city->max_inventory_threshold;?>" class="uiInput<?=($this->fields && in_array("max_inventory_threshold", $this->fields)?" error":"")?>" style="width: 200px" />
							</li>
						</ul>
					</li-->	    	    	    
			
					
				</ul>





				<!--h2 style="margin-top: 30px;">White Label</h2>
				<ul class="uiForm clearfix">
			
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Type <span style="font-weight: bold; color: #990000">*</span></li>
							<li class="field cc" style="padding-top: 20px;">
								<select name="type" id="type"  class="uiInput" style="width: 200px" >
										<option value="INTERNAL" <? if(isset($this->city->type) && $this->city->type == "INTERNAL") echo " selected"; ?>>INTERNAL</option>
										<option value="WHITELABEL"<? if(isset($this->city->type) && $this->city->type == "WHITELABEL") echo " selected"; ?>>WHITELABEL</option>
									</select>
							</li>
						</ul>
					</li>
			
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Domain </li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="domain" id="domain" value="<? if(isset($this->city->domain)) echo $this->city->domain;?>" class="uiInput" style="width: 200px" />
							</li>
						</ul>
					</li>
			
			
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">FB APP ID </li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="facebook_app_id" id="facebook_app_id" value="<? if(isset($this->city->facebook_app_id)) echo $this->city->facebook_app_id;?>" class="uiInput" style="width: 400px" />
							</li>
						</ul>
					</li>
					
			
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">FB APP Secret </li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="facebook_app_secret" id="facebook_app_secret" value="<? if(isset($this->city->facebook_app_secret)) echo $this->city->facebook_app_secret;?>" class="uiInput" style="width: 400px" />
							</li>
						</ul>
					</li>
							
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">FB Access Token </li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="facebook_access_token" id="facebook_access_token" value="<? if(isset($this->city->facebook_access_token)) echo $this->city->facebook_access_token;?>" class="uiInput" style="width: 400px" />
							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname cc">Logo </li>
							<li class="field cc" style="padding-top: 20px;">
								<input type="text" name="logo" id="logo" value="<? if(isset($this->city->logo)) echo $this->city->logo;?>" class="uiInput" style="width: 400px" />
							</li>
						</ul>
					</li>

				</ul-->
			<? if(isset($this->city->id) && ($this->city->id)) :?>
				<input type="submit" name="submitted" class="uiButton large right" value="Save">
			<? else : ?>
				<input type="submit" name="submitted" class="uiButton large right" value="Add">
			<? endif; ?>

			</form>
		</div>
	</div>
</div>