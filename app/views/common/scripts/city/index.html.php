<select name="city" id="city" style="margin-top: 25px;width: 200px; font-size: 30px; font-weight: bold;">
	<option value="*" <?php echo (empty($this->selectedCity)||$this->selectedCity=="*") ? "selected" : "";?>>All Cities</option>
	<?php if($this->cities): ?>
		<?php foreach($this->cities as $city): ?>
			<option value="<?php echo $city->id;?>" <?php echo (!empty($this->selectedCity)&&$this->selectedCity==$city->id) ? "selected" : "";?>><?php echo $city->name;?></option>
		<?php endforeach; ?>
	<?php endif; ?>
</select>

<script type="text/javascript"  language="javascript">

	$(document).ready(function() {
	    $("#city").bind("change",function(e){
		var selected	=   $("#city option:selected").val();
		var country = $("#country option:selected").val();
		var url	    =   "/city/select/";
		var data    =	{"city_id":selected,"country":country};
		$.ajax({
		  type: 'POST',
		  url: url,
		  data: data,
		  success: function(data){
		      if(data.error==false)
			  window.location.reload();
		      else
			  alert("Not possible to change city.")
		  },
		  dataType: "json"
		});
	    });
	});
</script>