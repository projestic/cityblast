
	<div class="row-fluid">
		<div class="bannerContainer no-margin">
			<img src="/images/why-us-banner.png" alt="">
			<span class="bannerInfo">
				<span class="bannerTitle">Hire a Certified Facebook Marketing Expert<br>for Your Real Estate Business,<br>For Less Than $2/Day</span>
			</span>
			<span class="bannerBottomBlack">
				<h3>Get the power of a certified<br>real estate and social media expert,</h3>
				<p>who'll manage your Facebook, Twitter and LinkedIn accounts every day,<br>exactly how you choose (so you don't have to).</p>
			</span>
		</div>
	</div>

	<div class="contentBox why-experts">
		<div>
			<div class="case">
				<img src="/images/why-case-1.png">
				<h3>You should be out there selling homes.</h3>
				<p>But you're stuck in front of a computer, looking for something clever to <a href="/blog/become-an-irresistible-real-estate-agent-by-sharing-other-peoples-content-2/">post to your Fanpage</a>... again.</p>
			</div>
			<div class="case">
				<img src="/images/why-case-2.png">
				<h3>There <u>must</u> be a better way! What if a professional managed your social media?</h3>
				<p>Maybe an expert in both real estate and social media?  A great communicator that can give you that professional sizzle?</p>
			</div>
			<div class="case">
				<img src="/images/why-case-3.png">
				<h3>That's what CityBlast's Social Experts do.</h3>
				<p>As often as you choose, your <i>Social Expert</i> <a href="/blog/how-to-use-inbound-marketing-to-generate-real-estate-leads/">will update your Facebook, Twitter and LinkedIn</a> presence, so you don't have to.</p>
			</div>
			
			<div class="clear"></div>

			<p class="fbPostsAfterText arrowsDown center">It's like having your own team of Social Media Experts, working on your behalf. Except CityBlast's Social Experts work for you for less than $2/day.</p>

			
		<?/***
			<p>You should be out there selling homes.  Instead, you're stuck in the office, slumped over your computer desk, trying to 
				<a href="/blog/become-an-irresistible-real-estate-agent-by-sharing-other-peoples-content-2/">find an article</a> 
				or <a href="/blog/how-to-use-inbound-marketing-to-generate-real-estate-leads/">helpful piece of information</a> to post to your Facebook Page... again.</p>
			<p>Wouldn't it be great if someone else managed your social media accounts for you?  Maybe someone who is an expert in both real estate and social media?  And who is a great communicator, and can give your accounts that professional sizzle?</p>
			<p>That's what <?=COMPANY_NAME;?>'s <i>Social Experts</i> do.  As often as you choose, your <i>Social Expert</i> will post exciting 
				<a href="/blog/how-to-use-inbound-marketing-to-generate-real-estate-leads/">lead-generating content</a> to your <a href="http://www.facebook.com">Facebook</a>, <a href="http://www.twitter.com">Twitter</a> 
				and <a href="http://www.linkedin.com">LinkedIn</a> accounts, so you don't have to.  
				
		
		
			<p class="fbPostsAfterText" style="padding-left: 10px; padding-right: 10px; margin-left: 0px; margin-right: 0px; margin-top: 30px; color: #000000; text-align: center;">It's like having your own team of Social Media Experts, working on your behalf. 
				<br/>Except CityBlast's Social Experts work for you for less than $2/day.</p>
		***/?>
		
		
	
		</div>
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow"  style="display: block; left: 500px;" />
	</div>
	
	<div>
		<div class="like-bar">
			<strong>HINT: </strong>It's been proven time and again. Consistency = Trust. You don't have to take our word for it, <a href="/blog/6-reasons-why-a-lack-of-social-media-consistency-is-ruining-your-business/">here's what the experts</a> are saying!
		</div>
		
	</div>
	
	<div class="contentBox whyCB">
		<h2>But perhaps you're still wondering...</h2>
		<h3>Why should you use a <?=COMPANY_NAME;?> Expert? Is this stuff even important?</h3>

		<img src="/images/why-us-pic.png" alt="But perhaps you're still wondering..." class="thinking" style="position: absolute; margin-left:-30px">

		<div class="features">
			<div class="feature search">
				<h3>Over 90% of Home Searches Begin Online.</h3>
				<p>Today, your prospects are beginning their real estate journey before choosing an agent.</p>
				<p>Be viewed as a trusted expert when your friends and followers come looking for professional advice, by staying consistently active online.</p>
			</div>
			<div class="feature people">
				<h3>People Spend 3X More Time on Facebook Versus Any Other Site.</h3>
				<p>If you're going to be in the right place at the right time, it pays to know where to be.</p>
				<p>Staying consistent on Facebook, Twitter and LinkedIn means you'll always appear active when your prospects are ready to move.</p>
			</div>
			<div class="feature clock last">
				<h3>Experts Recommend at Minimum 1 Hour/Day for Online Marketing.</h3>
				<p>But who has an hour every day to spend on social media? You're already busy enough.</p>
				<p>CityBlast's Social Experts do the heavy lifting for you, by keeping you consistently fresh and up-to-date, freeing up your time to sell.</p>
			</div>
		</div>

		<div class="clear"></div>
	</div>
	
	<div>
		<div class="like-bar">
			<strong>HINT: </strong>Read our blog post and find out <u>10 irrefutable reasons</u> why <a href="/blog/10-reasons-why-you-can-gain-more-authority-faster-with-social-media">social media will make you look like an expert</a> <i>faster</i>.
		</div>
		
	</div>

	<?= $this->render('common/14-day-trial.html.php');?>

	<div>
		<div class="like-bar">
			<strong>HINT: </strong>"<a href="https://www.cityblast.com/blog/stop-talking-about-yourself/">You first</a>" marketing gets you more leads. Period.
		</div>
	</div>

	<?/*
	<div style="margin-bottom: 50px;">
		<a href="/seminar"><img src="/images/webinar-1000x260.jpg" border="0"></a>
	</div>
	*/ ?>
	
	<?= $this->render('common/chat-with-an-expert.html.php');?>
	
	<?= $this->render('common/get-out-there-and-sell.html.php');?>

</form>

<? /* echo $this->render('member/upfront-settings.html.php'); */ ?>