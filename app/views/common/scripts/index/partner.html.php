<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>
<div class="contentBox">

   	<div class="contentBoxTitle">        		
       	<h1>Marketing Partner.</h1>
	</div>
	

	<p>Are you an Internet Marketing Professional looking to monetize their online presence? Are you ready to turn your promotions into profits? Then the <?=COMPANY_NAME;?> Affiliate Program might be right for you.</p>
	
	

	<h3 style="margin-top: 20px;">How it works</h3>

	<p>For every member you refer to <?=COMPANY_NAME;?>, we pay you monthly commissions for the duration of that member's tenure. Commission amounts are calculated based on the total charges paid by your referred members.</p>
		
	<p style="background-color: #ffff00;"><strong>We pay a 20% commission to our affiliates for all payments, for as long as your referrals remain paying members.</strong></p>
		
	<p>Your commissions are calculated every month, and paid the following month. Easy!</p>



		
	<a href="/affiliate/signup" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 20px;" id="submitBtn">Tell Me More!</a>	
	
	<div class="clearfix"></div>
	
</div>