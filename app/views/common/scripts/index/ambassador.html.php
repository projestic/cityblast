<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>
<div class="contentBox">

   	<div class="contentBoxTitle">        		
       	<h1>Turn <?=COMPANY_NAME;?> Into <u>Your</u> Small Business.</h1>
	</div>
	
		<div class="strategyContainer">



	
		<p>Are you an <? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>Events Professional<? else:?>Agent<? endif;?> who wants to share <a href="/"><?=COMPANY_NAME;?></a> with others and <i>earn more income</i>?</p>
		<p>Apply now to become a <a href="/"><?=COMPANY_NAME;?></a> Ambassador, and <u>make extra money</u> by telling your friends and colleagues about <a href="/"><?=COMPANY_NAME;?></a>!</p>


		<h3 style="margin-top: 20px; margin-bottom: 10px;">Why Be a <?=COMPANY_NAME;?> Ambassador?</h3>

		
		<p>Once you're approved, you'll be given a custom promotional code, a page on our website 
			and tool kit that you can use to get out there and share the <a href="/"><?=COMPANY_NAME;?></a> love.  Better yet?  All approved <span style="background-color: #ffff00;"><a href="/"><?=COMPANY_NAME;?></a> Ambassadors earn 
			monthly <u>GUARANTEED</u> commissions from <a href="/"><?=COMPANY_NAME;?></a></span> - it's that simple!.</p>
	
	
		<h3 style="margin-top: 20px; margin-bottom: 10px;">Make Steady Money Doing What You Love.</h3>



		<p><a href="/"><?=COMPANY_NAME;?></a> Ambassadors make an awesome, steady recurring monthly income from <a href="/"><?=COMPANY_NAME;?></a>.  Why not share <a href="/"><?=COMPANY_NAME;?></a> with your friends and colleagues, and earn 
			monthly commissions in addition to your real estate income?  Our Ambassadors love that <a href="/"><?=COMPANY_NAME;?></a>'s steady income 
			supplements them in the slow months, and boosts them over the top in the busy ones!</p>
		
		<p>Interested in discussing the opportunity to be a <a href="/"><?=COMPANY_NAME;?></a> Amabassador?  So are we.  Click below to get started!</p>
	
	</div>
	


	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 28px;">You're one click away.</h3>
			<h4>Join the <?=COMPANY_NAME;?> Brand Ambassador Team Today!</h4>
		</div>
		<div class="pull-right">
			<?/*<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>*/?>
			<a href="mailto:<?=HELP_EMAIL;?>?subject=<?=urlencode("I'd like to help spread the word about ".COMPANY_NAME."!");?>" class="uiButtonNew tryDemo facebook-signup-affiliate" id="tryusnow_step1_btn">Talk To Us!</a>
		</div>
	</div>



	<div class="clearfix"></div>
	
</div>