<?

$member_type = $this->member_type;


$prices = array(
	'month' => number_format($member_type->getPriceMonthly('month'), 2),
	'biannual' => number_format($member_type->getPriceMonthly('biannual'), 2),
	'annual' => number_format($member_type->getPriceMonthly('annual'), 2)
);
$prices_parts = array(
	'month' => explode('.', $prices['month']),
	'biannual' => explode('.', $prices['biannual']),
	'annual' => explode('.', $prices['annual'])
);
$prices_dollars = array(
	'month' => array_shift($prices_parts['month']),
	'biannual' => array_shift($prices_parts['biannual']),
	'annual' => array_shift($prices_parts['annual'])
);
$prices_cents = array(
	'month' => array_pop($prices_parts['month']),
	'biannual' => array_pop($prices_parts['biannual']),
	'annual' => array_pop($prices_parts['annual'])
);

$signup_urls = $this->signup_urls;

?>
<!--[if IE 8]>
	<style type="text/css">
		.planContainer > li{
			border: 1px solid #666;
			width: 226px;
		}
		.planContainer > li.popular{
			width: 256px;
		}
		
		.planContainer .uiButtonNew{
			margin-left: 15px;
		}
	</style>
<![endif]-->
<div class="row-fluid">
	<div class="contentBox">
		<div class="contentBoxTitle">
			<h1>A plan to fit your Social Media needs.</h1>
		</div>
		<p><?=COMPANY_NAME;?> offers you great flexibility in pricing packages. Whether you like the freedom of month-to-month, or the awesome savings of annual renewal, there's a package to suit your needs.</p>
		
		<div class="promoBox clearfix">
			
			<div class="pull-left">
				<h5>Special Promotion - Act Now!</h5>
				<p>Start your 14-Day Free Trial today, and take a further 15% OFF any featured pricing plan - for life! Simply use the Promo Code featured at right during your account setup.</p>
				
				<?/*<p>To access your special pricing, simply use the Promo Code featured at right during your account setup.</p>*/?>
				
			</div>
			
			<div class="pull-left">
				<h5>Promo Code:</h5>
				<span class="promoCode">SUCCESS</span>
			</div>
			
		</div>
						
		<ul class="planContainer unstyled clearfix">
			<li class="pull-left">
				<div class="title">
					<h3>Monthly</h3>
					<h6>Flexible: No contracts, cancel anytime.</h6>
				</div>
				<div class="priceContainer">
					<span class="price" <? if($this->member_type->code == 'AGENT') echo 'style="padding-left: 30px;"';?>>
						<sup>$</sup><span class="dollars"><?=$prices_dollars['month']?></span><sup class="cents"><?=$prices_cents['month']?></sup>
					</span>
					<span class="timePeriod" style="margin-left: -25px; margin-top: 10px;">You pay $<?=$member_type->getPriceMonthly('month');?> every month</span>
					<span class="join">Join</span>
				</div>
				<ul class="specification unstyled">
					<li class="techSupport">Pay monthly</li>
					<li class="advOptions">No contract</li>
					<li class="storage">Cancel any time</li>
					<?/*<li class="bandwidth">1GB Bandwidth</li>*/?>
				</ul>
				<a href="<?=$signup_urls['month']?>" class="uiButtonNew">Start Free Trial.</a>
			</li>
			<li class="popular pull-left">
				<div class="title">
					<img src="/images/new-images/plan-title-green-bg.png" />
					<h3>BI-ANNUAL</h3>
					<h6>Our most popular option.</h6>
				</div>
				<div class="priceContainer">
					<span class="price" <? if($this->member_type->code == 'AGENT') echo 'style="padding-left: 30px;"';?>>
						<sup>$</sup><span class="dollars"><?=$prices_dollars['biannual']?></span><sup class="cents"><?=$prices_cents['biannual']?></sup><sub style="font-size: 10px;">/mth</sub>
					</span>
					<span class="timePeriod" style="padding-left: 6px; margin-top: 10px;">You pay $<?=round($member_type->getPriceMonthly('biannual') * 6, 2);?> for 6 months</span>
					<span class="join">Join</span>
				</div>
				<ul class="specification unstyled">
					<li class="techSupport">Pay bi-annually</li>

					<li class="advOptions">Save <strong>$<?= round( ($member_type->getPriceMonthly('month') * 12) - ($member_type->getPriceMonthly('biannual') * 12) );?></strong></li>
					<li class="storage">Better value</li>
					<?/*<li class="bandwidth">1GB Bandwidth</li>*/?>
				</ul>
				<a href="<?=$signup_urls['biannual']?>" class="uiButtonNew">Start Free Trial.</a>
			</li>
			<li class="pull-left">
				<div class="title">
					<h3>ANNUAL</h3>
					<h6>Our absolute best price.</h6>
				</div>
				<div class="priceContainer">

					<span class="price" <? if($this->member_type->code  == 'AGENT') echo 'style="padding-left: 30px;"';?>>
						<sup>$</sup><span class="dollars"><?=$prices_dollars['annual']?></span><sup class="cents"><?=$prices_cents['annual']?></sup><sub style="font-size: 10px;">/mth</sub>
					</span>
					<span class="timePeriod" style="padding-left: 0px; margin-left: -22px; margin-top: 10px;">You pay $<?=round($member_type->getPriceMonthly('annual') * 12, 2);?> for 12 months</span>
					<span class="join">Join</span>
				</div>
				<ul class="specification unstyled">
					<li class="techSupport">Pay annually</li>
					<li class="advOptions">Save <strong>$<?= round( ($member_type->getPriceMonthly('month') * 12) - ($member_type->getPriceMonthly('annual') * 12) );?></strong></li>
					<li class="storage">Best value</li>
					<?/*<li class="bandwidth">1GB Bandwidth</li>*/?>
				</ul>
				<a href="<?=$signup_urls['annual']?>" class="uiButtonNew">Start Free Trial.</a>
			</li>
		</ul>
		
		

		<ul class="mobilePlanContainer unstyled">
			<li class="clearfix">
				<div class="pull-left">
					<div class="title">
						<h3>Monthly.</h3>
						<h6>Flexible: No contracts, cancel anytime.</h6>
					</div>
					<ul class="specification unstyled">
						<li class="techSupport">24/7 Tech Support</li>
						<li class="advOptions">Advanced Options</li>
						<li class="storage">100GB Storage</li>
						<li class="bandwidth">1GB Bandwidth</li>
					</ul>
				</div>
				<div class="pull-left right">
					<div class="priceContainer">
						<span class="price">
							<sup>&dollar;</sup><span class="dollars"><?=$prices_dollars['month']?></span><sup class="cents"><?=$prices_cents['month']?><sup>*</sup></sup>
						</span>
						<span class="timePeriod">Monthly</span>
						<span class="join">Join</span>
					</div>
					<a href="<?=$signup_urls['month']?>" class="uiButtonNew">Start Free Trial.</a>
				</div>
			</li>
			<li class="clearfix popular">
				<div class="pull-left">
					<div class="title">
						<img src="/images/new-images/small-plan-title-green-bg.png" />
						<h3>Bi-Annual</h3>
						<h6>Our most popular option.</h6>
					</div>
					<ul class="specification unstyled">
						<li class="techSupport">24/7 Tech Support</li>
						<li class="advOptions">Advanced Options</li>
						<li class="storage">100GB Storage</li>
						<li class="bandwidth">1GB Bandwidth</li>
					</ul>
				</div>
				<div class="pull-left right">
					<div class="priceContainer">
						<span class="price">
							<sup>&dollar;</sup><span class="dollars"><?=$prices_dollars['biannual']?></span><sup class="cents"><?=$prices_cents['biannual']?><sup>*</sup></sup><sub style="font-size: 10px;">/mth</sub>
						</span>
						<span class="timePeriod">You pay $299.94 for 6 months</span>
						<span class="join">Join</span>
					</div>
					<a href="<?=$signup_urls['biannual']?>" class="uiButtonNew">Start Free Trial.</a>
				</div>
			</li>
			<li class="clearfix">
				<div class="pull-left">
					<div class="title">
						<h3>ANNUAL</h3>
						<h6>Our best price.</h6>
					</div>
					<ul class="specification unstyled">
						<li class="techSupport">24/7 Tech Support</li>
						<li class="advOptions">Advanced Options</li>
						<li class="storage">100GB Storage</li>
						<li class="bandwidth">1GB Bandwidth</li>
					</ul>
				</div>
				<div class="pull-left right">
					<div class="priceContainer">
						<span class="price">
							<sup>&dollar;</sup><span class="dollars"><?=$prices_dollars['annual']?></span><sup class="cents"><?=$prices_cents['annual']?><sup>*</sup></sup><sub style="font-size: 10px;">/mth</sub>
						</span>
						<span class="timePeriod">You pay $479.88 for 12 months</span>
						<span class="join">Join</span>
					</div>
					<a href="<?=$signup_urls['annual']?>" class="uiButtonNew">Start Free Trial.</a>
				</div>
			</li>
		</ul>

		<div class="clearfix" style="margin-bottom: 15px; margin-top: 15px;">
			<div class="span6 aboutPlanNote">*All plans renew automatically. You may cancel any time.</div>
			
			<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
				<? if($this->member_type->code == 'AGENT') :?>
					<div class="span6 aboutPlanNote">Are you planning on using <a href="/member/broker"><?=COMPANY_NAME;?> for your entire Brokerage</a>?</div>
				<? else : ?>			
					<div class="span6 aboutPlanNote">Are you an <a href="/index/pricing">individual looking to use <?=COMPANY_NAME;?></a>?</div>
				<? endif; ?>
			<? endif; ?>
		</div>
		
		
		<? if(!stristr(APPLICATION_ENV, "myeventvoice")) : ?>
			<div class="quoteContainer clearfix">
				<div class="span6">
					<p>"I use <?=COMPANY_NAME;?> to help me market. Not only are they the heart of my online platform, but also help me to sell my clients’ listings quickly, and to secure the maximum price. 
						<?=COMPANY_NAME;?> is an incredible service for both beginners and top agents, and I highly recommend them."</p>
					<h5>- Sheree Cerqua / Ontario’s #1 Individual Agent</h5>
				</div>
				<div class="span6">
					<p>"My friends and previous colleagues noticed my Facebook updates right away. In only my first week using <?=COMPANY_NAME;?>, I received a call from a family friend who said she’d seen 
						my Facebook and wanted to list their home with me. The experts are awesome!"</p>
					<h5>- Leila Talibova / Top Rookie Agent </h5>
				</div>
			</div>		
							
			<div class="quoteContainer clearfix">
				<div class="span12">
					<?= $this->FacePile() ?>
				</div>
			</div>
		
		<? endif; ?>
		
		<div class="aboutPlans clearfix">
			<div class="span6 whoIsUsingCB">
				
				<? if(!stristr(APPLICATION_ENV, "myeventvoice")) : ?>
					<h3>Who's Using <?=COMPANY_NAME;?></h3>
					<div class="row-fluid">
						<a href="#" class="span4">
							<img src="/images/new-images/royal-lepage.png" />
						</a>
						<a href="#" class="span4">
							<img src="/images/new-images/lone-wolf.png" />
						</a>
						<a href="#" class="span4">
							<img src="/images/new-images/real-estate.png" />
						</a>
					</div>

					<div class="row-fluid">
						<a href="#" class="span4">
							<img src="/images/new-images/see-virtual.png" />
						</a>
						<a href="#" class="span4">
							<img src="/images/new-images/tech-tbes.png" />
						</a>
						<a href="#" class="span4">
							<img src="/images/new-images/real-estate-wealth.png" />
						</a>
					</div>
					<div class="row-fluid">
						<a href="#" class="span4">
							<img src="/images/new-images/the-globe-and-mail.png" />
						</a>
						<a href="#" class="span4">
							<img src="/images/new-images/retechalous.png" />
						</a>
					</div>

				<? endif; ?>
				

				<div class="securityNote">
					<img class="pull-right" src="/images/new-images/lock.png" />
					<h6>Total Security.</h6>
					<p><?=COMPANY_NAME;?> uses industry best practices, and partners with top firms (including Stripe), to ensure the security of sensitive data. Credit card information is encrypted 
						with SSL (secure socket layers), is not stored on <?=COMPANY_NAME;?> servers and is only used to verify billing info.</p>
					<p>Further, your personal information is not shared with 3rd parties without consent. Review the <a href="/index/privacy">Privacy Policy</a> for more information.</p>
					<h6>What methods of payment do you accept?</h6>
					<p>We currently accept payments online by Visa, Mastercard, American Express and Discover Card.</p>
				</div>
			</div>
			<div class="span6 faqs">
				<h3>Frequently Asked Questions</h3>
				
				<h5>How does the 14-day free trial work?</h5>
				<p>To explore how <?=COMPANY_NAME;?> can help you manage your social media accounts, simply sign-up and test at no cost for two weeks.  We'll quickly get our staff to 
					work posting expert real estate updates to your accounts.  If you like the service, after 14 days we'll begin whichever payment plan you've chosen.</p>
					
				<h5>Can I upgrade my plans at any time?</h5>
				<p>Yes!  You can upgrade your account at any time from your <?=COMPANY_NAME;?> dashboard or by contacting us.  Visit our pricing plans page for more information.</p>
				
				<h5>What subscription options do I have for <?=COMPANY_NAME;?>?</h5>
				<p><?=COMPANY_NAME;?> offers monthly, semi-annual and annual subscriptions. For monthly subscriptions, there are no term-length commitments. For more information, see the pricing plans page.</p>
				
				<h5>What support do you provide?</h5>
				<p>All <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> members can contact customer service at any time.  Our office hours are Mon-Fri, 9am-5pm EST, and real humans are on 
					standby to help at <b><?=TOLL_FREE;?></b> or via email at <a href="mailto:<?=HELP_EMAIL;?>"><?=HELP_EMAIL;?></a>!</p>
				
				<h5>Which platforms are supported?</h5>
				<p>We currently support: Internet Explorer 8+, Firefox 3+, Safari 4+, Chrome 4+</p>
				
				<h5>What methods of payment do you accept?</h5>
				<p>We currently accept payments online by Visa, Mastercard and American Express.</p>
			</div>
		</div>
	</div>
</div>
