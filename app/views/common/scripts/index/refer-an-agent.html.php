<?php
if ($this->member && $this->member->network_app_facebook) {
	$network_app_facebook = $this->member->network_app_facebook;
} else {
	$network_app_facebook = Zend_Registry::get('networkAppFacebook');
}
?>
<div class="contentBox">
	
	<div class="contentBoxTitle">
		<h1>Refer An Agent.</h1>			
		
	</div>

	<h4>Want to earn a FREE month of ClientFinder?</h4>
	<p style="padding-top: 8px;">Now that you&#39;ve seen the amazing power of <?=COMPANY_NAME;?> marketing, share it with other agents. With <?=COMPANY_NAME;?>, everyone benefits from more members; as more agents means a greater marketing platform for all!</p>
	<p>As an added bonus, for EVERY AGENT that signs up by your referral, you'll receive a <strong>FREE MONTH</strong> of Membership (<b>$49.99</b> value), credited directly to your account!  It's all automatic - just make sure you're logged in, and start sending!</p>
	
	<hr />
			
	<form action="/index/send-referral" method="POST" id="send_referral">
		<ul class="uiForm clearfix">
	
			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname cc"><span style="color: #990000;">*</span>Enter your friend&#39;s email</li>
					<li class="field cc">
						<input type="text" name="email_friend" value="" class="uiInput validateNotempty" />
					</li>
				</ul>
			</li>
			<li class="uiFormRow clearfix">
				<ul>
					<li class="buttons">
						<input type="submit" name="submitted" class="btn btn-primary btn-large pull-right" style="width: 195px;" value="Send Referral" id="submitBtn">
					</li>
				</ul>
			</li>
		</ul>
	</form>

</div>

<?php if (isset($this->popup) && $this->popup) : ?>

<script type="text/javascript">
	var USE_FB_JS_SDK	=   true;
	var fb_login_status	=	false;

	$(document).ready(function() {
        $("#send_referral").submit(function () {
            callPopup();
            return false;
        });
	});


	function callPopup() {

		$.colorbox({
			href: false,
			innerWidth:420,
			innerHeight:480,
			html:'	<div>'+
						'<div class="box clearfix">'+
							'<div class="box_section">'+
								'<h1 style="margin-bottom: 0px;">Welcome to <?=COMPANY_NAME;?>.</h1>'+
								'<h3 style="margin-bottom: 0px;">Please Login or Register.</h3>'+
							'</div>'+
							'<h3>Hello, before you can start, you\'ll need to login or register.</h3>'+
							'<p>If you\'re a returning member please <a href="#" class="login1">login</a> before Blasting. If you\'re new to the site please <a href="#" class="register1">register a new account</a> to get started.</p>'+
						'</div>'+
					'</div>'+
					'<div>'+
						'<div class="box clearfix">'+
							'<a href="#" class="uiButton login1" style="margin-left: 50px; width: 250px; margin-top: 20px;">Login Now</a>'+
							'<a href="#" class="uiButton register1 cancel" style="margin-left: 50px; width: 250px; margin-top: 20px;">Register Now</a>'+
						'</div>'+
					'</div>',

			onComplete: function(e) {

				$('.login1').bind('click', fb_login);
				$('.register1').bind('click', fb_login);

			},

			onClosed: function(e) {
				if (!fb_login_status) {

					$('.login1').unbind();
					$('.register1').unbind();
					callPopup();

				} else {

					$('.register1').unbind();
					$('.register1').bind('click', fb_register);

				}
			}

		});




		if(USE_FB_JS_SDK) {


			// avoid multiple initialization
			if (typeof is_facebook_init == 'undefined' || !is_facebook_init)
			{
				FB.init({
					appId  : '<?=$network_app_facebook->consumer_id;?>',
					status : true, // check login status
					cookie : true, // enable cookies to allow the server to access the session
					xfbml  : true, // parse XFBML
					oauth  : true, // enable OAuth 2.0
					channelUrl: 'http://<?php echo $_SERVER['HTTP_HOST'] ?>/channel.php' // channeling JS SDK
				});

				is_facebook_init	= true;
			}


		    function fb_login (event)
			{

			    event.preventDefault();
			    FB.login(function(response) {

				    if (response.authResponse)
				    {

					    $.ajax({

						    type: "POST",
						    url: "/member/fblogin",
						    data: "access_token="+response.authResponse.accessToken,
						    dataType: "json",

						    success: function(msg)
						    {
								fb_login_status	= true;
							   window.location =	msg.redirectBackTo;
						    }
					    });

				    }

			    }, {scope: '<?php echo FB_SCOPE;?>'});

		    }



		    function fb_register(event)
			{

				event.preventDefault();


				//alert('What is my City?'+$("#city").val());
				var city_id	= $("#city").val();
				if (city_id == null || city_id == '' || city_id == '*')
				{
					window.location	= "/member/blastcity";
					return false;
				}
				else
				{

					FB.login(function(response) {

						if (response.authResponse)
						{
							// console.log(response.authResponse);
							$.ajax({

								type: "POST",
								url: "/member/fbsignup",
								data: "access_token="+response.authResponse.accessToken+"&href="+$('#facebook-signup').attr('href'),
								dataType: "json",

								success: function(msg)
								{
									fb_login_status	= true;
									window.location =	msg.redirectBackTo;
								}
							});

						}

				    }, {scope: '<?php echo FB_SCOPE;?>'});
				}


		    }

		}



	}
</script>

<?php endif ?>

