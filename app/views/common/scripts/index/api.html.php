<div class="contentBox">

   	<div class="contentBoxTitle">        		
       	<h1>Let's Talk API's.</h1>
	</div>
	

	<? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>
		<p>Are you a service provider for the Events Industry? Then the <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> API might be right for you.</p>
	  
		<p>The <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> API let's you seamlessly integrate all of the great functions of <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> into your CMS, website, third-party web service, and more.</p>

	<? else : ?>
		<p>Are you a service provider for the Real Estate or Mortgage Industry? Then the <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> API might be right for you.</p>
	  
		<p>The <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> API let's you seamlessly integrate all of the great functions of <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> into your CMS, listing service, broker website, third-party web service, and more.</p>	
	
	<? endif; ?>
	

	
	<h4 style="margin-top: 20px;">What You Can Do with the API</h4>
	<p>The <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> API allows you to sync your database with <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> for easier or automated Listing, Blasting and Posting management.  
		Automatically create new listings, add a <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> feed to your website, access your Dashboard statistics, link campaign stats to your database, 
		remotely add new members or adjust your Experts' posting instructions, and more.</p>


	<p>Our API offers in-depth documentation and how-to-documents, and a great deal of the data and functionality within our web application is accessible, so the integration possibilities are limitless.</p>
    
	<h4 style="margin-top: 20px;">Need Help?</h4>
	<p>Not a programmer?  Not a problem.  We can help you develop custom integrations for your company or website.  Simply email us at <a href="mailto:<?=HELP_EMAIL;?>"><?=HELP_EMAIL;?></a> with your request, and 
		someone from our professional services team will contact you.</p>

	

	<p>To get started, you'll need an API key and an API secret something our <a href="mailto:<?=HELP_EMAIL;?>?subject=<?=urlencode('I need technical help with the API!');?>">professional services team</a> would be happy to help with!.</p>
	
	
		
	<a href="mailto:<?=HELP_EMAIL;?>?subject=<?=urlencode('Please send me more info about the API!');?>" class="btn btn-primary btn-large pull-right" style="width: 295px; margin-top: 20px;" id="submitBtn">Yes, I Want To Use The API</a>	
	
	<div class="clearfix"></div>
	
</div>