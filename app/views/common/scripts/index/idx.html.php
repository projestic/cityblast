<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>
<div class="contentBox">

   	<div class="contentBoxTitle">        		
       	<h1>Become a Franchise Partner.</h1>
	</div>
	

	<div class="strategyContainer">

		<p>Are you a franchise or large company with many listings?</p>
			
		<p><a href="/"><?=COMPANY_NAME;?></a> would like to form a partnership to <span style="background-color: #ffff00;">share your listings and company content with our massive Facebook, Twitter and LinkedIn audience;</span> 
			showcasing them to the real estate world's largest-reaching social media network, of over <a href="/index/our-story">3,500,000 potential buyers daily</a>.</p>
	
	
		<p>We offer you:
	
				<ol style="margin-bottom: 18px; margin-bottom: 1.5em; margin-left: 5.2em;">
					<li style="margin-bottom: 10px;list-style-type: decimal;">Incredible marketing exposure for listings.  <?=COMPANY_NAME;?>'s network reaches over 3.5M daily.</li>
					<li style="margin-bottom: 10px;list-style-type: decimal;">More and faster sales, and a demonstrable advantage over competitors.</li>
					<li style="margin-bottom: 10px;list-style-type: decimal;">A much higher propensity to 'double-end' real estate commissions by the brand's agents.</li>
					<li style="margin-bottom: 10px;list-style-type: decimal;">Massive brand exposure through social media with branded posting seen by millions.</li>
					<li style="margin-bottom: 10px;list-style-type: decimal;">Recognition of your constituent agents as social media leaders industry-wide.</li>
					
				</ol>
	
		</p>
	
		<p>Are you interested in discussing the opportunity to <u>publish your company's content and listings</u> to the real estate world's <i>largest social media audience</i>?</p>  
		
		<p>Let's chat!</p>
	
	</div>
	

	

	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 28px;">You're one click away.</h3>
			<h4>Become a Certified <?=COMPANY_NAME;?> Franchise Partner.</h4>
		</div>
		<div class="pull-right">
			<?/*<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>*/?>
			<a href="mailto:<?=HELP_EMAIL;?>?subject=<?=urlencode("I'd like to become a Franchise/IDX Partner!");?>" class="uiButtonNew tryDemo facebook-signup-affiliate" id="tryusnow_step1_btn">Talk To Us!</a>
		</div>
	</div>




	
		

	<div class="clearfix"></div>
	
</div>