<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>
<div class="contentBox">

   	<div class="contentBoxTitle">        		
       	<h1>Train Agents on Our System.</h1>
	</div>
	
	<div class="strategyContainer">
		
		<p>Are you a coach or trainer helping <? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>Wedding Professionals and Event Planners<? else : ?>Agents and Brokers<? endif;?> build profitable and sustainable businesses?</p>  
		<p>Do you love to help people reach their <u>personal goals</u>?</p>
		<p>Then we'd love to work with you.</p>
		
		
		<h3 style="margin-top: 20px;">Who we are.</h3>
		
		<p><a href="/"><?=COMPANY_NAME;?></a> is a team of dedicated and professional Social Media experts.</p>
		
		<p>We've published over 1,000,000 individual <a href="//www.facebook.com">Facebook</a>, <a href="//www.twitter.com">Twitter</a> and <a href="//www.linkedin.com">LinkedIn</a> updates for real 
			estate professionals in the United States and Canada.
		</p>
		
		<p>It's literally the <u><i>only</i></u> thing we do.</p>



		<h3 style="margin-top: 20px;">Why <?=COMPANY_NAME;?>?</h3>
		
		<p>You've been training your students to employ the power of  <span style="background-color: #ffff00;">inbound "value added" content</span>.</p> 
		
		<p>And even though they all <i>understand the value</i>, for some <? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>real estate<? endif;?> professionals it's <u>nearly impossible to dedicate several hours a day</u> to update their social media accounts.</p>
		
		
		<h3 style="margin-top: 20px;">That's where we can help.</h3> 
		
		
			
		<p>For <u>less than $2/day</u>, we help busy  <? if(stristr(APPLICATION_ENV, "cityblast")) : ?>real estate agents <? elseif(stristr(APPLICATION_ENV, "mortgagemingler")) : ?>mortgage brokers  <? else : ?>professionals <? endif;?>stay on top of their content marketing strategy.</p>
		
		<p>We bring your "client-first" value-added content teachings to life. <i>No "me-first" updates. No pushy sales content.</i> Just consistent, timely, quality content each and every day.</p>
		
		<p>Now tou finally have a opportunity to recommend a cost-effective social media solution that <u>you can feel good about</u></p>
		

		

		<? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>
	
			<h3 style="margin-top: 20px;">How it works.</h3>
		
			<p>Working with <a href="/"><?=COMPANY_NAME;?></a> is dead simple. We host a <i>FREE</i> joint training session for your coaching network and teach your friends or clients everything we know about social media marketing.</p>
			
			<p>All you have to do is <span style="background-color: #ffff00;">invite your clients </span> to the training session. Don't worry, we'll even supply you with the email copy for that too.<p>
				
			<p>It's that <u><i>easy</i></u>.</p>
				
	
			<h3 style="margin-top: 20px;">What's in it for me?</h3>
				
			<p>Just for reaching out to your agents and helping them solve a problem they <u><i>already</i></u> have, <a href="/"><?=COMPANY_NAME;?></a> would like to say 'thank-you' by offering a 
				 <span style="background-color: #ffff00;"><u>recurring</u> monthly commission for the <u>lifetime</u> of the membership!</p>
			
			<p>That's right, you can create a <u><i>recurring revenue</i></u> stream for yourself, just by helping your network of friends and clients solve a daily headache.<p>

		<? else : ?>

			<h3 style="margin-top: 20px;">How it works.</h3>
		
			<p>Working with <a href="/"><?=COMPANY_NAME;?></a> is dead simple. We host a <i>FREE</i> joint training session for your coaching network and teach your agents everything we know about social media marketing.</p>
			
			<p>All you have to do is <span style="background-color: #ffff00;">invite your agents</span> to the training session. Don't worry, we'll even supply you with the email copy for that too.<p>
				
			<p>It's that <u><i>easy</i></u>.</p>
				
	
			<h3 style="margin-top: 20px;">What's in it for me?</h3>
				
			<p>Just for reaching out to your agents and helping them solve a problem they <u><i>already</i></u> have, <a href="/"><?=COMPANY_NAME;?></a> would like to say 'thank-yo'u by offering a 
				 <span style="background-color: #ffff00;"><u>recurring</u> monthly commission for the <u>lifetime</u> of the membership!</p>
			
			<p>That's right, you can create a <u><i>recurring revenue</i></u> stream for yourself, just by helping your Agents solve a daily headache.<p>		
		
		<? endif; ?>
		
		<h3 style="margin-top: 20px;">Still not convinced?</h3>
		<p>We've generated thousands of dollars of recurring revenue for some of the biggest Training Professionals in the industry including: 
			<a href="http://retechulous.com/">Josh Schoenly</a>, <a href="http://www.listingboss.com/">Hoss Pratt</a>, <a href="http://jimtherealestatecoach.com/bio.asp">Jim Ouellette</a>, <a href="http://www.realbies.com/>Shawn Mahdavi</a>, 
			<a href="http://www.leadersedgetraining.com/bio_chris.php">Chris Leader</a>, <a href="http://www.performanceproperty.com/">Gerald Lucas</a>, 
			<a href="http://agentsboost.com/real-estate-coaching/">Wade Webb</a>, <a href="http://agentpartner.com/">David Broudie</a> and many, many more.</p>
		
		<p><br/>Today it's your turn.</p>  
	
	</div>	
		



	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 28px;">You're one click away.</h3>
			<h4>Schedule your <i><?=COMPANY_NAME;?></i> training seminar today.</h4>
		</div>
		<div class="pull-right">
			<?/*<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>*/?>
			<a href="mailto:<?=HELP_EMAIL;?>?subject=<?=urlencode("I'd like to become a Training Partner!");?>" class="uiButtonNew tryDemo facebook-signup-affiliate" id="tryusnow_step1_btn">Start Today!</a>
		</div>
	</div>




	
</div>