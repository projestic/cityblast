<style type="text/css">
	ol{
		margin-left: 1.3em;
	}
</style>

<div class="contentBox questionContainer">
	<div class="contentBoxTitle">
		<h1>Frequently Asked Questions.</h1>
	</div>
			
	<?/*********
	<h5>What are the rules for the May 2013 Referral Contest?</h5>
	
	<ol style="margin-left: 1.3em;">
		<li>The contest will run from May 13, 2013 to midnight EST May 27, 2013.</li>
		<li>There is no limit to the number of referral emails you can send to your friends and colleagues.</li>
		<li>For each of your friends that signs up for a trial membership during the contest period, you will receive a single ballot.</li>
		<li>At the end of the contest period, all ballots will be entered into a draw for the Grand Prize.</li>
		<li>The contest winner will be selected, announced and contacted by May 29, 2013.</li>
		<li>You can keep track of your emails, referrals and ballots on the contest leaderboard at <a href="/">CityBlast.com</a></li>
	</ol>
	
	<h5>GRAND PRIZE</h5>
	
	<p>A trip for two to Las Vegas, including airfare and 4 nights accommodation from June 20-24th, 2013 (Thursday to Monday). Flights may depart from anywhere in Canada and USA. Accommodation provided by the Wynn Las Vegas <a href="http://www.wynnlasvegas.com" target="_blank">www.wynnlasvegas.com</a></p>
	
	<hr />
	
	****************/?>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>Will my Social Expert publish content on my friends' walls/accounts?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">No.  Your Expert only posts to your account. Your friends can see the updates on your timeline, just as if you've written them yourself.</p>
	</div>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>Can my Social Expert publish to my Facebook Business Page?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Yes. Your Expert will be happy to publish to your Business Page. Simply register as normal, and activate any Page you administer in your <a href="/member/settings">personal settings</a>.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I've heard you shouldn't market or 'sell' via Facebook, Twitter and LinkedIn.</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		
		<? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>
			<p class="answer">Facebook, Twitter and LinkedIn represent today's greatest opportunity to reach your network with reminders that you are an events professional and your doors are open. By using added-value content, 
				focused on giving your friends fun and interesting events industry information, you are passively and thoughtfully keeping your network focused on you, rather than 'selling' to them. When they need your 
				services, they will turn to you first.</p>		
		<? else: ?>
			<p class="answer">Facebook, Twitter and LinkedIn represent today's greatest opportunity to reach your network with reminders that you are an 
			agent and your doors are open. By using added-value content, focused on giving your friends valuable and interesting real estate information, you are 
			passively and thoughtfully keeping your network focused on you, rather than 'selling' to them. When they are interested in buying, they will turn to you first.</p>
		<? endif; ?>	
		
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How often does my Expert update my account(s)?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">You choose!  Your Social Expert can be instructed to post from 1 to 7 times per week in your <a href="/member/settings">Settings</a>.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>Can I publish to Facebook, but not Twitter and LinkedIn, or vice versa?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Yes.  Your Expert will be happy to publish to any combination of your accounts that you choose.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>Can I publish to my website?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Unfortunately at this time our Social Experts do not support updates to your personal website.</p>
	</div>
	
	<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
		
		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>How much does it cost to Blast of one of my listings?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">Blasting and all other features of <?=COMPANY_NAME;?> are included with your Membership at no extra cost.  Enjoy!</p>
		</div>
		
		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>Are the other agents' listings featured on my accounts permitted to be advertised?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">Yes. When entering a listing Blast, <?=COMPANY_NAME;?> members agree that they are granting permission to advertise the listing and that they are working on behalf of the seller.</p>
		</div>
		
		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>How many times does my property Blast go out?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">Individual property Blasts are published for up to 2 weeks before being retired.</p>
		</div>
		
		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>What do I do once I have an incoming lead from a listing I have published?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">Simply look up the property information on <?=COMPANY_WEBSITE;?>, or your local board's listing service, and follow up with information or to book showings with your prospect.</p>
		</div>

		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>Can I Blast an exclusive listing, that is not on my board's listing service?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">Yes.  Any property offering a standard cooperating commission may be featured on <?=COMPANY_NAME;?>.</p>
		</div>
		
		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>Can I feature a link to a virtual tour or my website in my Blast?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">No.  No outside links or contact information are permitted within the content of property Blasts.</p>
		</div>

		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>Can I get a Social Expert working for me, but not use the Blasting feature?  Vice versa?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">Absolutely!  Use whatever tools you like, and however you choose.  It's all included for your benefit.</p>
		</div>

		<? endif; ?>





	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How does the 14-day free trial work?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">To explore how <?=COMPANY_NAME;?> can help you manage your social media accounts, simply sign-up and test at no cost for two weeks.  We'll quickly get our staff to work posting expert real estate updates to your accounts.  If you like the service, after 14 days we'll begin whichever payment plan you've chosen.</p>
	</div>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How much does membership cost?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer"><?=COMPANY_NAME;?> offers various pricing plans. <a href="/index/pricing">Please see them here</a>.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>Can I upgrade my plans at any time?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Yes!  You can upgrade your account at any time from your <?=COMPANY_NAME;?> dashboard or by contacting us.  Visit our pricing plans page for more information.</p>
	</div>
			
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>What subscription options do I have for <?=COMPANY_NAME;?>?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer"><?=COMPANY_NAME;?> offers monthly, semi-annual and annual subscriptions. For monthly subscriptions, there are no term-length commitments. For more information, see the pricing plans page.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>What support do you provide?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">All <?=COMPANY_NAME;?> members can contact customer service at any time.  Our office hours are Mon-Fri, 9am-5pm EST, and real humans are on 
		standby to help at <b><?=TOLL_FREE;?></b> or via email at <a href="mailto:HELP_EMAIL"><?=HELP_EMAIL;?></a>!</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>Which platforms are supported?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">We currently support: Internet Explorer 8+, Firefox 3+, Safari 4+, Chrome 4+</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>What methods of payment do you accept?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">We currently accept payments online by Visa, Mastercard and American Express.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>What type of posts does my Expert publish to my account(s)?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Whatever kind you want!  You can choose from over 15 different categories of content, and tailor your posts to suit your own personal style.  Your Expert does the rest, by finding and posting only the types of content you've selected.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>What if I don't see my posts?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Members should ensure that they are checking for posts on their own timeline, and not their News Feed. To access your timeline, simply 
		log in to Facebook, and click on your own name in the top right section of the site header.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>Can I stop and resume posts at any time?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Yes. You can request that your Expert pause or stop your postings at any time by simply accessing your <a href="/member/settings">Settings</a>.</p>
	</div>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>Does <?=COMPANY_NAME;?> or my Social Expert have access to my Facebook, Twitter or LinkedIn passwords?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">No. <?=COMPANY_NAME;?> only has the ability to post to your account. Your Expert and <?=COMPANY_NAME;?>.com do not have access to your login information or passwords, and can not see your private account info.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>Will my friends feel like I am publishing content too often?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">You have control over how often you publish. You should publish your own personal content as you normally would, as well as professional 
		added-value content via <?=COMPANY_NAME;?> to your social media accounts.  Tailor the types of content and posting frequency in your <a href="/member/settings">Settings</a> panel.</p>
	</div>

	<? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>
		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>Will I publish the same thing as other people, or at the same time?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">No.  Your Expert posts your own custom content to your account(s).  Your content will never be the same as anyone else's.</p>
		</div>
	<? else: ?>
		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>Will I publish the same thing as other agents, or at the same time?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">No.  Your Expert posts your own custom content to your account(s).  Your content will never be the same as anyone else's.</p>
		</div>	
	<? endif; ?>
	
	
	<? /*
	<h5>Is there a limit to how many agents may sign up in my City?</h5>
	<p>Yes.  <?=COMPANY_NAME;?> limits the number of members allowed in each City using a careful algorithm related to Facebook publishing and agent overlap.  <?=COMPANY_NAME;?> uses this algorithm and limit to keep the 
		service exclusive and maximize return for our members.  When 
		there are no more spaces available in your City, you may request to join the waiting list.</p>
	
	<hr/>
	*/ ?>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How locally-focused is <?=COMPANY_NAME;?>?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		
		<? if(stristr(APPLICATION_ENV, "myeventvoice")) : ?>
			<p class="answer">It depends on your market.  If you are in a major market, <?=COMPANY_NAME;?> your content will be somewhat more locally focused. As a simple rule: where the number of news sources in the market is higher, <?=COMPANY_NAME;?> will always be more geographically defined.</p>			
		<? else: ?>
			<p class="answer">It depends on your market.  If you are in a major market, <?=COMPANY_NAME;?> content will be more geographically focused on a smaller area.  As a simple rule: where the number of agents on the service is higher, <?=COMPANY_NAME;?> will always be more geographically defined.</p>					
		<? endif;?>
	</div>
	
	<? if(!stristr(APPLICATION_ENV, "myeventvoice")) : ?>
		<div class="questionWrapper">
			<a class="question" href="javascript:void(0);">
				<h5>What if I get a lead that I prefer not to deal with personally?</h5>
				<img src="/images/new-images/transparent.png"/>
			</a>
			<p class="answer">If you get a lead that you'd rather not work personally, refer them to someone in your office in exchange for a referral fee.  It won't cost you any time, and you'll still gain from having 
			created the business.</p>
		</div>
	<? endif; ?>
		
</div>



	<div class="contentBox">
		<div class="clearfix">
			<div class="pull-left tryDemoTxt">
				<h3 style="font-size: 26px;">You're now ready to conquer social media.</h3>
				<h4>Get started today with your Free 14-Day Trial of <?=COMPANY_NAME;?>.</h4>
			</div>
			<div class="pull-right">
				<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>
			</div>	
		</div>		
	</div>

<div class="contentBox">
	<div class="clearfix">
		<h3 style="margin-bottom:20px">Members helping members</h3>
		<fb:comments xid="member_join" href="<? echo APP_URL;?>/index/faq"></fb:comments>
	</div>		
</div>

<script type="text/javascript">
	$(document).ready(function() {
		// Resize FB comments block to width of the borwser window
		setTimeout(function() {
		  resizeFacebookComments();
		}, 1000);
		$(window).on('resize', function() {
		  resizeFacebookComments();
		});
		function resizeFacebookComments() {
		  var src = $('.fb_iframe_widget iframe').attr('src').split('width='),
		      width = $('.fb_iframe_widget').width() - 10;
		  $('.fb_iframe_widget iframe').attr('src', src[0] + 'width=' + width);
		}

		// Adding event listening on comment
		FB.Event.subscribe('comment.create', function(response)
		{
		    var commentQuery = FB.Data.query("SELECT object_id,id,text, fromid FROM comment WHERE post_fbid='"+response.commentID+"' AND object_id IN (SELECT comments_fbid FROM link_stat WHERE url='"+response.href+"')");
		    var userQuery = FB.Data.query("SELECT name FROM user WHERE uid in (select fromid from {0})", commentQuery);
		
		    FB.Data.waitOn([commentQuery, userQuery], function()
		    {
				var commentRow = commentQuery.value[0];
				var userRow = userQuery.value[0];

				var commentId = commentRow.id;
				var name = userRow.name;
				var fuid = commentRow.fromid;
				var text = commentRow.text;
				var mid	= <?php echo (empty($_GET['mid'])) ? 0 : intval($_GET['mid']);?>;
				var url = '/comments/broadcast';
				var data = 'name='+name+'&fuid='+fuid+'&text='+text+'&mid='+mid+'&referring_url=<?=$this->current_url?>';
		
				$.ajax({
					type: 'POST',
					url: url,
					data: data,
					success: function(response){
						// do nothing
					},
					dataType: 'json'
				});		
		    });
		});
	});
</script>