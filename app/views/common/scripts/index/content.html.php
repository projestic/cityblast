<style>

body, html, p, div
{
	font-size: 16px;
	line-height: 1.6em;	
}
	
</style>
<div class="contentBox">

   	<div class="contentBoxTitle">        		
       	<h1>Become a Content Partner.</h1>
	</div>
	



	<div class="strategyContainer">

		<p>Are you a publisher providing information for the Real Estate or Mortgage Industry?  Apply now to become a <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> Preferred publishing partner, and have your articles and content featured to our readers.</p>
		
		<h3 style="margin-top: 20px; margin-bottom: 10px;">Why become a Content Partner?</h3>
		
		<p>Once you're approved, your content will be sourced and shared on the social media accounts of thousands of professionals across North America - generating a massive increase in your organic click traffic.</p>
	
	
		<h3 style="margin-top: 20px; margin-bottom: 10px;">100% Free.</h3>
		<p>Best of all? It's 100% free. No tricks, no gimmicks, no strings attached!</p>
		
	
	
	
		<h3 style="margin-top: 20px; margin-bottom: 10px;">What about local publications?</h3>
		<p>Not national?  Not a problem.</p>
	
		<p>We are actively seeking local market Content Partners as well.  Please apply now if you fit the bill!</p>
		
			
		<h3 style="margin-top: 20px; margin-bottom: 10px;">We do all the work.</h3>
	
		<p>Once you're approved <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> does all the work. Simply point us to the content you want highlighted, we do the rest. Driving thousands of eyeballs to your site.</p>
	

	
	
		<p>Interested in discussing the opportunity to publish your content through out massive network?  So are we.  Click below to get started!</p>
	
	</div>
	


	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 28px;">You're one click away.</h3>
			<h4>Become a Certified <?=COMPANY_NAME;?> Content Partner.</h4>
		</div>
		<div class="pull-right">
			<?/*<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>*/?>
			<a href="mailto:<?=HELP_EMAIL;?>?subject=<?=urlencode("I'd like to become a Content Partner!");?>" class="uiButtonNew tryDemo facebook-signup-affiliate" id="tryusnow_step1_btn">Start Now!</a>
		</div>
	</div>




	
		

	<div class="clearfix"></div>
	
</div>