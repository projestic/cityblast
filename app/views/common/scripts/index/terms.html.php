<div class="contentBox">
		

	<div class="contentBoxTitle">
		<h1>Terms of Service.</h1>
	</div>
	
	<p>These are the <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> terms of service. The rules of the site are designed for the benefit of all members. In case of uncertainty, all members are provided these simple guidelines to help with successful and profitable operation of <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> for all. 
		
		<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
			Any published Blast that is in violation of the Terms of Service may be adjusted by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> editors without notice, in an effort to adhere to the spirit of the Terms of Service. For member convenience, resultant conforming Blasts will be scheduled for publication as intended, without further notice.
		<? endif; ?>
		
	</p>
	
	<p>By using the <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> website ("Service"), a service of <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> Media, Inc. ("<a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>"), you are agreeing to be bound by the following terms and conditions ("Terms of Service").</p>
	
	<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reserves the right to update and change the Terms of Service from time to time without notice. Any new features that augment or enhance the current Service, including the release of new tools and resources, shall be subject to the Terms of Service. Continued use of the Service after any such changes shall constitute your consent to such changes. You can review the most current version of the Terms of Service at any time here.</p>
	
	<p>Violation of any of the terms below may result in the termination of your Account without refund. While <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> prohibits such conduct and Content on the Service, you understand and agree that <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> cannot be responsible for the Content posted on the Service and you nonetheless may be exposed to such materials. You agree to use the Service at your own risk.</p>
	
	
	
	<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
	
		<br/>
		<p><strong>Please observe the following rules when Blasting:</strong></p>
		
		
		<p><strong>(1)</strong> Any licensed agent publishing a Blast with <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> Media, Inc acknowledges that they have read and understood the Terms of Service, and that they agree to them.</p>
		
		<hr>
		
		<p><strong>(2)</strong> Any licensed agent publishing a Blast warrants that the Blast will conform to their territory's Code of Ethics, Rules of Advertising, local advertising laws or similar, and that responsibility to conform to the Rules and Obligations of their licensing bodies and local government shall be theirs, and not that of <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> Media, Inc. By publishing, all licensed agents confirm that they are authorized to advertise the subject listing. Further, any person publishing with <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> Media, Inc and <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> indemnifies and holds harmless <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> Media, Inc against any and all claims arising from the use or misuse of the publishing service.</p>
		
		<hr>
		
		<p><strong>(3)</strong> All Blasts must represent currently available real estate listings for sale within the trade area of the selected <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> market, and the agent publishing the Blast must have the written consent of the property owner to market the property for sale. Most often, this will be in the form of a Listing Agreement. By Blasting, the agent warrants that they have obtained permission to market the property. Properties available for lease that are not also available for sale may not be published via the <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> service.</p>
		
		<hr>
		
		<p><strong>(4)</strong> A Blast <strong>can not</strong> contain any of the following:</p>
		
		<ul>
			<li style="list-style: circle outside; margin-left: 50px;">profanity or abusive language</li>
			<li style="list-style: circle outside; margin-left: 50px;">inaccurate or false information</li>
			<li style="list-style: circle outside; margin-left: 50px;">spelling errors</li>
			<li style="list-style: circle outside; margin-left: 50px;">information soliciting an agency agreement with the Blasting agent directly, including all forms of contact information</li>
			<li style="list-style: circle outside; margin-left: 50px;">links to any website</li>
	
		</ul>
		
		<hr>
		
		<p><strong>(5)</strong> All Blasts <strong>must</strong> contain:</p>
		
		<ul>
			<li style="list-style: circle outside; margin-left: 50px;">an address and MLS number for the property</li>
			<li style="list-style: circle outside; margin-left: 50px;">the name and address of the listing brokerage (to meet compliance)</li>
		</ul>
		
		<hr>
		
		<p><strong>(6)</strong> By publishing a Blast through <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a>, the person composing and publishing the Blast warrants that a cooperating commission is being offered to the Cooperating Brokerage, and that the minimum commission being offered is 2% of the purchase price of the subject property.</p>
		
		<hr>
		
		<p><strong>(7)</strong> Any person Blasting on <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> warrants that they are of the legal age to form a binding agreement in their jurisdiction, and that they have the legal capacity to enter into an agreement.</p>
		
		<hr>
		
		<p><strong>(8)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> does not control the content of your Blast, except for in reference to any edits based on the above content requirements, in which case <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> endeavours only to minimally edit violating content so as to create a conforming Blast. Any person Blasting on <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> takes responsibility to meet these requirements, and any and all content published by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> Media, Inc is deemed to be the original content of the Blasting individual and not that of <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> Media, Inc.</p>
		
		<hr>
			
		<p><strong>(9)</strong> Members may only Blast their own listings. No member may Blast the listing of another agent under any circumstances. If any member is found Blasting listings that are not theirs, service may be suspended or discontinued.</p>
		

	<? endif; ?>
	
	<br/>
	<p><strong>Account Terms</strong></p>
	
	<p><strong>(1)</strong> You must be 13 years or older to use this Service.</p>
	
	<hr>
	
	<p><strong>(2)</strong> You must be a human. Accounts registered by "bots" or other automated methods are not permitted.</p>
	
	<hr>
	
	<p><strong>(3)</strong> You must have a valid Facebook login and email registered with the service at all times.</p>
	
	<hr>
	
	<p><strong>(4)</strong> Your login may only be used by one person - a single login shared by multiple people is not permitted. You may create separate logins for as many people as you'd like.</p>
	
	<hr>
	
	<p><strong>(5)</strong> You are responsible for maintaining the security of your account and password. <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> cannot and will not be liable for any loss or damage from your failure to comply with this security obligation.</p>
	
	<hr>
	
	<p><strong>(6)</strong> You are responsible for all Content posted and activity that occurs under your account (even when Content is posted by others who have access to your account).</p>
	
	<hr>
	
	<p><strong>(7)</strong> You may not use the Service for any illegal or unauthorized purpose. You must not, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).</p>
	
	<br/>
	
	
	<p><strong>Payment, Refunds, Upgrading and Downgrading Terms</strong></p>
	
	<p><strong>(1)</strong> A valid credit card is required for your account, and will be logged in advance of your 14-day Free Trial.</p>
	
	<hr>
	
	<p><strong>(2)</strong> If you initially sign up for an account, and you don't cancel that account within 14 days, you will be billed monthly starting on the 14th day after your account was initially created. If you cancel prior to the processing 
		of your first invoice on the 14th day, you will not be charged.</p>
	
	<hr>
	
	<p><strong>(3)</strong> The Service is billed in advance on a monthly basis and is non-refundable. There will be no refunds or credits for partial months of service, upgrade/downgrade refunds, or refunds for months unused with an open account. In order to treat everyone equally, no exceptions will be made.</p>
	
	<hr>
	
	<p><strong>(4)</strong> All fees are exclusive of all taxes, levies, or duties imposed by taxing authorities, and you shall be responsible for payment of all such taxes, levies, or duties.</p>
	
	<br/>
	
	
	<p><strong>Cancellation and Termination</strong></p>
	
	<p><strong>(1)</strong> You are solely responsible for properly cancelling your account. An email or phone request to cancel your account is not considered cancellation. You can cancel your account at any time by clicking on the link available in your <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> Settings.</p>
	
	<hr>
	
	<p><strong>(2)</strong> All of your client information, including but not limited to statistics, name, contact information, image and more, may be deleted from the servers maintained by the Service upon your cancellation. This information may not be recoverable once your account is cancelled. All posts created by the Service on your social media channels will remain intact, pursuant however to your ongoing relationship with such third party providers.</p>
	
	<hr>
	
	<p><strong>(3)</strong> If you cancel the Service before the end of your current paid up month, your cancellation will take effect immediately and you will not be charged again.</p>
	
	<hr>
	
	<p><strong>(4)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>, in its sole discretion, has the right to suspend or terminate your account and refuse any and all current or future use of the Service, or any other <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> service, for any reason at any time. Such termination of the Service will result in the deactivation or deletion of your Account or your access to your Account, and the forfeiture and relinquishment of all Content in your Account. <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reserves the right to refuse service to anyone for any reason at any time.</p>
	
	<hr>
	
	<p><strong>(5)</strong> If you cancel your account and then choose to re-register, you will not be given preferential access to <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>.com in your territory, and may have to join a waiting list if your territory is at its maximum limit of available memberships.</p>
	
	<br/>
	
	
	<p><strong>Modifications to the Service and Prices</strong></p>
	
	<p><strong>(1)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Service (or any part thereof) with or without notice.</p>
	
	<hr>
	
	<p><strong>(2)</strong> Prices of all Services, including but not limited to monthly subscription plan fees to the Service, are subject to change upon 30 days notice from us. Such notice may be provided at any time by posting the changes to the <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> website or the Service itself.</p>
	
	<hr>
	
	<p><strong>(3)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> shall not be liable to you or to any third party for any modification, price change, suspension or discontinuance of the Service.</p>
	
	<br/>
	
	
	<p><strong>Copyright and Content Ownership</strong></p>
	
	<p><strong>(1)</strong> All content posted on the Service is must comply with U.S. Or Canadian copyright law, depending on jurisdiction.</p>
	
	<hr>
	
	<p><strong>(2)</strong> We claim no intellectual property rights over the material you provide to the Service. Your profile and materials uploaded remain yours.</p>
	
	<hr>
	
	<p><strong>(3)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> does not pre-screen Content, but <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> and its designee have the right (but not the obligation) in their sole discretion to refuse or remove any Content that is available via the Service.</p>
	
	<hr>
	
	<p><strong>(4)</strong> The look and feel of the Service is copyright &copy;2011 <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> Media, Inc. All rights reserved. You may not duplicate, copy, or reuse any portion of the HTML/CSS or visual design elements without express written permission from <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>.</p>
	
	<br/>
	
	
	<p><strong>General Conditions</strong></p>
	
	<p><strong>(1)</strong> Your use of the Service is at your sole risk. The service is provided on an "as is" and "as available" basis.</p>
	
	<hr>
	
	<p><strong>(2)</strong> Technical support is only provided to paying account holders and is only available via email.</p>
	
	<hr>
	
	<p><strong>(3)</strong> You understand that <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> uses third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run the Service.</p>
	
	<hr>
	
	<p><strong>(4)</strong> You must not modify, adapt or hack the Service or modify another website so as to falsely imply that it is associated with the Service, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>, or any other <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> service.</p>
	
	<hr>
	
	<p><strong>(5)</strong> You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service without the express written permission by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>.</p>
	
	<hr>
	
	<p><strong>(6)</strong> We may, but have no obligation to, remove Content and Accounts containing Content that we determine in our sole discretion are unlawful, offensive, threatening, libellous, defamatory, pornographic, obscene or otherwise objectionable or violates any party's intellectual property or these Terms of Service.</p>
	
	<hr>
	
	<p><strong>(7)</strong> Verbal, physical, written or other abuse (including threats of abuse or retribution) of any <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> customer, employee, member, or officer will result in immediate account termination.</p>
	
	<hr>
	
	<p><strong>(8)</strong> You understand that the technical processing and transmission of the Service, including your Content, may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices.</p>
	
	<hr>
	
	<p><strong>(9)</strong> You must not upload, post, host, or transmit unsolicited email, SMSs, or "spam" messages.</p>
	
	<hr>
	
	<p><strong>(10)</strong> You must not transmit any worms or viruses or any code of a destructive nature.</p>
	
	<hr>
	
	<p><strong>(11)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> does not warrant that (i) the service will meet your specific requirements, (ii) the service will be uninterrupted, timely, secure, or error-free, (iii) the results that may be obtained from the use of the service will be accurate or reliable, (iv) the quality of any products, services, information, or other material purchased or obtained by you through the service will meet your expectations, and (v) any errors in the Service will be corrected.</p>
	
	<hr>
	
	<p><strong>(12)</strong> You expressly understand and agree that <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> has been advised of the possibility of such damages), resulting from: (i) the use or the inability to use the service; (ii) the cost of procurement of substitute goods and services resulting from any goods, data, information or services purchased or obtained or messages received or transactions entered into through or from the service; (iii) unauthorized access to or alteration of your transmissions or data; (iv) statements or conduct of any third party on the service; (v) or any other matter relating to the service.</p>
	
	<hr>
	
	<p><strong>(13)</strong> The failure of <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> to exercise or enforce any right or provision of the Terms of Service shall not constitute a waiver of such right or provision. The Terms of Service constitutes the entire agreement between you and <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> and govern your use of the Service, superceding any prior agreements between you and <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> (including, but not limited to, any prior versions of the Terms of Service).</p>
	<hr>
	
	<p><strong>(14)</strong> Questions about the Terms of Service should be sent to our <a href="mailto: <?=HELP_EMAIL;?>">technical support staff</a>.</p>
	
	<br/>
	
	<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
		<blockquote>
				<p style="font-size: 14px;"><strong>Note: </strong>All Terms of Service are subject to change without advanced notice, and any changes will be posted here.  Please check back before Blasting if you're concerned that your Blast may not conform to the Terms of Service.</p>
		</blockquote>
	<? endif; ?>
	
	<br/>
</div>


<? /*
<div class="contentBox">	

	<div class="contentBoxTitle">
		<h1>Terms and Conditions of Use for Beta Version of <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a></h1>
	</div>

	<p><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> for online beta version (the "Beta Verson") is a service to provide functions of Blst.it ("Software") on a cloud server environment. <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is currently in its BETA version and undergoing 
		BETA testing. You understand and agree that the Beta Verson may still contain software bugs, suffer disruptions and not operate as intended or designated. Your use of the Beta Version at this stage 
		signifies your understanding of and agreement to participate in the Version's BETA testing.  By showing your consent to this terms and conditions on a  registration page for the Beta Version, you agreed 
	to be bound by this Agreement.</p>
	
	<p><strong>Definitions</strong></p>
	
	<p><strong>(1)</strong> "User" - For the purposes of this Agreement, "User" means an individual who has a unique login name generated or recognized by an instance of the Software that possesses the permission to do anything more than commenting on one or more listings. Notwithstanding the foregoing, an 
		individual whose relationship with <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> has been terminated and who is no longer authorized by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> to make use of the Software no longer counts nor be deemed as a User 
		effective upon the date of his/her termination. Use of a login name by more than one person is prohibited.</p>
	
	<p><strong>(2)</strong> You may use the Software only on the <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> servers and you may not make copy and other use of the Software on any other environment.</p>



	<p><strong>Effective Date</strong></p>
	<p><strong>(1)</strong> "Effective Date" - This Agreement is effective as soon as the User completes the registration process.</p>

	<p><strong>(2)</strong> During the Effective Term, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> will provide you functions of the Software and environment for use of the Software, by installing the Software on the Server prepared by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>. The 
	installation of the Software on the Server shall be made by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>.</p>



	<p><strong>Termination of the Beta </strong></p>
	<p><strong>(1)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reserves the right to time to discontinue the Beta Version without notice at any time for any reason including, but not limited to launching of or 
	discontinuing a development of a commercial version of the Software. You agree not to claim to <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> with respect to discontinuance of supplying the Beta Version.</p>
	
	<p><strong>(2)</strong> The User agrees that they do not have any right to a commercial version of the Software, if a commercial version of the Software is launched.</p>
	
	<p><strong>(3)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> may terminate this Agreement without a notice, if you violate any of terms and conditions of this Agreement.</p>
	
	<p><strong>(4)</strong> If you cancel this Agreement, this Agreement will be terminated as of the day when you cancel your User account (as described in our <a href="/faq">faq document</a>.</p>

		<p><strong>(5)</strong> Provisions regarding limitation of liability, Indemnification, Extent of Warranty and General in this Agreement survive termination of this Agreement.</a>

	
	<p><strong>Providing of Beta Version and Use of the Software</strong></p>	

	<p><strong>(1)</strong> Purposes of The Beta Version are that you may use the Software as installed and that <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> provides service of management and monitoring of the Server, on which the 
	Software is installed, on behalf of You. <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> will provide services of installation of the Software and its upgrades or updates on the Server environment, providing server functions, and 
	management and monitoring of the Server. You agree that you will be responsible for any other thing than the above, including not limited to designing screens of and configuration of the 
	Software after installation, at your own cost.</p>	
	
	<p><strong>(2)</strong> You may use the Software only on the Server prepared by  <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> and you may not make copy and other use of the Software on any other environment.</p>
	
		
	<p><strong>(3)</strong> You may use the Beta Version and the Software for its own use (if you are a corporation or organization, the use of them is limited for and within such corporation or 
	organization by persons belonging to it) and for a community's use by participants of such community, if you operate the community. Your rights are non-exclusive and non-transferable.</p>
		
	<p><strong>(4)</strong> You may not make available functions of all or part of the Beta Version and the Software, unless otherwise is expressly stated in this Agreement. 
	<a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reserves the right to determine, in its sole discretion, whether the your use of the Software complies with the authorized user under this Agreement. <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> owns all right, 
	title and interest in and to the Software. Also, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> reserves all rights to the Software that are not expressly granted in this Agreement.</p>

	<p><strong>(5)</strong> You understand and agree that the Beta Version is a beta version and your use of the Beta Version and the Software signifies your understanding of and agreement to participate 
	in the BETA testing. You agree and to provide information about bugs and errors of the Beta Version to <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> at no charge and not to claim any right, including, but not limited to, 
	any right under patent law and copyright law, with respect to correction or modification of the Beta Version or Software which <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> may make using information provided from you.</p>


	<p><strong>Technical Support</strong></p>
	<p><strong>(1)</strong> You agree that <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> will not provide any technical support service with respect to the Beta Service and the Software.</p>
	


	<p><strong>Compensation</strong></p>		
	<p><strong>(1)</strong> Your use of the Beta Version is not required to pay compensation.</p>
		
		<p><strong>(2)</strong> In consideration of the above, you agree that your will not make any claim against <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> about damage which may be raised from use or non-use of the Beta Version and from this Agreement.</p>




	<p><strong>Handling of Information About User</strong></p>

	<p><strong>(1)</strong> You agree that <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> has a right to use data about you for making aggregation or statistics (including providing and distributing it).However, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> shall not use data about you for 
	purpose or procedure which is not adapted with laws and regulations.</p>

<p><strong>(2)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> may provide or disclose information about you if <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is required to do so by a judgment or order issued by a court or other 
government official which is enforceable.</p>

<p><strong>(1)</strong> You agree that Personnel Information about you will be handled subject to a privacy policy stated by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>.</p>

	<p><strong>Compliance</strong></p>

<p><strong>(1)</strong> You agree to use the Beta Version in a way to comply with any law in a jurisdiction which you use it. Also, you shall use the Beta Version subject to 
regulations regarding protection of privacy and intellectual property. You are responsible for making your users to comply with terms and conditions of this Agreement, and you 
agreed that any breach by your users would be deemed as your breach.

	<p><strong>Restrictions</strong></p>

<p><strong>(1)</strong> You may not under any circumstances:
	<ul><li>Create and/or distribute derivative works based on the Software (however, the creation and/or distribution of plugins and other extensions to the Software making use of application
programming interfaces published by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is permitted);</li>

	<li>Reproduce the Software except as described in this Agreement;</li>
	
	<li>Reverse engineer, decompile, disassemble or otherwise attempt to reconstruct or discover any source code or algorithms of the Software;</li>

	
	
	
	
   <li> Sell, lease, assign, license, sublicense, disclose, distribute or otherwise transfer or make available the Software, in whole or in part or any copies thereof, in any form to any third parties;</li>
	
	<li>Host the Software for third party use or otherwise make the Software's functionality available to third parties as an application service provider or service bureau;</li>
	
   <li> Remove or alter any proprietary notices on the Software;</li>
	
	<li>Allow use of the Software by third parties not included in the definition or count of Users or Commentator;</li>
	
   <li> Conduct any illegal act or violate public order and moral, or make third party to do those acts, using the Beta Version;</li>
	
   <li> Use the Beta Version for any offers, displays or introduction of any obscene matter or business; or</li>
	
	<li>Use the Beta Version or making available the Beta Version to the third parties in order to excite a curiosity relating to obscene matter or similar matter in any way including texts, video images, voice or sound.</li>
</ul>
</p>
	
	
			
<p><strong>(1)</strong> You agree to resolve at your cost and responsibility any dispute, arising out of your use of the Beta service, including but not limited to, a claim of defamation, a breach of privacy or a claim relating to </p>
use some domain name.

<p><strong>(1)</strong> You shall be subject to rules established by users of Internet, such as a prohibition of sending "spam" mails".</p>

<p><strong>(1)</strong> If you are aware that any person uses the Beta Version for illegal acts or immoral acts, including but not limited to, operation of a "fishing site", you shall inform <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> it immediately.</p>



	<p><strong><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>'s Warranty</strong></p>

<p><strong>(1)</strong> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> warrants that the Software does not contain any instructions intentionally designed to modify, damage, destroy, record or transmit information within a computer, computer system or computer network without the intent or permission of the owner of the information. This warranty does not apply to any "open source" code included in or incorporated into the Software. During the term of this Agreement, if any portion of the Software (other than "open source" code) is found to be in violation of this warranty, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> will, at its sole expense, make reasonable commercial efforts to modify or replace the Software so that it complies with this warranty, without any material loss of the Software's functionality, and You will permit <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> a commercially reasonable amount of time to effect such modification or replacement prior to pursuing any other remedy for breach of this warranty.</p>
	
	<p><strong>Disclaimers</strong></p>

  <p><strong>(1)</strong> The Software is provided "as-is" and <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> makes no representation or warranty for the Software, whether express or implied. <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> makes no representation or warranty of any kind, whether express or implied, including but not limited to the implied warranties of merchantability or fitness for a particular purpose.</p>
  
  <p><strong>(2)</strong> You assume all risk associated with the quality, performance, installation and use of the Software, including, but not limited to, loss of data or software programs, or unavailable or interruption of operations. You are solely responsible for determining the appropriateness of use of the Software and assume all risks associated with its use.</p>

  <p><strong>(3)</strong> If one of following matter is occurred, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> may suspend or discontinue the Beta Version, at any time, in its sole direction. <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> shall not be liable to you or to any third party for such suspension or discontinuance of the Beta Version.</p>
  
	<ul>
		<li>Increase of traffics or data storage, determined by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>, which affects a material impact on a system of the Beta Version or which interferes other users' use.</li>
	<li>Use of the Beta Version, which is determined by <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> to be similar to the above and which affects a material impact on a system of the Beta Version.</li>
		<li><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> will make reasonable efforts to maintain the Version, However, <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is not responsible for any damage, loss of data, custom information or vendor data, revenue, or other harm to business arising out of delays, misdelivery or nondelivery of information, restriction or loss of access, bugs or other errors, unauthorized use due to your sharing of access to the Beta Version. You are responsible for maintaining and backing-up your data and information that may reside on the Beta Version. <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> does not warrant that (i) The Beta Version Will meet your specific requirements, (ii) The Beta Version will be uninterrupted, timely, secure or error-free, (iii) The results that may be obtained from the use of the Beta Version will be accurate or reliable.</li>
	</ul>



<p><strong>Indemnification</strong></p>

<p><strong>(1)</strong> You expressly understand and agree that <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a>, its officers, directors, employees, agents, subsidiaries, affiliates and other partners will not be liable for any direct, indirect, incidental, special, consequential or exemplar damage entered into through or from the Beta Version.</p>



	<blockquote>
			<p><strong>Note: </strong>All Terms of the Beta Service Agreement are subject to change without advanced notice, and any changes will be posted here.</p>
	</blockquote>
	
			


	
</div>
*/ ?>