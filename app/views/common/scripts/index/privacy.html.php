<div class="contentBox">
		

	<div class="contentBoxTitle">
		<h1>Privacy Policy.</h1>
	</div>


	<h3>We respect your privacy.</h3>

	<p>Thank you for visiting <a href="/index"><?=COMPANY_WEBSITE;?></a>. This privacy policy tells you how we use personal information collected at this site. Please read 
	this privacy policy before using the site or submitting any personal information. By using the site, you are accepting the practices 
	described in this privacy policy. These practices may be changed, but any changes will be posted and changes will only apply to 
	activities and information on a going forward, not retroactive basis. You are encouraged to review the privacy policy whenever you 
	visit the site to make sure that you understand how any personal information you provide will be used.<p>

	<blockquote>
		<p style="font-size: 14px;"><strong>Note:</strong> the privacy practices set forth in this privacy policy are for this web site only. If you link to other web sites, 
			please review the privacy policies posted at those sites.</p>
	</blockquote>
	
	<hr>

	<h5>Collection of Information</h5>
	<p>We collect personally identifiable information, like names, postal addresses, email addresses, etc., when voluntarily submitted by our visitors. The information you provide is used to fulfill you specific request. This information is only used to fulfill your specific request, unless you give us permission to use it in another manner, for example to add you to one of our mailing lists.</p>

	<hr>
	
	<h5>Cookie/Tracking Technology</h5>
	<p>The Site may use cookie and tracking technology depending on the features offered. Cookie and tracking technology are useful for gathering information such as browser type and operating system, tracking the number of visitors to the Site, and understanding how visitors use the Site. Cookies can also help customize the Site for visitors. Personal information cannot be collected via cookies and other tracking technology, however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.</p>

	<hr>
	
	<h5>Distribution of Information</h5>
	<p>We may share information with governmental agencies or other companies assisting us in fraud prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating fraud which has already taken place. The information is not provided to these companies for marketing purposes.</p>

	<hr>
	
	<h5>Commitment to Data Security</h5>
	<p>Your personally identifiable information is kept secure. Only authorized employees, agents and contractors (who have agreed to keep information secure and confidential) have access to this information. All emails and newsletters from this site allow you to opt out of further mailings.</p>

	<hr>
	
	<h5>Privacy Contact Information</h5>
	<p>If you have any questions, concerns, or comments about our privacy policy you may contact us by e-mail: <a href="mailto:<?=HELP_EMAIL;?>"><?=HELP_EMAIL;?></a></p>




	
	<p class="negative-margin-15" style="padding-top: 25px;"><center>We reserve the right to make changes to this policy at any time. Any changes to this policy will be posted.</center></p>

		
</div>