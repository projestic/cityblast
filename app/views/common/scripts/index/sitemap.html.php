<div class="contentBox">
	
	<div class="contentBoxTitle">
		<h1>Sitemap</h1>
	</div>

	<ul class="sitemap">
		<li style="width:160px">
			<h5><?= COMPANY_NAME ?>.</h5>
			<ul>
				<? if(strtolower(COMPANY_NAME) != "myeventvoice") : ?>
					<li><a href="/index/why-us">Why <?= COMPANY_NAME ?></a></li>
				<? endif; ?>
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<li><a href="/index/how-blasting-works">Blasting</a></li>
				<?php endif; ?>
				<li><a href="/index/pricing">Pricing</a></li>
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<li><a href="/blog">Blog</a></li>
				<? endif; ?>
			</ul>
		</li>
		<li style="width:180px">
			<h5>About.</h5>
			<ul>
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<li><a href="/index/our-story">Our Story</a></li>
				<? endif; ?>
				<li><a href="/index/faq">FAQ</a></li>
				<li><a href="/index/privacy">Privacy Policy</a></li>
				<li><a href="/index/terms">Terms and Conditions</a></li>
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<li><a href="/press/index">Press</a></li>
				<? endif; ?>
				<li><a href="/jobs/index">We're Hiring <span style="color: #790000;">*</span></a></li>
			</ul>
		</li>
		<li style="width:140px">
			<h5>Help.</h5>
			<ul>
				<li><a href="/help/index">Help</a></li>
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
				<li><a href="/listing/index">Search</a></li>
				<? endif; ?>
			</ul>
		</li>
		<li style="width:170px">
			<h5>Developers.</h5>
			<ul>
				<li><a href="/index/api">REST API</a></li>
				<li><a href="/api/index">Documentation</a></li>
			</ul>
		</li>
		<li style="width:120px">
			<h5>Get Approved.</h5>
			<ul>
				<li><a href="/affiliate/signup">Marketing Partner</a></li>
				<li><a href="/index/training">Training Affiliate</a></li>
				<li><a href="/index/content">Content Provider</a></li>
				<? if(strtolower(COMPANY_NAME) == "cityblast") : ?>
					<li><a href="/index/idx">Franchise Partner</a></li>
				<? endif; ?>
				<li><a href="/index/ambassador">Brand Advocate</a></li>
			</ul>
		</li>
	</ul>
	<? if (count($this->active_listing_cities) > 0) { ?>
	<div class="clear" style="margin-bottom:30px"></div>
	<ul>
		<li>
			<h5>Cities</h5>
			<ul class="sitemap">
				<?php foreach($this->active_listing_cities as $city) { ?>
				<li><a href="/listing?city_id=<?php echo $city->id; ?>"><?php echo $city->name; ?></a></li>
				<? } ?>
			</ul>
		</li>
	</ul>
	<? } ?>
	<div class="clear"></div>

</div>