<?php
$pagingParams = array('extraParams' => array(
	'field' => $this->field,
	'sort_by' => $this->sort_by,
	'sort_dir' => $this->sort_dir
));
?>

<div class="container_12 clearfix">

	<div class="grid_12" style="margin-top: 8px; margin-bottom: 10px;">
		<form id="filter" method="POST" action="/textfix/">
		<h1 style="margin: 0px;">Text Fix
					
			<span style="float: right;">
				<select name="field" id="field" style="float: left; font-size: 22px;  margin-right: 10px;">
				<?php
					$values = array(
						'' => 'Any', 
						'listing.title' => 'Listing Title',  
						'listing.message' => 'Listing Message', 
						'listing.raw_message' => 'Listing Raw Message', 
						'listing.description' => 'Listing Description', 
						'listing_description.description' => 'Listing Description Extra'
					);
				?>
				<?php foreach ($values as $value => $label) :?>
					<option value="<?= $value ?>"<?= ($this->field == $value) ? ' selected="1"' : '' ?>><?= $label ?></option>
				<?php endforeach; ?>
				</select>
			</span>
		</h1>

		<div class="clearfix" style="float:right; margin-top: 10px;"><input type="submit" class="uiButton" value="Update"/></div>
		</form>

	</div>
	
	<div class="grid_12" style="margin-top: 8px; margin-bottom: 10px;">
	<div class="grid_12" style="margin-bottom: 10px; float: right;">
		<div style="float: right;" class="clearfix"><?= ($this->paginator) ? $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml', $pagingParams) : ''; ?></div>
	</div>
	<?php foreach ($this->paginator as $data): ?>
	<input type="hidden" name="table_name" value="<?=$data->table_name?>" />
	<input type="hidden" name="row_id" value="<?=$data->row_id?>" />
	<input type="hidden" name="field_name" value="<?=$data->field_name?>" />
	<h2><?=$data->table_name . '.' . $data->field_name?> 
	<?php if ($data->listing): ?>
		<?php if ($data->listing->blast_type == 'LISTING'): ?>
		(<a href="/listing/update/<?=$data->row_id?>" target="new"><?=$data->row_id?></a>)
		<?php else: ?>
		(<a href="/content/edit/<?=$data->row_id?>" target="new"><?=$data->row_id?></a>)
		<?php endif; ?>
	<?php else: ?>
		(<?=$data->row_id?>)
	<?php endif; ?>
	</h2>
	
	<div>
		<h3>Current Listing Data</h3>
		<div style="padding: 10px; border: solid 1px; font-size: 12px;">
			<?php if ($data->listing): ?>
				<strong style="font-size: 140%;">Link</strong><div><a href="<?=$data->listing->content_link?>" target="new"><?=$data->listing->content_link?></a></div>
				<strong style="font-size: 140%;">Title</strong><div><?=$data->listing->title?></div>
				<strong style="font-size: 140%;">Message</strong><div><?=nl2br($data->listing->message)?></div>
				<strong style="font-size: 140%;">Raw Message</strong><div><?=nl2br($data->listing->raw_message)?></div>
				<strong style="font-size: 140%;">Description 1</strong><div><?=nl2br($data->listing->description)?></div>
				<?php if ($data->listing->descriptions): ?>
					<?php foreach ($data->listing->descriptions as $x => $desc): ?>
						<strong style="font-size: 140%;">Description <?=$x+2?></strong><div><?=nl2br($desc->description)?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			<?php else: ?>
				!! Listing was deleted !!
			<?php endif; ?>
		</div>
	</div>
	<div>
		<h3>Original
			<?php if ($this->canAccess('/textfix/fixapply')): ?>
			<div class="clearfix" style="float:right; margin-top: 0px;"><input type="submit" class="uiButton" name="apply_original" value="Apply Original"/></div>
			<?php endif; ?>
		</h3>
		<div style="padding: 10px; border: solid 1px; font-size: 12px;"><?=nl2br($data->original)?></div>
	</div>
	<div>
		<h3>Auto Fix
			<?php if ($this->canAccess('/textfix/fixapply')): ?>
			<div class="clearfix" style="float:right; margin-top: 0px;"><input type="submit" class="uiButton" name="apply_auto1" value="Apply Auto 1"/></div>
			<?php endif; ?>
		</h3>
		<div style="padding: 10px; border: solid 1px; font-size: 12px;"><?=nl2br($data->auto1)?></div>
	</div>
	<div>
		<h3>Manual Fix</h3>
		<div>
			<?php if ($this->canAccess('/textfix/fixapply')): ?>
			<div class="clearfix" style="float:right; margin-top: 0px;"><input type="submit" class="uiButton" name="apply_manual" value="Apply Manual"/></div>
			<?php endif; ?>
		</div>
		<textarea name="manual" style="padding: 10px; width: 980px; height: 240px;"><?=(!empty($data->manual)) ? $data->manual : $data->original;?></textarea>
		<div>
			<div class="clearfix" style="float:left; margin-top: 0px;"><input type="submit" class="uiButton" name="save_manual" value="Save"/></div>
		</div>
	</div>
	<?php endforeach; ?>
	</div>
	</div>
</div>

<script type="text/javascript">


$(function() {
	$('input[name=save_manual]').click(function(){
		var values = {
			table_name: $('input[name=table_name]').val(),
			row_id: $('input[name=row_id]').val(),
			field_name: $('input[name=field_name]').val(),
			text: $('textarea[name=manual]').val()
		};
		
		$.ajax({
			type: 'POST',
			url: '/textfix/manualsave',
			data: values
		}).done(function(data){
			if (data.result) {
				moveToNextPage();
			} else {
				alert('Save failed !!!');
			}
		});
	});
	
	$('input[name=apply_original]').click(function(){
		fixApply('original');
	});
	$('input[name=apply_auto1]').click(function(){
		fixApply('auto1');
	});
	$('input[name=apply_manual]').click(function(){
		fixApply('manual');
	});
	
	function fixApply(applyType)
	{
		var values = {
			table_name: $('input[name=table_name]').val(),
			row_id: $('input[name=row_id]').val(),
			field_name: $('input[name=field_name]').val(),
			text: $('textarea[name=manual]').val(),
			type: applyType
		};
		
		$.ajax({
			type: 'POST',
			url: '/textfix/fixapply',
			data: values
		}).done(function(data){
			if (data.result) {
				$('#filter').trigger('submit');
			} else {
				alert('Failed to apply fix !!!');
			}
		});
	}
	
	function moveToNextPage()
	{
		window.location = $('li.next a').first().attr('href');
	}
});

</script>