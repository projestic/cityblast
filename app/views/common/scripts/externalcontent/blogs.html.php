<div class="container_12 clearfix">
<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

<?php
/** @var Zend_Paginator $this->itemsPaginator */
/** @var Zend_Paginator $itemsPaginator */
$itemsPaginator = $this->itemsPaginator;

/** @var string $lastAction*/
$lastAction = $this->lastAction;

/** @var string $blogId*/
$blogId = $this->blogId;

/** @var string $blogName*/
$blogName = $this->blogName;

/** @var string[] $errors*/
$errors = $this->errors;

$urlEditBase = '/externalcontent/blogedit/';
?>

<?php if(!empty($this->message)): ?>
	<div class="notice">
		<h3 style="color: #005826;"><?php echo $this->message;?></h3>
	</div>
<?php endif; ?>

<?php if(count($errors)): ?>
	<div class="notice">
		<h3 style="color: #005826;">Warnings:</h3><br/>
		<?= implode("\n<br/>", $errors) ?>
	</div>
<?php endif; ?>

<?php if($lastAction == 'created') {
	$link = Blastit_ExternalContent_Blog_View_Man2::link($urlEditBase, $blogId, $blogName);
?>
	<div class="well well-small">
		<h4 style="color: #005826;">Blog has been created: <?= $link ?></h4>
	</div>
<?php } ?>

<?php if($lastAction == 'updated') {
$link = Blastit_ExternalContent_Blog_View_Man2::link($urlEditBase, $blogId, $blogName);
?>
	<div class="well well-small">
		<h4 style="color: #005826;">Blog has been updated: <?= $link ?></h4>
	</div>
<?php } ?>

<?php if($lastAction == 'deleted') { ?>
	<div class="well well-small">
		<h4 style="color: #005826;">Blog has been removed: <?= $blogName ?></h4>
	</div>
<?php } ?>

<?php if($lastAction == 'error') { ?>
	<div class="well well-small">
		<h4 style="color: #005826;">Error saving blog: <?=$blogName?></h4>
	</div>
<?php } ?>

<div class="clear"></div>

<div class="grid_12"><h1>Blogs <span style="display: inline; float: right"><a href="/externalcontent/blogedit/" class="uiButton" style="margin-top: 0px; padding-top:0px; margin-right: 0px;">Add blog</a> <a href="/externalcontent/download_blog_list" class="uiButton" style="margin-top: 0px; padding-top:0px; margin-right: 0px;">Download</a></h1></div>

<table style="width: 100%;" class="uiDataTable">
<tr>
	<th align="left">ID</th>
	<th align="left">Name</th>
	<th align="left">URL</th>
	<th align="left">Tier</th>
	<th align="left">Created</th>
	<th align="left" style="text-align: center;">Actions</th>
</tr>

<?
$parityTypeBool = true;
$viewMan = new Blastit_ExternalContent_Blog_View_Man();
foreach($itemsPaginator as $blogItem)
{
	$parityClass = Blastit_ExternalContent_Blog_View_Man2::oddEvenClass($parityTypeBool);
	echo "\n<tr {$parityClass}>\n";
	echo $viewMan->row($blogItem);
	echo "\n</tr>\n";
}
?>
</table>
<div style="float: right;" class="clearfix"><?= $this->paginationControl($itemsPaginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />
</div>
</div>

