<?php
/** @var string $actionLast*/
$actionLast = $this->actionLast;

/** @var Blog $blogItem*/
$blogItem = $this->blogItem;

$id = null;
$name = null;
$url = null;
$tier = null;
$created_at = 'not set';
$seek_images_in_external_article = 0;
$itemExists = false;
if (is_object($blogItem)) {
	$itemExists = true;
	$id = $blogItem->id;
	$name = $blogItem->name;
	$url = $blogItem->url;
	$tier = $blogItem->tier;
	$seek_images_in_external_article = $blogItem->seek_images_in_external_article;
	if(!empty($blogItem->created_at)) {
		$created_at = $blogItem->created_at->format('M-d-Y H:i');
	}
}

$tiers = ExternalcontentBlogTier::getAllWithNames(false, true);
?>
<form action="/externalcontent/blogeditsave" method="POST" class="clearfix">
<input type="hidden" name="id" value="<?= $id ?>" />

<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<div class="container_12 clearfix admin-update-listing" style="padding-top: 60px; padding-bottom: 60px;">
	<div class="grid_12">
		<div class="box form">
			<div class="box_section">
				<h1>Blog</h1>
			</div>

			<ul class="clearfix uiForm full">
		
				<li class="uiFormRow clearfix">
					<ul class="clearfix">
						<!-- name -->
						<?php Blastit_ExternalContent_Blog_Edit_Man::inputText('Name', 'name', $name) ?>

						<!-- name -->
						<?php Blastit_ExternalContent_Blog_Edit_Man::inputText('URL', 'url', $url) ?>

						<!-- seek_images_in_external_article -->
						<li class="fieldname">Seek images in external article <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="padding-top: 20px;">
							<input type="checkbox" name="seek_images_in_external_article" id="seek_images_in_external_article" value="on" class="uiInput"<?=($seek_images_in_external_article) ? ' checked="checked"' : ''?> />
						</li>

						<!-- tier -->
						<li class="fieldname">Tier <span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field">
							<span class="uiSelectWrapper">
								<span class="uiSelectInner">
									<select name="tier" class="uiSelect">
										<?
										$tierIdSelected = $tier;
										if (!$tierIdSelected) {
											$tierIdSelected = ExternalcontentBlogTier::Tier_1;
										}
										echo $this->partial('externalcontent/_tierSelectOptions.html.php', array('tiers' => $tiers, 'tierIdSelected' => $tierIdSelected));
										?>
									</select>
								</span>
							</span>
						</li>

						<!-- Created -->
						<? if($itemExists) {?>
						<li class="fieldname">Created</li>
						<li class="field pageTextBig pageTextLineHeight"><div><?= $created_at ?></div></li>
						<? } ?>
					</ul>
				</li>
			</ul>
		</div>

		<!-- Save button -->
		<? if($itemExists) :?>
			<input type="submit" name="submitted" class="uiButton large right"  value="Save">
		<? else : ?>
			<input type="submit" name="submitted" class="uiButton large right" value="Add">
		<? endif; ?>		
	</div>
</div>
</form>
