<?php
/** @var ExternalcontentBlogTier[]|string[] $tiers */
$tiers = $this->tiers;

/** @var int $tierIdSelected */
$tierIdSelected = $this->tierIdSelected;

foreach($tiers as $tierId => $tierName)
{
	$isSelected = $tierIdSelected == $tierId;
	$isSelectedStr = $isSelected ? 'selected' : null;
	echo "<option value='" . $tierId . "' " . " $isSelectedStr ";
	echo ">"; 
	if($tierName == "All") echo "All Tiers";
	else echo "Tier ".$tierName; 
	echo "</option>";
}
?>