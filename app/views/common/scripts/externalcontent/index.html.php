<div class="container_12 clearfix">

<?php
/** @var Zend_Paginator $blogEntriesPaginator */
$blogEntriesPaginator = $this->blogEntriesPaginator;

/** @var string[] $errors*/
$errors = $this->errors;

/** @var string $lastAction*/
$lastAction = $this->lastAction;

/** @var string $blogEntryTitle*/
$blogEntryTitle = $this->blogEntryTitle;

/** @var string $listingId*/
$listingId = $this->listingId;

/** @var int $page*/
$page = $this->page;

/** @var string $sortField*/
$sortField = $this->sortField;

/** @var string $sortOrder*/
$sortOrder = $this->sortOrder;

/** @var string $filterTier*/
$filterTier = $this->filterTier;

$urlBase = '/externalcontent/index/';
$sortLinkMan = new Blastit_ExternalContent_View_Link($urlBase, $page, $sortField, $sortOrder, $filterTier);

$tiers = ExternalcontentBlogTier::getAllWithNames(false, false);
?>



<div class="grid_12" style="margin-top: 30px; margin-bottom: 80px;">

<?php if(!empty($this->message)): ?>
	<div class="notice">
		<h3 style="color: #005826;"><?php echo $this->message;?></h3>
	</div>
<?php endif; ?>

<?php if(count($errors)): ?>
	<div class="notice">
		<h3 style="color: #005826;">Warnings:</h3><br/>
		<?= implode("\n<br/>", $errors) ?>
	</div>
<?php endif; ?>

<?php if($lastAction == 'approved'): ?>
	<div class="well well-small">
		<h4 style="color: #005826;">Blog entry has been approved!</h4>
		<p>It has been moved to the following content: <a href="content/edit/<?= $listingId?> "><?= $listingId ?></a></p>
	</div>
<?php endif; ?>

<?php if($lastAction == 'deleted'): ?>
	<div class="well well-small">
		<h4 style="color: #005826;">Blog entry has been removed</h4>
	</div>
<?php endif; ?>


<div class="clear"></div>

<table style="width: 100%; border-bottom: 0px;">
	<tr>
		<td style="border-bottom: 0px;">
			<h1>
				Content (<?=number_format($blogEntriesPaginator->getTotalItemCount());?>)
				
				<span style="float: right;">
				
					<!-- style="margin-top: 30px; text-align: right" -->
					<select name="filterTier" class="page_BoxNice_Font" onchange="window.location.replace('<?= $urlBase ?>filterTier/' + this.value);" style="width: 100px;">
						<?
						$tierIdSelected = $filterTier;
						if (!$tierIdSelected) 
						{
							$tierIdSelected = ExternalcontentBlogTier::Any;
						}
						$tiers = array_merge(array(ExternalcontentBlogTier::Any => 'All'), $tiers);
						echo $this->partial('externalcontent/_tierSelectOptions.html.php', array('tiers' => $tiers, 'tierIdSelected' => $tierIdSelected));
						?>
					</select>
				</span>
			</h1>
		</td>
	</tr>
</table>


<form method="post">

<table style="width: 100%;" class="uiDataTable">
<tr>
	
	<th align="left"></th>
	
	<th align="left"><?= $sortLinkMan->linkButton('ID', 'id')?></th>
	<th align="left"><?= $sortLinkMan->linkButton('Article date', 'article_date')?></th>
	<th align="left"><?= $sortLinkMan->linkButton('Tier', 'tier')?></th>
	<th align="left"><?= $sortLinkMan->linkButton('Blog', 'blog_name')?></th>	
	<th align="left">Title</th>
	<th align="left" style="text-align: center;">Actions</th>
</tr>

<?
$row = true;
$paginator = $blogEntriesPaginator;
$viewMan = new Blastit_ExternalContent_View_Man($sortField);
foreach($paginator as $blogEntry)
{
	echo "<tr ";
	if($row == true)
	{
		$row = false;
		echo "class='odd' ";
	}
	else
	{
		$row = true;
		echo "class='' ";
	}
	echo ">";
	
	
	//echo $viewMan->row($blogEntry);
	
	echo "<td style='vertical-align: top;'><input type='checkbox' name='delete[]' value='" . $blogEntry->id . "' /></td>";
	
	echo "<td style='vertical-align: top;'><a href='/externalcontent/edit/" . $blogEntry->id . "' style='text-decoration: none;'>" . $blogEntry->id . "</a></td>";

	$createdAtFormatted = null;
	if(isset($blogEntry->article_date) && !empty($blogEntry->article_date)) {
		$createdAtFormatted = $blogEntry->article_date->format('M-d-Y H:i');
	}
	echo "<td style='vertical-align: top; color: #a2a2a2'>" . $createdAtFormatted . "</td>";
	
	
	echo "<td style='vertical-align: top;'>" . $blogEntry->blog->tier . "</td>";
		
	echo "<td style='vertical-align: top;'>" . $blogEntry->blog->name . "</td>";
	//echo "<td style='vertical-align: top;'>" . $blogEntry->url . "</td>";
	
	
	$message = stripslashes(strip_tags($blogEntry->title));
	echo "<td style='vertical-align: top; width: 400px; font-size: 12px;'><a href='/externalcontent/edit/" . $blogEntry->id . "' style='text-decoration: none; color: #000000'>" . mb_substr($message, 0, 50) . " ...</a></td>";
					
	
	echo "<td style='text-align: right;' nowrap>";
	echo "<a href='/externalcontent/edit/" . $blogEntry->id . "'>edit</a>";
	echo "</td>";
	echo "</tr>";
}

?>
</table>

<div class="clearfix"><input type="submit" value="Delete checked"  title="Delete checked entries" class="uiButton alt" /></div><br />
</form>

<div style="float: right;" class="clearfix"><?= $this->paginationControl($blogEntriesPaginator, 'Elastic', '/common/pagination.phtml'); ?></div><br />
</div>
</div>

