<?php
/** @var BlogPending $this->blogEntry */
/** @var BlogImagePending[] $this->blogImages */

/** @var BlogPending $blogEntry */
$blogEntry = $this->blogEntry;
/** @var BlogImagePending[] $blogImages */
$blogImages = $this->blogImages;

/** @var string $this->blogSourceUrl */
/** @var string $blogSourceUrl */
$blogSourceUrl = $this->blogSourceUrl;

$blogEntryId = $blogEntry->id;

$description = $blogEntry->description;
$description = strip_tags($description);
if (mb_strlen($description) > 400) {
	$description = mb_substr($description, 0, 400);
	$description .= ' ...';
}

?>
<?php
//echo $this->render('externalcontent/_css.html.php');
?>
<link href="/css/forms.css" rel="stylesheet" type="text/css" />
<?php if(!empty($this->message)): ?>
	<div class="notice">
		<h3 style="color: #005826;"><?php echo $this->message;?></h3>
	</div>
<?php endif; ?>

<div class="container_12 clearfix admin-update-listing" style="margin-top: 30px; padding-bottom: 60px;">
	<div class="grid_12">
		<div class="box form">
			<div class="box_section">
				<h1>
					Blog entry #<?= $blogEntry->id ?>
					<div style='text-align:right; font-size: 22px; font-weight: bold; float:right;'><!-- right pane --></div>
				</h1>
			</div>

			<form action="" method="POST" id="approvalForm">

				<div class="clearfix" style="padding: 0 0 20px;">
					<ul class="uiForm full clearfix">
						<li class="uiFormRow clearfix" style="width: auto; background: none; border-top-color: #e5e5e5; border-radius: 0px;">
							<ul>
								<!-- Title -->
								<li class="fieldname">Title</li>
								<li class="field pageTextBig"><div><?= $blogEntry->title ?></div></li>

								<!-- Link -->
								<li class="fieldname">Content link</li>
								<li class="field pageTextBig">
									<div><a href="<?= $blogEntry->content_link ?>" style="color: #565656;"><?= $blogEntry->content_link ?></a>
									</div>
								</li>

								<!-- Source blog -->
								<li class="fieldname"><span style="color: #808080;">Source feed</span></li>
								<li class="field pageTextBig">
									<div><a href="<?= $blogSourceUrl ?>" style="color: #808080;"><?= $blogSourceUrl ?></a>
									</div>
								</li>

								<!-- Description -->
								<li class="fieldname">Description</li>
								<li class="field pageTextBig pageTextLineHeight"><div><?= $description ?></div></li>

								<!-- Images -->
								<li class="fieldname">Images</li>
								<li class="field">
									<table>
									<?php foreach ($blogImages as $blogImage) : ?>
										<tr>
											<td>
													<input type="checkbox" name="images[]" value="<?= $blogImage->id ?>" />
											</td>
											<td>
												<a href="<?= CDN_URL . $blogImage->image ?>" target="_blank"><img src="<?= CDN_URL . $blogImage->image ?>" style="width: 200px;"></a>
												<?php if ($blogImage->width >= 400 && $blogImage->height >= 200): ?>
												<div style="font-weight:bold; font-size:15px;">Big</div>
												<?php endif; ?>
												<div><?=$blogImage->width . 'x' . $blogImage->height?></div>
											</td>
										</tr>
									<?php endforeach ?>
									</table>
									Check the images you want to import when approving this listing.
								</li> <!-- images -->
							</ul>
						</li>
					</ul>
				</div>

				<div class="clearfix">
					<input type="hidden" name="op" value="" />
					<input type="submit" value="Approve" id="blogApprove" data-id="<?= $blogEntryId ?>" title="Approve the record" class="uiButton right alt"/>
					<span style="display: inline-block; float: right; border-right: 4px solid rgba(0, 0, 0, 0);">&nbsp;</span>
					<input type="submit" value="Delete" id="blogDelete" data-id="<?= $blogEntryId ?>" title="Delete the record" class="uiButton right cancel"/>
				</div>
			</form>
		</div> <!-- box form -->
	</div> <!-- grid_12 -->
</div> <!-- container_12 -->


<script type="text/javascript">
	
	$( function () {
	
		$('form#approvalForm').bind('submit', function (e) {

			var message = '';
			var op = $('input[name=op]').val();
			
			if (op == 'Approve') {
				message = 'Are you sure you want to approve this record and import checked images?';
			} else if (op == 'Delete') {
				message = 'Are you sure you want to delete this record?';
			} else {
				return false;
			}
			
			var isSure = confirm(message);

			if (!isSure) {
				return false;
			}
		
		});
		
		$('input[type=submit]').click( function (e) {
			$('input[name=op]').val(e.target.value);
		});
	
	});
	
</script>

<style type="text/css">
	.pageTextBig {
		font-size: 12pt;
		font-family: myriad-pro,Helvetica,Arial,Verdana,sans-serif;
		/*font-weight: 400;*/
	}
	.pageTextLineHeight {
		line-height: 17px;
	}
</style>
