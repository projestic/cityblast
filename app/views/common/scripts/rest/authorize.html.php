<div class="row-fluid">

    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Connect your <?= $this->client->client_name ?> and <?=COMPANY_NAME;?> accounts.</h1>
		</div>
		
		<p>Hi there, <a href="<?= $this->client->client_site ?>"><?= $this->client->client_name ?></a> would like to connect your account to <?=COMPANY_NAME;?>.</p>
		
		
		<?/*
		<p><a href="<?= $this->client->client_site ?>"><?= $this->client->client_name ?></a> (<?= $this->client->client_site ?>) is requesting access to perform these actions on your <?=COMPANY_NAME;?> account:</p>
		
		<ul><?php foreach ($this->scopes as $scope) : ?>
			<li><?= $scope ?></li><?php endforeach; ?>
		</ul>*/ ?>

		<p>Did you want to let <a href="<?= $this->client->client_site ?>"><?= $this->client->client_name ?></a> to help manage your <?=COMPANY_NAME;?> listings and more?</p>

		<form method="post">
		  	<input type="submit" name="authorized" value="Allow Access" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 10px;">
			<?/*<input type="submit" name="authorized" value="no">*/?>
		</form>
		
		<div class="clearfix"></div>
		
	</div>
</div>
