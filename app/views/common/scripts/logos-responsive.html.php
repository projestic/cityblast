<div class="row-fluid" style="border-bottom: 1px solid #c3c3c3;">
	<div class="span3 logo_span" style="border-right: 1px solid #c3c3c3;">
		<a href="http://www.royallepage.ca/" target="_blank"><img src="/images/lp_logos/royal_lepage.png" /></a>
	</div>
	<div class="span3 logo_span" style="border-right: 1px solid #c3c3c3;">
		<a href="http://blog.lwolf.com/partners/news/lone-wolf-invests-in-cityblast-81759/" target="_blank"><img src="/images/lp_logos/lone_wolf.png" /></a>
	</div>
	<div class="span3 logo_span" style="border-right: 1px solid #c3c3c3;">
		<a href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/" target="_blank"><img src="/images/lp_logos/real_estate_talk_show.png" /></a>
	</div>
	<div class="span3 logo_span">	
		<a href="http://www.seevirtual360.com/" target="_blank"><img src="/images/lp_logos/see_virtual.png" /></a>
	</div>
</div>

<div class="row-fluid" style="border-bottom: 1px solid #c3c3c3;">
	<div class="span3 logo_span" style="border-right: 1px solid #c3c3c3;">
		<a href="http://www.techvibes.com/blog/cityblast-enables-canadian-real-estate-agents-to-build-their-business-through-social-media-2012-08-03" target="_blank"><img src="/images/lp_logos/techvibes.png" /></a>
	</div>
	<div class="span3 logo_span" style="border-right: 1px solid #c3c3c3;">
		<a href="http://www.canadianrealestatemagazine.ca" target="_blank"><img src="/images/lp_logos/real_estate_wealth.png" /></a>
	</div>
	<div class="span3 logo_span" style="border-right: 1px solid #c3c3c3;">
		<a href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/" target="_blank"><img src="/images/lp_logos/globe_and_mail.png" /></a>
	</div>
	<div class="span3 logo_span">	
		<a href="http://retechulous.com/" target="_blank"><img src="/images/lp_logos/retechulous.png" /></a>
	</div>
	
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.logo_span img').each(function(){
			$(this).css('opacity', .6);
			$(this).mouseover(function(){ $(this).css('opacity', 1); });
			$(this).mouseout(function(){ $(this).css('opacity', .6); });
		});
	});
</script>