<? include "email-header.html.php"; ?>

	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Congrats! You've got a lead from <?=COMPANY_NAME;?>!</em></h1>
	
	<p>One of your friends, <strong><?php echo $this->name;?></strong>, would like 	
		<? if($this->type == "REQUEST_INFO") : ?>
		some additional information about the following property:
		<? else : ?>
		to book a showing for the following propery:
		<? endif; ?>
	</p>



	<? include "show-listing.html.php"; ?>


	
	<h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px; padding-top: 25px;"><em>Contact Details for <?php echo $this->name;?></em></h2>
	<p style="margin-top: 10px; margin-bottom: 10px;">The best time to reach <?php echo $this->name;?> is <i><?php if(empty($this->best_time)) echo "anytime"; else echo $this->best_time;?></i> by 
	<? if(strtolower($this->contact_by) == "email") echo "<a href='mailto:".$this->email."' style='color:#3D6DCC;'>"; ?>
	<? echo strtolower($this->contact_by); ?>
	<? if(strtolower($this->contact_by) == "email") echo "</a>";?>.</p>

	<? if(isset($this->phone) && !empty($this->phone)) : echo $this->phone . '<br>'; endif; ?>  
	<? if(isset($this->email) && !empty($this->email)) : echo "<a href='mailto:".$this->email."' style='color:#3D6DCC;'>".$this->email . '</a>.<br>'; endif; ?>  
	


	<? if(!empty($this->message)) : ?>
		<h3 style="font-family:'Myriad Pro', Arial; padding-bottom: 0px; padding-top: 15px; margin:0px;" ><em>A Personal Message From <?php echo $this->name;?></em></h3>
		<p style="margin-top: 2px; padding-left: 8px; padding-top: 10px; padding-bottom: 10px; border-top: solid 1px #d9d9d9; border-bottom: solid 1px #d9d9d9; background-color: #f7f7f7;"><i><? if(!empty($this->message)) echo $this->message; ?></i></p>
	<? endif; ?>

	

	<h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px; padding-top: 25px;"><em><?=$this->listing_agent_name; ?>, Listing Agent</em></h2>
	<p style="margin-top: 5px;">Need more information about the listing? Contact <strong><?=$this->listing_agent_name; ?></strong>:</p>

	<p>Phone: <? if (isset($this->listing_agent_phone) && !empty($this->listing_agent_phone)) echo $this->listing_agent_phone; else echo "No phone number on record"; ?> 
	<br/>Email: <? if (isset($this->listing_agent_email) && !empty($this->listing_agent_email)) echo "<a href='mailto:".$this->listing_agent_email."' style='color:#3D6DCC;'>".$this->listing_agent_email."</a>"; else echo "No email on record"; ?></p>
	

	
	
	<? /*<h2 style="margin-bottom: -5px; padding: 10px; padding-left: 14px; border-top: solid 1px #C4C4C4; border-bottom: solid 1px #C4C4C4; background-color: #F2F2F2; text-align: center; font-size: 18px; font-weight: bold; font-family: Myriad Pro', Arial; ">Happy selling!  And remember:  Sold fast?  <a href='http://www.city-blast.com'>City-Blast</a>.</h2>*/ ?>
	
	<? include "email-signature.html.php"; ?> 

	
	<? /* Shim */ ?>
	<!--<table><tr><td>&nbsp;</td></tr></table>-->
	
	<? include "email-footer-menu.html.php"; ?>
	
	<? include "franchise-footer.html.php"; ?> 
	

<? include "email-footer.html.php"; ?>