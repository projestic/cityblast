<? include "email-header.html.php"; ?>
	
	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>We're currently unable to publish using one or more of your account(s).</em></h1>
	
	<p style="margin-top: 10px; margin-bottom: 10px;">Hello <?= $this->member->first_name;?>,</p>
	
	<p style="margin-top: 10px; margin-bottom: 10px;">It seems that we're unable to publish to one or more of your accounts, which means you're currently not generating leads.  Most likely you've recently changed your password.  
	Any time you change your password on 
	<a href="<?=APP_URL;?>/member/settings" style="color: #3D6DCC;">Facebook</a>, <a href="<?=APP_URL;?>/member/settings" style="color: #3D6DCC;">Twitter</a> or 
	<a href="<?=APP_URL;?>/member/settings" style="color: #3D6DCC;">LinkedIn</a>, you'll need to update your <a href="<?=APP_URL;?>/member/settings" style="color: #3D6DCC;"><?=COMPANY_NAME;?></a> 
	permissions.</p>
	
	<p style="margin-top: 10px; margin-bottom: 10px;">Updating <a href="<?=APP_URL;?>/member/settings" style="color: #3D6DCC;"><?=COMPANY_NAME;?></a> is super-quick and easy.  Just click on the link below and follow the simple instructions:</p>
	
	
	<? include "member-settings.html.php"; ?>


	<h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px; padding-top: 15px;"><em>Need more help?</em></h2>
	<p style="margin-top: 10px; margin-bottom: 10px;">If you're having problems receiving your Posts, feel free to drop us a line at: <a href="mailto:<?=HELP_EMAIL;?>" style="color: #3D6DCC;"><?=HELP_EMAIL;?></a>. We're here to help.</p> 

	<? include "email-footer-menu.html.php"; ?> 
	<? include "mortgagemingler-footer.html.php"; ?> 
<? include "email-footer.html.php"; ?>