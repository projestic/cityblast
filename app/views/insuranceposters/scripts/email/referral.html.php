<? include "email-header.html.php"; ?>
	


<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Your <? if(!empty($this->member)) echo "friend " .$this->member->first_name . " ". $this->member->last_name; else echo "friend"; ?> would like to share <?=COMPANY_NAME;?> with you!</em></h1>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; padding-top: 20px;">
Your friend <?= $this->member->first_name;?> is a member of our site, and has sent you this special note to offer you a <b>FREE 30-Day Trial</b> of our <b>Social Expert</b> service.</p>  

<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Our Experts act as your social media marketing assistants, and post hot new real estate articles, videos and information to your Facebook, Twitter and LinkedIn 
				accounts each day - so you don't have to!</p>  

<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Click the button below to check out some examples, and read more about how our service works.  We'd love to show you what we can do!</p>

		

	<table cellspacing="0" cellpadding="0" width="600;" border="0">
		<tr>
			<td width="490">
				&nbsp;
			</td>
			<td style="text-align: right; width: 100px; padding-top: 15px;">
				<a href="<?=APP_URL;?>/member/join?aff_id=<? if(!empty($this->member)) echo $this->member->id;?>"><img src="<?=APP_URL;?>/images/email/button_join_now.jpg" width="106px" height="40px"  border="0"/></a>
			</td>
		</tr>
	</table>
	
	<br/>
	<br/>
	<br/>
		
	<? /*include "email-signature.html.php";*/ ?> 
	
	<? include "email-footer-menu.html.php"; ?> 

<? include "email-footer.html.php"; ?>