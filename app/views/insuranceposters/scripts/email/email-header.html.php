<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$this->title;?></title>
<style type="text/css">
div, p, a, li, td { -webkit-text-size-adjust:none; }
</style> 
</head>

<body style="margin:0; padding:0;">
<table width="600" style="width: 600px; margin: 0 auto; display:block; border-collapse:collapse; font-family: Helvetica, Arial, sans-serif; font-size: 12px;" border="0" cellpadding="0" cellspacing="0">
	<tr>
    		<td>
        		<table border="0" cellpadding="0" cellspacing="0" bgcolor="#f2f2f2" width="600">
            		<tr>
                		<td width="170" style="width: 170px;"><a href="<?=APP_URL;?>"><img src="<?=APP_URL;?>/images/email/mortgagemingler-logo.png" alt="<?=COMPANY_NAME;?>" border="0" /></a></td>                    
                    	<td width="430" style="width:430px;">
                    		<table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="auto" align="center" style="margin:0 auto;">
                        		<tr>
                            		<td><img src="<?=APP_URL;?>/images/email/header-left.png" alt="" width="15" height="33" border="0" /></td>
                                	<td valign="middle">
										<span style="font-size: 13px;">Our members mingled with</span> 
										<span style="color:#e30000; font-size:13px"><strong><?=number_format($this->total_count);?></strong></span> 
										<span style="font-size: 13px;">potential clients today.</span>
									</td>
                                	<td><img src="<?=APP_URL;?>/images/email/header-right.png" alt="" width="15" height="33" border="0" /></td>
                            	</tr>
                        	</table>
                    	</td>
                	</tr>
          	</table>
        	</td>
			
			
    	</tr>
		<tr>
			<td>
				<table border="0" width="600" cellspacing="0" cellpadding="0" style="background-color: #454545; font-size: 11px;">
                	<tr>
                    	<td style="padding-left: 25px; padding-top: 8px; padding-bottom: 8px; padding-right: 40px;"><a href="/member/join" style="color: #FCFCC1;">Register Free</a></td>
					<td style="padding-top: 8px; padding-bottom: 8px; padding-right: 40px;"><a href="<?=APP_URL;?>/index/faqs" style="color: #E5E5E5">FAQ's</a></td>
                        <td style="padding-top: 8px; padding-bottom: 8px; padding-right: 50px;"><a href="<?=APP_URL;?>/member/how-clientfinder-works" style="color: #E5E5E5">How It Works</a></td>
                        <td style="padding-top: 8px; padding-bottom: 8px; padding-right: 50px;"><a href="<?=APP_URL;?>/index/guarantee" style="color: #E5E5E5">Our Guarantee</a></td>
					</tr>
				</table>
			</td>
		</tr>
    
    	<tr>
  		<td style="padding-top: 4px;">