<? include "email-header.html.php"; ?>

	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Your 14 Day Free Trial Starts Today.</em></h1>
	


	<p style="font-size: 13px;">Hello <?php echo $this->member->first_name;?>, we noticed that you didn't quite finish the <?=COMPANY_NAME;?> signup processes.<p>
		
	<p style="font-size: 13px;">Not to worry!</p>

	<p style="font-size: 13px;">We're so confident that our <?=COMPANY_NAME;?> experts will help you generate more positive exposure, produce more leads, and ultimately generate more deals from your social network, that  
		<strong>we're going to give you a free 14 day trial anyways</strong>.</p>

	<p style="font-size: 13px; padding-bottom: 10px;">We hope you enjoy your free trial of <?=COMPANY_NAME;?>, the World's most powerful social media tool for mortgage professionals!</p>
	
	
	<p style="padding-top: 10px; margin-top: 15px; border-top: 1px solid #d9d9d9; color: #898989;">To change your settings at any time, simply log in to 
		<a href="<?=APP_URL;?>" style="color: #3D6DCC;"><?=COMPANY_WEBSITE;?></a> and go to your <a href="<?=APP_URL;?>/member/settings" style="color: #3D6DCC;">personal settings</a> to make the adjustments you need.</p>
	
	
	<p style="color: #898989;">Your current  settings are as follows:</p>
	
	<br />


	<? include "member-settings.html.php"; ?> 

	
	<? include "email-footer-menu.html.php"; ?> 
	<? include "mortgagemingler-footer.html.php"; ?> 
<? include "email-footer.html.php"; ?>