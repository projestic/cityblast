<? include "email-header.html.php"; ?>



<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Your 14 Day Trial Is Over.</em></h1>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold;">You've just experienced the awesome power of <?=COMPANY_NAME;?>!</p>

<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">You know that looking professional on social media is vital to your business. But finding great content, editing it, and then 
	posting it on your accounts each and every day is both laborious and time-consuming.</p>  

<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?=COMPANY_NAME;?>'s experts take all of the hard work out of maintaining your social media presence. And, by keeping your 
	social media accounts consistently up-to-date with first class real estate and mortgage content, your friends and acquaintances will know you're a trusted professional.</p>  

<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">Don't lose your momentum!</a>


<p style="padding-bottom: 60px;">
<a href="<?=APP_URL;?>/member/settings" style='	
width: 260px;
margin: 10px 0 0;
float: right;
font-size: 17px;
margin: 10px 0;
height: 40px;
line-height: 40px;
padding: 0 15px;
background: url("<?=APP_URL;?>/images/button_bgs.png") repeat-x scroll 0 -15px transparent;
border: medium none !important;
border-radius: 5px 5px 5px 5px;
box-shadow: 0 1px 0 #FF6C6C inset, 0 -1px 0 #410000 inset;

cursor: pointer;

font-family: myriad-pro,Helvetica,Arial,Verdana,sans-serif;
font-size: 19px;
font-style: italic;
font-weight: 600;
letter-spacing: 0.02em;

outline: medium none !important;
position: relative;
text-align: center;
text-decoration: none !important;
text-shadow: -1px -1px 0 #A10000;
z-index: 3;
color: #FFFFFF;
outline: medium none;
text-decoration: none;' alt="Win More Business">Win More Business</a>

</p>
		
							
<table style="width: 600px; font-family: Helvetica, Arial, Verdana, sans-serif; font-size: 10px; background-color: #d9d9d9; margin-top: 25px;"  cellspacing="0" cellpadding="0">
		<tr>
			<td style="padding: 5px; ">
				<table cellspacing="0" cellpadding="0" style="background-color: #fff;">

	
					<tr>
						<td style="padding-bottom: 15px;">
								
							<table  cellspacing="0" cellpadding="0" style="width: 100%;">
								<tr>
									<td>
										<p style="margin: 0;padding: 10px; border-top: solid 1px #C4C4C4; border-bottom: solid 1px #C4C4C4; background-color: #F2F2F2; text-align: center; font-size: 18px; font-weight: bold; font-family: Myriad Pro, Arial; ">
										Generate new leads, <a href="<?=APP_URL;?>/member/join?aff_id=<? if(!empty($this->member)) echo $this->member->id;?>" style="color:#0000FF; text-decoration:none;">Activate <?=COMPANY_NAME;?> now</a>!</p>	
									</td>
								</tr>
							</table>
							
				
						</td>
					</tr>	
					
					<tr>
						<td>
							<table  cellspacing="0" cellpadding="0" style="width: 100%;">
								<tr>
									<td style="width: 500px; padding-left: 8px;">
										<h4 style="font-family:Myriad Pro, Arial; font-size: 15px; margin-top: 0px;">Why Use <?=COMPANY_NAME;?>?</h4>
										<p style="color: #4D4D4D; margin: 0; font-size: 10px;"><?=COMPANY_NAME;?> turns your Facebook, Twitter or LinkedIn into an incredibly powerful lead generation machine, 
											keeping your image professional and current, and creating vastly more business from your personal online network.</p>
										<ul style="font-size: 10px;">
											<li style="margin-bottom: 3px;">Enjoy a first class online presence, and a lucrative top-producer image</li>
											<li style="margin-bottom: 3px;">Welcome legions of brand new clients and earn more</li>
											<li>Effortlessly project the confident business savvy of a current mortgage professional</li>
										</ul>
										<p style="color: #4D4D4D; margin: 0; font-size: 10px;">Best of all? <?=COMPANY_NAME;?> never requires maintenance. Simply set up your <?=COMPANY_NAME;?> once, and watch your mortgage business grow.</p>
									</td>
									
									<td style="width: 500px; padding-right: 8px;">	
										<h4 style="font-family:Myriad Pro, Arial; font-size: 15px; margin-top: 0px;">How Does it Work?</h4>
										<ol style="font-size: 10px;">
											<li style="margin-bottom: 5px;">Simply register the <?=COMPANY_NAME;?> app. Follow the on-screen instructions to activate your Facebook, Twitter and LinkedIn.</li>
											<li style="margin-bottom: 5px;">As often as you choose, <?=COMPANY_NAME;?> experts will automatically update your Facebook, Twitter or LinkedIn with first-class mortgage and real estate 
												articles and videos. These polished updates look just like you've created them yourself, and are visible to your friends and followers.</li>
											<li>Your friends will see your professional tips and info, and email or message you with questions, comments or to request rates. You simply 
												welcome their inquiries, build your client relationships, and earn more.</li>
										</ol>

									</td>
									
									
									<?/*
									<td style="vertical-align: top; width: 275px; padding-right: 10px; padding-left: 10px;">
										<h4 style="font-family:Myriad Pro, Arial; font-size: 15px;margin: 3px 0; width: 160px;">Testimonials</h4>
										<table>
											<tr>
												<td style="vertical-align: top;">
													<img src="<?=APP_URL;?>/images/email/clientfinder_feature_icons_4.jpg" alt="" height="30px" width="30px" style="margin-top: 2px;"  border="0"/>
												</td>
												<td style="vertical-align: top; padding-left: 5px; padding-bottom: 25px;">
													<h5 style="font-family:Myriad Pro, Arial; font-size: 12px;margin: 3px 0;">
														Leila Talibova, <span style="font-size: 10px;font-weight: normal;font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif">HomeLife Victory Realty Inc.</span>
													</h5>
													<p style="color: #4D4D4D; margin: 0; font-size: 10px;">
														My friends and previous colleagues noticed my Facebook updates right away. In only my first week using CityBlast, I received a call from a family friend who said she'd seen my Facebook and wanted list their home with me! They loved when I said I'd use CityBlast to market their home - it sold in just days!
													</p>
												</td>
											</tr>
											<tr>
												<td style="vertical-align: top;">
													<img src="<?=APP_URL;?>/images/email/clientfinder_feature_icons_5.jpg" alt="" height="30px" width="30px" style="margin-top: 2px;" border="0"/>
												</td>
												<td style="vertical-align: top; padding-left: 5px; padding-bottom: 25px;">
													<h5 style="font-family:Myriad Pro, Arial; font-size: 12px;margin: 3px 0;">
														Shaun Nilsson, <span style="font-size: 10px;font-weight: normal;font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif">CityBlast Founder</span>
													</h5>
													<p style="color: #4D4D4D; margin: 0; font-size: 10px;">
														When I began in real estate, I didn't have any clients or leads. I began asking more established agents if I could post their listings on my Facebook Wall to attract buyers. What a revelation! I was able to make $165,000 in commissions by doing this - in my first year! That's where CityBlast was born.
													</p>
												</td>
											</tr>
											<tr>
												<td style="vertical-align: top;">
													<img src="<?=APP_URL;?>/images/email/clientfinder_feature_icons_6.jpg" alt="" height="30px" width="30px" style="margin-top: 2px;" border="0"/>
												</td>
												<td style="vertical-align: top; padding-left: 5px;">
													<h5 style="font-family:Myriad Pro, Arial; font-size: 12px;margin: 3px 0; ">
														Luke McKenzie, <span style="font-size: 10px;font-weight: normal;font-family: myriad-pro, Helvetica, Arial, Verdana, sans-serif">Royal LePage Signature Realty Inc.</span>
													</h5>
													<p style="color: #4D4D4D; margin: 0; font-size: 10px;">
														Our whole team is thrilled with the amount of exposure CityBlast gives us. We've really been impressed with their innovative sales tools. We Blast every listing!
													</p>
												</td>
											</tr>
										</table>
										
									</td>
									*/ ?>
									
								</tr>
							</table>
						</td>	
					</tr>
					<tr>
						<td style="padding-top: 15px; padding-bottom: 0px;">
							<table  cellspacing="0" cellpadding="0" style="width: 100%;">
								<tr>
									<td>
										<p style="margin: 0px; padding: 14px; border-top: solid 1px #C4C4C4; 
											border-bottom: solid 1px #C4C4C4; background-color: #F2F2F2; text-align: center; 
											font-size: 18px; font-weight: bold; font-family: Myriad Pro, Arial; ">
										Who has time to consistently update their Facebook?
										
										<br/><a href="<?=APP_URL;?>/member/join?aff_id=<? if(!empty($this->member)) echo $this->member->id;?>" style="color: #0000ff; text-decoration: none;"><?=COMPANY_NAME;?></a> has you covered.</p>	
									</td>
								</tr>
							</table>
						</td>	
					</tr>
					
					<?/*
					<tr>
						<td>
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td style="width: 270px; padding-right: 10px; padding-left: 10px;">
										<h4 style="font-family:Myriad Pro, Arial; font-size: 15px;margin: 3px 0; ">Wondering How The Updates Appear?</h4>
										<p style="color: #4D4D4D; margin: 0; font-size: 10px;">
											This is what an actual ClientFinder update looks like on your Facebook. Every single update is manually reviewed by CityBlast's real human editors for professional content, spelling and grammar.
										</p>
										<ul style="font-size: 10px;">
											<li>Clean</li>
											<li>Professional</li>
											<li>Current</li>
											<li>Trusted Sources</li>
										</ul>
									</td>
									<td style="vertical-align: top; text-align: center;">
										<img src="<?=APP_URL;?>/images/email/facebook_screenshot_thumb.jpg" alt="facebook_screenshot_thumb" height="86px" width="286px" border="0"/>
										<a href="<?=APP_URL;?>/member/how-clientfinder-works?aff_id=<? if(!empty($this->member)) echo $this->member->id;?>">Enlarge Screenshot</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					*/?>
					
				</table>
			</td>
		</tr>
	</table>

	<table cellspacing="0" cellpadding="0" width="600;" border="0" style="margin-top: 25px; margin-bottom: 25px;">
		<tr>
			<td width="490" style="text-align: right;font-size: 18px; font-weight: bold; font-family: Myriad Pro, Arial;">
				<?/*<em>Win new business with ClientFinder&nbsp;&nbsp;</em>*/?>
			</td>
			<td style="text-align: right; width: 100px; padding-top: 3px;">
<a href="<?=APP_URL;?>/member/settings" style='	
width: 260px;
margin: 10px 0 0;
float: right;
font-size: 17px;
margin: 10px 0;
height: 40px;
line-height: 40px;
padding: 0 15px;
background: url("<?=APP_URL;?>/images/button_bgs.png") repeat-x scroll 0 -15px transparent;
border: medium none !important;
border-radius: 5px 5px 5px 5px;
box-shadow: 0 1px 0 #FF6C6C inset, 0 -1px 0 #410000 inset;

cursor: pointer;

font-family: myriad-pro,Helvetica,Arial,Verdana,sans-serif;
font-size: 19px;
font-style: italic;
font-weight: 600;
letter-spacing: 0.02em;

outline: medium none !important;
position: relative;
text-align: center;
text-decoration: none !important;
text-shadow: -1px -1px 0 #A10000;
z-index: 3;
color: #FFFFFF;
outline: medium none;
text-decoration: none;' alt="Activate Your Account">Activate Your Account</a>
			</td>
		</tr>
	</table>
	
	<? include "email-footer-menu.html.php"; ?> 
	<? include "mortgagemingler-footer.html.php"; ?> 
<? include "email-footer.html.php"; ?>