<? include "email-header.html.php"; ?>

<body>

		<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px; "><em>We Were Unable to Process Your Payment.</em></h1>

		<p style="margin-top: 10px; margin-bottom: 10px;">Hello <?= $this->member->first_name;?>,</p>

		<p>Unfortunately there was an issue with the credit card we have on file for your account. We were unable to process you monthly <?=COMPANY_NAME;?> payment. In order to keep generating leads using the revolutionary
		<?=COMPANY_NAME;?> lead generation system, you'll need to update the credit card information we have on file for you. </p>


		<p style="margin-top: 10px; margin-bottom: 10px;">Updating your <?=COMPANY_NAME;?> credit card information is quick and easy.  Just click on the link below and follow the simple instructions:</p>


<a href="<?=APP_URL;?>/member/settings" style='	
width: 260px;
margin: 10px 0 0;
float: right;
font-size: 17px;
margin: 10px 0;
height: 40px;
line-height: 40px;
padding: 0 15px;
background: url("<?=APP_URL;?>/images/button_bgs.png") repeat-x scroll 0 -15px transparent;
border: medium none !important;
border-radius: 5px 5px 5px 5px;
box-shadow: 0 1px 0 #FF6C6C inset, 0 -1px 0 #410000 inset;

cursor: pointer;

font-family: myriad-pro,Helvetica,Arial,Verdana,sans-serif;
font-size: 19px;
font-style: italic;
font-weight: 600;
letter-spacing: 0.02em;

outline: medium none !important;
position: relative;
text-align: center;
text-decoration: none !important;
text-shadow: -1px -1px 0 #A10000;
z-index: 3;
color: #FFFFFF;
outline: medium none;
text-decoration: none;' alt="Update your Credit Card">Update Your Credit Card</a>
<br><br>



	<h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px; padding-top: 25px;"><em>Need more help?</em></h2>
	<p style="margin-top: 10px; margin-bottom: 10px;">If you're having problems receiving your Posts, feel free to drop us a line at: <a href="mailto:<?=HELP_EMAIL;?>" style="color: #3D6DCC;"><?=HELP_EMAIL;?></a>. We're 
		here to help.</p> 

	<? include "email-footer-menu.html.php"; ?> 
	<? include "mortgagemingler-footer.html.php"; ?> 
<? include "email-footer.html.php"; ?>
