
	<div class="contentBox">
		<div class="contentBoxTitle">
			<h1 style="font-size: 42px;">Hire a Certified Facebook Marketing Expert for Your Real Estate Business, For Less Than $2/Day</h1>
			
			<h4 style="margin-top: 30px;">Get the power of a certified real estate and social media expert, who'll manage your Facebook, Twitter and LinkedIn accounts every day, exactly how you choose (so you don't have to).</h4>
		</div>
		<div>
			<p>You should be out there selling homes.  Instead, you're stuck in the office, slumped over your computer desk, trying to find an article or helpful piece of information to post to your Facebook Page... again.</p>
			<p>Wouldn't it be great if someone else managed your social media accounts for you?  Maybe someone who is an expert in both real estate and social media?  And who is a great communicator, and can give your accounts that professional sizzle?</p>
			<p>That's what CityBlast's Social Experts do.  As often as you choose, your Social Expert will post exciting lead-generating content to your Facebook, Twitter and LinkedIn accounts, so you don't have to.  It's like having your own team of Social Media Experts, working on your behalf.  
				Except CityBlast's Social Experts work for you for less than $2/day.</p>
		</div>
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" />
	</div>
	
	
	<?/****
	<div class="contentBox">
		<div class="stepBox step1">
			<div class="stepTitle">
				<div class="titleNumber">
					<span>1</span>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Tell us what you want us to post.</h2>
			</div>
			<p>Your Scoial Media Expert has many options for content she can post for you.<br />
				<span class="bold">Choose at least 5</span> from below to get started.</p>
			<div class="stepContent clearfix">
				<div class="ladyCartoon pull-left">
					<img src="/images/new-images/step1-lady-cartoon.png" />
				</div>
				<? echo $this->render('member/content-select.html.php'); ?>
			</div>
		</div>
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" />
		<img src="/images/new-images/pulsating-arrow.png" class="pulsatingArrow" />
	</div>
	****/ ?>
	
	<div class="contentBox">
		<div class="contentBoxTitle">
			<h1 style="font-size: 42px;">But perhaps you're still wondering...</h1>
			<h4>Why should you use a CityBlast Expert?  Is this stuff even important?</h4>
		</div>
		<div class="row-fluid">
			
			<div class="key_features clearfix">
				
				<div class="span4 key_feature">
					
					<h3 class="image">Over 90% of Home Searches Begin Online.</h3>
					
					<p>Today, your prospects are beginning their real estate journey before choosing an agent.</p>
					<p>Be viewed as a trusted expert when your friends and followers come looking for professional advice, by staying consistently active online.</p>
								</div>
				
				<div class="span4 key_feature">
					
					<h3 class="commissions">People Spend 3X More Time on Facebook Versus Any Other Site.</h3>
					
					<p>If you're going to be in the right place at the right time, it pays to know where to be.</p>
					<p>Staying consistent on Facebook, Twitter and LinkedIn means you'll always appear active when your prospects are ready to move.</p>
								</div>
				
				<div class="span4 key_feature">
					
					<h3 class="maintenance">Experts Recommend at Minimum 1 Hour/Day for Online Marketing.</h3>
					
					<p>But who has an hour every day to spend on social media? You're already busy enough.</p>
					<p>CityBlast's Social Experts do the heavy lifting for you, by keeping you consistently fresh and up-to-date, freeing up your time to sell.</p>
								</div>
				
			</div>		
			
		</div>
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" />
	</div>
	
	<?/*
	<div class="contentBox">
		<div class="stepBox step2">
			<div class="opacLayer"></div>
			<div class="stepTitle">
				<div class="titleNumber">
					<span>2</span>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Tell us where you work and how often we should update your account.</h2>
			</div>
			<div class="stepContent clearfix">
				<div class="ladyCartoon pull-left">
					<img src="/images/new-images/step2-lady-cartoon.png" />
				</div>
				<div class="wantUsToPostWrapper">
					<div class="postNumberWrapper clearfix">
						<? echo $this->render('member/frequency-slider.html.php'); ?>
					</div>
					<div class="selectCity">
						<? echo $this->render('member/city-select-new.html.php'); ?>
					</div>
				</div>
			</div>
		</div>
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" />
		<img src="/images/new-images/pulsating-arrow.png" class="pulsatingArrow" />
	</div>
	*/ ?>
	
	
	<div class="contentBox">
		<div class="contentBoxTitle">
			<h1 style="font-size: 42px;">CityBlast's Social Media Experts let you get back out there and SELL!</h1>
		</div>
		<div>
			<p><strong>That's how CityBlast's founder, Shaun Nilsson, was able to make over $165,000 in commissions through posting on his Facebook alone - in his FIRST YEAR AS AN AGENT!</strong></p>
			<p>Facebook is where your real estate business really needs to shine in order for you to be successful in today's market... and CityBlast gives you the professional presence you need, with none of the daily hassle.</p>
			<p><strong>But how does it work?  It's pretty simple, actually.</strong></p>
			<p>When you first sign up (PS - we offer a 14-day totally risk-free trial) just tell your Expert a bit about how you want them to post.</p>
		</div>
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" />
	</div>


	<div class="contentBox">
		<div class="clearfix">
			<div class="pull-left tryDemoTxt">
				<h3 style="font-size: 28px;">Your now ready to conquer social media.</h3>
				<h4>Get started today with your Free 14-Day Trial of <?=COMPANY_NAME;?>.</h4>
			</div>
			<div class="pull-right">
				<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>
			</div>	
		</div>		
	</div>
	
	<? /****
	<div class="contentBox">
		<div class="stepBox step3">
			<div class="opacLayer"></div>
			<div class="stepTitle">
				<div class="titleNumber">
					<span>3</span>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Submit your preferences to your new Social Media Expert.</h2>
			</div>
			<div class="stepContent clearfix">
				<div class="noteContainer pull-left">
					<h6>What's This:</h6>
					<p>You'll be asked to connect your Facebook profile with CityBlast. That's so we know where to post the content and updates you have previously chosen in Step 1. It's as simple as  that!</p>
					<img src="/images/new-images/note-left-arrow.png" />
				</div>
				<div class="ladyCartoon pull-left">
					<img src="/images/new-images/step3-lady-cartoon.png" />
				</div>
				<div class="btnContainer pull-left">
					<h4>Connect with Facebook to continue...</h4>
					<div class="termsBox"><input type="checkbox" name="terms" id="terms" checked="checked"> I agree to the <a href="/index/terms" >Terms and Conditions</a>.</div>
					<a href="#" class="uiButton" id="facebook_signup_button" style="width: 300px;">Send.</a>
				</div>
			</div>
			
		</div>
	
	</div>
	****/ ?>
	
	

</form>





<? /* echo $this->render('member/upfront-settings.html.php'); */ ?>