<style type="text/css">
#ytapiplayer_wrapper{
	height: 410px;
}
.Landing #city_error{
	width: 380px;
}
#video_header{
	height: 100%;
	width: 100%;
	background: none;
}
#video_header > img{
	position: absolute;
	top: 0;
	left: 0;
	z-index: -1;
}
#video_header .span6{
	text-align: center;
}
#video_header .span4{
	text-align: right;
}
#video_header h1{
	font-size: 48px;
	color: #fff;
	margin: 40px 0 0 0;
}
#video_header h2{
	font-size: 36px;
	color: #fff;
	margin: 5px 0 33px 0;
}
#video_header p{
	color: #fff;
	font-size: 16px;
	margin: 10px 0 0 0;
}
#video_header .highlight{
	font-weight: bold;
	line-height: 20px;
	padding-bottom: 10px;
	padding-top: 13px;
	margin-bottom: 25px;
	margin-top: 0;
}
#video_header .logo{
	margin: 75px 35px 45px 0;
}
#video_header .name{
	margin-right: 25px;
	padding-top: 14px;
	padding-bottom: 15px; 
}
#video_header h3{
	font-size: 24px;
	color: #fff;
	line-height: 24px;
	font-weight: normal;
	margin: 0;
}
#video_header h6{
	font-size: 12px;
	color: #fff;
	font-weight: normal;
	margin: 0;
}

#select_your_city li{
	margin-bottom: 0;
}

#select_your_city .fieldname{
	font-size: 20px;
}
#select_your_city select{
	width: 528px;
}

/*** Responsive style for #video_header starts ***/

@media (max-width: 1199px) and (min-width: 980px){
	#ytapiplayer_wrapper{
		height: 329px;
	}
	.Landing #city_error{
		width: 300px;
	}
	#video_header .logo{
		margin: 42px 35px 45px 0;
	}
	#video_header .name{
		padding-top: 8px;
		padding-bottom: 8px; 
	}
	#video_header h1{
		font-size: 44px;
		margin-top: 25px;
	}
	#video_header h2{
		font-size: 32px;
		margin-bottom: 20px;
	}
	#video_header .highlight{
		padding-top: 9px;
		margin-bottom: 15px;
	}
	#select_your_city .fieldname{
		font-size: 18px;
	}
	#select_your_city select{
		width: 377px;
	}
	.row-fluid .box.form{
		background-position-x: -45px;
	}
}

@media (max-width: 979px) and (min-width: 768px){
	#ytapiplayer_wrapper{
		height: 254px;
	}
	.Landing #city_error{
		width: 218px;
		padding: 10px 8px 5px;
	}
	#video_header h1{
		font-size: 36px;
		margin-top: 18px;
	}
	#video_header h2{
		font-size: 24px;
		margin-bottom: 18px;
		line-height: 25px;
	}
	#video_header p{
		font-size: 11px;
		margin-top: 5px;
	}
	#video_header .highlight{
		line-height: 14px;
		padding-top: 8px;
		margin-bottom: 11px;
	}
	#video_header .logo{
		margin: 30px 35px 27px 0;
	}
	#video_header .name{
		padding-top: 5px;
		padding-bottom: 3px;
	}
	#video_header h3{
		font-size: 17px;
		line-height: 17px;
	}
	#video_header h6{
		font-size: 9px;
	}
	#select_your_city .fieldname{
		font-size: 13px;
	}
	#select_your_city select{
		width: 234px;
	}
	.row-fluid .box.form{
		background-position-x: -115px;
	}
}

/*** Responsive style for #video_header ends ***/
</style>

<?/*
<div class="row-fluid">
	<div class="span12">
		<ul class="inline giveUsCall pull-right">
			<li>
				<div class="bold">Start Today!</div>
				<div>Give Us a Call</div>
			</li>
			<li>
				<div class="bold">1-855-GO-MINGLE</div>
				<div>9am to 5pm EST</div>
			</li>
		</ul>
	</div>
</div>
*/ ?>

<div class="row-fluid" style="margin-top: 0px; position: relative; z-index: 1;">
	
	<div class="box form clearfix" style="padding: 0px;">

		<script type="text/javascript" src="/js/swfobject.js"></script>    
		
		<div id="ytapiplayer_wrapper" class="clearfix" style="position: relative;">
			<div id="ytapiplayer">
				You need Flash player 8+ and JavaScript enabled to view this video.
			</div>
			
			<div id="video_header" class="row-fluid">
				<img src="/images/video-shaun-nilsson-mm.jpg" class="videoImg" />
				<div class="span6" style="margin-left: 0;">
					<h1>Sell More Homes</h1>
					<h2>Through Facebook.</h2>
					<p class="highlight">Get the Most Powerful Facebook Tool <br/> Ever Created For Real Estate Agents.</p>
					<p>90% of homebuyers begin their search online.</p>
					<p>Buyers spend 3X more time on Facebook vs. Any other site.</p>
					<p>Experts recommend an hour/day marketing on Facebook.</p>
				</div>
				<div class="span4 offset2">
					
					<?/**
					<div class="logo">
						<img src="/images/logo-bw.png" />
					</div>

					<div class="name">
						<h3>Shaun Nilsson</h3>
						<h6>Co-Founder MortgageMingler</h6>
					</div>

					**/ ?>
					
				</div>
			</div>
			<div class="video_topleft"><img src="/images/lp-video-topleft.png" width="12" height="13" /></div>
			<div class="video_topright"><img src="/images/lp-video-topright.png" width="14" height="13" /></div>
		</div>
	
		<script type="text/javascript">
	
			var params = { allowScriptAccess: "always", allowFullScreen: true };
			var atts = { id: "myytplayer" };
			swfobject.embedSWF("https://www.youtube.com/v/OrrGZk2ogwU?enablejsapi=1&playerapiid=ytplayer&version=3", "ytapiplayer", "1000", "562", "8", null, null, params, atts);
	
		</script>
		
	
		<div class="clearfix" id="select_your_city">
		
	<!--		<ul>
				<li class="uiFormRow clearfix"> -->
					<ul class="row-fluid">
						<li class="fieldname span4" style="line-height: 60px; padding: 20px;">Choose Your City &amp; Press Play<span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field span8" style="line-height: 60px; padding: 20px; position: relative;">
							<span class="uiSelectWrapper" id="citywrapper" style="margin-top: 10px; position: relative; z-index: 2; padding: 10px 1px;">
								<span class="uiSelectInner" id="citywrapperinner">
									<select name="pubcity" id="defaultpubcity" class="uiSelect">
										<option value="">Please Select A City</option>
										<? 
										foreach($this->cities as $city)
										{
											//print_r($city);
											
											echo "<option value='".$city->id."' ";
											if($this->selectedCity == $city->id) echo " selected ";
											echo ">".$city->name;
											if(isset($city->state) && !empty($city->state)) echo ", " .$city->state;
											echo "</option>"; 	
										}
										?>
									</select>
								
									<?/*	
									<input type="text" name="pubcityselect" id="pubcityselect" class="uiSelect" style="width: 590px;" value="<?=$this->selectedCityName;?>" />
									<input type="hidden" name="pubcity" id="pubcity" value="<? if($this->selectedCity != "*") echo $this->selectedCity;?>" />
									*/ ?>
									
								</span>
							</span>
							<div id='city_error' class="clearfix" style='display: none; position: absolute; bottom: 4px; left: 20px; z-index: 1;'>
								<strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> select a city to play the video
							</div>

							<a href="javascript:void(0);" onclick="play()" id="yt-play-btn"></a>

						</li>
					</ul>
	<!--			</li>
			</ul> -->

		</div>
	
	
		<div class="clearfix" id="connect_to_cityblast" style="display: none; padding: 0 20px;">
	
			<div class="box_section" style="height: 120px; padding: 10px 20px;">
				<h1  style="line-height: 80px; font-size: 34px; float: left; margin: 0px; color: #333;">Connect to <?=COMPANY_NAME;?> Now and <span style="color: #000000;">Get 30 Days Free!</span></h1>
				<a href="#" id="facebook_signup_button" style="float: left; margin: 10px 0 10px 20px;"><img width="280" height="60" class="connect_fb_btn" src="/images/blank.gif"></a>
				
				<p><input type="checkbox" name="terms" id="terms" checked="checked"> I agree to the <a href="/index/terms" >Terms and Conditions</a>.</p>
			</div>
			
		</div>

	</div>

</div>


<div class="clearfix" style="margin-top: 20px; border-top: 1px solid #c3c3c3;">	
	<? echo $this->render('logos-responsive.html.php'); ?>
</div>



<div class="clearfix" style="border-top: 1px solid #c3c3c3;">&nbsp;</div>
	
<? echo $this->FacePile(); ?>

<style type="text/css">
.fb_iframe_widget,
.fb_iframe_widget span,
.fb_iframe_widget iframe[style]  {width: 100% !important;}
</style>

<div class="row-fluid" style="padding-top: 10px;">

	<div class="span12 clearfix" style="min-height: 161px; padding-left: 8px; background-color: #D7D7D7; border-top: 1px solid #757575;">		
		<h3 style="color: #808080; ">Members helping members.</h3>
		<fb:comments xid="member_join" href="<? echo APP_URL;?>/join" width='100%'></fb:comments>
	</div>

</div>





<script type="text/javascript">
	
	var ytplayer = null;
	
	function onYouTubePlayerReady(playerId) {
		ytplayer = document.getElementById("myytplayer");
		
		ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
		ytplayer.setPlaybackQuality('hd720');
		$(ytplayer).css('height', '0px');
	}
	
	function onytplayerStateChange(newState) {
		
		if(newState == 1)
		{
			$("#ytapiplayer_wrapper").animate({height:562},500);
			$('#video_header').hide();
		}
		if(newState == 0)
		{
			$("#ytapiplayer_wrapper").animate({height:350},500);
			$('#video_header').show();
			$(ytplayer).css('height', '0px');
		}
		
	}
	
	function play()
	{
		if($("#defaultpubcity").val() != ""){
			$('#city_error').hide();
			$('#connect_to_cityblast').show();
			$('#select_your_city').hide();
			$(ytplayer).css('height', '562px');
			ytplayer.playVideo();
		}
		else
		{
			$('#city_error').show();
		}
	}

	$(document).ready(function() {

        $('#facebook_signup_button').click(function(event){

            event.preventDefault();
            $('#city_error').hide();


	
            if( $("#defaultpubcity").val() == "" )
            {
                 $('#citywrapper').addClass('city_error');
                 $("#defaultpubcity").addClass('error');
                 $('#city_error').show();
            }
            else
            {
			if ($('#terms').is(':checked')) 
			{

				if(!$(this).hasClass('alreadyclicked')) {
					
					$(this).addClass('alreadyclicked');
		            	
		                FB.login(function(response) {
		
		                    if (response.authResponse)
		                    {
		          
		          			window.location.href="/member/clientfind-signup/" + $("#defaultpubcity").val() + "/?access_token="+response.authResponse.accessToken; 
		                    }
		
		                }, {scope: '<?php echo FB_SCOPE;?>'});
		          }
		     }
            }
           
        	});

		
		<? echo $this->render('fb-comments.html.php'); ?>
		
	});
</script>

<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/0184.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);

</script>