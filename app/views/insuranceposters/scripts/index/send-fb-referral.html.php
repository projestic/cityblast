<div class="container_12">
	<div class="grid_12">
		<div class="box" style="margin-bottom: 0; background-color: #EDEDED;">
			<div class="box_section" style="border: 0; padding-top: 40px;">
				<h1>Your referral "Blast" has been sent.</h1>
				
				<p>When your friend registers, you'll be credited with a FREE month to your account (a $49.99 value)!</p>  
				
				<p>Thanks for the support.</p>
				
				<div><a class="uiButton right" style="margin: 10px 0 0;" href="/member/settings#referagenttab">Refer more agents now!</a></div>
				<div class="clearfix"></div>
				
			</div>
		</div>
	</div>
</div>
