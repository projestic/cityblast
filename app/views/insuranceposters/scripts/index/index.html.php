<div class="row-fluid">
	
	<div class="row-fluid">
		<div class="bannerContainer">

			<a href="//www.viddler.com/embed/f74c47ef/?f=1&autoplay=1&player=full&secret=22300716&loop=false&nologo=false&hd=false" id="explain_video" class="cboxElement">
				<img src="/images/mortgagemingler-banner-bg.png" />
				<span class="bannerInfo">
					<span class="bannerTitle">Our Certified and Dedicated Social Experts Are Ready To Turn
					Your Fanpage Into An EXPLOSIVE Growth Engine!</span>
					<span class="playButton"><span>Play Video</span></span>
				</span>
				<span class="bannerBottom">
					<h3>Are You Losing <u>Valuable</u> <b>Mortgage</b> Commissions?</h3>
					<p>A <?=COMPANY_NAME;?> Social Media Expert can help, see how.</p>
				</span>
			</a>

		</div>
		
		<?/*<iframe class="videoPlayer" src="//www.youtube.com/embed/8T6V3PZi8G0?rel=0" frameborder="0" allowfullscreen></iframe>*/?>

		<?/*
		<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
			<iframe class="videoPlayer" src="//www.youtube.com/embed/Ysi4-i_gOJc?rel=0" frameborder="0" allowfullscreen></iframe>
		<? else: ?>
			<iframe class="videoPlayer" id="viddler-f74c47ef" src="//www.viddler.com/embed/f74c47ef/?f=1&autoplay=0&player=full&secret=22300716&loop=false&nologo=false&hd=false" frameborder="0" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
		<? endif; ?>
		*/?>

	</div>

	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

	</script>
	<a id="we-are-hiring-tab" href="/jobs/index" title="We are hiring"></a>
	<div class="whatIsCBHolder contentBox">
		<h2>What Is <?=COMPANY_NAME;?>?</h2>
		<p class="firstLine"><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is your team of personal <i>Social Experts</i> that keeps your <a href="//www.facebook.com">Facebook</a>, <a href="//www.twitter.com">Twitter</a> and <a href="//www.linkedin.com">LinkedIn</a> up to date and professional.</p>
		<div class="fbEmbedPost">
			<div class="fbPostBefore"><center>We do this on your <u>Facebook</u> fanpage</center></div>
			
			<?/*<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=761852687160013&amp;set=a.408573635884312.100242.153723091369369&amp;type=1" data-width="440"><div class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/photo.php?fbid=600436866697987&amp;set=a.408573635884312.100242.153723091369369&amp;type=1">Post</a> by <a href="https://www.facebook.com/CityBlastInfo">CityBlast</a>.</div></div>*/?>
	
			<?/*<div class="fb-post" data-href="https://www.facebook.com/CityBlastInfo/posts/10100649384666441?stream_ref=10" data-width="440"></div>*/?>

			<?=$this->fbEmbedPost(
				'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/t1.0-1/c9.9.110.110/s50x50/1010948_619922391353044_1662553126_s.png',
				'Mortgage Mingler',
				'Company',
				"Patience is a virtue. But wouldn't it be nice to be able make haste with the mortgage and loan process? The following are five tips to help you do just that. Need help securing a mortgage and the home of your dreams? Feel free to drop me a line!",
				'http://blog.credit.com/2013/12/make-a-mortgage-move-faster-72443',
				'https://scontent-a-fra.xx.fbcdn.net/hphotos-frc3/t1.0-9/s417x417/1779204_761852687160013_318333996_n.jpg',
				'February 18'
			);?>
			
			<div class="fbPostAfter">
				<p>So that you don’t have to!</p>
				<p class="last">Yes!<br>We can help <br>with <u>Twitter</u> and<br><u>LinkedIn</u> too.</p>
			</div>
		</div>
		<p class="secondLine"><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> enables you to spend time where it matters most, with clients!</p> 
		
		<p class="secondLine">Our team of <i>Social Experts</i> research and develop authentic, relevant content that prospective buyers and sellers want to read.  This 
			original content is then automatically posted directly to <a href="//www.facebook.com">Facebook</a>, <a href="//www.twitter.com">Twitter</a> and <a href="//www.linkedin.com">LinkedIn</a> accounts, freeing up your time to spend with your clients.</p>
		<p class="bold" style="font-size: 20px; text-decoration: underline; margin-top: 25px">Welcome to <?=COMPANY_NAME;?>.</p>

	</div>
	
	
	
	
	<div class="contentBox whoUsesCB">
		<h2>Who Uses <?=COMPANY_NAME;?>?</h2>
		<h3>Used And Trusted By Thousands of Agents Every Day!</h3>
		

		
		<? echo $this->FacePile(); ?>
		
		
		
		<?/*****
		<div class="testimonialsHolder clearfix">
			<h3>And Here Is What They Have To Say:</h3>
			<div class="colLeft">
				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/ingrid-brunsch.jpeg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>Thanks so much! I must tell you I really to appreciate the articles that appear on my Facebook and Twitter page 
							daily - saves me so much time not having to hunt around trying to find appropriate information to post. I have also taken advantage 
							of blasting my listings and am not sure if it is working in terms of selling them any faster - but, it makes a wonderful tool 
							when I speak to my clients about my marketing tools - I appear to be very tech savvy - if they only knew! <i>~Ingrid Brunsch</i></p>
					</div>
				</div>


				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/leila-talibova.jpg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>My friends and previous colleagues noticed my Facebook updates right away. In only my first week using CityBlast, I received a call from a family 
							friend who said she’d seen my Facebook and wanted to list their home with me. The experts are awesome! <i>~Leila Talibova</i></p>
					</div>
				</div>

				

				

				
				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/sunny-dehghan.jpg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>I have a lot of realtor friends and my biggest concern was that I would be getting the exact same posts as everyone else. Well, I've been a member for over 8 months and so far so good! To be honest, it does
							everything as advertised and my clients think all I do is stay on top of industry news. Even my parents said something about my Facebook posts! <i>~Sunny Dehghan</i></p>
					</div>
				</div>
				



			</div>
			<div class="colRight">
				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/sheree-cerqua.jpg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>I use CityBlast to help me market. Not only are they the heart of my online platform, but also help me to sell my clients’ listings quickly, and to achieve 
							excellent prices. CityBlast is an incredible service for both beginners and top agents, and I highly recommend them. <i>~Sheree Cerqua</i></p>
					</div>
				</div>


				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/ashley-gollogly.jpg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>I'm pretty head strong when it comes to social media. I knew what I wanted to post on my wall, the problem was finding the time to do it every day. Thankfully I found CityBlast. 
							Now I can focus on the things that require my personal attention and I know that my Social Media marketing is being taken care of by dedicated professionals. P.S. I went 
							on vacation in January and my Social Expert was posting even when I was away. I came back to find 2 new leads in my Facebook inbox. Shhhh! Don't tell! <i>~Ashley Gollogly</i></p>
					</div>
				</div>


				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/valentina-pasquini.jpg" alt=""></span>
					</div>
					<div class="textHolder">

						<p>I have a young demanding family and fledgling business real estate business. Between diaper changes and open houses, quite frankly, I just couldn't find the time to add yet another thing to my to-do list. 
							Even though I knew that Social Media was the most talked about thing as Re/Max Kickstart, I just couldn't find the time to do it myself. CityBlast completely turned around my social media presence and now I have one less thing
							to worry about. <i>~Valentina Pasquini</i></p>

					</div>
				</div>



			</div>
		</div>
		***/ ?>
		
		<hr style="margin-top: 30px;" />
		
		<h3 style="margin-top: 20px;">Some of Our Partners</h3>
		<ul class="partnersList">
			<li><a target="_blank" href="http://www.royallepage.ca/"><img src="/images/lp_logos/royal_lepage.png" width="250"></a></li>
			<li><a target="_blank" href="http://blog.lwolf.com/partners/news/lone-wolf-invests-in-cityblast-81759/"><img src="/images/lp_logos/lone_wolf.png" width="250"></a></li>
			<li><a target="_blank" href="http://retechulous.com/"><img src="/images/lp_logos/retechulous.png" width="250"></a></li>
			
			<li><a target="_blank" href="http://www.seevirtual360.com/"><img src="/images/lp_logos/see_virtual.png" width="250"></a></li>
			
		</ul>
		
		<h3 style="margin-top: 20px;">As Featured On</h3>
		<ul class="partnersList" style="margin-top: 10px;">	
			<li><a target="_blank" href="http://www.techvibes.com/blog/cityblast-enables-canadian-real-estate-agents-to-build-their-business-through-social-media-2012-08-03"><img src="/images/lp_logos/techvibes.png" width="250"></a></li>
			<li><a target="_blank" href="http://www.canadianrealestatemagazine.ca"><img src="/images/lp_logos/real_estate_wealth.png"></a></li>
			<li><a target="_blank" href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/"><img src="/images/lp_logos/globe_and_mail.png" width="250"></a></li>
			
			<li><a target="_blank" href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/"><img src="/images/lp_logos/real_estate_talk_show.png" width="250"></a></li>
			
		</ul>
		
		
		
	</div> <!-- end whoUsesCB -->
	<div class="contentBox socialMedia">
		<h2>Real Estate and Social Media: A Perfect Match</h2>
		<div class="icons-holder clearfix">
			<div>
				<img src="/images/icon-1.png" alt="">
				<p>Percentage of home searches that start online</p>
			</div>
			<div>
				<img src="/images/icon-2.png" alt="">
				<p>Amount of time spent on Facebook vs any other site</p>
			</div>
			<div>
				<img src="/images/icon-3.png" alt="">
				<p>Percentage of Agent referrals that come from Social Media</p>
			</div>
		</div>
		<p class="description">
			Your clients are researching their next home purchase online and they're <i>spending more time on Facebook than anywhere else</i>. <u>Social Media marketing is the new word-of-mouth</u>. If you're not cultivating your brand on social media effectively, you're missing out on both new leads and referrals from other agents. Don't get left behind.
		</p>
		<a href="/member/index" class="uiButtonNew home"><?=COMPANY_NAME;?> and Me: A Perfect Match</a>
	</div>
	<div class="contentBox contentType">
		<h2>What Type of Content Do You Post?</h2>
		<p class="description">At <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> we only post the most powerful type of marketing content called Inbound Marketing. <a href="">Inbound marketing</a>, also known as <a href="">pull marketing</a> or <a href="">permission marketing</a>, is the use of interesting content to “earn your way” into a sales lead as opposed to push marketing where you “buy, beg or bug” your way into a sales lead.</p>
		<div class="fbPostsHolder">
			
			
			<?php /*<h3>What Does Inbound Marketing Look Like?</h3> */?>
			
			
			<div class="fbPostsAfterText arrowsAbove" style="margin-top: 30px; margin-bottom: 25px;">
				<span class="fb-like" data-href="https://www.facebook.com/MortgageMingler" data-layout="button" data-action="like" data-show-faces="true" data-share="false" style="position: absolute; display: inline-block; line-height: 0.7em; margin: 10px 0 0 50px"></span>
				Click <span style="display: inline-block; width:50px"></span> if you want your Fanpage to look like this!</div>
			
			<img src="/images/fanpage-sample-banner-mm.png" alt="Your New Fanpage!" style="margin-left: -7px;" />
			
			
			<?/*<div class="fbPosts clearfix">
				<div class="fbEmbedPost">
					<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=761854687159813&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
				</div>
							
				<div class="fbEmbedPost">
					<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=761853770493238&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
				</div>

				<div class="clear"></div>

				<div class="fbEmbedPost">
					<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=712937428718206&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
				</div>	
				
				<div class="fbEmbedPost">
					<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=761855663826382&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
				</div>
			</div>*/?>

			<div class="fbPosts clearfix">

				<?=$this->fbEmbedPost(
					'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/t1.0-1/c9.9.110.110/s50x50/1010948_619922391353044_1662553126_s.png',
					'Mortgage Mingler',
					'Company',
					"How important is the \"walkability\" score for you? A recent survey by Inman suggests that most of us most of us would prefer to live in a neighborhood with a mix of houses, stores, and other businesses that are within walking distance. What do you think?",
					'http://www.realtor.org/articles/nar-2013-community-preference-survey',
					'https://scontent-b-fra.xx.fbcdn.net/hphotos-prn1/t1.0-9/p370x247/1601116_761854687159813_129944578_n.png',
					'February 18'
				);?>

				<?=$this->fbEmbedPost(
					'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/t1.0-1/c9.9.110.110/s50x50/1010948_619922391353044_1662553126_s.png',
					'Mortgage Mingler',
					'Company',
					"People commonly think that the absolute best time to sell your home is in the spring and that the worst time is in the fall. Times have changed drastically. If you're not sure what the best time to sell your home is, check out what the experts at HGTV have to say to get the inside scoop!",
					'http://www.hgtv.ca/realestate/article/the-best-season-to-sell-your-home',
					'https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-ash3/t1.0-9/p370x247/1924790_761853770493238_756378280_n.jpg',
					'February 18'
				);?>

				<div class="clear"></div>

				<?=$this->fbEmbedPost(
					'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/t1.0-1/c9.9.110.110/s50x50/1010948_619922391353044_1662553126_s.png',
					'Mortgage Mingler',
					'Company',
					"8 Reasons to Love Herringbone<br><br>These 8 homes from Paris to Tokyo will show you how herringbone is a sleek and modern pattern:",
					'http://www.mortgagemingler.com/n/i/fb/900/16',
					'https://scontent-a-fra.xx.fbcdn.net/hphotos-ash3/l/t1.0-9/s526x395/1394430_712937428718206_1131541464_n.jpg',
					'November 26, 2013'
				);?>

				<?=$this->fbEmbedPost(
					'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/t1.0-1/c9.9.110.110/s50x50/1010948_619922391353044_1662553126_s.png',
					'Mortgage Mingler',
					'Company',
					"What are you doing to prepare to sell your home? Following these 10 tips can help you wow buyers:",
					'http://www.hgtv.com/decorating-basics/realtors-top-10-tips-for-wowing-buyers/pictures/index.html',
					'https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-ash3/t1.0-9/p370x247/1891059_761855663826382_1541275082_n.jpg',
					'February 18'
				);?>

			</div>


			<p class="fbPostsAfterText arrowsAside">Do you want your Facebook, Twitter and LinkedIn to look like this?</p>


			<a href="#" class="uiButtonNew small" id="showconentlib">View Full Content Library</a>
			
			
			<div id="contentlib_div" style="display: none;">
				<div class="fbPosts clearfix">
					<div class="clearfix">

						<?=$this->fbEmbedPost(
							'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/t1.0-1/c9.9.110.110/s50x50/1010948_619922391353044_1662553126_s.png',
							'Mortgage Mingler',
							'Company',
							"31 Insanely Clever Remodeling Ideas For Your New Home. I dare you to get through this post without calling your contractor! P.S. Comment below to let me know which one you think is the coolest/cleverest...",
							'http://www.buzzfeed.com/peggy/insanely-clever-remodeling-ideas-for-your-new-home',
							'https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-xaf1/t1.0-9/s526x296/1004715_761859450492670_2012666743_n.jpg',
							'February 17'
						);?>

						<?=$this->fbEmbedPost(
							'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/t1.0-1/c9.9.110.110/s50x50/1010948_619922391353044_1662553126_s.png',
							'Mortgage Mingler',
							'Company',
							"Quiz of The Day: Agents, even great listings can benefit from skillful property marketing. Take this quiz to see if you know what it takes to grab the attention of prospective buyers.",
							'http://www.mortgagemingler.com/n/i/fp/625/16',
							'https://scontent-a.xx.fbcdn.net/hphotos-xfa1/t1.0-9/s403x403/1959744_761858293826119_536823806_n.jpg',
							'February 17'
						);?>

					</div>
					<div class="clearfix">

						<?=$this->fbEmbedPost(
							'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/t1.0-1/c9.9.110.110/s50x50/1010948_619922391353044_1662553126_s.png',
							'Mortgage Mingler',
							'Company',
							"Do you ever wonder how the stars live? Take a peek into Cameron Diaz’s new NYC apartment...",
							'http://curbed.com/archives/2013/12/11/inside-cameron-diazs-new-9m-manhattan-apartment.php',
							'https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xaf1/t1.0-9/s403x403/1798629_761858943826054_1786470181_n.jpg',
							'February 17'
						);?>

						<?=$this->fbEmbedPost(
							'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/t1.0-1/c9.9.110.110/s50x50/1010948_619922391353044_1662553126_s.png',
							'Mortgage Mingler',
							'Company',
							"Are you starting a home renovation this year? Your smart phone can help with that too! Check out 6 super smart phone Apps that will help you experiment before you sink your hard earned dollars into your renovation project!",
							'http://mashable.com/2013/06/03/home-design-apps',
							'https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xap1/t1.0-9/s403x403/1623786_761856737159608_114933423_n.jpg',
							'February 17'
						);?>

						<?/*<div class="fbEmbedPost">
							<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=761858943826054&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
						</div>
						<div class="fbEmbedPost">
							<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=761856737159608&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
						</div>*/?>
					</div>
				</div>		
			</div>
			
		</div>
		<h3>How Often Is It Posted?</h3>
		<ul class="frequencyList">
			<li>
				<div>
					<p class="day">Sunday</p>
					<p class="number">1</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Monday</p>
					<p class="number">2</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Tuesday</p>
					<p class="number">3</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Wednesday</p>
					<p class="number">4</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Thursday</p>
					<p class="number">5</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Friday</p>
					<p class="number">6</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Saturday</p>
					<p class="number">7</p>
				</div>
			</li>
		</ul>
		<p>You choose the frequency and the days you want us to post and we do the rest. It’s that easy!</p>
	</div> <!-- end contentType -->
	<div class="contentBox freeTrial">
		<h2>How Do I Start My 14 Day Free Trial?</h2>
		<p>Our simple 3 step online signup process will have you looking professional <i>in less than 60 seconds</i>. You’ll never be embarrassed by your social media presence again AND you’ll never have to spend another minute searching for something to post on your Fanpage. All you have to do is:</p>
		<div class="icons-holder clearfix">
			<div class="item-1">
				<img src="/images/icon-4.png" alt="">
				<p class="step">Step 1</p>
				<p>Tell us where you work</p>
			</div>
			<div class="item-2">
				<img src="/images/icon-5.png" alt="">
				<p class="step">Step 2</p>
				<p>Tell us how often to post</p>
			</div>
			<div class="item-3">
				<img src="/images/icon-6.png" alt="">
				<p class="step">Step 3</p>
				<p>Tell us what you want posted</p>
			</div>
			<div class="clear"></div>
			<p class="description">That’s it. It’s literally that easy. Turn your social media marketing headache into something you can be proud of. Your friends and family will literally be complimenting you on your brand new online image.</p>
		</div>
		<a href="/member/index" class="uiButtonNew home">Start Your 14 Day Free Trial Now!</a>
	</div>
	<div class="contentBox questions">
		
		<div style="margin-bottom: 30px; border-bottom: 1px solid #CCCCCC; padding-bottom: 27px;">
		<h2>Do You Have More Questions?</h2>
			<p>Our <i>Social Experts</i> are on call. Feel free to call our <a href="tel:+1-888-712-7888">800 number</a>, <a href="#" class="chatbutton">Live Chat</a>
				 with us or drop us <a href="mailto: info@cityblast.com">an Email</a>. Our real estate marketing team is here to make your life easier.</p>
			<div class="teamHolder">
				<img src="/images/team.png" alt="">
			</div>
			<p class="arrowsAside teamAfterText"><?=COMPANY_NAME;?>’s <u>Real</u> <i>Social Experts</i> are always standing by ready to help you.</p>
		</div>
		
		<a href="#" class="uiButtonNew home chatbutton">Chat With An Expert</a>
		&nbsp;&nbsp;&nbsp;- OR -&nbsp;&nbsp;&nbsp;
		<a href="mailto: info@cityblast.com" class="uiButtonNew home">Send Us An Email</a>
		&nbsp;&nbsp;&nbsp;- OR -&nbsp;&nbsp;&nbsp;
		
		<!--href="tel:18887127888"-->
		<a href="tel:+1-888-712-7888" class="uiButtonNew home">1-888-712-7888</a>
		
	</div>


	<div class="contentBox">

		<h2 style="margin-bottom: 35px; font-weight: normal; text-align:left">Get a <i>Social Expert</i> managing your social media for less than $2/day, and never miss a deal again</h2>

		<div class="strategyContainer" style="margin-top: 10px;">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-2.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">We'll plan and manage your lead generation strategy.</h3>
					Simply tell us the city where you work, and what kind of posts suit your style best.  Our <i>Social Experts</i> <u>do the rest</u> - planning your marketing, sourcing articles and videos, and posting them to your accounts.
				</div>
			</div>
		</div>

		<?/*
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-2.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">Your Social Media Expert plans your campaign.</h3>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac sem vel eros dapibus bibendum sed non ipsum. Sed eu lorem lectus.
				</div>
			</div>
		</div>*/ ?>

		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-3.png" />
				</span>
				<div class="media-body">
					<?/*<h3 class="media-heading">As often as you choose, your Social Expert consistently posts articles, videos & information.</h3>*/?>
					<h3 class="media-heading">As often as you choose, we post articles, videos & information.</h3>
					You also control how often your <i>Social Expert</i> will post.  Following your instructions, they'll <i>consistently</i> post updates to your Facebook, Twitter and/or LinkedIn accounts that make sure you look professional and up-to-date.
				</div>
			</div>
		</div>
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-4.png" />
				</span>
				<div class="media-body">
					<?/*<h3 class="media-heading">You respond to new leads, and follow your success in your dashboard.</h3>*/?>
					<h3 class="media-heading">Manage your leads and measure success in your dashboard.</h3>
					With your powerful, professional presence, you'll see <i>an increase in new leads</i> through your social media accounts.  You just follow up with new leads as they come in, and generate more new business.
				</div>
			</div>
		</div>
		<div class="clearfix">
			<div class="pull-left tryDemoTxt">
				<?/*<h3 style="font-size: 28px;">Get an Expert handling your business's social media.</h3>*/?>
				<h3 style="font-size: 28px;">Our <i>Social Experts</i> can help.</h3>
				<h4>Start your Free 14-Day Trial of <?=COMPANY_NAME;?>  <i>Social Experts</i> now.</h4>
			</div>
			<div class="pull-right">
				<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>
			</div>
		</div>
	</div>


</div>

<div style="display:none">
	<div id="gotcha_popup_div">
		<h1>Made You Click!</h1>
		<p style="margin-top:20px">You've just seen the power of inbound marketing for yourself!</p>
	</div>
</div>

<? echo $this->render('logos-responsive-small.html.php');?>
<div style="margin-bottom: 25px;">&nbsp;</div>

<script type="text/javascript">
	$(document).ready(function() 
	{
		
		$("#showconentlib").click(function(e)
		{
			e.preventDefault();

			$("#contentlib_div").slideToggle( "slow", function() {
				// Animation complete.
			});	
		})		

		$(".chatbutton").click(function(e)
		{
			e.preventDefault();	
			$zopim.livechat.window.show(); 
		})

		$(".gotcha_popup").click(function(e)
		{
			e.preventDefault();
			$.colorbox({
				height: "200px",
				inline: true, 
				href: "#gotcha_popup_div"
			});
		});					
		
		
		$("#videopopup").click(function(e)
		{
			e.preventDefault();
			$.colorbox({height: "500px",inline:true, href:"#sample_popup",onClosed:function(){ $('#sample_popup').empty(); }});
		});
	});
</script>