<? include "email-header.html.php"; ?>

	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px; "><em>Your Invoice.</em></h1>

	<p style="margin-top: 10px; margin-bottom: 10px;">Hello <?= $this->member->first_name;?>,</p>
			
	<p><a href="<?=APP_URL;?>" style="color: #3D6DCC;"><?=COMPANY_NAME;?></a> experts are hard at work keeping your social media accounts consistent and up-to-date!  Please find below your lastest invoice for your records, and if you have the chance, 
		now is a great time to log in to <a href="<?=APP_URL;?>" style="color: #3D6DCC;"><?=COMPANY_WEBSITE;?></a> and check on your latest stats and personal settings and make sure that you're maximizing your results.</p>
		
	<h1 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 0px; padding-top: 25px;">Invoice</h1>

	<table width="600" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="24"><h3>Invoice Id: #<?=number_format(sprintf("%03d",$this->invoice_number), 0, '.', '-');?></h3></td>

			<td height="24" style="text-align: right;"><strong>Invoice Date: </strong><? echo date("M d, Y"); ?></td>
		</tr>
		<tr>
			<td width="50%" valign="top" style="padding-top: 15px; padding-bottom: 15px; border-top: solid 1px #d9d9d9; border-bottom: solid 1px #d9d9d9; background-color: #f7f7f7;">
				<strong><?=$this->member->first_name;?> <?=$this->member->last_name;?></strong><br />
				<? if(isset($this->member->address) && !empty($this->member->address)  && $this->member->address != "empty" && $this->member->address != "undefined") echo $this->member->address;?>
				<? if(isset($this->member->address2) && !empty($this->member->address2)  && $this->member->address2 != "empty" && $this->member->address2 != "undefined") echo $this->member->address2;?><br />
				<? if(isset($this->member->city) && !empty($this->member->city)  && $this->member->city != "empty" && $this->member->city != "undefined") echo $this->member->city . ",";?>
				<? if(isset($this->member->state) && !empty($this->member->state)  && $this->member->state != "empty" && $this->member->state != "undefined") echo $this->member->state;?>
				<? if(isset($this->member->zip) && !empty($this->member->zip)  && $this->member->zip != "empty" && $this->member->zip != "undefined") echo $this->member->zip;?><br />
				<? if(isset($this->member->country) && !empty($this->member->country) && $this->member->country != "empty" && $this->member->country != "undefined") echo $this->member->country;?>
			</td>
			<td width="50%" valign="top" style="padding-top: 15px; padding-bottom: 15px; border-top: solid 1px #d9d9d9; border-bottom: solid 1px #d9d9d9; background-color: #f7f7f7;">
				<strong><?=COMPANY_NAME;?></strong><br />
				<?=COMPANY_ADDRESS;?><br />
				<?=COMPANY_CITY;?>, <?=COMPANY_PROVINCE;?><br />
				<?=COMPANY_POSTAL;?><br />
				<?=COMPANY_COUNTRY;?>
			</td>
		</tr>
		<tr>
			<td height="30"></td>
			<td></td>
		</tr>
	</table>


			

<h3 style="font-family:'Myriad Pro', Arial; font-size: 18px; padding-top: 25px; border-bottom: 1px solid #888888">Invoicing Details</h3>



<table width="580" border="0" cellspacing="0" cellpadding="0" class="datatable">
	<tr>
		<th width="240" align="left">Purchase Item</th>
		<th width="100">Quantity</th>
		<th width="100" align="right">Unit Price</th>
		<th width="100" align="right">Item Total</th>
	</tr>
	<tr>
		<td  style="padding-top: 5px;"><?=COMPANY_NAME;?> - Facebook + Twitter + LinkedIn</td>
		<td align="center"  style="padding-top: 5px;">1</td>
		<td align="right"  style="padding-top: 5px;">

				$<?=number_format($this->amount, 2);?>
		</td>
		<td align="right"  style="padding-top: 5px;">
				$<?=number_format($this->amount, 2);?>
		</td>
	</tr>
	<tr>
		<td style="padding-top: 10px;"></td>
		<td style="padding-top: 10px;"></td>
		<td align="right"style="padding-top: 10px;"><strong>Subtotal</strong></td>
		<td align="right"style="padding-top: 10px;">
				$<?=number_format($this->amount, 2);?>
		</td>
	</tr>
	<tr>
		<td>HST <?=HST;?></td>
		<td></td>
		<td align="right"><strong>Tax (<?php echo ($this->taxes*100); ?>%)</strong></td>
		<td align="right">
			<? $tax_amount = $this->amount * $this->taxes; ?>
			$<?=number_format( $tax_amount , 2);?>
		</td>
	</tr>
	<tr>
		<td style="padding-top: 10px;"></td>
		<td style="padding-top: 10px;"></td>
		<td align="right"style="padding-top: 10px;"><strong>Total</strong></td>
		<td align="right"style="padding-top: 10px;">
			<strong>$<?=number_format( $this->amount + $tax_amount , 2);?></strong>
		</td>
	</tr>
</table>




	<h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px; padding-top: 25px;"><em>Need more help?</em></h2>
	<p style="margin-top: 10px; margin-bottom: 10px;">If you are having problems receiving your Posts, feel free to drop us a line at: <a href="mailto:<?=HELP_EMAIL;?>" style="color: #3D6DCC;"><?=HELP_EMAIL;?></a>. We're here to help.</p> 

	<? include "email-footer-menu.html.php"; ?> 
	<? include "mortgagemingler-footer.html.php"; ?> 
<? include "email-footer.html.php"; ?>