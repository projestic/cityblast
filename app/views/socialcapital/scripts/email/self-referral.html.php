<? include "email-header.html.php"; ?>

<body>
	
	<p style="margin-top: 10px; margin-bottom: 10px;">Dear <?=$this->member->first_name;?>,</p>
	
	<p>Thanks for using our referral tool.</p>
	
	<p>Although we definitely encourage you to refer friends to <?=COMPANY_NAME;?>, it seems recently you have accidentally referred yourself!<br />
	We’re just sending you this friendly reminder that only referrals of other agents will result in a free month.</p>
	
	<p>Thanks again; and happy referring!</p>
	
	<? include "email-footer-menu.html.php"; ?> 
	<? include "mortgagemingler-footer.html.php"; ?> 
<? include "email-footer.html.php"; ?>
