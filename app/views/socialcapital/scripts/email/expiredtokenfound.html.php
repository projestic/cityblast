<? include "email-header.html.php"; ?>
	
	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Your Facebook access token is about to expire.</em></h1>
	
	<p style="margin-top: 10px; margin-bottom: 10px;">Hello <?= $this->member->first_name;?>,</p>

<p style="margin-top: 10px; margin-bottom: 10px;">
Thanks for using <?=COMPANY_NAME;?>!  This is just a quick note to remind you to <b>log in to <a href="<?=APP_URL;?>/member/settings"><?=COMPANY_NAME;?></a> every 60 days</b> or less to keep your account active and up-to-date.  
As Facebook has evolved, we have included this feature in accordance with their philosophy, and think it's a great tool in reminding you to keep your <a href="<?=APP_URL;?>/member/settings"><?=COMPANY_NAME;?></a> Settings current.
</p>
	
<p style="margin-top: 10px; margin-bottom: 10px;">
<b><i>You're receiving this email because you need to log in TODAY to keep your current settings and your <?=COMPANY_NAME;?> publishing active.</i></b>
</p>


<p style="margin-top: 10px; margin-bottom: 10px;">
It couldn't be easier:  To do so, simply navigate to <a href="<?=APP_URL;?>"><?=COMPANY_WEBSITE;?></a> and login in the top right corner with your usual Facebook info. That's it!
</p>

<p style="margin-top: 10px; margin-bottom: 10px;">
While you're there, we encourage you to check on your <a href="<?=APP_URL;?>/member/settings">Settings</a>, and to look over the many new features of the site. 
</p>

<p style="margin-top: 10px; margin-bottom: 10px;">
 Or click <a href="<?=APP_URL;?>">HERE</a> to login now and instantly renew for the next 60 days without changing your Settings.
</p>





<a href="<?=APP_URL;?>/member/settings" style='	
width: 260px;
margin: 10px 0 0;
float: right;
font-size: 17px;
margin: 10px 0;
height: 40px;
line-height: 40px;
padding: 0 15px;
background: url("<?=APP_URL;?>/images/button_bgs.png") repeat-x scroll 0 -15px transparent;
border: medium none !important;
border-radius: 5px 5px 5px 5px;
box-shadow: 0 1px 0 #FF6C6C inset, 0 -1px 0 #410000 inset;

cursor: pointer;

font-family: myriad-pro,Helvetica,Arial,Verdana,sans-serif;
font-size: 19px;
font-style: italic;
font-weight: 600;
letter-spacing: 0.02em;

outline: medium none !important;
position: relative;
text-align: center;
text-decoration: none !important;
text-shadow: -1px -1px 0 #A10000;
z-index: 3;
color: #FFFFFF;
outline: medium none;
text-decoration: none;' alt="Renew Your Access">Renew Your Access</a>
<br><br>
	
	
	<? /*include "member-settings.html.php";*/ ?>


	<h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px; padding-top: 15px;"><em>Need more help?</em></h2>
	<p style="margin-top: 10px; margin-bottom: 10px;">If you're still having problems receiving your Posts, feel free to drop us a line at: <a href="mailto:<?=HELP_EMAIL;?>" style="color: #3D6DCC;"><?=HELP_EMAIL;?></a>. We're here to help.</p> 


	
	<? include "email-footer-menu.html.php"; ?> 
	<? include "mortgagemingler-footer.html.php"; ?> 
<? include "email-footer.html.php"; ?>