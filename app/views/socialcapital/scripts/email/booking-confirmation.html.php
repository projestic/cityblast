<? include "email-header.html.php"; ?>

    	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Thanks, <?php echo $this->name;?>.</em></h1>
	
	<p>Your request has been sent!  You should be hearing back from your friend <strong><?php echo $this->booking_agent->first_name;?> <?php echo $this->booking_agent->last_name;?></strong> regarding your property of interest shortly.</p>
	
	
	<? include "email-signature.html.php"; ?> 

	<? include "email-footer-menu.html.php"; ?> 

	<? include "franchise-footer.html.php"; ?> 
	
<? include "email-footer.html.php"; ?> 
