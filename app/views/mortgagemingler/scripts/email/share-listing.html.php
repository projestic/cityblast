<? include "email-header.html.php"; ?>

	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Your friend recommended a property.</em></h1>
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 12px; width: 600px;">Your friend thought you might like the following property listing.  Are you looking for 
	a property?  Click on the link below and search thousands of properties on <?=COMPANY_NAME;?> now.</p> 

	<? if(isset($this->comments) && !empty($this->comments)) : ?>
	
		<h3 style="font-family:'Myriad Pro', Arial; font-size: 18px;"><em>Comments:</em></h3>
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 12px; width: 600px;"><?=$this->comments;?></p>
		
	<? endif; ?>

	<? include "show-listing.html.php"; ?>

	<? include "email-signature.html.php"; ?> 	

	<? include "email-footer-menu.html.php"; ?> 

	<? include "franchise-footer.html.php"; ?> 
    
<? include "email-footer.html.php"; ?>