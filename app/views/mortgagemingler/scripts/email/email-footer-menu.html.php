<table border="0" width="600" cellspacing="0" cellpadding="0" style="background-color: #828282; font-size: 13px;">
    <tr>
        <td style="font-size: 11px;padding-left: 30px; padding-top: 6px; padding-bottom: 6px; padding-right: 25px;">
			<a href="<?=APP_URL;?>/index/refer-an-agent" style="color: #E5E5E5;">Refer an Agent</a>
		</td>
		<td style="font-size: 11px; padding-top: 6px; padding-bottom: 6px; padding-left: 10px; padding-right: 35px;">
			<a href="<?=APP_URL;?>/index/privacy" style="color: #E5E5E5; margin-left: 10px;">Privacy Policy</a>
		</td>
		<td style="font-size: 11px; padding-top: 6px; padding-bottom: 6px; padding-right: 50px;">
			<a href="<?=APP_URL;?>/index/terms" style="color: #E5E5E5">Terms of Use</a>
		</td>
		<td style="font-size: 11px; padding-top: 6px; padding-bottom: 6px; padding-right: 50px;">
			<a href="<?=APP_URL;?>/index/about" style="color: #E5E5E5;">About <?=COMPANY_NAME;?></a>
		</td>
		<td style="font-size: 11px; padding-top: 6px; padding-bottom: 6px; padding-right: 45px;">
			<a href="<?=APP_URL;?>/index/contact-us" style="color: #E5E5E5;">Contact Us</a>
		</td>
    </tr>
</table>
<br/>