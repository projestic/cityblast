<? include "email-header.html.php"; ?>

	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Success.</em></h1>

	<p>Congratulations!  And thank you for using <?=COMPANY_WEBSITE;?> to sell your listing <u>fast</u>.</p>
	
	<p>Your property Blast has been advertised to tens of thousands of potential local buyers on Facebook, Twitter and LinkedIn, and looked like this:</p>


	<? include "blast-preview.html.php"; ?>

	
	<?php
	/**********************
	<br />
	<a href="<?php echo $this->listing_url;?>">Check your blast reach details</a>
	**********************/
	?>
	
	<h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px; padding-top: 15px;"><em>Thank you.</em></h2>
	<p>Thank you for your Blast, and for using <?=COMPANY_NAME;?>'s professional-strength social media tools to help power your real estate business.  <?=COMPANY_NAME;?> 
	contributes to the speedy sale of every listing.  We look forward to your next successful Blast.</p>

	<? include "email-signature.html.php"; ?> 
	
	<? include "email-footer-menu.html.php"; ?> 

	<? include "franchise-footer.html.php"; ?> 

<? include "email-footer.html.php"; ?>