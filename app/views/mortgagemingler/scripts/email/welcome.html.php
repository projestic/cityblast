<? include "email-header.html.php"; ?>

	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Welcome to <?=COMPANY_NAME;?>, <?php echo $this->member->first_name;?>.</em></h1>
	
	<? /* <h2 style="margin-bottom: 10px; padding: 10px; border-top: solid 1px #C4C4C4; border-bottom: solid 1px #C4C4C4; background-color: #F2F2F2; text-align: center; font-size: 18px; font-weight: bold; font-family: Myriad Pro', Arial; ">CityBlast</a> account setup is complete.</h2>
	<? /* <h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px;">Your <a href="<?=APP_URL;?>">CityBlast</a> account setup is complete.</h2> */ ?>


	<p>Thanks for signing up - and welcome to a very exclusive international group of mortgage professionals. You have just moved to the head of the pack when it comes to online social media marketing in real estate. Congratulations!</p>

	
	<p>If you have set up your <?=COMPANY_NAME;?> Application, you should start to see brand new mortgage content appearing soon. To change your settings at any time, simply log in to 
		<a href="<?=APP_URL;?>/member/settings" style="color: #3D6DCC;"><?=COMPANY_WEBSITE;?></a> and go to your <a href="<?=APP_URL;?>/member/settings" style="color: #3D6DCC;">personal settings</a> to make the adjustments you need.</p>


			
	<div  style='border-color: #C2C2C2 #DDDDDD #F8F8F8; background-color: #e9e9e9; padding: 10px; margin-top: 15px'>	
	
		<h2 style="font-family:'Myriad Pro', Arial; font-size: 25px; margin-top: 0px;"><em>Helpful Tips:</em></h2>
		
		<h3>Want to change how often we post?</h3>
		<p>Posting too often? Not enough? Easily solved! How often we post is entirely up to you. Changing it is a breeze: <a href="<?=APP_URL;?>/member/settings#settingtab">Adjust your Frequency</a>.</p>
		
				
		<h3>Prefer no posts on your Personal Facebook?</h3>
		<p>No problem. We do that! You can effortlessly pick and choose where you'd like our <?=COMPANY_NAME;?> experts to post, including to your Facebook Business Page. Try this: 
			<a href="<?=APP_URL;?>/member/settings#settingtab">Stop publishing to my personal Facebook profile</a>.</p>	
				
				
		<h3>Want to publish to your Business Fan Page?</h3>
		<p>Facebook gave us Pages - so let's use them! If you want to have your posts directed to your Business Page, simply: <a href="<?=APP_URL;?>/member/settings#settingtab">Turn on your Fanpage</a>.</p>
		
	</div>	
	
	
	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px; margin-top: 20px;"><em>Your Settings.</em></h1>
	<p>Your current  settings are as follows:</p>
	
	<br />


	<? include "member-settings.html.php"; ?> 


<br/>
<a href="<?=APP_URL;?>/member/settings" style='	
width: 260px;
margin: 10px 0 0;
float: right;
font-size: 17px;
margin: 10px 0;
margin-bottom: 25px;
height: 40px;
line-height: 40px;
padding: 0 15px;
background: url("<?=APP_URL;?>/images/button_bgs.png") repeat-x scroll 0 -15px transparent;
border: medium none !important;
border-radius: 5px 5px 5px 5px;
box-shadow: 0 1px 0 #FF6C6C inset, 0 -1px 0 #410000 inset;

cursor: pointer;

font-family: myriad-pro,Helvetica,Arial,Verdana,sans-serif;
font-size: 19px;
font-style: italic;
font-weight: 600;
letter-spacing: 0.02em;

outline: medium none !important;
position: relative;
text-align: center;
text-decoration: none !important;
text-shadow: -1px -1px 0 #A10000;
z-index: 3;
color: #FFFFFF;
outline: medium none;
text-decoration: none;' alt="My Settings">Update Your Settings</a>
	


	<? include "email-footer-menu.html.php"; ?> 
	<? include "mortgagemingler-footer.html.php"; ?> 
<? include "email-footer.html.php"; ?>