<?

	$listing	= $this->listing;

	$full_address = null;
	$desc_array = null;
	if(!empty($listing->street))		$desc_array[] = $listing->street;
	if(!empty($listing->city))		$desc_array[] = $listing->city;
	if(!empty($listing->postal_code))	$desc_array[] = $listing->postal_code;

	if(is_array($desc_array)) $full_address .= implode(", ", $desc_array);

	if(empty($_REQUEST['mid']) && isset($_SESSION['member']))	$_REQUEST['mid'] = $_SESSION['member']->id;

	// LISTING PRICE
	if(!empty($listing->price))	$listing->price = preg_replace("/[^a-zA-Z0-9\s]/", "", $listing->price);
	$list_price = null;
	if(!empty($listing->price))
	{
		$list_price .= is_numeric($listing->price)?"$".number_format($listing->price). " ":$listing->price. " ";
	}

?>


<table cellpadding="0" cellspacing="0" border="0" width="600px">
	<tr>
		<td>
			
			<table border="0" cellpadding="0" cellspacing="10" width="600" style="border: 1px solid #e1e1e1">
				<tr>
					<td width="270"><a href="<?=APP_URL . "/listing/view/". $this->listing->id;?>?src=invc"><img src="<?= $this->listing->getSizedImage(280, 250) ?>" alt="Listing" width="270" border="0" /></a></td>
					<td align="left" width="298">
						<h3 style="font-family: Helvetica, Arial; font-size:30px; margin:0 0 10px 0;"><a href="<?= APP_URL . "/listing/view/". $this->listing->id;?>?src=invc" style="color: #000000; text-decoration: none;">$<?php echo number_format($this->listing->price) ?></a></h3>
						<h4 style="font-family: Helvetica, Arial; font-size:20px; line-height: 24px; margin:0 0 5px 0; color:#666; font-weight: bold;"><a href="<?= APP_URL . "/listing/view/". $this->listing->id;?>?src=invc" style="color: #666666; text-decoration: none;"><?php echo $this->listing->street ?></a></h4>
						<p style="margin:0 0 5px 0; color:#666;"><?php echo $this->listing->message ?></p>
					</td>
				</tr>
			</table>
		
		</td>
	
	</tr>
	
	<tr>    	
    	<td style="padding-top: 4px;">
        	<table cellpadding="0" cellspacing="0" border="0">
            	<tr>
	                <td width="380" height="130" bgcolor="#f2f2f2" valign="top" style="padding-left: 10px;">
                		<table cellpadding="0" cellspacing="0" border="0" width="370" style="text-align: left;">
                        	<tr>
                            	<td colspan="2" width="370" style="padding-top: 10px; padding-bottom: 10px;"><a href="<?=APP_URL . "/listing/view/". $this->listing->id;?>?src=invc" style="color: #000000; text-decoration: none;"><?php echo $full_address ?></a></td>
                            </tr>
                        	<tr>
                            	<td width="120"># Bedrooms:</td>
                                <td width="250"><?php echo $this->listing->number_of_bedrooms ?></td>
                            </tr>
                        	<tr>
                            	<td width="120"># Bathrooms:</td>
                                <td width="250"><?php echo $this->listing->number_of_bathrooms ?></td>
                            </tr>
                        	<tr>
                            	<td width="120"># Parking:</td>
                                <td width="250"><?php echo $this->listing->number_of_parking ?></td>
                            </tr>
                        	<tr>
                            	<td width="120">Maintenance:</td>
                                <td width="250">$<?php echo $this->listing->maintenance_fee ?> <span style='color: #a8a8a8'>/month</span></td>
                            </tr>

                        	<tr>
                            	<td width="120">Property Type:</td>
                                <td width="250">
                                
							<? 
								if(isset($this->listing->property_type_id) && !empty($this->listing->property_type_id))
								{
									if($this->listing->property_type_id == "1") echo "Detached";
									elseif($this->listing->property_type_id == "2") echo "Semi-detached";
									elseif($this->listing->property_type_id == "3") echo "Freehold Townhouse";
									elseif($this->listing->property_type_id == "4") echo "Condo Townhouse";
									elseif($this->listing->property_type_id == "5") echo "Condo Apartment";
									elseif($this->listing->property_type_id == "6") echo "Coop/Common Elements";
									elseif($this->listing->property_type_id == "7") echo "Attached/Townhouse";
									else echo "";
								}
                                	?>
                                	<?php /* echo $this->listing->prop_type; */ ?>
                                
                                </td>
                            </tr>

                        </table>
	                </td>
                    <td width="220" align="right" valign="top">
						<?php if(!empty($this->listing->latitude) && !empty($this->listing->longitude)	) : ?>
						<a target="_blank" href="http://maps.google.com/maps/api/staticmap?center=<?php echo $this->listing->latitude ?>,<?php echo $this->listing->longitude ?>&markers=<?php echo $this->listing->latitude ?>,<?php echo $this->listing->longitude ?>&zoom=13&size=600x400&sensor=true">
							<img width="220" height="130" border="0" src="http://maps.google.com/maps/api/staticmap?center=<?php echo $this->listing->latitude ?>,<?php echo $this->listing->longitude ?>&markers=<?php echo $this->listing->latitude ?>,<?php echo $this->listing->longitude ?>&zoom=13&size=220x130&sensor=true" alt="Google Map" />
						</a>
						<? endif; ?>
                    </td>
                </tr>
                

            </table>
        </td>
    </tr>
</table>


<table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td style="text-align: right; width: 100%; padding-top: 25px;">
<a href="<?php echo APP_URL."/listing/view/".$this->listing->id;?>?src=invc"><img src="<?=APP_URL;?>/images/email/button_view_this_listing.jpg" height="40px" width="196px" border="0" /></a>
</td></tr></table>