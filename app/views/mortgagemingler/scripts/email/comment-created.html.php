<? include "email-header.html.php"; ?>

	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>You've received a new comment.</em></h1>
	<p>Your friend <em><?php echo $this->comment->fb_name;?></em> had a question or comment on a recent listing.  They may be interested in the following property:</p> 
	
	<h2 style="margin-top: 35px; margin-bottom: 0px; padding-bottom: 0px; font-family:'Myriad Pro', Arial;"><a href="<?=APP_URL;?>/listing/view/<?=$this->listing->id;?>?ref=cmts_email" style="color: #333333; text-decoration: none;"><?=$this->listing->street . ", " . $this->listing->city . " $".number_format($this->listing->price);?></a></h2>
	<p style="padding-left: 8px; padding-top: 10px; padding-bottom: 10px; border-top: solid 1px #d9d9d9; border-bottom: solid 1px #d9d9d9; background-color: #f7f7f7;"><i><?php echo $this->comment->text;?></i></p>

	<p>You can respond directly to <?=$this->comment->fb_name;?> by visting us at <a href="<?php echo APP_URL."/listing/view/".$this->listing->id;?>?ref=cmts_email" style="color: #3D6DCC;"><?php echo APP_URL."/listing/view/".$this->listing->id;?></a>.</p>


	<? include "email-signature.html.php"; ?> 
	
	<? include "email-footer-menu.html.php"; ?> 

	<? include "mortgagemingler-footer.html.php"; ?> 

<? include "email-footer.html.php"; ?>	