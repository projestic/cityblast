<div class="grid_8">

	
	<div class="box clearfix">
	
		<div class="box_section">
			<h1>The <?=COMPANY_NAME;?> Guarantee.</h1>
		</div>
		
		<h3><?=COMPANY_NAME;?> is Guaranteed.</h3>
		
		<p>Try <?=COMPANY_NAME;?> for 14 days completely FREE. After your first 14 days, if you're not happy with your membership at any time, we'll refund your current month's payment &mdash; no questions asked!</p>  

		<hr>
		
		<a href="/member/join" class="uiButton right">Join Now</a>
		<span style="float: right; line-height: 40px; font-weight: bold; font-size: 14px; margin-right: 10px;">Yes, I'm ready to start my 14 day, risk-free trial.</span>
		
	</div>
	
				
</div>

<div class="grid_4">

	

	<div class="box clearfix" style="padding-top: 1px;">
									
		<h4>No Long Term Contracts</h4>
		<p>We're committed to making sure your <?=COMPANY_NAME;?> service works to generate more business for you. If you ever feel like you're not getting a great return on your investment 
			you can <u>cancel any time</u>. There are no long term contracts.</p>


		<p style="text-align: right;"><a href="/member/join">Start your FREE trial</a></p>
							
	</div>


	<div class="box clearfix" style="padding-top: 1px;">
		
		<h4>How It Works</h4>
		<p><?=COMPANY_NAME;?> is the new way that the industry is sharing information.</p>
		<p style="text-align: right;"><a href="/index/how-blasting-works">Learn more about Blasts</a></p>
		
	</div>

	
</div>