	<!--[if IE 7]>
			<style type="text/css">
				h5, .negative-margin-15{
					margin-top: -15px;
				}
			</style>
		<![endif]-->
	<div class="grid_12">
		
		<div class="box clearfix">
	
			<div class="box_section">
				<h1>Frequently Asked Questions.</h1>
			</div>
			
			<h5>Will <?=COMPANY_NAME;?> publish content on my friends' walls/accounts?</h5>

			<p>No. <?=COMPANY_NAME;?> only publishes to your account. You friends can see the updates, just as if you've written them yourself.<p>
	
			
			<hr />
	
			<h5>Can <?=COMPANY_NAME;?> publish to my Facebook Fan Page? </h5>
			<p>Yes.  <?=COMPANY_NAME;?> can be configured to publish to your fan page. Simply register as normal, and activate any fan page you administer in your <a href="/member/settings">personal settings</a>.</p>
	
			<hr />
			
			<h5>I've heard you shouldn't market or 'sell' via Facebook, Twitter and LinkedIn.</h5>
			<p>Facebook, Twitter and LinkedIn represent today's greatest opportunity to reach our network with reminders that you are a mortgage pro and your doors are open.  By using added-value content, 
				focused on giving your friends valuable and interesting mortgage information, you are passively and thoughtfully keeping your network focused on you, rather than 'selling' to them.  When they are interested in buying, they will turn to you first.</p>
			
			<hr />
			
			<h5>How often does <?=COMPANY_NAME;?> publish to my account(s)?</h5>
			<p>As often as you choose; from 1 to 7 times per week.</p>
			
			<hr/>
			
			<h5>Can I publish to Facebook, but not Twitter and LinkedIn, or vice versa?</h5>
			<p>Yes.  <?=COMPANY_NAME;?> can publish to any combination of your accounts.</p>
			
			<hr/>
			
			<h5>Can I publish to my website?</h5>
			<p>Unfortunately at this time <?=COMPANY_NAME;?> does not support updates to your personal website.</p>

			<hr/>
			
			<h5>What type of content does <?=COMPANY_NAME;?> publish to my accounts?</h5>
			<p><?=COMPANY_NAME;?> publishes three types of content: informational, lifestyle, and listings. Informational content consists of articles and videos featuring market data, statistics, 
				analysis and forecasts. Lifestyle content includes articles and videos of home and garden advice, renovation tips, and mortgage transaction processes. Listings are local 
				property listings.  You may choose to turn on or off the publishing of listings to your social media accounts at any time in your settings.</p>
			
			<hr/>


			<h5>What if I don't see my posts?</h5>
			<p>Members should ensure that they are looking on their wall, and not their news feed. To access the wall, simply log in to Facebook, and click on your own name in the top right section of the site header.</p>
			
			<hr/>
			
			
			<h5>Can I turn off <?=COMPANY_NAME;?>?</h5>
			<p>Yes.  You can turn off <?=COMPANY_NAME;?>'s publishing at any time by simply accessing your settings at <a href="/member/settings"><?=COMPANY_WEBSITE;?>/member/settings</a>.</p>
			
			<hr/>
			
			<h5>Does <?=COMPANY_NAME;?> have access to my Facebook, Twitter or LinkedIn passwords?</h5>
			<p>No.  <?=COMPANY_NAME;?> only has the ability to perform the functions described.  <?=COMPANY_NAME;?> does not have access to your login information or passwords.</p>
			
			<hr/>
			
			
			<h5>Will my friends feel like I am publishing content too often?</h5>
			<p>You have control over how often you publish.  You should publish your own personal content as you normally would, as well as professional added-value content via <?=COMPANY_NAME;?> to your social media accounts.</p>
			
			<hr/>
			
			<h5>Will I publish the same thing as other members, or at the same time?</h5>
			<p>No.  Everyone publishes different content, and no two members publish the same updates at the same time.  Although some posts may be similar, content, images, blurbs, order of publishing, and 
				time of day are randomized daily, providing an organic, natural-looking result.</p>
			
			<hr/>


			<h5>Is there a limit to how many members may sign up in my City?</h5>
			<p>Yes.  <?=COMPANY_NAME;?> limits the number of members allowed in each City using a careful algorithm related to Facebook publishing and member overlap.  <?=COMPANY_NAME;?> uses this algorithm and limit to keep the 
				service exclusive and maximize return for our members.  When 
				there are no more spaces available in your City, you may request to join the waiting list.</p>
			
			<hr/>
			
						
			<h5>Will <?=COMPANY_NAME;?> publish branded content, or content featuring another mortgage professional?</h5>
			<p>No.  <?=COMPANY_NAME;?> does not publish branded content from other members or mortgage companies.</p>
			
			<hr/>

			
			<h5>How locally-focused is <?=COMPANY_NAME;?>?</h5>
			<p>It depends on your market.  If you are in a major market, <?=COMPANY_NAME;?> content will be more geographically focused on a smaller area.  As a simple rule: where the number of 
				users on the service is higher, <?=COMPANY_NAME;?> will always be more geographically defined.</p>
			
			<hr/>
			
			<?/*
			<h5>What if I get a lead that I prefer not to deal with personally?</h5>
			<p>If you get a lead that you'd rather not work personally, refer them to someone in your office in exchange for a referral fee.  It won't cost you any time, and you'll still gain from having 
				created the business.</p>
			*/?>


			<br/>
		
		</div>
	</div>
	
