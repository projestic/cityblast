<div class="contentBox questionContainer">
	<div class="contentBoxTitle">
		<h1>Need Help With Your Account Settings?</h1>
	</div>

	<p>You've come to the right place! We've created some quick and simple 60 second videos that will help you with <i>every</i> aspect of setting up your account.</p>





	<h3 style="margin-top: 25px;">General Settings</h3>


	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I'm confused by some of the statistics on my Dashboard, what do they mean?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">If you're confused or unsure about some of the statistics in your Dashboard, watch this quick video that <a href="/help/dashboard">explains your Dashboard statistics</a>.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How do I change my credit card or billing information on <?=COMPANY_NAME;?>?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Changing your billing information is quick and easy. Please watch this quick video on <?=COMPANY_NAME;?> <a href="/help/updatecc">changing your Credit Card information</a> to learn how.</p> 
	</div>
	
	

	<h3 style="margin-top: 25px;">Content</h3>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How do I change the type of content my <i>Social Expert</i> is posting?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Telling your <i>Social Expert</i> what type of content to post on your behalf is a snap! Watch this quick video on <a href="/help/customcontent">changing your Content settings</a> to learn how.</p> 
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>My <i>Social Expert</i> is posting too little or too much , how do I change that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
			<p  class="answer">Telling your <i>Social Expert</i> how much or how little you want them to post takes only a minute. Watch this quick video and tell your <i>Social Expert </i> <a href="/help/frequency">to turn up or turn down</a> how often they post on your behalf.</p>
	</div>
	
	<?/*******
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How do I change the type of content my <i>Social Expert</i> is posting?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		
		<p class="answer">Telling your <i>Social Expert</i> what type of content to post on your behalf is a snap! Watch this quick video on <a href="/help/customcontent">changing your Content settings</a> to learn how.</p>
	</div> *****/ ?>
	

	<h3 style="margin-top: 25px;">Adding Additional Accounts</h3>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I want to add my Fanpage my account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Adding your Fanpage is a breeze, just watch this quick video on <a href="/help/addfanpage">setting up your Fanpage</a> to learn how.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I want to add Twitter to my account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Adding Twitter is a breeze, just watch this quick video on <a href="/help/addtwitter">setting up your Twitter account</a> to learn how.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I want to add LinkedIn to my account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Adding LinkedIn is a breeze, just watch this quick video on <a href="/help/addlinkedin">setting up your LinkedIn account</a> to learn how.</p>
	</div>
	
	

	<h3 style="margin-top: 25px;">Resetting Accounts</h3>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>My <i>Social Expert</i> asked me to reset my Fanpage, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Resetting your Fanpage is easy, just watch this quick video on <a href="/help/fanpagereset">resetting up your Fanpage account</a> to learn how.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>My <i>Social Expert</i> asked me to reset my Twitter account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Resetting your Twitter account is easy, just watch this quick video on <a href="/help/twitterreset">resetting up your Fanpage account</a> to learn how.</p>
	</div>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>My <i>Social Expert</i> asked me to reset my LinkedIn account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Resetting your LinkedIn account is easy, just watch this quick video on <a href="/help/linkedinreset">resetting up your Fanpage account</a> to learn how.</p>
	</div>
	
	

		
</div>


<div class="contentBox">
	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 26px;">Need more help?</h3>
			<h4>Our <i>Social Experts</i> are on standby.</h4>
		</div>
		<div class="pull-right">
			<a class="uiButtonNew tryDemo" href="mailto: info@<?=COMPANY_NAME;?>.com">Talk To Us.</a>
		</div>	
	</div>		
</div>