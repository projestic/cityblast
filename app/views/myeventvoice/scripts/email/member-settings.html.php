<table style="border-top: solid 1px #d9d9d9; border-bottom: solid 1px #d9d9d9; background-color: #f7f7f7;">
<!-- facebook settings-->
	<tr>
		<td colspan="2">
			<? if($this->member->facebook_publish_flag && $this->member->facebook_fails < 3) : ?>
				<h5 style="font-size: 16px; color: #333; margin-bottom: 0; margin-top: 0; padding-top: 10px;"><img border="0" src="<?=APP_URL;?>/images/right.jpg" width="17" height="17" style="float: left; margin-right: 3px;">Facebook is working.</h5>
				<p style="color: #b7b7b7; padding-top: 10px; margin-top: 0; margin-bottom: 10px;">It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
				business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
			<? else : ?>
				<h5 style="font-size: 16px; color: #333; margin-bottom: 0; margin-top: 0; padding-top: 10px;"><img  border="0" src="<?=APP_URL;?>/images/cross.jpg" width="17" height="17"  style="float: left; margin-right: 3px;">Facebook is Off.  No leads.</h5>
				<p style="padding-top: 10px; margin-top: 0; margin-bottom: 10px;">It's a great idea to keep your Facebook publishing on. It lets everyone know you're serious about your real estate 
				business, generates hot new leads from your friends and acquaintances, and gives you a professional, dedicated image.</p>
			<? endif; ?>
		</td>
	</tr>
	<tr>
			
		<? if($this->member->facebook_publish_flag && $this->member->facebook_fails < 3) : ?>
			<td colspan="2" style="text-align: right; border-top: solid 1px #FFFFFF; border-bottom: solid 1px #FFFFFF; padding: 10px;"><a href="<?=APP_URL;?>/member/settings" style="width: 160px;" alt="Turn Facebook Off"><img width="225px" height="40px" border="0" src="<?=APP_URL;?>/images/email/button_facebook_off.jpg" ></a></td>
		<? else : ?>
			<td colspan="2" style="text-align: right; border-top: solid 1px #F7F7F7; border-bottom: solid 1px #FFFFFF; padding: 10px;"><a href="<?=APP_URL;?>/member/settings" style="width: 160px;" alt="Turn Facebook On"><img border="0" width="225px" height="40px" src="<?=APP_URL;?>/images/email/button_facebook_on.jpg" ></a></td>		
				
		<? endif; ?>
	</tr>
	<!-- Twitter settings -->
	<tr>
		<td colspan="2">
			<? if( $this->member->twitter_publish_flag && $this->member->twitter_fails < 3) : ?>

				<h5 style="font-size: 16px; color: #333; margin-bottom: 0; margin-top: 0; padding-top: 10px;"><img border="0" src="<?=APP_URL;?>/images/right.jpg" width="17" height="17" style="float: left; margin-right: 3px;">Twitter is working.</h5>
				<p style="color: #b7b7b7; padding-top: 10px; margin-top: 0; margin-bottom: 10px;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers stay tuned with consistent and professional local real estate content.</p>
								
			<? else : ?>

				<h5 style="font-size: 16px; color: #333; margin-bottom: 0; margin-top: 0; padding-top: 10px;"><img border="0" src="<?=APP_URL;?>/images/cross.jpg" width="17" height="17" style="float: left; margin-right: 3px;">Twitter is Off.  No leads.</h5>					
				<p style="padding-top: 10px; margin-bottom: 10px; margin-top: 0;">With your Twitter publishing on, even when you are too busy to keep up with posts and comments, <?=COMPANY_NAME;?> makes sure your followers stay tuned with consistent and professional local real estate content.</p>
									
			<? endif; ?>
		</td>
	</tr>
	
	<tr>
		<? if( $this->member->twitter_publish_flag && $this->member->twitter_fails < 3) : ?>
			<td colspan="2" style="text-align: right; border-top: solid 1px #FFFFFF; border-bottom: solid 1px #FFFFFF; padding: 10px;"><a href="<?=APP_URL;?>/member/settings" style="width: 160px;"><img src="<?=APP_URL;?>/images/email/button_twitter_off.jpg" width="225px" height="40px"  border="0"  alt="Turn Twitter Off"/></a></td>
		<? else: ?>			
			<td  colspan="2" style="text-align: right; border-top: solid 1px #F7F7F7; border-bottom: solid 1px #FFFFFF; padding: 10px;"><a href="<?=APP_URL;?>/member/settings"  style="width: 160px;"><img  border="0" src="<?=APP_URL;?>/images/email/button_twitter_on.jpg" width="225px" height="40px"  alt="Turn Twitter On"/></a></td>
		<? endif; ?>
	</tr>
	
	<!-- LinkedIn Settings -->
	<tr>
		<td colspan="2">
			<? if( $this->member->linkedin_publish_flag && $this->member->linkedin_fails < 3) : ?>

				<h5 style="font-size: 16px; color: #333; margin-bottom: 0; margin-top: 0; padding-top: 10px;"><img  border="0" src="<?=APP_URL;?>/images/right.jpg" width="17" height="17" style="float: left; margin-right: 3px;">LinkedIn is working.</h5>
				<p style="color: #b7b7b7; padding-top: 10px; margin-top: 0; margin-bottom: 10px;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying visible and active in this network is highly recommended.</p>
				
			<?php else: ?>
				<h5 style="font-size: 16px; color: #333; margin-bottom: 0; margin-top: 0; padding-top: 10px;"><img  border="0" src="<?=APP_URL;?>/images/cross.jpg" width="17" height="17" style="float: left; margin-right: 3px;">LinkedIn is Off.  No leads.</h5>
				<p style="padding-top: 10px; margin-bottom: 10px; margin-top: 0;">LinkedIn is good for business.  Keep your publishing on and maintain your voice with this lucrative market of potential clients.  Staying visible and active in this network is highly recommended.</p>
			
			<? endif; ?>			
		</td>
	</tr>
	<tr>
		<? if( $this->member->linkedin_publish_flag && $this->member->linkedin_fails < 3) : ?>
		
			<? /*<td style="border-top: solid 1px #FFFFFF; border-bottom: solid 1px #FFFFFF; color: #4D4D4D;"><span style="line-height: 40px; font-weight: bold; font-size: 11px;"><strong style="color: #cc0000;">Note: </strong>Turning this off means no new leads will be generated.</span></td>*/ ?>
			<td colspan="2" style="text-align: right; border-top: solid 1px #FFFFFF; border-bottom: solid 1px #FFFFFF; padding: 10px;"><a href="<?=APP_URL;?>/member/settings" class="uiButton linkedin off right" style="width: 160px;"><img width="225px" height="40px"  border="0"  src="<?=APP_URL;?>/images/email/button_linkedin_off.jpg" alt="Turn LinkedIn Off"/></a></td>

		<?php else: ?>
			<? /*<td style="border-top: solid 1px #FFFFFF; border-bottom: solid 1px #FFFFFF; color: #4D4D4D; font-size: 14px;"><span style="line-height: 40px; font-weight: bold;">Attract new clients now.</span></td>*/ ?>
			<td colspan="2" style="text-align: right; border-top: solid 1px #FFFFFF; border-bottom: solid 1px #FFFFFF; padding: 10px;"><a href="<?=APP_URL;?>/member/settings" class="uiButton linkedin right" style="width: 160px;"><img  border="0" width="225px" height="40px"  src="<?=APP_URL;?>/images/email/button_linkedin_on.jpg" alt="Turn LinkedIn On"/></a></td>
		<? endif; ?>
	
	</tr>
</table>