<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>Welcome to <?=COMPANY_NAME;?></h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
	
		</div>
	
		<? $email_helper->openContentDiv(); ?>

 			<? $email_helper->contentH1(); ?>Get ready to have your social media marketing done for you.</h1>
			

			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>!</p>
		
			
			<? $email_helper->contentP(); ?>Welcome to  <a href="<?=APP_URL;?>" <? $email_helper->linkStyle();?>><?=COMPANY_NAME;?></a>.  Your preferences have been received, 
			and the industry's <u><i>Top Experts</i></u> are already getting started planning your online presence and keeping you fresh and up-to-date.  Congrats for taking action and improving your real estate business!</p> 
			
			<? $email_helper->contentP(); ?>Congrats for taking action and improving <u><i>your</i></u> real estate business!</p>
	
			<? /*$email_helper->contentP(); ?>You've now got your account set up, but you may want to <a href="<?=APP_URL;?>/member/authenticate" <? $email_helper->linkStyle();?>>log in</a> and add a few things, 
			like your Facebook business page, Twitter or LinkedIn profiles.  Not to worry - it's all included!  Simply <a href="<?=APP_URL;?>/member/authenticate" <? $email_helper->linkStyle();?>>click the button below</a> and 
			it will take you straight to your <a href="<?=APP_URL;?>/member/authenticate" <? $email_helper->linkStyle();?>>Settings page</a>, where you can update and tweak to your heart's delight.</p>*/?>

			<? $email_helper->contentP(); ?>Now that we know what type of stuff you want us to post, you may want to add a few other things, like your 
			<a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>Facebook business page</a>, 
			<a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>Twitter</a> or 
			<a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>LinkedIn profiles</a>.  Go ahead - 
			<a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>it's all included</a>!</p>
			 			
			<? $email_helper->contentP(); ?>Simply head to your <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>Dashboard</a>, and you can update and tweak to your heart's delight.</p>
					
			<? $email_helper->contentP(); ?>Also, remember you can always feel free to call us if you're having any trouble, or just want to chat <strong><?=TOLL_FREE?></strong>.</p>
				
				
			<? $email_helper->contentP(); ?>P.S. Did you know that <span style="background-color: #ffff00;"><i>you can reach thousands of potential buyers</i></span> 
			by "<a href="http://www.cityblast.com/blog/sell-your-listing-faster-unlock-the-secret-of-social-media-sharing/" <? $email_helper->linkStyle();?>>Blasting</a>" your listings to 
				our <i>massive network</i>?</p>  
					
			<? $email_helper->contentP(); ?>Best of all, it's 100% included in your membership!</p>
				
			<? $email_helper->contentP(); ?>Find out more about <a href="http://www.cityblast.com/index/how-blasting-works" <? $email_helper->linkStyle();?>>Blasting right here</a>!</p>
					
			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
	
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">&nbsp;Adjust Your Profile&nbsp;</a>
			</p>   
	    
	       			
		</div>
	
	</div>
		


<? include "email-footer-NEW.html.php"; ?>
