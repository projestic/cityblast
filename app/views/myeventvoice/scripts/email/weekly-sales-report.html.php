<? $email_helper = $this->email(); ?>
<? include "email-header-NEW.html.php"; ?>
	<? $email_helper->openBox(); ?>
		<? $email_helper->openContentDiv(); ?>
 			<? $email_helper->contentH1(); ?>Weekly Sales Report</h1>
 			<? $email_helper->contentP(); ?>Find below weekly sales report from <?= $this->from ?> to <?= $this->to ?></p>
 			<table style="margin-left:15px">	
				<tr>	
					<td style="background-color:#e4e4e4; padding:4px">Sales Person</td>
					<td style="background-color:#e4e4e4; padding:4px">Number of Sales</td>
					<td style="background-color:#e4e4e4; padding:4px">Total Volume</td>
					<td style="background-color:#e4e4e4; padding:4px">Commission</td>
				</tr>
				<? foreach ($this->members as $member) { ?>
				<tr>	
					<td style="padding:4px"><?= $member->first_name ?> <?= $member->last_name ?></td>
					<td style="padding:4px"><?= $member->count ?></td>
					<td style="padding:4px"><?= number_format($member->revenue) ?></td>
					<td style="padding:4px"><?= number_format($member->commission) ?></td>
				</tr>
 				<? } ?>
			</table>
		</div>
	</div>
<? include "email-footer-NEW.html.php"; ?>