<?php
	$email_helper = $this->email(); 
	$listing_id = !empty($this->listing) ? $this->listing->id : '';
?>

<? include "email-header-NEW.html.php"; ?>
	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?><?= $this->member->first_name;?> <?= $this->member->last_name;?> WANTS YOU TO TRY US OUT!</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>

		<? $email_helper->contentH1(); ?>A Special Invite from <?= $this->member->first_name;?> <?= $this->member->last_name;?></h1>

		<? $email_helper->contentP(); ?>Hi there!</p>
			
		<? $email_helper->contentP(); ?>Your friend <?= $this->member->first_name;?> uses our live Social Media Experts 
		to manage their business's social presence, and thought you might like to try us out as well.</p>
			
		<? $email_helper->contentP(); ?>
			Our Experts become your social media marketing consultants, and post hot new events-industry articles, 
			videos and information to your Facebook, Twitter and LinkedIn accounts each day - so you don't have to! 
			You choose exactly the type of content you want, and how your page should look.  They do the rest!
		</p>
		
		<? $email_helper->contentP(); ?>Click the button below to check out some examples, and read more about how we 
		can help you. We're standing by to show you what we can do!</p>

		<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
			<a href="<?php echo APP_URL."/listing/view/".$listing_id;?>?ref=cmts_email" style="<? $email_helper->actionButtonStyle();?>">Check It Out!</a>
		</p>   
	    
		</div> 
	</div>
<? include "email-footer-NEW.html.php"; ?>