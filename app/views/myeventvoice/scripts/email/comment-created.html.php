<? $email_helper = $this->email(); ?>

<? include "email-header-NEW.html.php"; ?>
	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>NEW INTERACTION</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
		</div>

		<? $email_helper->openContentDiv(); ?>
 			<? $email_helper->contentH1(); ?>You've Got a New Comment</h1>

			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>!</p>
			
			<? $email_helper->contentP(); ?>
			Your friend <?=$this->comment->fb_name;?> has a question or comment on a recent post that your Expert shared.   
			They may be interested in chatting about your business!
			</p>
			
			<? $email_helper->contentP(); ?>Respond to <strong><?=$this->comment->fb_name;?></strong> now by clicking below!</p>
		
			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?php echo APP_URL."/listing/view/".$this->listing->id;?>?ref=cmts_email" style="<? $email_helper->actionButtonStyle();?>">Talk To <?=$this->comment->fb_name;?> Now!</a>
			</p>   
	    
		</div> 

	</div>
<? include "email-footer-NEW.html.php"; ?>