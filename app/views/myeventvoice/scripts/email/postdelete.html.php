<? $email_helper = $this->email(); ?>
<? include "email-header-NEW.html.php"; ?>
	<? $email_helper->openBox(); ?>
		<? $email_helper->openCap(); ?>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>LISTING SHOWCASE</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
		</div>
		<? $email_helper->openContentDiv(); ?>
 			<? $email_helper->contentH1(); ?>Oh Dear, something went wrong with your listing!</h1>
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>
			<? $email_helper->contentP(); ?>After reviewing your recent listing, your Social Expert had this to say: <?=$this->comment;?></p>
            
            <? $email_helper->contentP(); ?>Here's what the post looked like on Facebook, for example:</p>
			<? include "blast-preview.html.php"; ?>
		</div>
	</div>	
<? include "email-footer-NEW.html.php"; ?>
