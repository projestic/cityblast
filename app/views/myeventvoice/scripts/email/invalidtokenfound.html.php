<?php
$network_type = $this->network_type;
$network_name = $this->network_name;
?>

<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>

	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>URGENT: POSTING PROBLEM</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>

 			<? $email_helper->contentH1(); ?>Your Expert Can't Currently Post To <?=$network_name?> </h1>
	
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>
			
			<? $email_helper->contentP(); ?>Just a friendly reminder from your Social Expert: We recently tried to post 
			a new article for you, but weren't able to because your account needs to be refreshed.</p>
			
			<? $email_helper->contentP(); ?>Not to worry!  This is very common, and happens when you don't log in for 60 
			days, OR when you've just changed one of your passwords.  The solution is super-easy:
			

					
				<? if(strtolower($network_name) == "facebook") : ?>

					<ul>
						<li <?=$email_helper->orderedListStyle();?>>Click the button below and log in to <?=COMPANY_NAME;?></li>
					</ul>

				<? elseif(strtolower($network_name) == "twitter") : ?>
				
					<ol>
						<li <?=$email_helper->orderedListStyle();?>>Click the button below and 'Sign In' to <?=COMPANY_NAME;?></li>
						<li <?=$email_helper->orderedListStyle();?>>Click on the Settings tab</li>
						<li <?=$email_helper->orderedListStyle();?>>Scroll down to the bottom of the page and click on the Twitter tab</li>
						<li <?=$email_helper->orderedListStyle();?>>In the bottom left hand corner there is a button labeled <b>Reset</b>. Please click that button and follow the instructions carefully</li>
						<li <?=$email_helper->orderedListStyle();?>>After you have completed those steps, you can use the Test Publish button to confirm that our Social Experts have access again!</li>
				
					</ol>
				
				<? elseif(strtolower($network_name) == "linkedin") : ?>

					<ol>
						<li <?=$email_helper->orderedListStyle();?>>Click the button below and 'Sign In' to <?=COMPANY_NAME;?></li>
						<li <?=$email_helper->orderedListStyle();?>>Click on the Settings tab</li>
						<li <?=$email_helper->orderedListStyle();?>>Scroll down to the bottom of the page and click on the LinkedIn tab</li>
						<li <?=$email_helper->orderedListStyle();?>>In the bottom left hand corner there is a button labeled <b>Reset</b>. Please click that button and follow the instructions carefully</li>
						<li <?=$email_helper->orderedListStyle();?>>After you have completed those steps, you can use the Test Publish button to confirm that our Social Experts have access again!</li>
				
					</ol>
		
				<? else : ?>
				
					<ol>
						<li <?=$email_helper->orderedListStyle();?>>Click the button below and 'Sign In' to <?=COMPANY_NAME;?></li>
						<li <?=$email_helper->orderedListStyle();?>>Click on the Settings tab</li>
						<li <?=$email_helper->orderedListStyle();?>>Scroll down to the bottom of the page and click on the FanPage tab</li>
						<li <?=$email_helper->orderedListStyle();?>>Find the FanPage you wish to have publishing with our service and please turn it OFF and then back ON again using the ON/OFF switch located on the right hand side of the panel.</li>
						<li <?=$email_helper->orderedListStyle();?>>After completing these steps, you can use the Test Publish button to confirm that our Social Experts have access again!</li>
				
					</ol>
					
				<? endif; ?>

			</p>
		
			<br/>
			<? $email_helper->contentP(); ?>It's that easy!</p>
			
			<? $email_helper->contentP(); ?>Also, feel free to call us if you're having any trouble.  Sometimes this stuff is easier with someone on the phone - <strong><?=TOLL_FREE;?></strong>.</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Fix It Now!</a>
			</p>   
	    
		</div> 

	</div>
	
<? include "email-footer-NEW.html.php"; ?>