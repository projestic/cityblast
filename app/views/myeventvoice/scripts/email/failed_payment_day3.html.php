<? $email_helper = $this->email(); ?>

<? include "email-header-NEW.html.php"; ?>
	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>PAYMENT FAILURE</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>

 			<? $email_helper->contentH1(); ?>We Were Unable to Process Your Payment</h1>

			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>!</p>

			<? $email_helper->contentP(); ?>We tried to complete your monthly payment today, and for some reason it 
			didn't process.  In order to keep your Expert posting on your accounts, we will need to update your information.</p>
			
			<? $email_helper->contentP(); ?>You can do so by clicking the button below, and entering any new credit card number that you'd prefer to use. 
			Or just give us a call at <strong><?=TOLL_FREE;?></strong> if that's easier?</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/payment?rebill=1" style="<? $email_helper->actionButtonStyle();?>">Update My Payment Info</a>
			</p>   
	    
		</div> 

	</div>

<? include "email-footer-NEW.html.php"; ?>