<? include "email-header.html.php"; ?>

		<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px; "><em>Your Order Was Successful.</em></h1>
			
		<p>Your Blast has been successfully created.  It is now scheduled to be advertised to tens of thousands of local buyers on Facebook, Twitter and LinkedIn, and will begin shortly.  Please retain your confirmation 
		code in case you'd like to contact <?=COMPANY_NAME;?> about your listing.  And of course, we'll send you a follow-up email once your listing has finished publishing.</p>
		
		<p>We look forward to your next successful Blast.</p>

		
		<h3 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 0px; padding-top: 25px;">Invoicing Details</h3>

			<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="24"><h3>Tracking Number: #<?=number_format(sprintf("%03d",$this->order_number), 0, '.', '-');?></h3></td>

					<td height="24" style="text-align: right;"><strong>Invoice Date: </strong><? if(isset($this->listing->created_at) && !empty($this->listing->created_at)) echo $this->listing->created_at->format("M d, Y"); ?></td>
				</tr>
				<tr>
					<td width="50%" valign="top" style="padding-top: 15px; padding-bottom: 15px; border-top: solid 1px #d9d9d9; border-bottom: solid 1px #d9d9d9; background-color: #f7f7f7;">
						<strong><?=$this->listing->member->first_name;?> <?=$this->listing->member->last_name;?></strong><br />
						<? if(isset($this->listing->payment->address1) && !empty($this->listing->payment->address1)  && $this->listing->payment->address1 != "empty" && $this->listing->payment->address1 != "undefined") echo $this->listing->payment->address1;?>
						<? if(isset($this->listing->payment->address2) && !empty($this->listing->payment->address2)  && $this->listing->payment->address2 != "empty" && $this->listing->payment->address2 != "undefined") echo $this->listing->payment->address2;?><br />
						<? if(isset($this->listing->payment->city) && !empty($this->listing->payment->city)  && $this->listing->payment->city != "empty" && $this->listing->payment->city != "undefined") echo $this->listing->payment->city;?>,
						<? if(isset($this->listing->payment->state) && !empty($this->listing->payment->state)  && $this->listing->payment->state != "empty" && $this->listing->payment->state != "undefined") echo $this->listing->payment->state;?>
						<? if(isset($this->listing->payment->zip) && !empty($this->listing->payment->zip)  && $this->listing->payment->zip != "empty" && $this->listing->payment->zip != "undefined") echo $this->listing->payment->zip;?><br />
						<? if(isset($this->listing->payment->country) && !empty($this->listing->payment->country) && $this->listing->payment->country != "empty" && $this->listing->payment->country != "undefined") echo $this->listing->payment->country;?>
					</td>
					<td width="50%" valign="top" style="padding-top: 15px; padding-bottom: 15px; border-top: solid 1px #d9d9d9; border-bottom: solid 1px #d9d9d9; background-color: #f7f7f7;">
						<strong><?=COMPANY_NAME;?></strong><br />
						<?=COMPANY_ADDRESS;?><br />
						<?=COMPANY_CITY;?>, <?=COMPANY_PROVINCE;?><br />
						<?=COMPANY_POSTAL;?><br />
						<?=COMPANY_COUNTRY;?>
						<?if(defined('COMPANY_TAX_TYPE') && defined('COMPANY_TAX_ID')):?>
						<br /><br />
						<strong><?=COMPANY_TAX_TYPE . '#';?>:</strong><?=COMPANY_TAX_ID;?>
						<?endif;?>
					</td>
				</tr>
				<tr>
					<td height="30"></td>
					<td></td>
				</tr>
			</table>

			<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th width="300" align="left">Purchased Item</th>
					<th width="100">Quantity</th>
					<th width="100" align="right">Unit Price</th>
					<th width="100" align="right">Item Total</th>
				</tr>
				<tr>
					<td><?=COMPANY_NAME;?> - Facebook + Twitter + LinkedIn</td>
					<td align="center">1</td>
					<td align="right">
						<? if(	strtoupper($this->listing->payment->paypal_confirmation_number) == "FREE BLAST") : ?>
							Free Blast!
						<? else : ?>
							$<?=number_format($this->listing->payment->amount, 2);?>
						<? endif;?>
					</td>
					<td align="right">
						<? if(	strtoupper($this->listing->payment->paypal_confirmation_number) == "FREE BLAST") : ?>
							$0.00
						<? else : ?>
							$<?=number_format($this->listing->payment->amount, 2);?>
						<? endif;?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right" style="padding-top: 12px;"><strong>Subtotal</strong></td>
					<td align="right" style="padding-top: 12px;">
						<? if(	strtoupper($this->listing->payment->paypal_confirmation_number) == "FREE BLAST") : ?>
							$0.00
						<? else : ?>
							$<?=number_format($this->listing->payment->amount, 2);?>
						<? endif;?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right"><strong>Tax (<?php echo ($this->taxes*100); ?>%)</strong></td>
					<td align="right">
						$<?=number_format( $this->listing->payment->taxes , 2);?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right"><strong>Total</strong></td>
					<td align="right">
						<strong>$<?=number_format( $this->listing->payment->amount + $this->listing->payment->taxes , 2);?></strong>
					</td>
				</tr>
			</table>

		<?/*<h2 style="margin-bottom: -5px; padding: 10px; padding-left: 14px; border-top: solid 1px #C4C4C4; border-bottom: solid 1px #C4C4C4; background-color: #F2F2F2; text-align: center; font-size: 18px; font-weight: bold; font-family: Myriad Pro', Arial; "><a href='<?php echo APP_URL."/listing/view/".$this->listing->id;?>?src=invc'>Preview your listing</a></h2>*/?>

		<table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td style="text-align: right; width: 100%; padding-top: 25px;">
		<a href="<?php echo APP_URL."/listing/view/".$this->listing->id;?>?src=invc">
			<img src="<?=APP_URL;?>/images/email/button_view_this_listing.jpg" height="40px" width="196px" border="0" />
		</a>
		</td></tr></table>
	

	<? include "email-signature.html.php"; ?> 

	<? include "email-footer-menu.html.php"; ?> 

	<? include "franchise-footer.html.php"; ?> 

<? include "email-footer.html.php"; ?>