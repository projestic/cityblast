<? $email_helper = $this->email(); ?>

<? include "email-header-NEW.html.php"; ?>

	<? $email_helper->openBox(); ?>
		<? $email_helper->openCap(); ?>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>URGENT: TOKEN EXPIRY</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
		</div>

		<? $email_helper->openContentDiv(); ?>
			<? $email_helper->contentH1(); ?>You Must Log In To Give Our Experts Access</h1>
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>
			
			<? $email_helper->contentP(); ?>
			This is a friendly reminder that you MUST log in to <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>><?=COMPANY_NAME;?></a> today 
			to keep your Social Expert posting to your social media profiles.</p>
			
			<? $email_helper->contentP(); ?>CityBlast and Facebook work hand-in-hand to keep your account secure, and so we ask that you log in at least once every 
				60 days just to say "hi" and confirm your Settings.</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Fix It Now!</a>
			</p>   
	    
		</div>
	</div>
	
<? include "email-footer-NEW.html.php"; ?>