<? $email_helper = $this->email(); ?>

<? include "email-header-NEW.html.php"; ?>

	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>PAYMENT RETRY</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>

 			<? $email_helper->contentH1(); ?>We Were Unable to Process Your Payment</h1>

			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>!</p>
		
			<? $email_helper->contentP(); ?>Just sending a friendly note to inform you: We tried to process your 
			monthly payment today, and for some reason it didn't go through.  Not to worry!  We'll give it a try again 
			tomorrow.</p>
			
			<? $email_helper->contentP(); ?>If you do need to update your payment info however, please do so by clicking 
			the button below, and entering the new number.</p>
			
			<? $email_helper->contentP(); ?>Or give us a call at <strong><?=TOLL_FREE;?></strong> if that's easier?  
			We're happy to help.</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/payment?rebill=1" style="<? $email_helper->actionButtonStyle();?>">Update My Payment Info</a>
			</p>   
	    
		</div> 
		
	</div>

<? include "email-footer-NEW.html.php"; ?>