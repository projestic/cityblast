<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>

	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>REFERRAL ERROR</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>

 			<? $email_helper->contentH1(); ?>Oh Dear, Something Went Wrong!</h1>

			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>
			
			<? $email_helper->contentP(); ?>Although we definitely encourage you to refer friends to <?=COMPANY_NAME;?>, it seems recently you have accidentally referred yourself!</p>
			
			<? $email_helper->contentP(); ?>We’re just sending you this friendly reminder that only referrals of other agents will result in a free month.</p>
			
			<? $email_helper->contentP(); ?>Thanks again; and happy referring!</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?php echo APP_URL."/member/settings#referagenttab";?>" style="<? $email_helper->actionButtonStyle();?>">Refer Another Person!</a>
			</p>   
	    
		</div> 
	</div>
	
<? include "email-footer-NEW.html.php"; ?>
