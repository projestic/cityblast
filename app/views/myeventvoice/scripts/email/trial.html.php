<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>WELCOME TO CITYBLAST</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Your FREE Trial Has Begun</h1>
	
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>!</p>

			
			<? $email_helper->contentP(); ?>Welcome to CityBlast.  Your preferences have been received by our Social Experts, and we'll get started keeping your 
			profiles fresh and up-to-date right away.  Congrats for taking action and improving your real estate business!</p>
			
			<? $email_helper->contentP(); ?>We noticed that you didn't quite finish the registration process.  Not a problem!</p>
		
			<? $email_helper->contentP(); ?>We'll go ahead and get started with updating your profiles now.  Just make sure to <span style="background-color: #ffff00;"><i>log in 
			some time during the next 7 days and add your payment information, that way we can unlock your full <b>14-Day FREE Trial</b></i></span>.</p>
			
			<? $email_helper->contentP(); ?>Or better yet, you can add your info now by simply clicking the button below!  It only takes 60 seconds.   Or feel free to call us if you're having any trouble - 1-888-712-7888.</p>
			
						

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Unlock My 14-Day Trial!</a>
			</p>   
	    
		</div>  		

		
	</div>	
	



<? include "email-footer-NEW.html.php"; ?>