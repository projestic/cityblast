<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>Welcome To <?=COMPANY_NAME;?></h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
	
		</div>
	
		<? $email_helper->openContentDiv(); ?>

 			<? $email_helper->contentH1(); ?>Welcome to <?=COMPANY_NAME;?></h1>
			

			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>
		
			
			<? $email_helper->contentP(); ?>Welcome to CityBlast.  Your preferences have been received by our Social Experts, and we'll get started keeping your 
			profiles fresh and up-to-date right away.  Congrats for taking action and improving your real estate business!</p>
	
			<? $email_helper->contentP(); ?>You've now got your account set up, but you may want to log in and add a few things, like your Facebook business page, 
			Twitter or LinkedIn profiles.  Not to worry - it's all included!  Simply click the button below and it will take you straight to your Settings page, where 
			you can update and tweak to your heart's delight.</p>
			
			
		<? $email_helper->contentP(); ?>Also, you can always feel free to call us if you're having any trouble, or just want to chat - 1-888-712-7888.</p>
		
		
<? $email_helper->contentP(); ?>P.S. Did you know that <span style="background-color: #ffff00;">you can reach thousands of potential buyers 
	by "<a href="http://www.cityblast.com/blog/sell-your-listing-faster-unlock-the-secret-of-social-media-sharing/">Blasting</a>" your listings</span> to the CityBlast Social Agent Network?  Best of all, it's absolutely FREE as part of your CityBlast membership.</p>
	
<? $email_helper->contentP(); ?>Find out more about <a href="http://www.cityblast.com/index/how-blasting-works">Blasting your listing</a> here!</p>
			
			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
	
				<a href="<?=APP_URL;?>/member/authenticate" style="<? $email_helper->actionButtonStyle();?>">Add or Adjust Your Profiles</a>
			</p>   
	    
	       			
		</div>
	
	</div>
		


<? include "email-footer-NEW.html.php"; ?>
