<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>
	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>DON'T STOP NOW! </h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>

			<? $email_helper->contentH1(); ?>Ah Nuts, Your 7-Day Trial Is Over.</h1>
	
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>

			
			<? $email_helper->contentP(); ?>Over the past 7 days, our Social Experts have been keeping your online profiles fresh and up-to-date with great lead-generating content.</p>
			
			<? $email_helper->contentP(); ?>Keep up the great momentum you've built with your online network.</p>

			<? $email_helper->contentP(); ?>Simply log in and add your payment info to extend your trial to a full 14 days!  You will NOT be charged anything now.</p>
		
			<? $email_helper->contentP(); ?>Plus, remember to secure your 15% lifetime discount by locking-in promo code "SUCCESS" while you're there!</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/payment?rebill=1" style="<? $email_helper->actionButtonStyle();?>">Unlock My 14-Day Trial!</a>
			</p>    
	    
		</div>

		
	</div>

<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>