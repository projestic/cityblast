<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>

	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>WELCOME TO PROFESSIONAL SOCIAL MANAGEMENT </h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>

 			<? $email_helper->contentH1(); ?>Your 7 Day <u>FREE</u> Trial Has Begun.</h1>
	
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>!</p>

			<? $email_helper->contentP(); ?>Welcome to <a href="<?=APP_URL;?>" <? $email_helper->linkStyle();?>><?=COMPANY_NAME;?></a>.  
			Your marketing preferences have been successfully received, and the industry's top <u>Experts</u> are already getting started planning 
			your online presence and keeping you fresh and up-to-date.</p>
			
			<? $email_helper->contentP(); ?>Congrats for taking action and improving your business!  We're going to get started posting to your accounts right away</p>
			
			<? $email_helper->contentP(); ?><strong>However, we noticed that you didn't quite finish the registration process, and wanted to make sure you saw that we have 
			a promotion on now. Getting your full trial and savings locked-in is easy.</strong></p>
		
			<? $email_helper->contentP(); ?>Simply head to your <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>Dashboard</a> 
			and lock in your full trial by adding your payment info.  You will NOT be billed now.  This will also ensure you can take advantage of our 
			current promotion:  you'll get 15% off FOR LIFE when you use the promo code "SUCCESS" when completing your registration.  Lock it in!</p> 
			
			<? $email_helper->contentP(); ?>
			<div style="text-align:center; background-color:#e2e2e2; margin: 0 12px; padding: 4px; font-size: 16px;">
				PROMO CODE: <strong>SUCCESS</strong>
			</div>
			</p> 
			
			<? $email_helper->contentP(); ?>Also, remember you can always feel free to call us if you're having any trouble, or just want to chat <strong><?=TOLL_FREE;?></strong>.</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Unlock My 14-Day Trial!</a>
			</p>   
	    
		</div> 

	</div>
	
<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>