<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>
	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>WE WANT YOUR FEEDBACK</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>
 			<? $email_helper->contentH1(); ?>Is Your Social Expert Everything You Hoped For?</h1>
	
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>

			<? $email_helper->contentP(); ?>At MyEventVoice, we're always striving to be better. Quite frankly, that's the reason we're the biggest Social Media Marketing company around.</p>
			
			<? $email_helper->contentP(); ?>And that's because we listen and improve based on your valuable feedback.</p>

			<? $email_helper->contentP(); ?>We want you to get the absolute most out of your 7-Day Trial.</p>
			
			<? $email_helper->contentP(); ?>And in order to do that, we're asking you to help your Expert get even better by answering 3 simple questions about their job so far.</i>.
				
			<? $email_helper->contentP(); ?>Tell us what you think, and we'll give you an extra 7 days of <?=COMPANY_NAME;?>, just to say 'thanks'.</p>
			
			<? $email_helper->contentP(); ?>No tricks. No gimmicks. No strings attached.</p>
		
			<? $email_helper->contentP(); ?>Thanks for helping us make MyEventVoice the best in the business, and telling us how we can be even better.</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Unlock My 14-Day Trial!</a>
			</p> 
		</div> 

	</div>

<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>