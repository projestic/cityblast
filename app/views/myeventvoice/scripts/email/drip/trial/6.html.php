<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>WE RESPECT YOUR LEADS</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>The New Way:  Your Wall, Your Lead.</h1>
	
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>

			
			<? $email_helper->contentP(); ?>Are you enjoying the piece of mind created by <a href="<?=APP_URL;?>/blog/introducing-the-social-media-revolution/" <? $email_helper->linkStyle();?>>CityBlast's</a> patented
				<a href="<?=APP_URL;?>/blog/introducing-the-social-media-revolution/" <? $email_helper->linkStyle();?>>Never-Lose-A-Lead</a> technology?</p>
			
			<? $email_helper->contentP(); ?> Well, if you aren't, you should probably <i>read on</i>.</p>
	
			<? $email_helper->contentP(); ?><span style="background-color: #ffff00;">We've <u>revolutionized</u> Social Media Marketing in such a <b>RADICAL</b> way</span>, that we've spent the past 
				year and <u>hundreds of thousands of dollars</u> <i>patenting</i> our processes.</p>
		
			<? $email_helper->contentP(); ?>Best of all, <span style="background-color: #ffff00;"><u><i>you reap all the benefits</i></u></span>, because it's 100% included with every 
				<a href="<?=APP_URL;?>/blog/introducing-the-social-media-revolution/" <? $email_helper->linkStyle();?>><?=COMPANY_NAME;?></a> membership.</p>

			
			<? $email_helper->contentP(); ?>Find out how <i>we respect your leads</i> with this and other key 
				<a href="<?=APP_URL;?>/blog/introducing-the-social-media-revolution/" <? $email_helper->linkStyle();?>><?=COMPANY_NAME;?>-only innovations</a> at our blog.</p>
			
						

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/blog/introducing-the-social-media-revolution/" style="<? $email_helper->actionButtonStyle();?>">Learn More Now!</a>
			</p>   
	    
		</div>  		

		
	</div>	
	


<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>