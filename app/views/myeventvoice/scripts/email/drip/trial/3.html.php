<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>MAXIMIZE THE POWER OF YOUR FREE TRIAL</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Content Is King.</h1>
	
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>

			
			<? $email_helper->contentP(); ?>Many years ago, Bill Gates uttered that world famous phrase: "Content is King".</p>

			<? $email_helper->contentP(); ?>Now more than ever, providing valuable information and tips to your clients and 
			prospects is the only way to properly market via social media without annoying your friends and followers.</p>
			
			<? $email_helper->contentP(); ?>Just ask Seth Godin who said: "Content Marketing is all the marketing that's left."  
			At <?=COMPANY_NAME;?>, your social media Expert takes those words to heart.</p> 
			
			<? $email_helper->contentP(); ?>Quite frankly, we didn't become the world's largest social media managers by 
			accident.  It's because we create a perfectly-tailored and customized feel by offering more types of great 
			content than anyone.  Period.  You tell us how you want your page to look, and we'll do the rest.</p>
		
			<? $email_helper->contentP(); ?>So go ahead and lock-in your fully-customizable trial now, and secure your 
			lifetime 15% discount by entering promo code "SUCCESS" by clicking below and adding your payment details.</p>
			
			<? $email_helper->contentP(); ?>
			<div style="text-align:center; background-color:#e2e2e2; margin: 0 12px; padding: 4px; font-size: 16px;">
				PROMO CODE: <strong>SUCCESS</strong>
			</div>
			</p> 
			
			<? $email_helper->contentP(); ?>Here's to social media success!</p> 
			
			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/payment?rebill=1" style="<? $email_helper->actionButtonStyle();?>">Unlock My 14-Day Trial!</a>
			</p>  
	    
		</div>  
		
	</div>
	



<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>