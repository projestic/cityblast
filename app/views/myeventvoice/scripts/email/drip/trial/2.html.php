<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>THE POWER OF CONSISTENCY</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>
			<? $email_helper->contentH1(); ?>Consistency and Trust.</h1>
	
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>

			<? $email_helper->contentP(); ?>If your social media marketing is suffering from inconsistency you have a 
			business flaw that has already lost you tens of thousands of dollars this year alone.  Did you know that 
			every time you give out your business card, potential clients go home and google you?  </p>   
					
			<? $email_helper->contentP(); ?>What are they finding when the click on the Facebook, Twitter and LinkedIn 
			links that appear? </p>
				
			<? $email_helper->contentP(); ?>If you've been inconsistent in the past, you're not alone.  But there's 
			good news - you can have an always up-to-date and professional presence right now. </p>

			<? $email_helper->contentP(); ?>If you've been too busy to keep on top of your social media posts, we can help.</p>

			<? $email_helper->contentP(); ?>Activate your full trial (and secure your lifetime discount with promo code 
			"SUCCESS") now, and have our Social Experts take care of it all for you.</p>
			
			<? $email_helper->contentP(); ?>
			<div style="text-align:center; background-color:#e2e2e2; margin: 0 12px; padding: 4px; font-size: 16px;">
				PROMO CODE: <strong>SUCCESS</strong>
			</div>
			</p> 
			
			<? $email_helper->contentP(); ?>BONUS SAVINGS: Upgrade to our ANNUAL plan and enjoy huge further savings!</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Unlock My 14-Day Trial!</a>
			</p>   
	    
		</div> 
	</div>	

<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>