<div class="row-fluid">
	
	<div class="row-fluid">


		<div class="bannerContainer" id="main-slider">
	
	
			<ul class="bxslider">
	
				<li style="display: block">
	
					<a href="//www.youtube.com/embed/h1vv8D_EZJI?rel=0&autoplay=1" id="explain_video" class="cboxElement">
						<img src="/images/myeventvoice-banner-bg.png" />
						<span class="bannerInfo">
							<span class="bannerTitle">Our Certified and Dedicated Social Experts Are Ready To Turn
							Your Fanpage Into An EXPLOSIVE Growth Engine!</span>
							<span class="playButton"><span>Learn More</span></span>
						</span>
						<span class="bannerBottom">
							<h3 style="font-size: 18px;">Are You Sick of Updating Your <u>Event Planning</u> Fanpage?</h3>
							<p>A <?=COMPANY_NAME;?> <i>Social Expert</i> can help, see how.</p>
						</span>
					</a>
					
				</li>
				<li style="display: block" class="slide-2">
	
					<a href="//www.youtube.com/embed/h1vv8D_EZJI?rel=0&autoplay=1" id="explain_video" class="cboxElement">
						<img src="/images/myeventvoiceslide-2.png" />
						<span class="bannerInfo">
							<span class="bannerTitle">Sign Up Now</span>
							<span class="bannerSubtitle">For our <span>FREE</span> webinar &nbsp; learn the power of Inbound Marketing!</span>
							<span class="playButton">Click Here To Sign Up</span>
						</span>
						<span class="bannerBottom">
							<h3>Top Pro’s Use Social Media Marketing To Turn Friends Into Clients</h3>
							<p>Learn The Secrets They Don’t Want YOU To Know About Social Media Marketing</p>
						</span>
					</a>
					
				</li>




			</ul>


			<style type="text/css">
				/* DIRECTION CONTROLS (NEXT / PREV) */
				/***
				.bx-wrapper .bx-prev {
					left: 10px;
					background: url(/images/new-images/slider-controls.png) no-repeat;
				}
	
				.bx-wrapper .bx-next {
					right: 10px;
					background: url(/images/new-images/slider-controls.png) no-repeat -52px 0;
				} ***/
				
				.bx-wrapper .bx-prev:hover {
					background-position: 0 0;
				}
				.bx-wrapper .bx-next:hover {
					background-position: -52px 0px;
				}
				.bx-wrapper .bx-controls-direction a {
					width: 50px;
					height: 50px;
				}
				
				/***
				#main-slider .bx-controls-direction {
				    display: block;
				    opacity: .9;
				}
				#main-slider:hover .bx-controls-direction {
				    display: block;
				    opacity: 1;
				}***/

			</style>
			<script type="text/javascript">
				var slider = $('.bxslider').bxSlider({
					adaptiveHeight: false,
					mode: 'fade',
					pager: 1,
					controls: true,
					infiniteLoop: 1,
					auto: true,
				});
			</script>

		</div>

	</div>


	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

	</script>
	
	<?/*<a id="we-are-hiring-tab" href="/jobs/index" title="We are hiring"></a>*/ ?>
	
	<div class="whatIsCBHolder contentBox">
		<h2>What Is <?=COMPANY_NAME;?>?</h2>
		
		<p class="firstLine"><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> is your team of personal Social Experts that keeps your <a href="//www.facebook.com">Facebook</a>, <a href="//www.twitter.com">Twitter</a> and <a href="//www.linkedin.com">LinkedIn</a> up to date and professional.</p>
	
			
		<div class="fbEmbedPost">
			<div class="fbPostBefore"><center>We do this on your <u>Facebook</u> fanpage</center></div>
			
			<!--div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=252180234962309&amp;set=a.408573635884312.100242.153723091369369&amp;type=1" data-width="440"><div class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/photo.php?fbid=600436866697987&amp;set=a.408573635884312.100242.153723091369369&amp;type=1">Post</a> by <a href="https://www.facebook.com/pages/Myeventvoice/249748248538841"><?=COMPANY_NAME;?></a>.</div></div-->

			<?=$this->fbEmbedPost(
				'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1.0-1/c1.0.50.50/p50x50/1618593_252180534962279_1546145875_s.jpg',
				'MyEventVoice',
				'Internet/Software',
				"Experts are all weighing in on what the hottest trend of 2014 weddings will be and the consensus is romance. Think vintage, classic, and chivalry. Picture following the footsteps of Great Gatsby or Downton Abbey. In this post, we’re highlighting a number of wedding trends you can expect to see in 2014.",
				'http://blog.wedbuddy.com/2014/02/18/wedding-trends-were-sure-to-see-in-2014/',
				'https://scontent-a-fra.xx.fbcdn.net/hphotos-frc1/t1.0-9/s417x417/1920557_252180234962309_572738848_n.jpg',
				'February 24'
			);?>

			<?/*<div class="fb-post" data-href="https://www.facebook.com/CityBlastInfo/posts/10100649384666441?stream_ref=10" data-width="440"></div>*/?>

			
			<div class="fbPostAfter">
				<p>So that you don’t have to!</p>
				<p class="last">Yes!<br>We can help <br>with <u>Twitter</u> and<br><u>LinkedIn</u> too.</p>
			</div>
		</div>
		<p class="secondLine"><a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> enables you to spend time where it matters most, with clients!</p> 
		
		<p class="secondLine">Our team of Social Experts research and post the latest exciting event industry news, fun articles and videos, and other engaging content that your friends and followers actually <i>want</i> to read!</p>
		
		  <p class="secondLine">
			We keep your social media presence up-to-date and generating leads, so you look professional and current when clients search for you online.  You can get back to spending time 
			where you should, instead of managing your social media!</p>
		<p class="bold" style="font-size: 20px; text-decoration: underline; margin-top: 25px">Welcome to <?=COMPANY_NAME;?>.</p>

	</div>
	
	
	
	<?/****
	<div class="contentBox whoUsesCB">
		<h2>Who Uses <?=COMPANY_NAME;?>?</h2>
		<h3>Used And Trusted By Thousands of Agents Every Day!</h3>
		

		<? echo $this->FacePile(); ?>
		
		
		<div class="testimonialsHolder clearfix">
			<h3>And Here Is What They Have To Say:</h3>
			<div class="colLeft">
				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/ingrid-brunsch.jpeg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>Thanks so much! I must tell you I really to appreciate the articles that appear on my Facebook and Twitter page 
							daily - saves me so much time not having to hunt around trying to find appropriate information to post. I have also taken advantage 
							of blasting my listings and am not sure if it is working in terms of selling them any faster - but, it makes a wonderful tool 
							when I speak to my clients about my marketing tools - I appear to be very tech savvy - if they only knew! <i>~Ingrid Brunsch</i></p>
					</div>
				</div>


				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/leila-talibova.jpg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>My friends and previous colleagues noticed my Facebook updates right away. In only my first week using CityBlast, I received a call from a family 
							friend who said she’d seen my Facebook and wanted to list their home with me. The experts are awesome! <i>~Leila Talibova</i></p>
					</div>
				</div>

				

				

				
				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/sunny-dehghan.jpg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>I have a lot of realtor friends and my biggest concern was that I would be getting the exact same posts as everyone else. Well, I've been a member for over 8 months and so far so good! To be honest, it does
							everything as advertised and my clients think all I do is stay on top of industry news. Even my parents said something about my Facebook posts! <i>~Sunny Dehghan</i></p>
					</div>
				</div>
				



			</div>
			<div class="colRight">
				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/sheree-cerqua.jpg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>I use CityBlast to help me market. Not only are they the heart of my online platform, but also help me to sell my clients’ listings quickly, and to achieve 
							excellent prices. CityBlast is an incredible service for both beginners and top agents, and I highly recommend them. <i>~Sheree Cerqua</i></p>
					</div>
				</div>


				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/ashley-gollogly.jpg" alt=""></span>
					</div>
					<div class="textHolder">
						<p>I'm pretty head strong when it comes to social media. I knew what I wanted to post on my wall, the problem was finding the time to do it every day. Thankfully I found CityBlast. 
							Now I can focus on the things that require my personal attention and I know that my Social Media marketing is being taken care of by dedicated professionals. P.S. I went 
							on vacation in January and my Social Expert was posting even when I was away. I came back to find 2 new leads in my Facebook inbox. Shhhh! Don't tell! <i>~Ashley Gollogly</i></p>
					</div>
				</div>


				<div class="item clearfix">
					<div class="thumbHolder">
						<span><img src="/images/valentina-pasquini.jpg" alt=""></span>
					</div>
					<div class="textHolder">

						<p>I have a young demanding family and fledgling business real estate business. Between diaper changes and open houses, quite frankly, I just couldn't find the time to add yet another thing to my to-do list. 
							Even though I knew that Social Media was the most talked about thing as Re/Max Kickstart, I just couldn't find the time to do it myself. CityBlast completely turned around my social media presence and now I have one less thing
							to worry about. <i>~Valentina Pasquini</i></p>

					</div>
				</div>



			</div>
		</div>
		<h3>Some of Our Partners</h3>
		<ul class="partnersList">
			<li><a target="_blank" href="http://www.royallepage.ca/"><img src="/images/lp_logos/royal_lepage.png" width="250"></a></li>
			<li><a target="_blank" href="http://blog.lwolf.com/partners/news/lone-wolf-invests-in-cityblast-81759/"><img src="/images/lp_logos/lone_wolf.png" width="250"></a></li>
			<li><a target="_blank" href="http://retechulous.com/"><img src="/images/lp_logos/retechulous.png" width="250"></a></li>
			
			<li><a target="_blank" href="http://www.seevirtual360.com/"><img src="/images/lp_logos/see_virtual.png" width="250"></a></li>
			
		</ul>
		
		<h3 style="margin-top: 20px;">As Featured On</h3>
		<ul class="partnersList" style="margin-top: 10px;">	
			<li><a target="_blank" href="http://www.techvibes.com/blog/cityblast-enables-canadian-real-estate-agents-to-build-their-business-through-social-media-2012-08-03"><img src="/images/lp_logos/techvibes.png" width="250"></a></li>
			<li><a target="_blank" href="http://www.canadianrealestatemagazine.ca"><img src="/images/lp_logos/real_estate_wealth.png"></a></li>
			<li><a target="_blank" href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/"><img src="/images/lp_logos/globe_and_mail.png" width="250"></a></li>
			
			<li><a target="_blank" href="http://www.theglobeandmail.com/life/home-and-garden/real-estate/real-estate-agents-find-home-tweet-home-in-social-media/article5087300/"><img src="/images/lp_logos/real_estate_talk_show.png" width="250"></a></li>
			
		</ul>
	</div> <!-- end whoUsesCB -->
	****/ ?>
	
	
	<div class="contentBox socialMedia">
		<h2>Event Plan and Social Media: A Perfect Match</h2>
		<div class="icons-holder clearfix">
			<div>
				<img src="/images/icon-1.png" alt="">
				<p>Percentage of clients who research you online</p>
			</div>
			<div>
				<img src="/images/icon-2.png" alt="">
				<p>Amount of time spent on Facebook vs any other site</p>
			</div>
			<div>
				<img src="/images/myeventvoice/icon-3.png" alt="">
				<p>Hours/month recommended to spend online marketing</p>
			</div>
		</div>

		<p class="description">
			Your prospects and clients are <i>Googling or Facebooking you every time you hand out a business card</i>.  Is your social media up to snuff?  Should <u>you</u> be spending over 40 hours every month on your social media marketing?  Let the Experts keep you polished and up-to-date, and get back to doing what you do best.
		</p>

			
		</p>
		<a href="/member/index" class="uiButtonNew home"><?=COMPANY_NAME;?> and Me: A Perfect Match</a>
	</div>
	<div class="contentBox contentType">
		<h2>What Type of Content Do You Post?</h2>
		<p class="description">At <a href="<?=APP_URL;?>"><?=COMPANY_NAME;?></a> we only post the most powerful type of marketing content called Inbound Marketing. <a href="">Inbound marketing</a>, also known as <a href="">pull marketing</a> or <a href="">permission marketing</a>, is the use of interesting content to “earn your way” into a sales lead as opposed to push marketing where you “buy, beg or bug” your way into a sales lead.</p>
		<div class="fbPostsHolder">
			
			
			<?php /*<h3>What Does Inbound Marketing Look Like?</h3> */?>
			
			
			<?php /*
			<div class="fbPostsAfterText arrowsAbove" style="margin-top: 30px; margin-bottom: 25px;">
				Our Social Experts can make your Fanpage look like this!</div>
			**/ ?>

			<div class="fbPostsAfterText arrowsAbove" style="margin-top: 30px; margin-bottom: 25px;">
				<span class="fb-like" data-href="https://www.facebook.com/pages/Myeventvoice/249748248538841" data-layout="button" data-action="like" data-show-faces="true" data-share="false" style="position: absolute; display: inline-block; line-height: 0.7em; margin: 10px 0 0 50px"></span>
				Click <span style="display: inline-block; width:50px"></span> if you want your Fanpage to look like this!</div>
			
			
			<img src="/images/fanpage-sample-banner-mev.png" alt="Your New Fanpage!" style="margin-left: -7px;" />
			
			
			<!--div class="fbPosts clearfix">
				<div class="fbEmbedPost">
					<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=252184934961839&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
				</div>
							
				<div class="fbEmbedPost">
					<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=252213664958966&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
				</div>

				<div class="clear"></div>

				<div class="fbEmbedPost">
					<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=252310878282578&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
				</div>	
				
				<div class="fbEmbedPost">
					<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=252189381628061&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
				</div>
			</div-->

			<div class="fbPosts clearfix">
				
				<?=$this->fbEmbedPost(
					'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1.0-1/c1.0.50.50/p50x50/1618593_252180534962279_1546145875_s.jpg',
					'MyEventVoice',
					'Internet/Software',
					"Regal is all in the details! Create a sophisticated reception style by layering shimmery and feminine extras on a foundation of crisp whites and gray. See nine ways to capture a modern regal style at your reception:",
					'http://blog.theknot.com/2014/02/19/9-ways-to-pull-off-a-modern-regal-wedding-style',
					'https://scontent-b-fra.xx.fbcdn.net/hphotos-prn1/t1.0-9/p370x247/1958250_252184934961839_736065659_n.jpg',
					'February 24'
				);?>

				<?=$this->fbEmbedPost(
					'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1.0-1/c1.0.50.50/p50x50/1618593_252180534962279_1546145875_s.jpg',
					'MyEventVoice',
					'Internet/Software',
					"Whether they're for your menu, guest book or centerpiece, these inspiring chalkboard signs are sure to impress guests!",
					'http://www.weddingwire.com/wedding-photos/reception/36-ways-to-chalk-up-your-reception/i/9e6af3a1025f2b44-0e97be92b016d157',
					'https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-ash3/t1.0-9/p370x247/1604962_252213664958966_808572405_n.jpg',
					'February 24'
				);?>

				<div class="clear"></div>

				<?=$this->fbEmbedPost(
					'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1.0-1/c1.0.50.50/p50x50/1618593_252180534962279_1546145875_s.jpg',
					'MyEventVoice',
					'Internet/Software',
					"Often times brides choose to make their wedding shoes their something blue! It’s a fun way to add a pop of color to your wedding attire and there are plenty of blue shoes out there in different styles and shades to be able to find one that is perfect for your wedding day.",
					'http://www.weddingwindow.com/blog/blue-wedding-shoes',
					'https://scontent-b-fra.xx.fbcdn.net/hphotos-prn2/t1.0-9/s370x247/1779126_252310878282578_1719837125_n.jpg',
					'February 24'
				);?>

				<?=$this->fbEmbedPost(
					'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1.0-1/c1.0.50.50/p50x50/1618593_252180534962279_1546145875_s.jpg',
					'MyEventVoice',
					'Internet/Software',
					"How to be a good Bridesmaid -- There are the obvious things like helping the bride go to the bathroom at the wedding, attending all the showers and bachelorette parties, making sure she eats the day of the wedding, but here are some overlooked pointers that in my mind are ABSOLUTE MUSTS:",
					'http://www.nearlyweds.com/blog/2010/07/07/how-to-be-a-good-bridesmaid',
					'https://scontent-a-fra.xx.fbcdn.net/hphotos-prn1/t1.0-9/p370x247/1620623_252189381628061_1045624566_n.jpg',
					'February 24'
				);?>
				
			</div>


			<p class="fbPostsAfterText arrowsAside">Do you want your Facebook, Twitter and LinkedIn to look like this?</p>


			<a href="#" class="uiButtonNew small" id="showconentlib">View Full Content Library</a>
			
			
			<div id="contentlib_div" style="display: none;">
				<div class="fbPosts clearfix">
					<?=$this->fbEmbedPost(
						'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1.0-1/c1.0.50.50/p50x50/1618593_252180534962279_1546145875_s.jpg',
						'MyEventVoice',
						'Internet/Software',
						"Think you've got it all covered? Think again. We're betting there are at least a couple items on this list you missed.",
						'http://weddings.weddingchannel.com/wedding-planning-ideas/wedding-reception-ideas/slideshows/wedding-details-every-bride-forgets.aspx',
						'https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xpf1/t1.0-9/s403x403/1505571_252215934958739_1516366294_n.jpg',
						'February 24'
					);?>
					<?=$this->fbEmbedPost(
						'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1.0-1/c1.0.50.50/p50x50/1618593_252180534962279_1546145875_s.jpg',
						'MyEventVoice',
						'Internet/Software',
						"Have you ever considered a winter wedding? Snowy winter weddings always steal our hearts. Kristin and David’s wedding, of course, did the same. Here are our top 10 tricks to make your winter wedding memorable!",
						'http://weddings.about.com/od/traditionscolorsthemes/a/winterwedding.htm',
						'https://scontent-b.xx.fbcdn.net/hphotos-xpf1/t1.0-9/s403x403/1779253_252310258282640_378946288_n.jpg',
						'February 24'
					);?>
					<?/*<div class="fbEmbedPost">
						<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=252215934958739&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
					</div>
					<div class="fbEmbedPost">
						<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=252310258282640&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
					</div>*/?>
					<div class="clear"></div>
					<?=$this->fbEmbedPost(
						'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1.0-1/c1.0.50.50/p50x50/1618593_252180534962279_1546145875_s.jpg',
						'MyEventVoice',
						'Internet/Software',
						"As the month of love comes to an end, we wanted to keep the romance in the air with some of our favorite songs for your wedding. Here's our top 10:",
						'http://blog.weddingwire.com/index.php/weddings/the-most-romantic-love-songs-for-your-wedding-day',
						'https://scontent-a.xx.fbcdn.net/hphotos-xfa1/t1.0-9/s403x403/1920131_252214951625504_657125680_n.jpg',
						'February 24'
					);?>
					<?=$this->fbEmbedPost(
						'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1.0-1/c1.0.50.50/p50x50/1618593_252180534962279_1546145875_s.jpg',
						'MyEventVoice',
						'Internet/Software',
						"I’m all about finding affordable ways to make an impact with wedding decor. Creating DIY wedding projects like details and decorations with tissue and crepe paper is a great to accomplish this. Here are 10 great ideas for your special day:",
						'http://www.examiner.com/slideshow/wedding-diy-hand-dyed-tissue-paper-pom-poms#slide=1',
						'https://scontent-a.xx.fbcdn.net/hphotos-xfa1/t1.0-9/s403x403/1912279_252313111615688_1785960246_n.jpg',
						'February 24'
					);?>
					<?/*<div class="fbEmbedPost">
						<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=252214951625504&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
					</div>
					<div class="fbEmbedPost">
						<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=252313111615688&set=a.408573635884312.100242.153723091369369&type=1&stream_ref=10" data-width="395"></div>
					</div>*/?>
				</div>				
			</div>
			
		</div>
		<h3>How Often Is It Posted?</h3>
		<ul class="frequencyList">
			<li>
				<div>
					<p class="day">Sunday</p>
					<p class="number">1</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Monday</p>
					<p class="number">2</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Tuesday</p>
					<p class="number">3</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Wednesday</p>
					<p class="number">4</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Thursday</p>
					<p class="number">5</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Friday</p>
					<p class="number">6</p>
				</div>
			</li>
			<li>
				<div>
					<p class="day">Saturday</p>
					<p class="number">7</p>
				</div>
			</li>
		</ul>
		<p>You choose the frequency and the days you want us to post and we do the rest. It’s that easy!</p>
	</div> <!-- end contentType -->
	
	
	
	
	
	
	<?= $this->render('common/14-day-trial.html.php');?>

	
	<?= $this->render('common/chat-with-an-expert.html.php');?>	
	





	<div class="contentBox">

		<h2 style="margin-bottom: 35px; font-weight: normal; text-align:left">Get a <i>Social Expert</i> managing your social media for less than $2/day, and never miss a deal again</h2>

		<div class="strategyContainer" style="margin-top: 10px;">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-2.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">We'll plan and manage your lead generation strategy.</h3>
					Simply tell us the city where you work, and what kind of posts suit your style best.  Our <i>Social Experts</i> <u>do the rest</u> - planning your marketing, sourcing articles and videos, and posting them to your accounts.
				</div>
			</div>
		</div>

		<?/*
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-2.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">Your Social Media Expert plans your campaign.</h3>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac sem vel eros dapibus bibendum sed non ipsum. Sed eu lorem lectus.
				</div>
			</div>
		</div>*/ ?>

		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-3.png" />
				</span>
				<div class="media-body">
					<?/*<h3 class="media-heading">As often as you choose, your Social Expert consistently posts articles, videos & information.</h3>*/?>
					<h3 class="media-heading">As often as you choose, we post articles, videos & information.</h3>
					You also control how often your <i>Social Expert</i> will post.  Following your instructions, they'll <i>consistently</i> post updates to your Facebook, Twitter and/or LinkedIn accounts that make sure you look professional and up-to-date.
				</div>
			</div>
		</div>
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-4.png" />
				</span>
				<div class="media-body">
					<?/*<h3 class="media-heading">You respond to new leads, and follow your success in your dashboard.</h3>*/?>
					<h3 class="media-heading">Manage your leads and measure success in your dashboard.</h3>
					With your powerful, professional presence, you'll see <i>an increase in new leads</i> through your social media accounts.  You just follow up with new leads as they come in, and generate more new business.
				</div>
			</div>
		</div>
		<div class="clearfix">
			<div class="pull-left tryDemoTxt">
				<?/*<h3 style="font-size: 28px;">Get an Expert handling your business's social media.</h3>*/?>
				<h3 style="font-size: 28px;">Our <i>Social Experts</i> can help.</h3>
				<h4>Start your Free 14-Day Trial of <?=COMPANY_NAME;?>  <i>Social Experts</i> now.</h4>
			</div>
			<div class="pull-right">
				<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>
			</div>
		</div>
	</div>


</div>

<div style="display:none">
	<div id="gotcha_popup_div">
		<h1>Made You Click!</h1>
		<p style="margin-top:20px">You've just seen the power of inbound marketing for yourself!</p>
	</div>
</div>

<? /****** 		PARTNER LOGOS 		******/ 	?>
<? if(stristr(APPLICATION_ENV, "cityblast") || stristr(APPLICATION_ENV, "mortgagemingler")) : ?>
	<? echo $this->render('logos-responsive-small.html.php');?>
<? endif; ?>


<? /****** 		SPACER 		******/ 	?>
<div style="margin-bottom: 25px;">&nbsp;</div>
	


<script type="text/javascript">
	$(document).ready(function() 
	{
		
		$("#showconentlib").click(function(e)
		{
			e.preventDefault();

			$("#contentlib_div").slideToggle( "slow", function() {
				// Animation complete.
			});	
		})		

		$(".chatbutton").click(function(e)
		{
			e.preventDefault();	
			$zopim.livechat.window.show(); 
		})		

		$(".gotcha_popup").click(function(e)
		{
			e.preventDefault();
			$.colorbox({
				height: "200px",
				inline: true, 
				href: "#gotcha_popup_div"
			});
		});			
		
		
		$("#videopopup").click(function(e)
		{
			e.preventDefault();
			$.colorbox({height: "500px",inline:true, href:"#sample_popup",onClosed:function(){ $('#sample_popup').empty(); }});
		});
	});
</script>