<?
class Zend_View_Helper_Email extends Zend_View_Helper_Abstract
{

	public function email()
	{
		return $this;
	}
	
	public function openBox()
	{
		echo '
				<div style="
				border: 1px solid #C7C9C8;
	    			border-radius: 5px 5px 5px 5px;
	    			height: 100%; width: 600px;
	    			margin-top: 20px;
					padding-bottom: 15px;">	';	
		
		//return $this;
	}

	public function openCap()
	{
		
		echo '<div style=\'
					background: -ms-linear-gradient(#F9F9F9, #EEEEEE) repeat scroll 0 0 transparent;
					background: -moz-linear-gradient(#F9F9F9, #EEEEEE) repeat scroll 0 0 transparent;
					background: -webkit-linear-gradient(#F9F9F9, #EEEEEE) repeat scroll 0 0 transparent;
					background: -o-linear-gradient(#F9F9F9, #EEEEEE) repeat scroll 0 0 transparent;
					background: linear-gradient(#F9F9F9, #EEEEEE) repeat scroll 0 0 transparent;
				    	border-bottom: 1px solid #C7C9C8;
				    	border-radius: 5px 5px 0 0;
				    	color: #7B7B7B;
				    	font-family: "myriad-pro-n4","myriad-pro","Helvetica Neue",Helvetica,Arial,sans-serif;
				    	line-height: 19px;
				    	padding: 0px;
				    	text-transform: uppercase;
				    	 width: 600px;
				    	\'
	    				>';
	    						
		}

	public function openH4()
	{
		echo '<h4
				style=\'    
						font-family: "myriad-pro-n6","myriad-pro","Helvetica Neue",Helvetica,Arial,sans-serif !important;
						font-weight: 600;
						letter-spacing: -0.01em;
					   	color: #7B7B7B;
						font-size: 18px;
						margin: 0px 15px;
						\'
				>';	
		
	}

	public function openContentDiv()
	{	
		echo '<div style="padding: 15px 0px 0px;">';

	}

	public function buzzscoreH1()
	{
		echo '<h1 style="text-align: center; font-family:\'Myriad Pro\', Arial; color: #7B7B7B; font-size: 126px; font-weight: 600; letter-spacing: -0.01em; vertical-align: baseline;margin: 16px 0 5px;line-height: 1em;  padding: 0;">';
	}
	
	
	public function contentH1()
	{
		echo '<h1 style="font-family:\'Myriad Pro\', Arial; color: #333333; font-size: 26px; font-weight: 600; letter-spacing: -0.01em; vertical-align: baseline;margin: 16px 15px 25px;line-height: 1em;  padding: 0;">';
	}

	public function contentH2()
	{
		echo '<h3 style="color: #7B7B7B; font-family:\'Myriad Pro\', Arial; font-size: 22px; font-weight: 600; letter-spacing: -0.01em; vertical-align: baseline;margin: 16px 15px 5px;line-height: 1em;  padding: 0;">';
	}

	public function contentH3()
	{
		echo '<h3 style="color: #7B7B7B; font-family:\'Myriad Pro\', Arial; font-size: 18px; font-weight: 600; letter-spacing: -0.01em; vertical-align: baseline;margin: 16px 15px 5px;line-height: 1em;  padding: 0;">';
	}
	
	public function contentP($style=null)
	{
		echo '<p style=" font-family:Arial; line-height: 1.2em; font-size: 16px; color: #252525; margin: 5px 15px 25px; padding: 0; '.$style.'">';
	}


	public function orderedListStyle($style=null)
	{
		echo ' style="line-height: 1.2em; font-size: 16px; color: #252525; '.$style.'" ';
	}
	
	public function tableCell($style = null)
	{
		echo ' style=" font-family:Arial; line-height: 1.2em; font-size: 16px; color: #252525; '.$style.'" ';
	}
		
	public function actionButtonStyle()
	{

		echo '
			display: inline-block !important;
			padding: 8px;
			background: none repeat scroll 0 0 #004ECC;
			box-shadow: none;
			color: #FFFFFF;
			font-size: 16px;
			font-style: normal;
			text-shadow: -1px -1px 0 #18458C;   		
		    	border: medium none !important;
		    	font-size: 14px !important;    

		   
		     border: medium none !important;
		    	border-radius: 5px 5px 5px 5px;  		
		   	cursor: pointer;
		   	font-family: myriad-pro,Helvetica,Arial,Verdana,sans-serif;
		   	font-weight: 600;
		    	letter-spacing: 0.02em;
		    	outline: medium none !important;
		    	text-align: center;
		    	text-decoration: none !important;';	
			
	}
	
	public function linkStyle()
	{
		echo ' style="color: #3D6DCC; text-decoration: underline;" ';
	}

	public function footerLinkStyle()
	{
		echo ' style="color: #3D6DCC; text-decoration: none;" ';
	}
}

?>