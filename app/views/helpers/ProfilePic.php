<?
class Zend_View_Helper_ProfilePic extends Zend_View_Helper_Abstract
{

	public function profilePic($type = 'small', $member = null)
	{

		if (false === ($member instanceOf Member)) {
			if ($_SESSION['member'] instanceOf Member) {
				$member = $_SESSION['member'];
			}  else {
				return false;
			}
		}

		if (empty($member->photo)) {
			if ($type == 'original') {
				$src = "https://graph.facebook.com/". $member->uid . "/picture?width=9999&height=9999&redirect=true";
			} else {
				// translate 'small' type to Facebook 'square' type
				if ($type == 'small') $type = 'square';
				$src = "https://graph.facebook.com/". $member->uid . "/picture?type=$type&redirect=true";
			}
		} else {
			$src = CDN_URL . $member->photo;
			$srcparts = pathinfo($src);
			if ($type == 'small') {
				$src = $srcparts['dirname'] . '/' . $srcparts['filename'] . '_50x50.' . $srcparts['extension'];
			} elseif ($type == 'large') {
				$src = $srcparts['dirname'] . '/' . $srcparts['filename'] . '_200x200.' . $srcparts['extension'];
			}
		}
		
		return $src;

	}

}
