<?
class Zend_View_Helper_JQuery extends Zend_View_Helper_Abstract
{
  public function jquery()
	{
	  return $this;
	}
	
	public function load($replacing, $url, $options = array())
	{
	  return "$('$replacing').load('$url');";
	}
	
	public function link_and_load_remote($text, $replacing, $url, $options = array())
	{
	  return "<a href='#' onclick='{$this->load($replacing, $url, $options)} return false;'>$text</a>";
	}
}
?>