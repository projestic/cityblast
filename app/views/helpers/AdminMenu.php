<?
class Zend_View_Helper_AdminMenu extends Zend_View_Helper_Abstract
{

	public function AdminMenu($menu) {
		
		$content = '';
		foreach ($menu as $link => $menu) {
			$content .= $this->makeLi($link, $menu);
		}
		return $content;
	}
	
	protected function makeLi($link, $menu) {
		if ($this->view->canAccess($link) == false) return '';
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$controller = $request->getControllerName();
		$action = $request->getActionName();
		$link = rtrim($link, '/');
		$class = '';
		if (strpos($link, "/$controller/" . str_replace('_', '-', $action)) === 0 || $action == 'index' && $link == "/$controller") {
			$class = "active";
		}
		$id = '';
		if (isset($menu['id'])) $id = "id=\"{$menu['id']}\"";
		$linkHTML = "<a $id href=\"$link\">{$menu['name']}</a>";
		$content = '';
		if (isset($menu['children']) && is_array($menu['children'])) {
			foreach ($menu['children'] as $link => $menu) {
				$content .= $this->makeLi($link, $menu);
			}
		}
		if (!$class && stristr($content, 'class="active"')) {
			$class = "active";
		}
		if ($class) $class = "class=\"$class\"";
		if (strlen($content)) $content = "<ul>$content</ul>";
		return "<li $class>$linkHTML $content</li>";
	}

}
?>