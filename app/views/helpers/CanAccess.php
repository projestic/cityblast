<?php

class Zend_View_Helper_CanAccess extends Zend_View_Helper_Abstract
{
	public function CanAccess($url) 
	{
		$request 			= new Zend_Controller_Request_Http(APP_URL . $url);
		$frontController 	= Zend_Controller_Front::getInstance();
		$router 			= $frontController->getRouter();
		
		$router->route($request);
		
		$params = $router->getCurrentRoute()->match($url);
		$request->setParams($params);
		
		$access = new Blastit_Access_Cityblast;
		return $access->isPermitted(
			$request->getActionName(),
			$request->getControllerName(),
			$request->getModuleName()
		);
	}
}
