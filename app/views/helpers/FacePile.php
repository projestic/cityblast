<?
class Zend_View_Helper_FacePile extends Zend_View_Helper_Abstract
{

	public function FacePile($type = 'responsive') {

		$sql = "SELECT CONCAT(first_name, ' ', last_name) as full_name, uid
			FROM `member`
			WHERE first_name IS NOT NULL AND last_name IS NOT NULL AND uid IS NOT NULL
			AND id NOT IN (1183, 1184, 1003920, 1002886, 1002766, 1002518, 1002418, 1002091, 1002070, 1002018, 1000949) 	
			AND uid != ''
			ORDER BY RAND()
			LIMIT 45";

		$memberData = Member::find_by_sql($sql);
		
		$view = new Zend_View();
		$view->addScriptPath(APPLICATION_PATH . '/views/common/scripts/');
		$view->member_list = $memberData;
		if ($type == 'standard') {
			return $view->render('facepile.html.php');
		} else {
			return $view->render('facepile-' . $type . '.html.php');
		}

	}

}