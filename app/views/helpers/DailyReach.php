<?
class Zend_View_Helper_DailyReach extends Zend_View_Helper_Abstract
{

	public function dailyReach()
	{
		//FB Count
		$query = "select sum(facebook_friend_count) as facebook_count from member where facebook_publish_flag=1 and status='active'";
		$result = Member::connection()->query($query)->fetch();
		$facebook_count = $result['facebook_count'];	
								
		//Twitter Count
		$query = "select sum(twitter_followers) as twitter_count from member where twitter_publish_flag=1 and status='active'";
		$result = Member::connection()->query($query)->fetch();
		$twitter_count = $result['twitter_count'];	

		//Linkedin Count
		$query = "select sum(linkedin_friends_count) as linkedin_count from member where linkedin_publish_flag=1 and status='active'";
		$result = Member::connection()->query($query)->fetch();
		$linkedin_count = $result['linkedin_count'];

		//Fanpages Count
		$query = "select sum(likes) as fanpages_count from fanpage";
		$result = Member::connection()->query($query)->fetch();
		$fanpages_count = $result['fanpages_count'];

		return $twitter_count + $facebook_count + $linkedin_count + $fanpages_count;

	}

}
