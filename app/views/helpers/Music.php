<?
class Zend_View_Helper_Music extends Zend_View_Helper_Abstract
{
  
  private $facebook_share_form_enabled = false;
  
  public function __construct()
  {
    $this->random_array = array();	
  	
    $stopwords  = "- a a's able about above according accordingly across actually after afterwards again against ain't all allow allows almost alone along already ";
    $stopwords .= "also although always am among amongst an and another any anybody anyhow anyone anything anyway anyways anywhere apart appear appreciate ";
    $stopwords .= "appropriate are aren't around as aside ask asking associated at available away awfully be became because become becomes becoming been ";
    $stopwords .= "before beforehand behind being believe below beside besides best better between beyond both brief but by c'mon c's came can ";
    $stopwords .= "can't cannot cant cause causes certain certainly changes clearly co com come comes concerning consequently consider considering contain containing contains ";
    $stopwords .= "corresponding could couldn't course currently definitely described despite did didn't different do does doesn't doing don't done down downwards during ";
    $stopwords .= "each edu eg eight either else elsewhere enough entirely especially et etc even ever every everybody everyone everything everywhere ex ";
    $stopwords .= "exactly example except far few fifth first five followed following follows for former formerly forth four from further furthermore get ";
    $stopwords .= "gets getting given gives go goes going gone got gotten greetings had hadn't happens hardly has hasn't have haven't having he he's hello help hence ";
    $stopwords .= "her here here's hereafter hereby herein hereupon hers herself hi him himself his hither hopefully how howbeit however i i'd i'll i'm i've ie if ignored ";
    $stopwords .= "immediate in inasmuch inc indeed indicate indicated indicates inner insofar instead into inward is isn't it it'd it'll it's its itself just keep keeps kept "; 
    $stopwords .= "know knows known last lately later latter latterly least less lest let let's like liked likely little look looking looks ltd mainly many may maybe ";
    $stopwords .= "me mean meanwhile merely might more moreover most mostly much must my myself name namely nd near nearly necessary need needs neither never nevertheless new ";
    $stopwords .= "next nine no nobody non none noone nor normally not nothing novel now nowhere obviously of off often oh ok okay old on once one ones only onto or other ";
    $stopwords .= "others otherwise ought our ours ourselves out outside over overall own particular particularly per perhaps placed please plus possible presumably ";
    $stopwords .= "probably provides que quite qv rather rd re really reasonably regarding regardless regards relatively respectively right said same saw say ";
    $stopwords .= "saying says second secondly see seeing seem seemed seeming seems seen self selves sensible sent serious seriously seven several shall ";
    $stopwords .= "she should shouldn't since six so some somebody somehow someone something sometime sometimes somewhat somewhere soon sorry specified specify specifying ";
    $stopwords .= "still sub such sup sure t's take taken tell tends th than thank thanks thanx that that's thats the their theirs them themselves then thence ";
    $stopwords .= "there there's thereafter thereby therefore therein theres thereupon these they they'd they'll they're they've think third this thorough thoroughly those ";
    $stopwords .= "though three through throughout thru thus to together too took toward towards tried tries truly try trying twice two un under unfortunately unless unlikely until ";
    $stopwords .= "unto up upon us use used useful uses using usually value various very via viz vs want wants was wasn't way we we'd we'll we're we've welcome well went were ";
    $stopwords .= "weren't what what's whatever when whence whenever where where's whereafter whereas whereby wherein whereupon wherever whether which while whither who ";
    $stopwords .= "who's whoever whole whom whose why will willing wish with within without won't wonder would would wouldn't yes yet you you'd you'll you're you've your dj good music";
    $stopwords .= "yours yourself yourselves zero ";


    $this->stopword_array = explode(" ",$stopwords);
  }
  
	public function SEO($title, $lower=true) 
	{       	
		$replace_what = array('  ', ' - ', ', ', ',', '_', '-', '.mp3');      
		//$replace_with = array(' ', '-', '-', ',', '-');    
		$replace_with = " ";
		
		if($lower) $title = strtolower(trim($title));
		
		$title = str_replace($replace_what, $replace_with, $title);   
		return ($title);
	}
	
	/******
	public function retailProducts($products)
	{
	  $html = '';
	  foreach($products as $retail)
			$html .= $this->retailProduct($retail);
			
		return $html;
	}
	********/
	
	public function trackImageURL($track)
	{
		if($track->retail_products)
		{
		  	foreach($track->retail_products as $retail)
			{
				if($retail->product_type == "JUNO")
				{
					//if(file_exists("http://images.juno.co.uk/150/CS".$retail->artist_id."-" . sprintf("%02d",$retail->song_id) . "A.jpg"))
					//{
						return "http://images.juno.co.uk/150/CS".$retail->artist_id."-" . sprintf("%02d",$retail->song_id) . "A.jpg";
					//}
			  	}
		  	}
		}

		
		if(isset($track->article))
		{
			if($track->article && $track->article->blog && $track->article->blog->image)
			{
				//if(file_exists($track->article->blog->image))
				//{
				 	return $track->article->blog->image;
				//}
			}
		}
		
		//Ok... let's see if we've switched over to assigned art... if so, use the assigned art ID... else use a random image
		if(!empty($track->assigned_art))
		{ 	
			return APP_URL."/images/audio_stub/audio".$track->assigned_art.".jpg";
		}

	}
	
	public function retailProducts($products)
	{			
		$product_buffer = null;

		if(is_array($products))
		{	
			$product_array = array();
			
			$found_juno = false;
			
			foreach($products as $retail)
			{
				if(is_object($retail))
				{
						
					switch ($retail->product_type)
					{
						case "JUNO":
							
							if(!$found_juno)
							{
								$found_juno = true; //Ghetto hack to avoid duplicated Juno D/L links
								$product_array[] = "<a class='retail_link downloads' href='http://www.junodownload.com/products/".$retail->artist_id."-".$retail->song_id."-".$retail->product_id.".htm?ref=alph' target='_blank'>Juno Download</a>";								
							}
							break;
						
						case "AMIE":
							$product_array[] =  "<a class='retail_link downloads' href='http://amiestreet.com/order/prepare?pytr=alphamale&songIds=".$retail->artist_id."' target='_blank'>Amie Street</a>";
							break;
	
						case "AMAZON":
							$product_array[] =  "<a class='retail_link downloads' href='".$retail->url."' target='_blank'>Amazon</a>";
							break;							
	
						case "THUMB":
							$product_array[] =  "<a class='retail_link ringtones' href='".$retail->url."' target='_blank'>Thumbplay</a>";
							break;

						case "BANDS":
							$product_array[] =  "<a class='retail_link tickets' href='".$retail->url."?affil_code=alpha-male.tv' target='_blank'>Bands In Town</a>";
							break;
					}		
					
				}
			}		

			//$product_buffer = implode(" / ", $product_array);		
		}
		
		//echo $product_buffer;
		return $product_array;	
	}
	
	public function music()
	{
	  return $this;
	}
	
	public function cleanBand($band_str)
	{
		$stopword_array = $this->stopword_array;
		
		if(stristr($band_str, ","))
		{
			$token = ",";
		}
		elseif(stristr($band_str, ";"))
		{		
			$token = ";";	
		}
		else
		{
			$token = " ";	
		}
	
		$my_favourite_bands = explode($token, strip_tags($band_str));
			
		if(!empty($band_str))
		{
			foreach($my_favourite_bands as $band)
			{
				$band = trim($band);
				
				foreach($my_favourite_bands as $key => $band)
				{
				
					if($band == "hip") $band = "hip hop";
					if($band == "hip-hop") $band = "hip hop";
					if($band == "hop") $band = "hip hop";
				
					
					if(empty($band))
					{
						unset($my_favourite_bands[$key]);
					}	
					else
					{
						//echo "Looking at:".$band."<BR>";
						if(in_array(strtolower($band), $stopword_array))
						{
							//echo "I found this word in stopwords:".$band."<BR>";	
							unset($my_favourite_bands[$key]);
							
						}	
					}
				}	
			}
		}	
		
		return($my_favourite_bands);
	}
	
	public function truncateOnWord($subject, $bounds, $end){
		$words = explode(" ", $subject);
		$shortname = "";
		foreach($words as $word) {
			if(strlen($shortname) <= $bounds)
			{
				$shortname = $shortname . " " . $word;   
			}
			else
			{
				$shortname = $shortname . $end;
				break;
			}
		}
		return trim($shortname);
	}
	
	public function breakUpLongWords($string, $limit){
		 return wordwrap($string, $limit, ' ', true);
	}
	
	function facebook_share_track_link_to($track_id)
	{
	  $html = $this->facebook_share_form_enabled ? '' : $this->facebook_share_form(); 
	  $html .= "<a href='#' onclick='facebook_share_dialog.track_id={$track_id}; facebook_share_dialog._loadedInDom$1=false; facebook_share_dialog.show(); return false;'></a>";
	  
	  return $html;
	}
	
	function facebook_share_form()
	{
	  $this->facebook_share_form_enabled = true;
	  $html = <<<HTML
	    
	    <div id="facebook_share_form">
        <div id="dialog_body" class="fb_interaction_dialog_body fb_interaction_form">
          This will share this track with your facebook friends
        </div>
        <div class="fb_dialog_buttons">
          <input class="fb_inputbutton" onclick="$.ajax({ url: '/listen/share_track/?track_id=' + facebook_share_dialog.track_id, complete: function() { facebook_share_dialog.close(true); } });" id="publish" name="publish" value="Share" type="button">
          <input class="fb_inputbutton fb_inputaux" onclick="facebook_share_dialog.close(false);" id="cancel" name="cancel" value="Skip" type="button">
        </div> 
      </div>
	  
HTML;
	  
	  $js = <<<JS
	    <script type="text/javascript">
	      FB_RequireFeatures(["Connect"], function() { 
          facebook_share_dialog = new FB.UI.PopupDialog("Share Track", document.getElementById('facebook_share_form'));
        });
      </script>
JS;
	  return  $html . $js;
	}
		
  
  
  
}
?>