<?php

class Zend_View_Helper_MemberIs extends Zend_View_Helper_Abstract
{
	public function memberIs($roles)
	{
		$access = new Blastit_Access_Cityblast;
		if (false == $access->isAuthenticated()) return false;
		if (!is_array($roles)) $roles = array($roles);
		return $access->hasRole($roles);
	}
}
