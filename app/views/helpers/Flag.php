<?
class Zend_View_Helper_Flag extends Zend_View_Helper_Abstract
{

	public function flag()
	{
		return $this;
	}
	
	public function getFlagByUser($user)
	{
		$hometown = unserialize($user->hometown_location);
		$current_location = unserialize($user->current_location);
		$flag_country = ($current_location)?$current_location:$hometown;
		return $this->getFlag($flag_country['country']);
	}
	
	public function getFlagByUserId($id)
	{
		$user = FacebookUser::find_by_id($id);
		$hometown = unserialize($user->hometown_location);
		$current_location = unserialize($user->current_location);
		$flag_country = ($current_location)?$current_location:$hometown;
		return $this->getFlag($flag_country['country']);
	}
	
	public function getFlag($country)
	{
		switch ($country){
	
			case "United States":
				$flag_img = "/images/flags/us.gif";
				echo $flag_img;
				break;
				
			case "Canada":
				$flag_img = "/images/flags/ca.gif";
				echo $flag_img;
				break;
				
			case "Afghanistan":
				$flag_img = "/images/flags/af.gif";
				echo $flag_img;
				break;
				
			case "Albania":
				$flag_img = "/images/flags/al.gif";
				echo $flag_img;
				break;
				
			case "Algeria":
				$flag_img = "/images/flags/dz.gif";
				echo $flag_img;
				break;
				
			case "American Samoa":
				$flag_img = "/images/flags/as.gif";
				echo $flag_img;
				break;
				
			case "Andorra":
				$flag_img = "/images/flags/ad.gif";
				echo $flag_img;
				break;
				
			case "Angola":
				$flag_img = "/images/flags/ao.gif";
				echo $flag_img;
				break;
				
			case "Anguilla":
				$flag_img = "/images/flags/ai.gif";
				echo $flag_img;
				break;
				
			case "Antarctica":
				$flag_img = "/images/flags/aq.gif";
				echo $flag_img;
				break;
				
			case "Antigua and Barbuda":
				$flag_img = "/images/flags/ag.gif";
				echo $flag_img;
				break;
				
			case "Argentina":
				$flag_img = "/images/flags/ar.gif";
				echo $flag_img;
				break;
				
			case "Armenia":
				$flag_img = "/images/flags/am.gif";
				echo $flag_img;
				break;
				
			case "Aruba":
				$flag_img = "/images/flags/aw.gif";
				echo $flag_img;
				break;
				
			case "Australia":
				$flag_img = "/images/flags/au.gif";
				echo $flag_img;
				break;
				
			case "Austria":
				$flag_img = "/images/flags/at.gif";
				echo $flag_img;
				break;
				
			case "Azerbaidjan":
				$flag_img = "/images/flags/az.gif";
				echo $flag_img;
				break;
				
			case "Bahamas":
				$flag_img = "/images/flags/bs.gif";
				echo $flag_img;
				break;
				
			case "Bahrain":
				$flag_img = "/images/flags/bh.gif";
				echo $flag_img;
				break;
				
			case "Bangladesh":
				$flag_img = "/images/flags/bd.gif";
				echo $flag_img;
				break;
				
			case "Barbados":
				$flag_img = "/images/flags/bb.gif";
				echo $flag_img;
				break;
				
			case "Belarus":
				$flag_img = "/images/flags/by.gif";
				echo $flag_img;
				break;
				
			case "Belgium":
				$flag_img = "/images/flags/be.gif";
				echo $flag_img;
				break;
				
			case "Belize":
				$flag_img = "/images/flags/bz.gif";
				echo $flag_img;
				break;
				
			case "Benin":
				$flag_img = "/images/flags/bj.gif";
				echo $flag_img;
				break;
				
			case "Bermuda":
				$flag_img = "/images/flags/bm.gif";
				echo $flag_img;
				break;
				
			case "Bhutan":
				$flag_img = "/images/flags/bt.gif";
				echo $flag_img;
				break;
				
			case "Bolivia":
				$flag_img = "/images/flags/bo.gif";
				echo $flag_img;
				break;
				
			case "Bosnia-Herzegovina":
				$flag_img = "/images/flags/ba.gif";
				echo $flag_img;
				break;
				
			case "Botswana":
				$flag_img = "/images/flags/bw.gif";
				echo $flag_img;
				break;
				
			case "Bouvet Island":
				$flag_img = "/images/flags/bv.gif";
				echo $flag_img;
				break;
				
			case "Brazil":
				$flag_img = "/images/flags/br.gif";
				echo $flag_img;
				break;
				
			case "British Indian Ocean Territory":
				$flag_img = "/images/flags/io.gif";
				echo $flag_img;
				break;
				
			case "Brunei Darussalam":
				$flag_img = "/images/flags/bn.gif";
				echo $flag_img;
				break;
				
			case "Bulgaria":
				$flag_img = "/images/flags/bg.gif";
				echo $flag_img;
				break;
				
			case "Burkina Faso":
				$flag_img = "/images/flags/bf.gif";
				echo $flag_img;
				break;
				
			case "Burundi":
				$flag_img = "/images/flags/bi.gif";
				echo $flag_img;
				break;
				
			case "Cambodia":
				$flag_img = "/images/flags/kh.gif";
				echo $flag_img;
				break;
				
			case "Cameroon":
				$flag_img = "/images/flags/cm.gif";
				echo $flag_img;
				break;
				
			case "Cape Verde":
				$flag_img = "/images/flags/cv.gif";
				echo $flag_img;
				break;
				
			case "Cayman Islands":
				$flag_img = "/images/flags/ky.gif";
				echo $flag_img;
				break;
				
			case "Central African Republic":
				$flag_img = "/images/flags/cf.gif";
				echo $flag_img;
				break;
				
			case "Chad":
				$flag_img = "/images/flags/td.gif";
				echo $flag_img;
				break;
				
			case "Chile":
				$flag_img = "/images/flags/cl.gif";
				echo $flag_img;
				break;
				
			case "China":
				$flag_img = "/images/flags/cn.gif";
				echo $flag_img;
				break;
				
			case "Christmas Island":
				$flag_img = "/images/flags/cx.gif";
				echo $flag_img;
				break;
				
			case "Cocos (Keeling) Islands":
				$flag_img = "/images/flags/cc.gif";
				echo $flag_img;
				break;
				
			case "Colombia":
				$flag_img = "/images/flags/co.gif";
				echo $flag_img;
				break;
				
			case "Comoros":
				$flag_img = "/images/flags/km.gif";
				echo $flag_img;
				break;
				
			case "Congo":
				$flag_img = "/images/flags/cg.gif";
				echo $flag_img;
				break;
				
			case "Cook Islands":
				$flag_img = "/images/flags/ck.gif";
				echo $flag_img;
				break;
				
			case "Costa Rica":
				$flag_img = "/images/flags/cr.gif";
				echo $flag_img;
				break;
				
			case "Croatia":
				$flag_img = "/images/flags/hr.gif";
				echo $flag_img;
				break;
				
			case "Cuba":
				$flag_img = "/images/flags/cu.gif";
				echo $flag_img;
				break;
				
			case "Cyprus":
				$flag_img = "/images/flags/cy.gif";
				echo $flag_img;
				break;
				
			case "Czech Republic":
				$flag_img = "/images/flags/cz.gif";
				echo $flag_img;
				break;
				
			case "Denmark":
				$flag_img = "/images/flags/dk.gif";
				echo $flag_img;
				break;
				
			case "Djibouti":
				$flag_img = "/images/flags/dj.gif";
				echo $flag_img;
				break;
				
			case "Dominica":
				$flag_img = "/images/flags/dm.gif";
				echo $flag_img;
				break;
				
			case "Dominican Republic":
				$flag_img = "/images/flags/do.gif";
				echo $flag_img;
				break;
				
			case "East Timor":
				$flag_img = "/images/flags/tp.gif";
				echo $flag_img;
				break;
				
			case "Ecuador":
				$flag_img = "/images/flags/ec.gif";
				echo $flag_img;
				break;
				
			case "Egypt":
				$flag_img = "/images/flags/eg.gif";
				echo $flag_img;
				break;
				
			case "El Salvador":
				$flag_img = "/images/flags/sv.gif";
				echo $flag_img;
				break;
				
			case "Equatorial Guinea":
				$flag_img = "/images/flags/gq.gif";
				echo $flag_img;
				break;
				
			case "Eritrea":
				$flag_img = "/images/flags/er.gif";
				echo $flag_img;
				break;
				
			case "Estonia":
				$flag_img = "/images/flags/ee.gif";
				echo $flag_img;
				break;
				
			case "Ethiopia":
				$flag_img = "/images/flags/et.gif";
				echo $flag_img;
				break;
				
			case "Falkland Islands":
				$flag_img = "/images/flags/fk.gif";
				echo $flag_img;
				break;
				
			case "Faroe Islands":
				$flag_img = "/images/flags/fo.gif";
				echo $flag_img;
				break;
				
			case "Fiji":
				$flag_img = "/images/flags/fj.gif";
				echo $flag_img;
				break;
				
			case "Finland":
				$flag_img = "/images/flags/fi.gif";
				echo $flag_img;
				break;
				
			case "Former Czechoslovakia":
				$flag_img = "/images/flags/cs.gif";
				echo $flag_img;
				break;
				
			case "Former USSR":
				$flag_img = "/images/flags/su.gif";
				echo $flag_img;
				break;
				
			case "France":
				$flag_img = "/images/flags/fr.gif";
				echo $flag_img;
				break;
				
			case "France (European Territory)":
				$flag_img = "/images/flags/fx.gif";
				echo $flag_img;
				break;
				
			case "French Guyana":
				$flag_img = "/images/flags/gf.gif";
				echo $flag_img;
				break;
				
			case "French Southern Territories":
				$flag_img = "/images/flags/tf.gif";
				echo $flag_img;
				break;
				
			case "Gabon":
				$flag_img = "/images/flags/ga.gif";
				echo $flag_img;
				break;
				
			case "Gambia":
				$flag_img = "/images/flags/gm.gif";
				echo $flag_img;
				break;
				
			case "Georgia":
				$flag_img = "/images/flags/ge.gif";
				echo $flag_img;
				break;
				
			case "Germany":
				$flag_img = "/images/flags/de.gif";
				echo $flag_img;
				break;
				
			case "Ghana":
				$flag_img = "/images/flags/gh.gif";
				echo $flag_img;
				break;
				
			case "Gibraltar":
				$flag_img = "/images/flags/gi.gif";
				echo $flag_img;
				break;
				
			case "Great Britain":
				$flag_img = "/images/flags/gb.gif";
				echo $flag_img;
				break;
				
			case "Greece":
				$flag_img = "/images/flags/gr.gif";
				echo $flag_img;
				break;
				
			case "Greenland":
				$flag_img = "/images/flags/gl.gif";
				echo $flag_img;
				break;
				
			case "Grenada":
				$flag_img = "/images/flags/gd.gif";
				echo $flag_img;
				break;
				
			case "Guadeloupe (French)":
				$flag_img = "/images/flags/gp.gif";
				echo $flag_img;
				break;
				
			case "Guam (USA)":
				$flag_img = "/images/flags/gu.gif";
				echo $flag_img;
				break;
				
			case "Guatemala":
				$flag_img = "/images/flags/gt.gif";
				echo $flag_img;
				break;
				
			case "Guinea":
				$flag_img = "/images/flags/gn.gif";
				echo $flag_img;
				break;
				
			case "Guinea Bissau":
				$flag_img = "/images/flags/gw.gif";
				echo $flag_img;
				break;
				
			case "Guyana":
				$flag_img = "/images/flags/gy.gif";
				echo $flag_img;
				break;
				
			case "Haiti":
				$flag_img = "/images/flags/ht.gif";
				echo $flag_img;
				break;
				
			case "Heard and McDonald Islands":
				$flag_img = "/images/flags/hm.gif";
				echo $flag_img;
				break;
				
			case "Honduras":
				$flag_img = "/images/flags/hn.gif";
				echo $flag_img;
				break;
				
			case "Hong Kong":
				$flag_img = "/images/flags/hk.gif";
				echo $flag_img;
				break;
				
			case "Hungary":
				$flag_img = "/images/flags/hu.gif";
				echo $flag_img;
				break;
				
			case "Iceland":
				$flag_img = "/images/flags/is.gif";
				echo $flag_img;
				break;
				
			case "India":
				$flag_img = "/images/flags/in.gif";
				echo $flag_img;
				break;
				
			case "Indonesia":
				$flag_img = "/images/flags/id.gif";
				echo $flag_img;
				break;
				
			case "International":
				$flag_img = "/images/flags/int.gif";
				echo $flag_img;
				break;
				
			case "Iran":
				$flag_img = "/images/flags/ir.gif";
				echo $flag_img;
				break;
				
			case "Iraq":
				$flag_img = "/images/flags/iq.gif";
				echo $flag_img;
				break;
				
			case "Ireland":
				$flag_img = "/images/flags/ie.gif";
				echo $flag_img;
				break;
				
			case "Israel":
				$flag_img = "/images/flags/il.gif";
				echo $flag_img;
				break;
				
			case "Italy":
				$flag_img = "/images/flags/it.gif";
				echo $flag_img;
				break;
				
			case "Ivory Coast (Cote D&#39;Ivoire)":
				$flag_img = "/images/flags/ci.gif";
				echo $flag_img;
				break;
				
			case "Jamaica":
				$flag_img = "/images/flags/jm.gif";
				echo $flag_img;
				break;
				
			case "Japan":
				$flag_img = "/images/flags/jp.gif";
				echo $flag_img;
				break;
				
			case "Jordan":
				$flag_img = "/images/flags/jo.gif";
				echo $flag_img;
				break;
				
			case "Kazakhstan":
				$flag_img = "/images/flags/kz.gif";
				echo $flag_img;
				break;
				
			case "Kenya":
				$flag_img = "/images/flags/ke.gif";
				echo $flag_img;
				break;
				
			case "Kiribati":
				$flag_img = "/images/flags/ki.gif";
				echo $flag_img;
				break;
				
			case "Kuwait":
				$flag_img = "/images/flags/kw.gif";
				echo $flag_img;
				break;
				
			case "Kyrgyzstan":
				$flag_img = "/images/flags/kg.gif";
				echo $flag_img;
				break;
				
			case "Laos":
				$flag_img = "/images/flags/la.gif";
				echo $flag_img;
				break;
				
			case "Latvia":
				$flag_img = "/images/flags/lv.gif";
				echo $flag_img;
				break;
				
			case "Lebanon":
				$flag_img = "/images/flags/lb.gif";
				echo $flag_img;
				break;
				
			case "Lesotho":
				$flag_img = "/images/flags/ls.gif";
				echo $flag_img;
				break;
				
			case "Liberia":
				$flag_img = "/images/flags/lr.gif";
				echo $flag_img;
				break;
				
			case "Libya":
				$flag_img = "/images/flags/ly.gif";
				echo $flag_img;
				break;
				
			case "Liechtenstein":
				$flag_img = "/images/flags/li.gif";
				echo $flag_img;
				break;
				
			case "Lithuania":
				$flag_img = "/images/flags/lt.gif";
				echo $flag_img;
				break;
				
			case "Luxembourg":
				$flag_img = "/images/flags/lu.gif";
				echo $flag_img;
				break;
				
			case "Macau":
				$flag_img = "/images/flags/mo.gif";
				echo $flag_img;
				break;
				
			case "Macedonia":
				$flag_img = "/images/flags/mk.gif";
				echo $flag_img;
				break;
				
			case "Madagascar":
				$flag_img = "/images/flags/mg.gif";
				echo $flag_img;
				break;
				
			case "Malawi":
				$flag_img = "/images/flags/mw.gif";
				echo $flag_img;
				break;
				
			case "Malaysia":
				$flag_img = "/images/flags/my.gif";
				echo $flag_img;
				break;
				
			case "Maldives":
				$flag_img = "/images/flags/mv.gif";
				echo $flag_img;
				break;
				
			case "Mali":
				$flag_img = "/images/flags/ml.gif";
				echo $flag_img;
				break;
				
			case "Malta":
				$flag_img = "/images/flags/mt.gif";
				echo $flag_img;
				break;
				
			case "Marshall Islands":
				$flag_img = "/images/flags/mh.gif";
				echo $flag_img;
				break;
				
			case "Martinique (French)":
				$flag_img = "/images/flags/mq.gif";
				echo $flag_img;
				break;
				
			case "Mauritania":
				$flag_img = "/images/flags/mr.gif";
				echo $flag_img;
				break;
				
			case "Mauritius":
				$flag_img = "/images/flags/mu.gif";
				echo $flag_img;
				break;
				
			case "Mayotte":
				$flag_img = "/images/flags/yt.gif";
				echo $flag_img;
				break;
				
			case "Mexico":
				$flag_img = "/images/flags/mx.gif";
				echo $flag_img;
				break;
				
			case "Micronesia":
				$flag_img = "/images/flags/fm.gif";
				echo $flag_img;
				break;
				
			case "Moldavia":
				$flag_img = "/images/flags/md.gif";
				echo $flag_img;
				break;
				
			case "Monaco":
				$flag_img = "/images/flags/mc.gif";
				echo $flag_img;
				break;
				
			case "Mongolia":
				$flag_img = "/images/flags/mn.gif";
				echo $flag_img;
				break;
				
			case "Montserrat":
				$flag_img = "/images/flags/ms.gif";
				echo $flag_img;
				break;
				
			case "Morocco":
				$flag_img = "/images/flags/ma.gif";
				echo $flag_img;
				break;
				
			case "Mozambique":
				$flag_img = "/images/flags/mz.gif";
				echo $flag_img;
				break;
				
			case "Myanmar":
				$flag_img = "/images/flags/mm.gif";
				echo $flag_img;
				break;
				
			case "Namibia":
				$flag_img = "/images/flags/na.gif";
				echo $flag_img;
				break;
				
			case "Nauru":
				$flag_img = "/images/flags/nr.gif";
				echo $flag_img;
				break;
				
			case "Nepal":
				$flag_img = "/images/flags/np.gif";
				echo $flag_img;
				break;
				
			case "Netherlands":
				$flag_img = "/images/flags/nl.gif";
				echo $flag_img;
				break;
				
			case "Netherlands Antilles":
				$flag_img = "/images/flags/an.gif";
				echo $flag_img;
				break;
				
			case "Neutral Zone":
				$flag_img = "/images/flags/nt.gif";
				echo $flag_img;
				break;
				
			case "New Caledonia (French)":
				$flag_img = "/images/flags/nc.gif";
				echo $flag_img;
				break;
				
			case "New Zealand":
				$flag_img = "/images/flags/nz.gif";
				echo $flag_img;
				break;
				
			case "Nicaragua":
				$flag_img = "/images/flags/ni.gif";
				echo $flag_img;
				break;
				
			case "Niger":
				$flag_img = "/images/flags/ne.gif";
				echo $flag_img;
				break;
				
			case "Nigeria":
				$flag_img = "/images/flags/ng.gif";
				echo $flag_img;
				break;
				
			case "Niue":
				$flag_img = "/images/flags/nu.gif";
				echo $flag_img;
				break;
				
			case "Norfolk Island":
				$flag_img = "/images/flags/nf.gif";
				echo $flag_img;
				break;
				
			case "North Korea":
				$flag_img = "/images/flags/kp.gif";
				echo $flag_img;
				break;
				
			case "Northern Mariana Islands":
				$flag_img = "/images/flags/mp.gif";
				echo $flag_img;
				break;
				
			case "Norway":
				$flag_img = "/images/flags/no.gif";
				echo $flag_img;
				break;
				
			case "Oman":
				$flag_img = "/images/flags/om.gif";
				echo $flag_img;
				break;
				
			case "Pakistan":
				$flag_img = "/images/flags/pk.gif";
				echo $flag_img;
				break;
				
			case "Palau":
				$flag_img = "/images/flags/pw.gif";
				echo $flag_img;
				break;
				
			case "Panama":
				$flag_img = "/images/flags/pa.gif";
				echo $flag_img;
				break;
				
			case "Papua New Guinea":
				$flag_img = "/images/flags/pg.gif";
				echo $flag_img;
				break;
				
			case "Paraguay":
				$flag_img = "/images/flags/py.gif";
				echo $flag_img;
				break;
				
			case "Peru":
				$flag_img = "/images/flags/pe.gif";
				echo $flag_img;
				break;
				
			case "Philippines":
				$flag_img = "/images/flags/ph.gif";
				echo $flag_img;
				break;
				
			case "Pitcairn Island":
				$flag_img = "/images/flags/pn.gif";
				echo $flag_img;
				break;
				
			case "Poland":
				$flag_img = "/images/flags/pl.gif";
				echo $flag_img;
				break;
				
			case "Polynesia (French)":
				$flag_img = "/images/flags/pf.gif";
				echo $flag_img;
				break;
				
			case "Portugal":
				$flag_img = "/images/flags/pt.gif";
				echo $flag_img;
				break;
				
			case "Puerto Rico":
				$flag_img = "/images/flags/pr.gif";
				echo $flag_img;
				break;
				
			case "Qatar":
				$flag_img = "/images/flags/qa.gif";
				echo $flag_img;
				break;
				
			case "Reunion (French)":
				$flag_img = "/images/flags/re.gif";
				echo $flag_img;
				break;
				
			case "Romania":
				$flag_img = "/images/flags/ro.gif";
				echo $flag_img;
				break;
				
			case "Russian Federation":
				$flag_img = "/images/flags/ru.gif";
				echo $flag_img;
				break;
				
			case "Rwanda":
				$flag_img = "/images/flags/rw.gif";
				echo $flag_img;
				break;
				
			case "S. Georgia & S. Sandwich Isls.":
				$flag_img = "/images/flags/gs.gif";
				echo $flag_img;
				break;
				
			case "Saint Helena":
				$flag_img = "/images/flags/sh.gif";
				echo $flag_img;
				break;
				
			case "Saint Kitts & Nevis Anguilla":
				$flag_img = "/images/flags/kn.gif";
				echo $flag_img;
				break;
				
			case "Saint Lucia":
				$flag_img = "/images/flags/lc.gif";
				echo $flag_img;
				break;
				
			case "Saint Pierre and Miquelon":
				$flag_img = "/images/flags/pm.gif";
				echo $flag_img;
				break;
				
			case "Saint Tome (Sao Tome) and Principe":
				$flag_img = "/images/flags/st.gif";
				echo $flag_img;
				break;
				
			case "Saint Vincent & Grenadines":
				$flag_img = "/images/flags/vc.gif";
				echo $flag_img;
				break;
				
			case "Samoa":
				$flag_img = "/images/flags/ws.gif";
				echo $flag_img;
				break;
				
			case "San Marino":
				$flag_img = "/images/flags/sm.gif";
				echo $flag_img;
				break;
				
			case "Saudi Arabia":
				$flag_img = "/images/flags/sa.gif";
				echo $flag_img;
				break;
				
			case "Senegal":
				$flag_img = "/images/flags/sn.gif";
				echo $flag_img;
				break;
				
			case "Seychelles":
				$flag_img = "/images/flags/sc.gif";
				echo $flag_img;
				break;
				
			case "Sierra Leone":
				$flag_img = "/images/flags/sl.gif";
				echo $flag_img;
				break;
				
			case "Singapore":
				$flag_img = "/images/flags/sg.gif";
				echo $flag_img;
				break;
				
			case "Slovak Republic":
				$flag_img = "/images/flags/sk.gif";
				echo $flag_img;
				break;
				
			case "Slovenia":
				$flag_img = "/images/flags/si.gif";
				echo $flag_img;
				break;
				
			case "Solomon Islands":
				$flag_img = "/images/flags/sb.gif";
				echo $flag_img;
				break;
				
			case "Somalia":
				$flag_img = "/images/flags/so.gif";
				echo $flag_img;
				break;
				
			case "South Africa":
				$flag_img = "/images/flags/za.gif";
				echo $flag_img;
				break;
				
			case "South Korea":
				$flag_img = "/images/flags/kr.gif";
				echo $flag_img;
				break;
				
			case "Spain":
				$flag_img = "/images/flags/es.gif";
				echo $flag_img;
				break;
				
			case "Sri Lanka":
				$flag_img = "/images/flags/lk.gif";
				echo $flag_img;
				break;
				
			case "Sudan":
				$flag_img = "/images/flags/sd.gif";
				echo $flag_img;
				break;
				
			case "Suriname":
				$flag_img = "/images/flags/sr.gif";
				echo $flag_img;
				break;
				
			case "Svalbard and Jan Mayen Islands":
				$flag_img = "/images/flags/sj.gif";
				echo $flag_img;
				break;
				
			case "Swaziland":
				$flag_img = "/images/flags/sz.gif";
				echo $flag_img;
				break;
				
			case "Sweden":
				$flag_img = "/images/flags/se.gif";
				echo $flag_img;
				break;
				
			case "Switzerland":
				$flag_img = "/images/flags/ch.gif";
				echo $flag_img;
				break;
				
			case "Syria":
				$flag_img = "/images/flags/sy.gif";
				echo $flag_img;
				break;
				
			case "Tadjikistan":
				$flag_img = "/images/flags/tj.gif";
				echo $flag_img;
				break;
				
			case "Taiwan":
				$flag_img = "/images/flags/tw.gif";
				echo $flag_img;
				break;
				
			case "Tanzania":
				$flag_img = "/images/flags/tz.gif";
				echo $flag_img;
				break;
				
			case "Thailand":
				$flag_img = "/images/flags/th.gif";
				echo $flag_img;
				break;
				
			case "Togo":
				$flag_img = "/images/flags/tg.gif";
				echo $flag_img;
				break;
				
			case "Tokelau":
				$flag_img = "/images/flags/tk.gif";
				echo $flag_img;
				break;
				
			case "Tonga":
				$flag_img = "/images/flags/to.gif";
				echo $flag_img;
				break;
				
			case "Trinidad and Tobago":
				$flag_img = "/images/flags/tt.gif";
				echo $flag_img;
				break;
				
			case "Tunisia":
				$flag_img = "/images/flags/tn.gif";
				echo $flag_img;
				break;
				
			case "Turkey":
				$flag_img = "/images/flags/tr.gif";
				echo $flag_img;
				break;
				
			case "Turkmenistan":
				$flag_img = "/images/flags/tm.gif";
				echo $flag_img;
				break;
				
			case "Turks and Caicos Islands":
				$flag_img = "/images/flags/tc.gif";
				echo $flag_img;
				break;
				
			case "Tuvalu":
				$flag_img = "/images/flags/tv.gif";
				echo $flag_img;
				break;
				
			case "Uganda":
				$flag_img = "/images/flags/ug.gif";
				echo $flag_img;
				break;
				
			case "Ukraine":
				$flag_img = "/images/flags/ua.gif";
				echo $flag_img;
				break;
				
			case "United Arab Emirates":
				$flag_img = "/images/flags/ae.gif";
				echo $flag_img;
				break;
				
			case "United Kingdom":
				$flag_img = "/images/flags/gb.gif";
				echo $flag_img;
				break;
				
			case "Uruguay":
				$flag_img = "/images/flags/uy.gif";
				echo $flag_img;
				break;
				
			case "USA Military":
				$flag_img = "/images/flags/mil.gif";
				echo $flag_img;
				break;
				
			case "USA Minor Outlying Islands":
				$flag_img = "/images/flags/um.gif";
				echo $flag_img;
				break;
				
			case "Uzbekistan":
				$flag_img = "/images/flags/uz.gif";
				echo $flag_img;
				break;
				
			case "Vanuatu":
				$flag_img = "/images/flags/vu.gif";
				echo $flag_img;
				break;
				
			case "Vatican City State":
				$flag_img = "/images/flags/va.gif";
				echo $flag_img;
				break;
				
			case "Venezuela":
				$flag_img = "/images/flags/ve.gif";
				echo $flag_img;
				break;
				
			case "Vietnam":
				$flag_img = "/images/flags/vn.gif";
				echo $flag_img;
				break;
				
			case "Virgin Islands (British)":
				$flag_img = "/images/flags/vg.gif";
				echo $flag_img;
				break;
				
			case "Virgin Islands (USA)":
				$flag_img = "/images/flags/vi.gif";
				echo $flag_img;
				break;
				
			case "Wallis and Futuna Islands":
				$flag_img = "/images/flags/wf.gif";
				echo $flag_img;
				break;
				
			case "Western Sahara":
				$flag_img = "/images/flags/eh.gif";
				echo $flag_img;
				break;
				
			case "Yemen":
				$flag_img = "/images/flags/ye.gif";
				echo $flag_img;
				break;
				
			case "Yugoslavia":
				$flag_img = "/images/flags/yu.gif";
				echo $flag_img;
				break;
				
			case "Zaire":
				$flag_img = "/images/flags/zr.gif";
				echo $flag_img;
				break;
				
			case "Zambia":
				$flag_img = "/images/flags/zm.gif";
				echo $flag_img;
				break;
				
			case "Zimbabwe":
				$flag_img = "/images/flags/zw.gif";
				echo $flag_img;
				break;
				
			default:
				$flag_img = "/images/flags/zw.gif";
				echo $flag_img;
				break;
		
		}
	}
}
?>