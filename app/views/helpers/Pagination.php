<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Zend_View_Helper_Pagination extends Zend_View_Helper_Abstract
{
	public function pagination($paginator = null, $scrollingStyle = null, $isAjax = false, $ajaxContainer = null)
	{
		if($paginator == null) $paginator = $this->view->paginator;
		if($scrollingStyle == null) $scrollingStyle = 'Sliding';

		$pagination = '';

		if($paginator)
		{
			$pages = $paginator->getPages($scrollingStyle);

			if ($pages->pageCount > 1)
			{
				$pagination .= '<ul id="pagination">';

				//Previous page link
				if (isset($pages->previous))
					$pagination .= '<li class="previous">'.$this->getLink($pages->previous, '&laquo; Previous', $isAjax, $ajaxContainer).'</li>';
				else
					$pagination .= '<li class="previous-off">&laquo; Previous</li>';

				foreach($pages->pagesInRange as $page)
				{
					if($page == $pages->current)
						$pagination .= '<li class="active">'.$page.'</li>';
					else
						$pagination .= '<li class="item">'.$this->getLink($page, $page, $isAjax, $ajaxContainer).'</li>';
				}

				//Next page link
				if (isset($pages->next))
					$pagination .= '<li class="next">'.$this->getLink($pages->next, 'Next &raquo;', $isAjax, $ajaxContainer).'</li>';
				else
					$pagination .= '<li class="next-off">Next &raquo;</li>';

				$pagination .= '</ul>';
			}
		}
		return $pagination;
	}

	private function getLink($page, $label, $isAjax = false, $ajaxContainer = null)
	{
		$urlParams = array('page' => $page);
		$request = Zend_Controller_Front::getInstance()->getRequest();
		//echo $this->view->url();
		if($isAjax)
			$link = '<a href="javascript:void(0)" class="'.$ajaxContainer.'" a:url="'. $this->view->url($urlParams).'">'.$label.'</a>';
		else
			$link = '<a href="'. $this->view->url($urlParams).'">'.$label.'</a>';

		return $link;
	}
}

?>
