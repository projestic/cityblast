<?php

class Zend_View_Helper_FbEmbedPost extends Zend_View_Helper_Abstract
{
	public function fbEmbedPost($icon_url, $profile_name, $category, $caption, $caption_url, $caption_image_url, $post_date = null)
	{
		if (!$post_date) $post_date = date('F j');
		$likes_num = number_format(rand(1400,9698));
		$people_likes_num = rand(1, 100);
		return <<<_DOC
<div class="fbEmbedPost">
	<div class="facebook-block">
		<div class="fbb-content">
			<div class="clearfix fbb-top">
				<a href="#" class="gotcha_popup"><img src="$icon_url" alt="" /></a>
				<div class="fbb-top-title-container">
					<div class="clearfix">
						<div class="fbb-top-button">
							<a role="button" href="#" class="gotcha_popup"><i></i>Timeline</a>
						</div>
						<div class="fbb-top-title" style="height: 48px"></div>
						<div class="fbb-top-title">
							<h5><a class="profileLink gotcha_popup" href="#">$profile_name</a></h5>
							<div class="fbb-top-likes">$category &middot; $likes_num Likes</div>
						</div>
					</div>
				</div>
			</div>
			<p>$caption</p>
			<p><a href="#" class="gotcha_popup">$caption_url</a></p>
			<div style="line-height: 1px"><a href="#" style="width:442px;" class="gotcha_popup"><img src="$caption_image_url" alt="$caption $caption_url" width="444" height="296"></a></div>
			<div class="fbb-date"><span><a href="#" class="gotcha_popup">$post_date</a></span><a data-hover="tooltip" aria-label="Public" href="#" target="_blank" role="button"></a></div>
		</div>
		<div class="fbb-footer">
			<a href="#" title="Like this item" class="like gotcha_popup"><i></i>Like</a>
			<a href="#" title="Comment this item" class="comment gotcha_popup"><i></i>Comment</a>
			<a href="#" title="Share this item" class="gotcha_popup"><i></i>Share</a>
			<div class="people-likes">
				<a href="#" class="gotcha_popup">$people_likes_num people</a> like this.
			</div>
		</div>
	</div>
</div>
_DOC;
	}
}
