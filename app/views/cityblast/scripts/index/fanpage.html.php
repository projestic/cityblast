<div class="grid_12">
	
	<div class="box clearfix">

		<div class="box_section">
			<h1>How To Set Up Your Facebook FanPage.</h1>
		</div>

		<p>Please follow the carefully outlined <strong>5 step process</strong> below and we will endeavor to set up your RSS Feed to your FanPage as quickly as possible.</p>
		
		<hr />
		<h5>Step 1: Log in to Facebook and click on your FanPage</h5>
		<p>Simply log in to your Facebook account and look for your Fan page in the <b>pages</b> section.</p>
		
		<img src="/images/fanpage-step-1.jpg" alt="" border="0" style="margin-bottom: 20px;" />
		<hr />
		
		<h5>Step 2: Click on Edit</h5>
		<p>This step is really easy, simply click on the Edit Page button.</p>
		<img src="/images/fanpage-step-2.jpg" alt="" border="0" style="margin-bottom: 20px;"/>
		<hr />
		
		<h5>Step 3: Copy and Paste Your FanPage ID</h5>
		<p>This step is extremely important; we absolutely must have your FanPage ID in order for the service to work!</p>
		<p>Copy the URL at the top of the page next to the <strong>id=</strong> sign.  You can see an example of this below.  Your URL may look different but as long as you copy the id NUMBER after the equal sign you will be all good. </p>
		<img src="/images/fanpage-step-3.jpg" alt="" border="0" style="margin-bottom: 20px;" />
		<hr />
		
		<h5>Step4: Ensure You Have Set Up Your Permissions Properly</h5>
		<p>Please be sure to set your &quot;People can write or post content to the wall&quot; and &quot;People can add photos and videos&quot;. You&#39;ll also need to remove any country restrictions.</p>
		<p>If your permissions set incorrectly and <?=COMPANY_NAME;?> will not be able to publish to your Fanpage.</p>
		<img src="/images/fanpage-step-4.jpg" alt="" border="0" style="margin-bottom: 20px;" />
		<hr />
		
		<h5>Step5: Email Us Your FanPage ID</h5>
		<p>That's the whole process. Just remember to <a href="mailto:<?=HELP_EMAIL;?>">email us your FanPage ID</a> and we'll start publishing as soon as possible.</p>
		<p>If you have any questions, please don't hesitate to contact us. We'll be waiting for your Fan Page ID and our support team will set your site up as soon as we receive your information. </p>
		

	</div>
	
</div>