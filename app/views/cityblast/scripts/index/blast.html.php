			<div class="grid_12">
			
				<div class="box" style="padding: 20px 40px;">
			
					<div style="width: 465px; height: 98px; margin: -20px -40px 20px; padding: 272px 0 0 535px; background-image: url(/images/blasting_banner_bg.jpg); position: relative;">
					
						<div id="blast_video_still" style="position: absolute; top: 50px; left: 27px; width: 480px; height: 270px; z-index: 2; background-image: url(/images/blast_video_still.jpg);"></div>
						<div style="position: absolute; top: 50px; left: 27px; width: 480px; height: 270px; z-index: 1;">
							
							<div id="blast_animation"></div>
							
						</div>
						
						<script type="text/javascript" src="/js/swfobject.js"></script>
						
						<script type="text/javascript">
				
							var flashvars = {};
								flashvars.file = "/video/blast_header_video.f4v";
								flashvars.skin = "/video/mediaplayer/cb/cb.xml";
								flashvars.image = "/images/blast_video_still.jpg";
								flashvars.smoothing = true;
								flashvars.stretching = "exactfit";
								flashvars.controlbar = 'none';
								flashvars.autostart = true;
								flashvars.displayclick = 'none';
								flashvars.repeat = 'list';
								flashvars.volume = 100;
							
							var params = {};
								params.menu = false;
								params.allowfullscreen = false;
								params.allowscriptaccess = true;
								params.wmode = "opaque";
							
							var attributes = {};
								attributes.id = "ba";
								attributes.name = "ba";
								
							swfobject.embedSWF("/video/mediaplayer/player.swf", "blast_animation", "480", "270", "9.0.0","/scripts/mediaplayer/expressInstall.swf", flashvars, params, attributes);
								
						</script>
						
						<a href="javascript:void(0);" id="blasting_video_btn"><img src="/images/blank.gif" width="90" height="60" class="video_btn" /></a>
						<a href="/"><img src="/images/blank.gif" width="280" height="60" class="compose_blast_btn" /></a>
					
					</div>
					
					<div class="key_features clearfix">
						
						<div class="key_feature">
							
							<h3>Impress Your Clients.</h3>
							
							<p>The real estate business has changed. Have you changed with it? To compete in today's cutting edge real estate world, your clients expect you to use the best systems, newest approaches, and current technology. With one simple click, you'll be marketing like an online real estate guru; and your clients will feel the difference.</p>
							
							<img src="/images/blank.gif" width="50" height="50" class="impress icon" />
						
						</div>
						
						<div class="key_feature">
							
							<h3>Sell Properties Faster.</h3>
							
							<p>To sell a listing quickly and easily, you need to expose it to many potential buyers in as short a time as possible. It's always been a simple theory, but was traditionally difficult to practice without a massive budget. Not anymore. Effectively advertise to tens of thousands of buyers all across your city, and sell your listings fast.</p>
							
							<img src="/images/blank.gif" width="50" height="50" class="sell icon" />
						
						</div>
						
						<div class="key_feature">
							
							<h3>Win More Listings.</h3>
							
							<p>When you feature <?=COMPANY_NAME;?>'s marketing tools in your listing presentations, you will dramatically increase the number of listings you win. Imagine describing to sellers your marketing edge: showcasing their home to 100,000 or more potential local buyers - the moment their listing hits the market. Now you can.</p>
							
							<img src="/images/blank.gif" width="50" height="50" class="win icon" />
						
						</div>
						
					</div>
					
					<div class="action_heading">Ready to sell your listing fast? <a href="/">Click here</a> to Blast now.</div>
					
					<div class="clearfix" style="margin: 20px 0;">
					
						<div style="width: 460px; float: left; font-size: 12px;">
							
							<h4>Why Blast My Listings?</h4>
							
							<p>When you Blast your listings, you harness the cumulative networking power of hundreds of agents throughout your city, and sell your property fast. You reach tens of thousands of local buyers, by advertising your listing on other local agents' personal Facebook, Twitter and LinkedIn accounts for all of their friends to see. And best of all? It's easy.</p>
							
							<ul>
								<li>enjoy a first class online presence, and a lucrative top-producer image</li>
								<li>get an edge in every listing presentation, with a powerful online marketing pitch</li>
								<li>earn a reputation for fast, top-dollar sales, and the referrals that come with it</li>
								<li>effortlessly project the confident business savvy of a current real estate professional</li>
							</ul>
							
							<h4>How Does it Work?</h4>
							
							<ol>
								<li style="margin-bottom: 10px;">You produce a unique marketing message to sell your listing, called a Blast. A Blast is created just like a traditional property listing, with a short write-up, several pictures, and property information like address and price.</li>
								<li style="margin-bottom: 10px;">Simply submit your Blast. It is immediately scheduled to be advertised by hundreds of other professional real estate agents all across your city, on Facebook, Twitter and LinkedIn.</li>
								<li>Tens of thousands of potential home buyers see your listing, and book showings or attend open houses, selling your properties faster and for much higher prices.</li>
							</ol>
						</div>
					
						<div style="width: 420px; float: left; margin-left: 40px;">
							
							<h4>Testimonials</h4>
							
							<div style="padding-left: 70px; margin-bottom: 20px; position: relative; font-size: 12px;">
								
								<h6 style="padding-bottom: 3px; border-bottom: solid 1px #e5e5e5;">Leila Talibova, <span style="font-size: 11px; color: #4d4d4d;">HomeLife Victory Realty Inc.</span></h6>
								<p>My friends and previous colleagues noticed my Facebook updates right away. In only my first week using <?=COMPANY_NAME;?>, I received a call from a family friend who said she'd seen my Facebook and wanted list their home with me! They loved when I said I'd use <?=COMPANY_NAME;?> to market their home - it sold in just days!</p>
							
								<img src="/images/blank.gif" width="50" height="50" class="icon leila" />
								
							</div>
							
							<div style="padding-left: 70px; margin-bottom: 20px; position: relative; font-size: 12px;">
								
								<h6 style="padding-bottom: 3px; border-bottom: solid 1px #e5e5e5;">Shaun Nilsson, <span style="font-size: 11px; color: #4d4d4d;"><?=COMPANY_NAME;?> Founder</span></h6>
								<p>When I began in real estate, I didn't have any clients or leads. I began asking more established agents if I could post their listings on my Facebook Wall to attract buyers. What a revelation! I was able to make $165,000 in commissions by doing this - in my first year!  That's where <?=COMPANY_NAME;?> was born.</p>
							
								<img src="/images/blank.gif" width="50" height="50" class="icon shaun" />
								
							</div>
							
							<div style="padding-left: 70px; position: relative; font-size: 12px;">
								
								<h6 style="padding-bottom: 3px; border-bottom: solid 1px #e5e5e5;">Luke McKenzie, <span style="font-size: 11px; color: #4d4d4d;">Royal LePage Signature Realty Inc.</span></h6>
								<p>Our whole team is thrilled with the amount of exposure <?=COMPANY_NAME;?> gives us. We've really been impressed with their innovative sales tools. We Blast every listing!</p>
							
								<img src="/images/blank.gif" width="50" height="50" class="icon luke" />
								
							</div>
							
						</div>
						
					</div>
					
					<div class="action_heading">Ready to reach tens of thousands of buyers through Facebook? <a href="/">Blast now.</a></div>
					
					<div class="clearfix" style="margin: 20px 0;">
					
						<div style="width: 400px; float: left; font-size: 12px;">
							
							<h4>Wondering How Your Blast Appears?</h4>
							
							<p>This is what an actual Blast looks like on an agent's Facebook. Your Blast will be advertised by hundreds of local agents to their personal clients just like this.</p>
							
							<ul>
								<li>Attention-getting</li>
								<li>Professional</li>
								<li>Includes picture and write-up</li>
								<li>Showcased beautifully</li>
							</ul>
						</div>
					
						<div style="width: 480px; margin-left: 40px; float: left; height: 165px;">
							
							<img src="/images/facebook_blasting_screenshot_thumb.jpg" width="480" height="145" />
							
							<span style="font-size: 12px; display: block; line-height: 30px; text-align: center;"><a href="javascript:void(0);" id="facebook_screenshot_link">Enlarge Screenshot</a></span>
							
						</div>
						
					</div>
					
					<div class="clearfix" style="margin: 20px 0;">
					
						<div style="width: 400px; float: left; font-size: 12px;">
							
							<h4>Wondering How Your Listing Appears?</h4>
							
							<p>This is what a listing looks like on <?=COMPANY_NAME;?>.</p>
							
							<ul>
								<li>Attention-getting</li>
								<li>Professional</li>
								<li>Includes picture and write-up</li>
								<li>Showcased beautifully</li>
							</ul>
						</div>
					
						<div style="width: 480px; margin-left: 40px; float: left; height: 165px;">
							
							<img src="/images/listing_screenshot_thumb.jpg" width="480" height="145" />
							
							<span style="font-size: 12px; display: block; line-height: 30px; text-align: center;"><a href="javascript:void(0);" id="listing_screenshot_link">Enlarge Screenshot</a></span>
							
						</div>
						
					</div>
					
					<div class="action_heading bottom clearfix"><a href="/" class="uiButton" style="width: 350px; margin: 10px 0 10px 325px; padding: 0px;">Create Your First Blast Now!</a></div>
					
				</div>
				
			</div>
			
			<div style="display: none;">
				
				<div id="blast_video_popup">
			
					 <div id="blasting_video"></div>
			
				</div>
				
			</div>
			
			<div style="display: none;">
				
				<div id="facebook_screenshot">
			
					 <img src="/images/facebook_blasting_screenshot.jpg" width="1060" height="714" />
			
				</div>
				
			</div>
			
			<div style="display: none;">
				
				<div id="listing_screenshot">
			
					 <img src="/images/listing_screenshot.jpg" width="840" height="515" />
			
				</div>
				
			</div>
			
<script type="text/javascript">

	$(document).ready(function(){
		
		$('#blasting_video_btn').click(function(event){
			event.preventDefault();
			$.colorbox({width:"700px", height: "475px", inline:true, href:"#blast_video_popup"});
			show_blast_video();
		});
		
		$('#facebook_screenshot_link').click(function(event){
			event.preventDefault();
			$.colorbox({inline:true, href:"#facebook_screenshot"});
		});
		
		$('#listing_screenshot_link').click(function(event){
			event.preventDefault();
			$.colorbox({inline:true, href:"#listing_screenshot"});
		});
	
	});

	var flashvars = {};
		flashvars.file = "/video/Blasting.f4v";
		flashvars.skin = "/video/mediaplayer/cb/cb.xml";
		flashvars.smoothing = true;
		flashvars.stretching = "exactfit";
		flashvars.autostart = true;
		flashvars.volume = 100;
	
	var params = {};
		params.menu = false;
		params.allowfullscreen = true;
		params.allowscriptaccess = true;
		params.wmode = "opaque";
	
	var attributes = {};
		attributes.id = "blasting_video";
		attributes.name = "blasting_video";
		
	function show_blast_video(){
		swfobject.embedSWF("/video/mediaplayer/player.swf", "blasting_video", "620", "380", "9.0.0","/scripts/mediaplayer/expressInstall.swf", flashvars, params, attributes);
	}
		
	var player = null; 
	function playerReady(thePlayer) {
		player = window.document[thePlayer.id];
		addListeners();
	}
	
	function addListeners() {
		if (player) { 
			player.addModelListener("STATE", "stateListener");
		} else {
			setTimeout("addListeners()",100);
		}
	}
	
	function stateListener(obj) { //IDLE, BUFFERING, PLAYING, PAUSED, COMPLETED
		currentState = obj.newstate; 
		previousState = obj.oldstate; 
	
		console.log("current state: " + currentState + "<br>previous state: " + previousState); 
		
		if(previousState == "BUFFERING" && currentState == "PLAYING"){
			$('#blast_video_still').css('display', 'none').delay(1000);
		}
		
		if(previousState == "PLAYING" && currentState == "IDLE"){
			$('#blast_video_still').css('display', 'block');
		}
	}
	
</script>