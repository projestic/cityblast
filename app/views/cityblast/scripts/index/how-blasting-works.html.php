<div class="row-fluid">
	<div class="bannerContainer howBlastingWorks no-margin">
		<img src="/images/how-blasting-works-banner.png" alt="">
		<span class="bannerInfo">
			<span class="bannerTitle">When you Blast your listings</span>
			<span class="bannerSubTitle">you harness the cumulative networking power of hundreds of agents throughout your city, and sell your property fast.</span>
		</span>
	</div>
</div>
<div class="contentBox how-blasting-works">
	<div>
		<div class="arrow-step">
			<img src="/images/icon-star.png">
			<h3>Impress Your Clients</h3>
			<p>The real estate business has changed. Have you changed with it? To compete in today's cutting edge real estate world, your clients expect you to use the best systems, newest approaches, and current technology. With one simple click, you'll be marketing like an online real estate guru; and your clients will feel the difference.</p>
		</div>
		<div class="arrow-step">
			<img src="/images/icon-home.png">
			<h3>Sell Properties Faster</h3>
			<p>To sell a listing quickly and easily, you need to expose it to many potential buyers in as short a time as possible. It's always been a simple theory, but was traditionally difficult to practice without a massive budget. Not anymore. Effectively advertise to tens of thousands of buyers all across your city, and sell your listings fast.</p>
		</div>
		<div class="arrow-step last">
			<img src="/images/icon-cup.png">
			<h3>Win More Listings</h3>
			<p>When you feature <?=COMPANY_NAME;?>'s marketing tools in your listing presentations, you will dramatically increase the number of listings you win. Imagine describing to sellers your marketing edge: showcasing their home to 100,000 or more potential local buyers - the moment their listing hits the market. Now you can.</p>
		</div>
	</div>
	<div class="clear"></div>
	<div class="clearfix composeBlastContainer">
		<div class="pull-left">
			<h1>Blast Your Listings</h1>
			<h3>Create Your Blast, and sell Your Listing Fast</h3>
		</div>
		<a class="pull-right" class="composeBlastBtn" href="/blast/index/<?=$this->listing_id;?>">Compose Your Blast.</a>
	</div>
</div>
<div class="contentBox why-blast">
	<img src="/images/why-blast-my-listing.png" style="position: absolute; margin-top:20px">
	<h2>Why Blast My Listings?</h2>
	<p><strong>When you Blast your listings, you harness the cumulative networking power of hundreds of agents throughout your city, and sell your property fast.</strong></p>
	<p>You reach tens of thousands of local buyers, by advertising your listing on other local agents' personal Facebook, Twitter and LinkedIn accounts for all of their friends to see.</p>
	<p class="small"><strong>And best of all? It's easy.</strong></p>
	<ul class="pull-left">
		<li>Enjoy a first class online presence, and a lucrative top-producer image</li>
		<li>Get an edge in every listing presentation, with a powerful online marketing pitch</li>
		<li>Earn a reputation for fast, top-dollar sales, and the referrals that come with it</li>
		<li>Effortlessly project the confident business savvy of a current real estate professional</li>
	</ul>
	<div class="clear" style="margin-bottom:20px"></div>
</div>
<div class="contentBox howitworksWrapper">
	<h2>How Does it Work?</h2>
	<div>
		<div class="arrow-step">
			<img src="/images/icon-flag.png">
			<h3>Step 1</h3>
			<p>You produce a unique marketing message to sell your listing, called a Blast. A Blast is created just like a traditional property listing, with a short write-up, several pictures, and property information like address and price.</p>
		</div>
		<div class="arrow-step">
			<img src="/images/icon-rocket.png">
			<h3>Step 2</h3>
			<p>Simply submit your Blast. It is immediately scheduled to be advertised by hundreds of other professional real estate agents all across your city, on Facebook, Twitter and LinkedIn.</p>
		</div>
		<div class="arrow-step last">
			<img src="/images/icon-home-2.png">
			<h3>Step 3</h3>
			<p>Tens of thousands of potential home buyers see your listing, and book showings or attend open houses, selling your properties faster and for much higher prices.</p>
		</div>
	</div>
	<div class="clear"></div>
	<!-- BXSLIDER FOR TESTIMONIALS STARTS -->
	<script type="text/javascript" src="/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript">
		$(function(){
			var slider = $('.testimonialsWrapper .testimonialsContainer').bxSlider({
				'controls' : false,
				'pager' : false,
				'auto' : true
			});
			$('.testPrevNextContainer .prev').click(function(){
					slider.goToPrevSlide();
			});
			$('.testPrevNextContainer .next').click(function(){
					slider.goToNextSlide();
			});
		});
	</script>
	<!-- BXSLIDER FOR TESTIMONIALS ENDS -->
	<div class="testimonialsWrapper pull-right">
		<div class="clearfix">
			<h6 class="pull-left testTitle">Testimonials</h6>
			<div class="pull-right testPrevNextContainer">
				<a class="prev" href="javascript:void(0);"></a>
				<a class="next" href="javascript:void(0);"></a>
			</div>
		</div>
		<div class="testimonialsContainer">
			<div class="slide">
				<div class="arrow"></div>
				<div class="media">
					<div class="media-object pull-right" style="background:url(/images/new-images/testimonial-user-talibova.jpg)">
						<img src="/images/new-images/testimonial-user-masking.png" />
					</div>
				</div>
				<div class="testimonials">
					<p>“My friends and previous colleagues noticed my Facebook updates right away. In only my first week using CityBlast, I received a call from a family friend who said she’d seen my Facebook and wanted to list their home with me. The experts are awesome!“</p>
					<p class="signature"><strong>Leila Talibova</strong><br>Top Rookie Agent<br>Home Life Realty</p>
				</div>
			</div>
			<div class="slide">
				<div class="arrow"></div>
				<div class="media">
					<div class="media-object pull-right" style="background:url(/images/new-images/testimonial-user-cerqua.jpg)">
						<img src="/images/new-images/testimonial-user-masking.png" />
					</div>
				</div>
				<div class="testimonials">
					<p>“I use CityBlast to help me market. Not only are they the heart of my online platform, but also help me to sell my clients’ listings quickly, and to achieve excellent prices. CityBlast is an incredible service for both beginners and top agents, and I highly recommend them.“</p>
					<p class="signature"><strong>Sheree Cerqua</strong><br>Ontario's #1 Individual Agent<br>Royal LePage</p>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="contentBox wondering">
	<h2>Wondering How Your Blast Appears?</h2>
	<p class="center">This is what an actual Blast looks like on an agent´s Facebook wall. Your blast will be advertised by hundreds of local agents to their personal clients just like this.</p>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

	</script>











	
<?/**
<h1>HELLO WORLD?</h1>
<div id="fb-root"></div> <script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/all.js#xfbml=1"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=600488086692865&amp;set=a.408573635884312.100242.153723091369369&amp;type=1" data-width="466"><div class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/photo.php?fbid=600488086692865&amp;set=a.408573635884312.100242.153723091369369&amp;type=1">Post</a> by <a href="https://www.facebook.com/CityBlast">CityBlast</a>.</div></div>
**/ ?>


	<div class="fbEmbedPost">
		<div class="fbPostBefore">We showcase your listing just like this</div>



				<div class="fbEmbedPost">
					<div class="facebook-block">
						<div class="fbb-content">
							<div class="clearfix fbb-top">
								<a href="#" class="gotcha_popup"><img src="/images/fanpage-thumb.png" alt="" /></a>
								<div class="fbb-top-title-container">
									<div class="clearfix">
										<div class="fbb-top-button">
											<a role="button" href="#" class="gotcha_popup"><i></i>Timeline</a>
										</div>
										<div class="fbb-top-title" style="height: 48px"></div>
										<div class="fbb-top-title">
											<h5><a class="profileLink gotcha_popup" href="#">Your Fan Page!</a></h5>
											<div class="fbb-top-likes">Real Estate &middot; 2,263 Likes</div>
										</div>
									</div>
								</div>
							</div>
							<p>Premier Alisanos gated community! This gorgeous 4 bedroom 2.5 bath home shows a pride of ownership throughout. Open kitchen with beautiful 
								granite counter tops and walk in pantry. Family room has built in entertainment center with stonework and gas fireplace. Classy coffered 
								ceilings and wide baseboards. Fruit trees and refreshing pool in the back. 3 car garage with built in cabinets too! <a href="#" class="gotcha_popup">https://www.cityblast.com/listing/view/20629</a></p>
							
							<div style="line-height: 1px"><a href="#" style="width:442px;" class="gotcha_popup"><img src="/images/sample-listing.png" alt="123 Apple Tree Lane" width="444" height="296"></a></div>
							<div class="fbb-date"><span><a href="#" class="gotcha_popup">about 2 months ago</a></span><a data-hover="tooltip" aria-label="Public" href="#" class="gotcha_popup" role="button"></a></div>
						</div>
						<div class="fbb-footer">
							<a href="#" class="gotcha_popup" title="Like this item" class="like"><i></i>Like</a>
							<a href="#" class="gotcha_popup" title="Comment this item" class="comment"><i></i>Comment</a>
							<a href="#" class="gotcha_popup" title="Share this item"><i></i>Share</a>
							<div class="people-likes">
								<a href="#" class="gotcha_popup">5 people</a> like this.
							</div>
						</div>
					</div>
				</div>


		<?/*<div class="fb-post" data-href="https://www.facebook.com/CityBlast/posts/600488086692865?stream_ref=10" data-width="440"></div>*/?>
		
		
		<div class="fbPostAfter">
			<p>On the Facebook walls of hundreds of agents in your local area!</p>
		</div>
	</div>



	
	<div class="clearfix composeBlastContainer"  style="border-top: 1px solid #cccccc; padding-top: 27px; margin-top: 30px;">
		<h3 style="font-size: 30px;">Ready to reach tens of thousands of buyers through Facebook? </h3>
		<a href="" class="uiButtonNew">Blast Now.</a>
	</div>
</div>



<?/******************

<div class="grid_12" style="text-align: right; font-style: italic; margin-bottom: 30px;">Do you still have more questions? 
	See what <a href="#" id="help_div_button" style="font-weight: bold;">other agents had to say</a>.</div>

<div style="display: none;" id="help_div">
	
	
	<div class="grid_7">
		<div class="clearfix" style="min-height: 161px; padding-left: 8px; background-color: #D9D9D9;">		
			<h3 style="color: #808080; ">Members helping members.</h3>
			<fb:comments xid="member_how_blasting_works" href="/member/how-blasting-works" width='550px'></fb:comments>
		</div>
	</div>
	
	
	<div class="grid_5">
		<div class="form clearfix">
	
	
			<h3 style="color: #808080;">Can I Blast an exclusive listing, that is not on my board's listing service?</h3>
			<p>Yes. Any property offering a reasonable cooperating commission may be featured on <?=COMPANY_NAME;?>.</p>	
	
			<h3 style="color: #808080; margin-top: 30px;">How many times does my property Blast go out?</h3>
			<p>Individual Blasts are published once. They remain on the accounts of the agent's indefinitely, but are slowly pushed down from their featured position as time goes by.</p>
	
			<h3 style="color: #808080; margin-top: 30px;">Can I feature a link to a virtual tour or my website in my Blast?</h3>
			<p>No. No outside links or contact information are permitted within the content of property Blasts.</p>
	
			<h3 style="color: #808080; margin-top: 30px;">How locally-focused is <?=COMPANY_NAME;?>?</h3>
			<p>It depends on your market. If you are in a major market, <?=COMPANY_NAME;?> content will be more geographically focused on a smaller area. As a simple rule: where the number of agents on the service is higher, <?=COMPANY_NAME;?> will always be more geographically defined.</p>
	
	
	
			<h3 style="color: #808080;">Help I'm still completely lost!</h3> 
			
			<p>Call us at <span style="color: #333333;">1-888-712-7888</span>
			&nbsp;<span style="font-size: 14px; color: #3D6DCC">(*9:00 am to 5:00 pm EST)</span>
			
		</div>
	</div>
</div>


**************/?>


<div style="display: none;">
	
	<div id="blast_video_popup" style="position: absolute;">
		<?/*<iframe width="853" height="480" src="//www.youtube.com/embed/M_6C58tJ5Y0?rel=0" frameborder="0" allowfullscreen></iframe>*/ ?>			
		<div id="blasting_video"></div>
	</div>
	
</div>

<div style="display: none;">
	
	<div id="facebook_screenshot">
		 <img src="/images/facebook_blasting_screenshot.jpg" width="1060" height="714" />
	</div>
	
</div>

<div style="display: none;">
	
	<div id="listing_screenshot">
		 <img src="/images/listing_screenshot.jpg" width="840" height="515" />
	</div>
	
</div>




<div style="display: none;">
	<div id="gotcha_popup_div">
		<h1>Made You Click!</h1>
		<p>You've just seen the power of inbound marketing for yourself!</p>
	</div>
</div>

			
<script type="text/javascript">

	$(document).ready(function(){
		//fix position of blasting banner div for IE7
		if($.browser.msie && $.browser.version < 8){
				//$('#blast_video_container').css({
				//								left: '-40px',
				//								top: '-20px',
				//								marginBottom: '-20px'
				//							});
				$('.box:eq(0)').css({
									'padding-bottom': '0px',
									'margin-bottom': '40px'
								});
			}

		$('#help_div_button').click(function(event)
		{
			event.preventDefault();
			$('#help_div').toggle();
		});

		
		$(".gotcha_popup").click(function(e)
		{
			e.preventDefault();
			$.colorbox({height: "500px",inline:true, href:"#gotcha_popup_div",onClosed:function(){ $('#gotcha_popup_div').empty(); }});
		});
		
		$('.facebook_screenshot_link').click(function(event){
			event.preventDefault();
			$.colorbox({inline:true, href:"#facebook_screenshot"});
		});
		
		$('.listing_screenshot_link').click(function(event){
			event.preventDefault();
			$.colorbox({inline:true, href:"#listing_screenshot"});
		});



		<? echo $this->render('fb-comments.html.php'); ?>

	
	
	});


	var flashvars = {};
		flashvars.file = "/video/BlastingNew.flv";
		flashvars.skin = "/video/mediaplayer/cb/cb.xml";
		flashvars.smoothing = true;
		flashvars.stretching = "exactfit";
		flashvars.autostart = true;
		flashvars.volume = 100;
	
	var params = {};
		params.menu = false;
		params.allowfullscreen = true;
		params.allowscriptaccess = true;
		params.wmode = "opaque";
	
	var attributes = {};
		attributes.id = "blasting_video";
		attributes.name = "blasting_video";
		
	function show_blast_video(){
		swfobject.embedSWF("/video/mediaplayer/player.swf", "blasting_video", "620", "380", "9.0.0","/scripts/mediaplayer/expressInstall.swf", flashvars, params, attributes);
	}
		
	var player = null; 
	function playerReady(thePlayer) {
		player = window.document[thePlayer.id];
		addListeners();
	}
	
	function addListeners() {
		if (player) { 
			player.addModelListener("STATE", "stateListener");
		} else {
			setTimeout("addListeners()",100);
		}
	}
	
	function stateListener(obj) { //IDLE, BUFFERING, PLAYING, PAUSED, COMPLETED
		currentState = obj.newstate; 
		previousState = obj.oldstate; 
	
		console.log("current state: " + currentState + "<br>previous state: " + previousState); 
		
		if(previousState == "BUFFERING" && currentState == "PLAYING"){
			$('#blast_video_still').css('display', 'none').delay(1000);
		}
		
		if(previousState == "PLAYING" && currentState == "IDLE"){
			$('#blast_video_still').css('display', 'block');
		}
	}
	
</script>


<!-- Google Code for CityBlast /index/how-blasting-works Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 956260748;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "IL1qCITeowMQjMP9xwM";
var google_conversion_value = 0;
if (49) {
  google_conversion_value = 49;
}
/* ]]> */
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/956260748/?value=49&amp;label=IL1qCITeowMQjMP9xwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<style>
	script[src*="googleadservices.com/pagead/conversion.js"]+img {display: none !important;}
</style>