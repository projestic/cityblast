	<div class="grid_8">
		
		<div class="box gray clearfix">
		
			<h1>Feedback</h1>
			
			<h3>Your Feedback is Important.</h3>
			<p><?=COMPANY_NAME;?> aims to always improve its service.  Was there a feature that would have helped with your Blast?  Do you require technical assistance? Do you want to recommend a content provider? No matter, please feel free to share your thoughts.</p>
			
			<form action="/index/feedback-submit" method="POST">

				<?
	/*******************************************************                
	<div class="grid_6">
		<div class="your_feed">
			
			<div class="was_service">
				<h3>Was the service easy to understand?</h3>
                    
                    <div class="aroo_div">
                   		<div class="very_one">
                       		<label class="radio_leble"><input type="radio" id="q1" name="q1" /></label><label class="tex_radio_leble">Very Easy</label>
                      	</div>
          		
          			<div class="very_two">
                         	<label class="radio_leble"><input type="radio" id="q2" name="q3" /></label><label class="tex_radio_leble">Easy</label>         
                      	</div>
                      	
                      	<div class="very_three">
                       		<label class="radio_leble"><input type="radio" id="q3" name="q3" /></label><label class="tex_radio_leble">Average</label>
                      	</div>
                      	
                      	<div class="very_four">
                        		<label class="radio_leble"><input type="radio" id="q4" name="q4" /></label><label class="tex_radio_leble">Poor</label>
                      	</div>
          			
          			<div class="very_five">
                         	<label class="radio_leble"><input type="radio" id="q5" name="q5" /></label><label class="tex_radio_leble">Extremely Poor</label>
                      	</div>
				</div>  
			</div>
         </div>

	</div>



	<div class="grid_6">
		<div class="your_feed">
			
			<div class="was_service">
				<h3>Question 2?</h3>
                    
                    <div class="aroo_div">
                   		<div class="very_one">
                       		<label class="radio_leble"><input type="radio" id="q1" name="q1" /></label><label class="tex_radio_leble">Very Easy</label>
                      	</div>
          		
          			<div class="very_two">
                         	<label class="radio_leble"><input type="radio" id="q2" name="q3" /></label><label class="tex_radio_leble">Easy</label>         
                      	</div>
                      	
                      	<div class="very_three">
                       		<label class="radio_leble"><input type="radio" id="q3" name="q3" /></label><label class="tex_radio_leble">Average</label>
                      	</div>
                      	
                      	<div class="very_four">
                        		<label class="radio_leble"><input type="radio" id="q4" name="q4" /></label><label class="tex_radio_leble">Poor</label>
                      	</div>
          			
          			<div class="very_five">
                         	<label class="radio_leble"><input type="radio" id="q5" name="q5" /></label><label class="tex_radio_leble">Extremely Poor</label>
                      	</div>
				</div>  
			</div>
         </div>

	</div>



	<div class="grid_6">
		<div class="your_feed">
			
			<div class="was_service">
				<h3>Question 3?</h3>
                    
                    <div class="aroo_div">
                   		<div class="very_one">
                       		<label class="radio_leble"><input type="radio" id="q1" name="q1" /></label><label class="tex_radio_leble">Very Easy</label>
                      	</div>
          		
          			<div class="very_two">
                         	<label class="radio_leble"><input type="radio" id="q2" name="q3" /></label><label class="tex_radio_leble">Easy</label>         
                      	</div>
                      	
                      	<div class="very_three">
                       		<label class="radio_leble"><input type="radio" id="q3" name="q3" /></label><label class="tex_radio_leble">Average</label>
                      	</div>
                      	
                      	<div class="very_four">
                        		<label class="radio_leble"><input type="radio" id="q4" name="q4" /></label><label class="tex_radio_leble">Poor</label>
                      	</div>
          			
          			<div class="very_five">
                         	<label class="radio_leble"><input type="radio" id="q5" name="q5" /></label><label class="tex_radio_leble">Extremely Poor</label>
                      	</div>
				</div>  
			</div>
         </div>

	</div>




	<div class="grid_6">
		<div class="your_feed">
			
			<div class="was_service">
				<h3>Question 4?</h3>
                    
                    <div class="aroo_div">
                   		<div class="very_one">
                       		<label class="radio_leble"><input type="radio" id="q1" name="q1" /></label><label class="tex_radio_leble">Very Easy</label>
                      	</div>
          		
          			<div class="very_two">
                         	<label class="radio_leble"><input type="radio" id="q2" name="q3" /></label><label class="tex_radio_leble">Easy</label>         
                      	</div>
                      	
                      	<div class="very_three">
                       		<label class="radio_leble"><input type="radio" id="q3" name="q3" /></label><label class="tex_radio_leble">Average</label>
                      	</div>
                      	
                      	<div class="very_four">
                        		<label class="radio_leble"><input type="radio" id="q4" name="q4" /></label><label class="tex_radio_leble">Poor</label>
                      	</div>
          			
          			<div class="very_five">
                         	<label class="radio_leble"><input type="radio" id="q5" name="q5" /></label><label class="tex_radio_leble">Extremely Poor</label>
                      	</div>
				</div>  
			</div>
         </div>

	</div>


	<div class="grid_6">
		<div class="your_feed">
			
			<div class="was_service">
				<h3>Question 5?</h3>
                    
                    <div class="aroo_div">
                   		<div class="very_one">
                       		<label class="radio_leble"><input type="radio" id="q1" name="q1" /></label><label class="tex_radio_leble">Very Easy</label>
                      	</div>
          		
          			<div class="very_two">
                         	<label class="radio_leble"><input type="radio" id="q2" name="q3" /></label><label class="tex_radio_leble">Easy</label>         
                      	</div>
                      	
                      	<div class="very_three">
                       		<label class="radio_leble"><input type="radio" id="q3" name="q3" /></label><label class="tex_radio_leble">Average</label>
                      	</div>
                      	
                      	<div class="very_four">
                        		<label class="radio_leble"><input type="radio" id="q4" name="q4" /></label><label class="tex_radio_leble">Poor</label>
                      	</div>
          			
          			<div class="very_five">
                         	<label class="radio_leble"><input type="radio" id="q5" name="q5" /></label><label class="tex_radio_leble">Extremely Poor</label>
                      	</div>
				</div>  
			</div>
         </div>

	</div>

	<div class="grid_6">
		<div class="your_feed">
			
			<div class="was_service">
				<h3>Question 6?</h3>
                    
                    <div class="aroo_div">
                   		<div class="very_one">
                       		<label class="radio_leble"><input type="radio" id="q1" name="q1" /></label><label class="tex_radio_leble">Very Easy</label>
                      	</div>
          		
          			<div class="very_two">
                         	<label class="radio_leble"><input type="radio" id="q2" name="q3" /></label><label class="tex_radio_leble">Easy</label>         
                      	</div>
                      	
                      	<div class="very_three">
                       		<label class="radio_leble"><input type="radio" id="q3" name="q3" /></label><label class="tex_radio_leble">Average</label>
                      	</div>
                      	
                      	<div class="very_four">
                        		<label class="radio_leble"><input type="radio" id="q4" name="q4" /></label><label class="tex_radio_leble">Poor</label>
                      	</div>
          			
          			<div class="very_five">
                         	<label class="radio_leble"><input type="radio" id="q5" name="q5" /></label><label class="tex_radio_leble">Extremely Poor</label>
                      	</div>
				</div>  
			</div>
         </div>

	</div>


	





	
	<div class="clearfix"></div>

	<div class="grid_12" style="height: 20px;"></div>
	
	<div class="grid_6">

          <div class="was_service_mess">
               <h3>What (if anything) would you change about the service:</h3>
               <div class="aroo_div">
              	  	<label class="arro_message_box"><textarea class="textarea_as" cols="10" rows="10"></textarea></label>
             	</div>  

         	</div>
	
	</div>
               
	<div class="grid_6">

          <div class="was_service_mess">
               <h3>Other comments:</h3>
               <div class="aroo_div">
              	  	<label class="arro_message_box"><textarea class="textarea_as" cols="10" rows="10"></textarea></label>
             	</div>  

         	</div>
	
	</div>

	******************************/
	?>
				
				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Your Feedback</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname" style="width: 160px">Message</li>
							<li class="field" style="width: 440px">
								<textarea class="uiTextarea" style="width: 432px; height: 200px;" name="feedback_comments"></textarea>
							</li>
						</ul>
					</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons">
								<input type="submit" value="Send" class="uiButton right" id="send_id" />
								<input type="reset" value="Reset" class="uiButton right cancel" />
							</li>
						</ul>
					</li>
				</ul>
				
			</form>
			
		</div>
		
	</div>
	
	<div class="grid_4">
		
		<div class="box clearfix">
		
			<h4>Email Member Support</h4>
			<p>Or if you prefer, email Member Support at <a href="mailto:<?=HELP_EMAIL;?>"><?=HELP_EMAIL;?></a>!</p>
			<p>Either way, <?=COMPANY_NAME;?> wants to hear from you.</p>
			
		</div>
		
	</div>