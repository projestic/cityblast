	<div class="grid_8">
	
		<div class="box clearfix">
	
			<div class="box_section">
				<h1>Agents - Welcome to <?=COMPANY_NAME;?>.</h1>
			</div>
						
			<p><?=COMPANY_NAME;?> is the world's first and largest Social Media Listing Service&#8482;, and is the new way that the real estate industry is sharing property information.  In the 
			beginning of the internet age, online Listing Services allowed agents and clients to search and share real estate info, gradually connecting buyers and sellers.  This basic approach used to suffice, 
			but times have changed.</p>
			
			
			<p>With the massive rise in usage of Facebook and other social media communities, internet users have shifted their online focus away from third-party websites, demanding that information be available 
			to them directly within the social media sphere.  In short?  They aren't leaving Facebook to get their info and updates &#8211 the updates need to come to Facebook.  This is why nearly every 
			Fortune 500 company has an intense online presence within these communities.  So should you.</p>


			<h3>Finally, real estate has made the shift.</h3>
			<p>Gone are the days of the traditional Listing Service.  <?=COMPANY_NAME;?> is here.  Real estate agents who wish to reach the over 1-billion Facebook, Twitter and LinkedIn users are now using CityBlas<?=COMPANY_NAME;?>t
			to share their real estate information, sell their properties, and gain legions of loyal clients.  As for the other agents?  Their business is out of date, and unfortunately will soon be out of sight 
			for the majority of real estate clients.</p>


			<? /*border-top: 1px dotted #CCCCCC;*/ ?>

			<h1>Who we are</h1>
			<p>Shaun and Alen have been good friends for over 10 years. Shaun has always been a natural salesperson with great people skills. Alen has always been extremely analytical with a flair for creative 
			thinking within the confines of a technical environment. But the glue that binds them has always been hard work and ambition. Coming from two sides of the same coin, the partnership has been extremely 
			successful because of the natural division of responsibilities. The two of them also make a handsome pair. Don't you think?</p>

			<h2>Shaun Nilsson, Co-Founder and CEO</h2>
			<p><img src="/images/shaun-nilsson.jpg" style="float: left; padding-right: 10px;">Shaun is Co-Founder and Chief Executive Officer of <?=COMPANY_NAME;?>, which was founded in 2011 along with long-time partner Alen Bubich.  Shaun is responsible for setting the overall direction and product 
			strategy for the company.  He created the concept for <?=COMPANY_NAME;?> after searching for a time-saving automation solution for his lucrative real estate social media marketing system, and hearing of a 
			similar product that Mr. Bubich was in the process of developing. </p>
			
			<p>Shaun leads the development of all client-facing systems, marketing and new feature development for <?=COMPANY_NAME;?>.  He studied at the University of Toronto before moving on to real estate sales and marketing.  
			He has been a licensed real estate agent in Ontario since 2007, and travels North America teaching agents about social media and online marketing.  Shaun has appeared on radio, television, online and in print, 
			speaking about his unique social media marketing philosophy.</p>
			
			<h2>Alen Bubich, Co-Founder and CTO</h2>
			<p><img src="/images/alen_bubich.jpg" style="float: left; padding-right: 10px;">Alen is the other Co-Founder of <?=COMPANY_NAME;?> and is the Chief Technology Officer. With over 15 years of software development experience at such world class companies as ScotiaBank, National Post, SinglesNet, 
			Verizon, Ogilvy and WebKinz, Alen is responsible for setting the overall technical direction of the company as well as heading up all online marketing initiatives.</p>
			
			<p><?=COMPANY_NAME;?> is based on the <a href="http://www.socialhp.com" target="_new">socialHP.com</a> publishing platform which was designed specifically to meet these types of needs. The practical application of this vertical was a natural extension for the product.</p>
			
			<p>Alen received his Bachelor of Arts in Computing and Information Science from the University of Guelph in 1997.</p>
			
			

			
			
		</div>
					
	</div>
	
	<div class="grid_4">



		<div class="box clearfix" style="padding-top: 1px;">
			<h4>Fortunately, <?=COMPANY_NAME;?> is easy.</h4>
			<p>To keep your real estate business on the cutting edge, simply <a href="/member/index">register your free <?=COMPANY_NAME;?> membership</a>.</p> 
				
				<? /*There is no cost, catch or gimmick.  Once you're registered, you can learn more about maximizing your results by checking out the following short tutorials:</p>*/?>
			
			
			<?/*<div style="padding-left: 25px; padding-right: 25px;">
			<p><a href="/index/how-blasting-works">How Blasting Works</a> &#8211 Create your Blast, and sell your listing fast.</p>*/?>
			
			<p><a href="/member/how-clientfinder-works">Learn how Agents in your area use ClientFinder</a> to attract new clients every day &#8211 free.</p>
			
			<?/*</div>*/?>
			
			<p style="text-align: right;"><a href="/member/join/" class="right">Register Free</a></p>
		</div>
			
		<div class="box clearfix" style="padding-top: 1px;">
										
			<h4>Have a Listing? Compose Your Blast, and Sell It Fast. </h4>
			<p>When you Blast your listing with <?=COMPANY_NAME;?>, you harness the cumulative power of your city's entire social media network to sell your property fast.  You reach tens of thousands of local buyers through these online communities by advertising your listing on hundreds of other local agents' personal Facebook, Twitter and LinkedIn accounts for all of their friends to see. </p>
	
			<p>And best of all?  <a href="/index/guarantee">It's guaranteed</a>.  So, what are you waiting for? </p>
	
			<p style="text-align: right;"><a href="/index">Create My Blast</a></p>
								
		</div>
	
		<div class="box clearfix" style="padding-top: 1px;">
			
			<h4>How Blasting Works</h4>
			<p><?=COMPANY_NAME;?> is the new way that the real estate industry is sharing property information. Simply create your Blast, and sell your listing fast. </p>
			<p style="text-align: right;"><a href="/index/how-blasting-works">Learn more</a></p>
			
		</div>
		
	</div>
	
