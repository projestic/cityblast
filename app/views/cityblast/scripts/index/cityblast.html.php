			<div class="grid_12">
			
				<div class="box" style="padding: 20px 40px;">
			
					<div style="width: 465px; height: 108px; margin: -20px -40px 20px; padding: 262px 0 0 535px; background-image: url(/images/clientfinder_banner_bg.jpg);">
						
						<a href="javascript:void(0);" id="clientfinder_video_btn"><img src="/images/blank.gif" width="90" height="60" class="video_btn" /></a>
						<a href="/member/join"><img src="/images/blank.gif" width="280" height="60" class="connect_fb_btn" /></a>
					
					</div>
					
					<div class="key_features clearfix">
						
						<div class="key_feature">
							
							<h3>Your Stellar Image.</h3>
							
							<p>Today, looking professional on Facebook is as vital to your business as looking professional in the outside world.  By keeping your Facebook consistently up-to-date with first class real estate content and gorgeous local listings, your friends and acquaintances will know you're a trusted professional.</p>
							
							<img src="/images/blank.gif" width="50" height="50" class="image icon" />
						
						</div>
						
						<div class="key_feature">
							
							<h3>Increased Commissions.</h3>
							
							<p>ClientFinder makes you more money.  A lot more.  By keeping your Facebook consistently up-to-date with first class real estate content and gorgeous local listings, you'll turn your friends and casual acquaintances into loyal clients.  And best of all?  You don't have to pressure.  They'll come to you.</p>
							
							<img src="/images/blank.gif" width="50" height="50" class="commissions icon" />
						
						</div>
						
						<div class="key_feature">
							
							<h3>Zero Maintenance.</h3>
							
							<p>ClientFinder's user-friendly setup takes under 30 seconds, and doesn't require another annoying account or password.  Connect effortlessly with one click, and tailor your personal settings.  ClientFinder does the rest.  Simply welcome your incoming leads, and build new client relationships.  It's that easy.</p>
							
							<img src="/images/blank.gif" width="50" height="50" class="maintenance icon" />
						
						</div>
						
					</div>
					
					<div class="action_heading">Ready to upgrade your online image? <a href="#">Click here</a> to set up ClientFinder - FREE</div>
					
					<div class="clearfix" style="margin: 20px 0;">
					
						<div style="width: 470px; float: left; font-size: 12px;">
							
							<h4>Why Use ClientFinder?</h4>
							
							<p>ClientFinder turns your Facebook, Twitter or LinkedIn into an incredibly powerful real estate machine, keeping your image professional and current, and generating vastly more business from your personal online network.</p>
							
							<ul>
								<li>enjoy a first class online presence, and a lucrative top-producer image</li>
								<li>welcome legions of brand new clients and higher monthly commissions</li>
								<li>effortlessly project the confident business savvy of a current real estate professional</li>
							</ul>
							
							<p>Best of all?  ClientFinder is completely free, and never requires maintenance.  Simply set up your ClientFinder once, and watch your real estate business grow.</p>

							<h4>How Does it Work?</h4>
							
							<ol>
								<li style="margin-bottom: 10px;">Simply register your free ClientFinder app. Follow the on-screen instructions to activate your Facebook, Twitter and LinkedIn.
								</li>
								<li style="margin-bottom: 10px;">As often as you choose, ClientFinder will automatically update your Facebook, Twitter or LinkedIn with first class real estate articles, videos and gorgeous local listings. These polished updates look just like you've created them yourself, and are visible to your network of friends and followers.
								</li>
								<li>Your friends will see your professional tips and hot featured listings, and email or message you with questions, comments or to book showings. You simply welcome their inquiries, build your client relationships, and earn more commissions.</li>
							</ol>
						</div>
					
						<div style="width: 410px; float: left; margin-left: 40px;">
							
							<h4>Testimonials</h4>
							
							<div style="padding-left: 70px; margin-bottom: 20px; position: relative; font-size: 12px;">
								
								<h6 style="padding-bottom: 3px; border-bottom: solid 1px #e5e5e5;">Leila Talibova, <span style="font-size: 11px; color: #4d4d4d;">HomeLife Victory Realty Inc.</span></h6>
								<p>My friends and previous colleagues noticed my Facebook updates right away. In only my first week using <?=COMPANY_NAME;?>, I received a call from a family friend who said she'd seen my Facebook and wanted list their home with me! They loved when I said I'd use <?=COMPANY_NAME;?> to market their home - it sold in just days!</p>
							
								<img src="/images/blank.gif" width="50" height="50" class="icon leila" />
								
							</div>
							
							<div style="padding-left: 70px; margin-bottom: 20px; position: relative; font-size: 12px;">
								
								<h6 style="padding-bottom: 3px; border-bottom: solid 1px #e5e5e5;">Shaun Nilsson, <span style="font-size: 11px; color: #4d4d4d;"><?=COMPANY_NAME;?> Founder</span></h6>
								<p>When I began in real estate, I didn't have any clients or leads. I began asking more established agents if I could post their listings on my Facebook Wall to attract buyers. What a revelation! I was able to make $165,000 in commissions by doing this - in my first year!  That's where <?=COMPANY_NAME;?> was born.</p>
							
								<img src="/images/blank.gif" width="50" height="50" class="icon shaun" />
								
							</div>
							
							<div style="padding-left: 70px; position: relative; font-size: 12px;">
								
								<h6 style="padding-bottom: 3px; border-bottom: solid 1px #e5e5e5;">Luke McKenzie, <span style="font-size: 11px; color: #4d4d4d;">Royal LePage Signature Realty Inc.</span></h6>
								<p>Our whole team is thrilled with the amount of exposure <?=COMPANY_NAME;?> gives us. We've really been impressed with their innovative sales tools. We Blast every listing!</p>
							
								<img src="/images/blank.gif" width="50" height="50" class="icon luke" />
								
							</div>
							
						</div>
						
					</div>
					
					<div class="action_heading">Who has time to consistently update their Facebook? <a href="#">ClientFinder</a> has you covered.</div>
					
					<div class="clearfix" style="margin: 20px 0;">
					
						<div style="width: 400px; float: left; font-size: 12px;">
							
							<h4>Wondering How The Updates Appear?</h4>
							
							<p>This is what an actual ClientFinder update looks like on your Facebook.  Every single update is manually reviewed by <?=COMPANY_NAME;?>'s real human editors for professional content, spelling and grammar.</p>
							
							<ul>
								<li>Clean</li>
								<li>Professional</li>
								<li>Current</li>
								<li>Trusted Sources</li>
							</ul>
						</div>
					
						<div style="width: 480px; margin-left: 40px; float: left; height: 165px;">
							
							<img src="/images/facebook_screenshot_thumb.jpg" width="480" height="145" />
							
							<span style="font-size: 12px; display: block; line-height: 30px; text-align: center;"><a href="javascript:void(0);" id="facebook_screenshot_link">Enlarge Screenshot</a></span>
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			
			<div style="display: none;">
				
				<div id="clientfinder_video_popup">
			
					 <div id="clientfinder_video"></div>
			
				</div>
				
			</div>
			
			<div style="display: none;">
				
				<div id="facebook_screenshot">
			
					 <img src="/images/facebook_screenshot.jpg" width="1060" height="714" />
			
				</div>
				
			</div>

<script type="text/javascript" src="/js/swfobject.js"></script>
			
<script type="text/javascript">

	$(document).ready(function(){
		
		$('#clientfinder_video_btn').click(function(event){
			event.preventDefault();
			$.colorbox({width:"700px", height: "475px", inline:true, href:"#clientfinder_video_popup"});
			show_clientfinder_video();
		});
		
		$('#facebook_screenshot_link').click(function(event){
			event.preventDefault();
			$.colorbox({inline:true, href:"#facebook_screenshot"});
			show_blast_video();
		});
	
	});

	var flashvars = {};
		flashvars.file = "/video/ClientFinder.f4v";
		flashvars.skin = "/video/mediaplayer/cb/cb.xml";
		flashvars.smoothing = true;
		flashvars.stretching = "exactfit";
		flashvars.autostart = true;
		flashvars.volume = 100;
	
	var params = {};
		params.menu = false;
		params.allowfullscreen = true;
		params.allowscriptaccess = true;
		params.wmode = "opaque";
	
	var attributes = {};
		attributes.id = "clientfinder_video";
		attributes.name = "clientfinder_video";
		
	function show_clientfinder_video(){
		swfobject.embedSWF("/video/mediaplayer/player.swf", "clientfinder_video", "620", "380", "9.0.0","/scripts/mediaplayer/expressInstall.swf", flashvars, params, attributes);
	}
	
</script>