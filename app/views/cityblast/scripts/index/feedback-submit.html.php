	<div class="grid_12">
		
		<div class="box gray clearfix">
		
			<h1>Thanks for the Feedback</h1>
			
			<h3>Your Feedback is Appreciated.</h3>
			
			<p>Your opinion is important to us. Thanks for taking the time to share it with us. We'll get back to you as soon as possible.</p>
			
		</div>
		
	</div>