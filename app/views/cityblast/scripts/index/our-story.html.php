<div class="contentBox">
	
	<div class="contentBoxTitle">
		<h1>Meet The Team</h1>			
	</div>


	<div class="clearfix" style="margin-top: 30px;">
		<img src="/images/our-story/our-team.png" />
	</div>

</div>


<div class="contentBox">
	
	<div class="contentBoxTitle">
		<h1>Our Story</h1>
	</div>
	
	
	<div class="clearfix">
		<div style="float: right; margin: 0 0 0 30px; padding: 30px 0; background: #f8f8f8; border: #e5e5e5 solid 1px; width: 280px;">
			<h2 style="text-align: center; font-size: 54px; margin: 0 0 20px; color: #004ecc;"><?=number_format($this->stats->total_posts);?></h2>
			<h5 style="text-align: center; color: #004ecc;">Total Social Media Posts</h5>
		</div>
		

		<p style="margin-top: 30px;">It's pretty simple.  At CityBlast we only do one thing: social media posts.   In fact, we've done <?=number_format($this->stats->total_posts);?> of them.</p>
		
		<p><i>And when you do something that many times, you tend to get pretty good at it.</i></p>
		
		
	</div>

	<div class="clearfix" style="margin-top: 25px;">
		<div style="float: left; margin: 0 30px 0 0; padding: 30px 0; background: #f8f8f8; border: #e5e5e5 solid 1px; width: 280px;">
			<h2 style="text-align: center; font-size: 54px; margin: 0 0 20px; color: #f96e24"><?=number_format($this->total_count);?></h2>
			<h5 style="text-align: center; color: #f96e24;">Total Social Media Reach</h5>
		</div>
		

		<p style="margin-top: 30px;">We post updates to Facebook, Twitter and LinkedIn accounts, so our members don't have to. And we've helped thousands of agents and brokerages across North America 
			improve their social media presence by providing expert, consistent content that their friends and followers love.</p>

	</div>

	<div class="clearfix" style="margin-top: 25px;">
		<div style="float: right; margin: 0 0 0 30px; padding: 30px 0; background: #f8f8f8; border: #e5e5e5 solid 1px; width: 280px;">
			<h2 style="text-align: center; font-size: 54px; margin: 0 0 20px; color: #000000;"><?=number_format($this->stats->total_clicks);?></h2>
			<h5 style="text-align: center; color: #000000;">CityBlast's Total Post Reads</h5>
		</div>
		
	
		<p style="margin-top: 24px;">Did you know that CityBlast reaches over <strong>3 TIMES</strong> more potential buyers and sellers every day through social media than <i>Zillow, Trulia, NAR and AirBnB <strong>combined?</strong></i></p>
			
		<p>Our posted articles have been read over <?=number_format($this->stats->total_clicks);?> times.</p>
	
	</div>


	<?/*
	<div class="clearfix">
		
		<div style="float: right; margin: 0 0 0 30px; padding: 30px 0; background: #f8f8f8; border: #e5e5e5 solid 1px; width: 280px;" class="clearfix">
			<h2 style="text-align: center; font-size: 54px; margin: 0 0 20px;"><?=number_format(	($this->stats->total_interactions + $this->stats->total_bookings) );?></h2>
			<h5 style="text-align: center; color: #999;">Automatic Buyer Leads Sent</h5>
			<hr />
			<h2 style="text-align: center; font-size: 54px; margin: 0 0 20px;"><?=number_format(	($this->stats->total_likes + $this->stats->total_comments));?></h2>
			<h5 style="text-align: center; color: #999;">Facebook Generated Leads</h5>
			<hr />
			<h2 style="text-align: center; font-size: 54px; margin: 0 0 20px;"><?=number_format($this->total_count);?></h2>
			<h5 style="text-align: center; color: #999;">Daily Post Reach</h5>
			<div style="text-align: center; margin: 10px 0 0; font-size: 24px; color: #999;"><i class="icon-arrow-down"></i></div>
			<h2 style="text-align: center; font-size: 36px; margin: 0 0 10px;"><?=number_format(	($this->facebook_count + $this->fanpages_count)	);?></h2>
			<h6 style="text-align: center; color: #999; margin-bottom: 0;">Daily Facebook Reach</h6>
			<div style="float: left; width: 140px;">
				<div style="text-align: left; margin: -5px 0 10px 30px; font-size: 16px; color: #999;"><i class="icon-arrow-down"></i></div>
				<h4 style="text-align: center; font-size: 28px; margin: 0 0 10px;"><?=number_format($this->twitter_count);?></h4>
				<h6 style="text-align: center; color: #999;">Daily Twitter Reach</h6>
			</div>
			<div style="float: left; width: 140px;">
				<div style="text-align: right; margin: -5px 30px 10px 0; font-size: 16px; color: #999;"><i class="icon-arrow-down"></i></div>
				<h4 style="text-align: center; font-size: 28px; margin: 0 0 10px;"><?=number_format($this->linkedin_count);?></h4>
				<h6 style="text-align: center; color: #999;">Daily LinkedIn Reach</h6>
			</div>
		</div>

	</div>
	*/ ?>
	

	<div class="clearfix" style="margin-top: 25px;">
		<div style="float: left; margin: 0 30px 0 0; padding: 30px 0; background: #f8f8f8; border: #e5e5e5 solid 1px; width: 280px;">
			<h2 style="text-align: center; font-size: 54px; margin: 0 0 20px; color: #605ca8;"><?=number_format(	($this->stats->total_interactions + $this->stats->total_bookings + $this->stats->total_likes + $this->stats->total_comments) );?></h2>
			<h5 style="text-align: center; color: #605ca8;">Total Leads Generated</h5>
		</div>
		
		<p>				

		<p style="margin-top: 14px;">CityBlast has helped more real estate agents generate more leads though social media than any other company on the planet!</p>
		<p>CityBlast.com has sent out <?=number_format(	($this->stats->total_interactions + $this->stats->total_bookings) );?>* automatic buyer leads to our members, from the website alone. We've also 
			generated <?=number_format(	($this->stats->total_likes + $this->stats->total_comments));?> leads on our members' Facebook Pages, and today alone, our posts will be seen by 
			over <?=number_format($this->total_count);?> potential buyers and sellers.</p>

	</div>
	

	<div class="clearfix" style="margin-top: 25px;">
		<div style="float: right; margin: 0 0 0 30px; padding: 30px 0; background: #f8f8f8; border: #e5e5e5 solid 1px; width: 280px;">		
			<h2 style="text-align: center; font-size: 54px; margin: 0 0 20px; color: #ff2c29">1,289,666</h2>
			<h5 style="text-align: center; color: #ff2c29;">CityBlast Unique Visitors</h5>
		</div>
		
		
		<p style="margin-top: 30px;">And we've sold a property or two in our day, also.  In fact, 1,289,666 potential buyers have viewed properties for sale on CityBlast.</p>
		<p>That's an average of 3,533 people every single day. A whole lot of real estate browsing.</p>
	
	</div>
	
	
	<div class="clearfix" style="margin-top: 25px;">
		<div style="float: left; margin: 0 30px 0 0; padding: 30px 0; background: #f8f8f8; border: #e5e5e5 solid 1px; width: 320px;">		
			<h2 style="text-align: center; font-size: 54px; margin: 0 0 20px; color: #009940;">$<?=number_format(	152000 *0.025*0.03*($this->stats->total_interactions + $this->stats->total_bookings)	);?></h2>
			<h5 style="text-align: center; color: #009940;">Revenue Generated</h5>
		</div>
		
		<p style="margin-top: 32px;">And for those who like a "dollars and cents" approach, based on recent data provided by the National Association of Realtors, we estimate that we'll generate over
		$<?=number_format(	152000 *0.025*0.03*($this->stats->total_interactions + $this->stats->total_bookings)	);?> in commissions for our members in this calendar year.</p>
		
	
	</div>

	
	<div class="clearfix" style="margin-top: 35px; text-align: center;">

		<h5 style="font-weight: normal;">Simply put, CityBlast has helped more real estate agents find more new clients though social media than any other company on the planet.  Period.</h5>

	</div>

<? /**

	<div class="clearfix" style="margin-top: 30px;">

		<p>Pretty cool, huh?</p>
		<p>Scroll down to meet the people who are working hard to make you look good!</p>
		
	</div>
	
	
	<div style="text-align: center; background: #808080; margin: 20px 30px 20px 0; padding: 20px 0; color: #fff; overflow: hidden;">
		<?=number_format(	($this->facebook_count + $this->fanpages_count)	);?> from Facebook &nbsp;&nbsp;
		<i class="icon-plus-sign-alt"></i>&nbsp;&nbsp; <?=number_format($this->twitter_count);?> from Twitter &nbsp;&nbsp;
		<i class="icon-plus-sign-alt"></i>&nbsp;&nbsp; <?=number_format($this->linkedin_count);?> from LinkedIn 
	</div>
	
	<? /*
		<p>That includes <?=number_format(	($this->facebook_count + $this->fanpages_count)	);?> of our members' awesome friends and fans on Facebook...</p>
		<p><i class="icon-plus-sign-alt"></i> <?=number_format($this->twitter_count);?> of their hip and cool Twitter followers</p> 
		<p><i class="icon-plus-sign-alt"></i> plus <?=number_format($this->linkedin_count);?> business-types on LinkedIn</p>
	*/ ?>

<? /*
	
	<p>And we've sold a property or two in our day, also.  In fact, 1,289,666 potential buyers have viewed properties for sale on CityBlast.</p>
	<p>That's an average of 3,533 people every single day. A whole lot of real estate browsing.</p>
	
	<div style="text-align: center; background: #808080; margin: 20px 30px 20px 0; padding: 20px 0; color: #fff; overflow: hidden;">
		<i style="color: #b3b3b3;" class="icon-male"></i> <i style="color: #ccc;" class="icon-male"></i> <i style="color: #e5e5e5;" class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i class="icon-male"></i> <i style="color: #e5e5e5;" class="icon-male"></i> <i style="color: #ccc;" class="icon-male"></i> <i style="color: #b3b3b3;" class="icon-male"></i>
	</div>
	
	<p>And for those who like a "dollars and cents" approach, based on recent data provided by the National Association of Realtors, we estimate that we'll generate over
	$<?=number_format(	152000 *0.025*0.03*($this->stats->total_interactions + $this->stats->total_bookings)	);?> in commissions for our members in this calendar year.</p>
	<p>Simply put, CityBlast has helped more real estate agents find more new clients though social media than any other company on the planet.  Period.</p>
		
	<p>Pretty cool, huh?</p>
	<p>Scroll down to meet the people who are working hard to make you look good!</p>
	
	
	
	<p>SIDEBAR</p>
	<p>	

		Pie Chart<br><br>
		
		New Visitors: 64.7%<br>
		Returning Visitors: 35.3%<br>

	<br><br>	
		
		"HUMAN Touch" Chart<br><br>
		Facebook: <?=number_format(	(($this->facebook_count / $this->total_count) * 100) );?>%<br/>
		Fanpages: <?=number_format(	(($this->fanpages_count / $this->total_count) * 100) );?>%<br/>
		Twitter: <?=number_format(	(($this->twitter_count / $this->total_count) * 100) );?>%<br/>
		LinkedIn: <?=number_format(	(($this->linkedin_count / $this->total_count) * 100) );?>%<br/>
		
	<br><br>	
		
		HOW THEY SAW IT:<br><br>

		320x480  - Cellphone icon: 38.98%<br>
		320x568  - Smartphone icon: 21.99%<br>
		768x1024 - Monitor icon: 24.17%<br>
		640x480  - Laptop icon: 14.86<br>
		
	</p>
	
	
		
	<p>
		<? /************************************
		<br/><br/><br/>
		<h1>Raw Stats</h1>
		
		Total Posts: <?=number_format($this->stats->total_posts);?><br/>

		Total Clicks: <?=number_format($this->stats->total_clicks);?><br/>

		Avg Clicks per Post: <?=number_format($this->stats->total_clicks / $this->stats->total_posts, 2);?><br/>
	
		Total Bookings: <?=number_format($this->stats->total_bookings);?><br/>	

		Total On Site Interactions: <?=number_format($this->stats->total_interactions);?>*(this needs to be a pop-up)<br/>	

		Total FB Likes: <?=number_format($this->stats->total_likes);?><br/>	

		Total FB Comments: <?=number_format($this->stats->total_comments);?><br/>
		
		
		<br>
		<br>
		
		Total Social Media Reach: <?=number_format($this->total_count);?><br>
		
		
		
		FB Reach: <?=number_format($this->facebook_count);?><br>
		
		Fanpage Reach: <?=number_format($this->fanpages_count);?><br>
		
		Twitter Reach: <?=number_format($this->twitter_count);?><br>
		
		Linked In: <?=number_format($this->linkedin_count);?><br>


		
		
<br>
<br>

		Total Potential Buyers This Year: 1,289,666<br>
		Average Visitors Per Day: 3,533<br>

<br>
<br>




<br>
<br>

		**************************************/ ?>
		
	
	</p>
	
	

</div>

