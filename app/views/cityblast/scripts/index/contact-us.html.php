<div class="grid_12">
	
	<div class="box clearfix">

		<div class="box_section">
			<h1>Contact Us.</h1>
		</div>


		<h3>By Phone</h3>

		<p>We're always here to help. Have a question? Have a problem? Drop us a line!</p>
		<h4 style="padding-left: 18px;">1-888-712-7888 <br/><span style="font-size: 16px; color: #3D6DCC">9:00 am to 5:00 pm EST</span></h4>
		
		<h3>Email</h3>

		<p>Member Support Specialists are standing by to help you with any question or suggestion.  Email <?=COMPANY_NAME;?> at the appropriate address below, and you'll always receive a reply within 24-hours - and usually much faster.<p>
		<p><a href="mailto:<?=HELP_EMAIL;?>"><?=HELP_EMAIL;?></a></p>
		
		<h3>Direct Mail</h3>
		
		<p>If you're a traditionalist, please use the following address to reach <?=COMPANY_NAME;?> Support directly:</p>
		<blockquote>
			<p>		
				<strong><?=COMPANY_NAME;?></strong><br />
				<?=COMPANY_ADDRESS;?><br />
				<?=COMPANY_CITY;?>, <?=COMPANY_PROVINCE;?><br />
				<?=COMPANY_POSTAL;?><br />
				<?=COMPANY_COUNTRY;?>
			</p>
		</blockquote>
		
		</p>
	
	</div>
	
</div>