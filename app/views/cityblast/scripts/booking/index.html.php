<?
	$full_address = null;
	$desc_array = null;
	if(isset($this->listing->street) && !empty($this->listing->street)) $desc_array[] = $this->listing->street;
	if(isset($this->listing->city) && !empty($this->listing->city)) $desc_array[] = $this->listing->city;


	if(is_array($desc_array)) $full_address .= implode(", ", $desc_array);

?>
<link href="/css/book_box.css" type="text/css" rel="stylesheet" />
<!--[if IE 7]>
	<style type="text/css">
		.uiForm .uiFormRow{
			height: 55px;
		}
		.uiForm .uiFormRow.textarea{
			height: 80px;
		}
		.popupWrapper form > ul{
			min-height: 277px;
		}
	</style>
<![endif]-->

<style type="text/css">
ul.uiForm{
	width: 568px;
	margin: 0;
}

.uiFormRowHeader{
}

.uiForm .uiFormRow{
	height: auto;
}

.uiForm .fieldname{
	width: 174px;
	color: #888;
	font-weight: 600;
}

.uiForm .field{
	width: 313px;
	padding-right: 13px;
}

.popupWrapper{
	width: 568px;
}
.popupWrapper form > ul{
	border-radius: 8px;
}
.agentDetails{
	position: relative;
}
.toggleContactType{
	position: absolute;
	top: 0;
	right: 0;
}
.toggleContactType > a{
	display: none;
}
.callInfo .toggleContactType > a.emailMe,
.emailInfo .toggleContactType > a.callMe{
	display: inline-block;
	text-decoration: none;
	color: white;
	background-color: #878787;
	padding: 3px 25px;
	border-radius: 4px;
	text-align: center;
	width: 86px;
}

.popupWrapper.callInfo form > ul.callContact,
.popupWrapper.emailInfo form > ul.emailContact{
	display: block;
}
.popupWrapper form > ul.callContact,
.popupWrapper form > ul.emailContact{
}

.popupWrapper.callInfo .contactTypeButtons,
.popupWrapper.emailInfo .contactTypeButtons {
	display: none;
}


h1
{
	font-weight: normal;
    	line-height: 1.3em;
    	margin: 0px;
 	font-size: 48px;   
  	font-family: "myriad-pro-n4","myriad-pro","Helvetica Neue",Helvetica,Arial,sans-serif !important; 	
}


</style>

<div class="popupWrapper">
	
	<center><h1>Ask <?=$this->booking_agent->first_name;?></h1></center>
	<hr>


	<div id="contact_choice">
		<h5 style="margin-top: 40px;">Contact Me</h5>
		<p>Do you have a question about this property? I'm always here to help! I'm available to assist you
				either by phone or email so feel free to contact me right away.</p>
		
		<hr />
		

		<div class="contactTypeButtons" style="padding-left: 135px; height: auto; margin-top: 40px;">
			<a href="#" class="btn btn-primary" id="email_button" style="margin-right: 50px;"><i class="icon-envelope"></i> Email Me Now</a>
			<a href="#" class="btn btn-primary" id="call_button"><i class="icon-phone"></i> Call Me Now</a>
		</div>
		
		
	
	</div>


	<form action="/booking/save/<?=$this->listing->id;?>/<?=$this->booking_agent->id;?>/" method="POST" id="add_booking">
		<input type="hidden" name="type" value="<?=$this->request_type;?>" />
		




		<ul class="uiForm clearfix" id="emailContact" style="display: none;">
			<li class="uiFormRow clearfix">
				<ul style="margin: 0px;">
					<li class="fieldname">Your Name</li>
					<li class="field">
						<input type="text" name="name" id="name" value="" class="uiInput validateNotempty <?=($this->fields && in_array("name", $this->fields)?" error":"")?>" style="width: 493px !important;" />
					</li>
				</ul>
			</li>

			<li class="uiFormRow emailContact clearfix">
				<ul style="margin: 0px;">
					<li class="fieldname">Email Address</li>
					<li class="field">
						<input type="text" name="email" id="email" value="" class="uiInput validateNotempty <?=($this->fields && in_array("email", $this->fields)?" error":"")?>" style="width: 493px !important;" />
					</li>
				</ul>
			</li>

			<li class="uiFormRow clearfix textarea">
				<ul style="margin: 0px;">
					<li class="fieldname">Your Message</li>
					<li class="field">
						<textarea name="message" class="uiTextarea" style="height: 50px !important; width: 493px !important;"></textarea>
					</li>
				</ul>
			</li>
			<li class="uiFormRow clearfix" style="margin-top: 24px; text-align: right;">

				<input type="submit" value="Send" class="btn btn-primary right" id="send_id" />
				
			</li>
		</ul>
		
		
		
		<ul class="uiForm clearfix" id="callContact" style="display: none;">

			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname">Direct Line</li>
					<li class="field">
						<label style="font-size: 22px; font-weight: bold;"><?=$this->booking_agent->phone;?></label>
					</li>
				</ul>
			</li>

			<li class="uiFormRow clearfix" style="margin-top: 24px; text-align: right;">
				<hr>
				
				<input type="button" value="Close" class="btn btn-primary right" onclick="$.colorbox.close();" />
				
			</li>			

		</ul>
	</form>
</div>

<script type="text/javascript">
	var $PopupWrapper = $('.popupWrapper');

	$("#email_button").click(function(e)
	{
		e.preventDefault();
		
		$("#contact_choice").hide();
		$("#callContact").hide();
		$("#emailContact").show();
		
				
	});
	$("#call_button").click(function(e)
	{
		e.preventDefault();
		
		$("#contact_choice").hide();
		$("#emailContact").hide();
		$("#callContact").show();


		// Log phone button click
		$.ajax({
			type: 'POST',
			url: '/booking/xhrphonecalllog/',
			data: {
				listing_id : '<?php echo $this->listing->id; ?>',
				booking_agent_id : '<?php echo $this->booking_agent->id; ?>'
			}
		});
		

				
	});






	$('input[type=submit]').removeAttr('disabled');

	$('form#add_booking').submit(function(){
		if( $("#name").val() == '')
		{
		    $("#name").addClass("error");
		    return false;
		}

		if( $("#email").val() == '')
		{
		    $("#email").addClass("error");
		    return false;
		}

		// Disable submit button
		$('input[type=submit]', this).attr('disabled', 'disabled');

		$.ajax({
		   type: "POST",
		   url: "/booking/savebox/<?=$this->listing->id;?>/<?=$this->booking_agent->id;?>/",
		   data: $('form#add_booking').serialize(),
		   success: function(msg){
		    $('#cboxLoadedContent').empty();
		    $('#cboxLoadedContent').append(msg);
		    $('#cboxLoadedContent').css('height','360px');
		    $.colorbox.resize()

		   }
		 });
	return false;

	});

</script>
