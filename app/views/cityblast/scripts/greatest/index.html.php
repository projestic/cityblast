<div id="step1" class="contentBox" style="padding: 2px 40px 10px 60px; margin-bottom: 10px !important;">

	<img class="book" src="/images/new-images/100_greatest.jpg" alt="Cityblast 100 Greatest Posts of All Time">

	<div class="greatest">
	
		<h3>Download your complimentary copy</h3>
		<h1>“The 100 Greatest Real Estate Posts of All Time”</h1>
		<img alt="Cityblast 100 Greatest Posts of All Time" src="/images/new-images/100_greatest.jpg" class="book mobile">

		<p>Enter your email address below, and we’ll instantly send you this powerful, time-saving book, showcasing the highest-engaging posts our Experts have ever come up with!</p>
		<p>The entire guide is produced in a handy COPY &amp; PASTE format so you can instantly use our best posts of all time to beef up your own social media presence.</p>
		<p>Distilled from over 1,300,000 Facebook, Twitter and LinkedIn posts, this awesome tool is sure to super-charge your social media!</p>

	</div>

	<div class="greatest-form">

		<form action="" id="getfreebook" method="post">
			<p>You’ll be instantly emailed the “100 Greatest Real Estate Posts of All Time” e-book!</p>
			<? if ($this->show_validation) { ?>
			<div class="validation">Please enter a valid email address.</div>
			<? } ?>
			<label>Get the book now free!</label>
			<input type="text" class="<? if ($this->show_validation) { ?>error <? } ?> uiInput <?= empty($this->emailError) ? '' : 'error'; ?>" id="email" name="email" placeholder="Your best email...">&nbsp;
			<button type="submit" id="step1_btn" class="btn btn-primary btn-large next-section" href="javascript:void(0);"><i class="icon-chevron-sign-right icon-fixed-width"></i> Reveal the Secrets NOW!</button>
		</form>

	</div>
	
</div>
