<div class="grid_12">
	<div class="box thankyou">
		<div class="box_section">
			<h1>Success!</h1>
		</div>
		<ul class="uiForm">

			<li class="uiFormRow">

				<h3>Your Tools are Now Ready for Use.</h3>
				<p>Congratulations on adding the world's most powerful social media toolkit for real estate agents to your marketing arsenal.  You may now do any of the following.</p>
				
			</li>
			<li class="clearfix uiFormRow">

				<div class="shareBlast">
					<p>Return to LoneWolf</p>
					<div class="clearfix">
						<a href="http://www.lwolf.com">Return to LoneWolf.com</a>
					</div>
				</div>


				<div class="previewBlast">
					<p>Instruct your Social Media Expert</p>
					<div class="clearfix">
						<a href="/member/settings" class="login1">Change Your Settings</a>
					</div>
				</div>

			</li>
		</ul>
	</div>
</div>

<?
	//echo $member->single_signon . "<BR>";
	
?>		