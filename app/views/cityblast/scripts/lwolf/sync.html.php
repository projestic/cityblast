<?php
if ($this->member && $this->member->network_app_facebook) {
	$network_app_facebook = $this->member->network_app_facebook;
} else {
	$network_app_facebook = Zend_Registry::get('networkAppFacebook');
}
?>
<div class="grid_12">
	<div class="box thankyou">
		<div class="box_section">
			<h1>Immediate Access to Your Lone Wolf Social Media Tools.</h1>
		</div>
		<ul class="uiForm">

			<li class="uiFormRow">

				<h3>Please login or register below to access the world's best social media tools for real estate agents.</h3>
				<p>Welcome to CityBlast; Lone Wolf's powerful social media toolkit for real estate agents.  We've connected our services, so you get the awesome power of CityBlast's Facebook, Twitter and LinkedIn tools right at your fingertips.  In just a few clicks, you'll synchronize your CityBlast account, and have immediate access to your Dashboard and toolkit!</p>
				
				<p>If you're a returning member please <a href="#" class="login1">Login to Access Your Tools</a>.</p> 
					
				If you're new to <?=COMPANY_NAME;?> please <a href="/member/join">Start Your FREE Trial</a> of the tools and see what the buzz is about.</p>

			</li>
			<li class="clearfix uiFormRow">

				<div class="shareBlast">
					<p>New members should register</p>
					<div class="clearfix">
						<a href="/member/join">Register now</a>
					</div>
				</div>


				<div class="previewBlast">
					<p>Existing members should login</p>
					<div class="clearfix">
						<a href="#" class="login1">Log in now</a>
					</div>
				</div>

			</li>
		</ul>
	</div>
</div>




<script>
	$(document).ready(function()
	{
		var USE_FB_JS_SDK	=   true;

		if(USE_FB_JS_SDK) {


			// avoid multiple initialization
			if (typeof is_facebook_init == 'undefined' || !is_facebook_init)
			{
				FB.init({
					appId  : '<?=$network_app_facebook->consumer_id;?>',
					status : true, // check login status
					cookie : true, // enable cookies to allow the server to access the session
					xfbml  : true, // parse XFBML
					oauth  : true, // enable OAuth 2.0
					channelUrl: 'http://<?php echo $_SERVER['HTTP_HOST'] ?>/channel.php' // channeling JS SDK
				});

				is_facebook_init	= true;
			}


		    $('.login1').click(function(event){


			    event.preventDefault();
			    FB.login(function(response) {

				    if (response.authResponse)
				    {

					    $.ajax({

						    type: "POST",
						    url: "/member/fblogin",
						    data: "access_token="+response.authResponse.accessToken,
						    dataType: "json",

						    success: function(msg)
						    {
							   window.location =	msg.redirectBackTo;
						    }
					    });

				    }

			    }, {scope: '<?php echo FB_SCOPE;?>'});

		    });

		    $('.register1').click(function(event){

			    	event.preventDefault();			    											
				
				
				//alert('What is my City?'+$("#city").val());
				var city_id	= $("#city").val();
				if (city_id == null || city_id == '' || city_id == '*') 
				{
					window.location	= "/member/blastcity";
					return false;
				}
				else
				{
				
				    	FB.login(function(response) {
	
					    if (response.authResponse)
					    {
						    // console.log(response.authResponse);
						    $.ajax({
	
							    type: "POST",
							    url: "/member/fbsignup",
							    data: "access_token="+response.authResponse.accessToken+"&href="+$('#facebook-signup').attr('href'),
							    dataType: "json",
	
							    success: function(msg)
							    {						    								    	
									window.location =	msg.redirectBackTo;
							    }
						    });
	
					    }
	
				    }, {scope: '<?php echo FB_SCOPE;?>'});
				}


		    });

		    $('#facebook-logout').click(function(event){


				event.preventDefault();
				FB.logout(function(response)
				{
				    $.ajax({

						    type: "POST",
						    url: "/member/fblogout",
						    data: "",
						    dataType: "json",

						    success: function(msg)
						    {
							    window.location = "/";
						    }
					    });

				});

		    });

		}

	});
</script>