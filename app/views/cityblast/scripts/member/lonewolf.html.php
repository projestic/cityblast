<style type="text/css">
html, body{
	background: #fff !important;
}
@media (min-width: 1200px){
	.container,
	.span12{
		width: 1024px;
	}
}

</style>
<div class="brokerPage">
	<div class="row-fluid">
		<img class="logo" src="/images/city-blast_logo.png" />
	</div>
	
	<div class="offerWrapper row-fluid">
		<div class="offerBlackWrapper">
			<div class="offerWhiteWrapper">
				<div class="offerGrayWrapper">
					<div class="offer clearfix">
						<div class="span9">
							<h1>Get the Marketing Savvy of an In-House Social Media Assistant - <span class="orange">Without the Enormous Cost</span></h1>
							<p>CityBlast's Social Experts guarantee your Real Estate business is never embarrassed<br/>by a neglected Twitter, Facebook or LinkedIn account again.</p>
						</div>
						<div class="span3">
							<img src="/images/offer-stamp-39_99.png" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="tryFree row-fluid">
		<p class="pull-left">Begin Your <span>Free 30-Day Trial,</span> and get our Social Experts working for you.</p>
		<a href="/member/join" class="pull-right btn btn-success btn-large">Try Us Free!</a>
	</div>
	
	<div class="row-fluid whySocialMedia">
		<div class="span6">
			<ul>
				<li class="searches">
					<h3>Over 90% of home searches begin online</h3>
					<p>That means over 90% of your prospects are starting their real estate journey without your company. You must stay active and not miss out on these clients!</p>
				</li>
				<li class="timeSpend">
					<h3>people spend 3x more time on facebook vs. any other site</h3>
					<p>If you're going to keep in front of your network consistently and look professional, then social media is the absolute best place to do so!</p>
				</li>
				<li class="expertsRecommend">
					<h3>experts recommend spending 1 hour every day on social media marketing</h3>
					<p>Who has an hour every day?! Let our experts do the heavy lifting for you, and keep you looking fresh and up-to-date on your social accounts.</p>
				</li>
			</ul>
		</div>
		<div class="span6 workForYou">
			<h2>Let our experts do the work for you.</h2>
			<p>We're like your in-house Virtual Social Media Assistant.</p>
			<ul>
				<li>We manage your social accounts, marketing your company</li>
				<li>Relevant real estate articles, videos and information</li>
				<li>Only trusted sources like HGTV, CNN and WSJ</li>
				<li>As often as you choose - up to 7 posts/week</li>
				<li>We update Facebook, Twitter and LinkedIn</li>
				<li>One low cost vs. hiring someone locally</li>
			</ul>
			<div class="dontMiss">
				<span>Don't Miss Out..</span>
				<a href="/member/join" class="btn btn-success btn-large"><span>Start Your</span><br/>Free 30-Day Trial</a>
			</div>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span4">
			<div class="testimonial">
				<h4>Great Service</h4>
				<p>"I use CityBlast to help me market. Not only are they the heart of my online platform, but also help me to sell my clients' listings quickly, and to achieve excellent prices. CityBlast is an incredible service for both beginners and top agents, and I highly recommend them."</p>
				<div class="clearfix">
					<img class="userImg pull-left" src="/images/user-thumb-img2.jpg" />
					<div class="userInfo">
						<span class="name">Sheree Cerqua</span>
						<span>Ontario's #1 Individual Agent</span>
						<span>Royal LePage</span>
					</div>
				</div>
				<span class="arrow"></span>
			</div>
		</div>

		<div class="span4">
			<div class="testimonial">
				<h4>Quick and Painless</h4>
				<p>In the Real Estate profession it is difficult to know which of the latest and greatest tools will provide the most bang for your buck. CityBlast makes sending your listing to your social networks quick and painless; it's something everyone should explore.</p>
				<div class="clearfix">
					<img class="userImg pull-left" src="/images/user-thumb-img3.jpg" />
					<div class="userInfo">
						<span class="name">Gordon Wallace</span>
						<span>Marketing and Technology Expert</span>
						<span>Lone Wolf Real Estate Technology</span>
					</div>
				</div>
				<span class="arrow"></span>
			</div>
		</div>

		<div class="span4">
			<div class="testimonial">
				<h4>Awesome Experts</h4>
				<p>"My friends and previous colleagues noticed my Facebook updates right away. In only my first week using CityBlast, I received a call from a family friend who said she'd seen my Facebook and wanted to list their home with me. The experts are awesome!"</p>
				<div class="clearfix">
					<img class="userImg pull-left" src="/images/user-thumb-img.jpg" />
					<div class="userInfo">
						<span class="name">Leila Talibova</span>
						<span>Top Rookie Agent</span>
						<span>Home Life Realty</span>
					</div>
				</div>
				<span class="arrow"></span>
			</div>
		</div>

	</div>
	
	<div class="tryFree black row-fluid">
		<p class="pull-left">Begin Your <span>Free 30-Day Trial,</span> and get our Social Experts working for you.</p>
		<a href="/member/join" class="pull-right btn btn-success btn-large">Try Us Free!</a>
	</div>
</div>



<? if (APPLICATION_ENV == 'cityblast'): ?>

<script type="text/javascript">
	//<![CDATA[
	var trendVar = "www.cityblast.com";
	var dcsidVar = "dcs6p84o900000w0yf68kq216_9x3g";
	var _tag=new WebTrends();
	_tag.dcsGetId();
	//]]>
</script>
<script type="text/javascript">
	//<![CDATA[
	_tag.dcsCustom=function(){
	// Add custom parameters here.
	//_tag.DCSext.param_name=param_value;
	}
	_tag.dcsCollect();
	//]]>
</script>
<noscript>
	<div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://statse.webtrendslive.com/dcs6p84o900000w0yf68kq216_9x3g/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=9.3.0&amp;WT.dcssip=www.city-blast.com"/></div>
</noscript>
<!-- END OF SmartSource Data Collector TAG -->

<? endif; ?>
	




<?/********************************************************************************

OLD STYLE --- WE'LL NEED THIS FOR THE HOME PAGE


<div class="grid_12" style="margin-top: 0px; position: relative; z-index: 1;">
	
	<div class="box form clearfix" style="padding: 0px;">

		<script type="text/javascript" src="/js/swfobject.js"></script>    
		
		<div id="ytapiplayer_wrapper" class="clearfix" style="height: 350px; position: relative;">
			<div id="ytapiplayer">
				You need Flash player 8+ and JavaScript enabled to view this video.
			</div>
			<div class="video_topleft"><img src="/images/lp-video-topleft.png" width="12" height="13" /></div>
			<div class="video_topright"><img src="/images/lp-video-topright.png" width="14" height="13" /></div>
			<div id="video_header" style="background-image: url(/images/lp-video-LoneWolf-header.jpg);"></div>
		</div>
	
		<script type="text/javascript">
	
			var params = { allowScriptAccess: "always", allowFullScreen: true };
			var atts = { id: "myytplayer" };
			swfobject.embedSWF("https://www.youtube.com/v/yVXg1tXJmX4?enablejsapi=1&playerapiid=ytplayer&version=3", "ytapiplayer", "1000", "562", "8", null, null, params, atts);
		</script>
		
	
		<div class="clearfix" id="select_your_city" style="padding: 0 20px;">
		
			<ul class="uiForm full clearfix">
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname" style="font-size: 24px; line-height: 60px; padding: 20px;">Choose Your City &amp; Press Play<span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="line-height: 60px; padding: 20px; position: relative;">
							<span class="uiSelectWrapper" id="citywrapper" style="margin-top: 10px; position: relative; z-index: 2; padding: 10px 1px;">
								<span class="uiSelectInner" id="citywrapperinner">
									<select name="pubcity" id="defaultpubcity" class="uiSelect" style="width: 400px;">
										<option value="">Please Select A City</option>
										<? 
										foreach($this->cities as $city)
										{
											//print_r($city);
											
											echo "<option value='".$city->id."' ";
											if($this->selectedCity == $city->id) echo " selected ";
											echo ">".$city->name;
											if(isset($city->state) && !empty($city->state)) echo ", " .$city->state;
											echo "</option>"; 	
										}
										?>
									</select>
								
									<?/*	
									<input type="text" name="pubcityselect" id="pubcityselect" class="uiSelect" style="width: 590px;" value="<?=$this->selectedCityName;?>" />
									<input type="hidden" name="pubcity" id="pubcity" value="<? if($this->selectedCity != "*") echo $this->selectedCity;?>" />
									* ?>
									
								</span>
							</span>
							<div id='city_error' class="clearfix" style='display: none; position: absolute; bottom: 4px; left: 20px; z-index: 1; width: 380px;'>
								<strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> select a city to play the video
							</div>
							<a href="javascript:void(0);" onclick="play()" id="yt-play-btn"></a>
						</li>
					</ul>
				</li>
			</ul>

		</div>
	
	
		<div class="clearfix" id="connect_to_cityblast" style="display: none; padding: 0 20px;">
	
			<div class="box_section" style="height: 120px; padding: 10px 20px;">
				<h1  style="line-height: 80px; font-size: 34px; float: left; margin: 0px; color: #333;">Connect to <?=COMPANY_NAME;?> Now and <span style="color: #000000;">Get 14 Days Free!</span></h1>
				<a href="#" id="facebook_signup_button" style="float: left; margin: 10px 0 10px 20px;"><img width="280" height="60" class="connect_fb_btn" src="/images/blank.gif"></a>
				
				<p><input type="checkbox" name="terms" id="terms" checked="checked"> I agree to the <a href="/index/terms" >Terms and Conditions</a>.</p>
			</div>
			
		</div>
	</div>

</div>



<? include_once "logos.html.php"; ?>
		



			

<div class="grid_12" style="border-top: 1px solid #c3c3c3; padding-top: 30px; padding-bottom: 10px;">
     <h1 style="text-align: center; margin: 0; line-height: 60px; color: #666666">Start Today! Call us at <span style="color: #333333;">1-888-712-7888</span>
     &nbsp;<span style="font-size: 14px; color: #3D6DCC">(*9:00 am to 5:00 pm EST)</span>
     </h1>
</div>

<? echo $this->FacePile('standard'); ?>

<script type="text/javascript">
	
	var ytplayer = null;
	
	function onYouTubePlayerReady(playerId) {
		ytplayer = document.getElementById("myytplayer");
		
		ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
		ytplayer.setPlaybackQuality('hd720');
		$(ytplayer).css('height', '0px');
	}
	
	function onytplayerStateChange(newState) {
		
		if(newState == 1){
			$("#ytapiplayer_wrapper").animate({height:562},500);
			$('#video_header').hide();
		}
		if(newState == 0){
			$("#ytapiplayer_wrapper").animate({height:350},500);
			$('#video_header').show();
			$(ytplayer).css('height', '0px');
		}
		
	}
	
	function play(){
		if($("#defaultpubcity").val() != ""){
			$('#city_error').hide();
			$('#connect_to_cityblast').show();
			$('#select_your_city').hide();
			$(ytplayer).css('height', '562px');
			ytplayer.playVideo();
		}
		else
		{
			$('#city_error').show();
		}
	}

	$(document).ready(function() {

        $('#facebook_signup_button').click(function(event){

            event.preventDefault();
            $('#city_error').hide();


	
            if( $("#defaultpubcity").val() == "" )
            {
                 $('#citywrapper').addClass('city_error');
                 $("#defaultpubcity").addClass('error');
                 $('#city_error').show();
            }
            else
            {
			if ($('#terms').is(':checked')) 
			{

				if(!$(this).hasClass('alreadyclicked')) {
					
					$(this).addClass('alreadyclicked');
		            	
		                FB.login(function(response) {
		
		                    if (response.authResponse)
		                    {
		          
		          			window.location.href="/member/clientfind-signup/" + $("#defaultpubcity").val() + "/?access_token="+response.authResponse.accessToken; 
		                    }
		
		                }, {scope: '<?php echo FB_SCOPE;?>'});
		          }
		     }
            }
           
        });
		
	});
</script>

<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/0184.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);

</script>

**************************************************/ 

?>