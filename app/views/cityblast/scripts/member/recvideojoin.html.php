<div class="grid_12" style="margin-top: 0px; position: relative; z-index: 1;">
	
	<div class="box form clearfix" style="padding: 0px;">

		<script type="text/javascript" src="/js/swfobject.js"></script>    
		
		<div id="ytapiplayer_wrapper" class="clearfix" style="height: 350px; position: relative;">
			<div id="ytapiplayer">
				You need Flash player 8+ and JavaScript enabled to view this video.
			</div>
			<div class="video_topleft"><img src="/images/lp-video-topleft.png" width="12" height="13" /></div>
			<div class="video_topright"><img src="/images/lp-video-topright.png" width="14" height="13" /></div>
			<div id="video_header" style="background-image: url(/images/lp-video-RETS-header.jpg);"></div>
		</div>
	
		<script type="text/javascript">
	
			var params = { allowScriptAccess: "always", allowFullScreen: true };
			var atts = { id: "myytplayer" };
			swfobject.embedSWF("https://www.youtube.com/v/yVXg1tXJmX4?enablejsapi=1&playerapiid=ytplayer&version=3", "ytapiplayer", "1000", "562", "8", null, null, params, atts);
		</script>
		
	
		<div class="clearfix" id="select_your_city" style="padding: 0 20px;">
		
			<ul class="uiForm full clearfix">
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname" style="font-size: 24px; line-height: 60px; padding: 20px;">Choose Your City &amp; Press Play<span style="font-weight: bold; color: #990000">*</span></li>
						<li class="field" style="line-height: 60px; padding: 20px; position: relative;">
							<span class="uiSelectWrapper" id="citywrapper" style="margin-top: 10px; position: relative; z-index: 2; padding: 10px 1px;">
								<span class="uiSelectInner" id="citywrapperinner">
									<select name="pubcity" id="defaultpubcity" class="uiSelect" style="width: 400px;">
										<option value="">Please Select A City</option>
										<? 
										foreach($this->cities as $city)
										{
											//print_r($city);
											
											echo "<option value='".$city->id."' ";
											if($this->selectedCity == $city->id) echo " selected ";
											echo ">".$city->name;
											if(isset($city->state) && !empty($city->state)) echo ", " .$city->state;
											echo "</option>"; 	
										}
										?>
									</select>
								
									<?/*	
									<input type="text" name="pubcityselect" id="pubcityselect" class="uiSelect" style="width: 590px;" value="<?=$this->selectedCityName;?>" />
									<input type="hidden" name="pubcity" id="pubcity" value="<? if($this->selectedCity != "*") echo $this->selectedCity;?>" />
									*/ ?>
									
								</span>
							</span>
							<div id='city_error' class="clearfix" style='display: none; position: absolute; bottom: 4px; left: 20px; z-index: 1; width: 380px;'>
								<strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> select a city to play the video
							</div>
							<a href="javascript:void(0);" onclick="play()" id="yt-play-btn"></a>
						</li>
					</ul>
				</li>
			</ul>

		</div>
	
	
		<div class="clearfix" id="connect_to_cityblast" style="display: none; padding: 0 20px;">
	
			<div class="box_section" style="height: 120px; padding: 10px 20px;">
				<h1  style="line-height: 80px; font-size: 34px; float: left; margin: 0px; color: #333;">Connect to <?=COMPANY_NAME;?> Now and <span style="color: #000000;">Get 14 Days Free!</span></h1>
				<a href="#" id="facebook_signup_button" style="float: left; margin: 10px 0 10px 20px;"><img width="280" height="60" class="connect_fb_btn" src="/images/blank.gif"></a>
				
				<p><input type="checkbox" name="terms" id="terms" checked="checked"> I agree to the <a href="/index/terms" >Terms and Conditions</a>.</p>
			</div>
			
		</div>
	</div>

</div>




<? include_once "logos.html.php"; ?>
		


			

<div class="grid_12" style="border-top: 1px solid #c3c3c3; padding-top: 30px; padding-bottom: 10px;">
     <h1 style="text-align: center; margin: 0; line-height: 60px; color: #666666">Start Today! Call us at <span style="color: #333333;">1-888-712-7888</span>
     &nbsp;<span style="font-size: 14px; color: #3D6DCC">(*9:00 am to 5:00 pm EST)</span>
      <?/* or <a href="#">Live Chat Now!</a>*/?></h1>
</div>

<? echo $this->FacePile('standard'); ?>

<script type="text/javascript">
	
	var ytplayer = null;
	
	function onYouTubePlayerReady(playerId) {
		ytplayer = document.getElementById("myytplayer");
		
		ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
		ytplayer.setPlaybackQuality('hd720');
		$(ytplayer).css('height', '0px');
	}
	
	function onytplayerStateChange(newState) {
		
		if(newState == 1){
			$("#ytapiplayer_wrapper").animate({height:562},500);
			$('#video_header').hide();
		}
		if(newState == 0){
			$("#ytapiplayer_wrapper").animate({height:350},500);
			$('#video_header').show();
			$(ytplayer).css('height', '0px');
		}
		
	}
	
	function play(){
		if($("#defaultpubcity").val() != ""){
			$('#city_error').hide();
			$('#connect_to_cityblast').show();
			$('#select_your_city').hide();
			$(ytplayer).css('height', '562px');
			ytplayer.playVideo();
		}
		else
		{
			$('#city_error').show();
		}
	}

	$(document).ready(function() {

        $('#facebook_signup_button').click(function(event){

            event.preventDefault();
            $('#city_error').hide();


	
            if( $("#defaultpubcity").val() == "" )
            {
                 $('#citywrapper').addClass('city_error');
                 $("#defaultpubcity").addClass('error');
                 $('#city_error').show();
            }
            else
            {
			if ($('#terms').is(':checked')) 
			{

				if(!$(this).hasClass('alreadyclicked')) {
					
					$(this).addClass('alreadyclicked');
		            	
		                FB.login(function(response) {
		
		                    if (response.authResponse)
		                    {
		          
		          			window.location.href="/member/clientfind-signup/" + $("#defaultpubcity").val() + "/?access_token="+response.authResponse.accessToken; 
		                    }
		
		                }, {scope: '<?php echo FB_SCOPE;?>'});
		          }
		     }
            }
           
        });
		
	});
</script>

<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/0184.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);

</script>