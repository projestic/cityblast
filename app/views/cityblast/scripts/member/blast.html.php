	<div class="grid_12">
		
		<div class="box gray clearfix">
			
				<h1>Blast Reach</h1>
				
				<p style='margin-bottom: 0px; padding-bottom: 0px; color: #790000;'><?=$this->listing->created_at->format("d M Y H:i");?></p>
				<p style='margin-top: 1px; padding-top: 0px; font-size: 20px; font-weight: normal;'>
		
					<? if(isset($this->listing->image) && ($this->listing->image))
					{
						if($this->listing->isContent()) echo "<a href='".$this->listing->content_link."'>";
							echo "<img src='" . $this->listing->getSizedImage(280, 250)."' border='0' style='max-width: 200px; max-height: 200px; float: left; padding-right: 12px;' />";
						if($this->listing->isContent()) echo "</a>";
					}
		
		
					echo $this->listing->message;
		
					if($this->listing->isContent())
					{
						echo "&nbsp;<a href='".$this->listing->content_link."'>...read more</a>";
						echo "</p>";
					}
					else
					{
						echo "</p>". "\n" . "<p>";
		
		
		
						$this->listing->price = preg_replace("/[^a-zA-Z0-9\s]/", "", $this->listing->price);
		
						$list_price = null;
						if(isset($this->listing->price) && !empty($this->listing->price))
						{
							if(is_numeric($this->listing->price)) $list_price .= "List Price: $".number_format($this->listing->price). " ";
							else $list_price .= "List Price: ".$this->listing->price. " ";
						}
		
						echo "<p style='color: #333333; font-size: 14px;'>".$list_price."</p>";
						echo "<p style='color: #acacac;'>Broker of Record: ".$this->listing->member->brokerage. "</p>";
						echo "<p style='color: #c1c1c1; font-size: 8px; font-style: italic;'>*Not intended to solicit clients currently under contract with another Brokerage.</p>";
					}
				?>

			<p style='margin-bottom: 0px; padding-bottom: 0px; color: #790000;'><?=$this->listing->created_at->format("d M Y H:i");?></p>
			<p style='margin-top: 1px; padding-top: 0px; font-size: 20px; font-weight: normal;'>
	
				<? if(isset($this->listing->image) && ($this->listing->image))
				{
					if($this->listing->isContent()) echo "<a href='".$this->listing->content_link."'>";
						echo "<img src='" . this->listing->getSizedImage(280, 250)."' border='0' style='max-width: 200px; max-height: 200px; float: left; padding-right: 12px;' />";
					if($this->listing->isContent()) echo "</a>";
				}
	
	
				echo $this->listing->message;
	
				if($this->listing->isContent())
				{
					echo "&nbsp;<a href='".$this->listing->content_link."'>...read more</a>";
					echo "</p>";
				}
				else
				{
					echo "</p>". "\n" . "<p>";
	
	
	
					$this->listing->price = preg_replace("/[^a-zA-Z0-9\s]/", "", $this->listing->price);
	
					$list_price = null;
					if(isset($this->listing->price) && !empty($this->listing->price))
					{
						if(is_numeric($this->listing->price)) $list_price .= "List Price: $".number_format($this->listing->price). " ";
						else $list_price .= "List Price: ".$this->listing->price. " ";
					}
	
					echo "<p style='color: #333333; font-size: 14px;'>".$list_price."</p>";
					echo "<p style='color: #acacac;'>Broker of Record: ".$this->listing->member->brokerage. "</p>";
					echo "<p style='color: #c1c1c1; font-size: 8px; font-style: italic;'>*Not intended to solicit clients currently under contract with another Brokerage.</p>";
				}
			?>
		
			<h3>Reach (<?php echo $this->listCount;?>)</h3>
	
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			   <tr>
			    <th width="10%" scope="col">Member</th>
				  <th width="15%" scope="col">Name</th>
				  <th width="12%" scope="col">Channel</th>
				  <th width="10%" scope="col">Date</th>
		
			   </tr>
				<?php
		
				foreach($this->paginator as $blast): ?>
					<tr>
						  <td scope="col"><?php echo $blast->member->first_name.' '.$blast->member->last_name;?></td>
						  <td scope="col"><?php echo $blast->friend->name;?></td>
						  <td scope="col"><?php echo $blast->channel;?></td>
						  <td scope="col"><?php echo $blast->created_at->format("Y-m-d H:i");?></td>
					</tr>
				<?php endforeach;?>
		    </table>
			
			<div style="float: right;"><?= $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?></div>
			
		</div>
		
	</div>
