<?php
	/** @var string[] $warnings*/
	$warnings = $this->warnings;

	//print_rf($this->listing);	


	$full_address = null;
	$desc_array = null;
	if(!empty($this->listing->street))		$desc_array[] = $this->listing->street;
	if(!empty($this->listing->city))		$desc_array[] = $this->listing->city;
	if(!empty($this->listing->postal_code))	$desc_array[] = $this->listing->postal_code;

	if(is_array($desc_array)) $full_address .= implode(", ", $desc_array);

	if(!empty($this->booking_agent_id) && intval($this->booking_agent_id)>0)
		$_REQUEST['mid'] = $this->booking_agent_id;

	if(empty($_REQUEST['mid']))	
		$_REQUEST['mid'] = null;

	// LISTING PRICE
	if(!empty($this->listing->price))	$this->listing->price = preg_replace("/[^a-zA-Z0-9\s]/", "", $this->listing->price);
	$list_price = null;
	if(!empty($this->listing->price))
	{ 
		$list_price .= is_numeric($this->listing->price)?"$".number_format($this->listing->price). " ":$this->listing->price. " ";			
	}
	
	
?>

<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
	#map_canvas { height: 100%; }
	.popupWrapper form > ul{
		height: 270px;
	}

	.popupWrapper form > ul.callContact,
	.popupWrapper form > ul.emailContact{ 
		display: none; 
	}
</style>
<!--[if IE 7]>
	<style type="text/css">
		/*.learn-more span{
			display: block;
			position: relative;
			right: 30px;
		}*/
		
		.recent_blasts{
			margin-top: 0px;
		}
		.ie7_hr{
			margin-top: 30px !important;
			margin-bottom: -10px;
		}
		.overlayed-img{
		
		}
</style>
<![endif]-->

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<?php if(count($warnings)): ?>
	<div class="notice" style="min-height: 60px;">
		<h6 style="color: #005826;">Warnings:</h6>
		<?= implode("\n<br/>", $warnings) ?>
	</div>
<?php endif; ?>

<?php if (!empty($this->listing)): ?>
<div class="row-fluid listing">
	<div class="span8">
		<?php 
		if($this->listing->property_status && $this->listing->property_status!=''){
			$property_status = $this->listing->property_status;
			if($property_status=='NEW')
			{
				$property_status_img = "/images/proerty_status/property_new.png";	
			}
			if($property_status=='EXPIRED')
			{
				$property_status_img = "/images/proerty_status/property_expired.png";
			}
			if($property_status=='SOLD')
			{
				$property_status_img = "/images/proerty_status/property_sold.png";
			}
			if($property_status=='PRICE_CHANGE')
			{
				$property_status_img = "/images/proerty_status/property_price_change.png";
			}
		?>
		
		<img src="<?php echo $property_status_img;?>" style="position: absolute; z-index: 1000;" />
		
		<?php 
		}
		?>
		
<?php
	$carouse_images = array();
	//$carouse_images[] = $this->listing->getOriginal()->image;
	$carouse_images[] = $this->listing->getOriginal()->getSizedImage(658, 0);
	if ($this->listing->getOriginal()->images) {
		foreach ($this->listing->getOriginal()->images as $img) {
			//$carouse_images[] = $img->image;
			$carouse_images[] = $img->getSizedImage(658, 0);
		}
	}
?>
		<script type="text/javascript" src="/js/jquery.bxslider.min.js"></script>
		<link href="/css/jquery.bxslider.css" rel="stylesheet" />
		<ul class="bxslider">
			<?php foreach($carouse_images as $carouse_image): ?>
				<li style="position:relative; background-color: #515151; width: 658px; display: block">
					<img style="display: block; left:0; right:0; top:0; bottom:0; margin:auto;" src="<?=$carouse_image?>" />
				</li>
			<?php endforeach; ?>
		</ul>
		<style type="text/css">
			/* DIRECTION CONTROLS (NEXT / PREV) */
			.bx-wrapper .bx-prev {
				left: 10px;
				background: url(/images/new-images/slider-controls.png) no-repeat;
			}

			.bx-wrapper .bx-next {
				right: 10px;
				background: url(/images/new-images/slider-controls.png) no-repeat -52px 0;
			}
			.bx-wrapper .bx-prev:hover {
				background-position: 0 0;
			}
			.bx-wrapper .bx-next:hover {
				background-position: -52px 0px;
			}
			.bx-wrapper .bx-controls-direction a {
				width: 50px;
				height: 50px;
			}
			/* DIRECTION CONTROLS (NEXT / PREV) */
		</style>
		<script type="text/javascript">
			var slider = $('.bxslider').bxSlider({
				adaptiveHeight: true,
				mode: 'fade',
				moveSlides: <?=count($carouse_images) > 1? 'true' : 'false'?>,
				pager: <?=count($carouse_images) > 1? 'true' : 'false'?>,
				controls: <?=count($carouse_images) > 1? 'true' : 'false'?>,
			});
		</script>
		<div class="propertyInfo">
			<div class="valueContainer">
				<h2><?=$list_price?></h2>
				<span class="address"><? if(!empty($this->listing->street)) echo $this->listing->street; ?></span>
				<div class="socialShare clearfix">
					<div class="facebookShare">
						<a id="button" target="_blank" href="/i/facebook/<?=$this->listing->id?>/<?=$this->booking_agent_id;?>" rel="nofollow"></a>  
						<div class="counter">
							<img src="/images/new-images/new_counter_arrow.gif" />
							<div id="fbcount"><?php	echo $this->fb_clicks[0]->total_clicks; ?></div>
						</div>
					</div>
					<div class="twitter">
						<a href="/i/twitter/<?=$this->listing->id;?>/<?=$this->booking_agent_id;?>" target="_blank" class="tweet" data-url="" data-via="" data-text="" data-related="" data-hashtags="" rel="nofollow"></a>
						<div class="count">
							<img src="/images/new-images/new_counter_arrow.gif" />
							<div class="twittercount"><?php echo $this->tw_clicks[0]->total_clicks; ?></div>
						</div>
					</div>
					<div class="mail">
						<a href="/i/email/<?=$this->listing->id;?>/<?=$this->booking_agent_id;?>" class="social-mail" rel="nofollow"></a>
						<div class="count">
							<img src="/images/new-images/new_counter_arrow.gif" />
							<div class="mailcount"><?php echo $this->em_clicks[0]->total_clicks; ?></div>
						</div>
					</div>
				</div>
			</div>
			<p>
			<?
				echo stripslashes($this->listing->message);
			
				if($this->listing->isContent())
				{ 
					echo "&nbsp;<a href='".$this->listing->content_link."'>...read more</a>";
				}				
			?>
			</p>
			
	<!-- Copy of Agent detail for mobile starts-->
			
			<? if(!empty($this->booking_agent)) :?>
			<div class="agentDetail">
				<div class="askQuestion">
					<h3>Questions about this property?</h3>
					<center><a href="javascript:void(0);" onclick="openBookBox('BOOK_SHOWING')" class="uiButtonNew" rel="nofollow" style="margin-top: 8px; outline: none !important; text-shadow: none;">Ask <?=$this->booking_agent->first_name;?>.</a></center>
				</div>
								
				<?/************
				<div class="media">
					<a class="pull-left" href="#" onclick="openBookBox('BOOK_SHOWING')">

						<?php 
						
							if(empty($this->booking_agent->photo))
							{
								$src = "https://graph.facebook.com/". $this->booking_agent->uid ."/picture" ;
							}
							else {
								$src = CDN_URL . $this->booking_agent->photo;
							}
						?>

						<img src="<?=$src;?>" class="media-object" height="66" width="66" />
					</a>
					<div class="media-body pull-left">
	
						

						<?/*<img width="50" height="50" src="<?php echo $src;?>" border="0" style="margin-right: 10px;" />/?>
						
						<div class="pull-right">
							
							<div style="font-size: 18px; font-weight: bold;"><?=$this->booking_agent->first_name;?> <?=$this->booking_agent->last_name;?></div>
							<div><?=$this->booking_agent->brokerage;?></div>
							<div class="clearfix"><?=$this->booking_agent->broker_address;?></div>
							
							
							<dl class="dl-horizontal" style="font-size: 13px!important;">
								<dt>Name:</dt>
								<dd><?=$this->booking_agent->first_name;?> <?=$this->booking_agent->last_name;?></dd>
								<dt>Brokerage:</dt>
								<dd><?=$this->booking_agent->brokerage;?></dd>
								<dt>Address:</dt>
								<dd><?=$this->booking_agent->broker_address;?></dd>
							</dl>
							
						
							<p style="margin-top: 18px;"><i>Questions about this property?</i></p>
							<a href="javascript:void(0);" onclick="openBookBox('BOOK_SHOWING')" class="uiButtonNew" rel="nofollow" style="outline: none !important; text-shadow: none;">Ask <?=$this->booking_agent->first_name;?>.</a>
						</div>
					</div>
					
					
				</div>
				*******************/ ?>
				<div class="contactMeWrapper text-center">
					<img src="/images/new-images/contact-me-top-arrow.jpg" class="contactMeTopBorder"/>
					<div class="closeBtn text-right">
						<a href="javascript:void(0);"></a>
					</div>
					<h4>Contact Me</h4>
					<p>Do you have a question about this property? I'm always here to help! I'm available to assist you either by phone or email so feel free to contact me right away.</p>
					<a href="#" class="emailMe"><img src="/images/new-images/email-icon.png"/> Email Me Now.</a>
					<a href="#" class="callMe"><img src="/images/new-images/call-icon.png"/> Call Me Now.</a>
				</div>
			</div>
		<? endif;?>
		
		<!-- Copy of Agent detail for mobile ends-->
		
		<!-- Copy of Propery details for mobile starts-->
		
		<div class="propertyDetail">
			<h3>Property Details</h3>
			<p class="address"><?=$full_address?$full_address:'N/A'?></p>
			<table border="0" cellspacing="0" cellpadding="0">
				<? if(isset($this->listing->property_type_id) && !empty($this->listing->property_type_id)) :?>
				<tr>
					<td class="title">Property Type:</td>
					<td>
						<?
							if($this->listing->property_type_id == "1") echo "Detached";
							elseif($this->listing->property_type_id == "2") echo "Semi-detached";
							elseif($this->listing->property_type_id == "3") echo "Freehold Townhouse";										
							elseif($this->listing->property_type_id == "4") echo "Condo Townhouse";
							elseif($this->listing->property_type_id == "5") echo "Condo Apartment";											
							elseif($this->listing->property_type_id == "6") echo "Coop/Common Elements";
							elseif($this->listing->property_type_id == "7") echo "Attached/Townhouse";
						?>
					</td>
				</tr>
				<? endif; ?>
				<tr>
					<td class="title">MLS Number:</td>
					<td><?=$this->listing->mls?$this->listing->mls:'N/A'?></td>
				</tr>
				<tr>
					<td class="title"># Bedrooms:</td>
					<td>
						<?
						$bedrooms = 'N/A';
						if ($this->listing->number_of_bedrooms) 
						{
							if ($this->listing->number_of_extra_bedrooms) 
							{
								$bedrooms = $this->listing->number_of_bedrooms . ' + ' . $this->listing->number_of_extra_bedrooms;
							} else {
								$bedrooms = $this->listing->number_of_bedrooms;
							}
						}
						echo $bedrooms;
						?>
					</td>
				</tr>
				<tr>
					<td class="title"># Bathrooms:</td>
					<td><?=$this->listing->number_of_bathrooms?$this->listing->number_of_bathrooms:'N/A'?></td>
				</tr>
				<tr>
					<td class="title"># Parking:</td>
					<td><?=$this->listing->number_of_parking?$this->listing->number_of_parking:'N/A'?></td>
				</tr>
				<? if(isset($this->listing->maintenance_fee) && !empty($this->listing->maintenance_fee)) :?>
					<tr>
						<td class="title">Maintenance:</td>
						<td>$<?=$this->listing->maintenance_fee;?> <span style='color: #a8a8a8'>/month</span></td>
					</tr>
				<? endif; ?>

				<? if(isset($this->listing->square_footage) && !empty($this->listing->square_footage)) :?>
					<tr>
						<td class="title"><strong>Approx Sq. Footage:</strong></td>
						<td><?=$this->listing->square_footage;?></td>
					</tr>
				<? endif; ?>
			</table>
			<p class="note">Not intended to solicit clients currently under contract with another Brokerage.</p>
			<div class="brokerInfo">
				<? if(isset($this->listing->member->brokerage) && !empty($this->listing->member->brokerage)) : ?>
					<p style="margin-bottom: 8px;"><i>Broker of Record:</i><br/><?=$this->listing->member->brokerage;?></p>
				<? elseif(isset($this->listing->broker_of_record) && !empty($this->listing->broker_of_record)) : ?>
					<p style="margin-bottom: 8px;"><i>Broker of Record:</i><br/><?=$this->listing->broker_of_record;?></p>
				<? endif; ?>

				
				<? if(isset($this->listing->member->broker_address) && !empty($this->listing->member->broker_address)) : ?>
					<p><i>Broker Address:</i><br/><?=$this->listing->member->broker_address;?></p>
				<? elseif(isset($this->listing->broker_address) && !empty($this->listing->broker_address)) : ?>
					<p><i>Broker Address:</i><br/><?=$this->listing->broker_address;?></p>
				<? endif; ?>
			</div>
		</div>
		
		<!-- Copy of Propery details for mobile ends-->
		
			<div class="map">
				<? if(!$this->listing->isContent() && !empty($this->listing->latitude) && !empty($this->listing->longitude)	) : ?>

					<script type="text/javascript" src="https://www.google.com/jsapi?key=<?php echo GOOGLE_MAPS_API; ?>"></script>
					
					<script type="text/javascript">
						markerPosition	= null
						function initialize(containerId)
						{
							var latlng = new google.maps.LatLng(<?=$this->listing->latitude;?>, <?=$this->listing->longitude;?>);
							var myOptions = {
								zoom: 12,
								center: latlng,
								mapTypeId: google.maps.MapTypeId.ROADMAP
							};
							var map = new google.maps.Map(document.getElementById(containerId), myOptions);
					
							var marker = new google.maps.Marker({
								map: map,
								position: map.getCenter()
							});
							//markerPosition	= map.getCenter();
								
						}

						$(document).ready(function()
						{	
							initialize('map_canvas');

							$('#map_canvas').click(function () {//alert(markerPosition);


								$.colorbox({
									href: false,
									innerWidth:700,
									innerHeight:520,
									initialWidth:780,
									initialHeight:520,
									html:"<div id='map_big_canvas' style='display: block; float: none; width: 100%; height: 100%;'></div>",

									onComplete: function(e) {
										
										initialize("map_big_canvas");
									}

								});


								//url	= 'http://maps.google.com/maps/api/staticmap?center=<?=$this->listing->latitude;?>,<?=$this->listing->longitude;?>'+
								//		'&markers=color:red%7C<?=$this->listing->latitude;?>,<?=$this->listing->longitude;?>&zoom=14&size=640x640&sensor=false';

								//var newWindow = window.open(url, '_blank');
								//newWindow.focus();
							});
						});
						
						
						
						// TWITTER share button
						var API_URL = "http://cdn.api.twitter.com/1/urls/count.json";
						var TWEET_URL = "https://twitter.com/intent/tweet";

						$('.tweet').each(function(){
							var elem = $(this);
							var url = encodeURIComponent(elem.data('url') || document.location.href);
							var text = elem.data('text') || document.title;
							var via = elem.data('via') || "";
							var related = encodeURIComponent(elem.data('related')) || "";
							var hashtags = encodeURIComponent(elem.data("hashtags")) || "";

							var sHref = TWEET_URL + "?hashtags=" + hashtags + "&original_referer=" + encodeURIComponent(document.location.href) + "&related=" + related
									+ "&source=tweetbutton&text=" + text + "&url=" + url + "&via=" + via;
							
						});
					</script>

					<div id="map_canvas" style="width: 100%; height: 250px;"></div>
					
				<? endif; ?>
			</div>
			<fb:comments xid="<? echo "listing_view_" . $this->listing->id; ?>" href="<?php echo APP_URL . "/listing/view/" . $this->listing->id; ?>" style="padding-top: 8px; width: 100%!important;"></fb:comments>

			
		</div>
		
		

		
		
		<div class="notUsingCBAgent">
			<h5>Know of a real estate <br/> agent who's not yet using <?=COMPANY_NAME;?>?</h5>
			<p>Now that you've seen the amazing power of <?=COMPANY_NAME;?>, <a href="/index/refer-an-agent">share it with an agent</a>. Everyone benefits from more member agents; as it creates a greater platform for all, and more local homes to choose from.</p>
		</div>
		

		
	</div>
	
	
	
	
	<div class="span4" style="padding-right: 2.5641%;">
		
		
		<? if(!empty($this->booking_agent)) :?>
			<div class="agentDetail">
				<h3>Questions about this property?</h3>
				<center><a href="javascript:void(0);" onclick="openBookBox('BOOK_SHOWING')" class="uiButtonNew" rel="nofollow" style="margin-top: 8px; outline: none !important; text-shadow: none;">Ask <?=$this->booking_agent->first_name;?>.</a></center>
				
			</div>
		<? endif;?>
		

		<div class="suggestions">
			<? if(is_array($this->last_4) && !empty($this->last_4)): ?>
				<h3>Suggested for You</h3>
				<? foreach($this->last_4 as $recent_listing): ?>
					<a class="suggestion" href='/listing/view/<?=$recent_listing->id?>/<?=$this->booking_agent_id;?>'>

						<?
							$original_listing = $recent_listing;
							while ($original_listing->reblast_from) 
							{
								$original_listing = $original_listing->reblast_from;
							}
							$thumb_nail = $original_listing->getSizedImage(280, 250);
							
						?>		
						
						<img src='<?=$thumb_nail;?>' border='0' />
						<span class="address"><?=$recent_listing->street?></span>
					</a>
				<? endforeach; ?>
			<? endif; ?>
		</div>


		<div class="suggestions" style="margin-top: 15px;">
			<a href="/index/how-blasting-works"><img src="/images/showcase-your-listing.png" border="0"></a>
		</div>	
		
	
		<!-- copy for width below 320px starts -->
		<div class="notUsingCBAgent">
			<h5>Know of a real estate <br/> agent who's not yet using <?=COMPANY_NAME;?>?</h5>
			<p>Now that you've seen the amazing power of <?=COMPANY_NAME;?>, <a href="/index/refer-an-agent">share it with an agent</a>. Everyone benefits from more member agents; as it creates a greater platform for all, and more local homes to choose from.</p>
		</div>

		
		<!-- copy for width below 320px starts -->

		<div class="suggestions">
			<? // BLOG POSTS ?>
			
			<h4>Recent Blog Posts.</h4>
			<? $count = 0;	 ?>
			<? if( $this->blogs ) : ?>
				<? while ($row = mysql_fetch_assoc($this->blogs)) : ?>
									
					<div class="clearfix">
						<div style="margin-top: 8px; padding-top: 8px;">
							<a href="<?=$row['guid']?>" style='text-decoration: none; font-size: 12px !important; color: #000;'><?=$row['post_title']?></a>
						</div>
						
						<? /* NO COMMENTS / DATE FOR NOW
						<div style='padding-top: 5px; margin-top: 5px; border-top: dashed 1px #e5e5e5; color: #999999; font-size: 11px; vertical-align: top; display: inline-block;'>
							<i class="icon-calendar"></i> <?=date('M d Y', strtotime($row['post_date']))?>
							<i class="icon-comment" style="margin-left: 10px;"></i> 
							<? if( $row['total_comments'] == 0 ) : ?>
								No Comments
							<? else: ?>
								<a href="<?=APP_URL?>/blog/?p=<?=$row['ID']?>#comments"><?=$row['total_comments']?> Comment<? if( $row['total_comments'] > 1 ): ?>s<? endif; ?></a>
							<? endif; ?>
						</div>
						*/ ?>
						
					</div>
						
				<? $count++; endwhile; ?>
				
			<? endif; ?>
		</div>


		
	</div>
</div>


<script>


	function fixFbCommentHeight() 
	{
		if($('.fb_ltr').length){
			if($(window).width() > 767){
				$('.row-fluid.listing > div').css('height', '');
				$('.row-fluid.listing > div').sameHeights();
			}
			setTimeout(function(){ fixFbCommentHeight(); }, 1000);
		}
	}


	function openBookBox(request_type)
	{
		$.colorbox({width:"650px", height: "540px;",  href:"/booking/index/<?=$this->listing->id;?>/<?=$this->booking_agent_id;?>/"+request_type});
	}

	$(document).ready(function()
	{
		setTimeout(function(){ fixFbCommentHeight(); }, 100);

		$('.other_listing_image').hover(
			function(event)
			{
				$(this).css('opacity', '1');
			},
			function(event)
			{
				$(this).css('opacity', '0.8');
			}
		);
		
		setTimeout(function(){
			if($(window).width() > 767)
				$('.row-fluid.listing > div').sameHeights(); 
		}, 1000);

		// Adding event listening on comment
		FB.Event.subscribe('comment.create', function(response)
		{
		    	var commentQuery = FB.Data.query("SELECT object_id,id,text, fromid FROM comment WHERE post_fbid='"+response.commentID+"' AND object_id IN (SELECT comments_fbid FROM link_stat WHERE url='"+response.href+"')");
		    	var userQuery = FB.Data.query("SELECT name FROM user WHERE uid in (select fromid from {0})", commentQuery);

			if($(window).width() > 767){
				$('.row-fluid.listing > div').css('height', '');
				$('.row-fluid.listing > div').sameHeights();
			}


		    	FB.Data.waitOn([commentQuery, userQuery], function() 
		    	{
				var commentRow = commentQuery.value[0];
				var userRow = userQuery.value[0];

				var commentId    =	commentRow.id;
				var name	 =	userRow.name;
				var fuid	 =	commentRow.fromid;
				var text	 =	commentRow.text;
				var listing_id   =	<?php echo $this->listing->id;?>;
				var mid		 =	<?php echo (empty($_REQUEST['mid'])) ? 0 : intval($_REQUEST['mid']);?>;
				var url		 =	'/comments/create';
				var data	 =	'name='+encodeURIComponent(name)+'&fuid='+fuid+'&text='+encodeURIComponent(text)+'&listing_id='+listing_id+'&mid='+mid+'&comment_id='+commentId;


				$.ajax({
					type: 'POST',
					url: url,
					data: data,
					success: function(response){
						//do nothing
					},

					dataType: 'json'
				});

		    	});

		});


	});
</script>

<style type="text/css">
	#cboxLoadedContent {
		margin: 0px 0px 10px !important;
	}
</style>
<!--   END OF DISPLAY SPECIFIC LISTING   -->
<?php endif ?>

