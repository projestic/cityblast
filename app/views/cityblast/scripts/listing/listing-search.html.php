<div class="search_bar clearfix" style="margin-bottom: 30px !important;">
	
	<h4 class="search_title">
		Search
		<span class="filter_open"></span>
	</h4>
	<div id="advancedSearch" class="form clearfix" style="margin-bottom: 25px;";>
		<form action="/listing/search" method="POST" id="extraForm" name="extraForm">

			<input type="hidden" name="blast_type" id="blast_type" />

			<ul class="uiForm full clearfix">
				<li class="uiFormRow clearfix">
					<ul class="inline">
						<li class="fieldname li_180" style="border-top-left-radius: 10px;">City</li>
				
						<li class="field">
							<!--
							<span class="selectWrapper">
								<select name="search_city" id="search_city" class="uiSelect" style="width: 590px;" tabindex="1">
									<option value="">Select A City</option>
									<?php foreach($this->cities as $city): ?>
									<option value="<?php echo $city->id;?>"
										<?php

										if (!empty($this->city_id) && $this->city_id == $city->id) echo 'selected="true"';
										elseif (!empty($this->select_city) && $this->select_city == $city->id) echo 'selected="true"';

										?>><?php echo $city->name;?>
									</option>
									<?php endforeach; ?>
								</select>
							</span>
							-->
								
							<input type="text" name="search_city" id="search_city" class="uiSelect" style="width: 590px;" value="<?php echo isset($this->search_city) ? $this->search_city : '' ?>" />
							<input type="hidden" name="city_id" id="city_id" value="<?php echo isset($this->city_id) ? $this->city_id : '' ?>" />
						</li>
					</ul>
				</li>

				<li class="uiFormRow clearfix">
					<ul class="inline">
						<li class="fieldname li_180">Street Address</li>
						<li class="field li_220">
							<input type="text" name="street" id="street" tabindex="2" value="<?php echo isset($this->street) ? $this->street : '' ?>" class="uiInput validateNotempty span12" />
						</li>
						<li class="fieldname right li_160">Postal/Zip Code</li>
						<li class="field li_160">
							<input type="text" name="postal" id="postal" tabindex="3" value="<?php echo isset($this->postal) ? $this->postal : '' ?>" class="uiInput validateNotempty span9" />
						</li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul class="inline">
						<li class="fieldname li_180">Price Range</li>
						<li class="field li_220">
							<span style="float: left; line-height: 30px; width: 18px; font-weight: bold; font-size: 16px;">$</span>
							<input type="text" name="price_range_low" id="price" value="<?php if(isset($this->price_range_low)) echo $this->price_range_low ?>" class="uiInput validateCurrency span10 pull-left" />
						</li>
						<li class="fieldname right" style="width: 10px; background: none; border: none;">To</li>
						<li class="field li_220">
							<span style="float: left; line-height: 30px; width: 18px; font-weight: bold; font-size: 16px;">$</span>
							<input type="text" name="price_range_high" id="price" value="<?php if(isset($this->price_range_high)) echo $this->price_range_high ?>" class="uiInput validateCurrency span10 pull-left" />
						</li>
					</ul>
				</li>
				<li class="uiFormRow clearfix">
					<ul class="inline">
						<li class="fieldname li_180">Property Type</li>
						<li class="field li_220">
							<span class="selectWrapper">
								<select name="property_type_id" id="property_type_id" class="uiSelect validateNotZero" style="width: 180px" tabindex="5">
									<option value="0">Please select...</option>
									<option value="1" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 1) ? 'selected="true"' : '' ?>>Detached</option>
									<option value="2" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 2) ? 'selected="true"' : '' ?>>Semi-detached</option>
									<option value="3" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 3) ? 'selected="true"' : '' ?>>Freehold Townhouse</option>
									<option value="4" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 4) ? 'selected="true"' : '' ?>>Condo Townhouse</option>
									<option value="5" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 5) ? 'selected="true"' : '' ?>>Condo Apartment</option>
									<option value="6" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 6) ? 'selected="true"' : '' ?>>Coop/Common Elements</option>
									<option value="7" <?php echo (!empty($this->property_type_id) && $this->property_type_id == 7) ? 'selected="true"' : '' ?>>Attached/Townhouse</option>
								</select>
							</span>
						</li>
						<li class="fieldname right li_160">Number of Bedrooms</li>
						<li class="field li_220">
							<span class="selectWrapper">
								<select name="number_of_bedrooms" id="number_of_bedrooms" class="uiSelect validateNotempty" style="width: 180px" tabindex="6">
									<option value="">Please select...</option>
									<?php
										$bedroom_options	= array("None", 1, 2, 3, 4, "5+");
										$this->number_of_bedrooms	= ($this->number_of_bedrooms > count($bedroom_options)) ? count($bedroom_options) : $this->number_of_bedrooms;
									?>
									<?php foreach ($bedroom_options as $key => $value): ?>
									<option value="<?php echo $key ?>" <?php echo (!empty($this->number_of_bedrooms) && $this->number_of_bedrooms == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
									<?php endforeach ?>
								</select>
							</span>
						</li>
					</ul>
				</li>
				<li class="uiFormRow clearfix">
					<ul class="inline">
						<li class="fieldname li_180">Number of Bathrooms</li>
						<li class="field li_220">
							<span class="selectWrapper">
								<select name="number_of_bathrooms" id="number_of_bathrooms"  class="uiSelect validateNotempty" style="width: 180px" tabindex="7">
									<option value="">Please select...</option>
									<?php
										$bathroom_options	= array("None", 1, 2, 3, 4, "5+");
										$this->number_of_bathrooms	= ($this->number_of_bathrooms > count($bathroom_options)) ? count($bathroom_options) : $this->number_of_bathrooms;
									?>
									<?php foreach ($bathroom_options as $key => $value): ?>
									<option value="<?php echo $key ?>" <?php echo (!empty($this->number_of_bathrooms) && $this->number_of_bathrooms == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
									<?php endforeach ?>
								</select>
							</span>
						</li>
						<li class="fieldname right li_160">Number of Parking</li>
						<li class="field li_220">
							<span class="selectWrapper">
								<select name="number_of_parking" id="number_of_parking" class="uiSelect validateNotempty" style="width: 180px" tabindex="8">
									<option value="">Please select...</option>
									<?php
										$parking_options	= array("None", 1, 2, "3+");
										$this->number_of_parking	= ($this->number_of_parking > count($parking_options)) ? count($parking_options) : $this->number_of_parking;
									?>
									<?php foreach ($parking_options as $key => $value): ?>
									<option value="<?php echo $key ?>" <?php echo (!empty($this->number_of_parking) && $this->number_of_parking == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
									<?php endforeach ?>

								</select>
							</span>
						</li>
					</ul>
				</li>
				<li class="uiFormRow clearfix" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
					<ul class="inline">
						<li class="fieldname li_180" style="border-bottom-left-radius: 10px;">Blast Type</li>
						<li class="field li_220">
							<span class="selectWrapper">
								<select name="listing_type" id="listing_type" tabindex="9" class="uiSelect" style="width: 180px;">

									<option value="">ALL</option>

									<option value="BLAST"<?php echo ($this->listing_type == Listing::TYPE_LISTING) ? " selected " : "" ?>>BLAST</option>

									<option value="MANUAL"<?php echo ($this->listing_type == Listing::TYPE_CONTENT) ? " selected " : "" ?>>MANUAL</option>

									<option value="SMLS"<?php echo ($this->listing_type == Listing::TYPE_IDX) ? " selected " : "" ?>>SMLS</option>

								</select>
							</span>
						</li>
						<li class="fieldname right li_160">MLS Number</li>
						<li class="field li_220">
							<input type="text" name="mls_number" tabindex="10" value="<? if(isset($this->mls_number)) echo $this->mls_number; elseif(isset($this->mls_number)) echo $this->mls_number;?>" class="uiInput validateNotempty <?=($this->fields && in_array("franchise", $this->fields)?" error":"")?>" style="width: 115px" />
						</li>
					</ul>
				</li>
			</ul>
			<input type="submit" name="list_button" id="list_button" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 15px;" value="Search" />
		</form>

	</div>


</div>
