<!-- LOAD UPLOADIFY -->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('#file_upload').uploadify({
    'uploader'  : '/js/uploadify/uploadify.swf',
    'script'    : '/blast/uploadimage',
    'cancelImg' : '/js/uploadify/cancel.png',
    'folder'    : '/images/',
    'auto'      : true,
    'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
    'fileDesc'  : 'Image Files',
    'buttonImg' : '/images/browse-button.png',
    'width'		: 85,
    'height'	: 30,
    'removeCompleted' : false,
    'onComplete': function(event, ID, fileObj, response, data) 
    {
    		alert(response);
		$('#fileUploaded').val(response);		
    }


    'onError': function(event, ID, fileObj, errorObj) 
    {
    		alert(errorObj.type + ' Error: ' + errorObj.info);		
    }

  });
});
</script>

<div class="container_12 clearfix">
	
	<div class="grid_8">
		
		<h4 style="margin-top: 0px">Hello <?=$this->member->first_name;?>, Tell Us About the Listing</h4>
		<p>Please tell us about your listing!</p>
	
		<form action="/listing/create/" method="post">
		
			<ul class="uiForm clearfix">
			
				<? echo $this->render('common/listing.information.html.php'); ?>
			
				<? echo $this->render('common/billing.information.html.php'); ?>
				
			</ul>
			
			<hr />
			
			<input type="hidden" name="post[imagePath]" value="" id="fileUploaded" />
			<input type="submit" class="uiButton right" value="Save" />	
			<input type="reset" class="uiButton right cancel" value="Reset" />	
		
		</form>
		
	</div>
	
	<div class="grid_4">
	
		A word about security?
	
	</div>
	
</div>