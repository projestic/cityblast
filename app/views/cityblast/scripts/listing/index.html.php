<?php if ($this->paginator): ?>

	<!--   START OF DISPLAY LISTINGS   -->
	
	<div class="row-fluid">




    <div class="listingBox" style="padding: 0px !important!;">



		<?php if ($this->listing404): ?>
		<div class="box clearfix" style="margin-bottom: 25px;">
			<h2>Oh Dear!</h2>
	
			<p style="font-size: 16px; margin-top: 12px;">It seems this property may be currently unavailable, or may have been temporarily de-listed.  This could mean that it has sold, or been briefly taken off the market
				<?php if ($this->listing404 != 'notfound' && intval($this->listing404) > 0 && $this->booking_agent_id): ?>(in which case it might still be available!)<?php endif; ?>
				.</p>
			<?php if ($this->listing404 != 'notfound' && intval($this->listing404) > 0 && $this->booking_agent_id): ?><p>Not to worry!</p>
			<p>Easily find out the status of this property by contacting an agent here:</p> <a href="javascript:void(0);" onclick="openBookBox('BOOK_SHOWING')" class="uiButton small" rel="nofollow">Ask An Agent!</a><?php endif; ?>
		</div>
		<script type="text/javascript">
		function openBookBox(request_type)
		{
			$.colorbox({width:"650px", height: "540px;",  href:"/booking/index/<?= $this->listing404 ?>/<?= $this->booking_agent_id; ?>/"+request_type});
		}
		</script>
		<?php endif; ?>
		
		

	
	
	<? echo $this->render('listing/listing-search.html.php'); ?>


		<?php 
		//counter is for adding clear:both styled div after every 3rd div for listing.
		$counter=0; 
		foreach ($this->paginator as $listing): 
		?>
		
			<div class="span4" <? if($counter%3==0) echo " style='margin-left: 0px !important;' "; ?>>
				
				<div class="listing_grid">

					<div class="pull-right clearfix" style="margin-bottom: 5px;">
						<a style="color: #000; font-size: 22px; font-weight: bold; margin-bottom: 3px;" href="/listing/view/<?php echo $listing->id ?>">
							$<?php echo number_format(floatval($listing->price)) ?>
						</a>
					</div>
							
	
					<div class="image_container">
						<a href="/listing/view/<?= $listing->id ?>">											
							<img src="<?= $listing->getSizedImage(276,182) ?>" alt="<?= $listing->city. " ".$listing->street ." ". $listing->postal_code ?>" style="width: 268px; height 182px;" />
						</a>
					</div>
	
		
					<div style="margin-top: 4px; margin-bottom: 10px;">


						<a style="color: #000; font-weight: bold;" href="/listing/view/<?php echo $listing->id ?>">
							<?php echo $listing->street; ?>
						</a>

							
						<br/>	


						<a style="color: #000;" href="/listing/view/<?php echo $listing->id ?>">
							<?php echo empty($listing->city) ? $listing->list_city->name : $listing->city ?>
						</a>

						<?/*
						<br/>
						

						<a style="color: #000;" href="/listing/view/<?php echo $listing->id ?>">
							<?php echo $listing->postal_code; ?>
						</a>
						*/ ?>
													
						
					</div>


					<a class="btn btn-cancel pull-right view_now" href="/listing/view/<?php echo $listing->id ?>">VIEW NOW</a>

					<div class="clearfix"></div>
					
				</div>									
	
				


			</div>
			
			<?php 
			
				//add clear div after every third listing.
				$counter++ ;
				if($counter%3==0)
				{			
					echo '<div style="clear:both">&nbsp;</div>';			
				}
			?>
		<?php endforeach ?>


			<?php if (!$counter) : ?>				
				<div style="clear:both"></div>
				<h3 style="text-align: center; margin-left: 10px;">We're sorry, none of our listings match your search criteria.</h3>
			
			<?php else: ?>
			

			<div style="clear:both"></div>
		
					        
				<div style="padding-right: 10px; float: right;">
					<?php echo $this->paginationControl($this->paginator, 'Elastic', '/common/pagination.phtml'); ?>
				</div>
	
				<div style="clear:both"></div>
						
			<?php endif ?>
		
			

			
			
			
		</div>
		

		
	</div>


</div>



<style type="text/css">
.view_listing 
{
	margin: 30px 0px 10px;
	padding: 20px 0 0 0;
}

.view_listing thead tr 
{
	height: 30px;
	background-color: #eee;
}

.view_listing tbody tr td 
{
	padding: 5px 2px;
}


.listing_grid 
{
	/*background: url("/images/accent1-bg.png") repeat scroll 0 0 transparent;*/
	
	background-color: #FFF;
	
	border: 1px solid #f1f1f1;
	
	padding: 15px;
	margin-bottom: 2px;
	position: relative;
	z-index: 1;
}

.listing_grid a:hover
{
	text-decoration: none !important;
}

.listing_grid .image_container
{
	width: 276px;
	height: 182px;
	overflow: hidden;
	margin-bottom: 4px;
}



.listing_grid .listing_detail
{
	float: left; 
	width: 170px; 
	font-weight: normal; 
	font-size: 14px;
}

.listing_grid .address
{
	color: #bdbdbd; 
	font-size: 11px; 
	font-weight: bold; 
	margin-top: 5px;
}

/* Search Box Styles starts*/

.filter_open
{
	background: url(/images/filter_opener-black.png) no-repeat;
	position: absolute;
	top: 1px;
	right: 10px;
	display: block;
	height: 30px;
	width: 30px;
	cursor: pointer;
}

.filter_open.close
{
	background-position: 0 -30px;
}

#advancedSearch{
	display: none;
}

#advancedSearch ul.uiForm{
	margin: 0px;
}

#advancedSearch .uiFormRow, #advancedSearch .form {
	background: none !important;
}

#advancedSearch .uiFormRow{
	border-bottom: solid 1px #EDEDED;
}
#advancedSearch .fieldname {
	padding: 18px 20px 19px !important;
	background-color: #FBFBFB;
	border-right: solid 1px #EDEDED;
}

#advancedSearch .li_180 {
	width: 180px !important;
}

#advancedSearch .li_160 {
	width: 160px !important;
}

#advancedSearch .selectWrapper{
	margin-bottom: 0px;
}

#advancedSearch input,
#advancedSearch select{
	margin-bottom: 0;
}

#advancedSearch input[class*="span"]{
	vertical-align: middle;
}

/*
.show-hide {
	float: right;
	clear: all;
	font-size: 14px;
	font-weight: bold;
	cursor: pointer;
	display: inline-block;
	color: sienna;
}*/
/* Search box Style Ends */
</style>

<script type="text/javascript">

	$(document).ready(function() {

		//autocomplete
		$('#search_city').autocomplete({
			appendTo: '#navigation_autocomplete',
			source: "/city/autocomplete",
			minLength: 2,

			select: function( event, ui ) {
				if(ui.item){//alert(ui.item.toSource())
					$('#city_id').val(ui.item.id);
				}
			},
			change:function(event, ui)
			{
				if(!ui.item){
					$('#city_id').val('');
					$(this).val('');
				}
			}

		}).focus(function() {
			if ($(this).val().search(/Select Your City/) != -1) {
				$(this).val('');
			}
		}).blur(function() {
			if ($(this).val() == '') {
				$(this).val('Select Your City');
			}
		});


		
		
		//bind click event to open/close search panel
		$('.filter_open').click(function(e)
		{
			var $this = $(this);
			var $AdvanceSearch = $('#advancedSearch');
			
			if($this.hasClass('close')){
				$AdvanceSearch.hide();
				$this.removeClass('close');
				$this.parent().addClass('roundit');
			}else{
				$AdvanceSearch.show();
				$this.addClass('close');
				$this.parent().removeClass('roundit');
			}
		});
		
		
		/*
		$('.show-hide').bind('click', function() {
			$('#advancedSearch').toggle('slow');
			if ($(this).text().search(/SHOW/i) == -1) {
		
				$(this).text($(this).text().replace("HIDE", "SHOW"));
			} else {
		
	    		$(this).text($(this).text().replace("SHOW", "HIDE"));
			}
		}); */

	});
</script>
<!--[if IE 7]>
	<style type="text/css">
		.listing_grid .grid_4 .view_now{
			width: 75px;
		}
		
	</style>
<![endif]-->

<!--   END OF DISPLAY LISTINGS   -->
<?php endif ?>
