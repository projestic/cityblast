
	<!--[if lt IE 9]>
		<style type="text/css">
			.box #note{
				margin-top: -10px;
				margin-bottom: 0px;
			}
		</style>
	<![endif]-->
	<!--[if IE 7]>
		<style type="text/css">
			.box #note{
				margin-top: -15px;
			}
		</style>
	<![endif]-->
	<div class="grid_12">

		<div class="box form clearfix">

			<div class="box_section">
				<h1>Share A Listing.</h1>

				<p>Want to share this property with friends?  Simply enter their email(s) below and <?=COMPANY_NAME;?> will forward them a link to the property of interest. It's that easy.</p>

				<div class="clearfix">
				</div>
			</div>

			<form action="/listing/share" method="POST" id="send_referral">


				<ul class="uiForm clearfix">
					<li class="uiFormRowHeader">Your Friend's Information</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Enter email(s) here:<br /><span style="font-size: 12px; font-style: italic; color: #999;">(Place a comma between multiple emails)</span></li>
							<li class="field">
								<input type="text" id="email_friend" name="email_friend" class="uiInput" style="width: 590px" />
								<input type="hidden" name="listing_id" value="<?php echo empty($this->listing_id) ? "" : $this->listing_id ?>" />
							</li>
						</ul>
					</li>

					<li class="uiFormRowHeader">Your Thoughts</li>
					<li class="uiFormRow clearfix">
						<ul>
							<li class="fieldname">Comments</li>
							<li class="field">
								<textarea name="comments" style="width: 590px; height: 75px;"  class="uiInput"/></textarea>

							</li>
						</ul>
					</li>

					<li class="uiFormRow clearfix">
						<ul>
							<li class="buttons" style="width: 950px;">
								<input type="submit" value="Send" class="uiButton right" id="send_id" />
							</li>
						</ul>
					</li>
				</ul>

			</form>

		</div>

	</div>