<!--[if lt IE 9]>
	<style type="text/css">
		.box #note{
			margin-top: -10px;
			margin-bottom: 0px;
		}
	</style>
<![endif]-->
<!--[if IE 7]>
	<style type="text/css">
		.box #note{
			margin-top: -15px;
		}
	</style>
<![endif]-->


    <div class="contentBox">

        	<div class="contentBoxTitle">   
			<h1>Share this listing with your Seller.</h1>
		</div>


			<p>You're a tech-savvy agent.  Now, showcase your online marketing strategy.  Simply enter your Seller client's email below and <?=COMPANY_NAME;?> will send them a professional 
				e-pamphlet, showcasing your Blast and online listing.  Get the credit you deserve.</p>


		<?/*<h4>Your Client's Information</h4>*/?>

		<form id="agentshareform" action="/listing/agentshare" method="POST">


			<ul class="uiForm clearfix">

				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname"><span style="color: #990000;">*</span>Enter email(s) here: <span style="margin-left: 15px;font-size: 12px; font-style: italic; color: #999;">(Place a comma between multiple emails)</span></li>
						<li class="field">
							<input type="text" id="email_friend" name="email_friend" class="uiInput" style="width: 100%" />
							<input type="hidden" name="listing_id" value="<?php echo empty($this->listing_id) ? "" : $this->listing_id ?>" />
						</li>
					</ul>
				</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="buttons">
							<input class="btn btn-primary btn-large pull-right" style="width: 195px;" type="submit" value="Send" class="uiButton right" id="send_id" />

						</li>
					</ul>
				</li>
			</ul>

		</form>

	</div>



<script type="text/javascript">

$(document).ready(function(){
	function isEmail(email) {
		var emails = email.split(',');
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		for (var i = 0; i < emails.length; i++) {
			if (!regex.test(emails[i].trim()) return false;
		}
		return true;
	}
	
	$('#agentshareform').submit(function(event){
		var inputEmail = $('#agentshareform input[name="email_friend"]');
		if (!isEmail(inputEmail.val())) {
			inputEmail.addClass('error');
			event.preventDefault();
			return false;
		} else {
			inputEmail.removeClass('error');
			return true;
		}
	});

});
</script>