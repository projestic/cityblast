<div class="contentBox">

   	<div class="contentBoxTitle">   									
		<h1>Success.</h1>
	</div>
	
	
	<p><?=COMPANY_NAME;?> is the new way that the real estate industry is using social media, and the way that the world's top agents are selling properties faster than ever before. 
	Your Client has received confirmation of your commitment to selling their property fast.</p>

			
	<a href="/listing/view/<?=$this->listing->id;?>" class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 20px;" >Preview Your Listing</a>

	
	<div class="clearfix"></div>

</div>