<div class="dashboardContent">

	<div class="contentBoxTitle">
				
		<h1>Here's What Your Fan Page Content Could Look Like With CityBlast's Help!</h1>
					
	</div>
	
	<div class="fbPostsHolder" style="border-top: 0px !important; margin-top: 10px !important; padding-top: 0px !important;">
	
		<div class="fbPostsAfterText arrowsAbove" style="margin-top: 30px; margin-bottom: 25px;">Do You Want Your Fan Page To Look Like This?</div>
		
		<img src="/images/fanpage-sample-banner.png" alt="Your New Fanpage!" style="margin-left: -7px;" />
		
		<div class="fbPosts clearfix">
		
			<?php $i = 0; foreach ($this->listings as $listing) : ?>
				<?php if ($i % 2 == 0) : ?><div class="clear"></div><?php endif; ?>
				<?= $this->fbEmbedPost(
										'/images/man_silhouette.png', 
										'Your Fan Page', 
										'Real Estate', 
										($listing['description'] ? $listing['description'] : $listing['message']), 
										$listing['content_link'], 
										$listing['image']
									); ?>
			<?php $i++; endforeach; ?>
		</div>
	
		<p class="fbPostsAfterText arrowsAside">Do you want your Facebook, Twitter and LinkedIn to look like this?</p>
	
	</div>

	<div class="clearfix">
	<div class="pull-left tryDemoTxt">
		<?/*<h3 style="font-size: 28px;">Get an Expert handling your business's social media.</h3>*/?>
		<h3 style="font-size: 28px;">Our <i>Social Experts</i> can help.</h3>
		<h4>Start your Free 14-Day Trial of <?=COMPANY_NAME;?>  <i>Social Experts</i> now.</h4>
	</div>
	<div class="pull-right">
		<a class="uiButtonNew tryDemo" href="/member/index" style="margin-top: 0px;">Try Demo.</a>
	</div>
	

</div>