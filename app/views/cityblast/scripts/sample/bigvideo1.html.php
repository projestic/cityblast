<style>
	/* Custom Popup */
	body, html {
		overflow: hidden;
		height: 100%;
		background: #000;
	}
</style>

<link rel="stylesheet" href="/js/bigvideo/css/bigvideo.css">
<script type="text/javascript" src="/js/video-js/video.js"></script>
<script type="text/javascript" src="/js/bigvideo/lib/bigvideo.js"></script>

<script>
	$(function() {
		var BV = new $.BigVideo();
		BV.init();
		BV.show('http://dev-www.cityblast.com/video/Stars_and_Planes.mp4',{ambient:true});
	});
</script>

<div class="cp-overlay">
	<div class="cp-content">
		<div class="cp-inner">
			<div class="cp-header-1">Turn Your Fanpage Into</div>
			<div class="cp-header-2">an <span class="vyebos">EXPLOSIVE<img src="/images/new-images/underline.png" alt="" /></span> Growth Engine For Your Real Estate Business</div>
			<form id="cp-form" class="cp-form" method="post" action="/sample/preview">
				<fieldset>
					<input type="text" name="city_and_state" placeholder="Enter Your City"><span class="cpf-button-container"><input type="submit" id="tryit" value="Try It Now!"><span class="cpf-arrow-2"><span class="cpf-text-2">Then Click To Watch The Magic!</span></span></span>
					<span class="cpf-arrow-1"></span>
					<span class="cpf-text-1">Tell us where you work</span>
				</fieldset>
			</form>
		</div>
	</div>
</div>

<script>
	// Custom popup
	var cp_center = function() {
		var $container = $('.cp-inner'),
			containerHeight = $container.height(),
			windowHeight = $(window).height();
		if (containerHeight < windowHeight) {
			$container.css('padding-top', (windowHeight - containerHeight) / 2);
		}
		else {
			$container.css('padding-top', 0);
		}
	}
	$(function() {
		cp_center();
		$('.cp-inner').show();
		$(window).resize(function() {
			cp_center();
		});

		$("#fanpage_preview").hide();

		$('.cp-form').submit(function(e) {
			e.preventDefault();
			var $input_text = $('input[name="city_and_state"]');
			if ($input_text.val() !== '') {


				// Close Overlay
				$('.cp-overlay').fadeOut(500, function() {
					$('body, html').css({
						'overflow': 'auto',
						'height': 'auto'
					});
				})


				$("#fanpage_preview").slideUp(1000);

				$("#fanpage_preview").slideDown(1000).html('<div class="dashboardContent"><div class="contentBoxTitle"><h1>We\'re Building Your Page!</h1></div><div class="fbPostsAfterText arrowsAbove" style="margin-top: 30px; margin-bottom: 25px;">Our Social Experts are building a custom example page just for you!</div><p><img width="780" height="144" style="display: block; margin: 0 auto;" src="/images/our_experts.png"></p><p style="margin-top: 30px;"><img src="/images/loading.gif" /></p></div></div>');


				$.post($('#cp-form').attr('action'), $('#cp-form').serialize(), function (response) {

					setTimeout(function (){

						//$("#fanpage_preview").fadeOut( 900 ).html("");

						$("#fanpage_preview").slideUp();

						setTimeout(function (){

							$("#fanpage_preview").slideDown().fadeIn( 3000 ).html(response);

						}, 1000);

					}, 4000);
				});



			}
			else {
				$input_text.focus();
			}
		});
	});
</script>

<div class="contentBox contentType" id="fanpage_preview"></div>

<div style="display:none">
	<div id="gotcha_popup_div">
		<h1>Made You Click!</h1>
		<p style="margin-top:20px">You've just seen the power of inbound marketing for yourself!</p>
	</div>
</div>

<script>

	$(".gotcha_popup").click(function(e)
	{
		e.preventDefault();
		$.colorbox({
			height: "200px",
			inline: true,
			href: "#gotcha_popup_div"
		});
	});


	/****************************
	 $(function(){

		var city 			= null;
		var country_code 	= null;	
		var region 		= null;
			
		google.load("search", "1.x", {callback: initialize});
		
		function initialize()
		{
			
			$(".geocomplete").geocomplete({
				details: "form"
			}).bind("geocode:result", function(event, result) {

				var country = '';
				var state   = '';
				var city    = '';
				
				$.each(result.address_components, function (ix, item) {
					if ($.inArray('locality', item.types) > -1) {
						city = item.long_name;
					} else if ($.inArray('administrative_area_level_1', item.types) > -1) {
						state = item.long_name;
					} else if ($.inArray('country', item.types) > -1) {
						country = item.short_name;
					} 
				});


				if (state == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>Please select a city and state/province from the dropdown menu to continue.</p>"});
					return false;
				}

				if (city == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>Please select a city from the dropdown menu to continue.</p>"});
					return false;
				}

			})
			.bind("geocode:multiple", function(event, status){
				console.log('multiple');
			}) 
			
			

			// City validation disabled 2013.10.17

			/* .bind("geocode:error", function(event, status){
				$('#tryusnow_step1_btn').addClass('disabled');
				step1_complete = false;
				$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>This part is a bit tricky! You must select a city <span style='color: #ff0000;'>from the Google Maps autocomplete dropdown</span>.</p><p>You'll know you've successfully picked a city when the <span style='color: #ff0000;'>red pin</span> shows up on the Google map!</p>"});
			}); *
				
		}

	});***********************/

</script>
