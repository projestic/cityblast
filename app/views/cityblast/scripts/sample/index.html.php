<form action="/sample/preview" method="POST" style="margin: 0px !important; padding: 0px !important;">
<input type="hidden" name="locality" value="" id="locality" /><? /* locality == city */ ?>
<input type="hidden" name="administrative_area_level_1" value="" id="administrative_area_level_1" /><? /* administrative_area_level_1 == state/province */ ?>
<input type="hidden" name="country" value="" id="country" />
	

	<div class="contentBox dashboardContent" id="step1">
		<div class="contentBoxTitle">
					
			<h1>Generate More Buyers and Sellers With A Powerful, DONE-FOR-YOU Facebook Marketing Tool.</h1>
						
		</div>

		<div class="stepBox">

			<div class="stepContent clearfix">

				<p class="fbPostsAfterText arrowsAboveLeft" style="margin-bottom: 12px !important;">Tell Us Where You Work!</p>
		
				<div style="overflow: hidden;">
					<ul class="uiForm full">
						<li class="clearfix">
							<ul>
								<li style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; width: 100%; padding: 0;" class="field cc clearfix">
								
									<input type="text" style="border: 1px solid rgb(28, 154, 218) ! important; background-color: rgb(245, 250, 252) ! important; float: left; box-sizing: border-box; width: 80%; height: 30px ! important;" class="geocomplete uiInput" name="location" id="defaultpubcity" placeholder="Enter Your City and State" autocomplete="off">								
									<a href="#" id="tryit" class="uiButton" style="margin-left: 2%; margin-bottom: 10px; display: inline-block; float: left; padding: 4px 0px; box-sizing: border-box; width: 18%; height: 30px ! important;">Try It!</a>
								
								</li>
							</ul>
						</li>
						
					</ul>
				</div>

				<p class="fbPostsAfterText arrowsAsideRight">Click Here To Watch The Magic!</p>

			</div>
				
		</div>
			
	</div>
	
</form>

<div class="contentBox contentType" id="fanpage_preview" style="display: none;"><img src="/images/loading.gif"></div>

<script>

	$('form').submit(function(event)
	{
		/* if ($('#locality').val() == '' || $('#administrative_area_level_1').val() == '' || $('#country').val() == '') 
		{
			$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>Please enter your city and state or province, then select an option from the dropdown menu to continue.</p>"});
			return false;
		} */
		
		event.preventDefault();
		
	//	$("#step1").slideUp();
		
		$("#fanpage_preview").slideDown();
		$.post($('form').attr('action'), $('form').serialize(), function (response) {
			$("#fanpage_preview").slideDown().html(response);
		});

	});

	$(function(){

		var city 			= null;
		var country_code 	= null;	
		var region 		= null;
			
		// Places Autocomplete disabled
		// google.load("search", "1.x", {callback: initialize});
		
		function initialize()
		{
			
			$(".geocomplete").geocomplete({
				details: "form"
			}).bind("geocode:result", function(event, result) {

				var country = '';
				var state   = '';
				var city    = '';
				
				$.each(result.address_components, function (ix, item) {
					if ($.inArray('locality', item.types) > -1) {
						city = item.long_name;
					} else if ($.inArray('administrative_area_level_1', item.types) > -1) {
						state = item.long_name;
					} else if ($.inArray('country', item.types) > -1) {
						country = item.short_name;
					} 
				});


				if (state == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>Please select a city and state/province from the dropdown menu to continue.</p>"});
					return false;
				}

				if (city == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>Please select a city from the dropdown menu to continue.</p>"});
					return false;
				}

			})
			.bind("geocode:multiple", function(event, status){
				console.log('multiple');
			}) 
			
			

			// City validation disabled 2013.10.17

			/* .bind("geocode:error", function(event, status){
				$('#tryusnow_step1_btn').addClass('disabled');
				step1_complete = false;
				$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>This part is a bit tricky! You must select a city <span style='color: #ff0000;'>from the Google Maps autocomplete dropdown</span>.</p><p>You'll know you've successfully picked a city when the <span style='color: #ff0000;'>red pin</span> shows up on the Google map!</p>"});
			}); */
				
		}

	});
	
</script>
