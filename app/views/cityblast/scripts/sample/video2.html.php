
<div class="contentBox" style="padding-left: 0px !important; padding-right: 0px; !important">
	
	<div id="video-container-2">
		<h2>The Ultimate &ldquo;<span style="color: #ff0000;">Social Marketing Automation Solution</span>&rdquo; 
			<br/>You Need To Start, Promote, And Grow Your Real Estate Business Online!</h2>

		<center><iframe src="//fast.wistia.net/embed/iframe/dz0k2qmc0n" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="853" height="480"></iframe></center>
		<?/*<center><iframe src="//player.vimeo.com/video/97663363?title=0&amp;byline=0&amp;portrait=0" width="853" height="481" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></center>*/?>
		
	</div>


	<center><h3 class="tk-open-sans" style="font-size: 20px; margin-top: 40px; margin-bottom: 20px;"><b>You're 30 Seconds Away From Your Own Personal Social Media Marketing Manager!</b></h3></center>

	<center><a class="uiButtonNew tryDemo" href="/index/why-us" style="padding-left: 50px; padding-right: 50px; width: 300px !important;">Learn More <i style="font-size: 30px;" class="fa icon-circle-arrow-right"></i></a>
	&nbsp;&nbsp;<a class="uiButtonNew tryDemo" href="/member/index" style="padding-left: 0px; padding-right: 0px; width: 400px !important;">Take A Test Drive <i style="font-size: 30px;" class="fa icon-circle-arrow-right"></i></a></center>

</div>


<div class="contentBox" style="padding-left: 0px !important; padding-right: 0px; !important; margin-top:0px; padding-top: 0px;">


	<h2 style="margin-bottom: 0px; padding-bottom: 5px; background-image:url(/images/arrow-grey-down-left.png); background-repeat: no-repeat; background-position: 150px 0">Trusted By The Best...</h2>


	<ul class="partnersList">

		<li><img src="/images/lp_logos/inman_news.png" width="250"></li>
		<li><img src="/images/lp_logos/active_rain.png" width="215"></li>
		<li><img src="/images/lp_logos/clareity_store.png" width="205"></li>
		
		<li class="small"><img src="/images/lp_logos/royal_lepage.png" width="190"></li>
		<li class="small"><img src="/images/lp_logos/lone_wolf.png" width="190"></li>
		<li class="small"><img src="/images/lp_logos/retechulous.png" width="190"></li>
		<li class="small"><img src="/images/lp_logos/see_virtual.png" width="190"></li>


		<li class="small"><img src="/images/lp_logos/techvibes.png" width="190"></li>
		<li class="small"><img src="/images/lp_logos/real_estate_wealth.png" width="190"></li>
		<li class="small"><img src="/images/lp_logos/globe_and_mail.png" width="190"></li>

		<li class="small"><img src="/images/lp_logos/real_estate_talk_show.png" width="190"></li>

	</ul>


</div>

<div class="contentBox" style="padding-left: 0px !important; padding-right: 0px; !important; margin-top:0px; padding-top: 0px;">


	<h2 style="margin-bottom: 8px; padding-bottom: 5px;; background-image:url(/images/arrow-grey-down-right.png); background-repeat: no-repeat; background-position: 800px 0">...Used By The Best!</h2>
	
	
	
	<div class="testimonialsHolder clearfix" style="border: 0px; width:820px; margin: 0 auto">

			<div class="colLeft">
				<div class="item clearfix">
					<div class="thumbHolder">
						<span><a href="https://www.facebook.com/ingrid.brunsch" target="_new"><img src="/images/ingrid-brunsch.jpeg" alt=""></a></span>
					</div>
					<div class="textHolder">
						<p>Thanks so much! I must tell you I really to appreciate the articles that appear on my Facebook and Twitter page 
							daily - saves me so much time not having to hunt around trying to find appropriate information to post. I have also taken advantage 
							of blasting my listings and am not sure if it is working in terms of selling them any faster - but, it makes a wonderful tool 
							when I speak to my clients about my marketing tools - I appear to be very tech savvy - if they only knew! 
							<i>~<a href="https://www.facebook.com/ingrid.brunsch" target="_new">Ingrid Brunsch</a></i></p>
					</div>
				</div>


				<div class="item clearfix">
					<div class="thumbHolder">
						<span><a href="https://www.facebook.com/leila.talibova" target="_new"><img src="/images/leila-talibova.jpg" alt=""></a></span>
					</div>
					<div class="textHolder">
						<p>My friends and previous colleagues noticed my Facebook updates right away. In only my first week using CityBlast, I received a call from a family 
							friend who said she'd seen my Facebook and wanted to list their home with me. The experts are awesome! 
							<i>~<a href="https://www.facebook.com/leila.talibova" target="_new">Leila Talibova</a></i></p>
					</div>
				</div>

				

				

				
				<div class="item clearfix">
					<div class="thumbHolder">
						<span><a href="https://www.facebook.com/Sunny.Dehghan" target="_new"><img src="/images/sunny-dehghan.jpg" alt=""></a></span>
					</div>
					<div class="textHolder">
						<p>I have a lot of realtor friends and my biggest concern was that I would be getting the exact same posts as everyone else. Well, I've been a member for over 8 months and so far so good! To be honest, it does
							everything as advertised and my clients think all I do is stay on top of industry news. Even my parents said something about my Facebook posts! 
							<i>~<a href="https://www.facebook.com/Sunny.Dehghan" target="_new">Sunny Dehghan</a></i></p>
					</div>
				</div>
				



			</div>
			<div class="colRight">
				<div class="item clearfix">
					<div class="thumbHolder">
						<span><a href="https://www.facebook.com/shereecerqua" target="_new"><img src="/images/sheree-cerqua.jpg" alt=""></a></span>
					</div>
					<div class="textHolder">
						<p>I use CityBlast to help me market. Not only are they the heart of my online platform, but also help me to sell my clients' listings quickly, and to achieve 
							excellent prices. CityBlast is an incredible service for both beginners and top agents, and I highly recommend them. 
							<i>~<a href="https://www.facebook.com/shereecerqua" target="_new">Sheree Cerqua</a></i></p>
					</div>
				</div>


				<div class="item clearfix">
					<div class="thumbHolder">
						<span><a href="http://www.linkedin.com/profile/view?id=68287523" target="_new"><img src="/images/ashley-gollogly.jpg" alt="Ashley Gollogly"></a></span>
					</div>
					<div class="textHolder">
						<p>I'm pretty head strong when it comes to social media. I knew what I wanted to post on my wall, the problem was finding the time to do it every day. Thankfully I found CityBlast. 
							Now I can focus on the things that require my personal attention and I know that my Social Media marketing is being taken care of by dedicated professionals. P.S. I went 
							on vacation in January and my Social Expert was posting even when I was away. I came back to find 2 new leads in my Facebook inbox. Shhhh! Don't tell! 
							<i>~<a href="http://www.linkedin.com/profile/view?id=68287523" target="_new">Ashley Gollogly</a></i></p>
					</div>
				</div>


				<div class="item clearfix">
					<div class="thumbHolder">
						<span><a href="https://www.facebook.com/valpasquini" target="_new"><img src="/images/valentina-pasquini.jpg" alt=""></a></span>
					</div>
					<div class="textHolder">

						<p>I have a young demanding family and fledgling business real estate business. Between diaper changes and open houses, quite frankly, I just couldn't find the time to add yet another thing to my to-do list. 
							Even though I knew that Social Media was the most talked about thing as Re/Max Kickstart, I just couldn't find the time to do it myself. CityBlast completely turned around my social media presence and now I have one less thing
							to worry about. <i>~<a href="https://www.facebook.com/valpasquini" target="_new">Valentina Pasquini</a></i></p>

					</div>
				</div>



			</div>


	</div>


	<center><h3 class="tk-open-sans" style="font-size: 28px; margin-top: 40px; margin-bottom: 20px;"><b>Yes!</b> <span style="font-weight: normal;">I Want to Test Drive The Power of Cityblast for My Business.</span></h3></center>
	
	<div class="clearfix" style="text-align: center; background-image:url(/images/arrow-down-left.png); background-repeat:no-repeat; background-position:50px 10px">

			<a class="uiButtonNew tryDemo" href="/member/index" style="width: 750px;">Try The Demo Today!</a>

	</div>




</div>