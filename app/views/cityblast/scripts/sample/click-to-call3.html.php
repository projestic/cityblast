<div class="cp-overlay cp-layout-3">
	<div class="cp-content">
		<div class="cp-inner">
			<div class="cp-width-wrapper">
				<div class="cp-header-1 tight">In Less Than <strong>30 Minutes</strong> Your Facebook Page Could Be A <strong>Lead Generating <span class="vyebos">Machine!<img src="/images/new-images/underline.png" alt="" /></span></strong></div>
			</div>
			<div class="cp-bg-wrapper large">
				<div class="cp-width-wrapper clearfix">
					<div class="modal-col-left">
						<div class="text">Yes, I Like Generating Leads and Want A FREE Facebook Makeover From Cityblast’s Social Media Experts.</div>
						<div class="cp-modal-bottom">Would you like us to <a href="javascript:void($zopim.livechat.window.show())" >call you</a> instead?</div>
					</div>
					<div class="modal-col-right">
						<div class="cp-phone-holder">
							<p><a href="tel:18448792489" class="modal-phone">1.844.879.2489</a></p>
							<?
							/*
								1-844-TRY-CITY
								1-844-FB-WORKS
								1-844-WE-POST1
							*/ 
							?>
							<span class="arrow"></span>
							<span class="text">Let’s Chat</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
	/* Custom Popup */
	body, html {
		overflow: hidden;
		height: 100%;
	}
</style>

<script>
	// Custom popup
	var cp_center = function() {
		var $container = $('.cp-inner'),
			containerHeight = $container.height(),
			windowHeight = $(window).height();
		if (containerHeight < windowHeight) {
			$container.css('padding-top', (windowHeight - containerHeight) / 2);
		}
		else {
			$container.css('padding-top', 0);
		}
	}
	$(function() {
		cp_center();
		$('.cp-inner').show();
		$(window).resize(function() {
			cp_center();
		});

		$('.cp-form').submit(function(e) {
			e.preventDefault();
			var $input_text = $('input[name="city_and_state"]');
			if ($input_text.val() !== '') {
				
				
				// Close Overlay
				$('.cp-overlay').fadeOut(500, function() {
					$('body, html').css({
						'overflow': 'auto',
						'height': 'auto'
					});
				}) 


				
				$('#fanpage_preview').hide().html( '<div class="dashboardContent"><div class="contentBoxTitle"><h1>We\'re Building Your Page!</h1></div><div class="fbPostsAfterText arrowsAbove" style="margin-top: 30px; margin-bottom: 25px;">Our Social Experts are building a custom example page just for you!</div><p><img width="780" height="144" style="display: block; margin: 0 auto;" src="/images/our_experts.png"></p><p style="margin-top: 30px;"><img src="/images/loading.gif" /></p></div></div>' ).slideDown(1000, function(){
					setTimeout( function(){
						
				     	$('#fanpage_preview').slideUp(500, function(){
				        	
				        		$.post($('#cp-form').attr('action'), $('#cp-form').serialize(), function (response) {
				        	
				            		$('#fanpage_preview').html( response ).slideDown(1000);
				        		});
				        	});
				        	
				    	}, 5000);
				});
			
		
			}
			else {
				$input_text.focus();
			}
		});
	});
</script>

<div class="contentBox contentType" id="fanpage_preview">

	<div class="fbPostsHolder">
			
		<img src="/images/fanpage-sample-banner.png" alt="Your New Fanpage!" style="margin-left: -7px;" />


		<div class="fbPosts clearfix">

			<?=$this->fbEmbedPost(
				'/images/man_silhouette.png',
				'Your Fan Page',
				'Real Estate',
				'31 Insanely Clever Remodeling Ideas For Your New Home. I dare you to get through this post without calling your contractor! P.S. Comment below to let me know which one you think is the coolest/cleverest...',
				'http://www.buzzfeed.com/peggy/insanely-clever-remodeling-ideas-for-your-new-home',
				'https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-prn1/t1.0-9/p370x247/1016243_600454043362936_1038823544_n.jpg',
				'February 9'
			);?>

			<?=$this->fbEmbedPost(
				'/images/man_silhouette.png',
				'Your Fan Page',
				'Real Estate',
				'How important is the "walkability" score for you? A recent survey by Inman suggests that most of us most of us would prefer to live in a neighborhood with a mix of houses, stores, and other businesses that are within walking distance. What do you think?',
				'http://www.realtor.org/articles/nar-2013-community-preference-survey',
				'https://scontent-b-fra.xx.fbcdn.net/hphotos-prn2/t1.0-9/p370x247/1798421_600457666695907_567387369_n.png',
				'February 9'
			);?>

			<div class="clear"></div>

			<?=$this->fbEmbedPost(
				'/images/man_silhouette.png',
				'Your Fan Page',
				'Real Estate',
				"People commonly think that the absolute best time to sell your home is in the spring and that the worst time is in the fall. Times have changed drastically. If you're not sure what the best time to sell your home is, check out what the experts at HGTV have to say to get the inside scoop!",
				'http://www.hgtv.ca/realestate/article/the-best-season-to-sell-your-home/',
				'https://scontent-b-fra.xx.fbcdn.net/hphotos-prn2/t1.0-9/p370x247/1606950_600464326695241_1521548139_n.jpg',
				'February 9'
			);?>

			<?=$this->fbEmbedPost(
				'/images/man_silhouette.png',
				'Your Fan Page',
				'Real Estate',
				'Thinking about revamping your bathroom? Any of these 12 incredible bathtubs would surely be a great addition!',
				'http://airows.com/12-incredible-bathtubs/',
				'https://scontent-b-fra.xx.fbcdn.net/hphotos-frc1/t1.0-9/s526x395/1618534_600468470028160_299312881_n.jpg',
				'February 9'
			);?>

		</div>
	
	</div>
</div>



<div style="display:none">
	<div id="gotcha_popup_div">
		<h1>Made You Click!</h1>
		<p style="margin-top:20px">You've just seen the power of inbound marketing for yourself!</p>
	</div>
</div>

<script>
	
	$zopim(function() 
	{
		$zopim.livechat.hideAll();
		$zopim.livechat.setNotes('CLICK TO CALL 3'); 
	
		$zopim.livechat.setGreetings({
		     'online': 'More Questions?',
		     'offline': 'Leave us a message'
		 });
		
		$zopim.livechat.window.setTitle('Want More Leads?');
		
		$zopim.livechat.concierge.setName("Ask Mike");    
		
		$zopim.livechat.concierge.setAvatar('https://graph.facebook.com/100007570274057/picture');			
	});	


	$(".gotcha_popup").click(function(e)
	{
		e.preventDefault();
		$.colorbox({
			height: "200px",
			inline: true, 
			href: "#gotcha_popup_div"
		});
	});			
	
	
</script>
