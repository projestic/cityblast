<div class="cp-overlay">
	<div class="cp-content">
		<div class="cp-inner">
			<div class="cp-header-1">Turn Your Fanpage Into</div>
			<div class="cp-header-2">an <span class="vyebos">EXPLOSIVE<img src="/images/new-images/underline.png" alt="" /></span> Growth Engine For Your Real Estate Business</div>
			<form id="cp-form" class="cp-form" method="post" action="/sample/preview">
				<fieldset>
					<input type="text" name="city_and_state" placeholder="Enter Your City"><span class="cpf-button-container"><input type="submit" id="tryit" value="Try It Now!"><span class="cpf-arrow-2"><span class="cpf-text-2">Then Click To Watch The Magic!</span></span></span>
					<span class="cpf-arrow-1"></span>
					<span class="cpf-text-1">Tell us where you work</span>
				</fieldset>
			</form>
		</div>
	</div>
</div>

<style>
	/* Custom Popup */
	body, html {
		overflow: hidden;
		height: 100%;
	}
</style>

<script>
	// Custom popup
	var cp_center = function() {
		var $container = $('.cp-inner'),
			containerHeight = $container.height(),
			windowHeight = $(window).height();
		if (containerHeight < windowHeight) {
			$container.css('padding-top', (windowHeight - containerHeight) / 2);
		}
		else {
			$container.css('padding-top', 0);
		}
	}
	$(function() {
		cp_center();
		$('.cp-inner').show();
		$(window).resize(function() {
			cp_center();
		});

		$('.cp-form').submit(function(e) {
			e.preventDefault();
			var $input_text = $('input[name="city_and_state"]');
			if ($input_text.val() !== '') {
				
				
				// Close Overlay
				$('.cp-overlay').fadeOut(500, function() {
					$('body, html').css({
						'overflow': 'auto',
						'height': 'auto'
					});
				}) 


				
				$('#fanpage_preview').hide().html( '<div class="dashboardContent"><div class="contentBoxTitle"><h1>We\'re Building Your Page!</h1></div><div class="fbPostsAfterText arrowsAbove" style="margin-top: 30px; margin-bottom: 25px;">Our Social Experts are building a custom example page just for you!</div><p><img width="780" height="144" style="display: block; margin: 0 auto;" src="/images/our_experts.png"></p><p style="margin-top: 30px;"><img src="/images/loading.gif" /></p></div></div>' ).slideDown(1000, function(){
					setTimeout( function(){
						
				     	$('#fanpage_preview').slideUp(500, function(){
				        	
				        		$.post($('#cp-form').attr('action'), $('#cp-form').serialize(), function (response) {
				        	
				            		$('#fanpage_preview').html( response ).slideDown(1000);
				        		});
				        	});
				        	
				    	}, 5000);
				});
			
		
			}
			else {
				$input_text.focus();
			}
		});
	});
</script>

<div class="contentBox contentType" id="fanpage_preview">

	<div class="fbPostsHolder">
			
		<img src="/images/fanpage-sample-banner.png" alt="Your New Fanpage!" style="margin-left: -7px;" />


		<div class="fbPosts clearfix">

			<?=$this->fbEmbedPost(
				'/images/man_silhouette.png',
				'Your Fan Page',
				'Real Estate',
				'31 Insanely Clever Remodeling Ideas For Your New Home. I dare you to get through this post without calling your contractor! P.S. Comment below to let me know which one you think is the coolest/cleverest...',
				'http://www.buzzfeed.com/peggy/insanely-clever-remodeling-ideas-for-your-new-home',
				'https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-prn1/t1.0-9/p370x247/1016243_600454043362936_1038823544_n.jpg',
				'February 9'
			);?>

			<?=$this->fbEmbedPost(
				'/images/man_silhouette.png',
				'Your Fan Page',
				'Real Estate',
				'How important is the "walkability" score for you? A recent survey by Inman suggests that most of us most of us would prefer to live in a neighborhood with a mix of houses, stores, and other businesses that are within walking distance. What do you think?',
				'http://www.realtor.org/articles/nar-2013-community-preference-survey',
				'https://scontent-b-fra.xx.fbcdn.net/hphotos-prn2/t1.0-9/p370x247/1798421_600457666695907_567387369_n.png',
				'February 9'
			);?>

			<div class="clear"></div>

			<?=$this->fbEmbedPost(
				'/images/man_silhouette.png',
				'Your Fan Page',
				'Real Estate',
				"People commonly think that the absolute best time to sell your home is in the spring and that the worst time is in the fall. Times have changed drastically. If you're not sure what the best time to sell your home is, check out what the experts at HGTV have to say to get the inside scoop!",
				'http://www.hgtv.ca/realestate/article/the-best-season-to-sell-your-home/',
				'https://scontent-b-fra.xx.fbcdn.net/hphotos-prn2/t1.0-9/p370x247/1606950_600464326695241_1521548139_n.jpg',
				'February 9'
			);?>

			<?=$this->fbEmbedPost(
				'/images/man_silhouette.png',
				'Your Fan Page',
				'Real Estate',
				'Thinking about revamping your bathroom? Any of these 12 incredible bathtubs would surely be a great addition!',
				'http://airows.com/12-incredible-bathtubs/',
				'https://scontent-b-fra.xx.fbcdn.net/hphotos-frc1/t1.0-9/s526x395/1618534_600468470028160_299312881_n.jpg',
				'February 9'
			);?>

		</div>
	
	</div>
</div>



<div style="display:none">
	<div id="gotcha_popup_div">
		<h1>Made You Click!</h1>
		<p style="margin-top:20px">You've just seen the power of inbound marketing for yourself!</p>
	</div>
</div>

<script>



		$(".gotcha_popup").click(function(e)
		{
			e.preventDefault();
			$.colorbox({
				height: "200px",
				inline: true, 
				href: "#gotcha_popup_div"
			});
		});			
		

	/****************************
	$(function(){

		var city 			= null;
		var country_code 	= null;	
		var region 		= null;
			
		google.load("search", "1.x", {callback: initialize});
		
		function initialize()
		{
			
			$(".geocomplete").geocomplete({
				details: "form"
			}).bind("geocode:result", function(event, result) {

				var country = '';
				var state   = '';
				var city    = '';
				
				$.each(result.address_components, function (ix, item) {
					if ($.inArray('locality', item.types) > -1) {
						city = item.long_name;
					} else if ($.inArray('administrative_area_level_1', item.types) > -1) {
						state = item.long_name;
					} else if ($.inArray('country', item.types) > -1) {
						country = item.short_name;
					} 
				});


				if (state == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>Please select a city and state/province from the dropdown menu to continue.</p>"});
					return false;
				}

				if (city == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>Please select a city from the dropdown menu to continue.</p>"});
					return false;
				}

			})
			.bind("geocode:multiple", function(event, status){
				console.log('multiple');
			}) 
			
			

			// City validation disabled 2013.10.17

			/* .bind("geocode:error", function(event, status){
				$('#tryusnow_step1_btn').addClass('disabled');
				step1_complete = false;
				$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>This part is a bit tricky! You must select a city <span style='color: #ff0000;'>from the Google Maps autocomplete dropdown</span>.</p><p>You'll know you've successfully picked a city when the <span style='color: #ff0000;'>red pin</span> shows up on the Google map!</p>"});
			}); *
				
		}

	});***********************/
	
</script>
