<div class="contentBox" style="padding-left: 0px !important; padding-right: 0px; !important">
	
	<div id="video-container-2">
		<h2>The Essential &ldquo;<span style="color: #ff0000;">Social Marketing Automation Solution</span>&rdquo; 
			<br/>You Need To Start, Promote and Grow Your Real Estate Business Online</h2>
		
			<center><iframe src="//fast.wistia.net/embed/iframe/dz0k2qmc0n" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="853" height="480"></iframe></center>
			<?/*<iframe src="//player.vimeo.com/video/97663363?title=0&amp;byline=0&amp;portrait=0" width="853" height="481" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>*/?>
	</div>

	<center><h3 class="tk-open-sans" style="font-size: 28px; margin-top: 40px; margin-bottom: 20px;"><b>Yes!</b> <span style="font-weight: normal;">I want CityBlast To Manage My Social Media and Get Me More Leads.</span></h3></center>

	<div id="get-started-now">
		<a class="uiButtonNew tryDemo" href="/member/index" style="padding-left:80px; padding-right:80px; width:auto">Get Started Now. <div style="font-size: 14px; font-weight: normal; margin-top: 0px; padding-top: 0px;">FREE 2-Week Trial For a Limited Time.</div></a>
	</div>

</div>


<div class="contentBox" style="padding-left: 0px !important; padding-right: 0px; !important">

	<h2 style="margin-bottom: 35px;">Get a Social Expert managing your social media for less than $2/day, and never miss a deal again!</h2>

	<img src="/images/new-images/video1-img1.png" style="float:right">
	<h3 class="video-heading" style="padding-top:60px">We'll plan and manage your lead generation strategy.</h3>
	<p class="video-p">Simply tell us the city where you work, and what kind of posts suit your style best. Our Social Experts do the rest - planning your marketing, sourcing articles and videos, and posting them to your accounts.</p>

	<div class="clearfix"></div>

	<img src="/images/new-images/video1-img2.png" style="float:left; margin-right:40px">
	<h3 class="video-heading" style="padding-top:40px">As often as you choose, we post articles, videos &amp; information.</h3>
	<p class="video-p">You also control how often your Social Expert will post. Following your instructions, they'll consistently post updates to your Facebook, Twitter and/or LinkedIn accounts that make sure you look professional and up-to-date.</p>

	<div class="clearfix"></div>

	<img src="/images/new-images/video1-img3.png" style="float:right; margin-top:-130px">
	<h3 class="video-heading" style="padding-top:20px">Manage your leads and measure success in your dashboard.</h3>
	<p class="video-p">With your powerful, professional presence, you'll see an increase in new leads through your social media accounts.  You just follow up with new leads as they come in, and generate more new business.</p>


	<div class="clearfix"></div>
	<div style="text-align: center;">
		<h3 class="tk-open-sans" style="font-size: 28px; margin-top: 40px; margin-bottom: 20px;"><b>Yes!</b> <span style="font-weight: normal;">I Want to Test Drive The Power of Cityblast for My Business.</span></h3>
	</div>


	<div class="clearfix" style="text-align: center; background-image:url(/images/arrow-down-left.png); background-repeat:no-repeat; background-position:50px 140px">

		<a class="uiButtonNew tryDemo" href="/member/index" style="width: 750px;margin-top:20px">Start Your FREE 14 Day Trial Today!</a>
	</div>

</div>