
<style type="text/css">
	.satisfaction_wrapper h1, .satisfaction_wrapper h2, .satisfaction_wrapper h3, .satisfaction_wrapper h4, .satisfaction_wrapper h5, .satisfaction_wrapper h6{
		/*font-family:  Futura, Impact, Arial Black, Arial; */
		font-weight: bold; 
		color: #1d1d1d;
		margin: 0;
	}
	#preview_blast{
		background-position: 0 -420px;
		height: 70px;
		float: none;
		/*font-family:  Futura, Impact, Arial Black, Arial;*/
		font-style: normal;
		letter-spacing: 1px;
		width: 620px;
		font-size: 38px;
		font-weight: normal;
		-moz-box-shadow: none;;
		-webkit-box-shadow: none;
		box-shadow: none;
		text-shadow: none;
		color: #FDFDFD;
	}
	#preview_blast:hover{
		background-position: 0 -490px;
		color: #FFF;
	}
	
	.satisfaction_wrapper{
		padding: 5px;
		margin-left: 10px;
		width: 990px !important;
	}
	
	.satisfaction_container{
		padding: 5px;
		background-color: #d8021f; 
		position: relative; 
		-moz-border-radius: 8px;
		-webkit-border-radius: 8px;
		border-radius: 8px 8px 8px 8px;
		behavior: url(/css/PIE.htc);
	}
	
	.satisfaction_container .blast_scheduled{
		float: left; 
		width: 700px; 
		position: relative; 
		z-index: 1;
		padding-bottom: 65px;
	}
	
	.satisfaction_container .statisfaction_gauranteed{
		position: relative; 
		float: left; 
		height: 170px; 
		width: 235px;
	}
	
	.blast_scheduled p{
		line-height: 1.3; 
		font-size: 12px; 
		color: #FFF;
	}
	
	.blast_scheduled .success_text{
		color: #FFF; 
		margin-bottom: 10px; 
		font-size: 27px; 
		font-weight: bold;
		margin-top: 10px;
	}
	
	.satisfaction_container .now_share_this{
		float: left; 
		position: relative; 
		z-index: 1; 
		padding-top: 10px; 
		padding-left: 10px; 
		padding-right: 4px; 
		padding-bottom: 10px; 
		margin-top: 15px;
	}
	
	.now_share_this .now_share_this_container{
		background: url(/images/yellow-box.png) no-repeat; 
		height: 232px; 
		width: 220px; 
		position: relative; 
		padding-left: 10px; 
		padding-top: 10px;
	}
	
	.now_share_this .now{
		color: #da0220; 
		font-size: 65px; 
		margin: -5px 0px 10px 0px;
	}
	
	.now_share_this .share_this{
		margin: -17px 0px 0px 0px; 
		font-size: 29px;
	}
	
	.now_share_this .blast_with,
	.now_share_this .your_client{
		margin-top: -7px; 
		margin-bottom: 0px; 
		font-size: 29px;
		height: 36px;
	}
	
	.now_share_this .email_now{
		margin-top: 12px; 
		width: 209px; 
		text-align: center;
		height: 55px;
	}
	
	.now_share_this .email_now .click_here{
		color: #b3b3b3;
		font-size: 14px; 
		margin-bottom: 0px;
	}
	
	.now_share_this .email_now .click_here a{
		text-decoration: none; 
		color: #b3b3b3;
	}
	
	.now_share_this .email_now .text a{
		text-decoration: none;
		color: #1D1D1D;
	}
	.now_share_this .email_now .text{
		font-size: 36px;
		margin-top: -8px; 
	}
	
	.satisfaction_container .reached_tag{
		position: absolute; 
		left: 125px; 
		bottom: 24px; 
		background-color: #1d1d1d; 
		color:#FFF; 
		padding: 6px 17px 7px 125px; 
		border-radius: 8px 0px 0px 8px;
		height: 34px;
	}
	
	.satisfaction_container .reached_tag .title{
		float: left; 
		font-size: 27px; 
		color: #FFF;
	}
	
	.satisfaction_container .reached_tag .title span,
	.now_share_this .email_now .text span{
		color: #da0220;
	}
	
	.satisfaction_container .reached_tag .arrow{
		position: absolute; 
		right: -44px; 
		top: 0px;
	}
</style>
<!--[if IE 7]>
	<style type="text/css">
		hr{
			margin-bottom: -5px;
		}
		.satisfaction_container .reached_tag .arrow{
			height: 52px;
			right: -50px;
		}
		.satisfaction_container .reached_tag{
			padding-top: 9px;
			padding-bottom: 10px;
		}
		
	</style>
<![endif]-->
<div class="grid_12 box satisfaction_wrapper">
	<div class="satisfaction_container">
		<div class="grid_9 blast_scheduled">
			<h3 class="success_text">SUCCESS. YOUR PROPERTY BLAST HAS BEEN SCHEDULED.</h3>
			<div class="statisfaction_gauranteed">
				<img src="/images/satisfaction-guarantee.png" alt="CityBlast : 100% Satisfaction Guaranteed" border="0" style="position: absolute; left: -50px;" />
			</div>
			<div style="float: left; width: 440px; margin-top: -10px; padding-bottom: 5px;">
				<h1 style="font-size: 45px; margin: 0; margin-left: -40px; margin-top: 4px;">
					NOW MAKE SURE
				</h1>
				<h1 style="font-size: 45px; margin: 0;">
					YOUR SELLER KNOWS.
				</h1>
				<p>Your Blast has been scheduled, and will shortly be advertised
					to tens of thousands of potential local buyers. There's only one
					thing left to do: share your Blast with your seller client. Simply
					enter your seller's email, and CityBlast will send them a
					professionale-pamphlet, showcasing your Blast and online listing -
					FREE. Get the credit you deserve.
				</p>
			</div>
		</div>
		<div class="grid_3 box now_share_this">
			<div class="now_share_this_container">
				<img src="/images/city-blast-batch.png" alt="CityBlast" border="0" style="position: absolute; right: -7px; top: 8px;" />
				<h1 class="now">NOW</h1>
				<h3 class="share_this">SHARE THIS</h3>
					<h3 class="blast_with">BLAST WITH </h3>
					<h3 class="your_client">YOUR CLIENT!</h3>
				<div class="email_now">
					<h6 class="click_here"><a href="#">CLICK HERE TO</a></h6>
					<h3 class="text"><a href="#">EMAIL <span>NOW</span></a></h3>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
		<div class="reached_tag">
			<h3 class="title">
				<span>6,800</span> REACHED TODAY ALREADY
			</h3>
			<img src="/images/reached_tag_arrow.png" alt="" border="0" class="arrow" />
			<div style="clear: both;"></div>
		</div>
	</div>
</div>
<div class="grid_8">

	<div class="box clearfix" style="padding-top: 20px;">
<!--
		<h1>Success.</h1>

		<div class="clearfix">

			<div style="width: 380px; float: left;">
				<h3>Your Blast Is Being Scheduled.</h3>


				<p>Congratulations, your listing is now visible on CityBlast.com.  Your marketing Blast has also been sent to the scheduler, and will begin advertising
				your new listing to tens of thousands of potential buyers on Facebook, Twitter and LinkedIn shortly.</p>

			</div>
			<div style="width: 190px; margin: -30px 0 0 40px; float: left;">

				<div style="float: right;"><img src="/images/successful_blast_badge.png" width="175" height="175" /></div>

			</div>

		</div>
-->
			<? if(isset($this->payment) && strtoupper($this->payment->paypal_confirmation_number) == "FREE BLAST") : ?>

			<div style="margin-top: 14px;"></div>
        		<div class="box white60 clearfix">


				<h3>That one was on the house.</h3>
				<p>CityBlast thanks you for your business. Enjoy your complementary Blast ? that one is FREE.</p>

				<? /**************
				<? if($this->listing->member->credits > 0): ?>
					And you've still got <?=$this->listing->member->credits;?> Free Blast<? if ($this->listing->member->credits > 1); echo "s"; ?> left.
				<? endif; ?>
				<a href="//www.youtube.com/watch?v=W9_nXlvY6Io" target="_new">Can You Feel the Love?</a>
				************/ ?>

			</div>
        	<? endif; ?>

		<!--<h3>My Invoice  </h3>-->

		<div class="box clearfix" style="background-color: #282828;">

			<table width="580" border="0" cellspacing="0" cellpadding="0" style="color: #FFF; text-shadow: none;">
				<tr>
					<td height="24" style="padding-top: 8px;"><strong style="color: #FFF;">Invoice Number:</strong> #<?=number_format(sprintf("%03d",$this->order_number), 0, '.', '-');?></td>

					<td height="24" style="text-align: right;padding-top: 8px;"><strong style="color: #FFF;">Invoice Date: </strong><? if(isset($this->listing->created_at) && !empty($this->listing->created_at)) echo $this->listing->created_at->format("M d, Y"); ?></td>
				</tr>
				<tr>
					<td width="290" valign="top" style="padding-top: 11px;">
						<strong style="color: #FFF;"><?=isset($this->member) ? $this->member->first_name : "" ;?>
						<?=isset($this->member) ? $this->member->last_name : "";?></strong><br />
						<? if(isset($this->payment->address1) && !empty($this->payment->address1)  && $this->payment->address1 != "empty" && $this->payment->address1 != "undefined") echo $this->payment->address1;?>
						<? if(isset($this->payment->address2) && !empty($this->payment->address2)  && $this->payment->address2 != "empty" && $this->payment->address2 != "undefined") echo $this->payment->address2;?><br />
						<? if(isset($this->payment->city) && !empty($this->payment->city)  && $this->payment->city != "empty" && $this->payment->city != "undefined") echo $this->payment->city;?>,
						<? if(isset($this->payment->state) && !empty($this->payment->state)  && $this->payment->state != "empty" && $this->payment->state != "undefined") echo $this->payment->state;?>
						<? if(isset($this->payment->zip) && !empty($this->payment->zip)  && $this->payment->zip != "empty" && $this->payment->zip != "undefined") echo "<br/>".strtoupper($this->payment->zip);?><br />
						<? if(isset($this->payment->country) && !empty($this->payment->country) && $this->payment->country != "empty" && $this->payment->country != "undefined") echo $this->payment->country;?>
					</td>
					<td valign="top" style="padding-top: 11px;">
						<strong style="color: #FFF;">CityBlast</strong><br />
						2 Regency Square<br />
						Toronto, Ontario<br />
						M1E 1N3<br />
						Canada
					</td>
				</tr>
				<tr>
					<td height="30"></td>
					<td></td>
				</tr>
			</table>

			<table width="580" border="0" cellspacing="0" cellpadding="0" class="datatable">
				<tr>
					<th width="240" align="left">Purchased Item</th>
					<th width="100">Quantity</th>
					<th width="100" align="right">Unit Price</th>
					<th width="100" align="right">Item Total</th>
				</tr>
				<tr>

					<td>CityBlast &#8211 Online Marketing</td>

					<td align="center">1</td>
					<td align="right">
						<? if(isset($this->payment) && strtoupper($this->payment->paypal_confirmation_number) == "FREE BLAST") : ?>
							Free Blast!
						<? else : ?>
							$<?=isset($this->payment) ? number_format($this->payment->amount, 2) : "";?>
						<? endif;?>
					</td>
					<td align="right">
						<? if(isset($this->payment) && strtoupper($this->payment->paypal_confirmation_number) == "FREE BLAST") : ?>
							$0.00
						<? else : ?>
							$<?=isset($this->payment) ? number_format($this->payment->amount, 2) : "";?>
						<? endif;?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right"><strong>Subtotal</strong></td>
					<td align="right">
						<? if(isset($this->payment) && strtoupper($this->payment->paypal_confirmation_number) == "FREE BLAST") : ?>
							$0.00
						<? else : ?>
							$<?=isset($this->payment) ? number_format($this->payment->amount, 2) : "";?>
						<? endif;?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right"><strong>Tax (<?php echo ($this->taxes*100); ?>%)</strong></td>
					<td align="right">
						$<?=isset($this->payment) ? number_format($this->payment->taxes , 2) : "";?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right"><strong>Total</strong></td>
					<td align="right">
						<strong>$<?=isset($this->payment) ? number_format( $this->payment->amount + $this->payment->taxes , 2) : "";?></strong>
					</td>
				</tr>
			</table>

		</div>

		<input type="button" name="preview_blast" id="preview_blast" class="uiButton right" value="CLICK TO PREVIEW YOUR BLAST">

	</div>


	<? /*************
	<h3>What next?</h3>



	<div class="box white80 clearfix" style="width: 190px; padding: 10px; margin-right: 15px; float: left;">

		<h5>Preview my listing</h5>
		<p>Do your friends know what you're up to?</p>
		<p>They should! Make sure to maximize the unbelievable power of City Blast by sharing Blasts with your friends!</p>
		<hr style="margin-top: 0px" />

		<div style="text-align: right">
			Share it on:
			<a href="/member/fbshare/<?=$this->order_number;?>">Facebook</a>

			<? if($this->member->twitter_id) : ?>
				 / <a href="/member/twshare/<?=$this->order_number;?>">Twitter</a>
			<? endif; ?>
		</div>

	</div>



	<div class="box white80 clearfix" style="width: 170px; margin-right: 15px; float: left;">

		<h4>Share This Blast</h4>
		<p>Do your friends know what you're up to?</p>
		<p>They should! Make sure to maximize the unbelievable power of City Blast by sharing Blasts with your friends!</p>
		<hr style="margin-top: 0px" />

		<div style="text-align: right">
			Share it on:
			<a href="/member/fbshare/<?=$this->order_number;?>">Facebook</a>

			<? if($this->member->twitter_id) : ?>
				 / <a href="/member/twshare/<?=$this->order_number;?>">Twitter</a>
			<? endif; ?>
		</div>

	</div>



	<div class="box white80 clearfix" style="width: 190px; padding: 10px; margin-right: 15px; float: left;">

		<h5>Join Our Network</h5>
		<p>Blasting is only half of the battle.  Sure, your listings are flying off of the shelves, but how about adding lots and lots of free leads?</p>
		<p>Take full advantage.</p>
		<hr style="margin-top: 0px" />

		<div style="text-align: right">
			<a href="/member/index">Join Now</a>
		</div>

	</div>

	<div class="box white80 clearfix" style="width: 190px; padding: 10px; float: left;">

		<h5>Blast Again</h5>
		<p>Have another Blast?  No problem, we'll reload the City Blast cannon for you.  Hit 'em with another one.</p>
		<p>Remember: Sold fast?  City Blast.</p>
		<hr style="margin-top: 0px" />

		<div style="text-align: right">
			<a href="/">Blast Again</a>
		</div>

	</div>

	<hr />

	

	*************/ ?>

</div>

<div class="grid_4">



	<!--<div style="padding-top: 1px" class="box clearfix">


		<h4>Share This With Your Client</h4>
		<p>You've done all the hard work. Why not reap the rewards? Share this listing with your client with the simple click of a button.</p>

		<a href="/listing/agentemail/<?=$this->listing->id;?>" class="uiButton right">Share by Email</a>

	</div> -->


	<div style="padding-top: 1px"  class="box clearfix">

		<h4>Join Our Network</h4>
		<p>Blasting is only half of the battle.  Sure, your listings are flying off of the shelves, but how about adding lots and lots of free leads?</p>

		<p>Take full advantage.</p>
	
		<a href="/member/index" class="uiButton right">Join Now</a>

	</div>


	<div class="box clearfix" style="padding-top: 1px">

		<h4>How did we do?</h4>
		<p>If you think we did a great job, why not <a href="/index/refer-an-agent">refer us to another agent</a> (we'll let you in on a secret, the bigger the network, the more people you'll reach with every Blast)?</p>

	</div>

	<div class="box clearfix" style="padding-top: 1px">

		<h4>Add us to your "safe" list</h4>
		<p>We'll be contacting you directly with confirmation of your Blast delivery. You might want to add us to your "safe" list so that you're sure to get our summary email.</p>

	</div>

</div>

<script>
$(document).ready(function()
{
	if($.browser.msie && $.browser.version < 9){
		$('.datatable tr:last-child td').css('border-bottom', 'none');
	}
		
	$('#preview_blast').click(function(event)
	{
		event.preventDefault();
		window.location = "/listing/view/<?=$this->listing->id;?>";
	});
});
</script> 