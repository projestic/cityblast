<div class="grid_12">

	<div class="box form clearfix">

		<div class="box_section">
			<h1>Hello <?=$this->member->first_name;?>, Welcome Back.</h1>
			<p>Turns out that we already have a credit card on file for you that ends in <strong>XXXX-
				<?
					if(isset($this->payment->last_four) && !empty($this->payment->last_four)) echo $this->payment->last_four;
					else echo "XXXX";
				?>
			</strong>.</p>
			<p style="margin-bottom: 40px;">If you'd like to Blast again using the card on file, just click <a href='/blast/payment' id='blast_again'>Blast Again</a>!</p>
		</div>

		<? if (isset($this->errors) && !empty($this->errors)): ?>

			<ul class="uiForm clearfix" style="margin-top: -20px;">
				<li class="uiFormRowHeader" style="color: #cc0000;">Oh Dear, Something Went Wrong!</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Error(s)</li>
						<li class="field">
							<ul>
								<?
									if(false === is_array($this->errors))
										$this->errors = array($this->errors);

									foreach($this->errors as $error):
										if(isset($error) && !empty($error)) echo "<li>" . $error . "</li>";
									endforeach;
								?>
							</ul>
						</li>
					</ul>
				</li>
			</ul>

		<? endif; ?>

		<form action="/blast/referencetransactionpayment/" method="post" id="reference_trans">
			<input type="hidden" name="pid" value="<?php echo $this->payment->id;?>"/>
			<ul class="uiForm clearfix">
				<li class="uiFormRowHeader">Your Billing Details</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname cc">Last Four Digits</li>
						<li class="field cc">
						<?
							if(isset($this->payment->last_four) && !empty($this->payment->last_four)) echo $this->payment->last_four;
							else echo "XXXX";
						?>
						</li>
					</ul>
				</li>

				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname cc">Last Used</li>
						<li class="field cc">
						    <?php echo $this->payment->created_at->format("D, d M Y h:i a");?>
						</li>
					</ul>
				</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname cc">First Name</li>
						<li class="field cc">
						    <?php echo ucfirst(strtolower($this->payment->first_name));?>
						</li>
					</ul>
				</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname cc">Last Name</li>
						<li class="field cc">
						    <?php echo ucfirst(strtolower($this->payment->last_name));?>
						</li>
					</ul>
				</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname cc">Expire Date</li>
						<li class="field cc">
						    <?php echo substr($this->payment->cc_expire_date, 0, 2) . "/" . substr($this->payment->cc_expire_date, -2, 2); ?>
						</li>
					</ul>
				</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="buttons">
							<input type="submit" class="uiButton right" value="Blast Again!">
							<span style="float: left; line-height: 40px; font-size: 12px;">I'd like to <a href="/blast/login/newcard">use another credit card</a></span>
						</li>
					</ul>
				</li>
			</ul>

		</form>
	</div>
</div>

<script>
$(document).ready(function()
{
	$('#blast_again').click(function(event)
	{
		event.preventDefault();
		$('reference_trans').submit();
	});
});
</script>

<!-- Google Code for CityBlast /index/how-blasting-works Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 956260748;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "IL1qCITeowMQjMP9xwM";
var google_conversion_value = 0;
if (49) {
  google_conversion_value = 49;
}
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/956260748/?value=49&amp;label=IL1qCITeowMQjMP9xwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>