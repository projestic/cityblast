<?php

if ($this->member && $this->member->network_app_facebook) {
	$network_app_facebook = $this->member->network_app_facebook;
} else {
	$network_app_facebook = Zend_Registry::get('networkAppFacebook');
}

?>
<!--[if lt IE 9]>
	<style type="text/css">
		.uiForm .upsell{
			margin-bottom: -20px !important;
		}
	</style>
<![endif]-->

<!-- LOAD UPLOADIFY -->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>

<?/*
<link type="text/css" rel="stylesheet" href="/css/forms.css" />
<link type="text/css" rel="stylesheet" href="/css/grid.css" />
<link type="text/css" rel="stylesheet" href="/css/slide.css" />
*/ ?>


<script type="text/javascript">

var no_flash = false;

$(document).ready(function() {

	$('#file_upload').uploadify({
		'uploader'  : '/js/uploadify/uploadify.swf',
		'script'    : '/blast/uploadimage',
		'cancelImg' : '/js/uploadify/cancel.png',
	    	'folder'    : '/images/',
		'auto'      : true,
		'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
		'fileDesc'  : 'Image Files',
		'buttonImg' : '/images/browse_button.png',
		'removeCompleted' : false,
		'height' : 30,
		'width' : 106,
		'name': 'fileUploaded',
		'onComplete': function(event, ID, fileObj, response, data)
		{
			/*alert('RESPONSE: ' +response);*/
			$('#fileUploaded').val(response);

			if ($('#imageerror'))
			{
				$('#imageerror').remove();
			}

			//$("#file_uploadQueue").hide();
			$("#file_uploadStatus").show();
			$("#file_uploadStatus").html("<span style='display: block; text-align: center;'>Upload Complete</span>");
			$('#main_photo').attr('src', response).show();
			$('#facebook_postpic').attr('src', response);
			$('#main_photo_progress').css('display', 'none');

		},

  		'onError'     : function (event,ID,fileObj,errorObj) {
      		alert(errorObj.type + ' Error: ' + errorObj.info);
    		},

		'onProgress'  : function(event,ID,fileObj,data)
		{
			$("#file_uploadStatus").html("<span style='display: block; text-align: center;'>Uploaded " + data.percentage + "% &hellip;</span>");
			$('#main_photo').hide();
			$('#main_photo_progress').css('display', 'block');
			$('#main_photo_progress').css('height', Math.round(data.percentage/100 * 64) + 'px');
			return false;
		}

	});

	/*
	Thumbnails disabled - Kfoster 2014-01-24
	$('#thumb_upload').uploadify({
		'uploader'  : '/js/uploadify/uploadify.swf',
		'script'    : '/blast/uploadimage',
		'cancelImg' : '/js/uploadify/cancel.png',
	    	'folder'    : '/images/',
		'auto'      : true,
		'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
		'fileDesc'  : 'Image Files',
		'buttonImg' : '/images/browse_button.png',
		'removeCompleted' : false,
		'height' : 30,
		'width' : 106,
		'name': 'thumbUploaded',
		'onComplete': function(event, ID, fileObj, response, data)
		{
			$('#thumbUploaded').val(response);

			//$("#file_uploadQueue").hide();
			$("#thumb_uploadStatus").attr('src', '').show();
			$("#thumb_uploadStatus").html("<span style='display: block; text-align: center;'>Upload Complete</span>");
			$('#thumb_photo').attr('src', response).show();

			$('#thumb_photo_progress').css('display', 'none');

            	if ($('#thumbimageerror'))
            	{
                	$('#thumbimageerror').hide();
            	}
		},

  		'onError'     : function (event,ID,fileObj,errorObj) {
      		alert(errorObj.type + ' Error: ' + errorObj.info);
    		},

		'onProgress'  : function(event,ID,fileObj,data)
		{
			$('#thumb_photo').attr('src', '').hide();
			$("#thumb_uploadStatus").html("<span style='display: block; text-align: center;'>Uploaded " + data.percentage + "% &hellip;</span>");
			$('#thumb_photo_progress').css('display', 'block');
			$('#thumb_photo_progress').css('height', Math.round(data.percentage/100 * 64) + 'px');
			return false;
		}

	});
	*/

	if(swfobject.hasFlashPlayerVersion('9.0.24') == false)
	{
		$('#extraForm').remove();
		$('#create_listing_heading').html('<h1 style="margin: 0 0 14px;">Oops!</h1><p>We\'re sorry, but listings can only currently be added through a flash enabled web browser on a computer. Mobile support is not yet supported.</p>');
		$('#create_listing_heading').removeClass('box_section');
		$('#create_listing_heading').closest('.box').removeClass('form').css('padding', 50);
	}

});


</script>

		

<div class="contentBox">

	<div class="contentBoxTitle">
		<h1>Create Your Listing.</h1>
	</div>
	
	

	<p>Simply enter your property information below to create a listing on <a href="//www.<?=APP_URL;?>"><?=COMPANY_NAME;?></a>.</p>
	
	<p>You will also have the option to publish your completed listing to thousands of potential buyers through <a href="//www.facebook.com">Facebook</a>, <a href="//www.twitter.com">Twitter</a> and <a href="//www.linkedin">LinkedIn</a>.</p>


	<form action="/blast/preview" method="POST" id="extraForm" name="extraForm" enctype="multipart/form-data" style="margin-top: 0px;" class="clearfix">
	    <input type="hidden" id="btnclicked" value=""/>
		<input type="hidden" name="blast_type" id="blast_type" value="<?php echo Listing::TYPE_LISTING;?>" />

	
		<?/***
		
		
		COMING SOON
		
		
		<div class="promoBox clearfix" style="margin-top: 4px;">
			
			<h5>FREE: Submit My Listing to the CityBlast Super Showcase!</h5>
			
			<p>Your listing may be featured on <b>three times</b> the Facebook, Twitter and LinkedIn accounts by checking the box below, and could receive extra views from interested national and local buyers.</a></p>
			
						
			<p style="margin-top: 10px;"><input type="checkbox" name="showcase" id="showcase" value="1" checked="checked"> Yes, add this listing to the Super Showcase (FREE)</p>
						
		</div>
		**/ ?>



	
	<div class="clearfix"></div>





<div class="search_bar clearfix"><h4 class="search_title">Listing Details</h4></div>
	

		<ul class="uiForm full">
		
		
			<li class="clearfix">
														
				<ul>
					<li class="fieldname cc"><span style="font-weight: bold; color: #990000">*</span>Publish In
					<span class="fieldhelp">If Blasting is not yet available for your city, it will not appear on the list.</span>
				</li>
				
				<li class="field cc clearfix">
					<select name="city_id" id="city_id" class="uiSelect" style="width: 100%;" tabindex="1">
						<option value="">Select A City</option>
					    <?php foreach($this->blast_cities as $city): ?>
							<option value="<?php echo $city->id;?>"
								<?php

								if (!empty($this->listing->city_id) && $this->listing->city_id == $city->id) echo ' selected="true"';
								elseif (!empty($this->select_city) && $this->select_city == $city->id) echo ' selected="true"';

								?>><?php echo $city->name;?>, <?=$city->state;?></option>
					    <?php endforeach; ?>
					</select>
				</li>
			</ul>
			

			
			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname" style="position: relative;">
						<span style="font-weight: bold; color: #990000;">*</span>Description
						<span class="fieldhelp">Must be 140-400 characters.</span>



						<div id="char_counter" class="pull-right">
							<span>0 Characters</span><div class="triangle shadow right" style="top: 9px; border-width: 11px;  border-left-color: #ccc!important;"></div>
							<div id="count_arrow" class="triangle right" style="top: 10px; border-width: 10px; right: -20px; border-left-color: #fff;"></div>
						</div>							
						<div class="clearfix"></div>
													
						
					</li>
					<li class="field">
						
						<? 	
						
							echo '<textarea class="uiTextarea" name="message" id="message" style="height: 95px !important; width: 98%;';
							if (empty($this->listing->message)) echo "color: #b7b7b7; font-style: italic;";
							echo '" tabindex="2">';
							if (empty($this->listing->message)) echo '[Example]: Looking for luxury? This 4-bedroom, 3-bathroom Downtown home is fully updated with hardwood throughout, and a stainless steel and granite kitchen. Garage parks 2 cars, and features a spacious landscaped backyard. A short walk to high-end retail shops and dog park. A must see!';
							else echo trim($this->listing->message);
							echo '</textarea>';
						
							
						?>

					</li>
				</ul>
			</li>
			
		</ul>
		
	
	<div class="clearfix">
		
		<div style="width: 50%" class="pull-left">

<div class="search_bar clearfix" style="margin-top: 20px;"><h4 class="search_title">Required</h4></div>

			<ul class="uiForm full">
				
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname"><span style="font-weight: bold; color: #990000">*</span>Asking Price</li>
						<li class="field">
							<span style="float: left; line-height: 30px; margin-top: 2px; width: 24px; text-align: center; font-weight: bold; font-size: 16px; position: relative; z-index: 3;">$</span><input type="text" name="price" id="price" value="<?php if(isset($this->listing->price)) echo $this->listing->price ?>" class="uiInput validateCurrency" style="width: 157px; padding-left: 20px!important; margin-left: -24px; position: relative; z-index: 1;" tabindex="3" />
							<div id='price_error' style='display: none; clear: left;'>
								<div class='error' style="padding-left: 4px;color: #CC0000"><strong style='color: #CC0000;'>*</strong>Please enter the exact list price. Do <strong style='color: #CC0000;'>not</strong> use commas, dollar signs or any other special characters</div><div class='clearfix'>&nbsp;</div>
							</div>
						</li>
					</ul>
				</li>
				
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname"><span style="font-weight: bold; color: #990000">*</span>Number of Bedrooms</li>
						<li class="field" style="width:130px; float: left;">
	
							<select name="number_of_bedrooms" id="number_of_bedrooms" class="uiSelect validateNotempty" style="width: 140px" tabindex="6">
								<option value="">Please select...</option>
								<?php
									$bedroom_options	= array("None", 1, 2, 3, 4, 5);
									$this->listing->number_of_bedrooms	= ($this->listing->number_of_bedrooms > count($bedroom_options)) ? count($bedroom_options) : $this->listing->number_of_bedrooms;
								?>
								<?php foreach ($bedroom_options as $key => $value): ?>
								<option value="<?php echo $key ?>" <?php echo (isset($this->listing->number_of_bedrooms) && $this->listing->number_of_bedrooms == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
								<?php endforeach ?>
							</select>
							
						</li>
					
						<li class="field" style="width: 25px; padding: 6px 0px 0px 2px; float: left;"><i class="icon-plus-sign"></i></li>
						<li class="field" style="width:175px; padding-left: 0px; float: left;">
	
							<select name="number_of_extra_bedrooms" id="number_of_extra_bedrooms" class="uiSelect validateNotempty" style="width: 140px" tabindex="6">
								<?php
									$bedroom_options	= array("None", 1, 2, 3, 4, 5);
									$this->listing->number_of_extra_bedrooms	= ($this->listing->number_of_extra_bedrooms > count($bedroom_options)) ? count($bedroom_options) : $this->listing->number_of_extra_bedrooms;
								?>
								<?php foreach ($bedroom_options as $key => $value): ?>
								<option value="<?php echo $key ?>" <?php echo (isset($this->listing->number_of_extra_bedrooms) && $this->listing->number_of_extra_bedrooms == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
								<?php endforeach ?>
							</select>
	
						</li>
					</ul>
				</li>

				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname"><span style="font-weight: bold; color: #990000">*</span>Property Type</li>
						<li class="field">
	
							<select name="property_type_id" id="property_type_id" class="uiSelect validateNotZero" style="width: 190px" tabindex="5">
								<option value="0">Please select...</option>
								<option value="1" <?php echo (isset($this->listing->property_type_id) && $this->listing->property_type_id == 1) ? 'selected="true"' : '' ?>>Detached</option>
								<option value="2" <?php echo (isset($this->listing->property_type_id) && $this->listing->property_type_id == 2) ? 'selected="true"' : '' ?>>Semi-detached</option>
								<option value="3" <?php echo (isset($this->listing->property_type_id) && $this->listing->property_type_id == 3) ? 'selected="true"' : '' ?>>Freehold Townhouse</option>
								<option value="4" <?php echo (isset($this->listing->property_type_id) && $this->listing->property_type_id == 4) ? 'selected="true"' : '' ?>>Condo Townhouse</option>
								<option value="5" <?php echo (isset($this->listing->property_type_id) && $this->listing->property_type_id == 5) ? 'selected="true"' : '' ?>>Condo Apartment</option>
								<option value="6" <?php echo (isset($this->listing->property_type_id) && $this->listing->property_type_id == 6) ? 'selected="true"' : '' ?>>Coop/Common Elements</option>
								<option value="7" <?php echo (isset($this->listing->property_type_id) && $this->listing->property_type_id == 7) ? 'selected="true"' : '' ?>>Attached/Townhouse</option>
							</select>
	
						</li>
					</ul>
				</li>

				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname"><span style="font-weight: bold; color: #990000">*</span>Number of Bathrooms</li>
						<li class="field">
	
							<select name="number_of_bathrooms" id="number_of_bathrooms"  class="uiSelect validateNotempty" style="width: 190px" tabindex="7">
								<option value="">Please select...</option>
								<?php
									$bathroom_options	= array("None", 1, 2, 3, 4, "5+");
									$this->listing->number_of_bathrooms	= ($this->listing->number_of_bathrooms > count($bathroom_options)) ? count($bathroom_options) : $this->listing->number_of_bathrooms;
								?>
								<?php foreach ($bathroom_options as $key => $value): ?>
								<option value="<?php echo $key ?>" <?php echo (isset($this->listing->number_of_bathrooms) && $this->listing->number_of_bathrooms == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
								<?php endforeach ?>
							</select>
	
						</li>
					</ul>
				</li>
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname"><span style="font-weight: bold; color: #990000">*</span>Number of Parking</li>
						<li class="field">
	
							<select name="number_of_parking" id="number_of_parking" class="uiSelect validateNotempty" style="width: 190px" tabindex="8">
								<option value="">Please select...</option>
								<?php
									$parking_options	= array("None", 1, 2, "3+");
									$this->listing->number_of_parking	= ($this->listing->number_of_parking > count($parking_options)) ? count($parking_options) : $this->listing->number_of_parking;
								?>
								<?php foreach ($parking_options as $key => $value): ?>
								<option value="<?php echo $key ?>" <?php echo (isset($this->listing->number_of_parking) && $this->listing->number_of_parking == $key) ? 'selected="true"' : '' ?>><?php echo $value ?></option>
								<?php endforeach ?>
	
							</select>
	
						</li>
					</ul>
				</li>	
					
			
			</ul>		
				
		</div>


		<div class="pull-right" style="width: 49%">

<div class="search_bar clearfix" style="margin-top: 20px;"><h4 class="search_title">Optional</h4></div>

			<ul class="uiForm full">
							
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">MLS Number</li>
						<li class="field">
							<input type="text" name="mls" id="mls" value="<?php if(isset($this->listing->mls)) echo $this->listing->mls; ?>" class="uiInput" style="width: 175px"  tabindex="4" />
						</li>
					</ul>
				</li> 
				


				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Maintenance Fee <span style="font-size: 12px; font-weight: 300;">(per month)</span></li>
						<li class="field">
							<span style="float: left; line-height: 30px; margin-top: 2px; width: 24px; text-align: center; font-weight: bold; font-size: 16px; position: relative; z-index: 3;">$</span><input type="text" name="maintenance_fee" id="maintenance_fee" tabindex="9" value="<?php if(isset($this->listing->price)) echo strstr($this->listing->maintenance_fee.'', '.') ? $this->listing->maintenance_fee : $this->listing->maintenance_fee.'.00' ?>" class="uiInput validateMaintenance" style="width: 157px; padding-left: 20px!important; margin-left: -24px; position: relative; z-index: 1;" />
							
							<div id='maintenance_fee_error' style='display: none;'>
								<div class='error' style="padding-left: 4px;color: #CC0000"><strong style='color: #CC0000;'>*</strong>Please enter the exact maintenance fee. Do <strong style='color: #CC0000;'>not</strong> use commas, dollar signs or any other special characters</div><div class='clearfix'>&nbsp;</div>
							</div>
						</li>
					</ul>
				</li>
	
	
				<li class="uiFormRow clearfix">
					<ul>
						<li class="fieldname">Approximate Square Footage</li>
						<li class="field">
							<input type="text" name="square_footage" id="square_footage" tabindex="10" value="<?php if(isset($this->listing->square_footage)) echo strstr($this->listing->square_footage.'', '.') ? $this->listing->square_footage : $this->listing->square_footage; ?>" class="uiInput" style="width: 175px" />
							
						</li>
					</ul>
				</li>
	
	
			</ul>
			
		</div>
	</div>
		
		

<div class="search_bar clearfix" style="margin-top: 20px;"><h4 class="search_title">Property Photos</h4></div>

		<ul class="uiForm full clearfix">


			<li class="uiFormRow clearfix">
				<ul>

					<li class="field">
						<div class="clearfix">
							<div style="float: left; width: 106px; margin-right: 20px;">
								<h6 style="margin-top: 0;">Main Photo<span style="font-weight: bold; color: #990000">*</span></h6>
								<div class="photo_upload_thumb">
									<div id="main_photo_progress" class="progress"></div>
									<?php if (isset($this->listing->image)): ?>
									<img src="<?php echo CDN_URL . $this->listing->image ?>" tabindex="11" width="96" id="main_photo" />
									<?php else: ?>
									<img src="/images/blank.gif" tabindex="12" width="96" id="main_photo" />
									<?php endif ?>
								</div>
								<input id="file_upload" name="file_upload" type="file" width="88" height="104" />
								<div id="file_uploadStatus" class="uploadstatus"></div>
								<input type="hidden" name="fileUploaded" id="fileUploaded" <?php echo empty($this->listing->image) ? '' : 'value="'. CDN_URL . $this->listing->image .'"' ?> />
							</div>
				
							<!--
							Thumbnails disabled - Kfoster 2014-01-24
							<div style="float: left; width: 106px; margin-right: 20px;">
								<h6 style="margin-top: 0;">Thumbnail<span style="font-weight: bold; color: #990000">*</span></h6>
								<div class="photo_upload_thumb">
									<div id="thumb_photo_progress" class="progress"></div>
									<?php if (isset($this->listing->thumbnail)): ?>
									<img src="<?php echo $this->listing->thumbnail ?>" width="96" id="thumb_photo" />
									<?php else: ?>
									<img src="/images/blank.gif" width="96" id="thumb_photo" />
									<?php endif ?>
								</div>
								<input id="thumb_upload" name="thumb_upload" type="file" width="88" height="104" />
								<div id="thumb_uploadStatus" class="uploadstatus"></div>
								<input type="hidden" name="thumbUploaded" id="thumbUploaded" <?php echo empty($this->listing->thumbnail) ? '' : 'value="'. $this->listing->thumbnail .'"' ?> />
							</div>
							-->





						<div id="extraImages">								
							<? 
							$i = 1;
							$j = 1;
							if (!empty($this->listing->images))
							{
								foreach ($this->listing->images as $img) {
							?>
								<div style="float: left; width: 106px; margin-right: 20px;">
									<h6 style="margin-top: 0;">Extra Photo <?=$j;?></h6>
									<div class="photo_upload_thumb">
										<div id="extra_photo_<?=$i;?>_progress" class="progress"></div>
										<img src="<?php echo $img->image ?>" width="96" id="extra_photo_<?=$i;?>" />
									</div>
									<input id="file_upload<?=$i;?>" name="file_upload<?=$i;?>" type="file" width="88" height="104" />
									<div id="file_upload<?=$i;?>Status" class="uploadstatus"></div>
									<input type="hidden" name="fileUploaded<?=$i;?>" id="fileUploaded<?=$i;?>" <?php echo empty($img->image) ? '' : 'value="'. $img->image .'"' ?> />

									<script type="text/javascript">
										$(document).ready(function() {
											$('#file_upload<?=$i;?>').uploadify({
												'uploader'  : '/js/uploadify/uploadify.swf',
												'script'    : '/blast/uploadimage',
												'cancelImg' : '/js/uploadify/cancel.png',
												'folder'    : '/images/',
												'auto'      : true,
												'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp',
												'fileDesc'  : 'Image Files',
												'buttonImg' : '/images/browse_button.png',
												'removeCompleted' : false,
												'height' : 30,
												'width' : 106,
												'name': 'fileUploaded<?=$i;?>',
												'onComplete': function(event, ID, fileObj, response, data)
												{
													$('#fileUploaded<?=$i;?>').val(response);

													//$("#file_uploadQueue").hide();
													$("#file_upload<?=$i;?>Status").show();
													$("#file_upload<?=$i;?>Status").html("<span style='display: block; text-align: center;'>Upload Complete</span>");
													$('#extra_photo_<?=$i;?>').attr('src', response).show();

													$('#extra_photo_<?=$i;?>_progress').css('display', 'none');
												},

												'onError'     : function (event,ID,fileObj,errorObj) {
												alert(errorObj.type + ' Error: ' + errorObj.info);
												},

												'onProgress'  : function(event,ID,fileObj,data)
												{
													$('#extra_photo_<?=$i;?>').attr('src', '').hide();
													$("#file_upload<?=$i;?>Status").html("<span style='display: block; text-align: center;'>Uploaded " + data.percentage + "% &hellip;</span>");
													$('#extra_photo_<?=$i;?>_progress').css('display', 'block');
													$('#extra_photo_<?=$i;?>_progress').css('height', Math.round(data.percentage/100 * 64) + 'px');
													return false;
												}

											});
										});
									</script>
								</div>
							<? 
								$i++;
								$j++;
								
								} 
							}
							?>
						</div>
						<div style="margin-top: 15px;">
							<div style="float: right;">
								<a class="uiButton small" id="add_img">+Add Another Photo</a>
							</div>
						</div>




						</div>
					</li>
				</ul>
			</li>

			<li class="uiFormRow clearfix">
				<ul>

					<li class="field">
						
						

					</li>
				</ul>
			</li>
		</ul>


		<script type="text/javascript">
		$(document).ready(function() {

			var counter = <?=$i;?> - 1;

			$("#add_img").click(function(){
				
				counter = counter + 1;
				
				var html = "<div style='float: left; width: 106px; margin-right: 20px;'> " + 
					"<h6 style='margin-top: 0;'>Extra Photo "+ counter +"</h6>" +
					"<div class='photo_upload_thumb'>" +
					"	<div id='extra_photo_" + counter + "_progress' class='progress'></div>" +
					"	<img src='/images/blank.gif' width='96' id='extra_photo_" + counter + "' />" +
					"</div>" +
					"<input id='file_upload" + counter + "' name='file_upload" + counter + "' type='file' width='88' height='104' />" +
					"<div id='file_upload" + counter + "Status' class='uploadstatus'></div>" +
					"<input type='hidden' name='fileUploaded"+ counter +"' id='fileUploaded"+ counter +"' />"
				"</div>";
			
				$("#extraImages").append(html);
				
				var uploadScript = "$('#file_upload" + counter + "').uploadify({" +
								"'uploader'  : '/js/uploadify/uploadify.swf'," +
								"'script'    : '/blast/uploadimage'," +
								"'cancelImg' : '/js/uploadify/cancel.png'," +
								"'folder'    : '/images/'," +
								"'auto'      : true," +
								"'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png;*.bmp'," +
								"'fileDesc'  : 'Image Files'," +
								"'buttonImg' : '/images/browse_button.png'," +
								"'removeCompleted' : false," +
								"'height' : 30," +
								"'width' : 106," +
								"'name': 'fileUploaded" + counter + "'," +
								"'onComplete': function(event, ID, fileObj, response, data)" +
								"{" +
								"	$('#fileUploaded" + counter + "').val(response);" +
								"	$('#file_upload" + counter + "Status').show();" +
								"	$('#file_upload" + counter + "Status').html('<span style=\"display: block; text-align: center;\">Upload Complete</span>');" +
								"	$('#extra_photo_" + counter + "').attr('src', response);" +
								"	$('#extra_photo_" + counter + "_progress').css('display', 'none');" +
								"	if ($('#imageerror'))" +
								"	{" +
								"		$('#imageerror').hide();" +
								"	}" +
								"}," +
								"'onError'     : function (event,ID,fileObj,errorObj) {" +
								"alert(errorObj.type + ' Error: ' + errorObj.info);" +
								"}," +
								"'onProgress'  : function(event,ID,fileObj,data)" +
								"{" +
									"$('#file_upload" + counter + "Status').html('<span style=\"display: block; text-align: center;\">Uploaded ' + data.percentage + '% &hellip;</span>');" +
									"$('#extra_photo_" + counter + "_progress').css('display', 'block');" +
									"$('#extra_photo_" + counter + "_progress').css('height', Math.round(data.percentage/100 * 64) + 'px');" +
									"return false;" +
								"}" +
							"});";
				
				var script   = document.createElement("script");
				script.type  = "text/javascript";
				script.text  = uploadScript;
				document.body.appendChild(script);

			});

			$("#add_img").click();
			
		});
		</script>


<div class="search_bar clearfix" style="margin-top: 20px;"><h4 class="search_title">Property Address</h4></div>

		
		<ul class="uiForm full clearfix">

			<?php
			/**
			 * extract diff fields of street
			 */
			if (!empty($this->listing->street)) {
				$street_parts		= explode(" ", $this->listing->street, 2);
				$street_number		= $street_parts[0];
				$street_addr_parts	= explode(' Unit ', $street_parts[1], 2);
				$street_address		= $street_addr_parts[0];
				if (!empty($street_addr_parts[1])) {
					$street_sfx_parts	= explode(' ', $street_addr_parts[1], 2);
					$appartment			= $street_sfx_parts[0];
					if (!empty($street_sfx_parts[1])) {
						# concate the sfx part to $street_addr
						$street_address	.= " ". $street_sfx_parts[1];
					}
				}
			}
			?>

			<li class="uiFormRow clearfix" style="float: left;">
				<ul>
					<li class="fieldname">Street Number<span style="font-weight: bold; color: #990000">*</span></li>
					<li class="field">
						<input type="text" name="street_number" id="street_number" tabindex="13" value="<?php echo isset($street_number) ? $street_number : '' ?>" class="uiInput validateNotempty" style="width: 92px" />
					</li>
				</ul>
			</li>

			<li class="uiFormRow clearfix" style="float: left;">
				<ul>
					<li class="fieldname" style="padding-left: 0px; padding-right: 0px;">Street Address<span style="font-weight: bold; color: #990000">*</span></li>
					<li class="field" style="padding-left: 0px; padding-right: 0px;">
						<input type="text" name="street" id="street" tabindex="14" value="<?php echo isset($street_address) ? $street_address : '' ?>" class="uiInput validateNotempty" style="width: 490px" />
					</li>
				</ul>
			</li>

			<li class="uiFormRow clearfix" style="float: left;">
				<ul>
					<li class="fieldname">Apt/Suite</li>
					<li class="field">
						<input type="text" name="appartment" id="appartment" tabindex="15" value="<?php echo isset($this->listing->appartment)? $this->listing->appartment : '' ?>" class="uiInput" style="width: 92px" />
					</li>
				</ul>
			</li>

			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname">City<span style="font-weight: bold; color: #990000">*</span></li>
					<li class="field">
						<input type="text" name="city" id="cityname" tabindex="16" value="<?php echo isset($this->listing->city) ? $this->listing->city : '' ?>" class="uiInput validateNotempty" style="width: 590px" />
					</li>
				</ul>
			</li>

			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname">Postal Code/Zip Code<span style="font-weight: bold; color: #990000">*</span></li>
					<li class="field">
						<input type="text" name="postal" id="postal" tabindex="17" value="<?php echo isset($this->listing->postal_code) ? $this->listing->postal_code : '' ?>" class="uiInput validateNotempty" style="width: 92px" />
					</li>
				</ul>
			</li>

			<? /*
			<li class="uiFormRow last clearfix">
				<ul>
					<li class="buttons">
						<input type="submit" value="Send" class="uiButton right" id="send_id" />
						<input type="reset" value="Reset" class="uiButton right cancel" />
					</li>
				</ul>
			</li> */ ?>
		</ul>

<div class="search_bar clearfix" style="margin-top: 20px;"><h4 class="search_title">Listing Agent</h4></div>

		<ul class="uiForm first clearfix">

			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname">Brokerage<span style="font-weight: bold; color: #990000">*</span></li>
					<li class="field">
						<input type="text" name="brokerage" id="brokerage" tabindex="18" value="<? if(isset($this->member->brokerage)) echo $this->member->brokerage; elseif(isset($this->listing) && $this->listing->broker_of_record) echo $this->listing->broker_of_record;?>" class="uiInput validateNotempty <?=($this->fields && in_array("brokerage", $this->fields)?" error":"")?>" style="width: 590px" />
					</li>
				</ul>
			</li>

			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname">Broker Address<span style="font-weight: bold; color: #990000">*</span></li>
					<li class="field">
						<input type="text" name="broker_address" id="broker_address" tabindex="19" value="<? if(isset($this->member->broker_address)) echo $this->member->broker_address;?>" class="uiInput validateNotempty <?=($this->fields && in_array("broker_address", $this->fields)?" error":"")?>" style="width: 590px" />
					</li>
				</ul>
			</li>


			<li class="uiFormRow clearfix">
				<ul>
					<li class="fieldname">Your Contact Phone Number</li>
					<li class="field">
						<input type="text" name="phone" id="phone" value="<? if(isset($this->member->phone)) echo $this->member->phone; elseif(isset($this->listing) && $this->listing->list_agent_phone) echo $this->listing->list_agent_phone;?>" class="uiInput" style="width: 590px" />
					</li>
				</ul>
			</li>

			<li class="uiFormRow clearfix">
				<ul>
					<li class="field">
						
						<div class="termsBox" style="font-weight: normal; text-align: left; border: 1px solid #C9C9C9; float: left;"><input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" checked> I have read and agree to the <a href="/index/terms" target="_new">Terms and Conditions</a> and I have the authority to advertise this property</div>
						
					</li>
				</ul>
			</li>


			
		</ul>


		<div id='terms_error' style='display: none;'>
			<div class='error' style="padding-left: 4px; color: #CC0000"><strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> agree to the terms and conditions</div><div class='clearfix'>&nbsp;</div>
		</div>
				
		<hr>
		
		<div>
			<div>

				<? if(empty($this->listing->id)) : ?>
					<a href="#" name="list_button" id="list_button" class="right" style="padding: 0;">No Thanks, Just List It</a>
				<? endif; ?>
							
			</div>
	
			<div class="pull-right">
							
				<input type="submit" name="blast_button" id="blast_button" class="btn btn-primary btn-large pull-right" style="width: 195px;"  value="Blast Now" />
	
	
				<div class="clearfix"></div>
							
	
			</div>
		</div>			


	</form>


</div>



<div style="display: none;">
	
	<div id="help_video_popup" style="width: 640px; height: 540px;">
	    	   
	    	<div style="width: 640px; height: 480px;">   		
			<iframe width="640" height="480" src="//www.youtube.com/embed/nOTl5Svnmy0?rel=0" frameborder="0" allowfullscreen></iframe>	
		</div>
		
		<div style="text-align: right; margin-top: 5px; border-top: 1px solid #b7b7b7; padding-top: 5px"><input type="checkbox" value="1" id="show_blast_help" name="show_blast_help">&nbsp;Please, don't show this message again.</div>
		
	</div>

</div>


<?php if(isset($this->member->show_blast_video) && $this->member->show_blast_video): ?>

<script type="text/javascript">

	$(document).ready(function() 
	{
		$.colorbox({width:"720px", height: "640px", inline:true, href:"#help_video_popup"});

		$('#help_video_btn').click(function(event)
		{
			event.preventDefault();
			$.colorbox({width:"720px", height: "640px", inline:true, href:"#help_video_popup"});
		});

		$("#show_blast_help").change(function(){
			$.ajax({
			  url: "/member/hide_blast_video"
			}).done(function() { 
				jQuery('#cboxClose').click();
			});
		});
	});

</script>

<?php endif; ?>



<style type="text/css">
	.upsell .title{
		margin-top: 0;
		color: white;
		float: left;
		font-size: 22px;
	}

	.upsell .title span,
	.upsell .big_title span{
		color: #ea0000;
	}

	.upsell .big_title{
		margin: 0;
		float: left;
		font-size: 65px;
		font-weight: normal;
		color: white;
		font-style: italic;
		margin-left: 40px;
		margin-top: -5px;
	}

	.upsell .box_title{
		font-style: italic;
		font-size: 15px;
		color: #ea0000;
		font-weight: bold;
		float: left;
	}

	.upsell .hr{
		background-color: #525252;
		width: 365px;
		height: 3px;
		float: left;
		margin-top: 9px;
		margin-left: 10px;
	}

	.upsell p.notes{
		margin:0;
		margin-top: 3px;
		padding-right: 10px;
		font-size: 12px;
		color: #FFF;
	}

</style>

<!--[if IE 8]>
	<style type="text/css">
		.blast_teaser{
			padding-bottom: 20px;
		}
</style>
<![endif]-->

<script language="javascript" src="/js/jquery.autogrowtextarea.js"></script>

<script type="text/javascript">

function setPreviewMsg(id){
	var message = $("#"+id).val();
	$('#facebook_message').val((message?message:'Awaiting message...'));
	$("#facebook_message").trigger('blur');
    var maxLength = 400;
	if ( $("#"+id).val().length > maxLength ) {
	    setTimeout(function () {
	    	$("#"+id).val($("#"+id).val().substr(0, maxLength));
                countCharacter($("#"+id).val());
            }, 250);
	} 
	else{
		countCharacter(message);
        }
	//$('#facebook_message').focus();

	var imageSrc = $('#main_photo').attr('src');
	if(imageSrc!='/images/listings/2012/08/livingdining_1345085091_1345135205_1346002523.jpg')
		$('#facebook_postpic').attr('src', imageSrc);
}

function setPreviewPriceDescription(){
	if ($('#appartment').val()) {
		var url_text = $('#street_number').val() + ' ' + $('#street').val() + ' apt ' + $('#appartment').val() + ' $' + $('#price').val() + ' : <?=COMPANY_NAME;?>';
	} else {
		var url_text = $('#street_number').val() + ' ' + $('#street').val() + ' $' + $('#price').val() + ' : <?=COMPANY_NAME;?>';
	}
	var description = 'List Price: $' + $('#price').val() + ' ' + $('#street_number').val() + ' ' + $('#street').val();
	$('#facebook_linktext').text((url_text?url_text:'Awaiting link text...'));
	$('#facebook_description').text((description?description:'Awaiting description...'));
}

function setDescription(id){
	var description = $("#"+id).val();
	$('#facebook_description').text((description?description:'Awaiting description...'));
}

function setPreviewUrl(id){
	var url = $("#"+id).val();
	if(url){
		var hasProtocol = (url.indexOf('://')>-1)?true:false;
		if(hasProtocol){
			var parts = url.split('://');
			var baseurl = parts[1].split('/');
		}else{
			baseurl = url.split('/');
		}
		$('#facebook_baseurl').text((baseurl?baseurl[0]:'< YOUR SITE URL >'));
	}
}

$(document).ready(function() {
	<?php 
	if(isset($this->listing->id) && $this->listing->id!=''){
	?>
		setPreviewMsg('message');
		//setDescription('description');
		setPreviewPriceDescription();
		setPreviewUrl('url');
	<?php 
	}
	?>
		
	$('#brokerage').bind('focus', function(){
		scrollToId('bottom');
	});

	$("#facebook_message").autoGrow();

	$('#message').bind('keyup', function(event){
		setPreviewMsg('message');
	}).bind('focus', function(){
		$('#facebook_message').css('background-color', '#ffffe5');
		$('#facebook_message').css('padding', '3px');
		$("#facebook_message").trigger('blur');
		//$('#facebook_message').focus();
	}).bind('blur', function(){
		$('#facebook_message').css('background-color', '#ffffff');
		$('#facebook_message').css('padding', '0');
		$("#facebook_message").trigger('blur');
		//$('#facebook_message').focus();
	});

	$('#street_number, #street, #appartment, #price').bind('keyup', function(){
		setPreviewPriceDescription();
	}).bind('focus', function(){
		$('#facebook_linktext').css('background-color', '#ffffe5');
		$('#facebook_linktext').css('padding', '3px');
		$('#facebook_description').css('background-color', '#ffffe5');
		$('#facebook_description').css('padding', '3px');
	}).bind('blur', function(){
		$('#facebook_linktext').css('background-color', '#ffffff');
		$('#facebook_linktext').css('padding', '0');
		$('#facebook_description').css('background-color', '#ffffff');
		$('#facebook_description').css('padding', '0');
	});
	/*
	$('#street').bind('keyup', function(){
		var url_text = $('#street_number').val() + ' ' + $('#street').val() + ' ' + $('#price').val() + ' : CityBlast';
		$('#facebook_linktext').text((url_text?url_text:'Awaiting link text...'));
	}).bind('focus', function(){
		$('#facebook_linktext').css('background-color', '#ffffe5');
		$('#facebook_linktext').css('padding', '3px');
	}).bind('blur', function(){
		$('#facebook_linktext').css('background-color', '#ffffff');
		$('#facebook_linktext').css('padding', '0');
	});

	$('#price').bind('keyup', function(){
		var url_text = $('#street_number').val() + ' ' + $('#street').val() + ' ' + $('#price').val() + ' : CityBlast';
		$('#facebook_linktext').text((url_text?url_text:'Awaiting link text...'));
	}).bind('focus', function(){
		$('#facebook_linktext').css('background-color', '#ffffe5');
		$('#facebook_linktext').css('padding', '3px');
	}).bind('blur', function(){
		$('#facebook_linktext').css('background-color', '#ffffff');
		$('#facebook_linktext').css('padding', '0');
	});
	*/
	$('#description').bind('keyup', function(){
		setDescription('description');
	}).bind('focus', function(){
		$('#facebook_description').css('background-color', '#ffffe5');
		$('#facebook_description').css('padding', '3px');
	}).bind('blur', function(){
		$('#facebook_description').css('background-color', '#ffffff');
		$('#facebook_description').css('padding', '0');
	});

	$('#url').bind('keyup', function(){
		setPreviewUrl('url');
	}).bind('focus', function(){
		$('#facebook_baseurl').css('background-color', '#ffffe5');
		$('#facebook_baseurl').css('padding', '3px');
	}).bind('blur', function(){
		$('#facebook_baseurl').css('background-color', '#ffffff');
		$('#facebook_baseurl').css('padding', '0');
	});

	/*************
	$('#previewLink').click(function(event){

			event.preventDefault();
			$.colorbox({width:"400px", height: "500px", inline:true, href:"#preview_popup"});
			$("#preview_popup #facebook_content").html($("#message").val());
			$("#preview_popup #twitter_content").html($("#message").val().substr(0, 144));
			$("#preview_popup #twtnumchars").html($("#message").val().substr(0, 144).length);

	}); *************/

	$('#message').focus(function()
	{

		if($('#message').val() == "Oooops! You forgot to add a message!" || $('#message').val() == exampleDescription)
		{
			$('#message').val("");
		}
		$('#message').removeClass("error");
		$('#message').css('color','#000');
		$('#message').css('font-style','normal');
	});
	$('#message').blur(function()
	{

		if($('#message').val() == "Oooops! You forgot to add a message!" || $('#message').val() == exampleDescription)
		{
			$('#message').val("");
		}
		$('#message').removeClass("error");
		$('#message').css('color','#4D4D4D');
		$('#message').css('font-style','normal');
	});
 
	$("#list_button").click(function(event){
		$("#btnclicked").val('list_button');
		event.preventDefault();
		$("#blast_type").val("<?php echo Listing::TYPE_IDX;?>");
		validateForm();
    });

    $("#blast_button").click(function(event){
    	$("#btnclicked").val('blast_button');
		$("#blast_button").attr('disabled', true);
        event.preventDefault();
		$("#blast_type").val("<?php echo Listing::TYPE_LISTING;?>");
		validateForm();
    });


	var firstErrOb	= 20;

	function validateForm()
	{

		var errors  =	0;
		
		if ($('#city_id').val() == '') {
			errors++;
		    $("#city_id").after("<div id='msgerror' class='error' style='color: #CC0000;'><strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> select a city.</div><div class='clearfix'>&nbsp;</div>");
		}

		/****
		if ($('#message').val().length < 140 || $('#message').val().length > 400) {
		  errors++;
		    $("#message").after("<div id='msgerror' class='error' style='color: #CC0000;'><strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> enter a description of 140-400 characters.</div><div class='clearfix'>&nbsp;</div>");
		} else ***/

		if ($('#message').val() == exampleDescription) {
			errors++;
			if (!document.getElementById('msgerror2')) {
				$("#message").after("<div id='msgerror2' class='error' style='color: #CC0000; clear: left; padding-top: 5px;'><strong style='color: #CC0000;'>*</strong>Example text is not allowed.<div class='clearfix'>&nbsp;</div>");
			}
			updateErrIndex($('#message'));
		}

		if($("#terms_and_conditions").attr("checked")==false)
		{
			errors = errors + 1;
			$("#terms_and_conditions").addClass('error');
			$('#terms_error').show();

			updateErrIndex($('#terms_and_conditions'));
		}
		else
		{
			$('#terms_error').hide();
		}

		$("#extraForm .validateNotempty").each( function()
		{
			$(this).removeClass('error');

			if($(this).val() == '') {
				errors = errors + 1;
				$(this).addClass('error');

				updateErrIndex($(this));
			}
		});

		$("#extraForm .validateNotZero").each( function(){

			$(this).removeClass('error');

			if( $("#"+$(this).attr("id")+" option:selected").val() == 0) {
				errors = errors + 1;
				$(this).addClass('error');

				updateErrIndex($(this));
			}
		});

		$("#extraForm .validateCurrency").each( function(){
		    $(this).removeClass('error');
			$('#price_error').hide();

			if( !validCurrency($(this).val()) ) {
				errors = errors + 1;
				$(this).addClass('error');

				updateErrIndex($(this));

		    	//$(this).after();
				$('#price_error').show();
				$(this).keyup(function() {
					if (validCurrency($(this).val())) {
						$('#price_error').hide();
						$(this).unbind('keyup');
					}
				})
			}
		});

		$("#extraForm .validateMaintenance").each( function(){
		    	$(this).removeClass('error');
			$('#maintenance_fee_error').hide();

			if($(this).val().length > 0)
			{
			  	if( !validCurrency($(this).val()) ) {
			     	errors = errors + 1;
			     	$(this).addClass('error');

					updateErrIndex($(this));
			    		//$(this).after();
			     	$('#maintenance_fee_error').show();
                    $(this).keyup(function() {
                        if ($(this).val().length == 0 || validCurrency($(this).val())) {
                            $('#maintenance_fee_error').hide();
                            $(this).unbind('keyup');
                        }
                    })
			  	}
			  } else {

              }
		});
		
		$("#imageerror").remove();
		$("#thumbimageerror").remove();
		if( $("#extraForm .uploadifyQueueItem").length == 0)
		if(!$("#fileUploaded").val() )
		{
		    errors = errors + 1;
		    $("#file_uploadUploader").after("<div id='imageerror' class='error' style='color: #CC0000;'><strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> upload a main image.</div><div class='clearfix'>&nbsp;</div>");
		    $("#file_uploadUploader").css("float","left");

			updateErrIndex($('#main_photo'));
		}
		/*
		Thumbnails disabled - Kfoster 2014-01-24
		if(!$("#thumbUploaded").val() )
		{
		    errors = errors + 1;
		    $("#thumb_uploadUploader").after("<div id='thumbimageerror' class='error' style='color: #CC0000;'><strong style='color: #CC0000;'>*</strong>You <strong style='color: #CC0000;'>must</strong> upload a thumbnail.</div><div class='clearfix'>&nbsp;</div>");
		    $("#thumb_uploadUploader").css("float","left");

			updateErrIndex($('#thumb_photo'));
		}
		*/


		if(errors == 0)
		{
			$("#extraForm").submit();
		}
		else
		{
			$("#blast_button").attr('disabled', false);

			if (firstErrOb) {
				if (firstErrOb > 1) {
					$('[tabindex='+(firstErrOb - 1)+']').focus().blur();
					if (firstErrOb != 9) {

						$('[tabindex='+firstErrOb+']').focus();
					}
				} else {
					$('[tabindex=1]').focus();
				}
			}
		}

	}


	function validCurrency(input)
	{
		var number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/i;
		var regex = RegExp(number);
		return regex.test(input) && input.length>0;
	}

	function updateErrIndex(ob)
	{
		if (firstErrOb > parseInt(ob.attr('tabindex'))) {
			firstErrOb	= parseInt(ob.attr('tabindex'));
		}
	}
 });

 function countCharacter(message){
	var cnt=0;
	var shortMessage = true;
	var longMessage = false;
	cnt = message.length;
	$('#char_counter span').text(cnt + ' Characters');

    if(message != exampleDescription){
        if ($('#msgerror2')) {
            $('#msgerror2').remove();
        }
	}


	if(cnt<140 && !shortMessage){
		$(this).addClass('error');
	}
	else if(cnt > 139 && cnt < 401){
		$(this).removeClass('error');
		shortMessage = false;
        if ($('#msgerror')) {
            $('#msgerror').remove();
        }
	}
	else if(cnt>400){
		$(this).addClass('error');
		longMessage=true;
	}
	else if(cnt==400){
		$(this).removeClass('error');
	}
}

$(document).ready(function() {
	$("#extraForm").submit(function(event){
		 var btnclicked = $("#btnclicked").val();
		 $("#"+btnclicked).val("Processing...");
		 $("#list_button,#blast_button").addClass('cancel');
		 $("#list_button").attr('disabled', 'disabled');
		 $("#blast_button").attr('disabled', 'disabled');
	})
});
	 
var isBlasted	= false;
var exampleDescription  = '[Example]: Looking for luxury? This 4-bedroom, 3-bathroom Downtown home is fully updated with hardwood throughout, and a stainless steel and granite kitchen. Garage parks 2 cars, and features a spacious landscaped backyard. A short walk to high-end retail shops and dog park. A must see!';
</script>

<?

/**********************

<!-- Google Code for CityBlast /index/how-blasting-works Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ *
var google_conversion_id = 956260748;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "IL1qCITeowMQjMP9xwM";
var google_conversion_value = 0;
if (49) {
  google_conversion_value = 49;
}
/* ]]> *
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/956260748/?value=49&amp;label=IL1qCITeowMQjMP9xwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


***************************/

?>
