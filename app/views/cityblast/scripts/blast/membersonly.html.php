    <div class="contentBox">

        	<div class="contentBoxTitle">
			<h1>Oh Dear, Your Membership Has Expired.</h1>
		</div>
		
		<p>Unfortunately it seems that your <?=COMPANY_NAME;?> account has expired. No big deal, just <a href="/member/settings">renew your membership today</a> 
			and you can continue reaching 1,000's of prospective buyers right now!</p>
		
		
		<a href="/member/settings"  class="btn btn-primary btn-large pull-right" style="width: 195px; margin-top: 20px;" >Renew Your Membership</a>

		<div class="clearfix"></div>
		
				
	</div>
