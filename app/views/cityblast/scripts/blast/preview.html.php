<style type="text/css">
<!--

.facebook_preview_header {
    -moz-background-clip: border;
    -moz-background-origin: padding;
    -moz-background-size: auto auto;
    background-attachment: scroll;
    background-color: transparent;
    background-image: url("/images/fb_message_header.png");
    background-position: 0 0;
    background-repeat: repeat;
    height: 30px;
    width: 490px;
}
.facebook_preview {
    width: 478px;
}
.facebook_preview_inner {
    width: 398px;
}
-->
</style>

<?php
    $button_label = ($this->blast_type === Listing::TYPE_IDX) ? 'List This Property Only' : 'I\'m sure, Blast It!';
?>
    <div class="contentBox">

        	<div class="contentBoxTitle">
        		
			<h1>Last Chance!</h1>
			<p>Are you sure this is the message you wish to publish? Click '<?php echo $button_label;?>' once you're ready to go.</p>
		</div>

		<form action="/blast/blastsave" method="POST" id="blast-preview">
            <?php foreach ( $this->params as $name => $value ) : ?>
                <input type="hidden" name="<?= $name; ?>" value="<?= $this->escape($value); ?>" />
            <?php endforeach; ?>

			<ul class="uiForm full clearfix" style="margin-top: -20px;">



                <li class="field first" style="margin-top: 30px;">
                	<div style="margin-left: 235px;">
                        <div class="facebook_preview_header"></div>
                        <div class="facebook_preview clearfix" style="margin-bottom: 30px;">
                            <div class="facebook_preview_inner clearfix">
            					<img src="https://graph.facebook.com/<?= $this->member->uid; ?>/picture" class="facebook_userpic" id="facebook_userpic" />

                                <div class="facebook_member" id="label_member">
                                    <?= $this->member->first_name . ' ' . $this->member->last_name; ?></div>

            					<div id="label_message" class="facebook_message">
            						<?= $this->params['message']; ?></div>

            					<div class="facebook_postpic_wrapper" id="facebook_postpic_wrapper">
            						<div>
            							<img src='<?= $this->params['fileUploaded']; ?>' width='75' height='75' style='float: left; padding: 6px;' />
            						</div>
            					</div>

			
			            	            					
            					<div class="facebook_postinfo clearfix">
            						<div class="facebook_linktext" id="facebook_linktext">
                                        <span id="label_street"><?= $this->params['street_number']; ?> <?= $this->params['street']; ?></span>
                                        <? if (!empty($this->params['appartment'])): ?>
											<span id="label_street">apt <?=$this->params['appartment']?></span> 
										<? endif; ?>
                                        <span id="label_city"><?= $this->params['city']; ?> $<?= number_format((int)$this->params['price']) ;?></span>                                        
                                        : <?=COMPANY_NAME;?>
                                    </div>
            						<div class="facebook_baseurl" id="facebook_baseurl" style="color: gray;">www.cityblast.com</div>
            						<div class="facebook_description" id="facebook_description">
                                        List Price: $<span id="label_price"><?= number_format((int)$this->params['price']) ;?></span>
            							<span id="label_street"><?= $this->params['street_number']; ?> <?= $this->params['street']; ?></span>
                                        <span id="label_city"><?= $this->params['city']; ?> <?= $this->params['postal']; ?></span>
            						</div>
            					</div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="field">
                    <div class="grid_12" style="margin: 0px;"><hr style="width: 96%" /></div>
                </li>
			</ul>

            <a href="javascript: void();" ref="/blast/index" id="blast-back" style="padding-right: 10px; margin-top: 0px; padding-top: 0px;">I've changed my mind, go back.</a>
            <input type="button"  class="btn btn-primary btn-large pull-right" style="width: 195px;"  ref="/blast/blastsave" style="margin-bottom: 25px;" id="blast-save" value="<?php echo $button_label;?>">

		</form>

	</div>

</div>

<script type="text/javascript">
<!--
    (function ($) {
        $("#blast-back, #blast-save").click(function () {
        	$("#blast-save").addClass('cancel').attr('disabled', 'disabled');
            $("form#blast-preview")
                .attr("action", $(this).attr("ref"))
                .submit();
            return false;
        });
    })(jQuery);
//-->
</script>
