<!--[if IE 7]>
	<style type="text/css">
		hr{
			margin-bottom: -5px;
		}
		.satisfaction_container .reached_tag .arrow{
			height: 52px;
			right: -50px;
		}
		.satisfaction_container .reached_tag{
			padding-top: 9px;
			padding-bottom: 10px;
		}
		
	</style>
<![endif]-->

    <div class="contentBox">

        	<div class="contentBoxTitle">        		        		
			<h1>Success.</h1>
		</div>
		
		<h4>Your property blast has been scheduled.</h4>
			
		<p style="margin-top: 10px">Your Blast has been scheduled, and will shortly be advertised to tens of thousands of potential local buyers. There's only one 
			thing left to do: <a href="/listing/agentemail/<?=$this->listing->id;?>">share your Blast with your seller client</a>. Simply enter your seller's 
			email, and <?=COMPANY_NAME;?> will send them a <a href="/listing/agentemail/<?=$this->listing->id;?>">professional e-pamphlet</a>, showcasing your Blast and online listing - <i>FREE</i>.</p> 
			
		<p>Get the credit you deserve, share this blast with your client now.</p>
			
		<hr>	

		<a href="/listing/agentemail/<?=$this->listing->id;?>"  class="btn btn-primary btn-large pull-right" style="width: 195px;">Share this Blast</a>

		
		<div class="clearfix"></div>
		<div class="clearfix pull-right" style="text-align: right; margin-top: 6px">

			No thanks, just <a href="/listing/view/<?=$this->listing->id;?>" id="preview_blast">take me to my listing</a>.
			
		</div>
		
		<div class="clearfix"></div>
	
	</div>

	
<script>
$(document).ready(function()
{
	if($.browser.msie && $.browser.version < 9){
		$('.datatable tr:last-child td').css('border-bottom', 'none');
	}
});
</script> 



<? /* ONLY FIRE THE CONVERSION CODE IF THE BLAST IS A PAID ONE */ ?>
<? if(isset($this->payment) && $this->payment->amount > 0) : ?>

<!-- Google Code for CityBlast Sale Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1014009274;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "L5O3CI6Z0gIQupvC4wM";
var google_conversion_value = 0;
if (49.00) {
  google_conversion_value = 49.00;
}
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1014009274/?value=49.00&amp;label=L5O3CI6Z0gIQupvC4wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<? endif; ?>