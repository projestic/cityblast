<style>

body, p, div
{
	font-size: 16px;	
}	
	
</style>

	<div class="contentBox stepped-content">
		<div class="contentBoxTitle">
			<h1>Here's What the <u>Top Agents All Know</u>, That the Other Agents Don't...</h1>
			
			
		</div>
		<div class="row-fluid">

			<h3 style="color: #666">Your Connections = Your Success</h3>
			<p>Here's the reality: without personal connections, you are the <u>bottom feeder</u> of the Real Estate world, in direct competition with everyone else.  But <i>when you have great connections</i>, you're a sure bet - the person people seem to always turn to first.</p>
			<p>How can you get your "<i>friend effect</i>" to grow beyond just your inner circle?</p>


			<h3 style="color: #666">Expand Your Influence to Your Entire Network</h3>			
			<p>The surest source of real estate business comes from <i>family</i>, <i>friends</i> or <i>referrals</i>.</p>
			<p><span style="background-color: #fff799">Let me share this secret with you</span>: when you're using Social Media right, you're going to create a sphere of influence <u><i>on steroids</i></u> for your Real 
				Estate business.  The very farthest reaches of your social network are as close as your <i>best friend</i> when you do <b>one simple thing in Social Media</b>...</p>


			<h3 style="color: #666">Stop Being A Salesperson, Start Being A Friend</h3>
			<p>When people <span style="background-color: #fff799">respect your opinion</span> and care about who you are as a human being, people will beat a path to your door.  
				It's been proven <i>over and over</i> again by the biggest real estate agents in the business.
			</p>
			
			<p>Do you think they started out as the center of attention?</p>  
			
			<p>Don't kid yourself. They positioned themselves there purposely.  You can too - <u>and it's way easier than you think</u>.</p>
			
			<p>But let's talk reality first.</p>


			<h3 style="color: #666">Be Honest: You Haven't Updated Your Fan Page in 9 Months</h3>
			<p>Sure your "<i>tier one</i>" friends know you're a Real Estate agent, but what about the <i>second</i> and <i>third</i> tier friends?</p> 
			<p>What about that <i>one</i> friend you haven't seen since highschool but added you to Facebook a couple of years ago? Does <i>she</i> know you're a Real Estate agent? 
				Does she know you're a <u>damn good</u> one?</p>
			
			<p>Probably not.</p>
			

			<p style="margin-top: 22px;">Read this page, and I'll tell you <u>plainly</u> the dead simple way you can <span style="background-color: #fff799">solve this massive problem</span> today - <i>right this minute</i> - and start becoming the center of your own real estate universe.</p>
	
			<?/*		
			<h3 style="color: #666">Expand Your Circle Of Friends</h3>
			<p>Nearly 100% of Real Estate Agents I talk to say most of their business comes from <i>family</i>, <i>friends</i> or <i>referrals</i>. So let me share this with you: when you're on Social Media, where your 
				<i>family</i>, <i>friends</i> and <i>past clients</i> are, when you can connect with them and create an engaging impression with them on social channels such as Facebook, LinkedIn, and Twitter, 
				you're going to create a sphere of influence on <u>steroids</u> for your Real Estate business.</p>
			*/ ?>
			
		</div>


		<div class="row-fluid" style=";background-color: #d9defb; margin-top: 15px; padding: 12px;">
			<p><u><b>Challenge #1</b></u>: Post a status update on your Facebook page that says this: "<i>Hello friends! just a quick question for all of you, how many of you knew I was a Real Estate Agent? Press 'Like' if you knew, if not, please add a comment that says: 'I had no idea!'</i>"</p>
			
			<p>Were you surprised by those responses?</p>						
		</div>

		
		<div class="row-fluid">
			<hr />
			<a href="javascript:void(0);" class="stepped-content-next btn btn-success btn-large pull-right next-section" id="socialmedia_index_step1_btn"><i class="icon-chevron-sign-down icon-fixed-width"></i> #1 Secret You DON'T Know About Your Online Friends</a>
		</div>


		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow"  style="display: block;" />

	</div>
	

	
	<div class="contentBox stepped-content">
		<div class="contentBoxTitle">


			<h1>Did You Know: <b>1 in 6</b><sup style="color: #790000;">*</sup> of Your Friends Is On The Market For A Home <u>Right Now</u>.</h1>
			<h2>(Of course you didn't.  You're not in the center of the action yet.  Keep reading...)</h2>

<?/*
http://www.apartmenttherapy.com/moving-stats-us-census-bureau-153381
http://www.melissadata.com/enews/articles/0705b/1.htm			
*/?>
			
		</div>
		
		<div class="row-fluid">
				
			<h3 style="color: #666">It's A Fact: People have moved their lives online.</h3>
				
			<p>New friendships, new marriages, new babies. It's all happening Facebook right now.</p>
			
			<p>Guess what else is happening on Facebook?  People are starting their search for a new home.  <i>More often that you can even imagine</i>.</p>
			
			<p><u>Here's the problem</u>: almost none of those people even know you're a Real Estate Agent.  What's worse, if they do happen to know you're a Realtor, they don't
				 know <u><i>how good</i></u> you are.  Why?  Because you haven't told them - at least not often and clearly enough.</p>
			
			<h3 style="color: #666">Big Problem!</h3>			
			<p>What does all that mean?  Unless you're currently working with <i>1 in 6 of your Social Media connections</i> on a real estate transaction, it means there are people you <i>personally</i> know on Facebook, 
				Twitter and LinkedIn who are in the process of buying a house <u>AND</u>, more importantly, <b>YOU</b> don't even know about it!</p>
			
			<p>The good news?  <span style="background-color: #fff799"><u>This is a problem you can easily solve</u></span>.</p>
			
			
		</div>


		<div class="row-fluid" style=";background-color: #d9defb; margin-top: 15px; padding: 12px;">
			<p><u>Challenge #2</u>: Name 5 of your friends on Facebook, Twitter or LinkedIn who are on the market <u>right now</u> for a new home.</i>"</p>
				
			<p>Can't do it?  Hmmm...</p>
		</div>


		<div class="row-fluid">
			<hr />
			<a href="javascript:void(0);" class="stepped-content-next btn btn-success btn-large pull-right next-section" id="socialmedia_index_step2_btn"><i class="icon-chevron-sign-down icon-fixed-width"></i> YES, I Want More Facebook Leads</a>
		</div>

		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" style="display: block;" />
		
	</div>
	



	
	<div class="contentBox stepped-content">
		<div class="contentBoxTitle">
			<h1>Ok... So How Do I Sell my Friends on Facebook Without Being Annoying?</h1>
		</div>
		<div class="row-fluid">
			
			
			<p>Begging your friends on Facebook for real estate leads is <u><span style="background-color: #fff799">embarrassing</span></u> and <u><span style="background-color: #fff799">annoying</span></u>.  In fact, if you do this you're probably better off just doing nothing.</p>
			
			<p>But there is a right way to get new leads on Facebook, Twitter and LinkedIn.</p>
			
			
			<h3 style="color: #666">The Answer is Simple: Give Your Friends Value!</h3> 
			<p>The "value" in this equation is <u>your knowledge</u>. 
				  Your friends are already looking for answers.  Or at least they're looking for information.  
				  You're going to give them those answers in the form of fun, informative <u>Value-Added Content</u>.</p>
				
			
			<h3 style="color: #666">What is value-added content?</h3>		
			<p>Value-added content is information that is beneficial to your friends. Screaming "I'm A Real Estate Agent!" is not valuable information to most people. 
				Instead, what you want to do is <span style="background-color: #fff799">imagine answering unspoken questions</span> that prospective buyers and sellers have on a daily basis. For example:

				<ol style="margin: 20px 0 24px 85px; list-style-type:decimal">
					<li style="margin-top: 6px"><i>10 Tips For Selling Your Home Faster</i>.</li>
					<li style="margin-top: 6px"><i>Rising Mortgage Rates: What Does That Mean For First-Time Home Buyers</i>?</li>
					<li style="margin-top: 6px"><i>When Does A Reverse Mortgage Make Sense</i>?</li>
				</ol>											
			</p>
			

			<p>Consistently answering these "unspoken" questions puts you in a position of being perceived as an <u>expert</u> and <u>trusted authority</u>.</p>
						
			<p>When someone is making the <u><span style="background-color: #fff799">single biggest purchase decision of their life</span></u>, they want to do it with someone who's 
				committed and more importantly knows what they're talking about: <u><b>you</b></u>.</p>


			<h3 style="color: #666">Ready To Become A Trusted Authority On Facebook?</h3>
			<p>We have <b>3 Commandments</b> that will instantly turn you into a trused online Real Estate Expert.</p>
			


			<div class="row-fluid" style=";background-color: #d9defb; margin-top: 15px; padding: 12px;">
				<p><u><b>Challenge #3</b></u>: Create one great piece of "<i>Value-Added Content</i>" and post it on your Facebook page. Count how many comments, likes and shares you get. More importantly, 
					keep track of <u>how long</u> it took.</p>
								
			</div>
	

			<div class="row-fluid">
				<hr />
				<a href="javascript:void(0);" class="stepped-content-next btn btn-success btn-large pull-right next-section" id="socialmedia_index_step3_btn"><i class="icon-chevron-sign-down icon-fixed-width"></i> YES, Teach Me The 3 Social Authority Commandments!</a>
			</div>
			
									
		</div>
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" style="display: block;" />
	</div>



	<div class="contentBox stepped-content">
		<div class="contentBoxTitle">
			<h1>Awesome, Now You Have A Plan. Time For The Rubber To Meet The Road.</h1>
		</div>
		<div class="row-fluid">
			

			<p>Being perceived as an authority in Real Estate is <i>dead simple</i> and it boils down to <b>3 Commandments</b>:
				
				<ol style="margin: 20px 0 20px 85px; list-style-type:decimal">
					<li style="margin-top: 6px"><b style="color: #666">Be generous.</b> Share your knowledge, experience and advice. You'll make more profit by helping more people.</li>
					
					<li style="margin-top: 6px"><b style="color: #666">Be present.</b> Work to participate in the communities where your target audience hangs out, and show up as a valuable member of that community.</li>
					
					<li style="margin-top: 6px"><b style="color: #666">Be consistent.</b> Building trust takes time. When someone is making the single biggest purchase decision of their life, 
						they want to do it with someone who's committed.</li>
				</ol>
					
			</p>
			
			<h3 style="color: #666">The Best Things Take a Bit of Time</h3>
			<p>Of the <b style="color: #666">3 Commandments</b>, the most important of all is consistency. More than anything else, becoming a trusted authority to your friends on Facebook, Twitter and LinkedIn <u>takes time</u>.</p> 
			
			<p>A lot of it.</p>
			
			<h3 style="color: #666">What Do The Experts Say?</h3>	
			<p>The industry leaders say that if you want to make an impact, you need to spend <u><span style="background-color: #fff799">no less than 1 hour a day</span></u> grooming your Social Media presence!!!</p>

			<p>That means every morning, you should be getting up, finding a great article, putting your personal spin on it and posting it to <u>ALL</u> of your Social Media networks including: Facebook, Twitter and LinkedIn.</p>
			
			<p><u>But frankly, I'm not telling you anything you didn't already know.</u></p>
			
			<h3 style="color: #666">So Why Aren't You Posting <u>Value-Added</u> Content Every Day?</h3>	
			<p>The reason why you're not posting <i>Value-Added Content</i> to your Fanpage every day is because <u><b style="color: #666">it's a complete pain in the ass</b></u>.</p>



	
			<div class="row-fluid" style=";background-color: #d9defb; margin-top: 15px; padding: 12px;">
				<p><u><b>Challenge #4</b></u>: Create one great piece of "<i>Value-Added Content</i>" and post it on your Facebook page. Count how many comments, likes and shares you get. More importantly, 
					keep track of <u>how long</u> it took.</p>
								
			</div>
	



			<div class="row-fluid">
				<hr />
				<a href="javascript:void(0);" class="stepped-content-next btn btn-success btn-large pull-right next-section" id="socialmedia_index_step3_btn"><i class="icon-chevron-sign-down icon-fixed-width"></i> I Know I Should Be Posting, What Are My Options?</a>
			</div>
			
			
			
		</div>
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" style="display: block;" />
	</div>

	<div class="contentBox stepped-content">
		<div class="contentBoxTitle">
			<h1>I Just Don't Have the Time To Take On Yet Another Daily Task.</h1>
		</div>
		<div class="row-fluid">
			<p>If your life is half as busy as mine is, let's be honest, there's just no way in this world that you can make a commitment to update your Facebook Fanpage <i>day after day</i>, <i>year after year</i>.</p>
			
			<p>Even though I know I'm <u><span style="background-color: #fff799">losing money</span></u> because I'm not updating my Fanpage, I know myself well enough to understand that <i>I won't do it</i>.</p>
			
			<p>That leaves us with three options:
				
				<ol style="margin: 20px 0 30px 85px; list-style-type:decimal">
					
					<li style="margin-top: 6px"><b>Hire Internal Social Media Marketer</b>. Finding the right person might be a challenge, but if you can afford the soaring price tag, this is your <u>best option</u>.</li>
					
					<li style="margin-top: 6px"><b>Hire a Social Media Agency</b>. There are some great options out there and it will typically run you between $500 and $1,500 a month. 
						<?/*It's sometimes hard to find Agencies that understand Real Estate but here's a <a href="#">list companies</a> we highly recommend.*/?></li>
										
					<li style="margin-top: 6px"><b>Hire CityBlast</b>. For less than $2 a day, our certified and professional Social Experts will manage your posts.</li>
				</ol>								
			</p>
			
			<p>CityBlast is the best of <u>both worlds</u>, it's like hiring your own personal Social Media Assistant, but at a fraction of the cost.</p> 
				
			<p>Most importantly, at CityBlast <u><span style="background-color: #fff799">all we do are Social Media Marketing posts for Real Estate Agents</span></u>.</p>
				
			<p>In fact, we've created more than <b>900,000</b> <i>hand-crafted</i> Social Media posts and helped <i>more than</i> <b>4,000</b> agents and brokers.</p>
			
			
		<h3 style="color: #666">Socialize Your Way To Success!</h3>				
			<p>And when you do something that many times, you tend to get pretty good at it.</p>
			
			<p>Best of all, you can <u>meet your Social Expert today</u> and try them out absolutely <b style="color: #666">FREE</b> of charge <i>for 14 days</i> with <u>no hidden contracts</u> and <u>no hidden fees</u>.</p>
			
			<p>Simply put, CityBlast has helped <u>more</u> Real Estate agents <u>find more new clients</u> though social media <i>than any other company on the planet</i>. Period.</p>
			
			<p>If you're ready to join the online conversation, we can help. <u><span style="background-color: #fff799">Start your free trial today</span></u>.</p>
			
		</div>
		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow" style="display: block;" />
	</div>

	<div class="contentBox stepped-content-last">
		<div class="clearfix">
			<div class="pull-left tryDemoTxt">
				<h3 style="font-size: 28px;">Become a <u>trusted</u> authority.</h3>
				<h4>Get started today with your Free 14-Day Trial of <?=COMPANY_NAME;?>.</h4>
			</div>
			<div class="pull-right">
				<a class="uiButtonNew tryDemo" href="/member/index" id="socialmedia_index_join_now_btn">Free Trial.</a>
			</div>	
		</div>		
	</div>
	
	<? /****
	<div class="contentBox">
		<div class="stepBox step3">
			<div class="opacLayer"></div>
			<div class="stepTitle">
				<div class="titleNumber">
					<span>3</span>
					<img src="/images/new-images/number-bg.png" />
				</div>
				<h2>Submit your preferences to your new Social Media Expert.</h2>
			</div>
			<div class="stepContent clearfix">
				<div class="noteContainer pull-left">
					<h6>What's This:</h6>
					<p>You'll be asked to connect your Facebook profile with CityBlast. That's so we know where to post the content and updates you have previously chosen in Step 1. It's as simple as  that!</p>
					<img src="/images/new-images/note-left-arrow.png" />
				</div>
				<div class="ladyCartoon pull-left">
					<img src="/images/new-images/step3-lady-cartoon.png" />
				</div>
				<div class="btnContainer pull-left">
					<h4>Connect with Facebook to continue...</h4>
					<div class="termsBox"><input type="checkbox" name="terms" id="terms" checked="checked"> I agree to the <a href="/index/terms" >Terms and Conditions</a>.</div>
					<a href="#" class="uiButton" id="facebook_signup_button" style="width: 300px;">Send.</a>
				</div>
			</div>
			
		</div>
	
	</div>
	****/ ?>
	
	

</form>


<script type="text/javascript">
	$(document).ready( function () {
		$('.stepped-content-next').click( function (e) {
			var $container = $(this).parents('.stepped-content');
			$container.next().slideDown();
			if ($container.next().find('.stepped-content-next').length == 0) {
				$('.stepped-content-last').show();
			}
			e.preventDefault();
		});
		$('.stepped-content, .stepped-content-last').hide().first().show();
	});
</script>


<? /* echo $this->render('member/upfront-settings.html.php'); */ ?>

<?php echo $this->partial('common/fb_conversion_lp.html.php'); ?>