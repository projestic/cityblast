<style>

body, p, div
{
	font-size: 16px;	
}	
	
</style>


	<div class="contentBox stepped-content">
		<div class="contentBoxTitle">
			<h1>The ONE Common Activity That Nearly Every <u>Top Producer</u> is Doing on <u>Facebook</u> ... That Struggling Agents Aren't</h1>
			
			
		</div>
		<div class="row-fluid">
	
			<p><b>Every day, the most powerful Top-Producing Agents are pulling farther ahead in the crucial race for online real estate leads, and earning more and more business - from the closest friends of other Realtors&reg;.</b></p>
				 
			<p>There is an <i>arms race</i> going on that you may not even be aware of, and it is <u>costing the other agents</u> a number of their deals every year.  How is this happening? </p>
			
			<h4 style="margin-top: 20px;">How are these agents stealing market share and selling to My Friends?!</h4>
					
			
			<p style="margin-top: 20px;">The answer is simple and right in front of your face - but hard to see until you're aware of it.  The top Realtors&reg; are using the widely-renowned strategy 
				of <a href="http://mashable.com/2012/09/13/inbound-marketing-strategies/"><b>Inbound Marketing</b></a> on all of their social accounts 
				- the exact same method employed religiously by business power players the world over, like <a href="http://mashable.com/2012/09/13/inbound-marketing-strategies/">Starbucks, Dell, 
					GE, Nike and Coca-Cola</a>.</p>
			
			
			<h3>Nearly all of the top Realtors&reg; have figured it out.</h3>  
				
			<div style=";background-color: #d9defb; margin-top: 15px; padding: 12px;" class="row-fluid">
				<p style="background-color: #d9defb;">Not because they're geniuses - but simply because they have the financial resources to hire these social media experts too.  And once social 
				media experts join their team, guess what?  They almost exclusively employ this as their #1 strategy to <u>generate incredible returns for their clients</u>.</p>
			</div>
				
			<p>The net result?  These Agents have '<i>discovered</i>' the key to massively more real estate business through social media channels.</p>
			
		</div>
	

		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow"  style="display: block;" />

	</div>
	

	<div class="contentBox stepped-content">
		
		<div class="contentBoxTitle">
			<h1>What is <span style="background-color: #fff799">Inbound Marketing</span>? <br/>Here's Your PhD in Under 10 Seconds:</h1>
		
		</div>
		
		<div class="row-fluid">
					
			
			<p>Inbound marketing earns the attention of customers, makes the company easy to be found, and draws customers to the company - by producing <a href="http://en.wikipedia.org/wiki/Inbound_marketing">interesting content that customers value.</a></p>
				
			<p>That's from Wikipedia.  We couldn't say it better ourselves!</p>
			
			<h3>Taking it one step further:</h3>
			
			<p>David Meerman Scott recommends that marketers "earn their way in" (via publishing helpful information), in contrast to outbound marketing where they "<a href="http://en.wikipedia.org/wiki/Inbound_marketing">buy, beg, or bug their way in".</a></p>
			
			<div style=";background-color: #d9defb; margin-top: 15px; padding: 12px;" class="row-fluid">
			
				<p><b>If it's good enough for Starbucks, Dell, GE, Nike and Coca-Cola and nearly every Top Producing Realtor&reg; in North America, then don't you think it's worth a try?</b></p>			
								
			</div>

			
			<h3 style="margin-top: 20px;">Hint:  It is!</h3>
			
			
			<p>According to <a href="http://www.hubspot.com">HubSpot</a>, the absolute worldwide authority, inbound marketing is especially effective for: 

				<ol style="margin: 20px 0 24px 85px; list-style-type:decimal">
					<li style="margin-top: 6px"><i>small businesses;</i></li>
					<li style="margin-top: 6px"><i>that deal with high dollar values;</i></li>
					<li style="margin-top: 6px"><i>and have long research cycles and knowledge-based products.</i></li>
				</ol>				
				
			</p>
			

			
			<p>In these areas, client prospects are more likely to <a href="http://blog.hubspot.com/blog/tabid/6307/bid/34209/93-of-Companies-Using-Inbound-Marketing-Increase-Lead-Generation-New-ROI-Data.aspx">get informed</a> 
				and <a href="http://blog.hubspot.com/blog/tabid/6307/bid/34209/93-of-Companies-Using-Inbound-Marketing-Increase-Lead-Generation-New-ROI-Data.aspx">hire someone who demonstrates expertise</a>.</p>
					
			
			<p>Does that sound like the <u><i>Real Estate Business</i></u> to you?  It should!</p>	
			
			
		</div>

		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow"  style="display: block;" />

	</div>
	


	<div class="contentBox stepped-content">
		
		<div class="contentBoxTitle">
			<h1>So... Is There a <i>Catch</i>?</h1>
		
		</div>
		
		<div class="row-fluid">
			
			<p>Yes!  Well - at least <u>there used to be</u>.  Keep reading.</p>
			
			<p><b>Inbound Marketing</b> is <u>very powerful</u>, but <u><i>very time consuming</i></u>.</p> 
			
			<div style=";background-color: #d9defb; margin-top: 15px; padding: 12px;" class="row-fluid">		
				<p>That's why it used to be available only to the <i>Top Producers</i> - because you almost certainly 
					needed to hire someone else to manage it for you.  It involves sourcing valuable local and relevant articles and information, creating catchy write-ups, 
					and posting consistently day after day - to create a <span style="background-color: #fff799">sense of trust</span> and <span style="background-color: #fff799">subject expertise</span>.
			
				</p>
			</div>
			
			
			<p style="margin-top: 15px;">	
				Experts will tell you that it takes <u>at minimum an hour every single day</u>.
			</p>
			
			<h3>Problem:  Busy Realtors&reg; Don't Have an HOUR EVERY DAY.</h3>
			
			
			<p>That's Where CityBlast's Social Experts Have <u>EVENED THE PLAYING FIELD</u>.</p>
			
			<p><u>Gone</u> are the days when only the <i>Top Producers</i> <span style="background-color: #fff799">could afford</span> a <u><i>Social Expert</i></u>.</p>
			
		</div>

		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow"  style="display: block;" />

	</div>
	




	<div class="contentBox stepped-content">
		
		<div class="contentBoxTitle">
			<h1>CityBlast's Experts Work For You For <u>Less Than <span style="background-color: #fff799">$2/Day</span></u>.</h1>
		
		</div>
		
		<div class="row-fluid">
			
			<h3>HERE'S HOW IT WORKS:</h3>
			
			<p>Just like hiring an expensive <b>INBOUND MARKETING</b> firm, you tell our experts:
				
				<ol style="margin: 20px 0 24px 85px; list-style-type:decimal">
					<li style="margin-top: 6px"><i>What geographical market you want content for;</i></li>
					<li style="margin-top: 6px"><i>What exact types of cool and interesting content we should source; and</i></li>
					<li style="margin-top: 6px"><i>How often to post.</i></li>
				</ol>						
				
			</p>

			<p>
				<u>That's it</u>!  Once you've told us how you like it, we'll do the rest - <u><i>forever</i></u>!  	
			</p>

			<div style=";background-color: #d9defb; margin-top: 15px; padding: 12px;" class="row-fluid">
				<p>We will post articles, videos and information to ALL of your major social media accounts - <a href="http://www.facebook.com">Facebook</a>, <a href="http://www.Twitter.com">Twitter</a> and <a href="http://www.LinkedIn.com">LinkedIn</a>.  
					And it will always be <b>INBOUND MARKETING</b> content that your social media contacts will find <span style="background-color: #fff799">interesting and valuable</span> - <u><i>not annoying and pushy</i></u>.</p>
			</div>
				
			<p style="margin-top: 20px"><b>Best of all?  When the time comes - they'll turn to you as a trusted friend with the knowledge and expertise they seek.</b></p>
			
			<p>We employ the exact same <b>INBOUND MARKETING</b> strategy as all of the top major brands, using an awesome team of <u>real</u> <i>Social Media Experts</i> here in our office.  
			The advantage is that work for you <i>for less than $2/day</i>.</p>


			<div class="contentBoxTitle">
				<h1>Meet The Team</h1>			
			</div>
		
		
			<div class="clearfix" style="margin-top: 30px;">
				<img src="//cityblast.com/images/our-story/our-team.png" />
			</div>
		



		</div>

		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow"  style="display: block;" />

	</div>
	

	<div class="contentBox stepped-content">
		
		<div class="contentBoxTitle">
			<h1>Don't Just Take <u>Our Word</u> For It...</h1>
		
		</div>
		
		<div class="row-fluid">


			<p><b>We believe in our Social Experts and in the INBOUND MARKETING magic so much, that we offer a <span style="background-color: #fff799"><u>100% FREE 14-day Trial</u></span>.</b></p>

			<div style=";background-color: #d9defb; margin-top: 15px; padding: 12px;" class="row-fluid">
				<p>If you don't agree that our Experts are as good at creating amazing lead-generating content that makes you look professional, up to date and knowledgeable as any 
					of the top firms charging thousands of dollars every month, simply cancel within 14 days and you'll never be billed.  </p>
			</div>
			
			<p style="margin-top: 20px;">It's that <u><i>simple</i></u>.</p>
			
			<p>If you <b>DO</b> think we're the best in the business, then <u>stick with us</u>, and we'll revolutionize your real estate marketing.</p>
			
			
			<p><b>Following Your FREE Trial, Our Social Experts Work for You for <span style="background-color: #fff799"><u>ONLY</u></span> $59.99/month.</b></p>
			
			<p>Why don't you give <a href="/">CityBlast</a> and <b>INBOUND MARKETING</b> a try?  Take 2 minutes right now to tell us where you work, what kind of content you'd like us to post, and 
				get us started <u>working for you</u>.</p>
					
			

		</div>

		<img src="/images/new-images/step-box-btm-arrow.png" class="boxBottomArrow"  style="display: block;" />

	</div>



	<div class="contentBox stepped-content-last">
		<div class="clearfix">
			<div class="pull-left tryDemoTxt">
				<h3 style="font-size: 28px;">It's <u>FREE</u> for 14 Days.</h3>
				<h4>Come see what CityBlast's <i>Social Experts</i> can do for YOU!</h4>
			</div>
			<div class="pull-right">
				<a class="uiButtonNew tryDemo" href="/member/index" id="socialmedia_index_join_now_btn">Try An Expert.</a>
			</div>	
		</div>		
	</div>
	
	
	
</div>

<?php echo $this->partial('common/fb_conversion_lp.html.php'); ?>