<div class="row-fluid">
	
	<div class="row-fluid">
		<div class="bannerContainer">
			
			<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
				<a href="//www.youtube.com/embed/Ysi4-i_gOJc?rel=0&autoplay=1" id="explain_video" class="cboxElement">
			<? else : ?>
				<a href="//www.youtube.com/embed/OCXo__6Q0Xw?rel=0&autoplay=1" id="explain_video" class="cboxElement">
			<? endif; ?>
				<img src="/images/cityblast-banner-bg.jpg" />
				<span class="bannerInfo">
					<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
						<span class="bannerTitle">Are You Losing Valuable Real Estate Commissions?</span>
					<? else : ?>
						<span class="bannerTitle">Are You Losing Valuable Mortgage Commissions?</span>
					<? endif;?>					
					
					<span class="bannerSubTitle">A <?=COMPANY_NAME;?> Social Media Expert can help, see how.</span>
					<span class="playButton"><span>Play Video</span></span>
				</span>
			</a>
		</div>
		
		<?/*<iframe class="videoPlayer" src="//www.youtube.com/embed/8T6V3PZi8G0?rel=0" frameborder="0" allowfullscreen></iframe>*/?>

		<?/*
		<? if(stristr(APPLICATION_ENV, "cityblast")) : ?>
			<iframe class="videoPlayer" src="//www.youtube.com/embed/Ysi4-i_gOJc?rel=0" frameborder="0" allowfullscreen></iframe>
		<? else: ?>
			<iframe class="videoPlayer" id="viddler-f74c47ef" src="//www.viddler.com/embed/f74c47ef/?f=1&autoplay=0&player=full&secret=22300716&loop=false&nologo=false&hd=false" frameborder="0" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
		<? endif; ?>
		*/?>

	</div>
		
	<div class="contentBox">
		
		<h2 style="margin-bottom: 35px; font-weight: normal;">Get a Social Expert managing your social media for less than $2/day, and never miss a deal again</h2>
		
		<div class="strategyContainer" style="margin-top: 10px;">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-2.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">We'll plan and manage your lead generation strategy.</h3>
					Simply tell us the city where you work, and what kind of posts suit your style best.  Our Social Experts <i>do the rest</i> - planning your marketing, sourcing articles and videos, and posting them to your accounts.
				</div>
			</div>
		</div>
		
		<?/*
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-2.png" />
				</span>
				<div class="media-body">
					<h3 class="media-heading">Your Social Media Expert plans your campaign.</h3>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac sem vel eros dapibus bibendum sed non ipsum. Sed eu lorem lectus.
				</div>
			</div>
		</div>*/ ?>
		
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-3.png" />
				</span>
				<div class="media-body">
					<?/*<h3 class="media-heading">As often as you choose, your Social Expert consistently posts articles, videos & information.</h3>*/?>
					<h3 class="media-heading">As often as you choose, we post articles, videos & information.</h3>
					You also control how often your Social Expert will post.  Following your instructions, they'll <i>consistently</i> post updates to your Facebook, Twitter and/or LinkedIn accounts that make sure you look professional and up-to-date.
				</div>
			</div>
		</div>
		<div class="strategyContainer">
			<img class="bottomArrow" src="/images/new-images/strategy-bottom-arrow.png" />
			<div class="media">
				<span class="pull-left">
					<img class="media-object" src="/images/new-images/media-icon-4.png" />
				</span>
				<div class="media-body">
					<?/*<h3 class="media-heading">You respond to new leads, and follow your success in your dashboard.</h3>*/?>
					<h3 class="media-heading">Manage your leads and measure success in your dashboard.</h3>
					With your powerful, professional presence, you'll see <i>an increase in new leads</i> through your social media accounts.  You just follow up with new leads as they come in, and generate more new business.
				</div>
			</div>
		</div>
		<div class="clearfix">
			<div class="pull-left tryDemoTxt">
				<?/*<h3 style="font-size: 28px;">Get an Expert handling your business's social media.</h3>*/?>
				<h3 style="font-size: 28px;">Our Social Experts can help.</h3>
				<h4>Start your Free 14-Day Trial of <?=COMPANY_NAME;?>  Social Experts now.</h4>
			</div>
			<div class="pull-right">
				<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>
			</div>
		</div>
	</div>

		
</div>


<? echo $this->render('logos-responsive-small.html.php');	?>
<div style="margin-bottom: 25px;">&nbsp;</div>

<script type="text/javascript">
	$(document).ready(function() 
	{
		$("#explain_video").colorbox({
			iframe: true, 
			innerWidth: 960, 
			innerHeight: 540, 
			maxWidth: '80%', 
			maxHeight: '80%'
		});
		
		$("#videopopup").click(function(e)
		{
			e.preventDefault();			
			$.colorbox({height: "500px",inline:true, href:"#sample_popup",onClosed:function(){ $('#sample_popup').empty(); }});
		});	
		
		
		$('#explain_video').trigger('click');
			
	});
</script>

<?php echo $this->partial('common/fb_conversion_lp.html.php'); ?>