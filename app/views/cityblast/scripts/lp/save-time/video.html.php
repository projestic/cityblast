<div class="row-fluid">
	<div style="margin-left: -9px; margin-bottom:5px;"><img src="/images/city-blast_logo.png"></div>	
</div>

<!-- Main hero unit for a primary marketing message or call to action -->
<? /*<div class="hero-unit">*/ ?>
<div style="box-shadow: 0px 0px 30px -1px #666; -webkit-box-shadow: 0px 0px 30px -1px #666; -moz-box-shadow: 0px 0px 30px -1px #666; margin-top: 10px;">
	<div class="row-fluid" style="padding-top: 8px; padding-bottom: 8px; background-color: #e8e8e8;">
			
		<div class="span6" style="padding-left: 38px;">
		
			<h1 style="margin-bottom: 0; padding-bottom: 12px; border-bottom: 1px solid #c0c0c0;"><i>The Astounding Results of Your Own Custom Social Media Campaign<br/>
				<span style="color: #ff7f00;">Without All of the Hard Work<span></i></h1>
			
			
			<div style="padding-top: 24px; padding-left: 35px; border-top: 1px solid #f1f1f1;"><img src="/images/checkmark-new.png">
				<span style="vertical-align: middle; margin-left: 10px; line-height: 18px; font-size: 18px; font-weight: bold; color: #7d7d7d">You Save An Hour A Day!</span></div>
			
			<div style="margin-top: 18px; padding-left: 35px;"><img src="/images/checkmark-new.png">
				<span style="vertical-align: middle; margin-left: 10px; line-height: 18px; font-size: 18px; font-weight: bold; color: #7d7d7d">Our Experts Do The Work For You!</span></div>
			
			<div style="margin-top: 18px; padding-left: 35px;"><img src="/images/checkmark-new.png">
				<span style="vertical-align: middle; margin-left: 10px; line-height: 18px; font-size: 18px; font-weight: bold; color: #7d7d7d">You Get a 30 Day Risk Free Trial!</span></div>
			
			
			
			<p style="margin-top: 35px; text-align: right;"><a href="<? echo $this->group_name;?>/<?=$this->page_name;?>/join" class="btn btn-success btn-large">I'm Ready to <em>Effortlessly</em> Meet New Clients</a></p>
			
		</div>
		
		<div class="span6" style="height: 350px; padding-right: 20px; padding-top: 20px; padding-bottom: 12px;">

			<iframe width="100%" height="100%" src="//www.youtube.com/embed/OrrGZk2ogwU?rel=0&autoplay=1" frameborder="1" allowfullscreen></iframe>

		</div>
		
	</div>


	<div class="clearfix" style="border-top: 1px solid #bfbfbf;">	
		<? echo $this->render('logos-responsive.html.php'); ?>
	</div>

		  
	<div class="row-fluid" style="background-color: #ececec;">
		<div class="span12" style="padding-top: 30px; padding-bottom: 10px;">
			<h1 style="text-align: center; margin: 0; line-height: 40px; color: #666666">
				Start Today! Call us at <span style="color: #333333;">1-888-712-7888</span>&nbsp;<span style="font-size: 14px; color: #3D6DCC">(*9:00 am to 5:00 pm EST)</span>
			</h1>
		</div>
	</div>

		  

	<? 
		$this->bgcolor = "background-color: #ececec;";
		echo $this->FacePile('responsive'); 
	?>

	<div class="row-fluid" style="padding: 9px 0; background: #4e4e4e; background : -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#4e4e4e), color-stop(100%,#3e3e3e)); background: -moz-radial-gradient(center, ellipse cover, #4e4e4e 0%, #3e3e3e 100%); background: -ms-radial-gradient(center, ellipse cover, #4e4e4e 0%,#3e3e3e 100%); background: -o-radial-gradient(center, ellipse cover, #4e4e4e 0%,#3e3e3e 100%); background: -ms-radial-gradient(center, ellipse cover, #4e4e4e 0%,#3e3e3e 100%); background: radial-gradient(ellipse at center, #4e4e4e 0%,#3e3e3e 100%);">
		
			<p style="font-size: 25px; color: #fff; font-weight: 600; margin-left: 60px; margin-right: 60px;" class="pull-left">Begin Your Free 30-Day Trial, and get our Social Experts working for you.</p>
		
			<a href="<?=$this->group_name;?>/<?=$this->page_name;?>/join" class="pull-right btn btn-success btn-large" style="text-transform: uppercase; font-size: 28px; font-weight: bold; padding: 19px; margin-top: 10px; margin-right: 60px;">Try Us Free!</a>
		
	</div>
	<style type="text/css">
	.fb_iframe_widget,
	.fb_iframe_widget span,
	.fb_iframe_widget iframe[style]  {width: 100% !important;}
	</style>

	<div class="row-fluid" style="padding-top: 10px; background-color: #ececec;">

		<div class="span12 clearfix" style="min-height: 161px; padding-left: 8px;">		
			<h3 style="color: #808080; ">Members helping members.</h3>
			<fb:comments xid="member_join" href="<? echo APP_URL;?>/join" width='100%'></fb:comments>
		</div>

	</div>
</div>


<script type="text/javascript">

	$(document).ready(function() 
	{	
		// Adding event listening on comment
		FB.Event.subscribe('comment.create', function(response){
		
		    var commentQuery = FB.Data.query("SELECT object_id,id,text, fromid FROM comment WHERE post_fbid='"+response.commentID+"' AND object_id IN (SELECT comments_fbid FROM link_stat WHERE url='"+response.href+"')");
		    var userQuery = FB.Data.query("SELECT name FROM user WHERE uid in (select fromid from {0})", commentQuery);
		
		
		    FB.Data.waitOn([commentQuery, userQuery], function() 
		    {
				var commentRow = commentQuery.value[0];
				var userRow = userQuery.value[0];
		
				var commentId   =	commentRow.id;
				var name	 	=	userRow.name;
				var fuid		=	commentRow.fromid;
				var text	 	=	commentRow.text;
				var mid		=	<?php echo (empty($_GET['mid'])) ? 0 : intval($_GET['mid']);?>;
				var url		=	'/comments/broadcast';
				var data	 	=	'name='+name+'&fuid='+fuid+'&text='+text+'&mid='+mid+'&referring_url=/<?=$this->group_name;?>/<?=$this->page_name;?>';
		
		
				$.ajax({
					type: 'POST',
					url: url,
					data: data,
					success: function(response){
						// do nothing
					},
					dataType: 'json'
				});
		
		    });
		});	
	});
</script>

	


<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/0184.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

<?php echo $this->partial('common/fb_conversion_lp.html.php'); ?>