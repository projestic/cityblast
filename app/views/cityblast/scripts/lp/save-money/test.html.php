<div class="grid_12">

	<div class="box" style="padding: 20px 40px;">

		<div class="lp_header clearfix">
		
			<div class="lp_header_left">
			
				<img src="/images/lp-header-logo.png" width="192" height="158" class="lp_header_logo" />
			
			</div>
		
			<div class="lp_header_right">
			
				<div class="trial_circle">FREE<span style="display: block; font-size: 20px; font-weight: 600;">30-DAY</span>TRIAL</div>
				
				<h1 class="heading">Make Money Now.</h1>
				<h3 class="subheading" style="font-weight: 400;">Sign Up for the <span style="font-weight: 600; color: #ffffb2;">Powerful Facebook Tool</span> &#8212; Made For <span style="font-weight: 600; color: #ffffb2;">Real Estate Agents</span>.</h3>
				
				<ul class="list">
					<li>Over 90% of home searches begin online.</li>
					<li>Buyers spend 3X more time on Facebook vs. All other websites.</li>
					<li>Experts recommend 60-Minutes of Facebook Marketing every day.</li>
				</ul>
				
				<div class="clearfix" style="padding-bottom: 20px; border-bottom: solid 1px #1a1a1a;">
				
					<div class="button_left">
						Facebook Marketing a Mystery?<br />Don't have 60-Minutes per day?
						<div class="button_left_arrow"></div>
					</div>
					
					<a href="<?=$this->member_join_url?>"><img src="/images/blank.gif" width="280" height="60" class="connect_fb_btn" /></a>
					
				</div>
				
				<span style="text-align: center; display: block; padding-top: 20px; border-top: solid 1px #404040; color: #999;">
				
					I'd like to learn more about how it works. <a href="javascript:void(0);" id="how-it-works-btn" style="color: #ccc;">Show me more.</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0);" id="clientfinder_video_btn" style="color: #ccc;">Watch Video</a> 
					
				</span>
				
			</div>
			
		</div>
		
		<div style="display: none;" id="how-it-works">
		
			<div class="key_features clearfix">
				
				<div class="key_feature">
					
					<h3>Your Stellar Image.</h3>
					
					<p>Today, looking professional on Facebook is as vital to your business as looking professional in the outside world.  By keeping your Facebook consistently up-to-date with first class real estate content and gorgeous local listings, your friends and acquaintances will know you're a trusted professional.</p>
					
					<img width="50" height="50" class="image icon" src="/images/blank.gif">
				
				</div>
				
				<div class="key_feature">
					
					<h3>Increased Commissions.</h3>
					
					<p>ClientFinder makes you more money.  A lot more.  By keeping your Facebook consistently up-to-date with first class real estate content and gorgeous local listings, you'll turn your friends and casual acquaintances into loyal clients.  And best of all?  You don't have to pressure.  They'll come to you.</p>
					
					<img width="50" height="50" class="commissions icon" src="/images/blank.gif">
				
				</div>
				
				<div class="key_feature">
					
					<h3>Zero Maintenance.</h3>
					
					<p>ClientFinder's user-friendly setup takes under 30 seconds, and doesn't require another annoying account or password.  Connect effortlessly with one click, and tailor your personal settings.  ClientFinder does the rest.  Simply welcome your incoming leads, and build new client relationships.  It's that easy.</p>
					
					<img width="50" height="50" class="maintenance icon" src="/images/blank.gif">
				
				</div>
				
			</div>
			
			<div class="action_heading">Ready to upgrade your online image? <a href="<?=$this->member_join_url?>">Click here</a> to set up ClientFinder</div>
		
			<div class="clearfix">
			
				<div style="width: 450px; margin-right: 20px; float: left; font-size: 12px;">
	
					<h4>How Does it Work?</h4>
					
					<ol>
						<li style="margin-bottom: 10px;">Simply register for ClientFinder. Follow the on-screen instructions to activate your Facebook, Twitter and LinkedIn.
						</li>
						<li style="margin-bottom: 10px;">As often as you choose, ClientFinder will automatically update your Facebook, Twitter or LinkedIn with first class real estate articles, videos and gorgeous local listings. These polished updates look just like you've created them yourself, and are visible to your network of friends and followers.
						</li>
						<li>Your friends will see your professional tips and hot featured listings, and email or message you with questions, comments or to book showings. You simply welcome their inquiries, build your client relationships, and earn more commissions.</li>
					</ol>
				
				</div>
			
				<div style="width: 450px; float: left; font-size: 12px;">
				
					<h4>Why Use ClientFinder?</h4>
					
					<p>ClientFinder turns your Facebook, Twitter or LinkedIn into an incredibly powerful real estate machine, keeping your image professional and current, and generating vastly more business from your personal online network.</p>
					
					<ul>
						<li>enjoy a first class online presence, and a lucrative top-producer image</li>
						<li>welcome legions of brand new clients and higher monthly commissions</li>
						<li>effortlessly project the confident business savvy of a current real estate professional</li>
					</ul>
					
					<p>Best of all?  ClientFinder never requires any maintenance.  Simply set up your ClientFinder once, and watch your real estate business grow.</p>
					
				</div>
					
			</div>
			
			<div class="action_heading">Who has time to consistently update their Facebook? <a href="#">ClientFinder</a> has you covered.</div>
			
			<div style="margin: 20px 0;" class="clearfix">
			
				<div style="width: 400px; float: left; font-size: 12px;">
					
					<h4>Wondering How The Updates Appear?</h4>
					
					<p>This is what an actual ClientFinder update looks like on your Facebook.  Every single update is manually reviewed by <?=COMPANY_NAME;?>'s real human editors for professional content, spelling and grammar.</p>
					
					<ul>
						<li>Clean</li>
						<li>Professional</li>
						<li>Current</li>
						<li>Trusted Sources</li>
					</ul>
				</div>
			
				<div style="width: 480px; margin-left: 40px; float: left; height: 165px;">
					
					<img width="480" height="145" src="/images/facebook_screenshot_thumb.jpg">
					
					<span style="font-size: 12px; display: block; line-height: 30px; text-align: center;"><a id="facebook_screenshot_link" href="javascript:void(0);">Enlarge Screenshot</a></span>
					
				</div>
				
			</div>
			
		</div>
	
	
		<?/*************************************
		<div class="action_heading clearfix">
		
			OUR NEWEST BRAND PARTNERS: <span style="background-image: url(/images/lp-partners.png); width: 254px; display: inline-block; height: 40px; margin: 10px 0 -10px;"></span>
			
		</div>
		***************************************/ ?>
		
		
		<div class="clearfix" style="margin: 20px 0;">
			
			<div style="padding-left: 70px; width: 220px; margin-right: 20px; position: relative; font-size: 12px; float: left;">
				
				<h6 style="padding-bottom: 3px; border-bottom: solid 1px #e5e5e5;">Leila Talibova, <span style="font-size: 11px; color: #4d4d4d;">HomeLife Victory Realty Inc.</span></h6>
				<p>My friends and previous colleagues noticed my Facebook updates right away. In only my first week using <?=COMPANY_NAME;?>, I received a call from a family friend who said she'd seen my Facebook and wanted list their home with me! They loved when I said I'd use <?=COMPANY_NAME;?> to market their home - it sold in just days!</p>
			
				<img width="50" height="50" class="icon leila" src="/images/blank.gif">
				
			</div>
			
			<div style="padding-left: 70px; width: 220px; margin-right: 20px; position: relative; font-size: 12px; float: left;">
				
				<h6 style="padding-bottom: 3px; border-bottom: solid 1px #e5e5e5;">Shaun Nilsson, <span style="font-size: 11px; color: #4d4d4d;"><?=COMPANY_NAME;?> Founder</span></h6>
				<p>When I began in real estate, I didn't have any clients or leads. I began asking more established agents if I could post their listings on my Facebook Wall to attract buyers. What a revelation! I was able to make $165,000 in commissions by doing this - in my first year!  That's where <?=COMPANY_NAME;?> was born.</p>
			
				<img width="50" height="50" class="icon shaun" src="/images/blank.gif">
				
			</div>
			
			<div style="padding-left: 70px; width: 220px; position: relative; font-size: 12px; float: left;">
				
				<h6 style="padding-bottom: 3px; border-bottom: solid 1px #e5e5e5;">Inesa Minaj, <span style="font-size: 11px; color: #4d4d4d;">Century 21</span></h6>
				<p>I am a real estate agent that is based out of Halifax. I use <?=COMPANY_NAME;?> for my real estate business and love it! I have gotten quite a few comments from my clients that they really enjoy reading the articles too. </p>
			
				<img width="50" height="50" class="icon inesa" src="/images/blank.gif">
				
			</div>
				
		</div>
	
		<div class="action_heading bottom clearfix">
		
			<a href="<?=$this->member_join_url?>" class="uiButton large right" style="width: 600px; margin: 0 200px 0; padding: 0px;">Start Your 30 Day, Risk-Free Trial.</a>
			
		</div>
	
	</div>
	
</div>

<div class="grid_12">
    <div style="padding: 0;" class="box white clearfix">
        <h1 style="text-align: center; margin: 0; line-height: 60px; color: #666666">Need help getting started? &nbsp;&nbsp;Call us at <span style="color: #333333;">1-888-712-7888</span>
        	<?/* or <a href="#">Live Chat Now!</a>*/?></h1>
    </div>
</div>


<div class="grid_12" style="margin-top: 40px;">
	
	<center>
	<table>
		<tr>
			<td style="valign: middle; padding: 8px;"><a href="/index/guarantee"><img src="/images/lp-guarantee.png"></a></td>
			<td style="valign: middle; padding: 8px;"><a href="http://www.royallepage.ca/"><img src="/images/lp-royal.png"></a></td>
			<td style="valign: middle; padding: 8px;"><a href="http://www.techvibes.com/blog/cityblast-enables-canadian-real-estate-agents-to-build-their-business-through-social-media-2012-08-03"><img src="/images/lp-techvibes.png"></a></td>
			<td style="valign: middle; padding: 8px;"><a href="http://www.realestatetalkshow.ca/"><img src="/images/lp-talkshow.png"></a></td>
		</td>
	</table>
	</center>
	
</div>


<div style="display: none;">
	
	<div id="clientfinder_video_popup" style="position: absolute;">

		 <div id="clientfinder_video"></div>

	</div>
	
</div>

<div style="display: none;">
	
	<div id="facebook_screenshot">

		 <img src="/images/facebook_screenshot.jpg" width="1060" height="714" />

	</div>
	
</div>


		


<script type="text/javascript" src="/js/swfobject.js"></script>
			
<script type="text/javascript">

	$(document).ready(function(){
		//fix position of clientfinder div for IE7
		//if($.browser.msie && $.browser.version < 8){
		//	$('#clientfinder_video_container').css({
		//									position: 'relative',
		//									left: '-40px',
		//									top: '-20px',
		//									marginBottom: '-20px'
		//								});
		//}
		
		$('#clientfinder_video_btn').click(function(event){
			event.preventDefault();
			$.colorbox({width:"703px", height: "475px", inline:true, href:"#clientfinder_video_popup"});
			show_clientfinder_video();
		});
		
		$('#facebook_screenshot_link').click(function(event){
			event.preventDefault();
			$.colorbox({inline:true, href:"#facebook_screenshot"});
			show_blast_video();
		});
		
		$('#how-it-works-btn').click(function(event){
			event.preventDefault();
			$('#how-it-works').slideDown();
		});
	
	});

	var flashvars = {};
		flashvars.file = "/video/ClientFinder.f4v";
		flashvars.skin = "/video/mediaplayer/cb/cb.xml";
		flashvars.smoothing = true;
		flashvars.stretching = "exactfit";
		flashvars.autostart = true;
		flashvars.volume = 100;
	
	var params = {};
		params.menu = false;
		params.allowfullscreen = true;
		params.allowscriptaccess = true;
		params.wmode = "opaque";
	
	var attributes = {};
		attributes.id = "clientfinder_video";
		attributes.name = "clientfinder_video";
		
	function show_clientfinder_video(){
		swfobject.embedSWF("/video/mediaplayer/player.swf", "clientfinder_video", "620", "380", "9.0.0","/scripts/mediaplayer/expressInstall.swf", flashvars, params, attributes);
	}

	setTimeout(function(){var a=document.createElement("script");
	var b=document.getElementsByTagName("script")[0];
	a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/0184.js?"+Math.floor(new Date().getTime()/3600000);
	a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
	
</script>

<?php echo $this->partial('common/fb_conversion_lp.html.php'); ?>
