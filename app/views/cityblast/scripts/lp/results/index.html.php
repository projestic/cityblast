<form action="/sample/index" method="POST">
<input type="hidden" name="locality" value="" id="locality" /><? /* locality == city */ ?>
<input type="hidden" name="administrative_area_level_1" value="" id="administrative_area_level_1" /><? /* administrative_area_level_1 == state/province */ ?>
<input type="hidden" name="country" value="" id="country" />

	<div class="contentBox dashboardContent" id="step1">
		<div class="contentBoxTitle">					
			<h1>Turn Your Fanpage Into An EXPLOSIVE Growth Engine For Your Real Estate Business.</h1>
		</div>
		<div class="stepBox step1">
			<div class="stepContent clearfix" style="margin-top: 30px;">

				<p class="fbPostsAfterText arrowsAboveLeft" style="margin-bottom: 12px !important;">Tell Us Where You Work!</p>
		
				<div style="overflow: hidden;">
					<ul class="uiForm full">
						<li class="clearfix">
							<ul>
								<li style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; width: 100%; padding: 0;" class="field cc clearfix">
								
									<input type="text" style="border: 1px solid rgb(28, 154, 218) ! important; background-color: rgb(245, 250, 252) ! important; float: left; box-sizing: border-box; width: 80%; height: 30px ! important;" class="geocomplete uiInput" name="location_input" id="defaultpubcity" placeholder="Enter a location" autocomplete="off">								
									<a onclick="upfrontsettings.submit();" href="#" class="uiButton" style="margin-left: 2%; margin-bottom: 10px; display: inline-block; float: left; padding: 4px 0px; box-sizing: border-box; width: 18%; height: 30px ! important;">Try It!</a>
								
								</li>
							</ul>
						</li>
					</ul>
				</div>

				<p class="fbPostsAfterText arrowsAsideRight">Click Here To Watch The Magic!</p>

			</div>
		</div>
	</div>
</form>

	
<style>

.uiForm .field, .uiForm .fieldname {
	padding-left: 0px!important;
}
	
</style>	


<script>

	$(function(){

		var city 			= null;
		var country_code 	= null;	
		var region 		= null;
			
		google.load("search", "1.x", {callback: initialize});
		
		function initialize()
		{
			if (google.loader.ClientLocation)
			{
				var lat = google.loader.ClientLocation.latitude;
				var long = google.loader.ClientLocation.longitude;
				
				city 		= google.loader.ClientLocation.address.city;
				country 		= google.loader.ClientLocation.address.country;
				region 		= google.loader.ClientLocation.address.region;
				country_code 	= google.loader.ClientLocation.address.country_code;
				
				$("#locality").val(city);
				$("#administrative_area_level_1").val(region); //State/Province
				$("#country").val(country);
				
			}
			else 
			{ 
				//Random default
				city 		= "Chicago";
				region 		= "IL";
				country_code 	= "USA";		
			}

			$(".geocomplete").geocomplete({
				details: "form",
				location: city + ", " + region + ", " + country_code,
			}).bind("geocode:result", function(event, result) {

				var country = '';
				var state   = '';
				var city    = '';
				
				$.each(result.address_components, function (ix, item) {
					if ($.inArray('locality', item.types) > -1) {
						city = item.long_name;
					} else if ($.inArray('administrative_area_level_1', item.types) > -1) {
						state = item.long_name;
					} else if ($.inArray('country', item.types) > -1) {
						country = item.short_name;
					} 
				});

				if (state == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>We need your city and state/province to continue.</p>"});
					return false;
				}

				if (city == '') {
					$.colorbox({href:false,innerWidth:420,initialWidth:450,initialHeight:200,html:"<h3>Oh Dear.</h3><p>We need your city to continue.</p>"});
					return false;
				}


			})
			.bind("geocode:multiple", function(event, status){
				console.log('multiple');
			}) 
							
		}

	});
	
</script>