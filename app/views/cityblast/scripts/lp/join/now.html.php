<style type="text/css">
.container{
	min-width: 320px;
}
@media (min-width: 1200px){
	.container,
	.span12{
		width: 1024px;
	}
}
</style>


<div class="row-fluid">
	<div class="span6" style="margin-bottom:5px;"><img style="margin-left: -9px;" src="/images/city-blast_logo.png"></div>
	<div class="span6">
		<ul class="inline giveUsCall pull-right">
			<li>
				<div class="bold">Start Today!</div>
				<div>Give Us a Call</div>
			</li>
			<li>
				<div class="bold">1-888-712-7888</div>
				<div>9am to 5pm EST</div>
			</li>
		</ul>
	</div>
</div>


<div class="vovoPage">
	<div class="row-fluid offer">
		<div class="span9">
			<h2>Get the Marketing Savvy of an In-House Social Media Assistant – Without the Enormous Cost!</h2>
			<p>CityBlast’s Social Experts guarantee your Real Estate business is never embarrassed by a neglected Twitter, Facebook or LinkedIn account again.</p>
			<h1>Special Price <span class="price"><sup class="dollar">&dollar;</sup>49<sup class="number">99</sup><sub class="unit">/ month</sub></span>
				
				<a id="watchDemo" href="/views/cityblast/scripts/lp/make-money/video.html.php" class="btn pull-right btn-large watchDemo"><span></span>Learn More</a>
				</h1>
		</div>
		<div class="span3">
			<img src="/images/shaun-nilsson-portrait.png" />
		</div>
		<span class="offerTag"></span>
	</div>
	<div class="tryFree row-fluid">
		<p class="pull-left">Begin Your <span>Free 30-Day Trial,</span> and get our Social Experts working for you.</p>
		<a href="/lp/join/now/join" class="pull-right btn btn-success btn-large">Try Us Free!</a>
	</div>
	<div class="contentWrapper">
		<img src="/images/vovo-content-bg.jpg" class="contentBgImg" />
		<div class="content">
			<div class="row-fluid whySocialMedia">
				<div class="span7">
					<ul>
						<li class="searches">
							<div class="pull-left iconImg">
								<img src="/images/vovo-search-icon.png" />
							</div>
							<div class="desc">
								<h3>Over 90% of home searches begin online</h3>
								<p>That means over 90% of your prospects are starting their real estate journey without your company. You must stay active and not miss out on these clients!</p>
							</div>
						</li>
						<li class="timeSpend">
							<div class="pull-left iconImg">
								<img src="/images/vovo-time-spend-fb-icon.png" />
							</div>
							<div class="desc">
								<h3>people spend 3x more time on facebook vs. any other site</h3>
								<p>If you’re going to keep in front of your network consistently and look professional, then social media is the absolute best place to do so!</p>
							</div>
						</li>
						<li class="expertsRecommend">
							<div class="pull-left iconImg">
								<img src="/images/vovo-expert-advice-icon.png" />
							</div>
							<div class="desc">
								<h3>experts recommend spending 1 hour every day on social media marketing</h3>
								<p>Who has an hour every day?! Let our experts do the heavy lifting for you, and keep you looking fresh and up-to-date on your Social accounts.</p>
							</div>							
						</li>
					</ul>
				</div>
				<div class="span5 workForYou">
					<div>
						<h2>Let our experts<br/><span class="green">do the work for you.</span></h2>
						<p>We’re like your in-house Virtual Social Media Assistant.</p>
						<ul>
							<li>We manage your social accounts, marketing your company</li>
							<li>Relevant real estate articles, videos and information</li>
							<li>Only trusted sources like HGTV, CNN and WSJ</li>
							<li>As often as you choose – up to 7 posts/week</li>
							<li>We update Facebook, Twitter and LinkedIn</li>
							<li>One low cost vs. hiring someone locally</li>
							<li>Check out these <a href="#" style="text-decoration: underline" id="show_posts">great example posts</a> that our dedicated professionals have written</li>
						</ul>
						<div class="dontMiss">
							<a href="/lp/join/now/join" class="btn btn-success btn-large"><span>Start Your</span><br/>Free 30-Day Trial</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="commentContainer">
					<div class="comment">
						<div class="pull-left userImg">
							<img src="/images/user-thumb-img2.jpg" />
						</div>
						<div class="userComment">
							<h5>Great Service</h5>
							<p class="desc">"I use CityBlast to help me market. Not only are they the heart of my online platform, but also help me to sell my clients’ listings quickly, and to achieve excellent prices. CityBlast is an incredible service for both beginners and top agents, and I highly recommend them."</p>
							<p class="userInfo"><span>Sheree Cerqua /</span> Ontario’s #1 Individual Agent - Royal LePage</p>
						</div>
						<span class="leftArrow"></span>
					</div>
					<div class="comment">
						<div class="pull-left userImg">
							<img src="/images/user-thumb-img.jpg" />
						</div>
						<div class="userComment">
							<h5>Awesome Experts</h5>
							<p class="desc">"My friends and previous colleagues noticed my Facebook updates right away. In only my first week using CityBlast, I received a call from a family friend who said she’d seen my Facebook and wanted to list their home with me. The experts are awesome!"</p>
							<p class="userInfo"><span>Leila Talibova /</span> Top Rookie Agent - Home Life Realty</p>
						</div>
						<span class="leftArrow"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tryFree row-fluid">
		<p class="pull-left">Begin Your <span>Free 30-Day Trial,</span> and get our Social Experts working for you.</p>
		<a href="/lp/join/now/join" class="pull-right btn btn-success btn-large">Try Us Free!</a>
	</div>
</div>



<div style="display: none;">

	<div id="show_posts_popup">
		
		<div class="box clearfix">
	
			
			<h3>Informational</h3>			
			<p><img src="/images/sample-informational.jpg" /></p>

			<h3>Lifesyle</h3>
			<p><img src="/images/sample-lifestyle.jpg" /></p>

			<h3>Listings</h3>
			<p><img src="/images/sample-listing.jpg" /></p>			
																			
		</div>
		
		<a href="<?=$this->group_name;?>/<?=$this->page_name;?>/join" class="uiButton right">Start your free trial</a>

	</div>
</div>


<script type="text/javascript">
	$(function(){
		$('#watchDemo').colorbox({href: '//www.youtube.com/embed/OrrGZk2ogwU?autoplay=1', iframe: true, width: '600', height: '500', innerWidth: '99%' , innerHeight: '99%'});
		
	});


	$(document).ready(function() 
	{	

		$("#show_posts").colorbox({

			width:"600px", 
			height: "900px", 
			inline:true, 
			href:"#show_posts_popup",
			onOpen:function(){ $("#videoframe").css("visibility", "hidden"); }, 
			onClosed:function(){ $("#videoframe").css("visibility", "visible"); }		
		}); 
			
		
	});	
</script>

<?php echo $this->partial('common/fb_conversion_lp.html.php'); ?>
