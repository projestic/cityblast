<style>

.h2smallpad {
    color: #AA0000;
    font-family: tahoma;
    font-size: 18pt;
    font-weight: bold;
    letter-spacing: -1px;
    margin-left: auto;
    padding-bottom: 10px;
    padding-top: 10px;
    text-align: center;
}

.sidebar1 {
    background: none repeat scroll 0 0 #E5F3FF;
    border: 2px solid #0545A5;
    display: block;
    float: right;
    margin: 0 0 10px 10px;
    padding: 20px;
    text-align: left;
}

.bonus {
    background-color: #E5F3FF;
    border: 3px solid #0545A5;
    height: auto;
    margin-left: auto;
    margin-right: auto;
    padding: 10px;
    text-align: left;
    width: 550px;
}

hr.style-one 
{ 
	border: 0; 
	height: 1px; 
	width: 90%; 
	background: #333; 
	background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc); background-image: -moz-linear-gradient(left, #ccc, #333, #ccc); 
	background-image: -ms-linear-gradient(left, #ccc, #333, #ccc); background-image: -o-linear-gradient(left, #ccc, #333, #ccc); 
}	

.proof {
    background-color: #E5F3FF;
    border: 1px solid #0545A5;
    height: auto;
    margin: 10px auto;
    padding: 10px;
    text-align: left;
}

body
{
	font-size: 16px;	
}

.highlight 
{
    background-color: #FFFF00;
    font-style: italic;
}	

.questions {
	background-color: #7FFFFF;
}


blockquote {
	margin: 15px 25px;
   	font-style: normal !important;
}

p {
    color: #000000;
    direction: ltr;
    font-family: Arial;
    font-size: 12pt;
    margin-bottom: 20px;
    orphans: 2;
    text-align: left;
    widows: 2;
}
h2 {
	padding-top: 20px;
	padding-bottom: 20px;
	color:#A00;
	letter-spacing:-1px;
	font-family: tahoma;
	font-size: 18pt;
	font-weight: bold;
}

.h2smallpad {
	padding-top: 10px;
	padding-bottom: 10px;
	color:#A00;
	letter-spacing:-1px;
	font-family: tahoma;
	font-size: 18pt;
	font-weight: bold;
	text-align: center;
	margin-left: auto;
}


</style>

<div class="row-fluid">
	<div class="span6" style="margin-bottom:5px;"><img style="margin-left: -9px;" src="/images/city-blast_logo.png"></div>
	<div class="span6">
		<ul class="inline giveUsCall pull-right">
			<li>
				<div class="bold" style="color:#0045cc">Pricing</div>
				<div>Only $49.99/month</div>
			</li>
			<li>
				<div class="bold" style="color:#0045cc">30 Day Free Trial</div>
				<div>Start using CityBlast today</div>
			</li>
		</ul>
	</div>
</div>

<!-- Main hero unit for a primary marketing message or call to action -->
<? /*<div class="hero-unit">*/ ?>
<div style="box-shadow: 0px 0px 30px -1px #666; -webkit-box-shadow: 0px 0px 30px -1px #666; -moz-box-shadow: 0px 0px 30px -1px #666; margin-top: 10px;">

	<?/* background-color: #fbfbda; */ ?>
	<div class="row-fluid" style="padding-top: 8px; padding-bottom: 8px; background-color: #ffffff;">
			
		<div class="span12" style="padding-left: 32px;">	
			
			<p style="font-size: 18pt; font-weight: bold; color: #000000; padding-top: 10ppx">Did You Know...</p>
			<h1 style="color: #CC0000; padding-top: 10px; font-size: 32pt; margin-top: 0px; line-height: 32pt;">Some Of Your <strong class="highlight">Best Friends</strong> Are <strong class="highlight">Buying Real Estate</strong> Right Now?</h1>		
		</div>


		<div class="span12" style="padding-left: 58px; padding-right: 78px; padding-top: 18px; padding-bottom: 38px;">	
			
			
			<div style="background: url('/images/long-form-bg.jpg') repeat scroll center; border: 1px solid #888888; padding: 25px;">
				<h2 style="color: #181F6b; font-family: Tahoma, Geneva, sans-serif; font-size: 26pt; font-weight: bold; padding-bottom: 10px; padding-top: 0px; margin-top: 0px;" align="center">
					"Last Year I Sold 3 Houses 
					<br /> To Friends I Haven't Seen Since Highschool. 
					<br /> Thank You Facebook!"
				</h2>				


				<p>
					<em><img align="right" width="261" height="282" style="padding-left: 10px;" src="/images/diane.jpg">"I am one of those patients who <strong>cancelled my back surgery because  inversion therapy worked for me!!</strong></em>
				</p>
				
				
				<p>
					<em>I suffered with chronic back pain and sciatica for three years!  I have a herniated disc (L4-L5), had 5 epidural injections, physical therapy, chiropractc therapy...you name it!! </em>
				</p>
				
				<p>
					<em>This was my last resort before surgery... I bought my inversion table about 6 weeks before my surgery. </em>
				</p>
				
				
				<p>
					<em>
					Two weeks before I was to go "under the knife",
					<strong>my pain diminished by 90%</strong>. Good enough for me, and glad to be off anti-inflammatory medications as well. I'm a believer!!"
					</em>
				</p>
				
				<p>
					<em>- Diane T.</em>
				</p>
				
				<div class="clearfix"></div>

	
			</div>
			
		</div>



		<div class="span12" style="padding-left: 8px; text-align: left;">
			<p>
				<strong>From:</strong> Shaun Nilsson <em>CFT, CPT, CPRS, CSPN</em>
				<br><strong>To:</strong> Real estate agents looking for some answers...
				<br><strong>Date:</strong>
				
				<script>
				var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
				var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
				function getthedate(){
				var mydate=new Date()
				var year=mydate.getYear()
				if (year < 1000)
				year+=1900
				var day=mydate.getDay()
				var month=mydate.getMonth()
				var daym=mydate.getDate()
				if (daym<10)
				daym="0"+daym
				var hours=mydate.getHours()
				var minutes=mydate.getMinutes()
				var seconds=mydate.getSeconds()
				var dn="a.m."
				if (hours>=12)
				dn="p.m."
				if (hours>12){
				hours=hours-12
				}
				if (hours==0)
				hours=12
				if (minutes<=9)
				minutes="0"+minutes
				if (seconds<=9)
				seconds="0"+seconds
				//change font size here
				var cdate=""+dayarray[day]+", "+hours+":"+minutes+" "+dn+""
				if (document.all)
				document.all.clock.innerHTML=cdate
				else
				document.write(cdate)
				}
				if (!document.all)
				getthedate()
				function goforit(){
				if (document.all)
				setInterval("getthedate()",1000)
				} 
				</script>
				
				<span id="clock"></span>
			</p>
			
			<p><strong>Dear back pain sufferer,</strong></p>
									
		</div>


		<div class="span7" style="padding-left: 38px; text-align: left;">

			<p><em><span style="font-size: 28pt; color: #cc0000;">A</span>fter  <strong>desperately searching</strong> for any back pain solution that does the job... spending hundreds of dollars at your doctors</em> on services which only give you very temporary relief... and being  force-fed harmful drugs because <strong>you have no other choice</strong>...</p>
			<p>... it's <u>no wonder</u> your hope for finding pain relief is beginning to disappear!</p>
			<p>The <strong>constant, nagging pain is beginning to steal  your lifestyle</strong> and any smidgen of hope for pain-relief is becoming  a pipe-dream.</p>
			<p><em>I'm here to tell you - it's <u>not</u> your fault.</em>.. and there <u>IS</u> hope. </p>
			<p><strong>I promise.</strong></p>
			<p>If you're reading this page it shows me you're serious about eliminating your back pain. You've probably heard both good and bad things about inversion tables... including many different rumors and speculations which may be swaying your impressions one way or another.</p>
			<p>So in the next few minutes... I'm going to pull back the velvet curtain and reveal the <u><strong>facts</strong></u><strong> about inversion tables. </strong></p>
			<p>Forget everything you've read in forums, what your family and friends told you, and even  what your doctors told you.</p>
			<p>You deserve the<strong> <u>truth</u>.</strong> </p>
			<p>More specifically you deserve to know whether or not inversion tables are right for <strong>you</strong>. After all, that's the real reason you're here isn't it?</p>
			
		</div>
		
		<div class="span4">
			
			<div style="border: 2px solid #0545a5; background: #e5f3ff; padding: 20px;">
				
				<span><span class="h2smallpad">Who Is Shaun Nilsson? </span></center>
					
					
				<img src="/images/shaun-nilsson-portrait.png" alt="Shaun Nilsson" align="right" style="padding-top: 15px;" /><br />
				
				
				<p>Jesse Cannone is one of the most highly recognized and most respected experts for treating back pain naturally in the world. </p>
				
				<p>
				Since creating "The Healthy Back Institute" in 2001, Jesse has
					<strong>
					personally helped over
					<u>115,385 people in 85 countries</u>
					free themselves of the shackles of back pain
					</strong>
				with all- natural, drug-free, innovative and sometimes controversial back-pain relieving methods.
				</p>
				
				<p>
				His methods allow you to finally experience what it's like to live without back-pain...
				
					<strong>
					<u>without</u>
					harmful drugs or dangerous surgery.
					</strong>
				
				</p>

				<p>
				He's been featured in dozens of newspapers and magazines such as Miss Fitness, Natural Bodybuilding, and Visage... numerous radio programs... thousands of websites and
					<strong>
					has even appeared as an
					<u>exclusive back-pain expert on NBC!</u>
					</strong>
				</p>				
				
			</div>
			
		</div>
		
				
		<?/*
		<div class="span12" style="padding-left: 38px; text-align: left;">
			<hr class="style-one clearfix" />
		</div>
		*/ ?>


		<div class="span12" style="padding-left: 38px; padding-right: 58px; text-align: left;">
	
			<h2><center>Myth #1 - Inversion Tables Don't Work</center></h2>
			<p> If someone tells you this... they're simply <u>ignorant to the cold-hard facts</u>. </p>
			<p>I've read many forums around the Internet and have talked with people somehow claiming their are no scientific studies proving the effectiveness of inversion tables.</p>
			<p>... that's a <u><strong>myth</strong></u>. Pure and simple.</p>
			<p>The fact is, those people simply haven't done their homework and have unfortunately seen their hopes and dreams washed away in a flood of disgust.</p>
			<p>But that's not you!</p>
			<p><strong>Here is <u>scientific proof</u> that inversion tables work</strong>...</p>
		
		</div>
		
		<div class="span2"></div>
		
		<div class="span8">
			<div class="proof"> 
				<p><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="45" height="45" align="left" class="img" /></strong>One study consisted of 175 patients experiencing back pain bad enough that they were unable to work. After just 8  inversion treatments - 88.6%  were able to return to their jobs full-time. <strong>This simply means that just 8 inversion treatments made a <u>significant decrease in the amount of pain in 88.6% of those people</u></strong> </p>
				<p><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="45" height="45" align="left" class="img" /></strong>A study conducted in 1978 testing EMG activity (indicating the amount of muscle pain in participants) showed a <strong class="highlight">35% decrease in pain - in as little as just 10 seconds</strong> after becoming inverted. This proves you can experience near-instant relief... as well as the long-term health benefits you'll read about a little later on.
				  </p>
				</center></p>
				<p><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="45" height="45" align="left" class="img" /></strong>Another study found that using an inversion table just 1-2 times per day on average resulted in a <strong>33% decrease in sick days due to pain back</strong>... plus a <strong>noticeable drop in pain throughout the entire year.</strong> </p>
				<p><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="45" height="45" align="left" class="img" /></strong><strong><img src="images/newcastle-master-sm.png" width="200" height="66" align="right" /></strong>A recent study done at New Castle University found that  <strong>70.5% of patients who performed inversion therapy were able to cancel their scheduled back surgeries. </strong></p>
			</div>
				
			

		
		</div>
		
		<div class="span2"></div>



		<div class="span12" style="padding-left: 38px; padding-right: 58px; text-align: left;">

			<p>Here are the <u>true facts.</u></p>
			<blockquote>
			  <p>... Virtually every study ever done  has shown a<strong> <u>noticeable decrease in pain</u> for the people using it. </strong></p>
			  <p><strong>... It's been used since 3,000 B.C.</strong> to re-balance the body, help increase circulation, stimulate the brain, enhance glandular system functioning, and relieve pressure on the abdominal organs.</p>
			  <p><strong>... Hippocrates, the &quot;Father of Medicine&quot;, created his own type of inversion therapy</strong> by hoisting his patients on a ladder with a series of ropes and pulleys to harness gravity in an effort to stretch his patients and relieve their ailments. </p>
			</blockquote>
			<p>... and many more fascinating facts you'll discover as you continue reading.</p>

		</div>



		<div class="span12" style="padding-left: 38px; text-align: left;">
			<hr class="style-one clearfix" />
		</div>

		
		<div class="span12" style="padding-left: 38px; padding-right: 58px; text-align: left;">		
			
			<h2><center>Myth #2 - Inversion Tables <u>Won't Work For Me</u></center></h2>
			<p>This is a question we've gotten literally hundreds of times, so you're not alone. And you're <u><strong>smart</strong></u> to be skeptical! There are way too many &quot;shucksters&quot; out there trying to force-feed you claims they can't back up... and products that simply don't do what they promise.</p>
			<p>However in the unique way in which inversion therapy heals your spine ... it really <u>does</u> help virtually anyone get at least some type of relief due to the fact that it dramatically <strong>promotes healing and regeneration of damaged discs</strong>... decompresses the spine... and <strong>re-hydrates your gravity-worn discs </strong>which creates a bigger &quot;cushion&quot; between your discs.</p>
			<p>Will some people experience better and faster results than others? <strong>Sure. Just like anything else.</strong></p>
			<p>However it's very, very unlikely that you won't see at least a significant decrease in pain as well as other improvements like increased flexibility and circulation. And that's the absolute worst case scenario!</p>
			<p><strong>Below is a list of different conditions that inversion tables can help alleviate.</strong></p>
			
		</div>
		
		
		
		<div class="span2"></div>					
		
		<div class="span8">
			<center><table width="100%" border="1" cellspacing="10" cellpadding="10">
			  <tr>
			    <td scope="col"><p><img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Arthritis of the Spine<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Degenerative Disc Disease<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Facet Joint Syndrome<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Fibromyalgia<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Forward Head Posture<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Herniated Disc<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Lower Back Pain<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Neck Pain<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Pinched Nerve</p>
			      <p><img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Piriformis Syndrome</p>
			    
			    </td>
			    <td scope="col"><p><img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Sacroiliac Joint Dysfunction<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Sciatica<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Scoliosis<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Somatic Pain and Your Back<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Spondylolisthesis<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Spinal Stenosis<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Types of Pain<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Upper Back Pain<br />
			        <img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />Vertebral Compression Fracture</p>
			      <p><img src="http://www.losethebackpain.com/images/nubax/blue-check.png" alt="" width="20" height="30" hspace="0" vspace="0" />... and many more!</p>
			     
			     </td>
			  </tr>
			  </table></center>

		</div>
		



		<div class="span12" style="padding-left: 38px; padding-right: 58px; text-align: left;">	
						  
			<center><h2>Myth #3 - Inversion Tables <u>Aren't Safe</u></h2></center> 
			<p>This <u><strong>MAY</strong></u> be true... depending on where you get your inversion table and how it's constructed.</p>
			<p>You see...  inversion tables you get at places like Walmart, Ebay and other &quot;discount&quot; stores are made very quickly with either plastic or incredibly low-quality metals. They're made very cheap... and &quot;cheap&quot; comes with a very high price if (and when) it breaks while you're on it!</p>

			
			<p> I would <u>n<em>ever</em></u> recommend you buy a &quot;cheap&quot; inversion table for that specific reason.</p>
			<p>When you buy a quality-made inversion table... their is virtually no risk. The chance of a <u>high-quality inversion table</u> made out of <u>high carbon steel</u> breaking is probably <strong>less than the chance you take of your car wheel suddenly falling off while driving</strong>... and you drive that everyday... don't you?</p>
			<p>The inversion table you end up buying should be made with <strong><u>NOTHING other than high carbon steel. </u></strong></p>
		
		</div>
		

		<div class="span12" style="padding-left: 38px; padding-right: 58px;  text-align: left;">
		
			<h2 align="center">10  Ways Inversion Therapy Will Create A <u>Pain-Free<br /></u> And More <u>Exciting</u> Life For You!</h2>
			
			<p>Many people mistakenly think inversion tables only reduce back pain. Yet, as you'll see in a second, inversion tables help <strong>create a new body</strong>... <strong>a new <u>YOU</u></strong>... in dozens of ways.</p>
			<p>Here are 10.</p>
			
			<blockquote>
			  <p style=""><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" width="30" height="30" align="left" class="img" />Convenience</strong> - You won't have to take nearly as many trips to the chiropractor, 	physical therapist, etc. because you'll get the same relief &ndash; in 	the comfort of your own home. <span style="font-family: Arial; font-size: 12pt;">In fact many people think of their inversion table as an &quot;in-home&quot; chiropractor!<br /></span></p>
			  <p><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="30" height="30" align="left" class="img" /><span class="highlight">Sleep Through The Night In Peace</span></strong> - Imagine how amazing your days will go as you sleep peacefully each night, then leap out of bed each morning and plant both feet on the floor... without any pain! Inversion tables let you sleep like a baby due to the pain reduction you'll receive... </p>
			  <p><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="30" height="30" align="left" class="img" />Reduce Shrinking As You Age</strong>! - Inversion tables take the gravitational pressure off your spine, allowing it to rebuild itself and maintain its strength... <strong>it slows down the &quot;shrinking&quot; process</strong> and helps  prevent the infamous &quot;hump&quot; you commonly see in elderly folks.<br />
			  </p>
			  <p><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="30" height="30" align="left" class="img" />Improved Circulation</strong> &ndash; As we age our bodies  can't produce enough blood to move 	through our system and because of that... we experience poor 	circulation. Inversion therapy  improves blood flow throughout 	your entire body... flooding it with nutrients and oxygen and <strong>skyrocketing your energy levels.</strong><br />
			  </p>
			  <p align="left"><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="30" height="30" align="left" class="img" />Improvement In 	Mobility</strong> &ndash; Taking 3 times as long to do normal movements such 	as getting out of a chair or wiggling  out of your car is quite 	embarrassing. As you strengthen and stretch... your muscles, joints and ligaments will  become stronger and more flexible... <strong>so you can do  things you once loved!</strong></p>
			  <p align="left"><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="30" height="30" align="left" class="img" />Create A Brand New Body</strong> - Many people who use an inversion table say they feel like they <strong>have a brand new body </strong>after just a few days or weeks of use. This is because gravity no longer wears your muscles and joints down like before... and feels amazing for people who deal with stiff, aching joints.</p>
			  <p align="left"><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="30" height="30" align="left" class="img" /><span class="highlight">Save Money</span></strong>&ndash; 	Chiropractors are getting more expensive, pain medications are 	dangerous and expensive... and spinal surgery is  frightening and unnecessary. My 	solution? Use an inversion table and use pain meds and chiropractors on an &quot;as-needed&quot; basis.<br />
			  </p>
			  <p align="left"><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="30" height="30" align="left" class="img" />Work Without 	Distractions</strong> &ndash; Let's face it, the market place out there isn't 	growing. Jobs are getting harder to keep... and if you can't focus 	due to pain &ndash; your boss <u>will</u> eventually  notice. Reduce the 	pain, watch your attention go through the roof, and feel secure that your job is more safe!<br />
			  </p>
			  <p align="left"><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="30" height="30" align="left" class="img" /><span class="highlight">Get Your Life Back</span></strong> -  Being unable to play your favorite sports, play with your children or grandchildren... or simply have a good time with their friends and family is heartbreaking. Imagine the triumph you'll feel as you're able to begin doing the things you used to love but can't anymore because of back pain!<br />
			  </p>
			<p align="left"><strong><img src="http://www.losethebackpain.com/images/Inversion/blue-yellow-checkmark.png" alt="" width="30" height="30" align="left" class="img" /><span class="highlight">Get Neck Pain 	Relief!</span></strong> - Finding relief for neck pain is incredibly hard and 	unbelievably frustrating. Most neck pain is caused by tense muscles 	in the back and shoulders which cause the neck muscles to cramp. Inversion therapy literally <strong>&ldquo;relaxes the pain away&rdquo;... leaving 	you pain free and happy!</strong></p>
			</blockquote>

		</div>

		<div class="span12" style="padding-left: 38px; padding-right: 58px; text-align: left;">				
			<p>I'm sure you understand just how important it is to invert your body and take the pressure off your spine and every other joint in your body.</p>
			<p>Here's the problem.</p>
			<p>Inversion tables are a dime a dozen. </p>
			<p>There are literally dozens, maybe even hundreds of versions out there to try... yet 99% of them are worthless piles of junk sold by people trying to make a quick buck.</p>
			<p>For nearly a decade, we've been researching and studying Inversion tables. We've taken all the best features and combined them into what I consider to be the best inversion table on the market today.</p>
			<p> It contains the <u><strong>most</strong></u> and <strong><u>best</u> features</strong> of the most top-of-the-line inversion tables on the market... is <strong>made with the absolute most high-quality steel you can find for <u>undeniable safety</u></strong>... and is about <strong><u>25% lower in cost!</u></strong></p>

		</div>
		



		<div class="span3" style="padding-left: 38px; text-align: left;"></div>
		<div class="span6" style="border: 3px dashed black;  width: 600px; height: auto; margin-top: 30px; margin-left:auto; margin-right:auto; padding: 20px">
		
			<h2 align="center" class="h2smallpad">Regularly <s>$399</s>...Today Just <span class="highlight">$299</span><br />
			Plus <span class="highlight"><u>FREE Shipping!</u></span>*</h2>
			<p>*Free shipping US only - excludes Alaska, Hawaii and Puerto Rico. To order from these states please call 1-800-216-4908. Also includes a 30-day iron-clad, no hassle money-back guarantee.</p>
			<center>
			<a href="#order"><img src="http://www.losethebackpain.com/images/Inversion/AddToCartJesse.png" alt="" width="399" height="145" border="0" class="img" /></a>
			</center>
			
		</div>		
		<div class="span3"></div>	
				
		

		<div class="span12" style="padding-left: 38px; text-align: left;">
			<hr class="style-one clearfix" />
		</div>


		<div class="span12" style="padding-left: 38px; padding-right: 68px; text-align: left;">
		
		
		      
		    <span class="h2smallpad"><center>"I slept my first pain-free night in FIVE YEARS!"</center></span><br>
		        
		        <p>"I am a diagnostic radiologist who abused his back through sports, hard work and horses for years. About seven years ago, I began to experience back and left leg pain that progressed rapidly. An MRI revealed a lot of degenerative changes in my lumbar spine with two intervertebral discs that were herniated and for all practical purposes 'worn out.' Five years ago I began to experience numbness in some of my left toes with sciatica in the left hip and leg, which progressed to constant pain, loss of feeling and then loss of motor function in my left toes.<br><br>
		          
		          A friend who is a sales rep for radiographic equipment, whom I have known for 15 years, told me he had become pain free in about two months using an inversion table. I ordered one that day. In one week of use, twice to three times per day, the numbness in my toes improved 100%. During the second week, I slept my first pain free night in five years."        </p>
		        <p><strong><em>- Ken Hamilton, M.D.</em><br>
		        </strong></p>
		        <hr>
		        <p><center><span class="h2smallpad">Pain Free For The First Time That I Can Remember... </span> </center></p>
		        <p><img src="http://www.losethebackpain.com/images/inversion-robert-transportation.jpg" alt="" style="margin-left: 10px;" align="right" border="0" height="193" width="150" />My wife bought me your inversion chair quite a few months ago because of back pain I have suffered most of my life. I thought it was probably just another one of those things you spend money on, but they </p>
		        <p>don't do you any good. After reading your newsletters and watching your videos I decided to give it a try. After about a week I noticed that my back didn't hurt anymore and I can say that I am virtually <strong>pain free for the first time that I can remember.</strong>
		          <br><br>
		          I would like to thank you very much for opening my eyes to a system that really does work like you say it does. I can't begin to tell you how much I appreciate what you have done for me by making me aware of the benefits of inversion therapy. Thank you again and I will be reading your newsletters regularly for all the benefits they (you) provide. Forever grateful!"</p>
		        <p>  <strong><em>- Robert O. <br />
		          Transportation Supervisor, Auburn, Ca.</em>        <br>
		        </strong></p>
		        <hr><br>
		  <p> <center><span class="h2smallpad">
		    &quot;The First Time I Tried It... The Pain Down My Leg<br />
		Almost Completely Disappeared!
		</span></center></p>
		  <p>"In May, I herniated the L5-S1 disc. My doctor recommended drugs and pain management, but the meds made me sick and I had a several week wait to go to pain management. The pain was intolerable, and I was having a hard time being pleasant at work. Home was a whole other story! I was telling a neighbor about it, and she suggested I come to her house and try her inversion table. </p>
		  <p><strong>The first time I tried it, the pain down my leg almost completely disappeared.</strong><br><br>
		    
		    I borrowed her table and as long as I use it, I feel good. I have gotten better and better. I take Aleve when I do have pain, cancelled pain management and am my sweet old self at home and work. Since I couldn't keep my neighbor's table forever, I have purchased my own and have a couple of friends anxious to try it. I am sooooo happy. I am 60 years old and never expected to feel this well again."        </p>
		  <p><strong><em>- J. Grieder, The Villages, FL</em><br>
		  </strong></p>
		  <hr>
		          <p><center><span class="h2smallpad">&quot;I have gone from being bed ridden to working in the garden, cutting down trees and just doing darn well anything that I can and want too...&quot;</span><br>
		         </center> </p>
		          <p><img src="http://www.losethebackpain.com/images/inversion-mel-kaufmann.jpg" alt="" style="margin-right: 10px; padding-left: 10px;" align="right" border="0" height="150" width="200" />I have gone from being bed ridden to working in the garden, cutting down trees and just&nbsp;doing darn well anything that I can and want too.  </p>
		          <p>I have also told friends and family ow wonderful&nbsp;your system is and how it has help me not to have surgery, for that is what I was told that I needed.&nbsp;  </p>
		          <p><strong>Thanks for the pain relief."  </strong></p>
		          <p><strong><em>- Mel Kauffman</em><br>
		          </strong></p>
		          <hr>
		  <p><center>
		    <span class="h2smallpad">"After 1 week I no longer hurt and I walk, dance, garden<br>
		    and shower with NO PAIN!"<br>
		    
		  </span></center></p>
		  <p>"I am and 54 year old nurse, and have had painful Sciatica for the last 4 years. I have tried massage, acupuncture, traditional medicines and Chinese medicine. It had gotten to a point where I could not even stand in the shower without having to cry because of the pain which radiated from the left side of my buttock all the way to my ankle. My life as far as I saw it was literally gone. My garden is my pride and joy , but have not been able to work it for many years.<br><br>
		          
		          When I looked in the mirror (with no clothes on) I saw a very old woman who was so lopsided that it made me cry. Traveling was no longer an option for me, because I would have to hang on or hold on to a cart or walker. I thought to myself that I will end up in a wheel chair with in a few years.<br><br>
		          
		          I have been communicating with the Lose the Back Pain web site and they had a advertisement about the inversion table. What did I have to lose? I received the table just after Christmas 2006, I am on there twice a day for no more than 5 minutes at a time. After 1 week I am no longer lopsided, I no longer hurt and am able to walk, dance, run, garden and shower with NO pain. What a blessing inversion is. My husband is so happy to see that I am no longer in pain and smiling. I believe that I have grown a few inches which makes my clothes fit better."</p>
		  <p><strong><em>� R. Baak, Perris, CA</em><br>
		  </strong></p>
		  <hr><br>
		        
		        <p> <center>
		          <span class="h2smallpad">
		          "Got immediate relief..."
		        </span>
		        </center></p>
		        <p><img src="http://www.losethebackpain.com/images/inversion-robert-dogs.jpg" alt="" style="margin-left: 10px;" align="right" border="0" height="150" width="200">"I started with Lose The Back Pain in May 07. I do my Sciatica Exercise every other day now, and not to jinx myself but I've been one hundred percent for the last five months. I've eased back into my regular daily exercise as well.<br><br>After the first few weeks of prescribed exercise, I started <strong>using an inversion table and got immediate relief.</strong> Thank you again!"</p>
		        <p><strong><em>- Robert R.</em><br>
		        </strong></p>
		<hr><br>
		        
		        <p> <center><span class="h2smallpad">
		          "Assembly was surprisingly easy..."
		        </span></center></p>
		        <p>"We recently received the inversion table. Assembly was surprisingly easy, instructions were great, and the quality of this product is amazing for the cost we paid for it. We're looking forward to experiencing the benefits. Thanks for an outstanding product in a time when so many others are cheaply made, yet overpriced. Yours is the way it should be."&nbsp;</p>
		        <p><strong><em>- S. Barnhart, Lititz, PA</em><br>
		        </strong></p>
		        <br>
		        <hr>
		        <p><br>
		          "The reason I haven't bought an inversion table from you is that I already have one. I absolutely&nbsp;love it. I also love the Trigger Point System which I purchased from you a while ago. &nbsp;It's brilliantly&nbsp;simple, and simply brilliant!!!&nbsp;I love all the great tips you send via your emails. I look forward to receiving more in the future.&nbsp;Your help has certainly made my back injury much easier to handle."
		          <br>
		          </p>
		        <p><em>Thanks again,</em></p>
		        <p><strong><em>Sally B, Australia&nbsp;</em><br>
		        </strong></p>
		        <hr>
		        <p><br>
		          "I signed up for your lose the back pain system a year ago and with the inversion table I also got from you, I've seen dramatic improvements in my lifestyle and things I can do again because I'm not in pain. I'm a true believer in your system and tell all that want to listen about it. Keep up the good work!"&nbsp;          </p>
		        <p><em><strong>- Chris S.</strong></em><br>
		        </p>
		        <hr>
		        <h2 align="center"> <span class="h2smallpad">Bad Pain In My Lower Left Leg And<br />
		          Left Butt Cheek Have Vanished...
		          </span>
		        </h2>
		        <p>"Just a note to let you know that I have been using my inversion table, the bad pain in my lower left leg and the strong pain in my left butt cheek have vanished. I didn't respond before now because I was trying to be sure it was gone before I personally thanked you for this tremendous invention. I've done pain killers, therapy, and doctors. They said, 'Just something you'll have to live with'. Thank you and your staff."  </p>
		        <p><strong><em>- Franklin Bair</em><br>
		        </strong></p>
		        <hr>
		        <h2 align="center"><span class="h2smallpad">
		          Cancelled My Back Surgery Because <br />
		          Inversion Therapy Worked For Me!&quot;
		</span></h2>
		        <p>&quot;Interesting...I am one of those patients who <strong>cancelled my back surgery because  inversion therapy worked for me!!</strong> I suffered with chronic back pain and sciatica for three years!&nbsp; I have a herniated disc (L4-L5), had 5 epidural injections, physical therapy,&nbsp;chiropractc therapy...you name it!! </p>
		        <p>This was my last resort before surgery... I bought my inversion table about 6 weeks before my surgery. Two weeks before I was to go &quot;under the knife&quot; for microdiscectomy, my pain&nbsp;diminished by 90%&nbsp;...good enough for me, and glad to be off anti-inflammatory medications as well. I'm a believer!!&quot; </p>
		        <p><strong><em>- Diane T.</em></strong></p>
		        <hr />
		        <h2 align="center">&quot;The Inversion table is well worth the investment...&quot;<br>
		        </h2>
		        <p>"I have purchased and use your "lose the back pain" system. It got me out of trouble, out of pain and I'm glad I made the investment. I also purchased my inversion table due to your emails. I believe the inversion table is well worth the investment. I find many of your emails extremely valuable, and plan to make additional purchases."
		          <br>
		        </p>
		        <p><em>Regards,</em></p>
		        <p><strong><em>Rolf S.</em><br>
		        </strong></p>
		        <hr>
		      
		          <h2 align="center"><span class="h2smallpad">The Inversion Table Got Rid Of The Pain<br />
		            In Less Than A Month!
		            </span></h2>
		   
		        <p>"I suffered for&nbsp;5 months and I now know it was needless suffering because <strong>the inversion table got rid of the pain in less than a month!</strong> </p>
		        <p>I'm sure there are many people out there suffering who could also benefit but are reluctant to spend the money because it was not recommended by their doctor. I do not understand why chiropractors do not have this table in their office. It makes no sense."          </p>
		        <p><strong><em>- Joe M.</em><br>
		        </strong></p></td>
		      </tr>
		    </tbody></table>
		 </div>




		<div class="span12" style="padding-left: 38px; text-align: left;">
			<hr class="style-one clearfix" />
		</div>

		

		<div class="span12" style="padding-left: 38px; padding-right: 68px; text-align: left;">

			<h2 align="center">Reserve Your Premium Inversion Table Today And<br />
			I'll Throw In These <u>Exclusive</u> Bonuses!</h2>
			  <p align="center">Let's be honest with each other.</p>
			  <p align="center"><strong>You're looking for a great deal</strong> on an Inversion table which will help alleviate your back pain for good... and <strong>I would be <u>honored</u> to get your business.</strong></p>
			  <p align="center">That's why...</p>
			  <blockquote>
			    <p align="center"><img src="http://www.losethebackpain.com/images/Inversion/orange-check.png" alt="" width="25" height="34" />Not only are you getting the <strong><u>best guarantee in the industry</u></strong> (more on this in a second)...</p>
			    <p align="center"><img src="http://www.losethebackpain.com/images/Inversion/orange-check.png" alt="" width="25" height="34" />Not only are you getting a &quot;top-of-the-line&quot; inversion table for <strong><u>LESS than our competitors</u></strong>...</p>
			    <p align="center"><img src="http://www.losethebackpain.com/images/Inversion/orange-check.png" alt="" width="25" height="34" />Not only are you getting our <strong><u>1-year manufacturer warranty</u></strong>...</p>
			  </blockquote>
			  <p align="center">I'll also throw in the following bonus - just to make sure you make the right decision today!</p>
			  <div
			 align="center"></div>
			  <div class="bonus">
			    <h2 align="center" style="margin-top: 0px; padding-top: 0px;">FREE Bonus:  90 Days Personal <br />
			        1-On-1 Support! ($97 value)</h2>
			      <p>Throughout the next 90 days as you discover your brand-new back... and your <strong>brand-new <u>life</u></strong>... you'll likely have  questions you'd like to have <strong class="highlight"><img src="images/Inversion/supportsm.png" alt="" width="150" height="94" align="right" class="img" /></strong>answered.</p>
			      <p>That's why we've decided to offer the BEST support for inversion tables in the world that we know of... a <strong>full 90 days of personal 1-on-1 support.</strong></p>
			      <p><strong>If you have any questions about anything</strong> - whether it's an easier way to keep pressure off your ankles... or more advanced exercises to do while on the inversion table... or if you'd like like to talk about specific conditions YOU are personally dealing with...</p>
			      <p>... <strong>we're here to help in any way possible!</strong> All it takes is a quick email or phone call and you'll receive immediate support.</p>
			 </div>

		</div>
		
		
	
	</div>



	
</div>

<?php echo $this->partial('common/fb_conversion_lp.html.php'); ?>