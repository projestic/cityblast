<style>

hr.style-one 
{ 
	border: 0; 
	height: 1px; 
	width: 94%; 
	background: #333; 
	background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc); background-image: -moz-linear-gradient(left, #ccc, #333, #ccc); 
	background-image: -ms-linear-gradient(left, #ccc, #333, #ccc); background-image: -o-linear-gradient(left, #ccc, #333, #ccc); 
}	

body
{
	font-size: 16px;	
}

.highlight 
{
    background-color: #FFFF00;
    /*font-style: italic;*/
}	

.questions {
	background-color: #7FFFFF;
}


blockquote {
	margin: 15px 25px;
   	font-style: normal !important;
}

h1
{
	font-weight: bold;
	font-family: "adelle", "arial",serif;
	font-weight: 800;
	
}


</style>

<div class="row-fluid">
	<div class="span6" style="margin-bottom:5px;"><img style="margin-left: -9px;" src="/images/city-blast_logo.png"></div>
	<div class="span6">
		<ul class="inline giveUsCall pull-right">
			<li>
				<div class="bold" style="color:#0045cc">Pricing</div>
				<div>Only $49.99/month</div>
			</li>
			<li>
				<div class="bold" style="color:#0045cc">30 Day Free Trial</div>
				<div>Start using CityBlast today</div>
			</li>
		</ul>
	</div>
</div>



<!-- Main hero unit for a primary marketing message or call to action -->
<? /*<div class="hero-unit">*/ ?>
<div style="box-shadow: 0px 0px 30px -1px #666; -webkit-box-shadow: 0px 0px 30px -1px #666; -moz-box-shadow: 0px 0px 30px -1px #666; margin-top: 10px;">

	<?/* background-color: #fbfbda; */ ?>
	<div class="row-fluid" style="padding-top: 8px; padding-bottom: 8px; background-color: #ffffff;">
			
		<div class="span12" style="padding-left: 30px; text-align: center">	
			
			<h1 class="tk-adelle" style="font-family: 'adelle', arial !important; padding-top: 30px; font-size: 32pt; margin-top: 0px; line-height: 38pt;">Did You Know <u>Your Friends</u><br /> 
				Are <span class="highlight" style="line-height: 32pt; display: inline-block; margin: 2pt 0;">Buying</span> Real Estate 
				<span class="highlight" style="line-height: 32pt; display: inline-block; margin: 2pt 0;">Right Now</span>?</h1>	
			
			<h2>90% of New Home Searches Start Online* - Without A Real Estate Agent</h2>
			<?/*<h2>Pssst... We'll Let You In On A Secret, That Means They're Looking For A New Place Without Your Help!</h2>*/?>
		</div>
	
	</div>


	<div class="row-fluid" style="background-color: #ffffff;">

		<div class="span12" style="text-align: center; padding-left: 38px; margin-bottom: 20px;">
			<hr class="style-one clearfix" />
		</div>

	</div>
		
		
	<div class="row-fluid" style="background-color: #ffffff;">
		
		<div class="span1"></div>
			
		<div class="span6">
			<h3 class="tk-adelle-n7" style="font-size: 28px; font-family: 'adelle', arial !important; color: #cc0000; margin-top: 0px;">Selling Real Estate Has <u>Changed Drastically</u> In The Last 5 Years. <u>Have You</u>?</h3>
			
			<p>Your friends are searching for real estate right now and you probably don't even know it. 

			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum justo et augue aliquam in cursus neque euismod. Nam sagittis aliquet laoreet. Maecenas massa arcu, tincidunt quis placerat non, placerat sed lorem. Vestibulum vehicula leo mollis arcu imperdiet vel lobortis ante condimentum. Ut orci nisl, tincidunt id aliquam ut, pulvinar ac purus. Aliquam nisi risus, vulputate non suscipit nec, placerat id dui. Fusce mattis imperdiet erat. Morbi placerat fermentum tellus non eleifend. Curabitur placerat lacus mauris, quis bibendum risus. Donec aliquet orci et magna fermentum id eleifend neque sodales. Cras non lorem in lorem ornare interdum. Etiam dignissim arcu ac ante porta vitae dignissim arcu cursus. 
				
			</p>
			
			<div style="width: 100%; text-align: center; margin-top: 35px;">
				<a style="text-transform: uppercase; font-size: 28px; font-weight: bold; padding: 19px;" class="btn btn-primary btn-large" href="/lp/make-money/video/join">Try Us Free!</a>				
			</div>
			
		</div>	
		
		
		<div class="span4">

			<img src="/images/dashboard.jpg" style="border: 2px solid #888888;" />

			<?/***
			<div class="dashboardBox" style="width: 100%; margin-top: 0px;">			
				<div class="dashboardHeading clearfix">
					<h4>People Reading Your Posts</h4>
				</div>
				<div class="summary clearfix">
					<div class="summaryTxt">
						<label>Last 30 Days</label>
						<div class="data">925</div>
					</div>
					
					<div class="summaryIcon"></div>
				</div>			
			</div>
			****/ ?>
			
		</div>

	</div>



		
	<div class="row-fluid" style="background-color: #ffffff;">
		
		<div class="span12" style="text-align: center; padding-left: 38px; margin-top: 20px;">
			<hr class="style-one clearfix" />
		</div>

	</div>


	<div class="row-fluid" style="background-color: #ffffff;">

		<div class="span12" style="text-align: center;">
		
			<h1 class="tk-adelle" style="font-family: 'adelle', arial !important; padding-top: 30px; font-size: 28pt; margin-top: 0px; line-height: 32pt;">TEN Ways CityBlast Helps You Win <u>More</u> Business</h1>
		</div>


		
		<div class="span2"></div>
		
		<div class="span4" style="text-align: left;  padding-left: 38px; margin-left: 2px; margin-top: 20px;">

			<div>
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td><span style="color: #cc0000;">This is something important</span> that leads to a black sentence that ends.</td>
					</tr>
				</table>
			</div>

			<div style="margin-top: 10px;">
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td>This is a completely different take on something. <span style="color: #cc0000;">It's also important</span> but in a different way.</td>
					</tr>
				</table>
			</div>

			<div style="margin-top: 10px;">
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td><span style="color: #cc0000;">This is something important</span> that leads to a black sentence that ends.</td>
					</tr>
				</table>
			</div>

			<div style="margin-top: 10px;">
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td>This is a completely different take on something. <span style="color: #cc0000;">It's also important</span> but in a different way.</td>
					</tr>
				</table>
			</div>

			<div style="margin-top: 10px;">
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td><span style="color: #cc0000;">This is something important</span> that leads to a black sentence that ends.</td>
					</tr>
				</table>
			</div>	
			
					
		</div>
		

		
		<div class="span4" style="text-align: left; padding-left: 38px; margin-top: 20px;">
		

			<div>
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td><span style="color: #cc0000;">This is something important</span> that leads to a black sentence that ends.</td>
					</tr>
				</table>
			</div>

			<div style="margin-top: 10px;">
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td>This is a completely different take on something. <span style="color: #cc0000;">It's also important</span> but in a different way.</td>
					</tr>
				</table>
			</div>

			<div style="margin-top: 10px;">
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td><span style="color: #cc0000;">This is something important</span> that leads to a black sentence that ends.</td>
					</tr>
				</table>
			</div>

			<div style="margin-top: 10px;">
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td>This is a completely different take on something. <span style="color: #cc0000;">It's also important</span> but in a different way.</td>
					</tr>
				</table>
			</div>

			<div style="margin-top: 10px;">
				<table style="font-size: 16px; color: #000000; font-weight: normal;">
					<tr>
						<td style="vertical-align: top !important; padding-right: 6px;"><img src="/images/checkmark1.png"  style="width: 20px; margin-top: 4px;" /></td>
						<td><span style="color: #cc0000;">This is something important</span> that leads to a black sentence that ends.</td>
					</tr>
				</table>
			</div>	
			
		
		</div>
		
		<div class="span2"></div>
		
		
	</div>



	<div class="row-fluid" style="background-color: #ffffff;">
		
		<div class="span12" style="text-align: center; padding-left: 38px; margin-top: 20px;">
			<hr class="style-one clearfix" />
		</div>

	</div>
	

	<div class="row-fluid" style="background-color: #ffffff;">

		<div class="span12" style="text-align: center;">		
			<h1 class="tk-adelle" style="font-family: 'adelle', arial !important; padding-top: 30px; padding-left: 25px; font-size: 34pt; margin-top: 0px; line-height: 32pt;">Our Customers Love CityBlast</h1>						
		</div>
		
		
		
		<div class="span12" style="text-align: center; color: #5d5d5d">
			
			<center>
			
			<hr style="float: none; width: 40%; height: 1px; background-color: #999999; margin: 15px auto;" />
		
			<h2 style='color: #5d5d5d; font-size: 42px; font-family: "adage-script", sans-serif !important; font-style: normal; font-weight: 400; margin-top: 0px; padding-top: 0px;'>
				Over 4,000 Real Estate Agents
				<br/> depend on CityBlast to manage 
				<br/>more than 2.5 million relationships
			</h2>

			<hr style="float: none; width: 40%; height: 1px; background-color: #999999; margin: 15px auto;" />
			
			</center>
		</div>
		

		
	</div>


	<div class="row-fluid" style="background-color: #ffffff; padding-top: 25px;">
		
		<div class="span3"></div>
		
		<div class="span6">
			
			<table border="0">
				<tr>
					<td style="padding-right: 15px;"><img src="/images/diane.jpg" style="height: 130px;"></td>
					
					<td style="vertical-align: top; padding: 0px; margin: 0px;" valign="top"><strong style="vertical-align: top; padding: 0px; margin: 0px;">"CityBlast is perfect for my busy life."</strong><br/>
						Here is some more text followed by even more text. At some point
						you're going to <span style="color: #cc0000;">add some bold burgundy</span> for effect.
						
						<br/>
						<br/>
						<i>~Tom Tomerton - 21st Century</i>
					</td>
					
					
				</tr>
			</table>
						
		</div>
		
		<div class="span3"></div>

	</div>


	<div class="row-fluid" style="background-color: #ffffff; padding-top: 25px;">
	
		<div class="span3"></div>
		
		<div class="span6">
			
			<table border="0">
				<tr>
				
					<td style="vertical-align: top; padding: 0px; margin: 0px;" valign="top"><strong style="vertical-align: top; padding: 0px; margin: 0px;">"CityBlast is perfect for my busy life."</strong><br/>
						Here is some more text followed by even more text. At some point
						you're going to <span style="color: #cc0000;">add some bold burgundy</span> for effect.
						
						<br/>
						<br/>
						<i>~Tom Tomerton - 21st Century</i>
					</td>

					<td style="padding-right: 15px;"><img src="/images/diane.jpg" style="height: 130px;"></td>				
					
				</tr>
			</table>
						
		</div>
		
		<div class="span3"></div>

	</div>


	<div class="row-fluid" style="background-color: #ffffff; padding-top: 25px;">

		<div class="span3"></div>
		
		<div class="span6">
			
			<table border="0">
				<tr>
					<td style="padding-right: 15px;"><img src="/images/diane.jpg" style="height: 130px;"></td>
					
					<td style="vertical-align: top; padding: 0px; margin: 0px;" valign="top"><strong style="vertical-align: top; padding: 0px; margin: 0px;">"CityBlast is perfect for my busy life."</strong><br/>
						Here is some more text followed by even more text. At some point
						you're going to <span style="color: #cc0000;">add some bold burgundy</span> for effect.
						
						<br/>
						<br/>
						<i>~Tom Tomerton - 21st Century</i>
					</td>
					
				</tr>
			</table>
						
		</div>
		
		<div class="span3"></div>

		
	</div>
	
	

	<div class="row-fluid" style="background-color: #ffffff;">
		
		<div class="span12" style="text-align: center; padding-left: 38px; margin-top: 25px;">
			<hr class="style-one clearfix" />
		</div>

	</div>
	


	<div class="row-fluid" style="background-color: #ffffff;">

		<div class="span12" style="text-align: center;">
		
			<h1 class="tk-adelle" style="font-family: 'adelle', arial !important; padding-top: 30px; font-size: 22pt; margin-top: 0px; line-height: 32pt;">
				Dedicated and Certified Trustworthy Social Media Virtual Assistants<br /> That Take The Pain Out Of Social Media Marketing	
			</h1>
			
		</div>
	
	</div>


	<div class="row-fluid" style="background-color: #ffffff; padding-top: 25px;">
	
		<div class="span3"></div>
		
		<div class="span6" style="text-align: center;">
			<img src="/images/sample-informational.jpg">
		</div>
		
		<div class="span3"></div>	

	</div>


	<div class="row-fluid" style="background-color: #ffffff; padding-top: 25px;">

	
		<div class="span3"></div>
		
		<div class="span6" style="text-align: center;">
			<img src="/images/sample-lifestyle.jpg">
		</div>
		
		<div class="span3"></div>	
		
	</div>


	<div class="row-fluid" style="background-color: #ffffff; padding-top: 25px;">

	
		<div class="span3"></div>
		
		<div class="span6" style="text-align: center;">
			<img src="/images/sample-listing.jpg">
		</div>
		
		<div class="span3"></div>	

	</div>	
	



	<div class="row-fluid" style="background-color: #ffffff; padding-top: 35px;">
	
		<div class="span2"></div>
		
		<div class="span8" style="border: 3px solid #e3e3e3;">
			<div style="width: 80%; padding: 0px; margin: 0px auto;">
								
				<center><h3 style="font-family: 'adelle', arial !important; font-size: 26pt;">Are You Ready Get The Upper Hand?</h3></center>
				
				<p><strong>CityBlast offers everything you need...</strong></p>
				
				<p>
					<ul>
						<li><span style="color:#cc0000;">This is bullet point number</span> one Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum justo et augue</li>
						
						<li><span style="color:#cc0000;">Ipsum dolor sit</span> two Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum justo et augue. dolor sit amet, consectetur adipiscing</li>
						
						<li><span style="color:#cc0000;">This is bullet point number</span> one Lorem ipsum dolor sit amet, consectetur adipiscing  augue</li>
						
						<li><span style="color:#cc0000;">Curabitur bibendum ipsum dolor</span> one Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ipsum dolor sit amet, consectetur adipiscing elit. Curabitur justo et augue</li>
					
					</ul>
				</p>


				<center><h3 style="font-family: 'adelle', arial !important; font-size: 26pt;">Try CityBlast For Yourself</h3></center>
				
				<div style="width: 80%; padding: 0px; margin: 0px auto; border: 1px solid #e7e7e7; text-align: center">
					<p style="font-size: 18px;">Your CityBlast account includes:</p>
					
					<p>
						Up to <span style="color:#cc0000;">7</span> custom social media posts a week<br/>
						<span style="color:#cc0000;">Unlimited</span> listings to our massive network<br/>
						<span style="color:#cc0000;">90 days</span> of 1-On-1 Support<br/>
						<span style="color:#cc0000;">Enhanced</span> security<br/>
						<span style="color:#cc0000;">World-class</span> support<br/>
					</p>
					
					<p style="padding-left: 15px; padding-right: 15px;">Try CityBlast now for <b>30 days free</b>. After that, CityBlast is <b>only $49/month</b></p>
					
					<p><a style="font-size: 16px; font-weight: bold; padding: 19px;" class="btn btn-primary" href="/lp/make-money/long-form1/join">Get started with CityBlast today!</a>
					
				</div>
				
			</div>
			
			<div style="width: 80%; padding: 0px; margin: 0px auto;">
				<div style="width: 80%; padding: 0px; margin: 0px auto;">
				
					<p style="font-size: 18px;">Signup takes only 60 seconds. Cancel any time.</p>				
					<p>CityBlast uses Facebook Connect and OAuth. There's nothing to install or update. No "computer guy" required. You can try any of our plans right now for 30 days free.</p>
				</div>
				
			</div>	
		</div>
		
		<div class="span2"></div>	

	</div>

	<div class="row-fluid" style="background-color: #ffffff; padding-top: 35px;">
	
		<div class="span2"></div>
		
		<div class="span8">
			
			<div>Wishing you the best success in your business,</div>
			
			
			<div style="margin-top: 14px; margin-bottom: 14px;"><img src="/images/cityblast-signature.jpg"></div>
			
			<div>
				CityBlast
				
				<p style="font-size: 12px;">
					P.S. We're real estate agents too! We went through the same process you're going through looking at all the social media marketing options available. 
					<span style="color:#cc0000;">They were either too expensive or not good enough</span>. That's why we created CityBlast. We hope you <a href="/lp/make-money/long-form1/join">try it</a> and 
					find out that it's a perfect fit for you too.
				</p>
				
				<p style="font-size: 12px;">
					P.S.S. Our support is <span style="color:#cc0000;">world class</span>. Our customers consistently rate our customer service highly. We'd love you to join over 4,000 real estate agents who get an edge with CityBlast.
				Get started with a <a href="/lp/make-money/long-form1/join">free 30 day trial</a> right now.
				</p>
			
			</div>
			
			
		</div>
		
		
		<div class="span2"></div>
	
	</div>


	<div class="clearfix" style="height: 60px; background-color: #ffffff; ">&nbsp;</div>

	
</div>

<?php echo $this->partial('common/fb_conversion_lp.html.php'); ?>