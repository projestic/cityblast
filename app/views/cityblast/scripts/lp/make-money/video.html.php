<div class="row-fluid">
	<div class="span6" style="margin-bottom:5px;"><img style="margin-left: -9px;" src="/images/city-blast_logo.png"></div>
	<div class="span6">
		<ul class="inline giveUsCall pull-right">
			<li>
				<div class="bold">Start Today!</div>
				<div>Give Us a Call</div>
			</li>
			<li>
				<div class="bold">1-888-712-7888</div>
				<div>9am to 5pm EST</div>
			</li>
		</ul>
	</div>
</div>

<!-- Main hero unit for a primary marketing message or call to action -->
<? /*<div class="hero-unit">*/ ?>
<div style="box-shadow: 0px 0px 30px -1px #666; -webkit-box-shadow: 0px 0px 30px -1px #666; -moz-box-shadow: 0px 0px 30px -1px #666; margin-top: 10px;">
	
	<?/* background-color: #fbfbda; */ ?>
	<div class="row-fluid" style="padding-top: 8px; padding-bottom: 8px; background-color: #ffffff;">
			
		<div class="span6" style="padding-left: 38px;">
		
			<h1 style="margin-bottom: 0; padding-bottom: 18px; border-bottom: 1px solid #c0c0c0;"><i>We <u>Promise</u> Just One Thing: &nbsp;<span style="color: #ff7f00;"><u>More Clients</u></i></span></h1>
			
			
			<div style="padding-top: 30px; padding-left: 35px; border-top: 1px solid #f1f1f1;"><img src="/images/checkmark-new.png">
				<span style="vertical-align: middle; margin-left: 5px; line-height: 20px; font-size: 18px; font-weight: bold; color: #7d7d7d">We Do The Heavy Lifting. You Collect The Commission!</span></div>
			
			<div style="margin-top: 20px; padding-left: 35px;"><img src="/images/checkmark-new.png">
				<span style="vertical-align: middle; margin-left: 5px; line-height: 20px; font-size: 18px; font-weight: bold; color: #7d7d7d">Watch Your Sales Explode!</span></div>
			
			<div style="margin-top: 20px; padding-left: 35px;"><img src="/images/checkmark-new.png">
				<span style="vertical-align: middle; margin-left: 5px; line-height: 20px; font-size: 18px; font-weight: bold; color: #7d7d7d">You Get a 30 Day Risk Free Trial!</span></div>
			
			
			
			<p style="margin-top: 35px; text-align: center"><a href="<? echo $this->group_name;?>/<?=$this->page_name;?>/join" class="btn btn-success btn-large">I'm Ready to <em>Earn More</em> Commission</a></p>
			
		</div>
		
		<div class="span6" style="height: 350px; padding-right: 20px; padding-top: 20px; padding-bottom: 12px; z-index: 1;">

			<iframe id="videoframe" style="position: relative; z-index: 2;" width="100%" height="100%" src="//www.youtube.com/embed/OrrGZk2ogwU?rel=0&autoplay=1" frameborder="1" allowfullscreen></iframe>

		</div>
		
	</div>


	<div class="row-fluid" style="padding-top: 38px; padding-bottom: 36px; background-color: #ececec; border-top: 1px solid #bfbfbf;">
		<div class="span4">
			<div class="questionBox howWorks">
				<span class="tagNumber">1</span>
				<h4>How It Works</h4>
				<ul>
					<li>Tell our experts what kind of content you want posted: articles, videos, listings, quotes, pictures and more!</li>
					<li>A real estate expert will source the chosen content, and post it to your social networks as often as you choose.</li>
					<li>All posting is customized to you - your local market; and how often you want to receive content.</li>
					<li>You're in control, but save time and effort.</li>
				</ul>
				<div class="boxLink clearfix">
					
					
					<a href="#" class="text">&nbsp;<?/*What Do The Posts Look Like?*/?></a>
					<a href="#" <?/*class="play"*/?>></a>
					
				</div>
			</div>
		</div>
		<div class="span4">
			<div class="questionBox whyWorks">
				<span class="tagNumber">2</span>
				<h4>WHY IT WORKS</h4>
				<ul>
					<li>90% of real estate clients start looking for info online without informing an agent.</li>
					<li>The agents who create the most business online are consistently in the public eye with new and relevant content.</li>
					<li>Social media sites like Facebook are the #1 place that these new clients are found.</li>
					<li>CityBlast keeps you in the public eye using professional content experts, so you don't have to spend your valuable time.</li>
				</ul>
				<div class="boxLink clearfix">
					<a href="#" class="text" id="show_posts" style="font-size: 14px;">What Do The Posts Look Like?</a>
					<a href="#" class="play"></a>
				</div>
			</div>
		</div>
		<div class="span4">
			<div class="questionBox getStarted">
				<span class="tagNumber">3</span>
				<h4>HOW DO I GET STARTED?</h4>
				<ul>
					<li>Getting started is quick, easy and free. </li>
					<li>Click on the “Try Us Free” button and a 30-second signup begins.</li>
					<li>Simply select what type of content you want our experts to post; how often; and for what local market.</li>
					<li>Your 30-day free trial will begin - and so will your upgraded online presence!</li>
					
				</ul>
				<div class="boxLink clearfix">
					<a href="<?=$this->group_name;?>/<?=$this->page_name;?>/join" class="text" style="font-size: 14px;">I’m Ready To Start My FREE Trial</a>
					<a href="<?=$this->group_name;?>/<?=$this->page_name;?>/join" class="play"></a>
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid" style="padding: 9px 0; background: #4e4e4e; background : -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#4e4e4e), color-stop(100%,#3e3e3e)); background: -moz-radial-gradient(center, ellipse cover, #4e4e4e 0%, #3e3e3e 100%); background: -ms-radial-gradient(center, ellipse cover, #4e4e4e 0%,#3e3e3e 100%); background: -o-radial-gradient(center, ellipse cover, #4e4e4e 0%,#3e3e3e 100%); background: -ms-radial-gradient(center, ellipse cover, #4e4e4e 0%,#3e3e3e 100%); background: radial-gradient(ellipse at center, #4e4e4e 0%,#3e3e3e 100%);">
		
			<p style="font-size: 25px; color: #fff; font-weight: 600; margin-left: 60px; margin-right: 60px;" class="pull-left">Dedicated and Certified Social Media Professionals Are Waiting To Help: </p>
		
			<a href="<?=$this->group_name;?>/<?=$this->page_name;?>/join" class="pull-right btn btn-success btn-large" style="text-transform: uppercase; font-size: 28px; font-weight: bold; padding: 19px; margin-top: 10px; margin-right: 60px;">Try Us Free!</a>
		
	</div>
	


	<?/*******	  
	<div class="row-fluid" style="background-color: #ececec;">
		<div class="span12" style="padding-top: 30px; padding-bottom: 10px;">
			<h1 style="text-align: center; margin: 0; line-height: 40px; color: #666666">
				Start Today! Call us at <span style="color: #333333;">1-888-712-7888</span>&nbsp;<span style="font-size: 14px; color: #3D6DCC">(*9:00 am to 5:00 pm EST)</span>
			</h1>
		</div>
	</div>
	***********/ ?>


	<div class="clearfix" style="border-top: 1px solid #bfbfbf;">	
		<? echo $this->render('logos-responsive-small.html.php'); ?>
	</div>



	<? 
		$this->bgcolor = "background-color: #ececec;";
		echo $this->FacePile('responsive'); 
	?>




	<div class="row-fluid" style="padding: 9px 0; background: #4e4e4e; background : -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#4e4e4e), color-stop(100%,#3e3e3e)); background: -moz-radial-gradient(center, ellipse cover, #4e4e4e 0%, #3e3e3e 100%); background: -ms-radial-gradient(center, ellipse cover, #4e4e4e 0%,#3e3e3e 100%); background: -o-radial-gradient(center, ellipse cover, #4e4e4e 0%,#3e3e3e 100%); background: -ms-radial-gradient(center, ellipse cover, #4e4e4e 0%,#3e3e3e 100%); background: radial-gradient(ellipse at center, #4e4e4e 0%,#3e3e3e 100%);">
	
		<a href="<?=$this->group_name;?>/<?=$this->page_name;?>/join" class="pull-left btn btn-success btn-large" style="text-transform: uppercase; font-size: 28px; font-weight: bold; padding: 19px; margin-top: 10px; margin-left: 60px;">Try Us Free!</a>
		
		<p style="font-size: 25px; color: #fff; font-weight: 600; margin-left: 60px; margin-right: 60px;" class="pull-left">Begin Your Free 30-Day Trial, and get our Social Experts working for you.</p>
					
	</div>
	

	
	<style type="text/css">
	.fb_iframe_widget,
	.fb_iframe_widget span,
	.fb_iframe_widget iframe[style]  {width: 100% !important;}
	</style>

	<div class="row-fluid" style="padding-top: 10px; background-color: #ececec;">

		<div class="span12 clearfix" style="min-height: 161px; padding-left: 8px;">		
			<h3 style="color: #808080; ">Members helping members.</h3>
			<fb:comments xid="member_join" href="<? echo APP_URL;?>/join"></fb:comments>
		</div>

	</div>
</div>

<script type="text/javascript">
	// Resize FB comments block to width of the borwser window
	$(document).ready(function() {
		setTimeout(function() {
		  resizeFacebookComments();
		}, 1000);
		$(window).on('resize', function() {
		  resizeFacebookComments();
		});
		function resizeFacebookComments() {
		  var src = $('.fb_iframe_widget iframe').attr('src').split('width='),
		      width = $('.fb_iframe_widget').width() - 10;
		  $('.fb_iframe_widget iframe').attr('src', src[0] + 'width=' + width);
		}
	});
</script>

<script type="text/javascript">

	$(document).ready(function() 
	{	
		// Adding event listening on comment
		FB.Event.subscribe('comment.create', function(response)
		{
		
		    var commentQuery = FB.Data.query("SELECT object_id,id,text, fromid FROM comment WHERE post_fbid='"+response.commentID+"' AND object_id IN (SELECT comments_fbid FROM link_stat WHERE url='"+response.href+"')");
		    var userQuery = FB.Data.query("SELECT name FROM user WHERE uid in (select fromid from {0})", commentQuery);
		
		
		    FB.Data.waitOn([commentQuery, userQuery], function() 
		    {
				var commentRow = commentQuery.value[0];
				var userRow = userQuery.value[0];
		
				var commentId   =	commentRow.id;
				var name	 	=	userRow.name;
				var fuid		=	commentRow.fromid;
				var text	 	=	commentRow.text;
				var mid		=	<?php echo (empty($_GET['mid'])) ? 0 : intval($_GET['mid']);?>;
				var url		=	'/comments/broadcast';
				var data	 	=	'name='+name+'&fuid='+fuid+'&text='+text+'&mid='+mid+'&referring_url=/<?=$this->group_name;?>/<?=$this->page_name;?>';
		
		
				$.ajax({
					type: 'POST',
					url: url,
					data: data,
					success: function(response){
						// do nothing
					},
					dataType: 'json'
				});		
		    });
		});	
		


		$("#show_posts").colorbox({

			width:"600px", 
			height: "900px", 
			inline:true, 
			href:"#show_posts_popup",
			onOpen:function(){ $("#videoframe").css("visibility", "hidden"); }, 
			onClosed:function(){ $("#videoframe").css("visibility", "visible"); }		
		}); 
				


	});
</script>



<div style="display: none;">

	<div id="show_posts_popup">
		
		<div class="box clearfix">
	
			
			<h3>Informational</h3>			
			<p><img src="/images/sample-informational.jpg" /></p>

			<h3>Lifesyle</h3>
			<p><img src="/images/sample-lifestyle.jpg" /></p>

			<h3>Listings</h3>
			<p><img src="/images/sample-listing.jpg" /></p>			
																			
		</div>
		
		<a href="<?=$this->group_name;?>/<?=$this->page_name;?>/join" class="uiButton right">Start your free trial</a>

	</div>
</div>
	


<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/0184.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

<?php echo $this->partial('common/fb_conversion_lp.html.php'); ?>