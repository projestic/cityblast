<div class="contentBox">

	<div id="introduction">
		
		<div class="contentBoxTitle">
			
			<h1 style="font-size: 40px;">The Secret Bullet To More Leads: Inbound Marketing.</h1>
									
			<h4 style="font-weight: normal; color: #666666;">CityBlast CEO Shaun Nilsson explains how he used Inbound Marketing to earn $165,000 in his first year as a real estate agent.</h4>
				
		</div>
		
	
	</div>


	


	
	<div>
		
		<?/*<p style="text-align: center; margin-top: 20px; width: 70%; padding: 10px 15%;" id="speakers"><strong style="color: #c00;">Note: </strong>The presentation is starting, please turn up your speakers. You may also pause the video at any point to get yourself ready to take notes and write down any questions.</p>*/?>
	
		<div style="overflow: hidden; border: 0px; margin-top: 20px;">
			
			<div>
				<iframe width="853" height="480" src="//www.youtube.com/embed/hTHpLE8zdVE" frameborder="0" allowfullscreen></iframe>			
				<?/*<div id="video"></div>*/?>				
			</div>
			
		</div>
		
	</div>




	<?/**************
	<div id="who_is_cityblast" style="display: none; margin-top: 25px;">
	
		<div class="contentBoxTitle">
									
			<h1 style="font-size: 40px;">Who is CityBlast and how can they help?</h1>
				
		</div>
		
		<div>
			
			<p>By now you’ve probably heard about CityBlast and how we’re the leaders in social media marketing in the real estate game, but just in case you haven’t here’s a short bio...</p>
		
			<div class="whos_cb_stats_box clearfix">
				
				<div style="width: 16%; text-align: center; float: left;">
					<strong style="font-size: 22px; line-height: 30px; display: block;">865,436</strong>
					Social Media Posts
				</div>
				
				<div style="width: 16%; text-align: center; float: left;">
					<strong style="font-size: 22px; line-height: 30px; display: block;">3,170,098</strong>
					Social Media Reach
				</div>
				
				<div style="width: 16%; text-align: center; float: left;">
					<strong style="font-size: 22px; line-height: 30px; display: block;">7,338,334</strong>
					Total Posts Read
				</div>
				
				<div style="width: 16%; text-align: center; float: left;">
					<strong style="font-size: 22px; line-height: 30px; display: block;">311,423</strong>
					Leads Generated
				</div>
				
				<div style="width: 16%; text-align: center; float: left;">
					<strong style="font-size: 22px; line-height: 30px; display: block;">1,289,666</strong>
					Unique Visitors
				</div>
				
				<div style="width: 16%; text-align: center; float: left;">
					<strong style="font-size: 22px; line-height: 30px; display: block;">21,293,148</strong>
					Revenue Generated
				</div>
				
			</div>
			
			<p>Numbers don’t lie. We guarantee our service helps you gain an incredible edge in online marketing. Join the 1000s of other real estate agents in your area already getting a leg up in social media marketing with CityBlast.</p>
			
		</div>
		
	</div>
	************/ ?>



	<div id="our_team" style="margin-top: 25px;">
	
		<div class="contentBoxTitle" style="border-bottom: 1px solid #a4a4a4;">
									
			<h1 style="font-size: 36px;">Too busy to implement this plan on your own?</h1>
			<h4 style="font-weight: normal; color: #666666;">Our social experts can design and execute your very own custom Social Media Campaign!</h4>
			<?/*<h1 style="font-size: 36px;">Our team of social experts are waiting to help you.</h1>*/?>
				
		</div>
		
		<div style="">
			
			<img src="/images/our_experts.png" width="780" height="144" style="display: block; margin: 0 auto;" />
			
		</div>



		<div class="clearfix" style="margin-top: 20px; padding-top: 20px; border-top: 1px solid #a4a4a4;">
			<div class="pull-left tryDemoTxt">
				<?/*<h3 style="font-size: 28px;">Get an Expert handling your business's social media.</h3>*/?>
				<h3 style="font-size: 28px;">Our Social Experts can help.</h3>
				<h4>Start your Free 14-Day Trial of <?=COMPANY_NAME;?>  Social Experts now.</h4>
			</div>
			<div class="pull-right">
				<a class="uiButtonNew tryDemo" href="/member/index">Try Demo.</a>
			</div>
		</div>

		
	</div>
	
	

	



	
</div>




	
<script src="/js/jwplayer/jwplayer.js" ></script>
<script type="text/javascript">
	
	var seen_whos_cb = false;
	var seen_our_team = false;
	seen_promtp_1 = false;
	seen_promtp_2 = false;
	seen_promtp_3 = false;

	$(document).ready(function(){
		
		/*EMBED
		jwplayer("video").setup({
			file:		"//www.youtube.com/watch?v=nmCQOlm6YYw",
			aspectratio:	'16:9',
			width:		'100%',
			autostart:	true,
			controls:		true
			/*
			logo: {
				file:		"/images/video_logo.png",
				hide:		true
			},
			
			ga: {
				idstring:		"title"
			}
			*
		});
		
		
		
		/*******************
		
		/////// READY?
		jwplayer().onReady(function() { 
			// SOME READY FUNCTION HERE
			setTimeout(function(){ 
				$('#speakers').html('In just a few minutes your presenter <strong>Shaun Nilsson</strong> will start to reveal the secrets to <strong>unlocking your potential</strong> using inbound marketing techniques.');
				$('#speakers').css( "backgroundColor", "#e5ffe5").animate({ "backgroundColor": "#fafbfc" }, 5000);
			}, 10000);
		}); 
		
		********************/
		
		/*******************
		/////// PLAY/PAUSE
		jwplayer().onDisplayClick(function() 
		{ 
			jwplayer().pause(); 
		}); 
		*********************/
		
		
		/****************
		/////// TIME BASED EVENTS
		jwplayer().onTime(function(event) 
		{
						
			// Mention of webinar panel
			if(Math.round(event.position) >= '15' && !seen_whos_cb){
				
				
				/*$('#introduction').slideUp();*
				
				/* $('#who_is_cityblast').slideDown(); *
				
			$('#our_team').slideDown();	
				
				seen_whos_cb = true;
			}
			
			// Mention of webinar panel
			if(Math.round(event.position) >= '30' && !seen_our_team){
				
				$('#who_is_cityblast').slideUp();
				
				
				$('#our_team').slideDown();
				
				seen_our_team = true;
			}
			
			// Mention of webinar panel
			if(Math.round(event.position) == '110'){
				$('#speakers').html('<strong style="color: #c00;">Important: </strong>Write down any questions you may have. You will have a chance to chat with one of our social media experts after the webinar.');
				$('#speakers').css( "backgroundColor", "#e5ffe5").animate({ "backgroundColor": "#fafbfc" }, 5000);
			}
			
			// Mention of webinar panel
			if(Math.round(event.position) >= '660' && !seen_promtp_1){
				$.colorbox({width: "400", html:"<h3>Awesome</h3><p style='font-size: 16px;'>You're well on your way to learning Shaun's secrets of inbound marketing. If you're ready to speak with one of our social media experts press the I'm Ready button below.</p><hr /><a href='javascript:void(0);' class='btn btn-primary pull-right'>I'm Ready</a>"});
				seen_promtp_1 = true;
			}
										
		});
		********************/
		
		
	});

</script>