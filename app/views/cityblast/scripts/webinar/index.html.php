<div id="step1" class="contentBox ">

	<div class="contentBoxTitle">
								
		<h1>Unlock the Power of Online Marketing</h1>
		<h3 style="font-weight: normal; color: #666666;">Get your free guide and automatic entry into our exclusive webinar.</h3>
		
	</div>
		
	<div class="stepBox step1 complete">
		
		<div style="margin-top: 30px;" class="stepContent clearfix">
			
			<div class="ladyCartoon pull-left">
				<img src="/images/new-images/step2-lady-cartoon.png">
			</div>
			
			<div style="overflow: hidden;">
				
				<form style="margin: 0px !important; padding: 0px !important;" method="POST" action="/free-webinar-insider-secrets/watch" id="upfrontsettings" name="upfrontsettings">
					
					<ul class="uiForm full">
						
						<li class="clearfix">
							<h2>Enter Your email. Gain Access.</h2>
						</li>
						
						<li class="clearfix">
							<ul>
								<li class="fieldname cc" style="padding-left: 0px">
									For automatic entry into our next exclusive webinar, free.
								</li>
								<li class="field cc clearfix" style="padding-left: 0px">
									<input type="text" class="uiInput <?= empty($this->emailError) ? '' : 'error'; ?>" id="email" name="email" placeholder="Your best email..."><br>
								</li>
							</ul>
						</li>
						
						<li class="uiFormRow clearfix">
							<ul class="clearfix">
								<li class="clearfix">
									<hr>
									<button type="submit" id="step1_btn" class="btn btn-primary btn-large pull-right next-section" href="javascript:void(0);"><i class="icon-chevron-sign-right icon-fixed-width"></i> Reveal the Secrets NOW!</button>
								</li>
							</ul>
						
						</li>
					
					</ul>
				
				</form>
			
			</div>
		
		</div>
	
	</div>
	
</div>


<script type="text/javascript">
$(document).ready(function(){
		
	$("#upfrontsettings").validate({errorElement: "div",
	    errorPlacement: function(error, element) {
        error.insertBefore($("#email"));
     },rules: {
        email: {
			required: true,
            email: true
        }
     },
     messages: {
        email: {
			required: "Please enter a valid email address.",
            email: "Please enter a valid email address."
        }
	}});

});
</script>