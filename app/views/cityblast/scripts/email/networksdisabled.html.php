<?php
$network_names = array();
if (!empty($this->networks)) {
	foreach ($this->networks as $network) {
		$network_names[] = ucfirst(strtolower($network));
	}
}
$networks = implode(' and ', $network_names);
?>

<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>URGENT: POSTING PROBLEM</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>



 			<? $email_helper->contentH1(); ?>Your Expert Can't Currently Post To <?=$networks?> </h1>
	
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>

			
			<? $email_helper->contentP(); ?>We just received word from your Social Expert.  They recently tried to post to a new article to <?=$networks?> for you, but weren't able to because your account needs to be refreshed.</p>
			
			<? $email_helper->contentP(); ?>Not to worry!  This is a fairly common hiccup, and happens every 60 days or when you've just changed one of your passwords.  The solution is easy:</p>
			<? $email_helper->contentP(); ?>
				<ol>
					<li <?=$email_helper->orderedListStyle();?>>Click the button below and log in to CityBlast</li>
					<li <?=$email_helper->orderedListStyle();?>>Reset your accounts with the <?=$networks?> Reset button</li>
				</ol>
														
			</p>
		
			<? $email_helper->contentP(); ?>It's that easy!  Also, feel free to call us if you're having any trouble.  Sometimes this stuff is easier with someone on the phone - 1-888-712-7888.</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Fix It Now!</a>
			</p>   
	    
		</div>  		

		
	</div>
	
	<p style="text-align: center;  margin-top: 25px;"><a href="*|UNSUB:<?=APP_URL;?>|*">Click here to unsubscribe.</a></p>

<? include "email-footer-NEW.html.php"; ?>