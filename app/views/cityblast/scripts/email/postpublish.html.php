<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>

	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>LISTING SHOWCASE</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Success! Your Listing Has Been Approved</h1>
	

			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>
			
			<? $email_helper->contentP(); ?>We've started publishing your listing on the walls of agents all across your market. Expect the clicks and leads to start pouring in!</p>
			
			<? $email_helper->contentP(); ?>Here's what the post looked like on Facebook, for example:</a></p>
			
			<br/>
			
			<? include "blast-preview.html.php"; ?>
			
			<br/>
			
			<? $email_helper->contentP(); ?>Great work! If you'd like to view or edit your listing, simply click the button below to access your Listings Tab.</p>
		
			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?php echo APP_URL."/member/settings#listingstab"; ?>" style="<? $email_helper->actionButtonStyle();?>">My Listings</a>
			</p>   
	    
		</div>
		
	</div>	
	
<? include "email-footer-NEW.html.php"; ?>
