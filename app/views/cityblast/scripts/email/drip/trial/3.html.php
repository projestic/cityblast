<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>THE POWER OF CONSISTENCY.</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Consistency = Trust.</h1>
	
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>

			
			<? $email_helper->contentP(); ?>If your Social Media Marketing campaign is suffering from inconsistency you have a <b>MAJOR</b>  business flaw that has already <span style="background-color: #ffff00;"><b>LOST</b> you tens of 
				thousands of dollars <b>THIS</b> year</span>.</p>

<? $email_helper->contentP(); ?>But there's <u>good news</u> - now you can fix it in a snap.</p>
			
			<? $email_helper->contentP(); ?>Your clients and prospects spend <i>3x more time on Facebook than any other website in the world</i>.</p> 
			
			<? $email_helper->contentP(); ?>Now, more than ever, cultivating a professional image on 
				<a href="<?=APP_URL;?>/blog/6-reasons-why-a-lack-of-social-media-consistency-is-ruining-your-business/" <? $email_helper->linkStyle();?>>Facebook</a> is an <i>necessity of professional real estate life</i>.</p>
		
			<? $email_helper->contentP(); ?>We have <span style="background-color: #ffff00;"><b>6 irrefutable reasons why you need to keep your social media marketing consistent</b></span>.  
				<a href="<?=APP_URL;?>/blog/6-reasons-why-a-lack-of-social-media-consistency-is-ruining-your-business/" <? $email_helper->linkStyle();?>>Have a read.</a>.</p>
			
	
			<? $email_helper->contentP(); ?>And if you've been <b>too busy</b> to keep on top of your social media posts? </p> 
			
			<? $email_helper->contentP(); ?><i>We can help</i>.</p>  
			
			<? $email_helper->contentP(); ?><a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>Activate your account now</a>
			 and our <i>Social Experts</i> take care of it all for you.</p>
			
			<? $email_helper->contentP(); ?>BONUS: Upgrade to our <b>ANNUAL</b> plan and enjoy a <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>whopping 33% discount</a> AND <i><u>you</u> won't have to worry</i> about your social media for a whole year!</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Yes, I Want Consistency!</a>
			</p>   
	    
		</div>  		

		
	</div>	
	



<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>