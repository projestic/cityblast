<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>DON'T STOP NOW!</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Ah Nuts, Your 7 Day Trial Is Over.</h1>
	
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>!</p>

			
			<? $email_helper->contentP(); ?>Over the past 7 days, our <i>Social Experts</i> have been keeping your online profiles <u>fresh and up-to-date</u> with great lead-generating real estate content.</p>
			
			<? $email_helper->contentP(); ?><span style="background-color: #ffff00;">Keep up the <u>great momentum</u> you've built with your online network. </span>
			
			<? $email_helper->contentP(); ?>Simply <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>click the button below</a> to extend your FREE trial to a <i><u>full 14 days</u></i>!</p>
		
		

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Extend Your FREE Trial!</a>
			</p>   
	    
		</div>  		

		
	</div>	
	


<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>