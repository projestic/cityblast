<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>MAXIMIZE THE POWER OF YOUR FREE TRIAL </h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Content Is King.</h1>
	
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>

			
			<? $email_helper->contentP(); ?>Sixteen years ago, Bill Gates uttered that world famous phrase: "<b>Content is King</b>".</p>
			
			<? $email_helper->contentP(); ?>Now more than ever <a href="<?=APP_URL;?>/blog/become-an-irresistible-real-estate-agent-by-sharing-other-peoples-content-2/" <? $email_helper->linkStyle();?>>content is the key to generating business online</a> - and not annoying your friends.</p>

			<? $email_helper->contentP(); ?>Just ask Seth Godin who said: "Content Marketing is all the marketing that's left."</p>
			
			<? $email_helper->contentP(); ?>At <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>><?=COMPANY_NAME;?></a> <i>we've taken those words to heart</i>.
				
			<? $email_helper->contentP(); ?>Quite frankly, we didn't become the <b><i>world's largest social media managers for the real estate industry</i></b> by accident.</p>
			
			<? $email_helper->contentP(); ?>It's because at <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>><?=COMPANY_NAME;?></a> we offer 
				<span style="background-color: #ffff00;">more types of <u>great content</u> than anyone else</span> in the business. Period. 
			
		<? $email_helper->contentP(); ?>Polls, Quizzes, Infographics, Videos, Million Dollar Homes, Historic Architecture, Local Listings, Real Estate Technology and more. It's all right here at your finger tips.</p>
		
				
		<? $email_helper->contentP(); ?>So go ahead and <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>explore the content choices</a> available to you at 
			<a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>><?=COMPANY_NAME;?></a> and unlock the <u>EXPLOSIVE</u> power of content marketing - the easy way!</p>
		
		

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Manage My Content Preferences</a>
			</p>   
	    
		</div>  		

		
	</div>	
	




<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>