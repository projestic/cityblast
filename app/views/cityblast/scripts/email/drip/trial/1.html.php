<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>WELCOME TO CITYBLAST</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Your 7 Day <u>FREE</u> Trial Has Begun.</h1>
	
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>!</p>

			
			<? $email_helper->contentP(); ?>Welcome to <a href="http://www.cityblast.com" <? $email_helper->linkStyle();?>>CityBlast</a>.  
			Your marketing preferences have been successfully received, and the industry's <u>Top Experts</u> are already getting started planning 
			your online presence and keeping you fresh and up-to-date.</p>
			
			<? $email_helper->contentP(); ?>Congrats for taking action and improving <u><i>your</i></u> real estate business!</p>
			
			<? $email_helper->contentP(); ?>But... we noticed that you didn't quite finish the registration process. Not a problem!</p>
		
			<? $email_helper->contentP(); ?>We'll go ahead and get started with updating your profiles now.</p> 
			
			
			<? $email_helper->contentP(); ?>In the meantime, simply head to your <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>Dashboard</a> and add your payment info, so we can <span style="background-color: #ffff00;">unlock your full 14-day trial</span>!  
			It only takes a second.</p>


			
			<? $email_helper->contentP(); ?>Also, remember you can always feel free to call us if you're having any trouble, or just want to chat - 1-888-712-7888.</p>
			
						


			<? $email_helper->contentP(); ?>P.S. Did you know that <span style="background-color: #ffff00;"><i>you can reach thousands of potential buyers</i></span> 
			by "<a href="http://www.cityblast.com/blog/sell-your-listing-faster-unlock-the-secret-of-social-media-sharing/" <? $email_helper->linkStyle();?>>Blasting</a>" your listings to 
				our <i>massive network</i>?</p>  
					
			<? $email_helper->contentP(); ?>Best of all, it's 100% included in your membership!</p>
				
			<? $email_helper->contentP(); ?>Find out more about <a href="http://www.cityblast.com/index/how-blasting-works" <? $email_helper->linkStyle();?>>Blasting right here</a>!</p>




			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL;?>/member/settings" style="<? $email_helper->actionButtonStyle();?>">Unlock My 14-Day Trial!</a>
			</p>   




	    
		</div>  		

		
	</div>	
	



<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>