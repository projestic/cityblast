<? $email_helper = $this->email(); ?>

	
<? include __DIR__ . "/../../email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>WE WANT YOUR FEEDBACK</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Is <?=COMPANY_NAME;?> Everything You Want It To Be?</h1>
	
			
			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>

			
			<? $email_helper->contentP(); ?>At <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>>CityBlast</a> we're always striving to be better.</p>
			
			<? $email_helper->contentP(); ?>Quite frankly, there's a reason why we're the <b><i>biggest Social Media Marketing company for Real Estate Agents</i></b> in the world.</p>

			<? $email_helper->contentP(); ?><span style="background-color: #ffff00;">And that's because <u>we listen</u>.</span></p>
		
		
			<? $email_helper->contentP(); ?>We want you to get the absolute most out of your <b>7 Day FREE Trial</b>.</p>
			
			<? $email_helper->contentP(); ?>And in order to do that, we're asking you to help us make <?=COMPANY_NAME;?> even better for you by answering 
			<a href="http://survey.constantcontact.com/survey/a07e8dabgmkhmw6kvgs/start" <? $email_helper->linkStyle();?>>3 simple multiple choice questions</a>.</p>


			<? $email_helper->contentP(); ?>Tell us what you think, and we'll <span style="background-color: #ffff00;">give you an extra <u>FREE 7 days of <?=COMPANY_NAME;?></u></span>, just to say 'thanks'. 
			
			<? $email_helper->contentP(); ?>No tricks. No gimmicks. No strings attached.</p>
									
			
			<? $email_helper->contentP(); ?>Thanks for <i>helping us</i> make <a href="<?=APP_URL;?>/member/settings" <? $email_helper->linkStyle();?>><?=COMPANY_NAME;?></a> the best 
				in the business, and telling us how we can be <i>even better</i>.</p>

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="http://survey.constantcontact.com/survey/a07e8dabgmkhmw6kvgs/start" style="<? $email_helper->actionButtonStyle();?>">Yes, I Can Help!</a>
			</p>   
	    
		</div>  		

		
	</div>	
	


<? include __DIR__ . "/../../email-footer-NEW.html.php"; ?>