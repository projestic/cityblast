<? $email_helper = $this->email(); ?>
<? include "email-header-NEW.html.php"; ?>
<? $email_helper->openBox(); ?>
	<? $email_helper->openCap(); ?>
		<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
		<? $email_helper->openH4(); ?>“The 100 Greatest Real Estate Posts of All Time”</h4>
		<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
	</div>
	<? $email_helper->openContentDiv(); ?>
		<? $email_helper->contentP(); ?>We've done the work so you don't have to!  CityBlast's social media Experts have posted well over 1,000,000 individual real estate posts to Facebook, Twitter, and LinkedIn.  Your attached e-book gives you the top 100 posts we've ever created - in a handy cut &amp; paste format, so you can put them to work immediately on your social accounts.</p>
		<? $email_helper->contentP(); ?>Enjoy!</p>
		<? $email_helper->contentP(); ?>Oh, one more thing: If you find you're still too busy to post these yourself, why not give one of our Social Experts a try?  We're offering a special promo code, to go along with your e-book.  You'll get a complimentary 14-day trial AND a 20% discount for life, when you use the code:  100POSTS</p>
	</div>
</div>
<? include "email-footer-NEW.html.php"; ?>