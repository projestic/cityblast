<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>PAYMENT EXPIRY</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Your Payment Information Needs an Update</h1>
	

			<? $email_helper->contentP(); ?>Hi <?= $this->member->first_name;?>,</p>
			
			<? $email_helper->contentP(); ?>We're just sending you a friendly note, as it appears the method of payment you have on file with us is set to expire soon.  Not to worry!  Please take 60 seconds to update your account as follows:

				<ol>
					<li <?=$email_helper->orderedListStyle();?>>Click the button below and log in to CityBlast</li>
					<li <?=$email_helper->orderedListStyle();?>>Click on the Invoices tab</li>
					<li <?=$email_helper->orderedListStyle();?>>Click on the Update My Credit Card button</li>
					<li <?=$email_helper->orderedListStyle();?>>Update your payment information</li>
				</ol>
								
			
			</p>
			
			<br/>
			
			<? $email_helper->contentP(); ?>It’s that easy!</p>
			
			<? $email_helper->contentP(); ?>Also, feel free to call us if you're having any trouble - 1-888-712-7888.</a></p>
			

			

			

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?php echo APP_URL."/member/payment?rebill=1"; ?>" style="<? $email_helper->actionButtonStyle();?>">Update My Card</a>
			</p>   
	    
		</div>  		

		
	</div>	
	


<? include "email-footer-NEW.html.php"; ?>