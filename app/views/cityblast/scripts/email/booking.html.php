<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>NEW LEAD</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Congrats!  You've Got a New Lead</h1>
	

		<? $email_helper->contentP(); ?>Hi <?php echo $this->booking_agent->first_name;?>!</p>
				
			<? $email_helper->contentP(); ?>One of your friends or followers, <b><?php echo $this->name;?></b>, would like more info about the property below.  They have asked that you contact them now to discuss!</p> 
			

			
			<? $email_helper->contentP(); ?>Here's the property your friend is interested in:</p>
			
			<? $email_helper->contentP("text-align: center;"); ?><a href="<?=APP_URL . "/listing/view/". $this->listing->id;?>?src=invc"><?php echo $this->listing->street; ?> <?php echo $this->listing->city; ?></a></p>
	
	
				
		
			<? $email_helper->contentP(); ?>Contact info provided by <?php echo $this->name;?>:</p>
		
		

			<? $email_helper->contentP(); ?>

				<a href="mailto:<?=$this->email;?>" <?=$email_helper->linkStyle();?>><?php echo $this->email;?></a>
				
			</p>
			
			

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL . "/listing/view/". $this->listing->id;?>?src=invc" style="<? $email_helper->actionButtonStyle();?>">View <?php echo $this->listing->street; ?></a>
			</p>   
	    
		</div>  		

		
	</div>	
	



	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>LISTING AGENT</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


			<? $email_helper->contentP(); ?>If you need more information about the listing, feel free to contact <?=$this->listing_agent_name; ?>.  They're the listing agent.</p>
			
			
			<? $email_helper->contenth3(); ?><?=$this->listing_agent_name; ?></h3>
		
			<? $email_helper->contentP(); ?>				
				Phone: <? if (isset($this->listing_agent_phone) && !empty($this->listing_agent_phone)) echo $this->listing_agent_phone; else echo "No phone number on record"; ?><br/>
				Email: <? if (isset($this->listing_agent_email) && !empty($this->listing_agent_email)) echo "<a href='mailto:".$this->listing_agent_email."' style='color:#3D6DCC;'>".$this->listing_agent_email."</a>"; else echo "No email on record"; ?>						
			</p>
		
	
	    
		</div>  		

		
	</div>	
	




<? include "email-footer-NEW.html.php"; ?>