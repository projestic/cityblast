		</td>
	</tr>

	<tr>
		<td style='color: #666666; font-size:16px; text-align: center;
			font-family:"Myriad Pro", Arial; color: #7B7B7B; font-weight: 600; letter-spacing: -0.01em; vertical-align: baseline;			
			'>

			<div style="margin-top: 45px;"></div>

				<?=COMPANY_NAME;?> agents reached
				<span><strong style="color: #ff0000 !important;"><?=number_format($this->dailyReach());?></strong></span> 
				potential home buyers today.			

			<div style="margin-top: 25px;"></div>
			
 		</td>
	</tr>
	   			
		
	<tr>
		<td>
			
			
			<div style='
					    	border: 1px solid #C7C9C8;
					    	border-radius: 5px 5px 5px 5px;
					    	color: #7B7B7B;
					    	font-family: "myriad-pro-n4","myriad-pro","Helvetica Neue",Helvetica,Arial,sans-serif;
					    	line-height: 19px;
					    	padding: 15px;
					    	position: relative;
					    	clear: both;
					    	width: 570px;
					    	font-size: 14px;
					    	text-align: center;
					    	'>
		    			
		    			<a href="<?=APP_URL;?>/member/join" <? $email_helper->footerLinkStyle(); ?>>Register Free</a> &#176; 
		    			<a href="<?=APP_URL;?>/member/how-cityblast-works" <? $email_helper->footerLinkStyle(); ?>>How CityBlast Works</a> &#176; 
		    			<a href="<?=APP_URL;?>/index/refer-an-agent" <? $email_helper->footerLinkStyle(); ?>>Refer An Agent</a> &#176;  
		    			<a href="<?=APP_URL;?>/index/faq" <? $email_helper->footerLinkStyle(); ?>>FAQ</a> &#176;  
		    			<a href="<?=APP_URL;?>/index/guarantee" <? $email_helper->footerLinkStyle(); ?>>Our Guarantee</a>
		    			
		    		</div>
		    	
 		</td>
	</tr>

	<tr>
		<td style="width: 600px; text-align: center; padding-top:15px;">
			
			
			<div style='

				    	color: #7B7B7B;
				    	font-family: "myriad-pro-n4","myriad-pro","Helvetica Neue",Helvetica,Arial,sans-serif;	
				    		   			
				    	line-height: 19px;
				    	padding: 15px;
				    	position: relative;
				    	clear: both;
				    	width: 570px;
				    	font-size: 12px;
				    	text-align: center;
				    	'>
					
					<?=COMPANY_ADDRESS;?>, <?=COMPANY_CITY;?>, <?=COMPANY_PROVINCE;?>, <?=COMPANY_POSTAL;?>, <?=COMPANY_COUNTRY;?> 
				
				</div>						  
 		</td>
	</tr>

	
</table>
</body>
</html>