<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>PROPERTY INFO</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Your Friend Recommended a Property</h1>
	

	
				
			<? $email_helper->contentP(); ?>Your friend thought you might like the following property listing:</p> 
			
			<? $email_helper->contentP("text-align: center;"); ?><a href="<?=APP_URL . "/listing/view/". $this->listing->id;?>?src=invc"><?php echo $this->listing->street; ?> <?php echo $this->listing->city; ?></a></p>
	
	
	
	
			<? $email_helper->contentP(); ?>Click on the link below and have a look!</p>
	
				
			

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL . "/listing/view/". $this->listing->id;?>?src=invc" style="<? $email_helper->actionButtonStyle();?>">Check It Out!</a>
			</p>   
	    
		</div>  		

		
	</div>	
	




	<? if(isset($this->comments) && !empty($this->comments)) : ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>COMMENTS</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


			<div style="height: 10px;"></div>
			<? $email_helper->contentP(); ?><?=$this->comments;?></p>
				

	    
		</div>  		

		
	</div>	
	

	
	
	<? endif; ?>




<? include "email-footer-NEW.html.php"; ?>



<? /**************

<? include "email-header.html.php"; ?>

	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Your friend recommended a property.</em></h1>
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 12px; width: 600px;">Your friend thought you might like the following property listing.  Are you looking for 
	a property?  Click on the link below and search thousands of properties on <?=COMPANY_NAME;?> now.</p> 



	<? include "show-listing.html.php"; ?>

<?
/*	

	<?php if (!empty($this->recent_blasts)) : ?>
	
		<table cellpadding="0" cellspacing="0" border="0" width="600px;">
		<tr>
	    	<td>
	          <h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px;"><em>More Great Properties</em></h2>
	        	<table border="0" cellpadding="5" cellspacing="0" style="border-collapse:collapse; margin: 0 auto; border: 1px solid #eee; margin:0 0 20px;" width="600">
	            	<tr>
					<?php foreach ($this->recent_blasts as $blast) : ?>
						<td align="center"><a href="<?=APP_URL;?>/listing/view/<?php echo $blast->id ?>"><img src="<?=APP_URL;?><?php echo $blast->image ?>" alt="Image of <?php echo $blast->street ?>" width="136" height="90" border="0" /></a></td>
					<?php endforeach ?>
	                </tr>
	                <tr>
					<?php foreach ($this->recent_blasts as $blast) : ?>
						<td align="center"><a href="<?=APP_URL;?>/listing/view/<?php echo $blast->id ?>" style="color:#3D6DCC; font-size:11px; text-decoration:none;"><?php echo $blast->street ?></a></td>
					<?php endforeach ?>
	                </tr>
	            </table>
	        </td>
	    </tr>
	    
	    </table>
	    
	<?php endif ?>



	<h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px; padding-top: 25px;"><em>Have You Considered Blasting?</em></h2>
	<p style="width: 600px;">When you Blast your listings, you harness the cumulative networking power of hundreds of agents throughout your city, and sell your property fast. You reach tens of thousands of local buyers, by advertising your listing on other local agents' personal Facebook, Twitter and LinkedIn accounts for all of their friends to see. And best of all? It's easy.</p>



	<table cellpadding="0" cellspacing="0" border="0" width="600px;" style="margin-top: 15px;">
	
		<tr>
	    	<td bgcolor="#f2f2f2" align="center" style=" border-top:1px solid #c4c4c4; border-bottom:1px solid #c4c4c4; background:#f2f2f2;">
	        	<p style="width: 600px; font-size:18px; margin: 15px 0; font-family:'Myriad Pro', Arial;"><strong>Harness the power of Social Media and 
	        		<em><a href="<?=APP_URL;?>/blast" style="color:#0000FF; text-decoration:underline;">sell your listing fast.</a></em></p>
	        </td>
	    </tr>
	
	</table>


	

	<h2 style="font-family:'Myriad Pro', Arial; border-bottom: solid 1px #e5e5e5; margin-top: 25px; padding-bottom: 6px;"><em>What Does A Blast Look Like?</em></h2>
	<p style="width: 600px;">A Blast is a link to your listing that is published to literally hundreds of real estate agents Social Media sites. A typical blast looks something like this:</p>


	<? include "blast-preview.html.php"; ?>

	
	<table cellpadding="0" cellspacing="0" border="0" width="600px;" style="margin-top: 25px;">
	
		<tr>
	    	<td bgcolor="#f2f2f2" align="center" style=" border-top:1px solid #c4c4c4; border-bottom:1px solid #c4c4c4; background:#f2f2f2;width: 600px;">
	        	<p style="width: 600px; font-size:18px; margin: 15px 0; font-family:'Myriad Pro', Arial;"><strong>Ready to sell your listing fast? 
	        		<em><a href="<?=APP_URL;?>/blast" style="color:#0000FF; text-decoration:underline;">Click here</a></em></a> to Blast now.</strong></p>
	        </td>
	    </tr>
	
	</table>


	<? include "email-signature.html.php"; ?> 	

	<? include "email-footer-menu.html.php"; ?> 

	<? include "franchise-footer.html.php"; ?> 
    
<? include "email-footer.html.php"; ?>

************/ ?>