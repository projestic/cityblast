<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>

	<? /*
	<? $email_helper->openBox(); ?>
		

		<? $email_helper->openCap(); ?>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?><?=COMPANY_NAME;?> Invoice</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>			
		</div>
	
		<? $email_helper->openContentDiv(); ?>



		<? $email_helper->contentH1(); ?>Your <? echo date("F"); ?> Invoice.</h1>


		<? $email_helper->contentP(); ?>Hello <?= $this->member->first_name;?>,</p>
			
		<? $email_helper->contentP(); ?>Your <?=COMPANY_NAME;?> is turning your Facebook, Twitter and LinkedIn into a powerful and incredibly powerful real estate machine, keeping your image professional and current, and generating 
			vastly more business from your personal online network.
			By keeping your social media presence consistently up-to-date with first class real estate content and gorgeous local listings, your friends and acquaintances now know you're a trusted professional.
			</p>
		

	       			
		</div>
	
	</div>
	*/ ?>	


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>Invoice Number: #<?=number_format(sprintf("%03d",$this->invoice_number), 0, '.', '-');?></h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>
	

	
		<? $email_helper->openContentDiv(); ?>


			<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr>

					<td height="24" colspan="2" style="font-size: 16px; padding: 15px;"><strong>Invoice Date: </strong><? echo date("F d, Y"); ?></td>
				</tr>
				<tr>
					<td width="50%" valign="top" style="font-size: 16px; padding: 15px;">
						<strong><?=$this->member->first_name;?> <?=$this->member->last_name;?></strong><br />
						
						<?/*
						<? if(isset($this->member->address) && !empty($this->member->address)  && $this->member->address != "empty" && $this->member->address != "undefined") echo $this->member->address;?>
						<? if(isset($this->member->address2) && !empty($this->member->address2)  && $this->member->address2 != "empty" && $this->member->address2 != "undefined") echo $this->member->address2;?><br />
						<? if(isset($this->member->city) && !empty($this->member->city)  && $this->member->city != "empty" && $this->member->city != "undefined") echo $this->member->city;?>,
						<? if(isset($this->member->state) && !empty($this->member->state)  && $this->member->state != "empty" && $this->member->state != "undefined") echo $this->member->state;?>
						<? if(isset($this->member->zip) && !empty($this->member->zip)  && $this->member->zip != "empty" && $this->member->zip != "undefined") echo $this->member->zip;?><br />
						<? if(isset($this->member->country) && !empty($this->member->country) && $this->member->country != "empty" && $this->member->country != "undefined") echo $this->member->country;?>
						*/ ?>
					</td>
					<td width="50%" valign="top" style="font-size: 16px; padding: 15px;">
						<strong><?=COMPANY_NAME;?></strong><br />
						<?=COMPANY_ADDRESS;?><br />
						<?=COMPANY_CITY;?>, <?=COMPANY_PROVINCE;?><br />
						<?=COMPANY_POSTAL;?><br />
						<?=COMPANY_COUNTRY;?>
					</td>
				</tr>
				<tr>
					<td height="30"></td>
					<td></td>
				</tr>
			</table>


			<br /><br />
			
			
			<table width="600" border="0" cellspacing="0" cellpadding="0" class="datatable">
				<tr>
					<th width="240" align="left" style="padding: 10px;">Purchase Item</th>
					<th width="100" style="padding: 10px;">Quantity</th>
					<th width="100" align="right" style="padding: 10px;">Unit Price</th>
					<th width="100" align="right" style="padding: 10px;">Item Total</th>
				</tr>
				<tr>
					<td style="padding: 10px;">
						<?=COMPANY_NAME;?> Social Expert
						<br/>
						<span style="font-size: 11px; font-color: #7b7b7b;">
							<? $hours = $this->member->frequency_id * 4 * 0.5; ?>
						(<?=$hours;?> hrs @ $29.99) = $<span style="text-decoration: line-through;"><?=round(($hours * 29.99),2); ?></span>
						<br/>Monthly Package: Only $<?=$this->member->price;?>
					</td>
					<td align="center" style="padding: 10px;">1</td>
					<td align="right"  style="padding: 10px;">
			
							$<?=number_format($this->amount, 2);?>
					</td>
					<td align="right" style="padding: 10px;">
							$<?=number_format($this->amount, 2);?>
					</td>
				</tr>
				<tr>
					<td  style="padding: 10px;"></td>
					<td  style="padding: 10px;"></td>
					<td align="right"  style="padding: 10px;"><strong>Subtotal</strong></td>
					<td align="right"  style="padding: 10px;">
							$<?=number_format($this->amount, 2);?>
					</td>
				</tr>
				<tr>
					<td style="padding: 10px;"></td>
					<td style="padding: 10px;"></td>
					<td align="right" style="padding: 10px;"><strong>Tax (<?php echo ($this->taxes*100); ?>%)</strong></td>
					<td align="right" style="padding: 10px;">
						<? $tax_amount = $this->amount * $this->taxes; ?>
						$<?=number_format( $tax_amount , 2);?>
					</td>
				</tr>
				<tr>
					<td  style="padding: 10px;"></td>
					<td  style="padding: 10px;"></td>
					<td align="right"  style="padding: 10px;"><strong>Total</strong></td>
					<td align="right"  style="padding: 10px;">
						<strong>$<?=number_format( $this->amount + $tax_amount , 2);?></strong>
					</td>
				</tr>
			</table>


		</div>
	</div>
	
		

<?/**

	<h2 style="font-family:'Myriad Pro', Arial;  border-bottom: solid 1px #e5e5e5; padding-bottom: 6px; padding-top: 25px;"><em>Need more help?</em></h2>
	<p style="margin-top: 10px; margin-bottom: 10px;">If you are having problems receiving your Blasts, feel free to drop us a line at: <a href="mailto:<?=HELP_EMAIL;?>" style="color: #3D6DCC;"><?=HELP_EMAIL;?></a>. We're here to help.</p> 

	<? include "email-signature.html.php"; ?> 
	<? include "email-footer-menu.html.php"; ?> 

	<? include "cityblast-footer.html.php"; ?> 

<? include "email-footer.html.php"; ?>

***/ ?>

<? include "email-footer-NEW.html.php"; ?>
