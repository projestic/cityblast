<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>PROPERTY INQUIRY RECEIVED!</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>Your Request Was Sent</h1>
	

		<? $email_helper->contentP(); ?>Hi <?php echo $this->name;?>!</p>
				
			<? $email_helper->contentP(); ?>We just wanted to confirm that your request has been sent.  You should be hearing back from <b><?php echo $this->booking_agent->first_name;?> <?php echo $this->booking_agent->last_name;?></b> soon regarding your property of interest.  
			In the meantime, we thought we'd pass along their contact info in case you don't have it currently.  Feel free to get in touch with them directly if you prefer.  And here's to a happy property hunt!</p> 
			
	
	
			<? $email_helper->contentP("text-align: center;"); ?><a href="<?=APP_URL . "/listing/view/". $this->listing->id;?>?src=invc"><?php echo $this->listing->street; ?> <?php echo $this->listing->city; ?></a></p>
	
	
			<? $email_helper->contentP(); ?>In case you want to get in touch directly, here's everything you need:</p>
		
		
			<? $email_helper->contentH3(); ?><?php echo $this->booking_agent->first_name;?> <?php echo $this->booking_agent->last_name;?></h3>

			<? $email_helper->contentP(); ?>
				<?php echo $this->booking_agent->phone;?>
				
				<br />
				<?
					$email = $this->booking_agent->email;
					if(isset($this->booking_agent->email_override) && !empty($this->booking_agent->email_override)) $email = $this->booking_agent->email_override;
				?>
				

				<a href="mailto:<?=$email;?>" <?=$email_helper->linkStyle();?>><?php echo $email;?></a>
				
			</p>
			

			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?=APP_URL . "/listing/view/". $this->listing->id;?>?src=invc" style="<? $email_helper->actionButtonStyle();?>">View <?php echo $this->listing->street; ?></a>
			</p>   
	    
		</div>  		

		
	</div>	
	


<? include "email-footer-NEW.html.php"; ?>