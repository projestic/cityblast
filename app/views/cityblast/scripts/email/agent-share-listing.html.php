<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>LISTING SHOWCASE</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
	
		</div>
	
		<? $email_helper->openContentDiv(); ?>

 			<? $email_helper->contentH1(); ?>Your Agent is a Rock Star</h1>
			

			<? $email_helper->contentP(); ?>Congrats!  Your real estate agent, <b><?= $this->listing->member->first_name;?></b>, just gave your home a massive online marketing push. </p>

			<? $email_helper->contentP("text-align: center;"); ?><a href="<?=APP_URL . "/listing/view/". $this->listing->id;?>?src=invc"><?php echo $this->listing->street; ?> <?php echo $this->listing->city; ?></a></p>
						
			
			<? $email_helper->contentP(); ?>Always a savvy agent, we wanted to let you know that <?= $this->listing->member->first_name;?> just showcased your property listing through our incredible Facebook, 
			Twitter and LinkedIn network.  It will be advertised to agents and buyers throughout your area via all the popular social media channels over this whole week, 
			helping to generate a faster sale, and a higher price for your home.</p>
	
			<? $email_helper->contentP(); ?>If you'd like to view the property listing, simply click the button below and have a look - and great work in choosing an agent who is smart and tech-savvy!</p>
			

			
			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
	
				<a href="<?=APP_URL;?>/listing/view/<?=$this->listing->id;?>" style="<? $email_helper->actionButtonStyle();?>">Share Your Listing</a>
			</p>   
	    
	       			
		</div>
	
	</div>
		


<? include "email-footer-NEW.html.php"; ?>


<?/*******************
<? include "email-header.html.php"; ?>


	<h1 style="font-family:'Myriad Pro', Arial; font-size: 25px;"><em>Your listing just reached thousands of buyers.</em></h1>
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 12px; margin-bottom: 10px;">
	Your agent, <?=$this->listing->member->first_name;?> <?=$this->listing->member->last_name;?>, just Blasted your property listing using <?=COMPANY_NAME;?>'s Blasting service.  When they Blast your listing with <?=COMPANY_NAME;?>, they advertise your property to tens of thousands of 
	local buyers through Facebook, Twitter and LinkedIn.  <?=COMPANY_NAME;?> is the new way that the real estate industry is using social media, and the way that the world's top agents are selling properties 
	faster than ever before.  View your listing below.</p> 


	<h2 style="font-family:'Myriad Pro', Arial; padding-top: 25px; padding-bottom: 6px; margin: 0px;"><em>Your integrated Social Media Listing.</em></h2>
	<? include "show-listing.html.php"; ?>





	<h2 style="font-family:'Myriad Pro', Arial; border-bottom: solid 1px #e5e5e5; padding-top: 25px; padding-bottom: 6px; margin: 0px;"><em>Your Blast has been scheduled.</em></h2>
	<p style="margin-bottom: 20px;">When <?=$this->listing->member->first_name;?> <?=$this->listing->member->last_name;?> Blasts your listings, they harness the cumulative networking power of social media users throughout your city, to sell your property fast. Your listing 
	reaches tens of thousands of local buyers, by being advertised on real people's Facebook, Twitter and LinkedIn accounts for all of their friends to see.</p>

	<? include "blast-preview.html.php"; ?>

	


	<h2 style="font-family:'Myriad Pro', Arial; border-bottom: solid 1px #e5e5e5; padding-top: 25px; padding-bottom: 6px; color: #000000;"><em>Sold fast?  <?=COMPANY_NAME;?>.</em></h2>
	<p style="margin-bottom: 20px;">Happy selling!  We look forward to hearing your success story.  Please feel free to contact us with questions, comments or kudos via <a href="<?=APP_URL;?>" style="color: #3D6DCC;"><?=COMPANY_WEBSITE;?></a>.</p>
	




	<? include "email-signature.html.php"; ?> 	

	<? include "email-footer-menu.html.php"; ?> 

	<? include "franchise-footer.html.php"; ?> 
    
<? include "email-footer.html.php"; ?>
****************/ ?>