<? $email_helper = $this->email(); ?>

	
<? include "email-header-NEW.html.php"; ?>


	<? $email_helper->openBox(); ?>
		
		<? $email_helper->openCap(); ?>

			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			<? $email_helper->openH4(); ?>TRY CITYBLAST</h4>
			<h4 style="height: 15px; margin: 0;">&nbsp;</h4>
			
		</div>

		<? $email_helper->openContentDiv(); ?>


 			<? $email_helper->contentH1(); ?>A Special Invite from <?= $this->member->first_name;?> <?= $this->member->last_name;?></h1>
	

			<? $email_helper->contentP(); ?>Hi fellow real estate agent!</p>
			
		<? $email_helper->contentP(); ?>Your friend <?= $this->member->first_name;?> is a member of our site, and has sent you this special note to offer you a <b>FREE 14-Day Trial</b> of our <b>Social Expert</b> service.</p>
			
			<? $email_helper->contentP(); ?>
				Our Experts act as your social media marketing assistants, and post hot new real estate articles, videos and information to your Facebook, Twitter and LinkedIn 
				accounts each day - so you don't have to!</p>
			
			<? $email_helper->contentP(); ?>Click the button below to check out some examples, and read more about how our service works.  We'd love to show you what we can do!</p>
		


			<p style="text-align: right; margin-right: 15px; margin-bottom: 0;">
				<a href="<?php echo APP_URL."/listing/view/".$this->listing->id;?>?ref=cmts_email" style="<? $email_helper->actionButtonStyle();?>">Check It Out!</a>
			</p>   
	    
		</div>  		

		
	</div>	
	



<? include "email-footer-NEW.html.php"; ?>