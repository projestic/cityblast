<style type="text/css">
html, body{
	background: #fff !important;
}
@media (min-width: 1200px){
	.container,
	.span12{
		width: 1024px;
	}
}

</style>
<div class="brokerPage">
	<div class="row-fluid">
		<?/*<img class="logo" src="/images/broker-blast-logo.png" />*/?>
		<img src="/images/city-blast_logo.png" class="logo">
	</div>
	
	<div class="offerWrapper row-fluid">
		<div class="offerBlackWrapper">
			<div class="offerWhiteWrapper">
				<div class="offerGrayWrapper">
					<div class="offer clearfix">
						<div class="span9">
							<h1>Get the Marketing Savvy of an In-House Social Media Assistant - <span class="orange">Without the Enormous Cost</span></h1>
							<p>CityBlast's Social Experts guarantee your Real Estate business is never embarrassed<br/>by a neglected Twitter, Facebook or LinkedIn account again.</p>
						</div>
						<div class="span3">
							<img src="/images/offer-stamp.png" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="tryFree row-fluid">
		<p class="pull-left">Begin Your <span>Free 14-Day Trial,</span> and get our Social Experts working for you.</p>
		<a href="/member/index?mtype=broker" class="pull-right btn btn-success btn-large">Try Us Free!</a>
	</div>
	
	<div class="row-fluid whySocialMedia">
		<div class="span6">
			<ul>
				<li class="searches">
					<h3>Over 90% of home searches begin online</h3>
					<p>That means over 90% of your prospects are starting their real estate journey without your company. You must stay active and not miss out on these clients!</p>
				</li>
				<li class="timeSpend">
					<h3>people spend 3x more time on facebook vs. any other site</h3>
					<p>If you're going to keep in front of your network consistently and look professional, then social media is the absolute best place to do so!</p>
				</li>
				<li class="expertsRecommend">
					<h3>experts recommend spending 1 hour every day on social media marketing</h3>
					<p>Who has an hour every day?! Let our experts do the heavy lifting for you, and keep you looking fresh and up-to-date on your Social accounts.</p>
				</li>
			</ul>
		</div>
		<div class="span6 workForYou">
			<h2>Let our experts do the work for you.</h2>
			<p>We're like your in-house Virtual Social Media Assistant.</p>
			<ul>
				<li>We manage your social accounts, marketing your company</li>
				<li>Relevant real estate articles, videos and information</li>
				<li>Only trusted sources like HGTV, CNN and WSJ</li>
				<li>As often as you choose - up to 7 posts/week</li>
				<li>We update Facebook, Twitter and LinkedIn</li>
				<li>One low cost vs. hiring someone locally</li>
			</ul>
			<div class="dontMiss">
				<span>Dont't Miss Out..</span>
				<a href="/member/index?mtype=broker" class="btn btn-success btn-large"><span>Start Your</span><br/>Free 14-Day Trial</a>
			</div>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span4">
			<div class="testimonial">
				<h4>Great Service</h4>
				<p>"I use CityBlast to help me market. Not only are they the heart of my online platform, but also help me to sell my clients' listings quickly, and to achieve excellent prices. CityBlast is an incredible service for both beginners and top agents, and I highly recommend them."</p>
				<div class="clearfix">
					<img class="userImg pull-left" src="/images/user-thumb-img2.jpg" />
					<div class="userInfo">
						<span class="name">Sheree Cerqua</span>
						<span>Ontario's #1 Individual Agent</span>
						<span>Royal LePage</span>
					</div>
				</div>
				<span class="arrow"></span>
			</div>
		</div>

		<div class="span4">
			<div class="testimonial">
				<h4>Quick and Painless</h4>
				<p>In the Real Estate profession it is difficult to know which of the latest and greatest tools will provide the most bang for your buck. CityBlast makes sending your listing to your social networks quick and painless; it's something everyone should explore.</p>
				<div class="clearfix">
					<img class="userImg pull-left" src="/images/user-thumb-img3.jpg" />
					<div class="userInfo">
						<span class="name">Gordon Wallace</span>
						<span>Marketing and Technology Expert</span>
						<span>Lone Wolf Real Estate Technology</span>
					</div>
				</div>
				<span class="arrow"></span>
			</div>
		</div>
		
		<div class="span4">
			<div class="testimonial">
				<h4>Awesome Experts</h4>
				<p>"My friends and previous colleagues noticed my Facebook updates right away. In only my first week using CityBlast, I received a call from a family friend who said she'd seen my Facebook and wanted to list their home with me. The experts are awesome!"</p>
				<div class="clearfix">
					<img class="userImg pull-left" src="/images/user-thumb-img.jpg" />
					<div class="userInfo">
						<span class="name">Leila Talibova</span>
						<span>Top Rookie Agent</span>
						<span>Home Life Realty</span>
					</div>
				</div>
				<span class="arrow"></span>
			</div>
		</div>

	</div>
	
	<div class="tryFree black row-fluid">
		<p class="pull-left">Begin Your <span>Free 14-Day Trial,</span> and get our Social Experts working for you.</p>
		<a href="/member/index?mtype=broker" class="pull-right btn btn-success btn-large">Try Us Free!</a>
	</div>
</div>



<? if (APPLICATION_ENV == 'cityblast'): ?>

<script type="text/javascript">
	//<![CDATA[
	var trendVar = "www.cityblast.com";
	var dcsidVar = "dcs6p84o900000w0yf68kq216_9x3g";
	var _tag=new WebTrends();
	_tag.dcsGetId();
	//]]>
</script>
<script type="text/javascript">
	//<![CDATA[
	_tag.dcsCustom=function(){
	// Add custom parameters here.
	//_tag.DCSext.param_name=param_value;
	}
	_tag.dcsCollect();
	//]]>
</script>
<noscript>
	<div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://statse.webtrendslive.com/dcs6p84o900000w0yf68kq216_9x3g/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=9.3.0&amp;WT.dcssip=www.city-blast.com"/></div>
</noscript>
<!-- END OF SmartSource Data Collector TAG -->

<? endif; ?>
	
