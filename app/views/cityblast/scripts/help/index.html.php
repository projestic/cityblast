<div class="contentBox questionContainer">
	<div class="contentBoxTitle">
		<h1>Need Help With Your Account Settings?</h1>
	</div>

	<p>You've come to the right place! We've created some quick and simple 60 second videos that will help you with <i>every</i> aspect of setting up your account.</p>





	<h3 style="margin-top: 25px;">General Settings</h3>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I want to change the photo that displays to my clients, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Changing your photo is a snap. Learn how to <a href="/help/addphoto">add or change your photo</a> in this 30 second video.</p>
	</div>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I'm confused by some of the statistics on my Dashboard, what do they mean?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">If you're confused or unsure about some of the statistics in your Dashboard, watch this quick video that <a href="/help/dashboard">explains your Dashboard statistics</a>.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How do I change my credit card or billing information on <?=COMPANY_NAME;?>?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Changing your billing information is quick and easy. Please watch this quick video on <?=COMPANY_NAME;?> <a href="/help/updatecc">changing your Credit Card information</a> to learn how.</p> 
	</div>
	
	

	<h3 style="margin-top: 25px;">Content</h3>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How do I change the type of content my <i>Social Expert</i> is posting?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Telling your <i>Social Expert</i> what type of content to post on your behalf is a snap! Watch this quick video on <a href="/help/customcontent">changing your Content settings</a> to learn how.</p> 
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>My <i>Social Expert</i> is posting too little or too much , how do I change that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
			<p  class="answer">Telling your <i>Social Expert</i> how much or how little you want them to post takes only a minute. Watch this quick video and tell your <i>Social Expert </i> <a href="/help/frequency">to turn up or turn down</a> how often they post on your behalf.</p>
	</div>
	
	
	<?/***********
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>How do I change the type of content my <i>Social Expert</i> is posting?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		
		<p class="answer">Telling your <i>Social Expert</i> what type of content to post on your behalf is a snap! Watch this quick video on <a href="/help/customcontent">changing your Content settings</a> to learn how.</p>
	</div>
	****/ ?>

	<h3 style="margin-top: 25px;">Adding Additional Accounts</h3>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I want to add my Fanpage my account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Adding your Fanpage is a breeze, just watch this quick video on <a href="/help/addfanpage">setting up your Fanpage</a> to learn how.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I want to add Twitter to my account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Adding Twitter is a breeze, just watch this quick video on <a href="/help/addtwitter">setting up your Twitter account</a> to learn how.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I want to add LinkedIn to my account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Adding LinkedIn is a breeze, just watch this quick video on <a href="/help/addlinkedin">setting up your LinkedIn account</a> to learn how.</p>
	</div>
	
	

	<h3 style="margin-top: 25px;">Resetting Accounts</h3>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>My <i>Social Expert</i> asked me to reset my Fanpage, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Resetting your Fanpage is easy, just watch this quick video on <a href="/help/fanpagereset">resetting up your Fanpage account</a> to learn how.</p>
	</div>
	
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>My <i>Social Expert</i> asked me to reset my Twitter account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Resetting your Twitter account is easy, just watch this quick video on <a href="/help/twitterreset">resetting up your Fanpage account</a> to learn how.</p>
	</div>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>My <i>Social Expert</i> asked me to reset my LinkedIn account, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Resetting your LinkedIn account is easy, just watch this quick video on <a href="/help/linkedinreset">resetting up your Fanpage account</a> to learn how.</p>
	</div>
	
	

	<h3 style="margin-top: 25px;">Listings</h3>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I want to feature my Listing on the walls of other members, how do I do that?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Featuring your Listing on the walls of other agents in your area is as simple as filling out a form. Watch this 60 second video to learn how to <a href="/help/listings">share your listing</a> on <?=COMPANY_NAME;?>.</p>
	</div>

	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I'd like to "Reblast" my Listing. Is that possible?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer">Yes, "reblasting" your listing to other agents in your area is possible after 7 days. Learn more about <a href="/help/reblasting">reblasting your listing</a> in this <a href="/help/reblasting">short video</a>.</p>
	</div>
			
	<div class="questionWrapper">
		<a class="question" href="javascript:void(0);">
			<h5>I received a "booking" request, how do I view that on <?=COMPANY_NAME;?>?</h5>
			<img src="/images/new-images/transparent.png"/>
		</a>
		<p class="answer"><?=COMPANY_NAME;?> carefully monitors any booking requests you receive from friends, family and clients. This short video tutorial shows you exactly how to <a href="/help/bookings">view all of the booking requests</a> you've ever received from <?=COMPANY_NAME;?>.</p>	
	</div>
		
</div>


<div class="contentBox">
	<div class="clearfix">
		<div class="pull-left tryDemoTxt">
			<h3 style="font-size: 26px;">Need more help?</h3>
			<h4>Our <i>Social Experts</i> are on standby.</h4>
		</div>
		<div class="pull-right">
			<a class="uiButtonNew tryDemo" href="mailto: info@cityblast.com">Talk To Us.</a>
		</div>	
	</div>		
</div>

<div class="contentBox">
	<div class="clearfix">
		<h3 style="margin-bottom:20px">Members helping members</h3>
		<fb:comments xid="member_join" href="<? echo APP_URL;?>/index/faq"></fb:comments>
	</div>		
</div>

<script type="text/javascript">
	$(document).ready(function() {
		// Resize FB comments block to width of the borwser window
		setTimeout(function() {
		  resizeFacebookComments();
		}, 1000);
		$(window).on('resize', function() {
		  resizeFacebookComments();
		});
		function resizeFacebookComments() {
		  var src = $('.fb_iframe_widget iframe').attr('src').split('width='),
		      width = $('.fb_iframe_widget').width() - 10;
		  $('.fb_iframe_widget iframe').attr('src', src[0] + 'width=' + width);
		}

		// Adding event listening on comment
		FB.Event.subscribe('comment.create', function(response)
		{
		    var commentQuery = FB.Data.query("SELECT object_id,id,text, fromid FROM comment WHERE post_fbid='"+response.commentID+"' AND object_id IN (SELECT comments_fbid FROM link_stat WHERE url='"+response.href+"')");
		    var userQuery = FB.Data.query("SELECT name FROM user WHERE uid in (select fromid from {0})", commentQuery);
		
		    FB.Data.waitOn([commentQuery, userQuery], function()
		    {
				var commentRow = commentQuery.value[0];
				var userRow = userQuery.value[0];
				
				var commentId = commentRow.id;
				var name = userRow.name;
				var fuid = commentRow.fromid;
				var text = commentRow.text;
				var mid	= <?php echo (empty($_GET['mid'])) ? 0 : intval($_GET['mid']);?>;
				var url = '/comments/broadcast';
				var data = 'name='+name+'&fuid='+fuid+'&text='+text+'&mid='+mid+'&referring_url=<?=$this->current_url?>';
		
				$.ajax({
					type: 'POST',
					url: url,
					data: data,
					success: function(response){
						// do nothing
					},
					dataType: 'json'
				});		
		    });
		});
	});
</script>