<?php

require_once 'LibAutoloader.php';

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initAppConfig()
	{
		Zend_Registry::set('appConfig', $this->getOptions());
	}
	
	#
	# This needs to go first, otherwise it will puke if the FrontController already has a dispatcher set
	#
	protected function _initDispatcher()
	{
		Zend_Controller_Front::getInstance()->setDispatcher(new Zend_Controller_Dispatcher_Extended());
	}
	
	protected function _initConfig() {
		$config = new Zend_Config($this->getOptions(), true);
		Zend_Registry::set('config', $config);
		return $config;
	}

	protected function _initActiveRecord() {

		$db = $this->getOption('db');

		$content = $this->getOption('content');

		$connections = array(
			APPLICATION_ENV => "{$db['adapter']}://{$db['username']}:{$db['password']}@{$db['host']}/{$db['database']}",
		);

		if (isset($content['copy_to_env'])) {
			$copy_db_ini = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', $content['copy_to_env'], true);
			$copy_db = $copy_db_ini->db;
			$connections[$content['copy_to_env']] = "{$copy_db->adapter}://{$copy_db->username}:{$copy_db->password}@{$copy_db->host}/{$copy_db->database}";
			define('CONTENT_COPY_ENV', $content['copy_to_env']);
		}

		ActiveRecord\Config::initialize(function($cfg) use ($db, $connections)
		{
		  $cfg->set_model_directory(APPLICATION_PATH . '/models');
		  $cfg->set_connections(
			$connections,
			APPLICATION_ENV
		  );
		});
	}

	protected function _initSession() {

        $config = Zend_Registry::get('config');
        
        $db = Zend_Db::factory('pdo_mysql', array(
        	'charset'  => 'UTF8',
            'host'     => $config->db->host,
            'username' => $config->db->username,
            'password' => $config->db->password,
            'dbname'   => $config->db->database
        ));
        
        Zend_Db_Table_Abstract::setDefaultAdapter($db); 
        
        $session_config = array(
            'name'           => 'sessions',
            'primary'        => 'id',
            'modifiedColumn' => 'modified',
            'dataColumn'     => 'data',
            'lifetimeColumn' => 'lifetime'
        );

        Zend_Session::setOptions(
            array(
                'use_only_cookies'      => 'true', 
                'name'                  => 'SESSION',
                'gc_maxlifetime'        => 259200,
                'remember_me_seconds'   => 259200
            )
        );

        Zend_Session::setSaveHandler( new Zend_Session_SaveHandler_DbTable($session_config) );
        Zend_Session::start();
        
        // Zend_Registry::set('session', $session);

	}
		
	protected function _initView()
	{
		$options = $this->getOption('viewextended');
		$view = new Zend_View_Extended($options);
		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
		$viewRenderer->setView($view);
		$viewRenderer->setViewSuffix(VIEW_SUFFIX);
		Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

		// Bootstrap the layout so we can set the viewSuffix
		$this->bootstrap('layout');
		Zend_Layout::getMvcInstance()->setViewSuffix(VIEW_SUFFIX);

		Zend_Registry::set('view', $view);
		Zend_Registry::set('viewRenderer', $viewRenderer);

//		$view->addScriptPath(APPLICATION_PATH.'/views/common/scripts');
		$view->addScriptPath(APPLICATION_PATH.'/layouts/common/scripts');

	}

	#
	# This will automatically load anything we place in the lib directory
	#
	protected function _initAutoload()
	{
		Zend_Session::start();
		$loader = Zend_Loader_Autoloader::getInstance();
		$loader->pushAutoloader(new LibAutoloader());
		$loader->registerNamespace('OAuth2');
		OAuth2\Autoloader::register();
		require_once(APPLICATION_PATH . '/../vendor/Ctct/autoload.php');
		require_once(APPLICATION_PATH . '/../vendor/SimplePie/autoloader.php');
	}
	
	protected function _initCdn() {
		$config = $this->getOption('cdn');
    	if ($config['enabled']) { 
            define('CDN_URL', rtrim($config['url'], '/'));
            
            // A seperate URL for publishing allows us to use the sites own domain for serving content (e.g static.cityblast.com) without SSL
            // - this is needed so that the correct domain is displayed in facebook for example
            $url_publish = !empty($config['url_publish']) ? rtrim($config['url_publish'], '/') : CDN_URL;
            define('CDN_URL_PUBLISH', $url_publish);
            
            define('CDN_ORIGIN_STORAGE', rtrim($config['storage'], '/'));
		} else {
			define('CDN_URL', rtrim(APP_URL, '/'));
			//define('CDN_URL', "http://www.cityblast.com");
			define('CDN_URL_PUBLISH', CDN_URL);
            
            define('CDN_ORIGIN_STORAGE', realpath(APPLICATION_PATH . '/../public'));
		}
		define('TMP_ASSET_STORAGE', CDN_ORIGIN_STORAGE . '/tmp');
		define('TMP_ASSET_URL', CDN_URL . '/tmp');
	}
	
	protected function _initSAML() {
		require_once(APPLICATION_PATH . '/../public/saml/lib/_autoload.php');
	}

	protected function _initCache() {
		$frontendOptions = array('lifetime' => 7200, 'automatic_serialization' => true);
		$backendOptions = array( 'cache_dir' => APPLICATION_PATH . '/../cache');
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		Zend_Registry::set('cache', $cache);
	}

	protected function _initControllerPlugins() {
		$front = Zend_Controller_Front::getInstance();
		$front->registerPlugin(new Blastit_Affiliate_RouterPlugin());
	}

    protected function _initCdnOriginStorage() {
        $config = $this->getOption('cdn');
        if ($config['enabled'] && stristr($config['storage'], '://')) {        	
        	require_once( APPLICATION_PATH . '/../vendor/aws/aws-autoloader.php' );
            $parts = parse_url($config['storage']);
            if ($parts['scheme'] == 's3') {
            	if (!defined('AMAZON_AWS_API_KEY') || !defined('AMAZON_AWS_API_SECRET')) {
            		throw new Exception('AMAZON_AWS_API_KEY and AMAZON_AWS_API_SECRET must be defined to use the S3 wrapper.');
            	}
            	$client = Aws\S3\S3Client::factory( array('key' => AMAZON_AWS_API_KEY, 'secret' => AMAZON_AWS_API_SECRET) );
                $client->registerStreamWrapper();
            }
        }
    }


	protected function _initLog()
	{
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/../log/' . APPLICATION_ENV . '.log');

		$logger->addWriter($writer);
		Zend_Registry::set('logger', $logger);

		$GLOBALS['ACTIVERECORD_LOGGER'] = $logger;
		$GLOBALS['ACTIVERECORD_LOG'] = LOG_SQL;

	}

    protected function _initSSL() {
		if (!isset($_SERVER['HTTP_HOST']) || !isset($_SERVER['SERVER_PORT'])) {
            // Mostly likely running from console
            return;
        }
        
    	$ssl_disallow_urls = $this->getOption('ssl_disallow_urls');
    	$required          = $this->getOption('ssl_require_enabled');
    	$sni_compatibility = $this->getOption('ssl_sni_compatibility_mode');
		$is_winxp_ie 	   = false;
		$disallowed        = false;
		
		if (is_array($ssl_disallow_urls)) {
			foreach ($ssl_disallow_urls as $ssl_disallow_url) {
				if (strpos($_SERVER['REQUEST_URI'], $ssl_disallow_url) === 0) {
					$required   = false;
					$disallowed = true;
					break;
				}
			}
        }
        
		if ($sni_compatibility && isset($_SERVER['HTTP_USER_AGENT'])) {
			$test = preg_match('/MSIE [0-9+]?\.[0-9+]?b?; Windows NT 5.[1,2]/', $_SERVER['HTTP_USER_AGENT']);
			if ($test == 1) {
				$is_winxp_ie = true;
			}
		}
		
		if ($disallowed == false && $required == 1 && ($sni_compatibility != 1 || $is_winxp_ie == false) && $_SERVER['SERVER_PORT'] != '443') {
  			header('Location: https://' . $_SERVER['HTTP_HOST'] .  $_SERVER['REQUEST_URI']);
  			exit;
        } elseif (($_SERVER['SERVER_PORT'] == '443') && (($is_winxp_ie && $sni_compatibility) || $disallowed)) {
 			header('Location: http://' . $_SERVER['HTTP_HOST'] .  $_SERVER['REQUEST_URI']);
  			exit;
        }
    }
	
	public function _initAlertEmailer()
	{
		$config = $this->getOption('alert-emailer');
		
		// Ensure that all emails will have a recpient
		if (empty($config['notify']['default']) && empty($config['notify']['all'])) {
			$config['notify']['default'] = array(
				'info@cityblast.com',
				'alen@alpha-male.tv',
				'kevin@socialhp.com',
				'shane@socialhp.com',
				'shaun@cityblast.com'
			);
		}
		
		$emailer = new Blastit_AlertEmailer();
		$emailer->configure($config);
		
		Zend_Registry::set('alertEmailer', $emailer);
	}
	
	protected function _initNetworkApps()
	{
		Zend_Registry::set('networkAppFacebook', NetworkApp::find_by_network_and_registration_enabled('FACEBOOK', 1));
		Zend_Registry::set('networkAppTwitter', NetworkApp::find_by_network_and_registration_enabled('TWITTER', 1));
		Zend_Registry::set('networkAppLinkedIn', NetworkApp::find_by_network_and_registration_enabled('LINKEDIN', 1));
	}
}