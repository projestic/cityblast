<?php
class BlastReach extends ActiveRecord\Model
{
	static $table = 'blast_reach';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
	    	array('friend', 'class' => 'Friends', 'key' => 'id','foreign_key'=>'friend_id')

	);

	static $has_one = array(
		array('post', 'class' => 'Post', 'foreign_key' => 'post_id')
	);
}
?>