<?php
class Country extends ActiveRecord\Model
{
	static $table = 'countries';

    static $has_many = array(
		array('state', 'class' => 'State', 'foreign_key' => 'country_code')
	);
		

  public static function countryForCombo(){
      $countries = Country::all(array('order' => 'country_name'));
        $all_country=array();
        foreach($countries as $country){
            $all_country[$country->country_code] = $country->country_name;
        }
        unset($all_country["US"]);
        unset($all_country["GB"]);
        unset($all_country["CA"]);
        $top_country = array('US'=>'united states','GB'=>'united kingdom','CA'=>'canada');
        $all_country = array_merge($top_country,$all_country);
        return $all_country;
  }
}
?>