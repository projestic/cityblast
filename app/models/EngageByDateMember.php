<?php

class EngageByDateMember extends ActiveRecord\Model
{
	static $table = 'engage_by_date_member';
	
	public function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$start_date = date('Y-m-d', $start_time);
		$end_date = date('Y-m-d', $end_time);
		
		Logger::log(
			'Compiling engagement by date and member between ' . $start_date . ' and ' . $end_date, 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT 
				ctbdm.date, 
				ctbdm.member_id,
				ctbdm.count,
				(SELECT likes FROM facebook_stat_by_date_member WHERE date = ctbdm.date AND member_id = ctbdm.member_id) AS likes,
				(SELECT comments FROM facebook_stat_by_date_member WHERE date = ctbdm.date AND member_id = ctbdm.member_id) AS comments,
				(SELECT shares FROM facebook_stat_by_date_member WHERE date = ctbdm.date AND member_id = ctbdm.member_id) AS shares,
				0,
				(
					SELECT 
						count(id) 
					FROM post 
					WHERE 
						member_id = ctbdm.member_id AND 
						DATE(created_at) <= ctbdm.`date` AND
						result = 1 
				) AS posts,
				0,
				0,
				0,
				0,
				0
			FROM `click_stat_by_date_member` AS ctbdm
			WHERE date >= " . $db_con->escape($start_date) . "  
			AND date <= " . $db_con->escape($end_date) . "  
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		$sql = "
			SELECT 
				fsbdm.date, 
				fsbdm.member_id,
				(SELECT count FROM click_stat_by_date_member WHERE date = fsbdm.date AND member_id = fsbdm.member_id) AS clicks,
				fsbdm.likes,
				fsbdm.comments,
				fsbdm.shares,
				0,
				(
					SELECT 
						count(id) 
					FROM post 
					WHERE 
						member_id = fsbdm.member_id AND 
						DATE(created_at) <= fsbdm.`date` AND
						result = 1 
				) AS posts,
				0,
				0,
				0,
				0,
				0
			FROM `facebook_stat_by_date_member` fsbdm
			WHERE date >= " . $db_con->escape($start_date) . "  
			AND date <= " . $db_con->escape($end_date) . "  
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		$sql = "
			UPDATE `engage_by_date_member` 
			SET 
				score = (clicks + (likes * 10) + (comments * 15) + (shares * 25))
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		$sql = "
			UPDATE `engage_by_date_member` 
			SET 
				clicks_perpost = ROUND(clicks / posts, 3),
				likes_perpost = ROUND(likes / posts, 3),
				comments_perpost = ROUND(comments / posts, 3),
				shares_perpost = ROUND(shares / posts, 3),
				score_perpost = ROUND(score / posts, 3)
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}