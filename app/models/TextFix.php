<?php

class TextFix extends ActiveRecord\Model
{
	static $table = 'text_fix';
	
	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id')
	);
	
	public static function populate()
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$db_con = static::connection();
		
		$sql = "
		INSERT IGNORE INTO text_fix (row_id, `table_name`, `field_name`, listing_id, original, created_at)
			SELECT 
				l.id, 
				'listing',
				'title',
				l.id,
				l.title,
				NOW()
			FROM listing l
			LEFT JOIN text_fix_history tfh 
				ON 
					tfh.table_name = 'listing' AND
					tfh.field_name = 'title' AND
					tfh.row_id = l.id
			WHERE 
				LENGTH(title)  != CHAR_LENGTH(title) AND
				tfh.row_id IS NULL
		";
		$res = $db_con->query($sql);
		
		$sql = "
		INSERT IGNORE INTO text_fix (row_id, `table_name`, `field_name`, listing_id, original, created_at)
			SELECT 
				l.id, 
				'listing',
				'message',
				l.id,
				l.message,
				NOW()
			FROM listing l
			LEFT JOIN text_fix_history tfh 
				ON 
					tfh.table_name = 'listing' AND
					tfh.field_name = 'message' AND
					tfh.row_id = l.id
			WHERE 
				LENGTH(message)  != CHAR_LENGTH(message) AND
				tfh.row_id IS NULL
		";
		$res = $db_con->query($sql);
		
		$sql = "
		INSERT IGNORE INTO text_fix (row_id, `table_name`, `field_name`, listing_id, original, created_at)
			SELECT 
				l.id, 
				'listing',
				'description',
				l.id,
				l.description,
				NOW()
			FROM listing l
			LEFT JOIN text_fix_history tfh 
				ON 
					tfh.table_name = 'listing' AND
					tfh.field_name = 'description' AND
					tfh.row_id = l.id
			WHERE 
				LENGTH(description) != CHAR_LENGTH(description) AND
				tfh.row_id IS NULL
		";
		$res = $db_con->query($sql);
		
		$sql = "
		INSERT IGNORE INTO text_fix (row_id, `table_name`, `field_name`, listing_id, original, created_at)
			SELECT 
				ld.id, 
				'listing_description',
				'description',
				ld.listing_id,
				ld.description,
				NOW()
			FROM listing_description ld
			LEFT JOIN text_fix_history tfh 
				ON 
					tfh.table_name = 'listing_description' AND
					tfh.field_name = 'description' AND
					tfh.row_id = ld.id
			WHERE 
				LENGTH(description) != CHAR_LENGTH(description) AND
				tfh.row_id IS NULL
		";
		$res = $db_con->query($sql);
		
		$sql = "
			UPDATE 
				text_fix
			SET
				auto1 = convert(cast(convert(original using latin1) AS binary) using utf8) 
			WHERE auto1 IS NULL;
		";
		$res = $db_con->query($sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	public function fix($type)
	{

		if (!in_array($type, array('original' , 'auto1' , 'manual'))) {
			throw new \Exception('Invalid fix type');
		}
		
		$history = new TextFixHistory();
		$history->table_name = $this->table_name;
		$history->row_id = $this->row_id;
		$history->field_name = $this->field_name;
		$history->listing_id = $this->listing_id;
		$history->original = $this->original;
		$history->created_by = !empty($_SESSION['member']) ? $_SESSION['member']->id : 0;

		switch ($type) {
			case 'original':
				$history->updated = $this->original;
			break;
			case 'auto1':
				$history->updated = $this->auto1;
			break;
			case 'manual':
				$history->updated = $this->manual;
			break;
		}
		
		if ($this->table_name == 'listing') {
			$listing = Listing::find($this->row_id);
			switch ($this->field_name) {
				case 'title':
					$history->original = $listing->title;
					$listing->title = $history->updated;
				break;
				case 'message':
					$history->original = $listing->message;
					$listing->message = $history->updated;
				break;
				case 'description':
					$history->original = $listing->description;
					$listing->description = $history->updated;
				break;
			}
			if ($history->original != $history->updated) {
				$history->save();
				$listing->save();
			}
		} elseif ($this->table_name == 'listing_description') {
			$description = ListingDescription::find($this->row_id);
			$history->original = $description->description;
			$description->description = $history->updated;
			if ($history->original != $history->updated) {
				$history->save();
				$description->save();
			}
		}
		
		$this->fixed = 1;
		$this->save();
	}
}