<?php
class Promo extends ActiveRecord\Model
{
	static $table = 'promo';

	static $belongs_to = array(
		array('affiliate', 'class' => 'Affiliate', 'foreign_key' => 'affiliate_id'),
		array('franchise', 'class' => 'Franchise', 'foreign_key' => 'franchise_id')
	);
	
	public function getPriceFor($member)
	{	
		$orig_price = $member->type->getPriceFor($member);
		if (!$this->isPermittedFor($member)) return $orig_price;
		
		$result = $this->getPrice($member->type_id, $member->billing_cycle);
		
		return $result;
	}
	
	public function getPrice($member_type_id, $billing_cycle)
	{
		$member_type = MemberType::find($member_type_id);
		$orig_price = $member_type->getPrice($billing_cycle);
		
		if ($member_type->promo_codes_allowed) {
			$result = static::calculatePrice($orig_price, $this->type, $this->amount);
		} else {
			$result = $orig_price;
		}
		
		return $result;
	}
	
	public function isPermittedFor($member)
	{
		//What does this function even do?
		
		return($member->type->promo_codes_allowed);
		//return (!$member->isPaying() && $member->type->promo_codes_allowed);
	}
	
	public function apply($member)
	{	
		if ($this->isPermittedFor($member)) {
			$member->price = $this->getPriceFor($member);
			$member->promo_id = $this->id;
			if ($this->affiliate_id) $member->referring_affiliate_id = $this->affiliate_id;
		}
		return $member;
	}
	
	public static function calculatePrice($orig_price, $type, $amount)
	{
		$price = $orig_price;
		switch ($type) {
			case 'FIXED_AMOUNT' :
				$price = $orig_price -  $amount;
				break;
			case 'PERCENT' :
				$price = $orig_price - ($orig_price *  $amount / 100);
				break;
		}

		//This is the absolute lowest price we offer...
		if($price < 14.99) $price = 14.99;
		
		$result = round($price, 2);
		
		return $result;
	}
}