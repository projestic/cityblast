<?php
class Refund extends ActiveRecord\Model
{
	static $table = 'refund';

	static $has_one = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);
	
}
?>