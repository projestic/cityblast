<?php
class Note extends ActiveRecord\Model
{
	static $table = 'note';

	static $has_one = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);
	
}
?>