<?php
class Friends extends ActiveRecord\Model
{
	static $table = 'friends';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);

	static $has_many = array(
		array('blast_reach', 'class' => 'BlastReach', 'foreign_key' => 'friend_id')
	);

}
?>