<?php
class GroupCity extends ActiveRecord\Model
{
	static $table = 'group_city';


	static $belongs_to = array(
		array('group', 'class' => 'Group', 'foreign_key' => 'group_id'),
		array('city', 'class' => 'City', 'foreign_key' => 'city_id')
	);
}

?>