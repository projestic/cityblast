<?php

class MemberStatusLogDaily extends ActiveRecord\Model
{
	static $table = 'member_status_log_daily';

    static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);
	
	public static function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$time_span = $end_time - $start_time;
		$time_span_days = $time_span / 86400;
		
		if ($time_span_days > 92) {
			throw new Exception('Time span is to large');
		}
		
		Logger::log(
			'Compiling member payment stats between ' . date('Y-m-d', $start_time) . ' and ' . date('Y-m-d', $end_time), 
			Logger::LOG_TYPE_DEBUG
		);
		
		$current_time = $start_time;
		while ($current_time <= $end_time){
			$current_date = date('Y-m-d', $current_time);
			self::populateDay($current_date);
			$current_time = strtotime($current_date . ' +1 day');
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	public function populateDay($date = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$date = ($date instanceof DateTime) ? $date->format('Y-m-d') : $date;
		$date = !empty($date) ? $date : date('Y-m-d');
		
		Logger::log('Compiling member stats for day ' . $date, Logger::LOG_TYPE_DEBUG);
		
		Logger::log('Deleting existing stats for day', Logger::LOG_TYPE_DEBUG);
		MemberStatusLogDaily::table()->delete(array('date' => $date));
		
		$records_per_query = 200;
		$offset = 0;
		Logger::log('Starting data compilation', Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		while (true) {
			Logger::log('Processing members in range: ' . $offset . ' to ' . ($offset + $records_per_query), Logger::LOG_TYPE_DEBUG);
			Logger::startSection(Logger::LOG_TYPE_DEBUG);
			
			$members = Member::all(array(
				'conditions' => array('DATE(created_at) <= ?', $date),
				'order' => 'id ASC', 
				'limit' => $records_per_query, 
				'offset' => $offset
			));
			
			$members_count = !empty($members) ? count($members) : 0;
			Logger::log('Found ' . $members_count . ' members in range ', Logger::LOG_TYPE_DEBUG);
			
			if (empty($members)) {
				// We have processed all member, break out of the while loop
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				break;
			}
			
			foreach ($members as $member) {
				$status = MemberStatusLog::getMemberStatusLog($member->id, $date);
				if (empty($status)) {
					continue;
				}
				$data = array(
					'member_id' => $member->id,
					'member_type_id' => $status->member_type_id,
					'payment_status' => $status->payment_status,
					'account_type' => $status->account_type,
					'status' => $status->status,
					'price' => $status->price,
					'billing_cycle' => $status->billing_cycle,
					'price_month' => $status->price_month,
					'date' => $date
				);
				$log_daily = MemberStatusLogDaily::create($data);
			}
			$offset = $offset + $records_per_query;
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
		} 
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}