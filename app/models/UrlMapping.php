<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class UrlMapping extends ActiveRecord\Model{
	static $table_name = 'url_mapping';
	
	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id')
	);

	public function isUrlAccessible()
	{
		$result = false;
		$headers = @get_headers($this->url, true);
		
		// Each numericaly indexed element of the headers array should contain a HTTP status response header
		// Usually there would be only one such element at index 0
		// If the server redirects the request there will be one header for each redirection 
		// and also one for the final request
		if (!empty($headers)) {
			for ($i = 0; true; $i++) {
				if (!isset($headers[$i])) {
					break;
				}
				$status_header = $headers[$i];
				$reponse_code = substr($status_header, 9, 3);
				if (!is_numeric($reponse_code)) {
					continue;
				}
				switch ($reponse_code) {
					case ($reponse_code >= 400):
					break 2;
					case ($reponse_code >= 301 && $reponse_code <= 307):
						continue;
					break;
					case 200:
						$result = true;
					break 2;
				}
			}
		}
		
		if (!$result) {
			
			$hours   = 72;

			if ($this->url_inaccessible_since == null) {
				$this->url_inaccessible_since = new DateTime();				
			} elseif ($this->isInaccessibleFor($hours) && ($this->listing instanceOf Listing)) {
				$this->notifyInaccessibleFor($hours, true);
				$this->listing->retire();
			}			

		} else {
			$this->url_inaccessible_since = null;
		}
		
		$this->save();
	
		return $result;
	}
	
	public function isInaccessibleFor($hours) {
		if ($this->url_inaccessible_since == null) return false;
		$now 			= new DateTime();
		$down_for       = $this->url_inaccessible_since->diff($now);
		$down_for_hours = $down_for->days * 24 + $down_for->h;
		return ($down_for_hours > $hours);
	}
	
	public function notifyInaccessibleFor($hours, $retired) {
			$emailer     = Zend_Registry::get('alertEmailer');
			$subject     = "Content link inaccessible for listing " . $this->listing_id . " (" . APPLICATION_ENV . ")";
			$retired_msg = "";
			
			if ($retired) $retired_msg = "[!] Listing URL has been inaccessible for $hours hours. Listing has been retired.";
			
			$msg = "$retired_msg

ID: {$this->listing_id}
URL: {$this->url}
";
			
			if ($this->listing instanceOf Listing) {
				$msg .=		
"Title: {$this->listing->title}
Description: {$this->listing->description}
Message: {$this->listing->message}
";
			} else {
				$msg .= "(No listing found)";
			}

			$emailer->send($subject, $msg, 'error-content-publish');
	}
	
}
