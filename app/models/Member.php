<?php

use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Components\Contacts\ContactList;
use Ctct\Components\Contacts\CustomField;
use Ctct\Components\Contacts\EmailAddress;
use Ctct\Exceptions\CtctException;

/**
 * @property int referring_affiliate_id
 * @property int referring_member_id
 *
 * @property int referral_blast_count
 * @property int referral_email_count
 * @property text testimonial
 *
 * @property Affiliate referring_affiliate
 * @property Member referring_member
 */
class Member extends ActiveRecord\Model
{
	public $payment_error = null;
	public $mcErrorCode = null;
	public $log_status_after_save = false;
	
	static $before_save   = array('beforeSave');
	static $after_save    = array('afterSave');
	static $table = 'member';
	//static $pk    = 'track_id';
	
	static $belongs_to = array(
		array('facebook_application', 'class' => 'FacebookApplication', 'foreign_key' => 'application_id'),
		array('default_city', 'class' => 'City', 'foreign_key' => 'city_id'),
		array('publish_city', 'class' => 'City', 'foreign_key' => 'publish_city_id'),
		array('promo_code', 'class' => 'Promo', 'foreign_key' => 'promo_id'),
		array('referring_affiliate', 'class' => 'Affiliate', 'foreign_key' => 'referring_affiliate_id'),
		array('referring_member', 'class' => 'Member', 'foreign_key' => 'referring_member_id'),
		array('type', 'class' => 'MemberType', 'foreign_key' => 'type_id'),
		array('franchise', 'class' => 'Franchise', 'foreign_key' => 'franchise_id'),
		array('network_app_facebook', 'class' => 'NetworkApp', 'foreign_key' => 'application_id'),
		array('network_app_twitter', 'class' => 'NetworkApp', 'foreign_key' => 'tw_application_id'),
		array('network_app_linkedin', 'class' => 'NetworkApp', 'foreign_key' => 'ln_application_id'),
		array('payment_gateway', 'class' => 'PaymentGateway', 'foreign_key' => 'payment_gateway_id')
	);

	static $has_many = array(
		array('content_prefs', 'class' => 'ContentPref', 'foreign_key' => 'member_id'),
		array('listings', 'class' => 'Listing', 'foreign_key' => 'member_id', 'order' => 'listing.id DESC'),
		array('posts', 'class' => 'Post', 'foreign_key' => 'member_id'),
		array('bookings', 'class' => 'Booking', 'foreign_key' => 'member_id'),
		array('payments', 'class' => 'Payment', 'foreign_key' => 'member_id', 'order' => 'created_at ASC'),
		array('clicks', 'class' => 'Click', 'foreign_key' => 'member_id'),
		array('interactions', 'class' => 'Interaction', 'foreign_key' => 'referring_agent_id'),
		array('fanpages', 'class' => 'FanPage', 'foreign_key' => 'member_id'),
		array('referral_credits', 'class' => 'ReferralCredit', 'foreign_key' => 'member_id'),
		array('referred_members', 'class' => 'Member', 'foreign_key' => 'referring_member_id')
	);
	static $has_one = array(
		array('testimonials', 'class' => 'Testimonial', 'foreign_key' => 'member_id'),
		array('affiliate_account', 'class' => 'Affiliate', 'foreign_key' => 'member_id'),
		array('oauth_client', 'class' => 'OAuthClient', 'foreign_key' => 'member_id'),
		array('saml_login', 'class' => 'SAMLLogin', 'foreign_key' => 'member_id'),
	);
		
	public function beforeSave() 
	{
		
		if ($this->application_id == null) $this->application_id = 1;
		if ($this->frequency_id == null)   $this->frequency_id = 4;
		if ($this->billing_cycle == null)  $this->billing_cycle = 'month';
		if ($this->price == null)          $this->applyPrice();
		if ($this->next_payment_date == null) $this->next_payment_date = date('Y-m-d', strtotime(date('Y-m-d').' +15days')); // Use 15 days instead of 14 to cause payment to be processed the morning after 14 days expires.
		if ($this->current_hoper == null)  $this->current_hoper = rand(1, $this->getNumberOfHoppers());
		
		if (empty($this->price_month) || $this->is_dirty('price') || $this->is_dirty('billing_cycle')) {
			$this->applyPriceMonth();
		}
		
		if ($this->is_dirty('payment_status') 	|| 
			$this->is_dirty('account_type') 	|| 
			$this->is_dirty('first_name') 		|| 
			$this->is_dirty('last_name')  		|| 
			$this->is_dirty('email')  			|| 
			$this->is_dirty('email_override')) {
			
			try {	
				$this->addToMailchimp();
			} catch (Exception $e) {
				$alert_emailer = Zend_Registry::get('alertEmailer');
				$alert_emailer->send(
					'MailChimp error ' . $e->getCode() . ' for member ' . $this->id,
					$e->getMessage(),
					'error-mailchimp',
					true
				);
				
			}
			
			try {	
				$this->addToConstantContact();
			} catch (Exception $e) {
				$alert_emailer = Zend_Registry::get('alertEmailer');
				$alert_emailer->send(
					'ConstantContact error ' . $e->getCode() . ' for member ' . $this->id,
					$e->getMessage(),
					'error-constantcontact',
					true
				);	
			}
		}
		
		if (
			$this->is_dirty('payment_status') || 
			$this->is_dirty('account_type') || 
			$this->is_dirty('status') ||
			$this->is_dirty('type_id') ||
			$this->is_dirty('price') ||
			$this->is_dirty('billing_cycle') ||
			$this->is_dirty('price_month')
		) {
			$this->log_status_after_save = true;
		}
	}
	
	private function applyPriceMonth()
	{
		switch ($this->billing_cycle) {
			case 'month':
				$this->price_month = $this->price;
			break;
			case 'biannual':
				$this->price_month = round($this->price / 6, 2);
			break;
			case 'annual':
				$this->price_month = round($this->price / 12, 2);
			break;
		}
	}

	public function getFacebookUserOrAppAccessToken() {
		$now = new DateTime;
		if ($this->access_token && $this->token_expires > $now) {
			return $this->access_token;
		} else {
			return $this->network_app_facebook->getFacebookAppAccessToken();
		}
	}

	public function updateFacebookAppVisibility() {
		$this->facebook_app_visibility = $this->getFacebookAppVisibility();
	}
	
	public function getFacebookAppVisibility() {
		$facebook = $this->network_app_facebook->getFacebookAPI();
		$data = $facebook->api(
			array(
				'method' => 'fql.query',
				'access_token' => $this->access_token,
				'query' => 'select value from privacy_setting where name="default_stream_privacy"'
			)
		);
		return $data[0]['value'];
	}

	public function update_content_prefs(array $tag_ids)
	{
		ContentPref::table()->delete(array('member_id' => $this->id));
	
		$tags = Tag::all();
		if (!empty($tags) && !empty($tag_ids)) 
		{
			foreach ($tags as $tag) 
			{
				$conten_pref = new ContentPref();
				$conten_pref->member_id = $this->id;
				$conten_pref->tag_id = $tag->id;
				$conten_pref->pref = isset($tag_ids[$tag->id]) ? '1' : '0';
				$conten_pref->created_by = $this->id;
								
				$conten_pref->save();
			}
		}		
				
	}

	public function afterSave() 
	{
		if ($this->log_status_after_save == true) 
		{
			$paymentStatusLog = MemberStatusLog::create(array(
				'member_id' => $this->id,
				'member_type_id' => $this->type_id,
				'payment_status' => $this->payment_status,
				'status' => $this->status,
				'account_type' => $this->account_type,
				'price' => $this->price,
				'billing_cycle' => $this->billing_cycle,
				'price_month' => $this->price_month
			));
			$this->log_status_after_save = false;
		}
	}

	public function applyPrice() 
	{
		if (!$this->type) 
		{
			throw new Exception('Member type not found');
		}
		$this->price = $this->type->getPriceFor($this);
		if ($this->promo_code instanceOf Promo) 
		{
			$this->applyPromo($this->promo_code);
		}
		$this->applyPriceMonth();
	}
	
	public function applyBillingCycle($billing_cycle)
	{
		if (!in_array($billing_cycle, array('month', 'biannual', 'annual'))) 
		{
			throw new Exception('Invalid billing cycle');
		}
		if ($billing_cycle != $this->billing_cycle) 
		{
			$this->billing_cycle = $billing_cycle;
			// When billing cycle changes the member looses any existing promo
			$this->promo_id = null;
			$this->applyPrice();
		}
	}
	
	public function applyType($type_id)
	{
		if ($type_id != $this->type_id) 
		{
			$this->type_id = $type_id;
			// When type changes the member looses any existing promo
			$this->promo_id = null;
			$this->applyPrice();
		}
	}

	public function applyPromo(Promo $promo) 
	{
		if (!$this->type->promo_codes_allowed) return false;
		return $promo->apply($this);
	}
	
	public function getNumberOfHoppers() {
		if ($this->publish_city) {
			return $this->publish_city->number_of_hoppers;
		} elseif ($this->default_city) {
			return $this->default_city->number_of_hoppers;
		}
		return NUMBER_OF_HOPPERS;
	}

	public function hasType($type, $inherits = false) {
		$has = false;
		$mtype = $this->type;
		while (!$has && $mtype) {
			$has = (strtolower($mtype->code) == strtolower($type));
			if (!$inherits) break;
			$mtype = $mtype->inherits;
		}
		if ($inherits && $this->type->is_super_admin == 1) return true;
		return $has;
	}
	
	public function is_dirty($attr) 
	{
		return in_array($attr, array_keys($this->dirty_attributes()));
	}

	
	// End defaults and callbacks
	function name($lastfirst = false) 
	{
		if ($lastfirst) return "{$this->last_name}, {$this->first_name}";
		return "{$this->first_name} {$this->last_name}";
	}

	public function getFacebookProfilePic() 
	{
		return "https://graph.facebook.com/{$this->uid}/picture?redirect=true";
	}

	public function addNote($note) 
	{
		$this->notes .= "\n\n" . $note;
		$this->save();
	}

	function total_reach()
	{
		return($this->facebook_friend_count + $this->twitter_followers + $this->linkedin_friends_count);
	}

	function booking_trailing_thirty()
	{
		$query = "SELECT count(*) as total_shares FROM booking WHERE created_at > DATE_SUB(NOW(), INTERVAL 30 DAY) and member_id=".$this->id;
		$clicks = Booking::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function phonecalls_trailing_thirty()
	{
		$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 30 DAY) and type='phone_call' and referring_agent_id=".$this->id;
		$clicks = Interaction::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function email_shares_trailing_thirty()
	{
		$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 30 DAY) and type='share_listing_email' and referring_agent_id=".$this->id;
		$clicks = Interaction::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function facebook_shares_trailing_thirty()
	{
		$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 30 DAY) and type='facebook_share' and referring_agent_id=".$this->id;
		$clicks = Interaction::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function linkedin_shares_trailing_thirty()
	{
		$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 30 DAY) and type='linkedin_share' and referring_agent_id=".$this->id;
		$clicks = Interaction::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function twitter_shares_trailing_thirty()
	{
		$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 30 DAY) and type='twitter_share' and referring_agent_id=".$this->id;
		$clicks = Interaction::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function email_shares_trailing_seven()
	{
		$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 7 DAY) and type='share_listing_email' and referring_agent_id=".$this->id;
		$clicks = Interaction::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function facebook_shares_trailing_seven()
	{
		$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 7 DAY) and type='facebook_share' and referring_agent_id=".$this->id;
		$clicks = Interaction::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function linkedin_shares_trailing_seven()
	{
		$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 7 DAY) and type='linkedin_share' and referring_agent_id=".$this->id;
		$clicks = Interaction::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function twitter_shares_trailing_seven()
	{
		$query = "SELECT count(*) as total_shares FROM interaction WHERE created_at > DATE_SUB(NOW(), INTERVAL 7 DAY) and type='twitter_share' and referring_agent_id=".$this->id;
		$clicks = Interaction::find_by_sql($query);
		
		return($clicks[0]->total_shares);
	}

	function clicks_trailing_seven()
	{
		$query = "SELECT SUM(count) AS total_clicks FROM click_stat_by_date_member WHERE date > DATE_SUB(NOW(), INTERVAL 7 DAY) AND member_id=".$this->id;
		$clicks = Click::find_by_sql($query);
		
		return($clicks[0]->total_clicks);		
	}

	function clicks_today()
	{
		$query = "SELECT SUM(count) AS total_clicks FROM click_stat_by_date_member WHERE date > DATE_SUB(NOW(), INTERVAL 1 DAY) AND member_id=".$this->id;
		$clicks = Click::find_by_sql($query);
		
		return($clicks[0]->total_clicks);		
	}
	
	
	function clicks_trailing_thirty()
	{
		$query = "SELECT SUM(count) AS total_clicks FROM click_stat_by_date_member WHERE date > DATE_SUB(NOW(), INTERVAL 30 DAY) AND member_id=".$this->id;
		$clicks = Click::find_by_sql($query);
		
		return($clicks[0]->total_clicks);	
	}
	
	function likes_and_comments_trailing_thirty()
	{
		$query = "select sum(facebook_likes) as total_likes, sum(facebook_comments) as total_comments from post where created_at > DATE_SUB(NOW(), INTERVAL 30 DAY) and member_id=".$this->id;
		$clicks = Click::find_by_sql($query);
		
		$return_array['likes'] = $clicks[0]->total_likes;
		$return_array['comments'] = $clicks[0]->total_comments;
		
		return($return_array);				
	}
	
	function weekly_blasts()
	{
		$query = "SELECT count(*) as total_blasts FROM listing WHERE created_at > DATE_SUB(NOW(), INTERVAL 1 WEEK) AND member_id=".$this->id;
		$post = Listing::find_by_sql($query);
		
		return($post[0]->total_blasts);
	}
	
	function monthly_posts()
	{
		$query = "SELECT count(*) as total_posts FROM post WHERE created_at > DATE_SUB(NOW(), INTERVAL 30 DAY) and result=1 and member_id=".$this->id;
		$post = Post::find_by_sql($query);
		
		return($post[0]->total_posts);
	}

	function weekly_posts()
	{
		$query = "SELECT count(*) as total_posts FROM post WHERE created_at > DATE_SUB(NOW(), INTERVAL 1 WEEK) AND result=1 and member_id=".$this->id;
		$post = Post::find_by_sql($query);
		
		return($post[0]->total_posts);
	}

	function weekly_posts_facebook()
	{
		$query = "SELECT count(*) as total_posts FROM post WHERE created_at > DATE_SUB(NOW(), INTERVAL 1 WEEK) and result=1 AND type='FACEBOOK' and member_id=".$this->id;
		$post = Post::find_by_sql($query);
		
		return($post[0]->total_posts);
	}

	function weekly_posts_twitter()
	{
		$query = "SELECT count(*) as total_posts FROM post WHERE created_at > DATE_SUB(NOW(), INTERVAL 1 WEEK) and result=1 AND type='TWITTER' and member_id=".$this->id;
		$post = Post::find_by_sql($query);
		
		return($post[0]->total_posts);
	}

	function weekly_posts_linkedin()
	{
		$query = "SELECT count(*) as total_posts FROM post WHERE created_at > DATE_SUB(NOW(), INTERVAL 1 WEEK) AND result=1 and type='LINKEDIN' and member_id=".$this->id;
		$post = Post::find_by_sql($query);
		
		return($post[0]->total_posts);
	}

	function weekly_posts_fanpage()
	{
		$query = "SELECT count(*) as total_posts FROM post WHERE created_at > DATE_SUB(NOW(), INTERVAL 1 WEEK) AND result=1 and type='FANPAGE' and member_id=".$this->id;		
		$post = Post::find_by_sql($query);
		
		return($post[0]->total_posts);
	}			
	function fanpage_count()
	{
		$query = "SELECT likes from fanpage WHERE member_id=".$this->id." and active='yes'";
		$likes = FanPage::find_by_sql($query);
		
		if ($likes) return($likes[0]->likes);
			
		else return(0);
	}

	public function ingestPhoto($original) 
	{
		$original = $this->imageToLocal($original);
	
		$imageDest  = '/images/agents/' . date('Y') . "/" . date('m') . "/" . $this->id . '/' . basename($original);
		$imageDestPath = CDN_ORIGIN_STORAGE . $imageDest;
		$resizer	= new Image();
		$images = $resizer->resizeAgentImage($original);
		$images[] = $original;
		
		if ( !file_exists(dirname($imageDestPath)) ) mkdir(dirname($imageDestPath), 0777, true);
		
		foreach ($images as $image) 
		{
			copy($image, dirname($imageDestPath) . '/' . basename($image));
		}
		foreach ($images as $image) 
		{
			unlink($image);
		}
		
		$this->photo = $imageDest;
		$this->save();
	
	}
	
	protected function imageToLocal($photo) {
		if (stristr($photo, '://')) {
			$tmp 	= '/tmp/' . md5(microtime());
			$tmpPhoto 	= $tmp . '-' . basename($photo);
			copy($photo, $tmpPhoto);
			return $tmpPhoto;	
		}
		return $photo;
	}
	
	public function getSizedPhoto($width = 50, $height = 50, $useCDN = true) 
	{
		return $this->_getSizedImagePath($this->photo, $width, $height, $useCDN);
	}
	
	protected function _getSizedImagePath($path, $width, $height, $useCDN = true) 
	{
		$pathinfo = pathinfo($path);
		$cdn = ($useCDN) ? CDN_URL : '';
		return $cdn . "{$pathinfo['dirname']}/{$pathinfo['filename']}_{$width}x{$height}.{$pathinfo['extension']}";
	}

	function testQueueItem($blast, $debugMode=true)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = null;
		
		Logger::log('Testing listing id: ' . $blast->listing_id, Logger::LOG_TYPE_DEBUG);
		
		if ($result !== false) 
		{
			if (!$blast->listing) 
			{
				Logger::log('Unable to find listing', Logger::LOG_TYPE_DEBUG);
				$result = false;
			}
		}
		
		if ($result !== false) 
		{
			if ($blast->listing->big_image && !$this->hasPhotoPermissions()) 
			{
				Logger::log('Member has not given FB photo permissions', Logger::LOG_TYPE_DEBUG);
				$result = false;
			}
		}
		
		if ($result !== false) {
			$post = Post::find_by_member_id_and_listing_id_and_result($this->id, $blast->listing_id, 1);
			if (!$post)
			{ 
				Logger::log('Listing has never been published before by this member', Logger::LOG_TYPE_DEBUG);
				//Make sure it needs more "inventory"								
				if ($blast->current_count < $blast->max_inventory_threshold)
				{
					$result = $blast;
				}
				else
				{
					Logger::log('Listing doesnt need any more inventory', Logger::LOG_TYPE_DEBUG);
					$result = false;
				}
			} else {
				Logger::log('Listing previously published by this member', Logger::LOG_TYPE_DEBUG);
				$result = false;
			}
		}
		
		if ($result !== false) 
		{
			if (!$blast->listing->isPublishable()) 
			{
				Logger::log('Listing is not publishable', Logger::LOG_TYPE_DEBUG);
				$result = false;
			}
		}
		
		$result = ($result) ? $result : false; 
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;	
	}
	
	//Find blast
	public function find_blast($options = null)
	{ 
		Logger::setLevelCacheEnabled(true);
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$this->initCurrentHopperAndTag();
		// If we cant find content in the members chosen city 
		// we will look for content in the city that represents their chosen cities country
		$national_city_id = $this->getNationalCityId();
		
		$city = $this->default_city;
		Logger::log(
			'Cities: ' . $city->name . ' (' . $city->id . ') / ' . $city->country . ' (' . $national_city_id . ')', 
			Logger::LOG_TYPE_DEBUG
		);
		
		$continue = true;
		
		if (!$this->city_id) {
			$continue = false;
			Logger::log('Member does not have an assigned city!!', Logger::LOG_TYPE_DEBUG);
			$alert_emailer = Zend_Registry::get('alertEmailer');
			$alert_emailer->send(
				'Publishing error - no city assigned (' . $this->id . ')',
				$this->first_name . ' ' . $this->last_name . ' (' . $this->id . ') does not have an assigned city.',
				'error-publish'
			);
		}
		
		$target_network = null;
		if (!empty($options)) {
			Logger::log('Options: ' . print_r($options, true), Logger::LOG_TYPE_DEBUG);
			$target_network = !empty($options['target_network']) ? $options['target_network'] : null;
		}
		
		// Get hopper prefs as defined by content prefs
		$hopper_prefs = $this->getHopperPrefs();
		$hoppers_count = !empty($hopper_prefs) ? count($hopper_prefs) : 0;

		$hoppers_searched = array();
		
		$queue_item = false;

		if ($continue) {
			for ($i = 1; $i <= $hoppers_count; $i++) 
			{

				if (in_array($this->current_hoper, $hoppers_searched)) 
				{
					continue;
				}
				switch ($this->current_hoper) 
				{
					case 5:
						$tag = Tag::find_by_ref_code('BROKERS_ONLY');
						// Brokerage Specific Content
						$hoppers_searched[] = $this->current_hoper;
						Logger::log('Looking for content in hopper 5', Logger::LOG_TYPE_DEBUG);
						if (empty($hopper_prefs[5])) 
						{
							// Based on the members prefs we shouldnt look in hopper 4 for content
							Logger::log('Hopper 5 disabled - skipping', Logger::LOG_TYPE_DEBUG, true);
							continue;
						}
						Logger::startSection(Logger::LOG_TYPE_DEBUG);
						Logger::log('Looking for city specific content', Logger::LOG_TYPE_DEBUG);
						$queue_opts = array(
							'city_id' => $this->city_id,
							'blast_type' => Listing::TYPE_CONTENT,
							'hopper_id' => 5,
							'inc_tag_id' => $tag->id,
							'target_network' => $target_network
						);
						$queue_item = PublishingQueue::getUnpublishedPublishable($this, $queue_opts);
						if (!$queue_item && $national_city_id) {
							Logger::log('Looking for national specific content', Logger::LOG_TYPE_DEBUG);
							// Couldnt find content in the members city lets try their country
							$queue_opts = array(
								'city_id' => $national_city_id,
								'blast_type' => Listing::TYPE_CONTENT,
								'hopper_id' => 5,
								'inc_tag_id' => $tag->id,
								'target_network' => $target_network
							);
							$queue_item = PublishingQueue::getUnpublishedPublishable($this, $queue_opts);
						}
						Logger::endSection(Logger::LOG_TYPE_DEBUG);
					break;
					case 4:
						// Franchise News
						$hoppers_searched[] = $this->current_hoper;
						Logger::log('Looking for content in hopper 4', Logger::LOG_TYPE_DEBUG);
						if (empty($hopper_prefs[4])) 
						{
							// Based on the members prefs we shouldnt look in hopper 4 for content
							Logger::log('Hopper 4 disabled - skipping', Logger::LOG_TYPE_DEBUG, true);
							continue;
						}
						if(!isset($this->franchise_id) || empty($this->franchise_id))
						{
							//We don't know what Franchise this person belongs to
							Logger::log('Unknown Franchise', Logger::LOG_TYPE_DEBUG, true);
							continue;
						}
						Logger::startSection(Logger::LOG_TYPE_DEBUG);
						Logger::log('Looking for franchise specific content', Logger::LOG_TYPE_DEBUG);
						$queue_opts = array(
							'city_id' => $this->city_id,
							'blast_type' => Listing::TYPE_CONTENT,
							'hopper_id' => 4,
							'franchise_id' => $this->franchise_id,
							'target_network' => $target_network
						);
						$queue_item = PublishingQueue::getUnpublishedPublishable($this, $queue_opts);
						if (!$queue_item && $national_city_id) {
							Logger::log('Looking for franchise specific content', Logger::LOG_TYPE_DEBUG);
							// Couldnt find content in the members city lets try their country
							$queue_opts = array(
								'city_id' => $national_city_id,
								'blast_type' => Listing::TYPE_CONTENT,
								'hopper_id' => 4,
								'franchise_id' => $this->franchise_id,
								'target_network' => $target_network
							);
							$queue_item = PublishingQueue::getUnpublishedPublishable($this, $queue_opts);
						}
						Logger::endSection(Logger::LOG_TYPE_DEBUG);
					break;
					case 3:
						// Listing
						$hoppers_searched[] = $this->current_hoper;
						Logger::log('Looking for content in hopper 3', Logger::LOG_TYPE_DEBUG);
						if (empty($hopper_prefs[3])) {
							// Based on the members prefs we shouldnt look in hopper 3 for content
							Logger::log('Hopper 3 disabled - skipping', Logger::LOG_TYPE_DEBUG, true);
							continue;
						}
						Logger::startSection(Logger::LOG_TYPE_DEBUG);
						Logger::log('Looking for city specific content', Logger::LOG_TYPE_DEBUG);
						$queue_opts = array(
							'city_id' => $this->city_id,
							'blast_type' => Listing::TYPE_LISTING,
							'hopper_id' => 3,
							'target_network' => $target_network
						);
						$queue_item = PublishingQueue::getUnpublishedPublishable($this, $queue_opts);
						Logger::endSection(Logger::LOG_TYPE_DEBUG);
					break;
					case 2:
						// Local content
						$hoppers_searched[] = $this->current_hoper;
						Logger::log('Looking for content in hopper 2', Logger::LOG_TYPE_DEBUG);
						if (empty($hopper_prefs[2])) {
							// Based on the members prefs we shouldnt look in hopper 2 for content
							Logger::log('Hopper 2 disabled - skipping', Logger::LOG_TYPE_DEBUG, true);
							continue;
						}
						Logger::startSection(Logger::LOG_TYPE_DEBUG);
						Logger::log('Looking for city specific content', Logger::LOG_TYPE_DEBUG);
						$queue_opts = array(
							'city_id' => $this->city_id,
							'blast_type' => Listing::TYPE_CONTENT,
							'hopper_id' => 2,
							'target_network' => $target_network
						);
						$queue_item = PublishingQueue::getUnpublishedPublishable($this, $queue_opts);
						Logger::endSection(Logger::LOG_TYPE_DEBUG);
					break;
					case 1:
						// Tag based, non location specific content
						$hoppers_searched[] = $this->current_hoper;
						Logger::log('Looking for content in hopper 1', Logger::LOG_TYPE_DEBUG);
						Logger::startSection(Logger::LOG_TYPE_DEBUG);
						
						$content_pref_tags = ContentPref::findAllForMemberAsTag($this->id);
						
						$total_taqs_enabled = 0;
						$debug_tag_names = array();
						// Find the total number of tags enabled so we know 
						// the maximum number of possible rotations for the next loop
						foreach ($content_pref_tags as $content_pref_tag) {
							$debug_tag_names[$content_pref_tag->id] = $content_pref_tag->name;
							if (!$content_pref_tag->publishable || !$content_pref_tag->pref) {
								continue;
							}
							$total_taqs_enabled++;
						}
						Logger::log('Total enabled content preference tags: ' . $total_taqs_enabled, Logger::LOG_TYPE_DEBUG);
						$prev_tag_name = !empty($debug_tag_names[$this->prev_tag_id]) ? 
											'"' . $debug_tag_names[$this->prev_tag_id] . '" (' . $this->prev_tag_id . ')' : 'Unknown tag';
						Logger::log('Previous tag was: ' . $prev_tag_name , Logger::LOG_TYPE_DEBUG);
						
						
						for ($x = 1; $x <= $total_taqs_enabled; $x++) {
							$current_tag_name = !empty($debug_tag_names[$this->current_tag_id]) ? 
													'"'.$debug_tag_names[$this->current_tag_id] . '" (' . $this->current_tag_id . ')' : 'Unknown tag';
							Logger::log('Looking for content with tag: ' . $current_tag_name, Logger::LOG_TYPE_DEBUG);
							$queue_opts = array(
								'city_id' => $national_city_id,
								'blast_type' => Listing::TYPE_CONTENT,
								'hopper_id' => 1,
								'inc_tag_id' => $this->current_tag_id,
								'ex_tag_id' => $this->prev_tag_id,
								'target_network' => $target_network
							);
							$queue_item = PublishingQueue::getUnpublishedPublishable($this, $queue_opts);
							if ($queue_item) {
								// Queue item found
								break;
							} else {
								Logger::log('Rotating current tag', Logger::LOG_TYPE_DEBUG);
								$this->rotateCurrentTag();
							}
						}
						Logger::endSection(Logger::LOG_TYPE_DEBUG);
					break;
				}
				if ($queue_item) {
					break;
				} else {
					Logger::log('Rotating current hopper', Logger::LOG_TYPE_DEBUG);
					$this->rotateCurrentHopper();
				}
			}
		}
		if (!$queue_item){
			$content_pref_tags = array();
			foreach ($this->content_prefs as $cp) {
				if ($cp->pref == 1) $content_pref_tags[] = $cp->tag->name;
			}
		
			$message = 'Unable to find content for: ' . $this->first_name . ' ' . $this->last_name . ' (' . $this->id . ")
			
Member type: {$this->type->name} (ID {$this->type->id})
Tags enabled: " . (count($content_pref_tags) ? implode(', ', $content_pref_tags) : 'none') . "

Publisher log: 

" . Logger::getCurrentSectionLines();
		
			Logger::log('Unable to find any content', Logger::LOG_TYPE_DEBUG);
			$alert_emailer = Zend_Registry::get('alertEmailer');
			$alert_emailer->send(
				'Publishing error - no content found (' . $this->id . ')',
				 $message,
				'error-publish'
			);
		} else {
			Logger::log('Found content - listing id: ' . $queue_item->listing_id, Logger::LOG_TYPE_DEBUG);
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		Logger::setLevelCacheEnabled(false);
		
		return $queue_item;
	}
	
	//Get national city id
	public function getNationalCityId()
	{
		$city_id = null;
		
		if (strtoupper($this->default_city->country) == "CANADA")
		{
			//Use CANADA
			$city_id = 1102;
		}	
		if (strtoupper($this->default_city->country) == "UNITED STATES")
		{
			//Use US
			$city_id = 1103;
		}
		
		return $city_id;
	}
	
	//Get hopper preferences
	protected function getHopperPrefs()
	{
		$result = array(
			1 => true,
			2 => false,
			3 => false,
			4 => false,
			5 => false,
		);
		
		$content_pref_tags = ContentPref::findAllForMemberAsTag($this->id);

		foreach ($content_pref_tags as $content_pref_tag) 
		{
			if ($content_pref_tag->pref) 
			{
				if ($content_pref_tag->ref_code == 'CONTENT_LOCAL') {
					$result[2] = true;
				}
				if ($content_pref_tag->ref_code == 'LISTINGS') {
					$result[3] = true;
				}
				if ($content_pref_tag->ref_code == 'FRANCHISE') {
					$result[4] = true;
				}
				if ($content_pref_tag->ref_code == 'BROKERS_ONLY') {
					$result[5] = true;
				}
			}
		}
		return $result;
	}
	
	//
	// Init current hopper and tag
	// 
	// This method ensures that current_hoper and current_tag_id both have valid values
	// based on the members current content preferences.
	function initCurrentHopperAndTag()
	{
		$content_pref_tags = ContentPref::findAllForMemberAsTag($this->id);
		
		$hopper_prefs = $this->getHopperPrefs();
		
		if (!$this->current_hoper || empty($hopper_prefs[$this->current_hoper])) {
			// Currently selected hopper is no longer valid so we must change it
			$this->rotateCurrentHopper();
		}
		$current_tag_is_valid = false;
		foreach ($content_pref_tags as $content_pref_tag) {
			if ($content_pref_tag->id == $this->current_tag_id && $content_pref_tag->publishable && $content_pref_tag->pref) {
				$current_tag_is_valid = true;
			}
		}
		if (!$current_tag_is_valid) {
			// Currently selected tag id is not valid so we must change it
			$this->rotateCurrentTag();
		}
	}
	
	//Move current hopper to the next hopper in the list
	public function rotateCurrentHopper()
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$hopper_prefs = $this->getHopperPrefs();
		
		$num_hoppers = !empty($hopper_prefs) ? count($hopper_prefs) : 0;
		$new_hopper = null;
		$test_hopper = $this->current_hoper;
		for ($x = 0; $x < $num_hoppers; $x++) {
			$test_hopper = ($test_hopper < $num_hoppers) ? $test_hopper + 1 : 1;
			$test_result = !empty($hopper_prefs[$test_hopper]);
			Logger::log('Checking hopper: ' . $test_hopper. ' = ' . ($test_result ? 'true' : 'false'), Logger::LOG_TYPE_DEBUG);
			if (!empty($hopper_prefs[$test_hopper])) {
				$new_hopper = $test_hopper;
				break;
			}
		}

		$this->current_hoper = ($new_hopper) ? $new_hopper : 1;
		$this->save();
		Logger::log('New hopper is: ' . $this->current_hoper, Logger::LOG_TYPE_DEBUG);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	//Move current tag to the next tag in the list
	
	public function rotateCurrentTag()
	{
		$content_pref_tags = ContentPref::findAllForMemberAsTag($this->id); 
		
		
		$original_tag_id = $this->current_tag_id;
		
		// select the next tag in list
		$current_tag_found = false;
		$current_tag_updated = false;
		foreach ($content_pref_tags as $content_pref_tag) {
			if ($current_tag_found && $content_pref_tag->publishable && $content_pref_tag->pref) {
				$this->current_tag_id = $content_pref_tag->id;
				$current_tag_updated = true;
				break;
			}
			if ($content_pref_tag->id == $this->current_tag_id) {
				$current_tag_found = true;
			}
		}
		if ($current_tag_updated == false) {
			foreach ($content_pref_tags as $content_pref_tag) {
				if ($content_pref_tag->publishable && $content_pref_tag->pref) {
					$this->current_tag_id = $content_pref_tag->id;
					break;
				}
			}
		}
		
		$this->save();
	}

	function published_today()
	{
		$posts = Post::count(array(
			'conditions' => array("member_id = ? AND DATE(created_at) = DATE(NOW()) AND result = 1 AND status = 'active'", $this->id)
		));
		return (bool) $posts;
	}

	function should_i_publish()
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$outcome = array();
		
		for ($i = 1; $i <= $this->frequency_id; $i++) {
			$outcome[$i] = 1;	
		}
		
		for ($i = 1; $i <= 7; $i++) {
			if (!isset($outcome[$i])) $outcome[$i] = 0;
		}
		
		$random = rand(1,7);

		Logger::log('My frequency is: '.$this->frequency_id.'/7', Logger::LOG_TYPE_DEBUG);

		

		if ($this->frequency_id < 6)
		{
			// Small tweak to influence the lower probabilty dice rolls (people are complaining they publish too infrequently)
			$reverse_frequency  = 7 - $this->frequency_id;
			$sql = "
				SELECT COUNT(*) as count 
				FROM (
					SELECT id, DATE(created_at) as date_published 
					FROM post 
					WHERE 
						member_id = ? AND 
						result = 1 AND 
						created_at > DATE(NOW() - INTERVAL $reverse_frequency DAY) 
					GROUP BY date_published 
					ORDER BY created_at) as tmp
			";
			$results = Post::find_by_sql($sql, array($this->id));
			$recent_post_count  = $results[0]->count;
			
			// Small tweak to influence the higher probabilty dice rolls (people are complaining they publish too frequently)
			$reverse_frequency_b  = floor($reverse_frequency/2);
			$sql = "
				SELECT COUNT(*) as count 
				FROM (
					SELECT id, DATE(created_at) as date_published 
					FROM post 
					WHERE 
						member_id = ? AND 
						result = 1 AND 
						created_at > DATE(NOW() - INTERVAL 1 DAY) 
					GROUP BY date_published 
					ORDER BY created_at) as tmp
			";
			$results = Post::find_by_sql($sql, array($this->id));
			$recent_post_count_b  = $results[0]->count;
			
			if (!$recent_post_count) {
				Logger::log("Haven't published in the last $reverse_frequency days. Must ... publish!");
				return true;
			} elseif ($recent_post_count_b) {
				Logger::log("Have published in the last $reverse_frequency_b days. Dont ... publish!");
				return false;
			} else {
				Logger::log("Published $recent_post_count time" . ($recent_post_count == 1 ? '' : 's') . " in the last $reverse_frequency days. Rolling dice.");
			}
		}
		
		Logger::log('The dice rolled: ' . $random. '. Anything greater than '.$this->frequency_id.' will not publish (for example: if the frequency is 4 a roll of 5 will not bingo)', Logger::LOG_TYPE_DEBUG);
			
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		return (bool) $outcome[$random];
						
	}

	//
	// Rotate content flags
	// 
	// This method should be called after each automated post to update the flags indicating what content
	// should be posted next.
	// 
	//
	
	public function rotateCurrentContent()
	{
		if ($this->current_hoper == 1) 
		{
			// We just published tag based content
			// Set prev_tag_id so we dont use the same tag next time
			$this->prev_tag_id = $this->current_tag_id;
			$this->save();
			// Move to next tag
			$this->rotateCurrentTag();
		}
		// Rotate hopper
		$this->rotateCurrentHopper();
	}


	public function publishOneOff($listing) 
	{

		//Cheat the PUB-QUEUE
		$publish_queue = new PublishingQueue();
		$publish_queue->city_id = $listing->city_id;
		$publish_queue->hopper_id = 3;
		$publish_queue->listing_id = $listing->id;
		$publish_queue->priority = 1;
		$publish_queue->min_inventory_threshold = 1;
		$publish_queue->max_inventory_threshold = 9999;
		$publish_queue->current_count = 0;
		$publish_queue->save();

		$published = $this->publishBlast($publish_queue, false);
		$publish_queue->delete();

		//Let's find out how many people live in this CITY
		$total_members	=   Member::count(array("conditions" => array( 'city_id' => $listing->city_id )));

		if (!$total_members	|| $total_members <= 1) {

			$agentOnly = true;
			$agent = $listing->member;

			if ($listing->list_city) {
				foreach ($listing->list_city->members as $member) {
					if ($member != $agent) $agentOnly = false;
				}
			}

			if ($agentOnly) $listing->retire();
		}
		
		return $published;
	}

	function publishBlast($publish_queue,  $debugMode = true, $services = array()) 
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result =  false;
		
		if (!is_array($services)) $services = array($services);
		
		if ($publish_queue->listing) {
			if (!count($services) || in_array('facebook', $services)) $this->publishFacebook($publish_queue, $debugMode);
			if (!count($services) || in_array('fanpages', $services))  $this->publishFacebookFanpage($publish_queue, $debugMode);
			if (!count($services) || in_array('twitter', $services))  $this->publishTwitter($publish_queue, $debugMode=true);
			if (!count($services) || in_array('linkedin', $services)) $this->publishLinkedin($publish_queue, $debugMode);
			$this->save();
			$result = true;
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		return $result;
	}

	public function saveListingPost($listing, $message, $type, $result, $post_id = null, $reach = null) 
	{
		$post = new Post();
		if ($listing && $listing->id) $post->listing_id   = $listing->id;
		$post->member_id    = $this->id;
		$post->message      = $message;
		$post->type      	= $type;
		$post->result		= $result;
		$post->frequency_id = $this->frequency_id;
		$post->hopper_id	= $this->current_hoper;
		$post->reach        = $reach;
		if ($post_id) {
			switch ($type) {
				case 'FACEBOOK' :
				case 'FANPAGE' :
					$post->fb_post_id = $post_id;
					break;
				case 'TWITTER' :
					$post->twitter_post_id = $post_id;
					break;
			}
		}
		$post->save();	
	}

	function publishTwitter($publish_queue, $debugMode=true)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		if (empty($this->twitter_publish_flag) || ($this->twitter_publish_flag == 0) || empty($this->twitter_request_token) || empty($this->twitter_access_token)	)
		{
			Logger::log('TWITTER Publish Flag is OFF', Logger::LOG_TYPE_DEBUG);
			$message      = "TWITTER FAIL: this user had set their PERSONAL SETTINGS to OFF, do NOT publish to TWITTER!";
			$this->saveListingPost(null, $message, 'TWITTER', null);
			Logger::endSection(Logger::LOG_TYPE_DEBUG);	
			return false;		
		}

		$listing = $publish_queue->listing;	
				
		$my_twitter = new Blastit_Service_Twitter(null, $this);
		Logger::log(date("Y-m-d H:i:s").' TWITTER Publishing Member: '.$this->first_name . ' ' . $this->last_name, Logger::LOG_TYPE_DEBUG);	
		
		//Make sure the length is 140
		$twitter_suffix = '';
		
		if ($listing->isContent()) {
			if ($this->click_tracking) {
				if ($listing->content_link) {
					$listing->filter_content_link('tw');
					$twitter_suffix = "... "  . $listing->content_link ."/".$this->id;
				}
			} else {
				$twitter_suffix = "... "  . $listing->url_mapping->url;
			}
		} else {
			$twitter_suffix = "... " . APP_URL . "/listing/view/".$listing->id."/".$this->id."/tw";
		}

		//Randomize the message when possible
		$random_message = ListingDescription::getRandomDescription($listing);
		$current_length = mb_strlen($random_message);
		$suffix_length = mb_strlen($twitter_suffix);
		if (($current_length + $suffix_length) > 140)
		{
			$total_available = 140 - $suffix_length;
			if ($total_available > 0)
			{	
				$prefix = mb_substr($random_message, 0, $total_available);
				$twitter_msg = $prefix . $twitter_suffix;
			}
			else
			{
				$twitter_msg = mb_substr($twitter_suffix, 0, 139);
			}
		} else {
			$twitter_msg = $random_message . $twitter_suffix;
		}
		
		Logger::log('$listing->big_image: ' . $listing->big_image, Logger::LOG_TYPE_DEBUG);
		$image_file_path = null;
		if ($listing->big_image) {
			$img = ListingImage::getRandomImage($listing);
			$image_file_path = TMP_ASSET_STORAGE . '/' . strtotime('now') . '_' . basename($img);
			if (!copy(CDN_ORIGIN_STORAGE . $img, $image_file_path)) {
				throw new Exception('Failed copying image to temp directory');
			}
		}

		//Ok, let's post...
		// Second param means that a twitter object will be returned
		// Response will be Json format
		
		$response = null;
		$result = null;
		try
		{
			if ($listing->big_image) {
				Logger::log($image_file_path, Logger::LOG_TYPE_DEBUG);
				$response = $my_twitter->statusUpdateWithMedia($twitter_msg, $image_file_path);
				@unlink($image_file_path);
			} else {
				$response = $my_twitter->statusUpdate($twitter_msg);
			}
		}
		catch(Exception $e)
		{			
			// Handling invalid token
			if ( !strstr($e->getMessage(), "Error validating") )
			{
				switch ($e->getCode()) {
					case '89':
						$this->invalidTokenNotify('twitter');
					break;
				}
				
				$message = "TW FAIL: Twitter post FAIL. COULD NOT CONNECT TO TWITTER. ERROR: ".$e->getMessage() . ' (' . $e->getCode() . ')';
				$this->saveListingPost($listing, $message, 'TWITTER', 0);
				Logger::log('TW FAIL: Twitter post FAIL. COULD NOT CONNECT TO TWITTER. ERROR: '.$e->getMessage() . ' (' . $e->getCode() . ')', Logger::LOG_TYPE_DEBUG);	
				
				Logger::endSection(Logger::LOG_TYPE_DEBUG);	
				$result = array('error'=>true,'message'=>'Twitter post FAIL. COULD NOT CONNECT TO TWITTER.','postlink'=>'');
			}
			
			if ($image_file_path) {
				@unlink($image_file_path);
			}
		}

		if ($response)
		{		
			if ( !empty($response->error) )
			{
				$message = "TW FAIL: Twitter post FAIL. ERROR:".print_r($response->error, true);
				$this->saveListingPost($listing, $message, 'TWITTER', 0);

				$this->twitter_fails++;
				if ($this->twitter_fails > 3)
				{
					$this->twitter_auto_disabled = 1;
				}
				$this->save();

				Logger::log($this->email . ' -> FAILED to post to Twitter: '.$response->error, Logger::LOG_TYPE_DEBUG);	
				Logger::endSection(Logger::LOG_TYPE_DEBUG);	
				$result = array('error'=>true,'message'=>'FAILED to post to Twitter: '.$response->error,'postlink'=>'');
			}
			else
			{
		
				$message 		= "TWITTER: Successful post to Twitter: " .$response->id_str;
				$this->saveListingPost($listing, $message, 'TWITTER', 1, $response->id_str, $this->twitter_followers);
					
				Logger::log($this->email . ' -> Successful post to Twitter: ' .$response->id_str, Logger::LOG_TYPE_DEBUG);	

				//Let's grab the record one more time because it might have been updated by other running cron jobs
				if (isset($publish_queue->id) && !empty($publish_queue->id))
				{
					$new_publish  	= PublishingQueue::find_by_id($publish_queue->id);
					$publish_queue = $new_publish;
				}
				
				//Add to the inventory
				$publish_queue->current_count += $this->twitter_followers;
				$publish_queue->save();
				//Reset the FAIL to zero again, because we've succeeded!
				$this->twitter_fails = 0;
				$this->save();
				
				
				unset($post);
				Logger::endSection(Logger::LOG_TYPE_DEBUG);	
				$result = array('error'=>false,'message'=>'Successful post to Twitter','postlink'=>'https://twitter.com/'.$this->twitter_id.'/status/'.$response->id_str);
			}
		} else {
			Logger::log('No TWITTER response', Logger::LOG_TYPE_DEBUG);	
			$message 		= "TW FAIL: Twitter post FAIL. No TWITTER response";
			$this->saveListingPost($listing, $message, 'TWITTER', 0);
			Logger::endSection(Logger::LOG_TYPE_DEBUG);	
			$result = array('error'=>true,'message'=>'Twitter post FAIL. No TWITTER response!','postlink'=>'');
		}
		
		return $result;
	}	

	function buildFacebookPublishArray($publish_queue, $click_source = null)
	{
		$listing = $publish_queue->listing;
		if ($listing) return $listing->getFacebookPayload($this, $click_source);		
	}
	
	
	// Below method appears to be used only in testing.
	
	function publishAlbum()
	{

		$facebook = $this->network_app_facebook->getFacebookAPI();

		echo 'https://graph.facebook.com/oauth/authorize?client_id=' . $this->network_app_facebook->consumer_id . '&redirect_uri='.urlencode("http://alen.cityblast.com/member/signupis").'&scope=user_photos,friends_photos,publish_stream';		
		echo "<BR><BR>";
				
		echo $this->network_app_facebook->getFacebookAppAccessToken() . "<BR><BR>";
				
		try
		{	
			//Set up the FACEBOOK VARIABLES...
			$publish_array['access_token'] 	= $this->network_app_facebook->getFacebookAppAccessToken();
			$publish_array['name'] 			= "123 Apple Tree Lane, Toronto";
			$publish_array['message'] 		= "Does a Link work? https://www.cityblast.com/view/listing/1234";
			$publish_array['link'] 			= "https://www.cityblast.com/view/listing/1234";
				
			$fb_result = $facebook->api("/".$this->uid."/albums", "post", $publish_array);
			
			$album_id = $fb_result['id'];
		
			$facebook->setFileUploadSupport(true);
			
			#PHOTO #1  
			$file = "@".realpath("/home/abubic/alen.cityblast.com/public/images/listings/2013/02/6533/PIC_1_605x239.jpg");  			$args = array(  
			    'message' => 'Photo Caption. Does a Link work? https://www.cityblast.com/view/listing/1234',  
			    "access_token" => $this->network_app_facebook->getFacebookAppAccessToken(),  
			    "image" => $file,  
			    "link" => "https://www.cityblast.com/view/listing/1234"
			);  			
			$data = $facebook->api('/'.$album_id.'/photos', 'post', $args);
			if ($data) print_r("success");
						echo "<h1>ADDED PHOTO #1</h1>";
			
			exit();
					
		} 
		catch(Exception $e) 
		{
			echo $e->getMessage() . "<BR><BR>";		
		}
					
	}

	public function getActivePublishServices() 
	{
		$active = array();
		if ($this->facebook_publish_flag) $active['facebook'] = $this->uid;
		if ($this->twitter_publish_flag)  $active['twitter']  = $this->twitter_id;
		if ($this->linkedin_publish_flag) $active['linkedin'] = $this->linkedin_id;
		$fanpages = array();
		foreach ($this->fanpages as $fanpage) 
		{
			if ($fanpage->active == 'yes') $fanpages[] = $fanpage->fanpage_id;
		}
		$active['fanpages'] = $fanpages;
		return $active;
	}


	function publishDirectToFanpage($facebook, $fanpage, $listing, $publish_array)
	{

		if ($listing->big_image) 
		{
			try {
				//Set up the FACEBOOK VARIABLES...
				$publish_array['access_token'] = $fanpage->access_token;
				
				$album_id = null;
				if ($fanpage->album_id) {
					try {
						$fb_result = $facebook->api("/".$fanpage->album_id, 'get', $publish_array);
						$album_id = !empty($fb_result['id']) && !empty($fb_result['can_upload']) ? $fb_result['id'] : null;
					} catch (Exception $e) {
						// Album doesnt exist
					}
				}
				
				if (empty($album_id)) {
					$publish_array['name'] = COMPANY_NAME;
					$publish_array['message'] = COMPANY_NAME;
					try {
						$fb_result = $facebook->api("/".$fanpage->fanpage_id."/albums", "post", $publish_array);
						$album_id = !empty($fb_result['id']) ? $fb_result['id'] : null;
						if (!empty($album_id)) {
							$fanpage->album_id = $album_id;
							$fanpage->save();
						}
					} catch (Exception $e) {
						throw new Exception('Failed creating album: ' . $e->getMessage());
					}
				}
				
				if (empty($album_id)) {
					throw new Exception('Failed creating album');
				}
				
				$publish_array['message'] = ListingDescription::getRandomDescription($listing);
								
				// Make sure the proper jump link format exists

				if ($this->click_tracking) {
					if ($listing->content_link) {
						$listing->filter_content_link('fp');
						$publish_array['link'] = $listing->content_link ."/".$this->id;
					}
				} else {
					$publish_array['link'] = $listing->url_mapping->url;
				}

				if (isset($listing->title) && !empty($listing->title))  
					$publish_array['name'] = $listing->title;
				elseif (isset($listing->url_mapping->url)) 
					$publish_array['name'] = $listing->url_mapping->url;
	
				$facebook->setFileUploadSupport(true);
											
				#PHOTO #1  
				$img = ListingImage::getRandomImage($listing);
				$tmpfile = TMP_ASSETS_STORAGE . '/' . strtotime('now') . '_' . basename($img);

				if (copy(CDN_ORIGIN_STORAGE . $img, $tmpfile)) {
					$publish_array['image'] = "@" . $tmpfile; 
				}
							
				$publish_array['name'] = $publish_array['name'] . "\n\n" . $publish_array['message'] . "\n\n" . $publish_array['link'];
				
				$data = $facebook->api('/'.$album_id.'/photos', 'post', $publish_array);
				$photo_id = !empty($data['id']) ? $data['id'] : null;
				
				if (empty($photo_id)) {
					throw new Exception('Failed creating photo');
				}
				
				$message = "FACEBOOK_FANPAGE SUCCESS: post to FB FANPAGE album id: " . $album_id;
				$this->saveListingPost($listing, $message, 'FANPAGE', 1, $album_id, $this->facebook_friend_count);
				
				@unlink($tmpfile);
				
				if ($data) print_r("success");
				
				return array('error'=>false,'message'=>'Successful post to Facebook FanPage: '.$fanpage->fanpage,'postlink'=>'http://www.facebook.com/' . $album_id, 'post_id' => $album_id);	
			
			} catch(Exception $e) {
	
				if ($e instanceOf FacebookApiException) {
					$this->handleFacebookException($e, $facebook, $fanpage);
				}

				Logger::log('ERROR: ' . $e->getMessage(), Logger::LOG_TYPE_DEBUG);

				Logger::log('Publish array was: ' . var_export($publish_array, true));
				$message      = "FB FANPAGE FAIL, Facebook post FAIL. ERROR:".$e->getMessage();
				$this->saveListingPost($listing, $message, 'FACEBOOK', 0);	
				Logger::log($this->email .' -> FAILED post to FB:' .$e->getMessage(), Logger::LOG_TYPE_DEBUG);

				Logger::log('Publish array was: ' . var_export($publish_array, true));
	
				$this->facebook_fails++;
				if ($this->facebook_fails > 3)
				{
					$this->facebook_auto_disabled = 1;	
				}
				
				$this->save();
	
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				return array('error'=>true,'message'=>'Unable to post to FB - Big Photo: '.$e->getMessage(),'postlink'=>'');
	
			}

		} else {
		
			try {
				$fb_result = $facebook->api("/".$fanpage->fanpage_id."/feed", "post", $publish_array);
				$post_id = !empty($fb_result['id']) ? $fb_result['id'] : null;
				
				if (empty($post_id)) {
					throw new Exception('Failed creating post');
				}

				$message      	= "FACEBOOK_FANPAGE SUCCESS: post to FB FANPAGE: " . $post_id;
				$this->saveListingPost($listing, $message, 'FANPAGE', 1, $post_id, $fanpage->likes);
	
				Logger::log('Facebook FANPAGE Success, '.$fanpage->fanpage .' -> Successful post to FB FANPAGE:  '.$fanpage->fanpage.' ' . $post_id, Logger::LOG_TYPE_DEBUG);
				
				//Reset the FAIL to zero again, because we've succeeded!
				$this->facebook_fails = 0;
				$this->save();


				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				list($fbmember,$postid) = explode("_",$post_id);

				if ($this->auto_like) {
					$this->likePost($postid);
				}
				
				return array('error'=>false,'message'=>'Successful post to Facebook FanPage: '.$fanpage->fanpage,'postlink'=>'http://www.facebook.com/'.$fbmember.'/posts/'.$postid, 'post_id' => $postid);
					
			} catch(Exception $e) {
			
				if ($e instanceOf FacebookApiException) {
					$this->handleFacebookException($e, $facebook, $fanpage);
				}

				$message = "FB FANPAGE FAIL: Facebook post FAIL. ERROR:".$e->getMessage();
				$this->saveListingPost($listing, $message, 'FANPAGE', 0);
				
				Logger::log($this->email .' ' . $fanpage->fanpage . ' -> FAILED post to FB FANPAGE: ' .$e->getMessage(), Logger::LOG_TYPE_DEBUG);

				Logger::log('Publish array was: ' . var_export($publish_array, true));

				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				
				$message = 'Unable to post to Facebook FanPage: '.$fanpage->fanpage. ': '.$e->getMessage() . ' ' . get_class($e) . ' ' . $e->getCode();
				if ($e instanceOf FacebookApiException) {
					$message .=  ' ' . $e->getSubcode();
				} 
				return array(
					'error' => true,
					'message' => $message,
					'postlink' => ''
				);
			}
		}
	}

	function publishFacebookFanpage($publish_queue, $debugMode=true)
	{

		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$listing = $publish_queue->listing;

		$facebook = $this->network_app_facebook->getFacebookAPI();
		
		if (!isset($listing->message) || empty($listing->message)) 
		{
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return(false);
		}
		
		
		//Set up the FACEBOOK VARIABLES...
		$publish_array = $this->buildFacebookPublishArray($publish_queue, 'fp');
				
		if (count($publish_array)==0) {
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return false;
		}
		
		foreach($this->fanpages as $fanpage) 
		{
			if ($fanpage->active == "yes") 
			{
				Logger::log(date("Y-m-d H:i:s").' FACEBOOK FANPAGE Member: '.$this->first_name . ' ' . $this->last_name . ' ---> ' . $fanpage->fanpage, Logger::LOG_TYPE_DEBUG);
				
				if (!empty($fanpage->access_token)) 
					$publish_array['access_token'] = $fanpage->access_token;
				
				
				return($this->publishDirectToFanpage($facebook, $fanpage, $listing, $publish_array));
												
			}
			
		}	
	
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		return array('error'=>false,'message'=>'Unable to post to Facebook FanPage','postlink'=>'');
	}

	public function create_affiliate_account() {
		if ($affiliate = Affiliate::find_by_member_id($this->id)) {
			return $affilate;
		}
		$affiliate = new Affiliate;
		$affiliate->id 		  				 = $this->id;
		$affiliate->member_id 				 = $this->id;
		$affiliate->first_payout_percent 	 = 20;
		$affiliate->recurring_payout_percent = 20;
		$affiliate->save();
		return $affiliate;
	}
	
	public function handleFacebookException(FacebookApiException $e, $facebook, $fanpage = null) {
		Logger::log('Handling exception: ' . var_export($e->getResult(), true), Logger::LOG_TYPE_DEBUG);

		$appNotifier = Zend_Registry::get('alertEmailer');

		if ($e->getType() == 'OAuthException') {
			if ($e->getCode() == 190) {
				switch ($e->getSubcode()) {
					case 458:
						$message = "Publishing for " . $this->name() . " is failing because the " . COMPANY_NAME . " app is not installed on their Facebook account.";
						 $appNotifier->send(
							'Facebook app not installed for member ' . $this->id,
							$message,
							'error-content-publish',
							true
						);
						break;
					case 459:
					case 464:
						// Notify CSRs
						$message = "Publishing for " . $this->name() . " is failing with error code " . $e->getCode() . ", subcode " . $e->getSubcode() . ". " . "This user will need to log into Facebook to correct the issue. " . APP_URL . "/member/update/" . $this->id;
						$appNotifier->send(
							'Facebook publishing failing for member ' . $this->id,
							$message,
							'error-content-publish',
							true
						);
						break;
					case 460:
						// User changed password
						$now = new DateTime;
						if ($this->token_expires > $now) $this->token_expires = $now;
							if ($this->facebook_app_visibility != 'EVERYONE') {
							if (empty($fanpage)) {
								$this->invalidTokenNotify('facebook');
							} else {
								$this->invalidTokenNotify(null, $fanpage);
							}
						}
						break;
					case 463:
						// Token expired
						$now = new DateTime;
						if ($this->token_expires > $now) $this->token_expires = $now;
						// Check if app is public. If not, notify them about the expired token.
						if ($this->facebook_app_visibility != 'EVERYONE') $this->expiredTokenNotify();
						break;
					case 467:
						// Token invalid
						$now = new DateTime;
						if (empty($fanpage)) {
							$this->invalidTokenNotify('facebook');
						} else {
							$this->invalidTokenNotify(null, $fanpage);
						}
						break;
					default : 
						Logger::log('Nothing to do for exception with subcode ' . $e->getSubcode());
						break;
				}
				
				$this->save();
				
			} elseif ($e->getCode() >= 200 && $e->getCode() <= 299) {
				
				// API permissions error. This is either user-specific, or an app problem. For the latter, it will show up in the publisher error rate checks.
				// Once the current error 200 is fixed we can enable the below.
				
				// Token invalid
				if (empty($fanpage)) {
					$this->invalidTokenNotify('facebook');
				} else {
					$this->invalidTokenNotify(null, $fanpage);
				}
				
			} elseif ($e->getCode() == 100 && stristr($e->getMessage(), 'canvas URL')) {
				// "Links must direct to the application's connect or canvas URL" error
				 $appNotifier->send(
					'Facebook publishing error ' . $e->getCode() . ': '. $e->getMessage(),
					'Seems that Facebook app ' . $facebook->getAppId() . ' has stream post URL security enabled! It must be turned off to stop these errors.',
					'facebook-error',
					true
				);
			} elseif ($e->getCode() == 2010) {
				// App restricted from uploading photos error
				Logger::log('Nothing to do for exception with code ' . $e->getCode() . ' (' . $e->getMessage() .')');
			} else {
				 $appNotifier->send(
					'Facebook publishing error ' . $e->getCode() . ': '. $e->getMessage(),
					'Unknown Facebook publishing error ' . $e->getCode() . ' (' . $e->getMessage() .') occurred for ' . $this->name() . ' (ID ' . $this->id . ')',
					'facebook-error',
					true
				);
			}
		} else {
			Logger::log('Nothing to do for exception of type ' . $e->getType());
		}
		
	}

	public function publishDirectToFacebook($facebook, $listing, $publish_array)
	{
		if (!isset($listing->message) || empty($listing->message)) 
		{
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return(false);
		}

		if ($listing->big_image)
		{
			try
			{
				//Set up the FACEBOOK VARIABLES...
				$publish_array['access_token'] = $this->network_app_facebook->getFacebookAppAccessToken();
				
				$album_id = null;
				if ($this->facebook_album_id) {
					try {
						$fb_result = $facebook->api("/".$this->facebook_album_id, 'get', $publish_array);
						$album_id = !empty($fb_result['id']) && !empty($fb_result['can_upload']) ? $fb_result['id'] : null;
					} catch (Exception $e) {
						// Album doesnt exist
					}
				}
				
				if (empty($album_id)) {
					$publish_array['name'] = COMPANY_NAME;
					$publish_array['message'] = COMPANY_NAME;
					try {
						$fb_result = $facebook->api("/".$this->uid."/albums", "post", $publish_array);
						$album_id = !empty($fb_result['id']) ? $fb_result['id'] : null;
						if (!empty($album_id)) {
							$this->facebook_album_id = $album_id;
							$this->save();
						}
					} catch (Exception $e) {
						throw new Exception('Failed creating album: ' . $e->getMessage());
					}
				}
				
				if (empty($album_id)) {
					throw new Exception('Failed creating album');
				}
				
				if (isset($listing->title) && !empty($listing->title))  
					$publish_array['name'] = $listing->title;
				elseif (isset($listing->url_mapping->url)) 
					$publish_array['name'] = $listing->url_mapping->url;
				
				$publish_array['message'] = ListingDescription::getRandomDescription($listing);
				
				//Make sure the proper jump link format exists
				
				if ($this->click_tracking) {
					if ($listing->content_link) {
						$listing->filter_content_link('fb');
						$publish_array['link'] = $listing->content_link ."/".$this->id;
					}
				} else {
					$publish_array['link'] = $listing->url_mapping->url;
				}
				
				$facebook->setFileUploadSupport(true);
				
				#PHOTO #1  
				$img = ListingImage::getRandomImage($listing);
				$tmpfile = TMP_ASSETS_STORAGE . '/' . strtotime('now') . '_' . basename($img);
			
				if (copy(CDN_ORIGIN_STORAGE . $img, $tmpfile)) {
					$publish_array['image'] = "@" . $tmpfile; 
				}
				
				$link = !empty($publish_array['link']) ? $publish_array['link'] : '';
				$publish_array['name'] = $publish_array['name'] . "\n\n" . $publish_array['message'] . "\n\n" . $link;
				
				$data = $facebook->api('/'.$album_id.'/photos', 'post', $publish_array);
				$photo_id = !empty($data['id']) ? $data['id'] : null;
						
				if (empty($photo_id)) {
					throw new Exception('Failed creating photo');
				}

				$message = "FACEBOOK Successful post to FB album id: " . $album_id;
				$this->saveListingPost($listing, $message, 'FACEBOOK', 1, $album_id, $this->facebook_friend_count);
				
				return array('error'=>false,'message'=>'Successful post to FB','postlink'=>'http://www.facebook.com/' . $album_id, 'post_id' => $album_id);
				
				
				@unlink($tmpfile);
					
			} catch(Exception $e) {
	
				if ($e instanceOf FacebookApiException) {
					$this->handleFacebookException($e, $facebook);
				}
				
				Logger::log('ERROR: ' . $e->getMessage(), Logger::LOG_TYPE_DEBUG);
				$message  = "FB FAIL, Facebook post FAIL. ERROR:".$e->getMessage();
				$this->saveListingPost($listing, $message, 'FACEBOOK', 0);
					
				Logger::log($this->email .' -> FAILED post to FB:' .$e->getMessage(), Logger::LOG_TYPE_DEBUG);
				Logger::log('Publish array was: ' . var_export($publish_array, true));
	
				$this->facebook_fails++;
				if ($this->facebook_fails > 3)
				{
					$this->facebook_auto_disabled = 1;	
				}
				
				$this->save();
	
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				return array('error'=>true,'message'=>'Unable to post to FB - Big Photo: ' . $e->getMessage(),'postlink'=>'');
	
			}		
			
		} 
		else 
		{
		
			// Set up the FACEBOOK VARIABLES...		
			$publish_array['access_token'] = $this->network_app_facebook->getFacebookAppAccessToken();
		
			Logger::log(date("Y-m-d H:i:s") . ' - FACEBOOK Publishing Member: '.$this->first_name . ' ' . $this->last_name, Logger::LOG_TYPE_DEBUG);
	
			try {
	
				$fb_result = $facebook->api("/".$this->uid."/feed", "post", $publish_array);
				$post_id = !empty($fb_result['id']) ? $fb_result['id'] : null;
				
				if (empty($post_id)) {
					throw new Exception('Failed creating post');
				}
				
				$message      = "FACEBOOK Successful post to FB: " . $post_id;
				$this->saveListingPost($listing, $message, 'FACEBOOK', 1, $post_id, $this->facebook_friend_count);
								
				//Let's grab the record one more time because it might have been updated by other running cron jobs 
				//NOTE - If publishing DIRECT, a publishing_queue object is not passed along so skip this step if necessary!!!
				if(isset($publish_queue))
				{
					if (isset($publish_queue->id) && !empty($publish_queue->id))
					{
						$new_publish  	= PublishingQueue::find_by_id($publish_queue->id);
						$publish_queue = $new_publish;
					}
					
					//Add to the inventory
					$publish_queue->current_count += $this->facebook_friend_count;
					$publish_queue->save();	
				}
							
				Logger::log('Facebook Success, '.$this->email .' -> Successful post to FB: ' . $post_id, Logger::LOG_TYPE_DEBUG);
	
				//Reset the FAIL to zero again, because we've succeeded!
				$this->facebook_fails = 0;
				$this->save();
				
				unset($post);
	
				list($fbmember,$postid) = explode("_",$post_id);
	
				if ($this->auto_like) {
					$this->likePost($postid);
				}
				
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				return array('error'=>false,'message'=>'Successful post to FB','postlink'=>'http://www.facebook.com/'.$this->uid.'/posts/'.$postid, 'post_id' => $postid);
	
	
			} catch(Exception $e) {
	
				if ($e instanceOf FacebookApiException) {
					$this->handleFacebookException($e, $facebook);
				}
		
				$message      = "FB FAIL, Facebook post FAIL. ERROR " . $e->getCode() . ": " . $e->getMessage();
				$this->saveListingPost($listing, $message, 'FACEBOOK', 0);

				Logger::log('Publish array was: ' . var_export($publish_array, true));
	
				$this->facebook_fails++;
				if ($this->facebook_fails > 3)
				{
					$this->facebook_auto_disabled = 1;	
				}
				
				$this->save();
	
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				return array('error'=>true,'message'=>'Unable to post to FB','postlink'=>'');
	
			}
		
		}		
	}

	public function publishFacebook($publish_queue, $debugMode=true)
	{	
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

        if (empty($this->facebook_publish_flag) || ($this->facebook_publish_flag == 0))
		{						
			Logger::log('FACEBOOK Publish Flag is OFF', Logger::LOG_TYPE_DEBUG);
			$message = "FB FAIL: this user had set their PERSONAL SETTINGS to OFF, do NOT publish to FB!";
			$this->saveListingPost(null, $message, 'FACEBOOK', null);
			Logger::endSection(Logger::LOG_TYPE_DEBUG);	
			return false;
		}

		if (!is_object($publish_queue)) 
		{
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return array();
		}

		$listing = $publish_queue->listing;

		$facebook = $this->network_app_facebook->getFacebookAPI();
		
		if (!isset($listing->message) || empty($listing->message)) 
		{
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return(false);
		}

		//Set up the FACEBOOK VARIABLES...
		$publish_array = $this->buildFacebookPublishArray($publish_queue, 'fb');

		return($this->publishDirectToFacebook($facebook, $listing, $publish_array));
		
	}

	public function likePost($post_id) 
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		// FIXME: May only be possible to use user access token here, so check token expiry/etc.
		
		try {
			$facebook = $this->network_app_facebook->getFacebookAPI();
			$facebook->api("/" . $post_id . "/likes", "post", array('access_token' => $this->access_token ));
			Logger::log('Auto like success');
		} catch(Exception $e) {
			Logger::log('Auto like failed' );
			Logger::log('Exception: ' . get_class($e) . ' ' . $e->getMessage());
			if ($e->getPrevious()) Logger::log($e->getPrevious()->getMessage);
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}

	function publishLinkedin($publish_queue, $debugMode=true)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		if (empty($this->linkedin_publish_flag) || empty($this->linkedin_access_token))
		{
			Logger::log('LINKEDIN Publish Flag is OFF', Logger::LOG_TYPE_DEBUG);
			$message = "LINKEDIN FAIL: this user had set their PERSONAL SETTINGS to OFF, do NOT publish to LINKEDIN!";
			$this->saveListingPost(null, $message, 'LINKEDIN', null);
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return false;
		}
		
		$listing = $publish_queue->listing;
		
		if ($listing->isContent() && !$listing->content_link) {
			Logger::log('Content has no link (#' . $listing->id . '). Publishing to LinkedIn is not possible.', Logger::LOG_TYPE_DEBUG);
			$message = 'LINKEDIN FAIL: Content has no link (#' . $listing->id . '). Publishing to LinkedIn is not possible.';
			$this->saveListingPost(null, $message, 'LINKEDIN', null);
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return false;
		}
		
		Logger::log(date("Y-m-d H:i:s")." Linkedin Publishing Member: ".$this->first_name . " " . $this->last_name, Logger::LOG_TYPE_DEBUG);
		//Randomize the message when possible
		$random_message = ListingDescription::getRandomDescription($listing);
		//Randomize the message when possible
		$random_image = "";
		if ($listing->image)
		{ 
			$img = ListingImage::getRandomImage($listing);
			$random_image = CDN_URL . $img;
		}
		
		$link_title = null;

		if ($listing->isContent())
		{
			//Make sure the proper jump link format exists
			if ($this->click_tracking) {
				if ($listing->content_link) {
					$listing->filter_content_link('ln');
					$link = $listing->content_link ."/".$this->id;
				}
			} else {
				$link = $listing->url_mapping->url;
			}
		} else {
			$link = APP_URL . "/listing/view/".$listing->id."/".$this->id."/ln";

			$price_components = explode(".", $listing->price);		
			$price_components[0] = preg_replace("/[^a-zA-Z0-9\s]/", "", $price_components[0]);
			$listing->price = $price_components[0];
			
			//$listing->price = preg_replace("/[^a-zA-Z0-9\s]/", "", $listing->price);
			
			$desc = null;
			$desc_array = null;
			if (isset($listing->price) && !empty($listing->price))
			{
			    if (is_numeric($listing->price)) $desc .= "List Price: $".number_format($listing->price). " ";
			    else $desc .= "List Price: ".$listing->price. " ";
			}
			if (isset($listing->street) && !empty($listing->street)) $desc_array[] = $listing->street;
			if (isset($listing->appartment) && !empty($listing->appartment)) $desc_array[] = 'apt ' . $listing->appartment;
			if (isset($listing->city) && !empty($listing->city)) $desc_array[] = $listing->city;
			if (isset($listing->postal_code) && !empty($listing->postal_code)) $desc_array[] = $listing->postal_code;
			if (is_array($desc_array) && count($desc_array)) $desc .= implode(", ", $desc_array);
			$link_title = $desc;
		}	
		
		$ln_service = Blastit_Service_LinkedIn::newInstance($this);
		$response = $ln_service->statusUpdate($random_message, $link, $link_title, $random_image);

		if ($response)
		{
			$message 	= "Successful post to Linkedin";
			$this->saveListingPost($listing, $message, 'LINKEDIN', 1, null, $this->linkedin_friends_count);
			//Let's grab the record one more time because it might have been updated by other running cron jobs
			if (!empty($publish_queue->id))
			{
				$new_publish = PublishingQueue::find_by_id($publish_queue->id);
				$publish_queue = $new_publish;
				if ($publish_queue) {
					//Add to the inventory
					$publish_queue->current_count += $this->linkedin_friends_count;
					$publish_queue->save();
				}
			}
			unset($post);

			//Reset the FAIL to zero again, because we've succeeded!
			$this->linkedin_fails = 0;
			$this->save();	
				
			Logger::log($this->email . ' -> Successful post to Linkedin', Logger::LOG_TYPE_DEBUG);
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return array('error'=>false,'message'=>' Successful post to Linkedin','postlink'=>'http://www.linkedin.com/nhome/');
		} else {
			Logger::log('LinkedIn response: ' . $ln_service->getResponseStatus() . ' ' . $ln_service->getErrorMessage(), Logger::LOG_TYPE_DEBUG);
			$message 	= "Linkedin post FAIL: \"" . $ln_service->getResponseStatus() . " " .$ln_service->getErrorMessage() . "\"";
			$this->saveListingPost($listing, $message, 'LINKEDIN', 0);
			$this->linkedin_fails++;
			if ($this->linkedin_fails > 3)
			{
				$this->linkedin_auto_disabled = 1;	
			}
			$this->save();

			if ($ln_service->getResponseStatus() == '401') {
				// Unauthorized - access token is not valid
				// It either expired or the user withdrew permissions
				$this->invalidTokenNotify('linkedin');
			}

			Logger::log('LINKEDIN FAIL, '.$this->email . ' -> FAILED to post to LinkedIn', Logger::LOG_TYPE_DEBUG);
	
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return array('error'=>true,'message'=>'Unable to post to LinkedIn','postlink'=>'');
		}		
		
	}

	public function publishing_active()
	{
		$publish = false;
		
		if ($this->account_type == "free") {
			$publish = true;
		} elseif ($this->payment_status == "extended_trial") {
			$publish = true;
		} elseif ($this->account_type == "paid" AND $this->payment_status == "paid") {
			$publish = true;
		} elseif ($this->payment_status == "signup") {
			$publish = true;
		}
		
		return $publish;		
	}
	
	public function save_user($fb_user)
	{
		
		if (isset($fb_user['id']) && !empty($fb_user['id']))
			$this->uid = $fb_user['id'];
		
		if (isset($fb_user['first_name']) && !empty($fb_user['first_name'])) $this->first_name = $fb_user['first_name'];
		if (isset($fb_user['last_name']) && !empty($fb_user['last_name'])) $this->last_name = $fb_user['last_name'];
		if (isset($fb_user['gender']) && !empty($fb_user['gender'])) $this->gender = $fb_user['gender'];
		if (isset($fb_user['interested_in']) && !empty($fb_user['interested_in'])) $this->interested_in = $fb_user['interested_in'][0];
		
		
		if (isset($fb_user['birthday']) && !empty($fb_user['birthday']))
		{ 						
			$raw = explode("/",$fb_user['birthday']);
			$m = trim($raw[0]);
			$d = trim($raw[1]);
			$y = trim($raw[2]);
			$this->birthday = "$y-$m-$d";
		}
		
		if (isset($fb_user['email']) && !empty($fb_user['email']))
		{
			//Make sure that FB has already verified this EMAIL addy
			//if (isset($fb_user['verified']) && !empty($fb_user['verified']))
			//{
				$this->email = $fb_user['email'];								
			//}
		}

		
		if (isset($fb_user['hometown']) && !empty($fb_user['hometown'])) 
		{	
			$raw = explode(",",$fb_user['hometown']->name);
			if (isset($raw[0]) && !empty($raw[0])) $this->hometown_city = trim($raw[0]);
			if (isset($raw[1]) && !empty($raw[1])) $this->hometown_state = trim($raw[1]);
			//if (isset($raw[2]) && !empty($raw[2])) $this->hometown_country = trim($raw[2]);
		}
		
		if (isset($fb_user['location']) && !empty($fb_user['location'])) 
		{
			$raw = explode(",",$fb_user['location']->name);
			if (isset($raw[0]) && !empty($raw[0])) $this->current_city = trim($raw[0]);
			if (isset($raw[1]) && !empty($raw[1])) $this->current_state = trim($raw[1]);
			//if (isset($raw[2]) && !empty($raw[2])) $this->current_country = trim($raw[2]);
		}		

		$this->createExtendedAccessToken();
		$this->updateFacebookFriendCount();

		$this->save();

		
		//KISS METRICS IDENTIFY USER
		if(defined('KISSMETRICS_API_KEY'))
		{
			require_once(APPLICATION_PATH . '/../lib/KissMetrics/KissMetrics.class.php');

			if(isset($member->email) && !empty($member->email))
			{
				KissMetrics::init(KISSMETRICS_API_KEY);
				KissMetrics::identify($member->email);
			}
		}		
	}	

	
	public function assign_inventory_hopper()
	{
		if (isset($this->publish_city) && !empty($this->publish_city->number_of_inventory_units) && $this->publish_city->number_of_inventory_units > 1)
		{			
            $query  = "
						SELECT inventory_id,
						SUM(if (facebook_friend_count IS NOT NULL AND facebook_publish_flag > 0 and facebook_fails < 3,facebook_friend_count,0) 
						+ if (twitter_followers IS NOT NULL AND twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL,twitter_followers,0) 
						+ if (linkedin_friends_count IS NOT NULL AND linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL,linkedin_friends_count, 0 ) ) AS total
						FROM `member`
						WHERE inventory_id IS NOT NULL
						AND status='active' 
						AND city_id=".$this->publish_city_id." 
						GROUP BY inventory_id
						ORDER BY total ASC
						LIMIT 1 
					";
			$result = Member::find_by_sql($query);
            if (isset($result[0])) {
                $INVENTORY_ID = $result[0]->inventory_id;
            }
			//If for some reason the inventory id is empty... just assign a random value
			if (empty($INVENTORY_ID))
			{
				mail("alen@alpha-male.tv, shane@socialhp.com, kevin@socialhp.com","Random inventory triggered","triggered the random inventory thingy for member_id ".$this->id . " city_id: " .$this->publish_city_id);
				$INVENTORY_ID = rand(1, $this->publish_city->number_of_inventory_units);
			}
		} else {
			$INVENTORY_ID = 1;
		}
		
	
		
		if (isset($this->publish_city) && !empty($this->publish_city->number_of_hoppers) && $this->publish_city->number_of_hoppers > 0)
		{
			$query = "
					SELECT current_hoper, count(current_hoper) as total
					FROM member
					WHERE
							( (facebook_publish_flag > 0 and facebook_fails < 3) or (linkedin_publish_flag > 0 and linkedin_fails < 3 and linkedin_access_token IS NOT NULL) or (twitter_publish_flag > 0 and twitter_fails < 3 and twitter_access_token IS NOT NULL) ) 
							AND status='active' 
							and city_id=".$this->publish_city_id . " 
							and inventory_id=".$INVENTORY_ID . "
							GROUP BY current_hoper
							ORDER by total ASC											
							";
			$result = Member::find_by_sql($query);	
	
			for($i=1;$i<=$this->publish_city->number_of_hoppers;$i++)
			{
			 	$min_hopper[$i] = 0;
			}					foreach($result as $hopper_count)
			{
				$min_hopper[$hopper_count->current_hoper] = $hopper_count->total;
			}			asort($min_hopper);
						//Find the min array element now...
			foreach($min_hopper as $key => $value)
			{
				$HOPPER_ID = $key;
				break;
			}
				
		} else {
			$HOPPER_ID = 1;	
		}
		
	
		$this->inventory_id = $INVENTORY_ID;
		$this->current_hoper = $HOPPER_ID;
		$this->save();
	}

	public function trialNotify($errors = null)
	{

	    $params =	array(
		    'member'    => $this,
	    );
		if (!empty($this->email) || !empty($this->email_override))
		{
			if (!$this->trial_email)
			{	
				$email = $this->email;
				if (!empty($this->email_override)) $email = $this->email_override;
				$view = Zend_Registry::get('view');
				$fromEmail = defined('HELP_EMAIL') ? HELP_EMAIL : 'noreply@'.APPLICATION_ENV.'.com';
				$fromName = defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
								
				
				$subject = "Your 7 day ".COMPANY_NAME."  trial starts now!";
				$htmlBody = $view->partial('email/trial.html.php', $params );
								
				
				if ($this->email)
				{		
				    $member_email = !empty($this->email_override) ? $this->email_override : $this->email;
					
					$mandrill = new Mandrill(array('TRIAL_NOTIFY'));
					$mandrill->setFrom($fromName, $fromEmail);
					$mandrill->addTo($this->first_name . " " . $this->last_name, $member_email);
		
					if ($member_email != $this->email)
					{
						$mandrill->addTo("", $member_email);
					}
					$mandrill->send($subject, $htmlBody);
					
					
					//Keep track of the Email in our local LOG
					$mail = new ZendMailLogger('TRIAL_NOTIFY', $this->id);
					$mail->addTo($member_email, $member_email);
					$mail->setSubject($subject);
					$mail->log();
				}
	
				$this->trial_email = 1;
				$this->save();
			}	
		}
		
	}

	public function activateNotify($errors = null)
	{

	    $params =	array(
		    'member'    => $this
	    );
		if (isset($this->email) && !empty($this->email))
		{
			if (!$this->activate_email)
			{
				$view = Zend_Registry::get('view');
				$fromEmail	=   defined('HELP_EMAIL') ? HELP_EMAIL : 'noreply@'.APPLICATION_ENV.'.com';
				$fromName		=   defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
				
				$subject  = "Ah Nuts, Your ".COMPANY_NAME." Trial Is Over.";
				$htmlBody = $view->partial ( 'email/activate.account.html.php', $params );
				
		
		
				$email = $this->email;
				if (!empty($this->email_override)) $email = $this->email_override; 
				$mail = new ZendMailLogger('TRIAL_OVER', $this->id);
	
				if ($this->email)
				{		
				    $member_email = !empty($this->email_override) ? $this->email_override : $this->email;
					
					$mandrill = new Mandrill(array('TRIAL_OVER'));
					$mandrill->setFrom($fromName, $fromEmail);
					$mandrill->addTo($this->first_name . " " . $this->last_name, $member_email);
		
					if (  $member_email != $this->email )
					{
						$mandrill->addTo("", $member_email);
					}
					$mandrill->send($subject, $htmlBody);
					//Keep track of the Email in our local LOG
					$mail = new ZendMailLogger('TRIAL_OVER', $this->id);
					$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
					$mail->setFrom($fromEmail, $fromName);
					$mail->addTo($member_email, $member_email);
					$mail->setSubject($subject);
					$mail->log();
				}
	
				$this->activate_email = 1;
				$this->payment_status = 'cancelled';
												
				$this->save();
				
				mail(HELP_EMAIL, "Trial Expiry: " .$this->first_name . " " . $this->last_name, APP_URL . "/admin/member/".$this->id);
			}
		}
		
		$this->payment_status = 'signup_expired';
		$this->save();
	}

	public function expiredTokenNotify($errors = null)
	{
		if ($this->pword_reminder > 2) return false;

		$subject  = "Oh boy! Seems like there's a problem with your ".COMPANY_NAME." account.";
		$this->sendTemplateEmail($subject, 'email/expiredtokenfound.html.php', 'EXPIRED_TOKEN', array(), true);
		
		$this->pword_reminder++;
		$this->save();
	}
	
	public function invalidTokenNotify($networks = null, $fanpages = null)
	{
		// If no network is specified we default to facebook
		$networks = empty($networks) ? array() : $networks;
		$networks = !is_array($networks) && !empty($networks) ? array((string)$networks) : $networks;
		
		$fanpages = empty($fanpages) ? array() : $fanpages;
		$fanpages = !is_array($fanpages) && !empty($fanpages) ? array($fanpages) : $fanpages;
		
		if (isset($this->email) && !empty($this->email))
		{
			$finalStrikeNetworks = array();
			$finalStrikeFanpages = array();
			
			$subject  = "Oh boy! Seems like there's a problem with your ".COMPANY_NAME." account.";
			
			foreach ($networks as $network) {
				$reminder_field = 'token_reminder';
				switch ($network) {
					case 'twitter':
						$reminder_field = 'twitter_token_reminder';
					break;
					case 'linkedin':
						$reminder_field = 'linkedin_token_reminder';
					break;
				}
				
				if ($this->$reminder_field < 3) {
					$params = array(
						'network_type' => $network,
						'network_name' => ucfirst(strtolower($network))
					);
					
					$this->sendTemplateEmail($subject, 'email/invalidtokenfound.html.php', 'AUTH_REMINDER', $params, true, 'PASSWORD_CHANGE');

					$this->$reminder_field++;
					$this->save();
					
					if (3 == $this->$reminder_field) {
						$finalStrikeNetworks[] = $network;
					}
				}
			}
			
			foreach ($fanpages as $fanpage) {
				if ($fanpage->token_reminder < 3) {
					$params = array(
						'network_type' => 'fanpage',
						'network_name' => 'Fan page: ' . $fanpage->fanpage
					);
					
					$this->sendTemplateEmail($subject, 'email/invalidtokenfound.html.php', 'AUTH_REMINDER', $params, true, 'PASSWORD_CHANGE');

					$fanpage->token_reminder++;
					$fanpage->save();
					
					if (3 == $fanpage->token_reminder) {
						$finalStrikeFanpages[] = $fanpage;
					}
				}
			}
			
			
			if (!empty($finalStrikeNetworks) || !empty($finalStrikeFanpages)) {
				$emailer = Zend_Registry::get('alertEmailer');
				$member_page_url = APP_URL . '/admin/member/' . $this->id . '#update';
				$subject = 'Member publishing auto disabled (3 strikes): ' . $this->first_name . ' ' . $this->last_name . ' (' . $this->id . ')';
				$msg_lines = array();
				$msg_lines[] = 'We have disabled publishing for member, ' . 
						'<a href="' . $member_page_url . '">' . $this->first_name . ' ' . $this->last_name . '</a> ' . 
						'(' . $this->id . ')' . ' via the following networks, after 3 failed publishing attemps:';
				
				$msg_lines[] = '';
				$msg_lines[] = 'FAILED NETWORK(S): ';
				if (!empty($finalStrikeNetworks)) {
					foreach ($finalStrikeNetworks as $network) {
						$msg_lines[] = '- ' . ucfirst(strtolower($network));
					}
				}
				if (!empty($finalStrikeFanpages)) {
					foreach ($finalStrikeFanpages as $network) {
						$msg_lines[] = '- ' . 'Fan page: ' . $fanpage->fanpage;
					}
				}
				$msg_lines[] = '';
				$msg_lines[] = '------------------------------------------------------------------------';
				$msg_lines[] = 'Contact details: ';
				$msg_lines[] = 'Name: ' . '<a href="' . $member_page_url . '">' . $this->first_name . ' ' . $this->last_name . '</a> ' . '(' . $this->id . ')';
				$msg_lines[] = 'Company Name: ' . (!empty($this->company_name) ?  $this->company_name : '?');
				$msg_lines[] = '';
				$msg_lines[] = 'Email: ' . (!empty($this->email) ?  $this->email : '?');
				$msg_lines[] = 'Email Override: ' . (!empty($this->email_override) ?  $this->email_override : '?');
				$msg_lines[] = 'Business Email: ' . (!empty($this->business_email) ?  $this->business_email : '?');
				$msg_lines[] = '';
				$msg_lines[] = 'Address: ' . (!empty($this->address) ?  $this->address : '?');
				$msg_lines[] = 'Address 2: ' . (!empty($this->address2) ?  $this->address2 : '?');
				$msg_lines[] = 'City: ' . (!empty($this->city) ?  $this->city : '?');
				$msg_lines[] = 'State: ' . (!empty($this->state) ?  $this->state : '?');
				$msg_lines[] = 'Country: ' . (!empty($this->country) ?  $this->country : '?');
				$msg_lines[] = 'Zip: ' . (!empty($this->zip) ?  $this->zip : '?');
				$msg_lines[] = '';
				$msg_lines[] = 'Phone: ' . (!empty($this->phone) ?  $this->phone : '?');
				$msg_lines[] = '';
				$msg_lines[] = 'Member since: ' . $this->created_at->format('Y-m-d');
				
				$msg = implode("\n", $msg_lines);
				$emailer->send($subject, $msg, 'publish-auto-disable');
			}
		}
	}
	
	public function sendTemplateEmail($subject, $template, $tag, $params = array(), $send_to_both_emails = false, $mandrill_tag = null) {
	
		if (!$this->email) return false;
	
		$email = new Blastit_Email_MandrillLogger($this, $tag, $mandrill_tag);
		$email->setSubject($subject);
		$email->setHtmlTemplate($template );
		$email->setTemplateParams($params);
		
		if ($send_to_both_emails && ($this->email != $this->email_override)) {
			$email->addTo($this->name(), $this->email);
		}
		
		return $email->send();
	}
	
	public function notifyNetworksDisabled()
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$networks = array();
		if ($this->facebook_auto_disabled) {
			$networks[] = 'facebook';
		}
		if ($this->twitter_auto_disabled) {
			$networks[] = 'twitter';
		}
		if ($this->linkedin_auto_disabled) {
			$networks[] = 'linkedin';
		}
		
		$member_email = !empty($this->email_override) ? $this->email_override : $this->email;
		
		if (empty($networks)) {
			Logger::log('No disabled networks found', Logger::LOG_TYPE_DEBUG);
		} elseif (!empty($member_email)) {
			Logger::log('Networks currently disabled: ' . implode(', ', $networks), Logger::LOG_TYPE_DEBUG);
			
			$recent_emails = EmailLog::all(array(
				'conditions' => array(
					"member_id = ? AND " . 
					"DATE(created_at) >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND " . 
					"type IN ('EXPIRED_TOKEN', 'AUTH_REMINDER', 'NETWORKS_DISABLED')"
					, $this->id
				),
				'limit' => 1
			));
			
			if (!empty($recent_emails)) {
				Logger::log('We have already sent an email within the past 7 days', Logger::LOG_TYPE_DEBUG);
			} else {
				$view = Zend_Registry::get('view');
				$fromEmail = 'networksdisabled@'.APPLICATION_ENV.'.com';
				$fromName = defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
					
				$subject  = "Oh boy! Seems like there's a problem with your ".COMPANY_NAME." account.";
				
				$params = array(
					'member' => $this, 
					'networks' => $networks
				);
				$htmlBody = $view->partial('email/networksdisabled.html.php', $params);
				
				$mandrill = new Mandrill(array('NETWORKS_DISABLED'));
				$mandrill->setFrom($fromName, $fromEmail);
				$mandrill->addTo($this->first_name . " " . $this->last_name, $this->email);
				if ($member_email != $this->email)
				{
					$mandrill->addTo("", $this->email);
				}
				$mandrill->send($subject, $htmlBody);
				
				Logger::log('Email notification sent', Logger::LOG_TYPE_DEBUG);

				//Keep track of the Email in our local LOG
				$mail = new ZendMailLogger('NETWORKS_DISABLED', $this->id);
				$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
				$mail->setFrom($fromEmail, $fromName);
				$mail->addTo($member_email, $member_email);
				$mail->setSubject($subject);
				$mail->log();
			}
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}

	public function welcome_email($params = null)
	{
		
		$view = Zend_Registry::get('view');
		$params =	array(
		    'member' => $this,
	    );
		$subject = 'Welcome to '. COMPANY_NAME .' '.$this->first_name;
		$htmlBody = $view->partial('email/welcome.html.php', $params);
		$email = new ZendMailLogger('WELCOME', $this->id);
		$member_email = !empty($this->email_override) ? $this->email_override : $this->email;
		$fromEmail = defined('HELP_EMAIL') ? HELP_EMAIL : 'noreply@'.APPLICATION_ENV.'.com';
		$fromName = defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
		$mandrill = new Mandrill(array('WELCOME_EMAIL'));
		
		$mandrill->addTo($this->first_name . " " . $this->last_name, $member_email);
		$mandrill->setFrom($fromName, $fromEmail);
		$mandrill->send($subject, $htmlBody);
		
		//Keep track of the Email in our local LOG
		$mail = new ZendMailLogger('WELCOME_EMAIL', $this->id);
		$mail->setBodyHtml( $htmlBody, 'utf-8', 'utf-8');
		$mail->setFrom($fromEmail, $fromName);
		$mail->addTo($member_email, $member_email);
		$mail->setSubject($subject);
		$mail->log();
			
	}		
	public function tokenExpiringNotify($errors = null)
	{
	    $params =	array(
		    'member'    => $this,
	    );
		if (isset($this->email) && !empty($this->email))
		{
			$view = Zend_Registry::get('view');
			$fromEmail = defined('HELP_EMAIL') ? HELP_EMAIL : 'noreply@'.APPLICATION_ENV.'.com';
			$fromName = defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
				
			$subject  = "It's been almost 60 days! You must log into your ".COMPANY_NAME." account to keep it active!";	
			$htmlBody = $view->partial ( 'email/tokenexpiring.html.php', $params );
			
			$member_email = !empty($this->email_override) ? $this->email_override : $this->email;
			$mandrill = new Mandrill(array('EXPIRED_TOKEN'));
			$mandrill->setFrom($fromName, $fromEmail);
			$mandrill->addTo($this->first_name . " " . $this->last_name, $member_email);
			if ($member_email != $this->email)
			{
				$mandrill->addTo("", $member_email);
			}
			$mandrill->send($subject, $htmlBody);

			//Keep track of the Email in our local LOG
			$mail = new ZendMailLogger('EXPIRED_TOKEN', $this->id);
			$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
			$mail->setFrom($fromEmail, $fromName);
			$mail->addTo($member_email, $member_email);
			$mail->setSubject($subject);
			$mail->log();
		}
	}	

	public function creditAffiliate($credits = 1) 
	{
		$conditions_sql = "referred_member_id = ?";
		$conditions = array($conditions_sql, $this->id);
		$previously_credited = ReferralCredit::count('all', array('conditions' => $conditions));
		if ($previously_credited) {
			throw new Zend_Exception('Cannot assign credit as credit has already been assigned.');
		}
		
		if ($this->referring_affiliate_id == null && $this->referring_member_id == null) {
			throw new Zend_Exception('Cannot assign credit to null affiliate.');
		}
		if ( (($this->referring_affiliate instanceOf Affiliate) == false) && ($this->referring_member instanceOf Member) == false ) {
			throw new Zend_Exception('Cannot assign credit to invalid affiliate.');
		}
		if ( ($this->referring_member_id != $this->id) && (!$this->referring_affiliate || $this->referring_affiliate->member_id != $this->id))
		{
			if ($this->referring_affiliate) {
				$this->referring_affiliate->owner->assign_referer_credits($this, $credits);
			} elseif ($this->referring_member) {
				$this->referring_member->assign_referer_credits($this, $credits);
			}
			return true;
		} else {
			$this->referring_affiliate_id = null;		
			$this->notifySelfReferral();
			return false;
		}	
	}

	public function assign_referer_credits($referred, $credits)
	{
		if ($referred instanceOf Member)
		{
			// Has the member received credit
			$existsCredit	=	ReferralCredit::find_by_member_id_and_referred_member_id($this->id, $referred->id);
			$alreadyGotCredit = !is_null($existsCredit);
			//IS THIS A KNOWN AFFILIATE? IF SO, DON'T GIVE THEM A "CREDIT"
			if ($this->isAffiliate() || $alreadyGotCredit)
			{
				return FALSE;	
			}
			if (!empty($this->id))
			{
				if ($this->credits)
				{
					$this->credits	+= $credits;
				} else
				{
					$this->credits	= $credits;
				}
				$this->save();
				
				// Creating invoice
				$payment = new Payment();
	
				$payment->paypal_confirmation_number    	= 'FREE_REFERRAL_CREDIT';
				$payment->paypal_transaction_expire_date	= '';
				$payment->cc_expire_date                	= '';
				$payment->last_four   					= '';
				$payment->first_name  					= $this->first_name;
				$payment->last_name   					= $this->last_name;
				$payment->payment_type					= 'clientfinder';
	
				//Save the member id
				$payment->member_id	= $this->id;
	
				$payment->address1	= $this->address;
				$payment->city    	= $this->city;
				$payment->state   	= $this->state;
				$payment->country 	= $this->country;
				$payment->zip     	= $this->zip;
				$payment->amount  	= 0;
				$payment->taxes   	= 0;
				$payment->save();
	
				$next_payment_date    	=	is_null($this->next_payment_date) ? date("Y-m-d") : $this->next_payment_date->format("Y-m-d");
				$this->next_payment_date	=	date("Y-m-d", strtotime($next_payment_date." +30day"));
				$this->save();
				//new entry for referral_credit
	
				$rc	= new ReferralCredit();
				$rc->member_id			= $this->id;
				$rc->referred_member_id	= $referred->id;
				$rc->created_at			= date('Y-m-d H:i:s');
				$rc->payment_id 		= $payment->id;
				$rc->save();
				
				if (count($this->referral_credits) >= 10) {
					$this->account_type   = 'free';
					$this->payment_status = 'free';
					$this->addNote('Account type set to "free" because member reached 10 referrals on ' . date('Y-m-d g:ia'));
					$this->save();
				}
				mail("alen@alpha-male.tv, shaun@shaunnilsson.com, ".HELP_EMAIL,
					"REFERRAL AWARDED",
					"REFERRAL CREDIT AWARDED TO: " . $this->first_name . " " . $this->last_name . " " . $this->email. "\nREFERRED: " . $referred->first_name . " " . $referred->last_name . " " . $referred->email
				);
	
			}			return TRUE;
		}
		
		return FALSE;
	}
	

    	static function getcity()
	{
	    return (!empty($_COOKIE['selectedcity'])) ? $_COOKIE['selectedcity'] : false;
	}

	public function to_array(array $options=array())
	{
		return $this->serialize('array', $options);
	}
	static function get_reach_by_city($city_id=0)
	{
			$query = "
					SELECT c.name, c.id, COUNT(DISTINCT(m.id)) AS total_member, c.country, SUM( if ( m.facebook_friend_count, m.facebook_friend_count, 0 )  + if ( m.twitter_followers, m.twitter_followers, 0 ) + if ( m.linkedin_friends_count, m.linkedin_friends_count, 0 ) ) AS total_reach,
							m.facebook_friend_count, m.linkedin_friends_count, m.twitter_followers 
					FROM city c 
					INNER JOIN member m ON c.id = m.city_id
					LEFT JOIN payment p on p.member_id = m.id
					WHERE ". ($city_id ? "m.city_id=".$city_id ." AND " : "") ."m.status = 'active' 
						AND 
							(
								DATEDIFF(NOW(), m.created_at) <= 14
								OR
								(
									p.payment_type = 'clientfinder' and m.payment_status='paid' AND
									m.account_type='paid' AND
									p.amount > 0
								) OR
								(
									p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' AND
									DATEDIFF(NOW(), m.created_at) <= 30 AND
									p.payment_type = 'clientfinder' 
								)
							)
					GROUP BY c.id
					ORDER by total_reach DESC, c.country ASC, c.id ASC
					";
			$result = Member::find_by_sql($query);
			return $result;
	}
	
	public function notifySelfReferral() 
	{
		$fromEmail	=   defined('HELP_EMAIL') ? HELP_EMAIL : 'noreply@'.APPLICATION_ENV.'.com';
		$fromName	=   defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
		
		$view = Zend_Registry::get('view');
		$params	=   array(
			'member'      => $this,
		);
		
		$subject  = "Oops! You can't refer yourself";
		$htmlBody =   $view->partial('email/self-referral.html.php', $params);
		
		$mandrill = new Mandrill(array('SELF-REFERRAL'));
		$mandrill->setFrom($fromName, $fromEmail);
		$mandrill->addTo($this->first_name . " " . $this->last_name, $this->email);
		$mandrill->send($subject, $htmlBody);

		//Keep track of the Email in our local LOG
		$mail = new ZendMailLogger('SELF-REFERRAL', $this->id);
		$mail->setBodyHtml ( $htmlBody, 'utf-8', 'utf-8' );
		$mail->setFrom ( $fromEmail, $fromName );
		$mail->addTo ( $email, $email );
		$mail->setSubject ( $subject );
		$mail->log();	
	}

	public function update_account_status($is_payment = true)
	{
		// If an actual payment was made update the next payment date
		if ($is_payment) 
		{
			$next_payment_date = null;
			switch ($this->billing_cycle) 
			{
				case 'month':
					$next_payment_date = date('Y-m-d', strtotime('+1 month'));
				break;
				case 'biannual':
					$next_payment_date = date('Y-m-d', strtotime('+6 months'));
				break;
				case 'annual':
					$next_payment_date = date('Y-m-d', strtotime('+1 year'));
				break;
			}
			$this->next_payment_date = $next_payment_date;
		}
	 
		$this->account_type = 'paid';
		$this->payment_status = 'paid';
		$this->billing_failures = 0;
		$this->clientfinder_switch = 1;
		$this->save();
	}
	
	/**
	 * Get ISO alpha-2 country code from country name
	 * 
	 * @param string $country
	 * @reutrn string
	 */
	private function getCountryCode($country)
	{
		// lower case
		$country = mb_strtolower(trim($country));
		// ensure only single space word separator
		$country = preg_replace('!\s+!', ' ', $country);
		
		$country_map = array(
			'canada' => 'ca',
			'united states' => 'us',
			'united kingdom' => 'uk'
		);
		
		$country = isset($country_map[$country]) ? $country_map[$country] : $country;
		
		return $country;
	}
	
	/**
	 * Get alpha-2 state code from state name
	 * 
	 * @param string $country
	 * @param string $state
	 * @reutrn string
	 */
	private function getStateCode($country, $state)
	{
		$country = $this->getCountryCode($country);
		// lower case
		$state = mb_strtolower(trim($state));
		// ensure only single space word separator
		$state = preg_replace('!\s+!', ' ', $state);
		$state_map = array(
			'ca' => array(
				'ontario' => 'on',
				'quebec' => 'qc',
				'nova scotia' => 'ns',
				'new brunswick' => 'nb',
				'manitoba' => 'mb',
				'british columbia' => 'bc',
				'prince edward island' => 'pe',
				'saskatchewan' => 'sk',
				'alberta' => 'ab',
				'newfoundland and labrador' => 'nl',
				'newfoundland' => 'nl',
				'northwest territories' => 'nt',
				'yukon' => 'yt',
				'nunavut' => 'nu',
			)
		);
		
		$state = isset($state_map[$country]) && isset($state_map[$country][$state]) ? $state_map[$country][$state] : $state;
		
		return $state;
	}
	
	/**
	 * Get tax data for this member
	 * 
	 * @param array $params Payment request params
	 * @return array
	 */
	public function getTaxData($params = array())
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$params = !empty($params) ? $params : array();
		$country = !empty($params['country']) ? $params['country'] : null;
		$state = !empty($params['state']) ? $params['state'] : null;
		
		
		$result = array();
	
		$tax_rate_map = array(
			'ca' => array(
				'ab' => 0.05,
				'mb' => 0.05,
				'nt' => 0.05,
				'nu' => 0.05,
				'pe' => 0.14,
				'qc' => 0.05,
				'sk' => 0.05,
				'yt' => 0.05,
				'bc' => 0.05,
				'on' => 0.13,
				'nb' => 0.13,
				'nl' => 0.13,
				'ns' => 0.15
			),
			'us' => array(
				'*' => 0
			)
		);
		
		$tax_rate = null;
		
		// Look for tax rate based on country and state values passed in as arguments
		if ($tax_rate === null && !empty($country) && !empty($state)) {
			$country = $this->getCountryCode($country);
			$state = $this->getStateCode($country, $state);
			Logger::log(
				'Looking for tax rate based on specified country and state: ' . 
				json_encode(array('country' => $country, 'state' => $state))
			);
			if (isset($tax_rate_map[$country])) {
				if (isset($tax_rate_map[$country][$state])) {
					$tax_rate = $tax_rate_map[$country][$state];
				} elseif (isset($tax_rate_map[$country]['*'])) {
					$tax_rate = $tax_rate_map[$country]['*'];
				}
			}
			if ($tax_rate === null) {
				// We didnt find a tax rate based on the passed in arguments
				// but since the passed in arguments should override any other values
				// do not fall back to other methods of calculating the tax rate
				$tax_rate = 0;
			}
		}
		
		// Look for tax rate based on country and state of last payment details
		if ($tax_rate === null) {
			$last_payment = Payment::find(array(
				'conditions' => 'member_id=' . addslashes($this->id) . ' and payment_type="clientfinder"', 
				'limit' => 1, 
				'order'=>'id desc'
			));
			if ($last_payment) {
				$country = $this->getCountryCode($last_payment->country);
				$state = $this->getStateCode($country, $last_payment->state);
				Logger::log(
					'Looking for tax rate based on last payment details: ' . 
					json_encode(array('country' => $country, 'state' => $state))
				);
				if (isset($tax_rate_map[$country])) {
					if (isset($tax_rate_map[$country][$state])) {
						$tax_rate = $tax_rate_map[$country][$state];
					} elseif (isset($tax_rate_map[$country]['*'])) {
						$tax_rate = $tax_rate_map[$country]['*'];
					}
				}
			}
		}
		// Look for tax rate based on country and state of default publish city
		if ($tax_rate === null && $this->default_city) {
			$country = $this->getCountryCode($this->default_city->country);
			$state = $this->getStateCode($country, $this->default_city->state);
			Logger::log(
				'Looking for tax rate based on default publish city: ' . 
				json_encode(array('country' => $country, 'state' => $state))
			);
			if (isset($tax_rate_map[$country])) {
				if (isset($tax_rate_map[$country][$state])) {
					$tax_rate = $tax_rate_map[$country][$state];
				} elseif (isset($tax_rate_map[$country]['*'])) {
					$tax_rate = $tax_rate_map[$country]['*'];
				}
			}
		}
		
		if ($tax_rate !== null) {
			Logger::log('Tax rate found: ' . $tax_rate);
		} else {
			Logger::log('Tax rate not found');
		}
		
		// We coulnt find a tax rate, set the tax rate to zero
		if ($tax_rate === null) {
			$tax_rate = 0;
		}
		
		$result = array(
			'country' => strtoupper($country),
			'state' => strtoupper($state),
			'rate' => $tax_rate
		);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public function processPayment($params)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		if (!$this->payment_gateway) {
			$default_payment_gateway = PaymentGateway::find_by_is_default(1);
			$this->payment_gateway_id = $default_payment_gateway->id;
			$this->save();
		}
		
		$result = $this->payment_gateway->processPayment($this, $params);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		return $result;
	}

	public function saveAuthorization($params)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		if (!$this->payment_gateway) {
			$default_payment_gateway = PaymentGateway::find_by_is_default(1);
			$this->payment_gateway_id = $default_payment_gateway->id;
			$this->save();
		}
		
		$result = $this->payment_gateway->saveAuthorization($member, $params);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		return $result;
	}
	
	private function getMembershipStatus() {
		// The $status values below must match the status options in MailChimp!
		$status = null;
		
		if ($this->account_type == 'paid') {
			switch ($this->payment_status) {
				case 'signup' :
					$status = 'Unpaid';
					break;
				case 'signup_expired' :
					$status = 'Unpaid dropoff';
					break;
				case 'paid' :
					$status = 'Paid';
					break;
				case 'free' :
					// this shouldn't happen, but it does
					$status = 'Paid';
					break;
				case 'payment_failed' :
					$status = 'Paid dropoff';
					break;
				case 'cancelled' :
					$status = 'Paid dropoff';
					break;
				case 'extended_trial' :
					$status = 'Paid';
					break;
				case 'refund_issued' :
					$status = 'Paid';
					break;
			}
		} elseif ($this->account_type == 'free') {
			switch ($this->payment_status) {
				case 'signup' :
					$status = 'Unpaid';
					break;
				case 'signup_expired' :
					$status = 'Unpaid dropoff';
					break;
				case 'paid' :
					// this shouldn't happen, but it does
					$status = 'Paid';
					break;
				case 'free' :
					$status = 'Unpaid';
					break;
				case 'payment_failed' :
					$status = 'Unpaid dropoff';
					break;
				case 'cancelled' :
					$status = 'Unpaid dropoff';
					break;
				case 'extended_trial' :
					$status = 'Unpaid';
					break;
				case 'refund_issued' :
					// this shouldn't happen
					$status = 'Unpaid';
					break;
			}
		}
		
		return $status;
	}
	
	private function getCTCTList() {
		
		$list = new ContactList;
		
		switch ($this->getMembershipStatus()) {
			case 'Paid' :
				$list->id = '2';
				break;
			case 'Paid dropoff' :
				$list->id = '4';
				break;
			case 'Unpaid' :
				$list->id = '3';
				break;
			case 'Unpaid dropoff' :
				$list->id = '5';
				break;
		}
		
		return $list;
		
	}

	public function addToConstantContact() 
	{
	
		if (!defined('CONSTANT_CONTACT_API_KEY')) return false;
		
		// $api = new ConstantContact_API('oauth2', CONSTANT_CONTACT_API_KEY, CONSTANT_CONTACT_USERNAME, CONSTANT_CONTACT_OAUTH2_ACCESS_TOKEN);
		
		$cc = new ConstantContact(CONSTANT_CONTACT_API_KEY);

		$old_email 	= null;

		if ($this->id) {
			$old = self::find($this->id);
			if ($old) $old_email 	= !empty($old->email_override) ? $old->email_override : $old->email;
		}
		
		$new_email 	= !empty($this->email_override) ? $this->email_override : $this->email;

		if (!$new_email || !filter_var($new_email, FILTER_VALIDATE_EMAIL)) return false;

		// Clean up old records under the wrong email address
		
		if ($old->email_override != null && $old->email != $old->email_override) {
			$dupe = $cc->getContactByEmail(CONSTANT_CONTACT_OAUTH2_ACCESS_TOKEN, $old->email);
			if (!empty($dupe->results)) $cc->deleteContact(CONSTANT_CONTACT_OAUTH2_ACCESS_TOKEN, $dupe->results[0]);
		}

		$response = $cc->getContactByEmail(CONSTANT_CONTACT_OAUTH2_ACCESS_TOKEN, $old_email);
				
		if (empty($response->results)) {

			$method	 = "addContact";
			$contact = new Contact();
			$contact->addEmail($new_email);

		} else {
			
			$method	 = "updateContact";
			$contact = $response->results[0];

			if ($old_email != $new_email) {
				$contact->email_addresses = array();
				$contact->addEmail($new_email);
			}
		
			$contact->lists = array();
			
		}

		$contact->addList($this->getCTCTList());

		$contact->first_name 	= $this->first_name;
		$contact->last_name 	= $this->last_name;
		
		$contact->addCustomField( CustomField::create( array('name' => 'CustomField1', 'value' => $this->getMembershipStatus() )) );
		$contact->addCustomField( CustomField::create( array('name' => 'CustomField2', 'value' => $this->payment_status )) );
		$contact->addCustomField( CustomField::create( array('name' => 'CustomField3', 'value' => $this->account_type )) );
		$contact->addCustomField( CustomField::create( array('name' => 'CustomField4', 'value' => (string) $this->id )) );

		try 
		{
			return $cc->$method(CONSTANT_CONTACT_OAUTH2_ACCESS_TOKEN, $contact, false);
		} catch (Exception $e) {
			if ($e->getCode() == 0 || $e->getCode() == 409) return false;
			throw $e;
		}

		return true;

	}

	public function addToMailchimp() 
	{
		if (!defined('MAILCHIMP_API_KEY')) return false;
		$mcapi = new MCAPI(MAILCHIMP_API_KEY);
		$merge_vars = array();
		if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && $_SERVER['HTTP_X_FORWARDED_FOR'])
			$remote_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		elseif (isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']))
			$remote_ip = $_SERVER['REMOTE_ADDR'];
		else
			$remote_ip = null;
		if ($remote_ip)
			$merge_vars['OPTINIP'] = $_SERVER['REMOTE_ADDR'];		
		
		$merge_vars['ID']         = $this->id;
		$merge_vars['STATUS']     = $this->getMembershipStatus();
		$merge_vars['PMT_STATUS'] = $this->payment_status;
		$merge_vars['ACCT_TYPE']  = $this->account_type;
		$email_type        = 'html';
		$double_optin      = false;
		$update_existing   = true;
		$replace_interests = false;
		$send_welcome      = false;

		if (!empty($this->first_name)) $merge_vars['FNAME'] = $this->first_name;
		if (!empty($this->last_name)) $merge_vars['LNAME']  = $this->last_name;

		/****
		if (!empty($this->default_city->name))
		{
			$merge_vars['GROUPINGS'][0]['name'] = 'CITY';
			$merge_vars['GROUPINGS'][0]['groups'] = strtoupper($this->default_city->name);
			//$merge_vars['CITY'] = $this->default_city->name;
		} ***/
		
		$old_email 	= null;

		if ($this->id) {
			$old = self::find($this->id);
			if ($old) $old_email 	= !empty($old->email_override) ? $old->email_override : $old->email;
		}
		
		$new_email = !empty($this->email_override) ? $this->email_override : $this->email;

		if (!$new_email || !filter_var($new_email, FILTER_VALIDATE_EMAIL)) return false;

		// Clean up old records under the wrong email address
		
		if (isset($old)) {
			if ($old->email_override != null && $old->email != $old->email_override) {
				$mcapi->listUnsubscribe(MAILCHIMP_LIST_ID, $old->email, false, false, false);
			}
		}
	    
		if ($old_email != $new_email) $mcapi->listUnsubscribe(MAILCHIMP_LIST_ID, $old_email, false, false, false);
		$mcapi->listSubscribe(MAILCHIMP_LIST_ID, $new_email, $merge_vars, $email_type, $double_optin, $update_existing, $replace_interests, $send_welcome);
		
		if ($mcapi->errorCode) {
			throw new Zend_Exception($mcapi->errorMessage, $mcapi->errorCode);
		}
		
		return true;
		
	}

	public function isPublishPermissionOn() {
		return $this->hasFacebookPermission('publish_stream') || $this->hasFacebookPermission('publish_actions');
	}

	public function hasPhotoPermissions() {
		return $this->hasFacebookPermission('user_photos');
	}

	public function isManagePagesPermissions() {
		return $this->hasFacebookPermission('manage_pages');
	}
	
	public function hasFacebookPermission($perm) {
		$access_token = $this->network_app_facebook->getFacebookAppAccessToken();
		$graph_url = "https://graph.facebook.com/" . $this->uid . "/permissions?access_token=" . $access_token;
		$permissions = json_decode(@file_get_contents($graph_url),true);
		return is_array($permissions['data'][0]) ? array_key_exists($perm, $permissions['data'][0]) : null;
	}

	public function isAffiliate() {
		return ($this->affiliate_account instanceOf Affiliate);
	}
	
	public function isOAuthClient() {
		return ($this->oauth_client instanceOf OauthClient);
	}
	
	public function isPaying() {
		return Payment::count(array("conditions" => array("member_id = ? and payment_type='clientfinder' and amount > 0", $this->id)));
	}
	
	public function isActive() {
		$inactive = array('signup_expired','payment_failed','cancelled');
		return !in_array($this->payment_status, $inactive);
	}
	
	public function getFirstPayout() {
		if (false === ($this->referring_affiliate instanceOf Affiliate)) return false;
		// The first payout percentage applies to the cost of the first month only
		// not the total amount of the first payment
		$result = false;
		switch ($this->billing_cycle) {
			case 'month';
				$result = ($this->referring_affiliate->first_payout_percent / 100) * $this->price;
				$result = round($result, 2);
			break;
			case 'biannual';
				$result = (($this->referring_affiliate->first_payout_percent / 100) * $this->price_month) + 
						((($this->referring_affiliate->recurring_payout_percent / 100) * $this->price_month) * 5);
				$result = round($result, 2);
			break;
			case 'annual';
				$result = (($this->referring_affiliate->first_payout_percent / 100) * $this->price_month) + 
						((($this->referring_affiliate->recurring_payout_percent / 100) * $this->price_month) * 11);
				$result = round($result, 2);
			break;
		}
		return $result;
	}
	
	public function getRecurringPayout() {
		if (false === ($this->referring_affiliate instanceOf Affiliate)) return false;
		return round(($this->referring_affiliate->recurring_payout_percent / 100) * $this->price, 2);
	}

	public function doReferralPayout($payment) {
		if (false === ($this->referring_affiliate instanceOf Affiliate)) return false;
		if ($this->account_type != "free") {
			if ($payable = Payable::factory($this, $payment)) {
				$payable->save();
			}
		}
	}
	
	public function createAccessToken($code, $return_url) 
	{
	
		if (!$this->network_app_facebook) throw new Exception('Member must have Facebook app set first');
	
		$network_app_facebook = Zend_Registry::get('networkAppFacebook');
		
		//make 100% sure that redirect_uri is identical when making authroize and access_token call!
		$token_url = "https://graph.facebook.com/oauth/access_token?client_id=" . $this->network_app_facebook->consumer_id . 
						"&redirect_uri=" . urlencode(APP_URL . $return_url) . 
						"&client_secret=" . $this->network_app_facebook->consumer_secret . 
						"&code=" . $code;

		// create a new cURL resource
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $token_url);
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// should curl return or print the data? true = return, false = print
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// timeout in seconds
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// grab URL and pass it to the browser
		$response = curl_exec($ch);

		// close cURL resource, and free up system resources
		curl_close($ch);

		// $response = file_get_contents($token_url);

		$tokenData = array();

		parse_str($response, $tokenData);

		if (!isset($tokenData['access_token'])) {
			throw new Exception('Unable to retrieve access token. Facebook responded with: ' . $response);
		}
		
		$this->setAccessTokenData($tokenData);
		
		return true;
	}

	// Extend a Facebook Token
	// @returns String $extended_access_token
	public function createExtendedAccessToken($token = null) {
	
		if (!$token && !$this->access_token) throw new Exception('No access token provided');
	
		if (!$token) $token = $this->access_token;
	
		$tokenData = array();
		$encoded = file_get_contents("https://graph.facebook.com/oauth/access_token?client_id=" . $this->network_app_facebook->consumer_id . "&client_secret=" . $this->network_app_facebook->consumer_secret . "&grant_type=fb_exchange_token&fb_exchange_token=" . $token);
		parse_str($encoded, $tokenData);
		
		if (!isset($tokenData['access_token'])) {
			throw new Exception('Unable to retrieve extended access token. Facebook responded with: ' . $encoded);
		}
		
		$this->setAccessTokenData($tokenData);
		$this->extendFanpageTokens();
	    return true;    
	}

	public function extendFanpageTokens() {
	
		$facebook = $this->network_app_facebook->getFacebookAPI();
		$accounts = $facebook->api( '/me/accounts', array('access_token' => $this->access_token) );

		foreach ($accounts['data'] as $account) {
			if ($account['category'] != "Application" && $fp = FanPage::find_by_fanpage_id($account['id'])) {
				$fp->access_token = $account['access_token'];
				$fp->save();
			}
		}
		
	}

	public function updateFacebookFriendCount() {
		
		// don't just count the /friends connection from the Graph API here, not all users will show in that list due to privacy settings

		$facebook = $this->network_app_facebook->getFacebookAPI();

		$data = $facebook->api(
			array(
				'method' => 'fql.query',
				'access_token' => $this->network_app_facebook->getFacebookAppAccessToken(),
				'query' => 'SELECT friend_count FROM user WHERE uid = ' . $this->uid
			)
		);
		
		$fb_friends = null;
		if (is_array($data) && isset($data[0])) $fb_friends	= $data[0]['friend_count'];

		if ($fb_friends > 0)
		{
			$this->facebook_friend_count	=	$fb_friends;
		}
		
	}
	
	public function setAccessTokenData($tokenData) {
	
		$this->access_token  = $tokenData['access_token'];
		
		if (isset($tokenData['expires'])) {	
			$this->token_expires = new DateTime();
			$this->token_expires->setTimestamp($tokenData['expires'] + strtotime('now') - 5);
		}
		
	}
	
	public function getLastPublishedPost($channel, $page_id=0) {
		$member = $this;
		$add_query = "AND message like 'FACEBOOK %'";
		if ($channel == 'FACEBOOK_FANPAGE') {
			$channel = "FACEBOOK";
			$add_query = "AND message like 'FACEBOOK_FANPAGE%' AND fb_post_id like '".$page_id."_%'";
		}
		$query = "select * from post where 
		type='".$channel."' 
		AND member_id=".intval($member->id)." 
		AND result=1 
		AND ( 
			( type='TWITTER' AND twitter_post_id IS NOT NULL AND twitter_post_id != 'EMPTY')  
			OR (type='FACEBOOK' AND fb_post_id IS NOT NULL AND fb_post_id != 'EMPTY' ".$add_query.") 
			OR (type='LINKEDIN') 
			)
		                                   	order by created_at DESC LIMIT 1";
		$posts = Post::find_by_sql($query);
			
		$post = current($posts);
		return $post;
	}

	public function creditcardexpiringnextmonthNotify($errors = null)
	{
		//Get the last payment data
		$sql = "select * from payment where member_id=".intval($this->id)." order by id desc limit 1";
		$payment = Payment::find_by_sql($sql);
		$payment = (is_array($payment)) ? current($payment) : null;
		// Validate expiring date with
		$nextMonth = str_pad(date("mY",strtotime(date("Y-m-d")." +1month")), 6, "0", STR_PAD_LEFT);
		if ( $payment == false || $payment->cc_expire_date != $nextMonth ) {
			return false; // Nothing to be done
		}
		if (isset($this->email) && !empty($this->email))
		{
			$params = array(
				'member' => $this,
			);
			$fromEmail	=   defined('HELP_EMAIL') ? HELP_EMAIL : 'noreply@'.APPLICATION_ENV.'.com';
			$fromName	=   defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
			$view = Zend_Registry::get('view');
			$subject = 'Your credit card will expire soon!';
		 	$htmlBody = $view->partial( 'email/creditcardexpiring.html.php', $params);
			$member_email = !empty($this->email_override) ? $this->email_override : $this->email;
			$mandrill = new Mandrill(array('CC_EXPIRING'));
			$mandrill->setFrom($fromName, $fromEmail);
			$mandrill->addTo($this->first_name . " " . $this->last_name, $member_email);
			if (  $member_email != $this->email )
			{
				$mandrill->addTo("", $member_email);
			}
			$mandrill->send($subject, $htmlBody);
			//Keep track of the Email in our local LOG
			$mail = new ZendMailLogger('CC_EXPIRING', $this->id);
			$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
			$mail->setFrom($fromEmail, $fromName);
			$mail->addTo($member_email, $member_email);
			$mail->setSubject($subject);
			$mail->log();
			return true;
		}
	}

	public function is14DayFreeTrial() 
	{
		$last_payment   =	Payment::find(array('conditions' => 'member_id='.$this->id.' and payment_type="clientfinder"', 'limit' => 1, 'order'=>'created_at desc, id desc'));
	
		return is_null($last_payment) && $this->payment_status == "signup";
	}

	
	/**
	 * Get members price monthly
	 * 
	 * @return float
	 */
	public function getPriceMonthly()
	{
		$result = round($this->price / MemberType::getBillingCycleMonths($this->billing_cycle), 2);

		return $result;
	}
	
	public function mapFranchise($promo_code = null, $notify_failure = true)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$continue = true;
		
		if ($this->franchise) {
			Logger::log('Member (' . $this->id . ') franchise previously assigned (' . $this->franchise_id . ')', Logger::LOG_TYPE_DEBUG);
			$continue = false;
		}
		
		if ($continue) {
			Logger::log('Member (' . $this->id . ') franchise not yet assigned', Logger::LOG_TYPE_DEBUG);
			Logger::startSection(Logger::LOG_TYPE_DEBUG);
			
			$unknown = FranchiseUnknownMember::find_by_member_id($this->id);
			if ($unknown) {
				Logger::log('Member (' . $this->id . ') has previously been flagged as broker unknown', Logger::LOG_TYPE_DEBUG);
			}
			
			$franchise = null;
			
			if (empty($franchise) && $promo_code) {
				Logger::log('Looking for franchise by promo code (' . $promo_code . ')', Logger::LOG_TYPE_DEBUG);
				$promo = Promo::find_by_promo_code($promo_code);
				if ($promo && $promo->franchise) {
					$franchise = $promo->franchise;
				}
			}
			
			$search_string = preg_replace('/[^a-zA-Z0-9]+/', '', $this->brokerage); // Strip none alphanumeric and spaces
			$search_string = trim($search_string);

			if (empty($franchise)) {
				Logger::log('Looking for franchise by name (' . $this->brokerage . ')', Logger::LOG_TYPE_DEBUG);
				// Check for a franchise name that is a substring of the member brokerage string
				// Spaces are stripped from both from the input (member.brokerage) and from the field we are searching on
				$franchises = Franchise::all(array(
					'conditions' => array('? LIKE CONCAT("%", REPLACE(name, " ", ""), "%")', $search_string),
					'limit' => 2
				));
				// Check if we have a match, but only 1 match
				if (!empty($franchises) && !isset($franchises[1])) {
					$franchise = $franchises[0];
				}
			}
			
			if (empty($franchise)) {
				Logger::log('Looking for franchise by alias (' . $this->brokerage . ')', Logger::LOG_TYPE_DEBUG);
				// Check for a franchise alias name that is a substring of the member brokerage string
				// Spaces are stripped from both from the input (member.brokerage) and from the field we are searching on
				$franchise_aliases = FranchiseAlias::all(array(
					'conditions' => array('? LIKE CONCAT("%", REPLACE(name, " ", ""), "%")', $search_string),
					'limit' => 2
				));
				// Check if we have a match, but only 1 match
				if (!empty($franchise_aliases) && !isset($franchise_aliases[1])) {
					$franchise = $franchise_aliases[0]->franchise;
				}
			}
			
			if (empty($franchise) && $this->email) {
				list($email_user, $email_domain) = explode('@', $this->email);
				
				if (!empty($email_domain)) {
					$email_domain = '%@' . strtolower($email_domain);
					Logger::log('Looking for franchise by member email (' . $this->email . ')', Logger::LOG_TYPE_DEBUG);
					$franchise_emails = FranchiseEmail::all(array(
						'conditions' => array("email LIKE ?", $email_domain),
						'limit' => 2
					));
					// Check if we have a match, but only 1 match
					if (!empty($franchise_emails) && !isset($franchise_emails[1])) {
						$franchise =  $franchise_emails[0]->franchise;
					}
				}
			}

			if (!empty($franchise)) {
				$this->franchise_id = $franchise->id;
				$this->save();

				Logger::log('Found: ' . $franchise->name . ' (' .  $franchise->id . ')', Logger::LOG_TYPE_DEBUG, true);
				
				$unknown = FranchiseUnknownMember::find_by_member_id($this->id);
				if ($unknown) {
					$unknown->delete();
				}
			} else {
				Logger::log('Unable to find franchise', Logger::LOG_TYPE_DEBUG);
				
				if (!$unknown) {
					$unknown = new FranchiseUnknownMember();
					$unknown->member_id = $this->id;
					$unknown->save();
					
					if ($notify_failure) {
						Logger::log('Sending email notification to admin', Logger::LOG_TYPE_DEBUG);
						$emailer = Zend_Registry::get('alertEmailer');
						$member_page_url = APP_URL . '/admin/member/' . $this->id . '#update';
						$subject = 'Unable to map franchise (' . $this->brokerage . ') for member, ' . $this->first_name . ' ' . $this->last_name . ' (' . $this->id . ')';
						$msg = 'Unable to map franchise (' . $this->brokerage . ') for member, ' . 
								'<a href="' . $member_page_url . '">' . $this->first_name . ' ' . $this->last_name . '</a> ' . 
								'(' . $this->id . ')' . "\n\n";
						$emailer->send($subject, $msg, 'member-unknown-franchise');
					}
				}
			}
			
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
		} 
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	public static function getActive()
	{
		if (isset($_SESSION['member']) && $_SESSION['member'] instanceOf Member) return $_SESSION['member'];
	}
	
	public static function setActive(Member $member)
	{
		$_SESSION['member'] = $member;
	}
	
	public static function isLoggedIn() 
	{
		if (static::getActive()) {
			$sso = new Zend_Session_Namespace('sso');
			if ((static::getActive()->saml_login instanceOf SAMLLogin) && !$sso->loginid) {
				return false;
			}
		}

		return (bool) static::getActive();
	}
}
