<?php

class FacebookStatByDateMember extends ActiveRecord\Model
{
	static $table = 'facebook_stat_by_date_member';
	
	public function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$start_date = date('Y-m-d', $start_time);
		$end_date = date('Y-m-d', $end_time);
		
		Logger::log(
			'Compiling facebook stat by date and member between ' . $start_date . ' and ' . $end_date, 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT
				psfbd.`date`,
				p.member_id,
				COALESCE(SUM(psfbd.likes), 0) AS likes,
				COALESCE(SUM(psfbd.comments), 0) AS comments,
				COALESCE(SUM(psfbd.shares), 0) AS shares
			FROM post_stat_facebook_by_date psfbd 
			INNER JOIN post p ON psfbd.post_id = p.id AND p.result = 1
			WHERE
				p.member_id > 0 AND
				psfbd.date >= " . $db_con->escape($start_date) . " AND
				psfbd.date <= " . $db_con->escape($end_date) . "
			GROUP BY psfbd.`date`, p.member_id
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}