<?php
class City extends ActiveRecord\Model
{
	static $table = 'city';
	//static $pk    = 'track_id';


	static $has_many = array(
		array('members', 'class' => 'Member', 'foreign_key' => 'city_id'),
		array('groups', 'class' => 'GroupCity', 'foreign_key' => 'city_id')					
	);

}
?>