<?php
class WeeklySales extends ActiveRecord\Model
{
	static $table = 'weekly_sales';
	
	static $belongs_to = array(
		array('owner', 'class' => 'Member', 'foreign_key' => 'member_id')
	);
}