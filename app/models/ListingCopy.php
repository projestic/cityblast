<?php
class ListingCopy extends Listing
{
	static $table      = 'listing';
	static $connection = CONTENT_COPY_ENV;

	protected $listing_copy_image_path;
	protected $from_listing;

	public static function copy(Listing $listing, $listing_copy_member_id, $listing_copy_image_path, $listing_copy_app_url) {
		$listing_copy = new ListingCopy();
		$listing_copy->published = 0;
		$listing_copy->member_id = $listing_copy_member_id;
		$listing_copy->created_at = new DateTime;
		$listing_copy->title = $listing->title;
		$listing_copy->description = $listing->description;
		$listing_copy->message = $listing->message;
		$listing_copy->raw_message = $listing->raw_message;
		$listing_copy->street = $listing->street;
		$listing_copy->blast_type = $listing->blast_type;
		$listing_copy->status = $listing->status;
		$listing_copy->expiry_date = $listing->expiry_date;
		$listing_copy->big_image = $listing->big_image;
		$listing_copy->email_passthru = $listing->email_passthru;
		$listing_copy->content_link = str_replace(APP_URL, $listing_copy_app_url, $listing->content_link);
		$listing_copy->image = $listing->image;
		$listing_copy->thumbnail = $listing->thumbnail;
		$listing_copy->save();
		
		// Cant just use $listing->descriptions as its cached
		$descriptions = ListingDescription::all(array('conditions' => array('listing_id = ?', $listing->id)));
		if ($descriptions) {
			foreach ($descriptions as $desc) {
				$desc_copy = new ListingDescriptionCopy();
				$desc_copy->listing_id = $listing_copy->id;
				$desc_copy->description = $desc->description;
				$desc_copy->save();
			}
		}
		
		$url_mapping = UrlMapping::find_by_listing_id($listing->id);
		if ($url_mapping) {
			$url_mapping_copy = new UrlMappingCopy();
			$url_mapping_copy->listing_id = $listing_copy->id;
			$url_mapping_copy->url = $url_mapping->url;
			$url_mapping_copy->save();
		}
		
		// Cant just use $listing->images as its cached
		$images = ListingImage::all(array('conditions' => array('listing_id = ?', $listing->id)));
		if ($images) {
			foreach ($images as $img) {
				$image_copy = new ListingImageCopy();
				$image_copy->listing_id = $listing_copy->id;
				$image_copy->image = static::convertImagePathListing($img->image, $listing_copy->id);
				$image_copy->save();
			}
		}

		if ($listing->image || !empty($images)) {
			$listing_copy->copyImages($listing, $listing_copy_image_path);
		}
		
		$listing_copy->image = $listing_copy->convertImagePath($listing->image);
		$listing_copy->thumbnail = $listing_copy->convertImagePath($listing->thumbnail);
		$listing_copy->save();
		
		return $listing_copy;
	}
	
	public function copyImages(Listing $listing, $listing_copy_image_path) {
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$source_dir = CDN_ORIGIN_STORAGE . dirname($listing->image);
		$dest_dir = dirname($listing_copy_image_path . $this->convertImagePath($listing->image));
		Logger::log('Source dir: ' . $source_dir, Logger::LOG_TYPE_DEBUG);
		Logger::log('Dest dir: ' . $dest_dir, Logger::LOG_TYPE_DEBUG);
		
		if (!file_exists($dest_dir)) mkdir($dest_dir, 0775, true);
		
		if ($handle = opendir($source_dir)) {
			while (false !== ($filename = readdir($handle))) {
				if ($filename == '.' || $filename == '..') {
					continue;
				}
				
				$source_file = $source_dir . '/' . $filename;
				$dest_file = $dest_dir . '/' . $filename;
				Logger::log('Source file: ' . $source_file, Logger::LOG_TYPE_DEBUG);
				Logger::startSection(Logger::LOG_TYPE_DEBUG);
				Logger::log('Dest file: ' . $dest_file, Logger::LOG_TYPE_DEBUG);
				
				if (!is_file($source_file)) {
					Logger::log('No such file: ' . $source_file, Logger::LOG_TYPE_DEBUG);
					Logger::log('Skipping', Logger::LOG_TYPE_DEBUG);
					Logger::endSection(Logger::LOG_TYPE_DEBUG);
					continue;
				}
				
				copy($source_file, $dest_file);
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
			}
			closedir($handle);
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}

	protected function convertImagePath($image_path) {
		return static::convertImagePathListing($image_path, $this->id);
	}
	
	protected static function convertImagePathListing($image_path, $listing_id)
	{
		$image_parts = explode('/', $image_path);
		$file = array_pop($image_parts);
		array_pop($image_parts);
		return implode('/', $image_parts) . '/' . $listing_id . '/' . $file;
	}
}