<?php
class FranchiseUnknownMember extends ActiveRecord\Model
{
	static $table = 'franchise_unknown_member';
	
	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);
}