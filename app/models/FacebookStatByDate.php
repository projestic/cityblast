<?php

class FacebookStatByDate extends ActiveRecord\Model
{
	static $table = 'facebook_stat_by_date';
	
	public function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$start_date = date('Y-m-d', $start_time);
		$end_date = date('Y-m-d', $end_time);
		
		Logger::log(
			'Compiling facebook stat by date between ' . $start_date . ' and ' . $end_date, 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT 
				psfbd.date, 
				SUM(psfbd.likes),
				SUM(psfbd.comments),
				SUM(psfbd.shares)
			FROM `post_stat_facebook_by_date` AS psfbd
			WHERE psfbd.date >= " . $db_con->escape($start_date) . "  
			AND psfbd.date <= " . $db_con->escape($end_date) . " 
			GROUP BY psfbd.date 
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}