<?php
class Listing extends NondestructiveDelete
{
	const TYPE_LISTING 	= 'LISTING';
	const TYPE_CONTENT 	= 'CONTENT';
	const TYPE_IDX 		= 'IDX';
	
	static $table = 'listing';
	
	static $before_save   = array('beforeSave');

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('list_city', 'class' => 'City', 'foreign_key' => 'city_id'),
		array('reblast', 'class' => 'Listing', 'foreign_key' => 'reblast_id'),
	);


	static $has_many = array(
		array('comments', 'class' => 'Comment', 'foreign_key' => 'listing_id'),
		array('posts', 'class' => 'Post', 'foreign_key' => 'listing_id'),
		array('bookings', 'class' => 'Booking', 'foreign_key' => 'listing_id'),
		array('descriptions', 'class' => 'ListingDescription', 'foreign_key' => 'listing_id'),
		array('images', 'class' => 'ListingImage', 'foreign_key' => 'listing_id'),
		array('publish_queue', 'class' => 'PublishingQueue', 'foreign_key' => 'listing_id'),
		array('tag_cloud', 'class' => 'TagCloud', 'foreign_key' => 'listing_id')		
	);

	static $has_one	=   array(
		array('hoper', 'class' => 'Hoper', 'foreign_key' => 'listing_id'),
		array('url_mapping', 'class' => 'UrlMapping', 'foreign_key' => 'listing_id'),
		array('delete_action', 'class' => 'DeleteAction', 'foreign_key' => 'listing_id', 'order' => 'created_at desc'),
		array('reblast_from', 'class' => 'Listing', 'foreign_key' => 'reblast_id'),
	);

	public function __construct($attributes = array(), $guard_attributes = true, $instantiating_via_find = false, $new_record = true) 
	{
		if (!isset($attributes['pre_approval'])) 
		{
			$attributes['pre_approval'] = 0;
		}
		parent::__construct($attributes, $guard_attributes, $instantiating_via_find, $new_record);
	}
	
	public function beforeSave() 
	{
		if (
			!in_array($this->blast_type, array(
				Listing::TYPE_CONTENT, 
				Listing::TYPE_IDX, 
				Listing::TYPE_LISTING
			))
		) {
			// Debuging code to find out what is saving listing records without a valid blast_type
			$alert_emailer = Zend_Registry::get('alertEmailer');
			ob_start();
				debug_print_backtrace();
			$trace = ob_get_contents();
			ob_end_clean();
			$message = 'Listing id ' . $this->id . ' saved without a valid blast type ("'. $this->blast_type.'")' . "\n\n" . $trace;
			$alert_emailer->send(
				'Listing id ' . $this->id . ' saved without a valid blast type',
				$message,
				'listing-no-blast-type',
				true
			);
		}
	}

	/**
	 * Does this Object have a Listing Blast Type?
	 * 
	 * @return boolean
	 */
	public function isListing() {
		return $this->blast_type === static::TYPE_LISTING;
	}
	
	/**
	 * Does this Object have a Content Blast Type?
	 * @return boolean
	 */
	public function isContent() {
		return $this->blast_type === static::TYPE_CONTENT;
	}
	
	/**
	 * Does this Object have an IDX Blast Type?
	 * @return boolean
	 */
	public function isIdx() {
		return $this->blast_type === static::TYPE_IDX;
	}


	public function getSizedImage($width, $height, $useCDN = true) 
	{
		if ($this->reblast_from)
		{ 
			return $this->reblast_from->getSizedImage($width, $height);
		}
		
		if (!$this->image) 
		{
			if ($width == 276 && $height == 182) 
			{
				return '/images/no-listing-image.jpg';
			} else {
				return '/images/blank.gif';			
			}
		}

		return $this->_getSizedImagePath($this->image, $width, $height, $useCDN);
	}

	/*
	public function getSizedThumbnail($width = 101, $height = 99, $useCDN = true) 
	{
		if ($this->reblast_from) return $this->reblast_from->getSizedThumbnail($width, $height);
		if (!$this->thumbnail) 
		{
			return $this->getSizedImage($width, $height);
		}
		return $this->_getSizedImagePath($this->thumbnail, $width, $height, $useCDN);
	}
	*/
	
	protected function _getSizedImagePath($path, $width, $height, $useCDN = true) 
	{
		$pathinfo = pathinfo($path);
		$cdn = ($useCDN) ? CDN_URL : '';
		
		$ext = !empty($pathinfo['extension']) ? '.' . $pathinfo['extension'] : '';

		return $cdn . "{$pathinfo['dirname']}/{$pathinfo['filename']}_{$width}x{$height}{$ext}";
	}
	
	public function getOriginal() 
	{
		if ($this->reblast_from) return $this->reblast_from->getOriginal();
		return $this;
	}

	public function ingestImage($original, $deleteOriginal = true) 
	{
		$this->ingestImageFile($original, 'image', $deleteOriginal);
	}
	
	public function ingestThumbnail($original, $deleteOriginal = true) 
	{
		$this->ingestImageFile($original, 'thumbnail', $deleteOriginal);
	}
	
	public function fixImageFilePaths()
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$properties = array('image', 'thumbnail');
		
		foreach ($properties as $property) {
			Logger::log('Checking ' . $property . ' of listing id: ' . $this->id, Logger::LOG_TYPE_DEBUG);
			Logger::startSection(Logger::LOG_TYPE_DEBUG);
			$src_path = $this->$property;
			if ($src_path && !preg_match('@^/images/listings/[0-9]{4}/[0-9]{2}/[0-9]+/.+@i', $src_path)) {
				Logger::log('Path appears invalid: ' . $src_path, Logger::LOG_TYPE_DEBUG);
				$src_path_full = CDN_ORIGIN_STORAGE . str_replace('//', '/', $src_path);
				if (!is_file($src_path_full)) {
					Logger::log('Path does not exist (' . $src_path_full . '). No further action can be taken. ', Logger::LOG_TYPE_DEBUG);
				} else {
					// Image doest appear to have a valid path
					$src_filename = basename($src_path);
					$dest_dir_path = '/images/listings/' . $this->created_at->format('Y') . "/" . $this->created_at->format('m') . "/" . $this->id;
					$dest_dir_path_full = CDN_ORIGIN_STORAGE . $dest_dir_path;
					$dest_path  = $dest_dir_path . '/' . $src_filename;
					$dest_path_full = CDN_ORIGIN_STORAGE . $dest_path;
					
					// Esnure destination directory exists
					if (!file_exists($dest_dir_path_full)) mkdir($dest_dir_path_full, 0777, true);
					
					// Copy image
					Logger::log('Copying image', Logger::LOG_TYPE_DEBUG);
					Logger::log('From: ' . $src_path_full, Logger::LOG_TYPE_DEBUG);
					Logger::log('To: ' . $dest_path_full, Logger::LOG_TYPE_DEBUG);
					$copy_result = copy($src_path_full, $dest_path_full);
					if ($copy_result) {
						Logger::log('Image copy complete', Logger::LOG_TYPE_DEBUG);
						Logger::log('Generating image sizes', Logger::LOG_TYPE_DEBUG);
						$resizer = new Image();
						$resizer->resizeListingImage($dest_path_full, true);
						// Update all listing paths with the same src listing path
						// There might be more thna one as reblasts use the same image path as the original listing
						Logger::log('Update listings', Logger::LOG_TYPE_DEBUG);
						$db_con = static::connection();
						$sql = "
							UPDATE `listing` 
							SET 
								`" . $property . "`  = " . $db_con->escape($dest_path) . " 
							WHERE
								`" . $property . "` = " . $db_con->escape($src_path) . " 
						";
						$res = $db_con->query($sql);
						Logger::log('Listing updated', Logger::LOG_TYPE_DEBUG);
					} else {
						Logger::log('Image copy failed!!', Logger::LOG_TYPE_DEBUG);
					}
				}
			}
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
		}
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	protected function ingestImageFile($original, $property, $deleteOriginal = true) 
	{
	
		$original = $this->imageToLocal($original);

		if (!pathinfo($original, PATHINFO_EXTENSION)) {
			$ext = $this->autoImageExtension($original);
			if ($deleteOriginal) {
				rename($original, $original . '.' . $ext);
			} else {
				copy($original, $original . '.' . $ext);
				// so we clean up our new and improved file with extension
				$deleteOriginal = true;
			}
			$original .= '.' . $ext;
		}

		$imageDest  = '/images/listings/' . date('Y') . "/" . date('m') . "/" . $this->id . '/' . basename($original);
		$imageDestPath = CDN_ORIGIN_STORAGE . $imageDest;

		$resizer	= new Image();
		$images = $resizer->resizeListingImage($original);
		$images[] = $original;
		
		if ( !file_exists(dirname($imageDestPath)) ) mkdir(dirname($imageDestPath), 0777, true);
		
		foreach ($images as $image) 
		{
			copy($image, dirname($imageDestPath) . '/' . basename($image));
		}

		foreach ($images as $image) 
		{
			if ($image != $original || $deleteOriginal == true) unlink($image);
		}

		$this->$property = $imageDest;
		$this->save();
	
	}
	
	protected function imageToLocal($photo) {
		if (stristr($photo, '://')) {
			$tmp 	= '/tmp/' . md5(microtime());
			$tmpPhoto 	= $tmp . '-' . basename($photo);
			copy($photo, $tmpPhoto);
			return $tmpPhoto;	
		}
		return $photo;
	}
	
	protected function autoImageExtension($image) {
		$info = getimagesize($image);
		switch ($info[2]) {
			case IMAGETYPE_GIF :
				return 'gif';
			case IMAGETYPE_JPEG :
			case IMAGETYPE_JPEG2000 :
				return 'jpg';
			case IMAGETYPE_PNG :
				return 'png';
		}
		return false;
	}
	
	public function sendPublishNotification()
	{
		$member = $this->member;
		if ($this->blast_type == static::TYPE_LISTING && $member) {
			$params =	array(
				'to_name'		=> $member->last_name . ',' . $member->first_name,
				'message'		=> $this->raw_message,
				'listing_id'	=> $this->id,
				'listing'		=> $this,
				'listing_url'	=> APP_URL . '/member/blast/ '. $this->id,
				'member' 		=> $member,
			);
			
			$view = Zend_Registry::get('view');
			$htmlBody = $view->partial('email/postpublish.html.php', $params);
			
			$fromEmail = defined('APP_EMAIL') ? APP_EMAIL : 'noreply@cityblast.com';
			$fromName = defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : 'city blast';
			$member_email = !empty($member->email_override) ? $member->email_override : $member->email;
			
			$subject = 'Your ' . COMPANY_NAME . ' Listing Has Been Approved!';
			
			$mandrill = new Mandrill(array('LISTING_PUBLISH'));
			$mandrill->setFrom($fromName, $fromEmail);
			$mandrill->addTo($member_email, $member_email);
			if ($member_email != $member->email) {
				$mandrill->addTo('', $member->email);
			}
			$mandrill->send($subject, $htmlBody);

			$mail = new ZendMailLogger('LISTING_PUBLISH', $this->id);
			$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
			$mail->setFrom($fromEmail, $fromName);
			$mail->addTo($member_email, $member_email);
			$mail->setSubject($subject);
			$mail->log();
		}
	}
    
    public function sendDeleteNotification($comment)
    {
        $member = $this->member;
		if ($this->blast_type == static::TYPE_LISTING && $member) {
            $params =	array(
				'to_name' => $member->last_name . ',' . $member->first_name,
				'message' => $this->raw_message,
				'listing_id' => $this->id,
				'listing' => $this,
				'listing_url' => APP_URL . '/member/blast/ '. $this->id,
				'member' => $member,
                'comment' => $comment
			);
			
			$view = Zend_Registry::get('view');
			$htmlBody = $view->partial('email/postdelete.html.php', $params);
			
			$fromEmail = defined('APP_EMAIL') ? APP_EMAIL : 'noreply@cityblast.com';
			$fromName = defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : 'city blast';
			$member_email = !empty($member->email_override) ? $member->email_override : $this->email;
			
			$subject = 'Oh Dear, something went wrong with your ' . COMPANY_NAME . ' listing!';
			
			$mandrill = new Mandrill(array('LISTING_DELETE'));
			$mandrill->setFrom($fromName, $fromEmail);
			$mandrill->addTo($member_email, $member_email);
			if ($member_email != $member->email) {
				$mandrill->addTo('', $member->email);
			}
			$mandrill->send($subject, $htmlBody);

			$mail = new ZendMailLogger('LISTING_DELETE', $this->id);
			$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
			$mail->setFrom($fromEmail, $fromName);
			$mail->addTo($member_email, $member_email);
			$mail->setSubject($subject);
			$mail->log();
		}
    }

	function setLatitude()
	{			
		// is cURL installed yet?
		if (!function_exists('curl_init')){
		   	die('Sorry cURL is not installed!');
		}
		
		// OK cool - then let's create a new cURL resource handle
		$ch = curl_init();
		
		$map_pull_url = 'http://maps.googleapis.com/maps/api/geocode/json?address=';
		
		
		
		//Construct the Address
		$address = $this->street.",".$this->city." ".$this->postal_code;		
		//echo $map_pull_url . urlencode($address) . "&sensor=false";

	 
		// Set URL to download
		curl_setopt($ch, CURLOPT_URL, $map_pull_url . urlencode($address) . "&sensor=false"	);
		
		
		// User agent
		curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
		
		// Include header in result? (0 = yes, 1 = no)
		curl_setopt($ch, CURLOPT_HEADER, 0);
		
		// Should cURL return or print out the data? (true = return, false = print)
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		// Timeout in seconds
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		
		// Download the given URL, and return output
		$output = curl_exec($ch);



		$result = json_decode($output);
		

	
		if($result->status == "OK")
		{
			//Set the LAT and LONG
			$this->latitude = $result->results[0]->geometry->location->lat;
			$this->longitude = $result->results[0]->geometry->location->lng;
			//$offerLocation->address_lookup_status = $result->status;
			
			$this->save();
		}
		//else
		//{
			//Otherwise something went wrong, just set the STATUS
			//$offerLocation->address_lookup_status = $result->status;
		//}

	 
		// Close the cURL resource, and free system resources
		curl_close($ch);
		
		return $output;		
	}


	/**
	 *
	 * @param array $params		associative array of search form values
	 * @param integer $per_page		integer
	 * @param integer $current_page		integer
	 * @return Zend_Paginator
	 */
	static function search(Array $params, $per_page = 10, $current_page = 1)
	{
		$joins = array();

		if ($params)
		{
			$conditions	= array();
			$conn = static::connection();

			if (!empty($params['id']))
			{
				$conditions['id'] = "`id` = '". $params['id'] ."'";
			}
			if (!empty($params['member_id']))
			{
				$conditions['member_id'] = "`member_id` = '". $params['member_id'] ."'";
			}
			if (!empty($params['message']))
			{
				$conditions['message'] = "`message` like '%". $params['message'] ."%'";
			}
			if (!empty($params['city_id']))
			{
				$conditions['city'] = "`city_id` = '". $params['city_id'] ."'";
			}

			if (!empty($params['price_range_low']))
			{
				$conditions['priceL'] = "`price` > ". (intval($params['price_range_low']) - 1);
			}

			if (!empty($params['price_range_high']))
			{
				$conditions['priceH'] = "`price` < ". (intval($params['price_range_high']) + 1);
			}

			if (!empty($params['property_type_id']))
			{
				$conditions['type'] = "`property_type_id` = ". $params['property_type_id'];
			}

			if (!empty($params['number_of_bedrooms']))
			{
				$conditions['bedroom'] = "`number_of_bedrooms` = ". $params['number_of_bedrooms'];
			}

			if (!empty($params['number_of_bathrooms']))
			{
				$conditions['bathroom'] = "`number_of_bathrooms` = ". $params['number_of_bathrooms'];
			}

			if (!empty($params['number_of_parking']))
			{
				$conditions['parking'] = "`number_of_parking` = ". $params['number_of_parking'];
			}

			if (!empty($params['street']))
			{
				$conditions['street'] = "`street` LIKE '%". $params['street'] ."%'";
			}

			if (!empty($params['deleted']))
			{
				$conditions['deleted'] = "status = 'deleted'";
			}
			
			if (!empty($params['content_link']))
			{
				$conditions['content_link'] = "(content_link LIKE '%". $params['content_link'] ."%' OR url_mapping.url LIKE '%". $params['content_link'] ."%')";
				$joins[] = "LEFT JOIN url_mapping ON url_mapping.listing_id = listing.id";
			}

			if (!empty($params['postal']))
			{
				$conditions['postal'] = "`postal_code` = '". $params['postal'] ."'";
			}

			if (!empty($params['mls_number']))
			{
				$conditions['mls'] = "`mls` = '". $params['mls_number'] ."'";
			}

			if (!empty($params['listing_type']) && in_array($params['listing_type'], array('BLAST', 'MANUAL', 'SMLS')))
			{
				$conditions['listing_type']  = "`blast_type` = '". $params['listing_type'] ."'";
			}

			if (!empty($params['big_image']))
			{
				$conditions['big_image'] = "`big_image` < ". (intval($params['big_image']) );
			}
			
			if (!empty($params['expiry_date']) && 
				!empty($params['expiry_date_comparator'])
			) {
				$comparator = in_array($params['expiry_date_comparator'], array('=', '<=', '>=', '<', '>')) ? $params['expiry_date_comparator'] : '=';
				$conditions['expiry_date'] = "listing.expiry_date " . $comparator . " ". $conn->escape($params['expiry_date']);
			}

			if (count($conditions))
			{
				$conditions	= implode(" AND ", $conditions);
			}
			else
			{
				$conditions	= ' 1 ';
			}
			
			//var_dump($conditions);
			//exit;

			//$conditions	.= " AND image IS NOT NULL";

			$options	= array(
				'conditions'	=> $conditions,
				'joins' 		=> $joins,
				'order'			=> 'id DESC',
			);


			if ($per_page && $current_page)
			{
				$paginator = new Zend_Paginator(new ARPaginator('Listing', $options));
				$paginator->setCurrentPageNumber($current_page);
				$paginator->setItemCountPerPage($per_page);

				return $paginator;
			}
			else
			{
				$result	= self::all($options);
				return $result;
			}
		}

		return array();

	}

	function filter_content_link($click_source)
	{
		if (!empty($this->content_link)) {
			if (FALSE === strstr($this->content_link, '/n/i'))
			{
				$this->content_link	= APP_URL ."/n/i/";
				if(isset($click_source)) $this->content_link .=  $click_source."/";
				$this->content_link .=  $this->id;
				
				$this->save();
			}
			else
			{
				$content_link_parts	= explode('/', $this->content_link);	

				if(!is_numeric(end($content_link_parts)))
				{
					//We've somehow missed the listing_id
					$this->content_link	= APP_URL ."/n/i/";
					if(isset($click_source)) $this->content_link .=  $click_source."/";
					$this->content_link .=  $this->id;
					
					$this->save();
				}					
			}
		}

	}

	function reblast_from() 
	{
		$sql = "SELECT * FROM listing where status != 'deleted' AND reblast_id=".intval($this->id);
		$reblast_from = Listing::find_by_sql($sql);
		
		return count($reblast_from)==0 ? null : $reblast_from[0]->id;
	}

	/**
	 * Method to get the facebook payload data
	 * @input Member $member
	 * @return array $publish_array
	 * $publish_array = array(
	 * 	"message" 	=> "",
	 *  "picture" 	=> "",
	 * 	"link"		=> "",
	 * 	"description" => ""
	 * ) 
	 */
	public function getFacebookPayload($member, $click_source) 
	{
		$publish_array 	=	array();

		// FIXME: Get right token if it's for a fan page;

		$publish_array['access_token'] = $member->network_app_facebook->getFacebookAppAccessToken();

		//Randomize the message when possible
		$publish_array['message'] = ListingDescription::getRandomDescription($this);

		//Randomize the message when possible
		if ($img = ListingImage::getRandomImage($this->getOriginal())) {
			
			$publish_array['picture']  = CDN_URL_PUBLISH . $img;
		}

		if($this->blast_type == LISTING::TYPE_CONTENT)
		{
			if ($this->content_link) {
				if ($member->click_tracking) {
					//Make sure the proper jump link format exists
					$this->filter_content_link($click_source);
					$publish_array['link'] = $this->content_link ."/".$member->id;
				} else {
					$publish_array['link'] = $this->url_mapping->url;
				}
			}

			if(isset($this->title) && !empty($this->title))  
				$publish_array['name'] = $this->title;
			elseif(isset($this->url_mapping->url)) 
				$publish_array['name'] = $this->url_mapping->url;
			
			if(isset($this->description) && !empty($this->description))  
				$publish_array['description'] = $this->description;
		}
		else
		{
			$name_array = null;
			
			$publish_array['link'] = APP_URL . "/listing/view/".$this->id."/".$member->id."/".$click_source;

			$desc = null;
			$desc_array = null;
			
			if(isset($this->street) && !empty($this->street)) 
				$desc_array[] = stripslashes($this->street);
			if(isset($this->appartment) && !empty($this->appartment)) 
				$desc_array[] = 'apt ' . stripslashes($this->appartment);
			if(isset($this->city) && !empty($this->city)) 
				$desc_array[] = stripslashes($this->city);
			if(isset($this->postal_code) && !empty($this->postal_code)) 
				$desc_array[] = $this->postal_code;
			
			if(is_array($desc_array) && count($desc_array)) 
				$desc .= implode(", ", $desc_array);
			
			$desc .= ". Click for more photos and pricing";
			
			
			if(!empty($desc)) 
				$publish_array['description'] = $desc;


			//LINK NAME
			if(isset($this->street) && !empty($this->street)) 
				$name_array[] = stripslashes($this->street);
			if(isset($this->appartment) && !empty($this->appartment)) 
				$name_array[] = 'apt ' . stripslashes($this->appartment);
			if(isset($this->city) && !empty($this->city)) 
				$name_array[] = stripslashes($this->city);

			$name = '';
			if(is_array($name_array) && count($name_array)) 
				$name .= implode(", ", $name_array);


			$name .= ". Click for more photos and pricing";
			
			$publish_array['name']  =  $name;
			
		}

		return $publish_array;
	}
	
	public function isPublishable()
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = false;
		$url_mapping = UrlMapping::find_by_listing_id($this->id);
		$result = (!$url_mapping || $url_mapping->isUrlAccessible());
		
		if ($this->big_image) {
			Logger::log('Big image listings temp disabled', Logger::LOG_TYPE_DEBUG);
			$result = false;
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public function retire()
	{
		if($this->publish_queue){
			foreach($this->publish_queue as $publish_queue){
				$publish_queue->delete();
			}
		}
		
		$this->published = $this->published + 1;
		$this->published_on = date("Y-m-d H:i:s");
		$this->save();
	}

	public function delete() {
		if ($this->status == 'deleted') return false;
		foreach ($this->posts as $post) {
			$post->delete();
		}
		if ($this->publish_queue) {
			foreach($this->publish_queue as $publish_queue){
				$publish_queue->delete();
			}
		}
		return parent::delete();
	}

	/**
	 * Fix invalid utf8 at the point of retrieval
	 * 
	 * This is a temporary fix. We need to add the filter to all input and
	 * updated all existing data in the DB. Then we can remove this method.
	 */
	public function &__get($name)
	{
		$result = parent::__get($name);
		$filter_attributes = array('title', 'description', 'message', 'raw_message');
		if (in_array($name, $filter_attributes) && !empty($result)) {
			$result = Blastit_Text::utf8Fix($result);
		}
		return $result;
	}
	
	public function createShowCase()
	{
		$content = new Listing();
		$content->createShowCase_GuessingGame($this);
		
		if($this->price > 1000000)
		{
			$content = new Listing();
			$content->createShowCase_WhatCity($this);
		}		
		
		//More games can be added here in the future...
	}
	
	public function createShowCase_GuessingGame($listing)
	{
		$filename = APP_URL . $listing->image;
		$filesize = getimagesize ( $filename );

		$width  = $filesize[0];
		$height = $filesize[1];
		
		if($width > 400) $this->big_image = 1;
		

	
		$this->title = "How Much Would You Pay?";
		$this->description = "Test your real estate IQ. Take my fun quiz!";
		
		$this->image = $listing->image;
		$this->content_link = APP_URL . "/listing/view/".$listing->id;
		

		$price[] = $listing->price;
		
		//Between 10 and 30% lower
		$price[] = $listing->price - ($listing->price * (	rand(1,3) / 10) );
	
		//Between 10 and 20% higher
		
		$percent = (rand(1,2) / 10) + 1;
		//echo $percent . "<BR><BR>";
		
		$price[] = $listing->price * $percent ;
	
		//Between 30 and 40% higher
		$percent = (rand(3,4) / 10) + 1;
		$price[] = $listing->price *	$percent;
	

		shuffle($price);
		

		$message = "How much would you pay for this ";
		if(isset($listing->number_of_bedrooms) && $listing->number_of_bedrooms > 0) $message .= $listing->number_of_bedrooms ." bedroom, ";
		if(isset($listing->number_of_bathrooms) && $listing->number_of_bathrooms > 0) $message .= $listing->number_of_bathrooms." bathroom ";
		$message .= "home in ".$listing->list_city->name."?"."\n\n";



		$letter = "a";
		foreach($price as $value)
		{
			$message .= $letter . ") " . " $".number_format($value) . "\n";
			$letter++; 	
		}
		

		$message .= "\n" . "Type your answer in the comments section below, then click the link to find out!";
		

		$this->message = $message;
		$this->blast_type = Listing::TYPE_CONTENT;
		$this->pre_approval = 1;
		$this->member_id = 35;

		
		//#TODO - We need to set the adjunct images (if any)
		
		
		//#TODO - We need to set the category tag (should be quizzes and polls)

		

		
		$this->save();	
				
	}


	function createShowCase_WhatCity($listing)
	{
	
		$query = "select * from listing join city on listing.city_id=city.id where listing.price > 1000000 and blast_type='LISTING' and city.country='".$listing->list_city->country."' group by city.id";
		$city_list = City::find_by_sql($query);
		
		//print_r($city_list);
		
		foreach($city_list as $city)
		{
			//echo $city->name . "<BR>";
			if(strtolower($city->name) != "unknown")
			{
				$final_list[] = $city->name;	
			}	
		}	
		
		shuffle($final_list);
	

	
		$final_city_array[] = $listing->list_city->name;
	
		for($i = 0; $i <= 2; $i++)
		{
			$city_name = array_pop($final_list);
			//echo $city_name . "<BR>";
			
			$final_city_array[] = $city_name;
		}		
		
		shuffle($final_city_array);
	
	

		//CREATE THE MESSAGE...
		$message = "This property is selling for over $".number_format($listing->price).". What city is it in?" . "\n\n";
		
		$letter = "a";
		foreach($final_city_array as $name)
		{
			$message .= $letter . ") " . " " . $name . "\n";
			$letter++; 	
		}
		

		$message .= "\n" . "Type your answer in the comments section below, then click the link to find out!";	
		
		
		
		
		$filename = APP_URL . $listing->image;
		$filesize = getimagesize ( $filename );

		$width  = $filesize[0];
		$height = $filesize[1];
		
		if($width > 400) $this->big_image = 1;
		
		
		$this->title = "What City Can You Find Me In?";
		$this->description = "Take this fun quiz and test your real estate IQ!";
		
		$this->image = $listing->image;
		$this->content_link = APP_URL . "/listing/view/".$listing->id;
		
				
		$this->message = $message;
		$this->blast_type = Listing::TYPE_CONTENT;
		$this->pre_approval = 1;
		$this->member_id = 35;
		
	

		//#TODO - We need to set the adjunct images (if any)
		
		
		//#TODO - We need to set the category tag (should be quizzes and polls)

		
		
			
		$this->save();
	}
	
	/**
	 * Test if listing can be reblasted
	 * 
	 * @return bool
	 */
	public function reblastPermitted()
	{
		$result = (
			$this->pre_approval &&
			$this->approval	&&
			($this->created_at->format('Y-m-d') <= date('Y-m-d', strtotime('-7days')))
		);
		
		return $result;
	}

    /**
     * Check is queue exists for current listing.
     *
     * @return bool
     */
    public function isPublishingQueueExists()
    {
        $isRecordExists = false;
        $publishQueueItem = PublishingQueue::find(array('listing_id' => $this->id));
        if (!empty($publishQueueItem)) {
            $isRecordExists = true;
        }
        return $isRecordExists;
    }
}


