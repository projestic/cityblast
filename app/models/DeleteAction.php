<?php
class DeleteAction extends ActiveRecord\Model
{
	static $table = 'delete_actions';

	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id'),
		array('post', 'class' => 'Post', 'foreign_key' => 'post_id'),
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
	);
	
}
?>