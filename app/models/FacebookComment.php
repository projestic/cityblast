<?php

class FacebookComment extends ActiveRecord\Model
{
	static $table = 'facebook_comment';
	
	public static function createFromPostCommentsApiResp($post_id, $comments)
	{
		if (!empty($comments)) {
			foreach ($comments as $comment_data) {
				$id = !empty($comment_data['id']) ? $comment_data['id'] : null;
				if ($id) {
					$comment = FacebookComment::find_by_id($id);
					if (!$comment) {
						$comment = new FacebookComment();
					}
					$comment->id = $id;
					$comment->post_id = $post_id;
					$comment->from_uid = !empty($comment_data['from']['id']) ? $comment_data['from']['id'] : '';
					$comment->from_name = !empty($comment_data['from']['name']) ? $comment_data['from']['name'] : '';
					$comment->message = !empty($comment_data['message']) ? $comment_data['message'] : '';
					$comment->likes = !empty($comment_data['like_count']) ? $comment_data['like_count'] : '';
					
					$created_time = !empty($comment_data['created_time']) ? $comment_data['created_time'] : '';
					if (!empty($created_time)) {
						$created = new DateTime($created_time);
						$created->setTimezone(new DateTimeZone(date_default_timezone_get())); 
						$created_time = $created->format('Y-m-d H:i:s');
					}
					$comment->created_time = $created_time;
					
					$comment->save();
				}
			}
		}
	}
}