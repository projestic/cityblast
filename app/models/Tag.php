<?php
class Tag extends ActiveRecord\Model
{
	static $table = 'tag';

	static $belongs_to = array(
		array('tag_group', 'class' => 'TagGroup', 'foreign_key' => 'tag_group_id'),
	);
	
	
	/**
	 * Get member tags by member type and group
	 * 
	 * @param MemberType $member_type
	 * @param TagGroup $member_type
	 * @return array 
	 */
	public static function findAllByMemberTypeAndTagGroup($member_type, $tag_group)
	{
		$sql = 
			"SELECT " . 
				"t.id, " . 
				"t.ref_code, " . 
				"t.name, t.tag_group_id, " . 
				"t.publishable, " .
				"tmts.default_value " . 
			"FROM tag t ". 
			"INNER JOIN tag_member_type_setting tmts ON tmts.tag_id = t.id " . 
				"AND tmts.member_type_id = ? " . 
				" AND tmts.available = 1 " .
				" AND t.tag_group_id = ? " .
			" GROUP BY t.id";


		
		$tags = Tag::find_by_sql($sql, array($member_type->id,  $tag_group->id));
		
		return $tags;
	}

	/**
	 * Get tag groups.
	 *
	 * @param $params
	 * @return array
	 */
	public static function getTagGroups(array $params)
	{
		$signupSession  = new Zend_Session_Namespace('signup');
		$mtype = !empty($params['mtype']) ? strtoupper($params['mtype']) : 'AGENT';
		$signupSession->member_type = in_array($mtype, array('AGENT', 'BROKER')) ? $mtype : 'AGENT';

		$tag_groups = TagGroup::find('all');
		$member_type = MemberType::find_by_code($signupSession->member_type);

		$tag_group_data = array();
		if (!empty($tag_groups))
		{
			foreach ($tag_groups as $group)
			{
				$group_tags = Tag::findAllByMemberTypeAndTagGroup($member_type, $group);
				$tag_group_data[] = array(
					'group' => $group,
					'tags' => $group_tags
				);
			}
		}

		return $tag_group_data;
	}
}
