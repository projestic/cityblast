<?php

class TagMemberTypeSetting extends ActiveRecord\Model
{
	static $table = 'tag_member_type_setting';
	
	public static function initMemberType($member_type_id)
	{
		$settings = TagMemberTypeSetting::find_all_by_member_type_id($member_type_id);
		
		$tags = Tag::all();
		
		if (!empty($tags)) {
			foreach ($tags as $tag) {
				if (!empty($settings)) {
					foreach ($settings as $setting) {
						if ($setting->tag_id = $tag->id) {
							// This tag already has a related member setting so skip it
							continue 2;
						}
					}
				} 
				$new_setting = new TagMemberTypeSetting();
				$new_setting->tag_id = $tag->id;
				$new_setting->member_type_id = $member_type_id;
				$new_setting->available = 1;
				$new_setting->default_value = 1;
				$new_setting->save();
			}
		}
	}
}