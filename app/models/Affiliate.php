<?php
class Affiliate extends ActiveRecord\Model
{
	static $table = 'affiliate';
	
	static $belongs_to = array(
		array('owner', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('override_affiliate', 'class' => 'Affiliate', 'foreign_key' => 'override_affiliate_id')
	);

	static $has_one = array(
		array('promo', 'class' => 'Promo', 'foreign_key' => 'affiliate_id')
	);

	static $has_many = array(
		array('referred_members', 'class' => 'Member', 'foreign_key' => 'referring_affiliate_id')
	);

	public function hasReferredMembers() {
		return (bool) $this->countReferredMembers();
	}
	
	public function countReferredMembers() {
		return Member::count( array('conditions' => array('referring_affiliate_id' => $this->id)) );
	}
	
}