<?php

/**
 * ListingImage model to manage multiple images for manual blast
 *
 * @author nur
 */
class ListingImage extends ActiveRecord\Model
{
	static $table = 'listing_image';

	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id'),
	);

	public function getSizedImage($width, $height, $useCDN = true) {
		if (!$this->image) {
			return '/images/blank.gif';			
		}
		return $this->_getSizedImagePath($this->image, $width, $height, $useCDN);
	}

	protected function _getSizedImagePath($path, $width, $height, $useCDN = true) {
		$pathinfo = pathinfo($path);
		$cdn = ($useCDN) ? CDN_URL : '';
		return $cdn . "{$pathinfo['dirname']}/{$pathinfo['filename']}_{$width}x{$height}.{$pathinfo['extension']}";
	}

	public function ingestImage($original) {

		if (!is_readable($original)) throw new Exception("No image exists at $original");

		$original = $this->imageToLocal($original);

		if (!pathinfo($original, PATHINFO_EXTENSION)) {
			$ext = $this->autoImageExtension($original);
			rename($original, $original . '.' . $ext);
			$original .= '.' . $ext;
		}

		if (!$this->listing_id) {
			throw new Exception('ListingImage must have a listing attached first.');
		}
		
		$imageDest  = '/images/listings/' . date('Y') . "/" . date('m') . "/" . $this->listing_id . '/' . basename($original);
		$imageDestPath = CDN_ORIGIN_STORAGE . $imageDest;

		$resizer  = new Image();
		$images   = $resizer->resizeListingImage($original);
		$images[] = $original;
		
		if ( !file_exists(dirname($imageDestPath)) ) mkdir(dirname($imageDestPath), 0777, true);
		
		foreach ($images as $image) {
			copy($image, dirname($imageDestPath) . '/' . basename($image));
		}

		foreach ($images as $image) {
			unlink($image);
		}

		$this->image = $imageDest;
		$this->save();
	
	}
	
	protected function imageToLocal($photo) {
		if (stristr($photo, '://')) {
			$tmp 	= '/tmp/' . md5(microtime());
			$tmpPhoto 	= $tmp . '-' . basename($photo);
			copy($photo, $tmpPhoto);
			return $tmpPhoto;	
		}
		return $photo;
	}

	protected function autoImageExtension($image) {
		$info = getimagesize($image);
		switch ($info[2]) {
			case IMAGETYPE_GIF :
				return 'gif';
			case IMAGETYPE_JPEG :
			case IMAGETYPE_JPEG2000 :
				return 'jpg';
			case IMAGETYPE_PNG :
				return 'png';
		}
		return false;
	}

	public static function getRandomImage($listing)
	{
		if($listing->isListing())
		{	
			$img_array[] = $listing->image;
			
			$total = count($img_array) - 1;
			$rand = rand(0, $total);
			return($img_array[$rand]);
		}
		else
		{
		
			if(is_array($listing->images) && count($listing->images))
			{
				//$descriptions = $listing->message;
				foreach ($listing->images as $image)
				{
					$img_array[] = $image->image;
				}
				
				//Make sure to add in the orginal description into the mix
				$img_array[] = $listing->image;
				
	
				$total = count($img_array) - 1;
				$rand = rand(0, $total);
	
				
				return($img_array[$rand]);
			}
			else
			{
				return($listing->image);
			}
		}
	}

	/**
	 * Adjust uploaded images with provided Manual Blast
	 *
	 * @param Listing $listing
	 * @param array $images
	 */
	public static function adjustImages (Listing &$listing, Array $images)
	{

		if (is_array($images) && count($images) && $listing->id)
		{
			$newImgs		= array();
			$removeImgIds	= array();

			foreach ($listing->images as $img) {

				$image	= current($images);
				if (FALSE !== $image)
				{
					if ($image)
					{

						/**
						 * update existing image
						 */
						$img->ingestImage($image);

					}
					
					next($images);
				}
				else
				{
					/**
					 * remove existing image
					 */
					$removeImgIds[]	= $img->id;
					$img->delete();
				}
			}

			/**
			 * is there new image for add
			 */
			while (FALSE !== ($img = current($images)))
			{

				if ($img)
				{
					$image	= new self;
					$image->listing_id	= $listing->id;
					$image->ingestImage($img);
					$image->save();
					$newImgs[]		= $image;
				}
				
				next($images);
			}

		}
	}
}
?>
