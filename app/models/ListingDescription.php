<?php


class ListingDescription extends ActiveRecord\Model
{
	static $table = 'listing_description';

	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id'),
	);

	public static function getRandomDescription($listing)
	{
		$descriptions = $listing->descriptions;
		
		if(is_array($descriptions))
		{
			//$descriptions = $listing->message;
			foreach($descriptions as $description)
			{
				$desc_array[] = $description->description;
			}
			
			//Make sure to add in the orginal description into the mix
			$desc_array[] = $listing->message;
			

			$total = count($desc_array) - 1;
			$rand = rand(0, $total);

			
			return($desc_array[$rand]);
		}
		else
		{
			return($listing->message);
		}
	}

	/**
	 * Adjust submitted descriptions with provided Listing
	 *
	 * @param Listing $listing
	 * @param array $descriptions
	 */
	public static function adjustDescriptions (Listing &$listing, Array $descriptions)
	{

		if (is_array($descriptions) && count($descriptions) && $listing->id)
		{
			$newDescs		= array();
			$removeDescIds	= array();

			foreach ($listing->descriptions as $desc) {

				$description	= current($descriptions);
				if (FALSE !== $description)
				{
					if ($description)
					{
						/**
						 * update existing description
						 */
						$desc->description	= $description;
						$desc->save();
					}
					
					next($descriptions);
				}
				else
				{
					/**
					 * remove existing description
					 */
					$removeDescIds[]	= $desc->id;
					$desc->delete();
				}
			}

			/**
			 * is there new description for add
			 */
			while (FALSE !== ($desc = current($descriptions)))
			{

				if ($desc)
				{
					$description	= new self;
					$description->listing_id	= $listing->id;
					$description->description	= $desc;
					$newDescs[]					= $description;

					$description->save();
				}
				next($descriptions);
			}
		}
	}
	
		/**
	 * Fix invalid utf8 at the point of retrieval
	 * 
	 * This is a temporary fix. We need to add the filter to all input and
	 * updated all existing data in the DB. Then we can remove this method.
	 */
	public function &__get($name)
	{
		$result = parent::__get($name);
		$filter_attributes = array('description');
		if (in_array($name, $filter_attributes) && !empty($result)) {
			$result = Blastit_Text::utf8Fix($result);
		}
		return $result;
	}
}


