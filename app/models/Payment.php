<?php
class Payment extends ActiveRecord\Model
{
	static $table = 'payment';
	//static $pk    = 'track_id';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('cash_trans_debit', 'class' => 'CashTrans', 'foreign_key' => 'debit_cash_trans_id'),
		array('payment_gateway', 'class' => 'PaymentGateway', 'foreign_key' => 'payment_gateway_id')
	);

}