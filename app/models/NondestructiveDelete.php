<?php

/*
 *   This class extends ActiveRecord\Model to make deletes nondestructive. Records are flagged as deleted through the "status" field, 
 *   and filtered out of finder methods, with the exception of find_by_sql. You will need to include "status != 'deleted" in your find_by_sql calls as needed. 
 */

abstract class NondestructiveDelete extends ActiveRecord\Model
{

	public function delete() {
		$this->status = 'deleted';
		$this->save();
	}
	
	public function undelete() {
		$this->status = 'active';
		$this->save();
	}
	
	// Override ActiveRecord\Model to hide deleted listings unless specifically querying deleted listings
	
	public static function __callStatic($method, $args)
	{
		$args   = self::filter_options($args);
		return forward_static_call_array( array('ActiveRecord\Model', $method), $args );
	}
	
	public static function count()
	{
		$args   = func_get_args();
		$args   = self::filter_options($args);
		return forward_static_call_array( array('ActiveRecord\Model', 'count'), $args );
	}
	
	public static function find()
	{
		$args   = func_get_args();
		if (count($args) > 1) $args = self::filter_options($args);
		return forward_static_call_array( array('ActiveRecord\Model', 'find'), $args );
	}
	
	public static function find_deleted()
	{
		$args   = func_get_args();
		$args[1] = array('conditions' => array("id = ? AND status = ?", $args[0], 'deleted'));
		return forward_static_call_array( array('ActiveRecord\Model', 'find'), $args );
	}

	public static function find_including_deleted()
	{
		$args   = func_get_args();
		return forward_static_call_array( array('ActiveRecord\Model', 'find'), $args );
	}
	
	public static function pk_conditions($args)
	{
		$conditions = parent::pk_conditions($args);
		$conditions[] = array('status != ?' => 'deleted');
		return $conditions;
	}

	public static function filter_options(array &$array)
	{
		$i = 0;
		$filtered = false;
		$op = '!=';
		if ($array[0] == 'deleted') {
			$op = '=';
		}
		$status_field_regex = "/(^|[\(`. ])status[`= ]/";
		foreach ($array as $options) {
			if (is_array($options)) {
				if (isset($options['conditions']) && is_string($options['conditions'])) {
					if (!preg_match($status_field_regex, $options['conditions'])) {
						if (strlen($options['conditions'])) {
							$options['conditions'] .= " AND status $op 'deleted'";
						} else {
							$options['conditions'] = "status $op 'deleted'";
						}
					}
				}
				if (isset($options['conditions']) && is_array($options['conditions'])) {
					$status_set = false;
					foreach ($options['conditions'] as $c) {
						if (preg_match($status_field_regex, $c)) {
							$status_set = true;
							break;
						}
					}
					if (!$status_set) {
						if (isset($options['conditions'][0])) {
							$options['conditions'][0] .= " AND status $op ?";
						} else {
							$options['conditions'][0] = "status $op ?";
						}
						$options['conditions'][] = 'deleted';
					}
				}
				$array[$i] = $options;
				return $array;
			}
			$i++;
		}
		if (!$filtered) {
			$array[] = array('conditions' => array("status $op ?", 'deleted'));
		}
		return $array;
	}

}
?>