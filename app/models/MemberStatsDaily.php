<?php

class MemberStatsDaily extends ActiveRecord\Model
{
	static $table = 'member_stats_daily';
	
	public static function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$time_span = $end_time - $start_time;
		$time_span_days = $time_span / 86400;
		
		if ($time_span_days > 1095) {
			throw new Exception('Time span is to large');
		}
		
		Logger::log(
			'Compiling member payment stats between ' . date('Y-m-d', $start_time) . ' and ' . date('Y-m-d', $end_time), 
			Logger::LOG_TYPE_DEBUG
		);
		
		$current_time = $start_time;
		while ($current_time <= $end_time){
			$current_date = date('Y-m-d', $current_time);
			self::populateDay($current_date);
			$current_time = strtotime($current_date . ' +1 day');
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	public static function populateDay($date = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		// By default we calculate yesterdays data 
		// - todays data can not be calculated until the day is over since data can still change
		$date = !empty($date) ? $date : date('Y-m-d', strtotime('yesterday'));
		$date = self::getDateString($date);
		
		Logger::log('Compiling member payment stats for day ' . $date, Logger::LOG_TYPE_DEBUG);
		
		$yesterday_stats = self::getPreviousDayStats($date);
		
		$stats = array();
		$stats['date'] = $date;	
		$stats['nocc_trial_total_prev'] = $yesterday_stats ? $yesterday_stats->nocc_trial_total : 0;
		$stats['nocc_trial_new'] = self::getNoCcTrialNew($date);
		$stats['nocc_trial_failed_convert'] = self::getNoCcTrialFailedConvert($date);
		$stats['nocc_trial_cancel'] = self::getNoCcTrialCancel($date);
		$stats['nocc_trial_deleted'] = self::getNoCcTrialDeleted($date);
		$stats['nocc_trial_total'] = self::getNoCcTrialTotal($date);
		$stats['nocc_trial_converted'] = self::getNoCcTrialConverted($date);
		$stats['nocc_trial_failed_convert_total'] = self::getNoCcTrialFailedConvertTotal($date);
		$stats['cc_trial_total_prev'] = $yesterday_stats ? $yesterday_stats->cc_trial_total : 0;
		$stats['cc_trial_new'] = self::getCcTrialNew($date);
		$stats['cc_trial_cancel'] = self::getCcTrialCancel($date);
		$stats['cc_trial_deleted'] = self::getCcTrialDeleted($date);
		$stats['cc_trial_total'] = self::getCcTrialTotal($date);
		$stats['cc_trial_converted'] = self::getCcTrialConverted($date);
		$stats['cc_trial_alltime'] = self::getCcTrialAllTime($date);
		$stats['paid_total_prev'] = $yesterday_stats ? $yesterday_stats->paid_total : 0;
		$stats['paid_new'] = self::getPaidNew($date);
		$stats['paid_cancel'] = self::getPaidCancel($date);
		$stats['paid_deleted']  = self::getPaidDeleted($date);
		$stats['paid_total'] = self::getPaidTotal($date);
		$stats['paid_alltime'] = self::getPaidAllTime($date);
		$stats['active_cc'] = self::getCcActive($date);
		$stats['cancelled_new']  = self::getCancelledNew($date);
		$stats['cancelled_total'] = self::getCancelledTotal($date);
		$stats['deleted_new']  = self::getDeletedNew($date);
		$stats['deleted_total'] = self::getDeletedTotal($date);
		$stats['free_new'] = self::getFreeNew($date);
		$stats['free'] = self::getFree($date);
		$stats['signup_new'] = self::getSignupNew($date);
		$stats['signup_alltime'] = self::getSignupAlltime($date);
		$stats['total'] = $stats['nocc_trial_total'] + $stats['cc_trial_total'] + $stats['paid_total'];
		$stats['avrg_lifetime_value'] = self::getAvrgLifetimeValue($date);
		
		Logger::log('Saving stats for day', Logger::LOG_TYPE_DEBUG);
		$existing_stats = MemberStatsDaily::find_by_date($date);
		if ($existing_stats) {
			Logger::log('Updating existing stats record', Logger::LOG_TYPE_DEBUG);
			foreach ($stats as $key => $value) {
				$existing_stats->assign_attribute($key, $value);
				$existing_stats->save();
			}
		} else {
			Logger::log('Creating new stats record', Logger::LOG_TYPE_DEBUG);
			MemberStatsDaily::create($stats);
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	/**
	 * Get number of members who were on trial and had not given payment details on the given date
	 */
	protected static function getNoCcTrialNew($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member was created on given date
				DATE(m.created_at) = ? AND
				# Member had 
				# - payment status 'signup' or 'extended_trial' on given date
				# - account_type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ? 
					AND (msld.payment_status = 'signup' OR msld.payment_status = 'extended_trial')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL
				# Member had not given payment details or made an actual payment 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcTrialCancel($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'cancelled' on given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.payment_status = 'cancelled'
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				AND
				# Member had 
				# - payment status 'signup' or 'extended_trial' on day before given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'signup' OR msld.payment_status = 'extended_trial')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Member had not given payment details or made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcTrialFailedConvert($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'signup_expired' on given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.payment_status = 'signup_expired'
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				AND
				# Member had 
				# - payment status 'signup' or 'extended_trial' on day before given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'signup' OR msld.payment_status = 'extended_trial')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Member had not given payment details or made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcTrialFailedConvertTotal($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'signup_expired' on given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.payment_status = 'signup_expired'
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcTrialDeleted($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had status 'deleted' on given day
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ? 
					AND msld.status = 'deleted'
				) IS NOT NULL 
				AND
				# Member had 
				# - payment status 'signup' or 'extended_trial' on day before given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'signup' OR msld.payment_status = 'extended_trial')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Member had not given payment details or made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcTrialConverted($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'paid' on given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.payment_status = 'paid'
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				AND
				# Member had 
				# - payment status 'signup' or 'extended_trial' on day before given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'signup' OR msld.payment_status = 'extended_trial')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				AND
				# Member had not given payment details or made an actual payment on day before
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
				AND
				# Member had given payment details or made an actual payment
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NOT NULL
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date_day_before, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcTrialTotal($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'signup' or 'extended_trial' on given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'signup' OR msld.payment_status = 'extended_trial')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL
				# Member had not given payment details or made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
		";

		$date = self::getDateString($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcTrialNew($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));

		if (strtotime($date) < strtotime('2013-06-28')) {
			// Use legacy data query for stats relating to dates before we introduced the member status log
			$sql = "
				SELECT count(m.id) AS total 
				FROM member m
				WHERE 
					# Member had given payment details on given date
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) <= ?
						AND p.amount = 0
						LIMIT 1
					) IS NOT NULL
					# Member had not given payment details before given date
					AND
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) < ?
						AND p.amount = 0
						LIMIT 1
					) IS NULL
					# Member had not made an actual payment
					AND
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) <= ?
						AND p.amount > 0
						LIMIT 1
					) IS NULL
			";
			$query_result = Member::find_by_sql($sql, array($date, $date, $date));
		} else {
			$sql = "
				SELECT count(m.id) AS total 
				FROM member m
				WHERE 
					# Member had 
					# - payment status 'paid' or 'extended_trial'
					# - account type 'paid'
					# - status 'active'
					(
						SELECT msld.member_id 
						FROM member_status_log_daily msld 
						WHERE m.id = msld.member_id 
						AND msld.date = ?
						AND (msld.payment_status = 'paid' OR msld.payment_status = 'extended_trial')
						AND msld.account_type = 'paid' 
						AND msld.status = 'active'
					) IS NOT NULL 
					AND
					# Member did not have status 'paid' or 'payment_failed' or 'extended_trial' the day before given date
					(
						SELECT msld.member_id 
						FROM member_status_log_daily msld 
						WHERE m.id = msld.member_id 
						AND msld.date = ? 
						AND (msld.payment_status = 'paid' OR msld.payment_status = 'payment_failed' OR msld.payment_status = 'extended_trial')
					) IS NULL 
					# Member had given payment details on given date
					AND
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) <= ?
						AND p.amount = 0
						LIMIT 1
					) IS NOT NULL
					# Member had not made an actual payment
					AND
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) <= ?
						AND p.amount > 0
						LIMIT 1
					) IS NULL
			";
			$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date, $date));
		}
		
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcTrialCancel($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'cancelled' on given date
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.payment_status = 'cancelled' 
					AND msld.status = 'active'
				) IS NOT NULL 
				AND
				# Member had 
				# - payment status 'paid' or 'payment_failed' or 'extended_trial' on day before given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'paid' OR msld.payment_status = 'payment_failed' OR msld.payment_status = 'extended_trial')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Member had given payment details 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) < ?
					AND p.amount = 0
					LIMIT 1
				) IS NOT NULL
				# Member had not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) < ?
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";

		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcTrialDeleted($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had status 'deleted' on given date
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.status = 'deleted'
				) IS NOT NULL 
				AND
				# Member had 
				# - payment status 'paid' or 'payment_failed' or 'extended_trial' on day before given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'paid' OR msld.payment_status = 'payment_failed' OR msld.payment_status = 'extended_trial')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Member had given payment details 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) < ?
					AND p.amount = 0
					LIMIT 1
				) IS NOT NULL
				# Member had not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) < ?
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";

		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcTrialConverted($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Had 
				# - payment status 'paid' on given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.payment_status = 'paid'
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				AND
				# Had 
				# - payment status 'paid' or 'payment_failed' or 'extended_trial' on day before given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (
						msld.payment_status = 'paid' OR 
						msld.payment_status = 'payment_failed' OR 
						msld.payment_status = 'extended_trial'
					)
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Has made an actual payment on given day
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) = ?
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
				# Had not made an actual payment the day before
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NULL
				# Had given payment details the day before
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NOT NULL
		";

		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date, $date_day_before, $date_day_before));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcTrialTotal($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'paid' or 'payment_failed' or 'extended_trial' on given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ? 
					AND (msld.payment_status = 'paid' OR msld.payment_status = 'payment_failed' OR msld.payment_status = 'extended_trial')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Member had given payment details 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount = 0
					LIMIT 1
				) IS NOT NULL
				# Member had not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date, $date, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcTrialAllTime($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had given payment details 
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount = 0
					LIMIT 1
				) IS NOT NULL
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getPaidNew($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Had 
				# - payment status 'paid' on given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.payment_status = 'paid'
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Has made an actual payment at some point
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
				# Either hadnt made a payment before given date (paid-trial to paid)
				# Or had a status other than 'paid' or 'payment_failed' on day before given date (extended_trial/cancelled to paid)
				# Or has a free or inactive account the day before the given date
				AND
				(
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) <= ?
						AND p.amount > 0
						LIMIT 1
					) IS NULL
					OR
					(
						SELECT msld.member_id 
						FROM member_status_log_daily msld 
						WHERE m.id = msld.member_id 
						AND msld.date = ?
						AND (
							msld.payment_status = 'paid' OR 
							msld.payment_status = 'payment_failed' OR
							msld.account_type != 'paid' OR
							msld.status != 'active'
						)
						LIMIT 1
					) IS NULL
				) 
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		$query_result = Member::find_by_sql($sql, array($date, $date, $date_day_before, $date_day_before));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getPaidCancel($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		if (strtotime($date) < strtotime('2013-06-28')) {
			// Use legacy data query for stats relating to dates before we introduced the member status log
			$sql =  "
				SELECT count(m.id) AS total 
				FROM member m
				WHERE 
					# Member had status cancelled
					m.payment_status = 'cancelled' 
					# Member cancelled on given date
					AND
					DATE(m.cancelled_date) = ?
					# Member had made an actual payment
					AND
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) < ?
						AND p.amount > 0
						LIMIT 1
					) IS NOT NULL
			";
			$query_result = Member::find_by_sql($sql, array($date, $date));
		} else {
			$sql = "
				SELECT count(m.id) AS total 
				FROM member m
				WHERE 
					# Member had 
					# - payment status 'cancelled' or 'refund_issued' or account_type 'free' on given date
					# - status 'active'
					(
						SELECT msld.member_id 
						FROM member_status_log_daily msld 
						WHERE m.id = msld.member_id 
						AND msld.date = ?
						AND (msld.payment_status = 'cancelled' OR msld.payment_status = 'refund_issued' OR msld.account_type = 'free')
						AND msld.status = 'active'
					) IS NOT NULL 
					# Member had status 'paid' or 'payment_failed' on day before given date
					# - account type 'paid'
					# - status 'active'
					AND
					(
						SELECT msld.member_id 
						FROM member_status_log_daily msld 
						WHERE m.id = msld.member_id 
						AND msld.date = ?
						AND (msld.payment_status = 'paid' OR msld.payment_status = 'payment_failed')
						AND msld.account_type = 'paid'
						AND msld.status = 'active'
					) IS NOT NULL 
					# Member had made an actual payment
					AND
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) < ?
						AND p.amount > 0
						LIMIT 1
					) IS NOT NULL
			";
			$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date));
		}
		
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getPaidDeleted($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had status 'deleted' on given day
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ? 
					AND msld.status = 'deleted'
				) IS NOT NULL 
				# Member had 
				# - payment status 'paid' or 'payment_failed' on day before given date
				# - account type 'paid'
				# - status 'active'
				AND
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'paid' OR msld.payment_status = 'payment_failed')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Member had made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));

		$query_result = Member::find_by_sql($sql, array($date, $date_day_before, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getPaidTotal($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'paid' or 'payment_failed' on given date
				# - account type 'paid'
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'paid' OR msld.payment_status = 'payment_failed')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Member had made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getPaidAllTime($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had made an actual payment
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public static function getCcActive($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'paid' or 'payment_failed' on given date
				# - account type 'paid' on given date
				# - status active on given data
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.payment_status = 'paid' OR msld.payment_status = 'payment_failed')
					AND msld.account_type = 'paid'
					AND msld.status = 'active'
				) IS NOT NULL 
				# Member had given payment details
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NOT NULL
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	} 
	
	protected static function getCancelledNew($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'cancelled' or 'refund_issued' on given date
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ? 
					AND (
						msld.payment_status = 'cancelled' OR 
						msld.payment_status = 'refund_issued'
					)
					AND msld.status = 'active'
				) IS NOT NULL 
				AND
				# Member did not have
				# - payment status 'cancelled' or 'refund_issued' on day before given date
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ? 
					AND (
						msld.payment_status = 'cancelled' OR 
						msld.payment_status = 'refund_issued'
					)
					AND msld.status = 'active'
				) IS NULL 
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));

		$query_result = Member::find_by_sql($sql, array($date, $date_day_before));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCancelledTotal($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had 
				# - payment status 'cancelled' or 'refund_issued' on given date
				# - status 'active'
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ? 
					AND (
						msld.payment_status = 'cancelled' OR 
						msld.payment_status = 'refund_issued'
					)
					AND msld.status = 'active'
				) IS NOT NULL 
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public static function getFreeNew($date)
	{		
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had account type 'free' on given date
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.account_type = 'free' OR msld.payment_status = 'free')
				) IS NOT NULL 
				AND
				# Member did not have account type 'free' on day before given date
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.account_type = 'free' OR msld.payment_status = 'free')
				) IS NULL 
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));

		$query_result = Member::find_by_sql($sql, array($date, $date_day_before));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public static function getFree($date)
	{		
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had account type 'free' on given date
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND (msld.account_type = 'free' OR msld.payment_status = 'free')
				) IS NOT NULL 
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public static function getDeletedNew($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had status 'deleted' on given date
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.status = 'deleted'
				) IS NOT NULL 
				AND
				# Member did not have status 'deleted' on day before given date
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.status = 'deleted'
				) IS NULL 
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));

		$query_result = Member::find_by_sql($sql, array($date, $date_day_before));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public static function getDeletedTotal($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				# Member had status 'deleted' on given date
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.status = 'deleted'
				) IS NOT NULL 
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public function getSignupNew($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				DATE(created_at) = ?
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public function getSignupAlltime($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			WHERE 
				DATE(created_at) <= ?
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	public function getAvrgLifetimeValue($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT
				ROUND
				(
					(
						# Total payments from all members
						SELECT SUM(p.amount) AS total 
						FROM payment p
						WHERE 
							p.amount > 0 AND
							DATE(p.created_at) <= ?
					)
					/
					(
						# Total members who paid
						SELECT COUNT(DISTINCT p.member_id) AS members 
						FROM payment p
						WHERE 
							p.amount > 0 AND
							DATE(p.created_at) <= ?
					)
				, 2) AS total
		";
		
		$date = self::getDateString($date);

		$query_result = Payment::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) && $query_result[0]->total ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getPreviousDayStats($date)
	{
		$date = self::getDateString($date);
		$previous_date_date = date('Y-m-d', strtotime('yesterday', strtotime($date)));
		$previous_day_stats = MemberStatsDaily::find_by_date($previous_date_date);
		return $previous_day_stats;
	}
	
	protected static function getDateString($date)
	{
		$date = ($date instanceof DateTime) ? $date->format('Y-m-d') : $date;
		$date = !empty($date) ? $date : date('Y-m-d');
		return $date;
	}
}