<?php
class CancelAccount extends ActiveRecord\Model
{
	static $table = 'cancel_account';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('admin', 'class' => 'Member', 'foreign_key' => 'admin_id')
	);

}
?>