<?php
class ReferralCredit extends ActiveRecord\Model
{
	static $table = 'referral_credit';


	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('referred_member', 'class' => 'Member', 'foreign_key' => 'referred_member_id'),
		array('payment', 'class' => 'Payment', 'foreign_key' => 'payment_id')		
	);
}

?>