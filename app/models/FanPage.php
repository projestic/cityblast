<?php
class FanPage extends ActiveRecord\Model
{
	static $table = 'fanpage';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);

}
?>