<?php

class NetworkApp extends ActiveRecord\Model
{
	static $table = 'network_app';
	
	/**
	 * Get application suitable for use in new registration with a particular network
	 * 
	 */
	public static function getForRegistration($network)
	{
		$conditions = array(
			'conditions'	=> array('deprecated = 0 AND registration_enabled = 1 AND network = ?', array($network)),
			'order'			=> 'members DESC',
			'limit'			=> 1,
		);
		$results = static::all($conditions);
		$result = !empty($results) ? $results[0] : null;
		return $result;
	}
	
	public function getFacebookAPI() {
	
		if ($this->network != 'FACEBOOK') throw new Exception('Not Facebook network. Is ' . $this->network . ' network.');
	
		return new Facebook(
								array(
								'appId'  => $this->consumer_id,
								'secret' => $this->consumer_secret
								)
							);

	}
	
	public function getFacebookAppAccessToken() {

		if ($this->network != 'FACEBOOK') throw new Exception('Not Facebook network. Is ' . $this->network . ' network.');
		
		return $this->consumer_id . '|' . $this->consumer_secret;

	}
	
}