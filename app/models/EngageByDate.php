<?php

class EngageByDate extends ActiveRecord\Model
{
	static $table = 'engage_by_date';
	
	public function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$start_date = date('Y-m-d', $start_time);
		$end_date = date('Y-m-d', $end_time);
		
		Logger::log(
			'Compiling engagement by date between ' . $start_date . ' and ' . $end_date, 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT 
				ctbdl.date, 
				ctbdl.count,
				(SELECT likes FROM facebook_stat_by_date WHERE date = ctbdl.date),
				(SELECT comments FROM facebook_stat_by_date WHERE date = ctbdl.date),
				(SELECT shares FROM facebook_stat_by_date WHERE date = ctbdl.date),
				0,
				(
					SELECT 
						count(id) 
					FROM post 
					WHERE 
						DATE(created_at) = ctbdl.`date` AND
						result = 1 
				) AS posts,
				0,
				0,
				0,
				0,
				0
			FROM `click_stat_by_date` AS ctbdl
			WHERE date >= " . $db_con->escape($start_date) . "  
			AND date <= " . $db_con->escape($end_date) . "  
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		$sql = "
			SELECT 
				fsbd.date, 
				(SELECT count FROM click_stat_by_date WHERE date = fsbd.date),
				fsbd.likes,
				fsbd.comments,
				fsbd.shares,
				0,
				(
					SELECT 
						count(id) 
					FROM post 
					WHERE 
						DATE(created_at) = fsbd.`date` AND
						result = 1 
				) AS posts,
				0,
				0,
				0,
				0,
				0
			FROM `facebook_stat_by_date` fsbd
			WHERE fsbd.date >= " . $db_con->escape($start_date) . "  
			AND fsbd.date <= " . $db_con->escape($end_date) . "  
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		$sql = "
			UPDATE `engage_by_date` 
			SET 
				score = (clicks + (likes * 10) + (comments * 15) + (shares * 25))
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		$sql = "
			UPDATE `engage_by_date` 
			SET 
				clicks_perpost = ROUND(clicks / posts, 3),
				likes_perpost = ROUND(likes / posts, 3),
				comments_perpost = ROUND(comments / posts, 3),
				shares_perpost = ROUND(shares / posts, 3),
				score_perpost = ROUND(score / posts, 3)
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}