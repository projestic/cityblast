<?php

use ActiveRecord\Utils;
class Interaction extends ActiveRecord\Model
{

	static $table	= 'interaction';

	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id'),
		array('agent', 'class' => 'Member', 'foreign_key' => 'referring_agent_id'),
	);

	static function checkup()
	{
		$shared	= FALSE;

		if (isset($_SESSION['fb_share_processing']))
		{
			self::entry($_SESSION['fb_share_processing'], 'facebook_share');
			unset($_SESSION['fb_share_processing']);
			$shared	= TRUE;
		}


		if (isset($_SESSION['twitter_share_processing']))
		{
			self::entry($_SESSION['twitter_share_processing'], 'twitter_share');
			unset($_SESSION['twitter_share_processing']);
			$shared	= TRUE;
		}


		if (isset($_SESSION['google_share_processing']))
		{
			self::entry($_SESSION['google_share_processing'], 'google_plus_share');
			unset($_SESSION['google_share_processing']);
			$shared	= TRUE;
		}


		if (isset($_SESSION['email_share_processing']))
		{
			self::entry($_SESSION['email_share_processing'], 'email_share');
			unset($_SESSION['email_share_processing']);
			$shared	= TRUE;
		}

		return $shared;
	}


	static function entry($listing_id, $booking_agent_id, $type)
	{
		$interaction	= new self;

		$interaction->referring_agent_id	 = $booking_agent_id;
		$interaction->listing_id = $listing_id;
		$interaction->type = $type;

		// store additional information
		if ( isset($_SERVER['HTTP_USER_AGENT']) ) {
		    $interaction->http_user_agent = ActiveRecord\Utils::http_user_agent($_SERVER['HTTP_USER_AGENT']);
		}

        if ( isset($_SERVER['REMOTE_ADDR']) ) {
            $interaction->remote_addr = $_SERVER['REMOTE_ADDR'];
        }

        if ( isset($_SERVER['REMOTE_HOST']) ) {
            $interaction->remote_host = gethostbyaddr($_SERVER['REMOTE_HOST']);
        }

		$interaction->save();
	}

}
?>
