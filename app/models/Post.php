<?php

class Post extends NondestructiveDelete
{
	static $table = 'post';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id')
	);
	
	static $has_one = array(
		array('delete_action', 'class' => 'DeleteAction', 'foreign_key' => 'post_id', 'order' => 'created_at desc'),
	);

	public function delete() {
		$network_app_facebook = ($this->member && $this->member->network_app_facebook) ? $this->member->network_app_facebook : Zend_Registry::get('networkAppFacebook');
		$result = parent::delete();
		try {
			if (strlen($this->fb_post_id) && ($this->fb_post_id != 'EMPTY')) {
				$facebook = $network_app_facebook->getFacebookAPI();
				$result = $facebook->api('/' . $this->fb_post_id, 'DELETE');
			}
		} catch (Exception $e) {
			return false;
		}
		return $result;
	}


}

?>