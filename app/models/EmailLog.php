<?php

use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Components\Contacts\ContactList;
use Ctct\Components\Contacts\CustomField;
use Ctct\Components\Contacts\EmailAddress;
use Ctct\Exceptions\CtctException;

class EmailLog extends ActiveRecord\Model
{
	static $table = 'email_sent';

    static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);
	
	public function addToMailchimp() 
	{
	
		// ONLY sync referrals.
	
		if (!defined('MAILCHIMP_API_KEY') || ($this->type != 'REFERRAL')) return false;
		$mcapi = new MCAPI(MAILCHIMP_API_KEY);

		$email_type  	   = 'html';
		$double_optin      = false;
		$update_existing   = true;
		$replace_interests = false;
		$send_welcome      = false;
		
		$merge_vars = array('STATUS' => 'Referral');

		if ($this->member) {
			$merge_vars['REF_MID']   = $this->member->id;
			$merge_vars['REF_FNAME'] = $this->member->first_name;
			$merge_vars['REF_LNAME'] = $this->member->last_name;
		}
		
		if (!$this->email || !filter_var($this->email, FILTER_VALIDATE_EMAIL)) return false;
	    
		$existing = $mcapi->listMemberInfo(MAILCHIMP_LIST_ID, $this->email);

		if (count($existing['data']) && isset($existing['data'][0]['id'])) return false;

		$mcapi->listSubscribe(MAILCHIMP_LIST_ID, $this->email, $merge_vars, $email_type, $double_optin, $update_existing, $replace_interests, $send_welcome);
		
		if ($mcapi->errorCode) {
			throw new Zend_Exception($mcapi->errorMessage, $mcapi->errorCode);
		}
		
		return true;
	}

	public function addToConstantContact() 
	{
	
		// ONLY sync referrals.

		if (!defined('CONSTANT_CONTACT_API_KEY') || ($this->type != 'REFERRAL')) return false;

		$cc = new ConstantContact(CONSTANT_CONTACT_API_KEY);

		if (!$this->email || !filter_var($this->email, FILTER_VALIDATE_EMAIL)) return false;

		$response = $cc->getContactByEmail(CONSTANT_CONTACT_OAUTH2_ACCESS_TOKEN, $this->email);
				
		if (!empty($response->results) && $response->results[0]->id) {		
			// don't sync if it's already a contact, this model has no changes that need to be synced and we don't want to set existing members to referral status
			return false;
		}

		$list = new ContactList;
		
		// Matches the list ID in ConstantContact
		
		$list->id = '15';
		
		$contact = new Contact();
		$contact->addEmail($this->email);			
		$contact->addList($list);

		if ($this->member) {
			$contact->addCustomField( CustomField::create( array('name' => 'CustomField5', 'value' => $this->member->first_name )) );
			$contact->addCustomField( CustomField::create( array('name' => 'CustomField6', 'value' => $this->member->last_name )) );
			$contact->addCustomField( CustomField::create( array('name' => 'CustomField7', 'value' => (string) $this->member->id )) );
		}
		
		try 
		{
			return $cc->addContact(CONSTANT_CONTACT_OAUTH2_ACCESS_TOKEN, $contact, false);
		} catch (Exception $e) {
			if ($e->getCode() == 0 || $e->getCode() == 409) return false;
			throw $e;
		}
		
		return true;

	}


}