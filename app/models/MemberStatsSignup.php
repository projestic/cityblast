<?php

class MemberStatsSignup extends ActiveRecord\Model
{ 
	static $table = 'member_stats_signup';
	const TRIAL_DAYS = 14;
	
	public static function populateDateRange($start, $end)
	{
		Logger::log($start . ', ' . $end, Logger::LOG_TYPE_DEBUG);
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$time_span = $end_time - $start_time;
		$time_span_days = $time_span / 86400;
		
		if ($time_span_days > 365) {
			throw new Exception('Time span is to large');
		}
		
		Logger::log(
			'Compiling member signup stats between ' . date('Y-m-d', $start_time) . ' and ' . date('Y-m-d', $end_time), 
			Logger::LOG_TYPE_DEBUG
		);
		
		$current_time = $start_time;
		while ($current_time <= $end_time){
			$current_date = date('Y-m-d', $current_time);
			static::populateDay($current_date);
			$current_time = strtotime($current_date . ' +1 day');
		}

		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	
	public static function populateDay($date = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		// By default we calculate yesterdays data 
		// - todays data can not be calculated until the day is over since data can still change
		$date = !empty($date) ? $date : date('Y-m-d', strtotime('yesterday'));
		$date = self::getDateString($date);
		
		Logger::log('Compiling member signup stats for day ' . $date, Logger::LOG_TYPE_DEBUG);
		
		$trialIsOver = static::isTrialOver($date);
		
		if ($trialIsOver) {
			Logger::log('Trial is over', Logger::LOG_TYPE_DEBUG);
		} else {
			Logger::log('Trial is not over', Logger::LOG_TYPE_DEBUG);
		}

		$stats = array();
		$stats['ccnew'] = static::getCcNew($date);
		$stats['cc_paid_cancel'] = static::getCcPaidCancel($date);
		$stats['total'] = static::getTotal($date);
		$stats['cc'] = static::getCc($date);
		$stats['nocc'] = static::getNoCc($date);
		
		if ($trialIsOver) {
			$stats['cc_paid'] = static::getCcPaid($date);
			$stats['cc_cancel'] = static::getCcCancelled($date);
			$stats['cc_failed_payment'] = static::getCcFailedPayment($date);
			$stats['nocc_paid'] = static::getNoCcPaid($date);
			$stats['nocc_cancel'] = static::getNoCcCancelled($date);
			$stats['nocc_failed_payment'] = static::getNoCcFailedPayment($date);
		}
		
		$stats['avconv_rate'] = static::getAverageConversionRate($date);
		
		$stats['cc_avconv_rate'] = static::getCcAverageConversionRate($date);
		$stats['nocc_avconv_rate'] = static::getNoCcAverageConversionRate($date);
		
		$stats['cc_revenue_projected_max'] = static::getCcRevenueProjectedMax($date);
		$stats['nocc_revenue_projected_max'] = static::getNoCcRevenueProjectedMax($date);
		$stats['revenue_projected_max'] = $stats['cc_revenue_projected_max'] + $stats['nocc_revenue_projected_max'];
		
		$stats['cc_revenue_projected'] = static::getCcRevenueProjected($date);
		$stats['nocc_revenue_projected'] = static::getNoCcRevenueProjected($date);
		$stats['revenue_projected'] = static::getRevenueProjected($date);
		
		if ($trialIsOver) {
			$stats['revenue_actual'] = static::getRevenueActual($date);
			$stats['profit'] = static::getRevenueActual($date);
		} else {
			$stats['revenue_actual'] = 0;
			$stats['profit'] = 0;
		}
		
		Logger::log('Saving stats for day', Logger::LOG_TYPE_DEBUG);
		$existing_stats = MemberStatsSignup::find_by_date($date);
		if ($existing_stats) {
			Logger::log('Updating existing stats record', Logger::LOG_TYPE_DEBUG);
			$stats['profit'] = $stats['profit'] - $existing_stats->ad_spend;
			foreach ($stats as $key => $value) {
				$existing_stats->assign_attribute($key, $value);
			}
			$existing_stats->save();
		} else {
			Logger::log('Creating new stats record', Logger::LOG_TYPE_DEBUG);
			$stats['date'] = $date;
			MemberStatsSignup::create($stats);
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	/**
	 * Get number of members who signed up on give date
	 */
	protected static function getTotal($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE
				DATE(m.created_at) = ? 
		";
		
		$date = self::getDateString($date);
		
		$query_result = Member::find_by_sql($sql, array($date));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getAverageConversionRate($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$ccAvrgConversionRate = static::getCcAverageConversionRate($date);
		$noccAvrgConversionRate = static::getNoCcAverageConversionRate($date);
		
		$result = ($ccAvrgConversionRate + $noccAvrgConversionRate) / 2;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcAverageConversionRate($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT 
			(
				(
					SELECT 
						SUM(cc_paid/cc)
					FROM
						member_stats_signup
					WHERE
						cc > 0 AND 
						date <= ?
				) 
				/
				(
					SELECT 
						COUNT(id)
					FROM
						member_stats_signup
					WHERE
						cc > 0 AND 
						date <= ?
				)
			) AS total
		";
		
		
		$query_result = Member::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) ? (float) $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcAverageConversionRate($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT 
			(
				(
					SELECT 
						SUM(nocc_paid/nocc)
					FROM
						member_stats_signup
					WHERE
						nocc > 0 AND 
						date <= ?
				) 
				/
				(
					SELECT 
						COUNT(id)
					FROM
						member_stats_signup
					WHERE
						nocc > 0 AND 
						date <= ?
				)
			) AS total
		";
		
		$query_result = Member::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) ? (float) $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCc($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE
				# Member signedup on given date
				DATE(m.created_at) = ? 
				# Member does not have payment_status or account type 'free'
				AND (payment_status != 'free' AND account_type != 'free')
				# Member had given payment details on given date
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NOT NULL
		";
		
		$date = self::getDateString($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCc($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE 
				# Member signedup on given date
				DATE(m.created_at) = ? 
				# Member does not have payment_status or account type 'free'
				AND (payment_status != 'free' AND account_type != 'free')
				# Member had not given payment details on given date
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcNew($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE 
				# Member does not have payment_status or account type 'free'
				(payment_status != 'free' AND account_type != 'free')
				# Member had given payment details on given date
				AND 
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) = ?
					LIMIT 1
				) IS NOT NULL
				# Member had not given payment details on day before
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) < ?
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);

		$query_result = Member::find_by_sql($sql, array($date, $date));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcPaid($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE 
				# Member signedup on given date
				DATE(m.created_at) = ? 
				# Member does not have payment_status or account type 'free'
				AND (payment_status != 'free' AND account_type != 'free')
				# Member had given payment details on given date
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NOT NULL
				# Member has made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		$date = self::getDateString($date);
		$date_processing_end = static::getProcessingEndDate($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date, $date_processing_end));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcCancelled($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE 
				# Member signedup on given date
				DATE(m.created_at) = ? 
				# Member does not have payment_status or account type 'free'
				AND (payment_status != 'free' AND account_type != 'free')
				# Member had given payment details on given date
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NOT NULL
				# Member had payment status 'cancelled'
				AND
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date <= ?
					AND msld.payment_status = 'cancelled'
					ORDER BY msld.date DESC
					LIMIT 1
				) IS NOT NULL 
				# Member has not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);
		$date_processing_end = static::getProcessingEndDate($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date, $date_processing_end, $date_processing_end));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcPaidCancel($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE 
				# Member does not have payment_status or account type 'free'
				(payment_status != 'free' AND account_type != 'free')
				# Member had made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
				# Member had payment status 'cancelled' on given date
				AND
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.payment_status = 'cancelled'
					ORDER BY msld.date DESC
					LIMIT 1
				) IS NOT NULL 
				# Member did not have had payment status 'cancelled' on day before given date
				AND
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date = ?
					AND msld.payment_status != 'cancelled'
					ORDER BY msld.date DESC
					LIMIT 1
				) IS NOT NULL 
		";
		
		$date = self::getDateString($date);
		$date_day_before = date('Y-m-d', strtotime($date . ' -1 day'));
		
		$query_result = Member::find_by_sql($sql, array($date, $date, $date_day_before));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getCcFailedPayment($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE 
				# Member signedup on given date
				DATE(m.created_at) = ? 
				# Member does not have payment_status or account type 'free'
				AND (payment_status != 'free' AND account_type != 'free')
				# Member had given payment details on given date
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NOT NULL
				# Member had payment status 'payment_failed'
				AND
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date <= ?
					AND msld.payment_status = 'payment_failed'
					ORDER BY msld.date DESC
					LIMIT 1
				) IS NOT NULL 
				# Member has not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);
		$date_processing_end = static::getProcessingEndDate($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date, $date_processing_end, $date_processing_end));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcPaid($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE 
				# Member signedup on given date
				DATE(m.created_at) = ? 
				# Member does not have payment_status or account type 'free'
				AND (payment_status != 'free' AND account_type != 'free')
				# Member had not given payment details on given date
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
				# Member had made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		$date = self::getDateString($date);
		$date_processing_end = static::getProcessingEndDate($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date, $date_processing_end));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcCancelled($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE 
				# Member signedup on given date
				DATE(m.created_at) = ? 
				# Member does not have payment_status or account type 'free'
				AND (payment_status != 'free' AND account_type != 'free')
				# Member had not given payment details on given date
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
				# Member had payment status 'cancelled'
				AND
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date <= ?
					AND (msld.payment_status = 'cancelled' OR msld.payment_status = 'signup_expired')
					ORDER BY msld.date DESC
					LIMIT 1
				) IS NOT NULL 
				# Member had not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND p.amount > 0
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);
		$date_processing_end = static::getProcessingEndDate($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date, $date_processing_end, $date_processing_end));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getNoCcFailedPayment($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT count(m.id) AS total 
			FROM member m
			INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
			WHERE 
				# Member signedup on given date
				DATE(m.created_at) = ? 
				# Member does not have payment_status or account type 'free'
				AND (payment_status != 'free' AND account_type != 'free')
				# Member had not given payment details on given date
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					LIMIT 1
				) IS NULL
				# Member had payment status 'payment_failed'
				AND
				(
					SELECT msld.member_id 
					FROM member_status_log_daily msld 
					WHERE m.id = msld.member_id 
					AND msld.date <= ?
					AND msld.payment_status = 'payment_failed'
					ORDER BY msld.date DESC
					LIMIT 1
				) IS NOT NULL 
				# Member has not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= ?
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";
		
		$date = self::getDateString($date);
		$date_processing_end = static::getProcessingEndDate($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date, $date_processing_end, $date_processing_end));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected revenue for members who signed up on given date with cc
	 */
	protected static function getCcRevenueProjectedMax($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$date = self::getDateString($date);
		
		$live_query = static::isLiveQuery($date);
		
		if ($live_query) {
			$sql = "
				SELECT COALESCE(SUM(m.price), 0) AS total
				FROM member m
				INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
				WHERE
					# Member signedup on given date
					DATE(m.created_at) = ? 
					# Member does not have payment_status or account type 'free'
					AND (payment_status != 'free' AND account_type != 'free')
					# Member had given payment details
					AND
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) <= ?
						LIMIT 1
					) IS NOT NULL
			";
			$query_result = Member::find_by_sql($sql, array($date, $date));
		} else {
			$sql = "
				SELECT COALESCE(SUM(msld.price), 0) AS total
				FROM member_status_log_daily msld 
				WHERE msld.member_id IN (
					SELECT m.id
					FROM member m
					INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
					WHERE
						# Member signedup on given date
						DATE(m.created_at) = ? 
						# Member does not have payment_status or account type 'free'
						AND (payment_status != 'free' AND account_type != 'free')
						# Member had given payment details
						AND
						(
							SELECT p.id
							FROM payment p
							WHERE p.member_id = m.id 
							AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
							AND DATE(p.created_at) <= ?
							LIMIT 1
						) IS NOT NULL
				)
				AND msld.date = ?
			";
			$query_result = Member::find_by_sql($sql, array($date, $date, $date));
		}

		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected revenue for members who signed up on given date
	 */
	protected static function getNoCcRevenueProjectedMax($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$date = self::getDateString($date);
		
		$live_query = static::isLiveQuery($date);
		
		if ($live_query) {
			$sql = "
				SELECT COALESCE(SUM(m.price), 0) AS total
				FROM member m
				INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
				WHERE
					# Member signedup on given date
					DATE(m.created_at) = ? 
					# Member does not have payment_status or account type 'free'
					AND (payment_status != 'free' AND account_type != 'free')
					# Member had not given payment details on given date
					AND
					(
						SELECT p.id
						FROM payment p
						WHERE p.member_id = m.id 
						AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
						AND DATE(p.created_at) <= ?
						LIMIT 1
					) IS NULL
			";
			$query_result = Member::find_by_sql($sql, array($date, $date));
		} else {
			$sql = "
				SELECT COALESCE(SUM(msld.price), 0) AS total
				FROM member_status_log_daily msld 
				WHERE msld.member_id IN (
					SELECT m.id
					FROM member m
					INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
					WHERE
						# Member signedup on given date
						DATE(m.created_at) = ? 
						# Member does not have payment_status or account type 'free'
						AND (payment_status != 'free' AND account_type != 'free')
						# Member had not given payment details on given date
						AND
						(
							SELECT p.id
							FROM payment p
							WHERE p.member_id = m.id 
							AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
							AND DATE(p.created_at) <= ?
							LIMIT 1
						) IS NULL
				)
				AND msld.date = ?
			";
			$query_result = Member::find_by_sql($sql, array($date, $date, $date));
		}

		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected revenue for members who signed up on given date
	 */
	protected static function getCcRevenueProjected($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$convRateDate = static::getDateString($date);
		$convRateDate = static::isTrialOver($date) ? $date : static::getDateString(' -' . static::TRIAL_DAYS . 'days');
		$convRate = static::getCcAverageConversionRate($convRateDate);
		$convRate = !empty($convRate) ? $convRate : static::getCcAverageConversionRate($date);
		
		$result = static::getCcRevenueProjectedMax($date) * $convRate;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected revenue for members who signed up on given date
	 */
	protected static function getNoCcRevenueProjected($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$convRateDate = static::getDateString($date);
		$convRateDate = static::isTrialOver($date) ? $date : static::getDateString(' -' . static::TRIAL_DAYS . 'days');
		$convRate = static::getNoCcAverageConversionRate($convRateDate);
		$convRate = !empty($convRate) ? $convRate : static::getNoCcAverageConversionRate($date);
		
		$result = static::getNoCcRevenueProjectedMax($date) * $convRate;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected revenue for members who signed up on given date
	 */
	protected static function getRevenueProjected($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$date = self::getDateString($date);

		$ccProjected = static::getCcRevenueProjected($date);
		$noCcProjected = static::getNoCcRevenueProjected($date);
		$result = $ccProjected + $noCcProjected;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Actual revenue for members who signed up on given date
	 */
	protected static function getRevenueActual($date)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sql = "
			SELECT COALESCE(SUM(p.amount), 0) AS total
			FROM payment p
			WHERE p.member_id IN (
				SELECT m.id
				FROM member m
				INNER JOIN member_type mt ON m.type_id = mt.id AND mt.is_customer = 1
				WHERE
					# Member signedup on given date
					DATE(m.created_at) = ? 
					# Member does not have payment_status or account type 'free'
					AND (payment_status != 'free' AND account_type != 'free')
			)
			AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
			AND DATE(p.created_at) <= ?
			AND p.amount > 0
		";
		
		$date = self::getDateString($date);
		$date_processing_end = static::getProcessingEndDate($date);
		
		$query_result = Member::find_by_sql($sql, array($date, $date_processing_end));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	protected static function getDateString($date)
	{
		$date = ($date instanceof DateTime) ? $date->format('Y-m-d') : $date;
		$date = !empty($date) ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
		return $date;
	}
	
	protected static function getTrialEndUnixtime($signup_date)
	{
		$signup_date = static::getDateString($signup_date);
		$trial_end = strtotime($signup_date . ' +' . static::TRIAL_DAYS . ' days');
		return $trial_end;
	}
	
	protected static function getTrialEndDate($signup_date)
	{
		$signup_date = static::getDateString($signup_date);
		$trial_end_date = date('Y-m-d', static::getTrialEndUnixtime($signup_date));
		return $trial_end_date;
	}
	
	protected static function getProcessingEndDate($signup_date)
	{
		$trial_end_date = static::getTrialEndDate($signup_date);
		$end_date = date('Y-m-d', strtotime($trial_end_date . ' +15 days'));
		return $end_date;
	}
	
	protected static function isTrialOver($signup_date)
	{
		$signup_date = static::getDateString($signup_date);
		$trial_end_time = static::getTrialEndUnixtime($signup_date);
		$start_of_day = strtotime(date('Y-m-d'));
		$result = $trial_end_time <= $start_of_day;
		return $result;
	}
	
	protected static function isLiveQuery($date)
	{
		// If the date given is after the start of the current day its a live query
		$result = strtotime($date) >= strtotime(date('Y-m-d'));
		return $result;
	}
}