<?php
/**
 * External Content Blog Tier.
 *
 * @author konstv
 * @date: 2013nov26
 */
class ExternalcontentBlogTier {
	const Any = 0;
	const Tier_1 = 1;
	const Tier_2 = 2;
	const Tier_3 = 3;

	public static function getAllIds() {
		return array(self::Tier_1, self::Tier_2, self::Tier_3);
	}

	/**
	 * @param bool $isFullName Full or short name
	 * @param bool $isAddHintMinMax If to add a hint: min / max
	 * @return ExternalcontentBlogTier[]|string[]
	 */
	public static function getAllWithNames($isFullName, $isAddHintMinMax) {
		$ids = self::getAllIds();
		$res = array();
		foreach($ids as $id) {
			$name = null;
			if($isFullName) {
				$name = self::getName($id);
			} else {
				$name = $id;
			}
			$res[$id] = $name;
		}

		if ($isAddHintMinMax) {
			self::addHintMinMax($res);
		}
		return $res;
	}

	/**
	 * @param ExternalcontentBlogTier|int $id
	 * @return string
	 */
	public static function getName($id) {
		return 'Tier ' . $id;
	}

	/**
	 * @param ExternalcontentBlogTier[]|string[] $tiers
	 */
	protected static function addHintMinMax(&$tiers) {
		$tiers[self::Tier_1] .= ' [max]';
	}
}