<?php
class Administrator extends ActiveRecord\Model
{
	static $table = 'administrator';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);

	public function isSuperadmin()
	{
		if($this->type == 'superadmin') return true;
		else return false;
	}

	public function isSalesadmin()
	{
		if($this->type == 'salesadmin') return true;
		else return false;
	}
}
?>