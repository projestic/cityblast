<?php

class Testimonial extends ActiveRecord\Model
{

	static $table = 'testimonial';


	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
	);

}
