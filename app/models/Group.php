<?php
class Group extends ActiveRecord\Model
{
	static $table = 'group';


	static $has_many = array(
		array('cities', 'class' => 'GroupCity', 'foreign_key' => 'group_id')		
	);
}

?>