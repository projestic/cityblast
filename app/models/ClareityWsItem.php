<?php

class ClareityWsItem extends ActiveRecord\Model
{
	static $table = 'clareity_ws_item';
	
	static $belongs_to = array(
		array('member_type', 'class' => 'MemberType', 'foreign_key' => 'member_type_id'),
	);
}