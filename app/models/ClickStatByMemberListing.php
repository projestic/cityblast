<?php

class ClickStatByMemberListing extends ActiveRecord\Model
{
	static $table = 'click_stat_by_member_listing';
	
	public function populate()
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		Logger::log(
			'Compiling click stat by member and listing', 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT 
				member_id,
				listing_id,
				SUM(count) 
			FROM `click_stat` 
			GROUP BY member_id, listing_id";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}
