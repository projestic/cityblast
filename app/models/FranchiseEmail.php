<?php
class FranchiseEmail extends ActiveRecord\Model
{
	static $table = 'franchise_email_root';

	static $belongs_to = array(
		array('franchise', 'class' => 'Franchise', 'foreign_key' => 'franchise_id')
	);


}