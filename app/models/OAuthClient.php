<?php
class OAuthClient extends ActiveRecord\Model
{
	static $table = 'oauth_clients';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
	);

	public function hasTokens() {
		return (bool) $this->countReferredMembers();
	}
	
	public function countTokens() {
		return OAuthAccessToken::count( array('conditions' => array('client_id' => $this->id)) ) + OAuthAuthorizeToken::count( array('conditions' => array('client_id' => $this->id)) );
	}
	
	public static function generateCredentials() {

		$fp = fopen('/dev/urandom','rb');
		$entropy = fread($fp, 32);
		fclose($fp);
	
		$entropy .= uniqid(mt_rand(), true);
		$hash 	= hash('sha512', $entropy);
		//$hash     = gmp_strval(gmp_init($hash, 16), 62);		//function not found?

		return array(
			'client_id' => substr($hash, 0, 32),
			'client_secret' => substr($hash, 32, 48)
		);
	
	}

}
