<?php

class SAMLLogin extends ActiveRecord\Model
{
	static $table = 'saml_login';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);

	public function authenticationURL() {
		// just a workaround for old metadata loaded in clareity QA store
		$idp = $this->idp;
		if ($idp == 'clareity') $idp = 'clareity-qa';
		if ($idp == 'store1')   $idp = 'clareity-qa';
		return '/sso/saml2/' . $idp;
	}
	
}

?>