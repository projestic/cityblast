<?php

class EventLog extends ActiveRecord\Model
{
	static $table = 'event_log';
	
	const ENTITY_TYPE_MEMBER = 'member';
	const ENTITY_TYPE_LISTING = 'listing';
	
	/**
	 * Create new log entry
	 * 
	 * @param string $entityType Entity type identifier - see ENTITY_TYPE_* constants
	 * @param string $eventType Event type identifier 
	 * @param string $desc Desription of action 
	 * @param int $entityId 
	 * @param int $entityMemberId Id of the member who own the entity
	 * @param mixed $newVal Modified value - this can be a string or an array
	 * @param mixed $oldVal Original value - this can be a string or an array
	 * @param int $actorMemberId Id of the member who performed the action
	 * @return EventLog
	 */
	public static function create($entityType, $eventType, $desc, $entityId = 0, $entityMemberId = 0, $newVal = null, $oldVal = null, $actorMemberId =  null)
	{
		$actorMemberId = ($actorMemberId === null && isset($_SESSION['member'])) ? $_SESSION['member']->id : $actorMemberId;
		
		$newVal = self::clean($newVal);
		$oldVal = self::clean($oldVal);
		
		$log = new EventLog();
		$log->entity_member_id = ($entityMemberId) ? $entityMemberId : 0;
		$log->actor_member_id = ($actorMemberId) ? $actorMemberId : 0;
		$log->entity_type = $entityType;
		$log->entity_id = ($entityId) ? $entityId : 0;
		$log->event_type = $eventType;
		$log->description = $desc;
		$log->value_old = !empty($oldVal) ? json_encode(self::getModifiedDataOld($newVal, $oldVal)) : '';
		$log->value_new = !empty($newVal) ? json_encode(self::getModifiedDataNew($newVal, $oldVal)) : '';
		$log->save();
		
		return $log;
	}
	
	/**
	 * Get modified old data
	 * 
	 * @param mixed $newVal
	 * @param mixed $oldVal
	 * @param mixed
	 */
	private static function getModifiedDataOld($newVal = null, $oldVal = null)
	{
		if (!is_array($newVal) || !is_array($oldVal)) {
			return $oldVal;
		}
		
		$data = array();
		foreach ($oldVal as $key => $value) {
			$newValue = isset($newVal[$key]) ? $newVal[$key] : null;
			switch (true) {
				case !isset($newVal[$key]): // Value was removed
				case ($newValue != $value): // Value changed
					$data[$key] = self::getModifiedDataOld($newValue, $value);
				break;
			}
		}
		
		return $data;
	}
	
	/**
	 * Get modified new data
	 * 
	 * @param mixed $newVal
	 * @param mixed $oldVal
	 * @param mixed
	 */
	private static function getModifiedDataNew($newVal = null, $oldVal = null)
	{
		if (!is_array($newVal) || !is_array($oldVal)) {
			return $newVal;
		}
		
		$data = array();
		foreach ($newVal as $key => $value) {
			$oldValue = isset($oldVal[$key]) ? $oldVal[$key] : null;
			switch (true) {
				case !isset($oldVal[$key]): // Value was added
				case ($oldValue != $value): // Value changed
					$data[$key] = self::getModifiedDataNew($value, $oldValue);
				break;
			}
		}
		
		return $data;
	}
	
	/**
	 * Clean data ready for DB storage
	 * 
	 * @param mixed $data May be a String, Array or ActiveRecord\Model instance
	 */
	private static function clean($data)
	{
		if (!$data || (!is_array($data) && !is_object($data))) {
			return $data;
		}
		
		$data = ($data instanceof ActiveRecord\Model) ? $data->attributes() : $data;
		$newData = array();
		if (!empty($data) && is_array($data)) {
			foreach ($data as $name => $value) {
				if ($name == 'id') {
					continue; // Ids should never change
				}
				if ($value instanceof DateTime) {
					$newData[$name] = $value->format('Y-m-d H:i:s');
				} else {
					$newData[$name] = trim($value);
				}
			}
		}
		return $newData;

	}
	
	public function getOldValue()
	{
		return ($this->value_old) ? json_decode($this->value_old, true) : null;
	}
	
	public function getNewValue()
	{
		return ($this->value_new) ? json_decode($this->value_new, true) : null;
	}
}