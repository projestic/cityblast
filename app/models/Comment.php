<?php
class Comment extends ActiveRecord\Model
{
	static $table = 'comments';


	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id'),
		array('sales_agent', 'class' => 'Member', 'foreign_key' => 'mid')
	);

}
?>
