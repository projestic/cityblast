<?php

class ListingStats
{
	public static function getNewManualForDateRange($start_date, $end_date)
	{
		$stats = array();
		
		static::iterateDateRange($start_date, $end_date, function($date) use (&$stats) {
			$stats[$date] = ListingStats::getNewManualByDate($date);
		});
		
		return $stats;
	}
	
	public static function getNewManualByTagIdForDateRange($tag_id, $start_date, $end_date)
	{
		$stats = array();
		
		static::iterateDateRange($start_date, $end_date, function($date) use (&$stats, $tag_id) {
			$stats[$date] = ListingStats::getNewManualByTagIdAndDate($tag_id, $date);
		});
		
		return $stats;
	}

	/**
	 * Get new listings by city in date range.
	 *
	 * @param $city_id
	 * @param $start_date
	 * @param $end_date
	 * @return array
	 */
	public static function getNewManualByCityIdForDateRange($city_id, $start_date, $end_date)
	{
		$stats = array();
		static::iterateDateRange($start_date, $end_date, function($date) use (&$stats, $city_id) {
			$stats[$date] = ListingStats::getNewManualByCityIdAndDate($city_id, $date);
		});
		return $stats;
	}
	
	public static function getNewManualForAllTagsForDateRange($start_date, $end_date)
	{
		$tags = Tag::all();

		$stats = array();
		
		foreach ($tags as $tag) {
			$tag_stats = static::getNewManualByTagIdForDateRange($tag->id, $start_date, $end_date);
			$stats[$tag->id]['name'] = $tag->name;
			$stats[$tag->id]['stats'] = $tag_stats;
		}
		
		return $stats;
	}

	/**
	 * Get new manual listings by city for all cities in date range.
	 *
	 * @param $start_date
	 * @param $end_date
	 * @return array
	 */
	public static function getNewManualForAllCitiesForDateRange($start_date, $end_date)
	{
		$cities = City::all(array('order' => 'name asc'));
		$stats = array();
		foreach ($cities as $city) {
			$city_stats = static::getNewManualByCityIdForDateRange($city->id, $start_date, $end_date);
			$stats[$city->id]['name'] = $city->name;
			$stats[$city->id]['stats'] = $city_stats;
		}
		return $stats;
	}

	public static function iterateDateRange($start_date, $end_date, $callback)
	{
		$start_date = static::getDateString($start_date);
		$end_date = static::getDateString($end_date);
		
		$start_time = strtotime($start_date);
		$end_time = strtotime($end_date);
		
		$time_span = $end_time - $start_time;
		$time_span_days = $time_span / 86400;
		
		if ($time_span_days > 365) {
			throw new Exception('Time span is to large');
		}
		
		$current_time = $start_time;
		while ($current_time <= $end_time){
			$current_date = date('Y-m-d', $current_time);
			$callback($current_date);
			$current_time = strtotime($current_date . ' +1 day');
		}
	}

	public static function getNewManualByDate($date = null)
	{
		$sql = "DATE(created_at) = ? AND blast_type = '".Listing::TYPE_CONTENT."'";
		
		$conditions = array(
			$sql,
			static::getDateString($date)
		);
		
		$result = Listing::count(array('conditions' => $conditions));
		
		return $result;
	}

	public static function getNewManualByTagIdAndDate($tagId, $date = null)
	{
		$sql = "DATE(listing.created_at) = ? AND tag_cloud.tag_id = ? AND blast_type = '".Listing::TYPE_CONTENT."'";

		$conditions = array(
			$sql,
			static::getDateString($date),
			$tagId
		);
		
		$sql_joins = "INNER JOIN tag_cloud ON listing.id = tag_cloud.listing_id";
		
		$ar_opts = array(
			'conditions' => $conditions,
			'joins' => $sql_joins
		);
		
		$result = Listing::count($ar_opts);
		
		return $result;
	}

	public static function getNewManualByCityIdAndDate($city_id, $date = null)
	{
		$sql = "DATE(listing.created_at) = ? AND listing.city_id = ? AND blast_type = '".Listing::TYPE_CONTENT."'";

		$conditions = array(
			$sql,
			static::getDateString($date),
			$city_id
		);

		$ar_opts = array(
			'conditions' => $conditions
		);

		$result = Listing::count($ar_opts);

		return $result;
	}

	protected static function getDateString($date)
	{
		$date = ($date instanceof DateTime) ? $date->format('Y-m-d') : $date;
		$date = !empty($date) ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
		return $date;
	}
}
