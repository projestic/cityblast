<?php

class EngageByYearMember extends ActiveRecord\Model
{
	static $table = 'engage_by_year_member';
	
	public function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$start_date = date('Y-01-01', $start_time);
		$end_date = date('Y-12-t', $end_time);
		
		Logger::log(
			'Compiling engagement by year and member between ' . $start_date . ' and ' . $end_date, 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT 
				DATE_FORMAT(date, '%Y-01-01') AS year_date, 
				member_id,
				DATE_FORMAT(date, '%Y') AS year,
				SUM(clicks),
				SUM(likes),
				SUM(comments),
				SUM(shares),
				0,
				MAX(posts),
				0,
				0,
				0,
				0,
				0
			FROM `engage_by_date_member` 
			WHERE date >= " . $db_con->escape($start_date) . "  
			AND date <= " . $db_con->escape($end_date) . "  
			GROUP BY year_date, member_id
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		$sql = "
			UPDATE `engage_by_year_member` 
			SET 
				score = (clicks + (likes * 10) + (comments * 15) + (shares * 25))
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		$sql = "
			UPDATE `engage_by_year_member` 
			SET 
				clicks_perpost = ROUND(clicks / posts, 3),
				likes_perpost = ROUND(likes / posts, 3),
				comments_perpost = ROUND(comments / posts, 3),
				shares_perpost = ROUND(shares / posts, 3),
				score_perpost = ROUND(score / posts, 3)
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}