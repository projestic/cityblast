<?php

class MemberStatsFinancial 
{
	
	/**
	 * Get actual revenue for the current calendar month
	 * 
	 * @return float
	 */
	public static function getActualRevenueCurrentMonth()
	{

		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT SUM(p.amount) AS total
			FROM payment p 
			WHERE DATE(p.created_at) >= DATE_FORMAT(NOW() ,'%Y-%m-01')
			AND DATE(p.created_at) <= DATE(NOW())
		";

		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;

	}
	
	/**
	 * Get actual revenue for the current year (since Jan 1st)
	 * 
	 * @return float
	 */
	public static function getActualRevenueAnnual()
	{

		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT SUM(p.amount) AS total
			FROM payment p 
			WHERE DATE(p.created_at) >= DATE_FORMAT(NOW() ,'%Y-01-01')
			AND DATE(p.created_at) <= DATE(NOW())
		";

		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get recieved revenue for the current calendar month
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getReceivedRevenueCurrentMonth($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sub_select = "
			SELECT m.id
			FROM member m
			WHERE 
				# Member has made an actual payment
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sub_select .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}

		$sql = "
			SELECT SUM(p.amount) AS total
			FROM payment p 
			WHERE DATE(p.created_at) >= DATE_FORMAT(NOW() ,'%Y-%m-01')
			AND DATE(p.created_at) <= DATE(NOW())
			AND p.member_id IN (
			" . $sub_select . "
			)
		";

		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get recieved revenue today
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getReceivedRevenueToday($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$sub_select = "
			SELECT m.id
			FROM member m
			WHERE 
				# Member has made an actual payment
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sub_select .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}

		$sql = "
			SELECT SUM(p.amount) AS total
			FROM payment p 
			WHERE DATE(p.created_at) = DATE(NOW())
			AND p.member_id IN (
			" . $sub_select . "
			)
		";

		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get expected revenue for the current calendar month
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getExpectedRevenueCurrentMonth($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$result = self::getExpectedRevenueCurrentMonthPaidMember($billing_cycle) + 
				  self::getExpectedRevenueCurrentMonthCcTrialMember($billing_cycle);
				 
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get expected (not yet recieved) paid member revenue for the current calendar month
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getExpectedRevenueCurrentMonthPaidMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT SUM(m.price) AS total 
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# next payment date is before end of month
				AND m.next_payment_date <= LAST_DAY(NOW())
				AND (
					# next payment date is today or after today
					m.next_payment_date >= DATE(NOW())
					OR
					(
						m.payment_status = 'payment_failed'
						# next payment date within past 15 days
						AND m.next_payment_date >= DATE_SUB(NOW(), INTERVAL 15 DAY)
						# next payment date after first day of month
						AND m.next_payment_date >= DATE_FORMAT(NOW() ,'%Y-%m-01')
					)
				)
				# Member has made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					
					LIMIT 1
				) IS NOT NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
			//if ($billing_cycle == 'annual') echo $sql;
		}
		
		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get expected (not yet recieved) CC trial member revenue for current calendar month
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getExpectedRevenueCurrentMonthCcTrialMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT SUM(m.price) AS total 
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# next payment date is before end of month
				AND m.next_payment_date <= LAST_DAY(NOW())
				AND (
					# next payment date is today or after today
					m.next_payment_date >= DATE(NOW())
					OR
					(
						m.payment_status = 'payment_failed'
						# next payment date within past 15 days
						AND m.next_payment_date >= DATE_SUB(NOW(), INTERVAL 15 DAY)
						# next payment date after first day of month
						AND m.next_payment_date >= DATE_FORMAT(NOW() ,'%Y-%m-01')
					)
				)
				# Member had given payment details 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount = 0
					LIMIT 1
				) IS NOT NULL
				# Member had not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}
		
		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get expected revenue for the next 30 days
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getExpectedRevenue30day($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$result = self::getExpectedRevenue30dayPaidMember($billing_cycle) + 
				  self::getExpectedRevenue30dayCcTrialMember($billing_cycle);
				 
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get expected (not yet recieved) paid member revenue for the next 30 days
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getExpectedRevenue30dayPaidMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$end_date = date('Y-m-d', strtotime('+30days'));

		$sql = "
			SELECT SUM(m.price) AS total 
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# next payment date is before end date
				AND m.next_payment_date <= ?
				AND (
					# next payment date is today or after today
					m.next_payment_date >= DATE(NOW())
					OR
					(
						m.payment_status = 'payment_failed'
						# next payment date within past 15 days
						AND m.next_payment_date >= DATE_SUB(NOW(), INTERVAL 15 DAY)
					)
				)
				# Member has made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
			//if ($billing_cycle == 'annual') echo $sql;
		}
		
		$query_result = Member::find_by_sql($sql, array($end_date));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get expected (not yet recieved) CC trial member revenue for next 30 days
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getExpectedRevenue30dayCcTrialMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$end_date = date('Y-m-d', strtotime('+30days'));

		$sql = "
			SELECT SUM(m.price) AS total 
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# next payment date is before end date
				AND m.next_payment_date <= ?
				AND (
					# next payment date is today or after today
					m.next_payment_date >= DATE(NOW())
					OR
					(
						m.payment_status = 'payment_failed'
						# next payment date within past 15 days
						AND m.next_payment_date >= DATE_SUB(NOW(), INTERVAL 15 DAY)
					)
				)
				# Member had given payment details 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount = 0
					LIMIT 1
				) IS NOT NULL
				# Member had not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}
		
		$query_result = Member::find_by_sql($sql, array($end_date));
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected (recieved + expected) revenue for the current calendar month
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedMonthlyRevenue($billing_cycle = null)
	{

		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = self::getExpectedRevenueCurrentMonth($billing_cycle) + 
				  self::getReceivedRevenueCurrentMonth($billing_cycle);
				  
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				 
		return $result;
	}
	
	/**
	 * Get projected (recieved + expected) paid member revenue for the current calendar month
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedMonthlyRevenuePaidMember($billing_cycle = null)
	{

		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = self::getExpectedRevenueCurrentMonthPaidMember($billing_cycle) + 
				  self::getReceivedRevenueCurrentMonth($billing_cycle);
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				 
		return $result;
	}
	
	/**
	 * Get projected (recieved + expected) revenue for the current calendar month
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedRevenue30day($billing_cycle = null)
	{

		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = self::getExpectedRevenue30day($billing_cycle) + 
				  self::getReceivedRevenueToday($billing_cycle);
				  
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				 
		return $result;
	}
	
	/**
	 * Get projected (recieved + expected) paid member revenue for the current calendar month
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedRevenue30dayPaidMember($billing_cycle = null)
	{

		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = self::getExpectedRevenue30dayPaidMember($billing_cycle) + 
				  self::getReceivedRevenueToday($billing_cycle);
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				 
		return $result;
	}
	
	
	/**
	 * Get projected monthly average revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getMemberCount($billing_cycle = null, $type = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = self::getPaidMemberCount($billing_cycle, $type) +
				self::getCcTrialMemberCount($billing_cycle, $type);
				
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				
		return $result;
	}
	
	/**
	 * Get projected monthly average paid member revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getPaidMemberCount($billing_cycle = null, $type = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT COUNT(m.id) AS total 
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# Member has made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		if(!empty($type))
		{ 
			$conn = Member::connection();
			$sql .= " AND type_id=".$conn->escape($type)." ";
		}
		
		
		if (!empty($billing_cycle)) 
		{
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}
		
		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected monthly average cc trial member revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getCcTrialMemberCount($billing_cycle = null, $type = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT COUNT(m.id) AS total
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# Member had given payment details 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount = 0
					LIMIT 1
				) IS NOT NULL
				# Member had not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";
		
		if(!empty($type))
		{ 
			$conn = Member::connection();
			$sql .= " AND type_id=".$conn->escape($type)." ";
		}
				
		if (!empty($billing_cycle)) 
		{
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}
		
		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected monthly average revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedMonthlyAveragePrice($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = round((self::getProjectedMonthlyAveragePricePaidMember($billing_cycle) +
				self::getProjectedMonthlyAveragePriceCcTrialMember($billing_cycle)) / 2, 2);
				
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				
		return $result;
	}
	
	/**
	 * Get projected monthly average paid member revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedMonthlyAveragePricePaidMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT (SUM(m.price_month) / COUNT(m.id)) AS total 
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# Member has made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}
		
		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected monthly average cc trial member revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedMonthlyAveragePriceCcTrialMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT (SUM(m.price_month) / COUNT(m.id)) AS total
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# Member had given payment details 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount = 0
					LIMIT 1
				) IS NOT NULL
				# Member had not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}
		
		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected monthly average revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedMonthlyAverageRevenue($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = self::getProjectedMonthlyAverageRevenuePaidMember($billing_cycle) +
				self::getProjectedMonthlyAverageRevenueCcTrialMember($billing_cycle);
				
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				
		return $result;
	}
	
	/**
	 * Get projected monthly average paid member revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedMonthlyAverageRevenuePaidMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT SUM(m.price_month) AS total 
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# Member has made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NOT NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}
		
		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	/**
	 * Get projected monthly average cc trial member revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedMonthlyAverageRevenueCcTrialMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);

		$sql = "
			SELECT SUM(m.price_month) AS total
			FROM member m
			WHERE 
				(m.payment_status = 'paid' OR m.payment_status = 'payment_failed')
				AND m.account_type = 'paid'
				AND m.status = 'active'
				# Member had given payment details 
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount = 0
					LIMIT 1
				) IS NOT NULL
				# Member had not made an actual payment
				AND
				(
					SELECT p.id
					FROM payment p
					WHERE p.member_id = m.id 
					AND p.paypal_confirmation_number != 'FREE_REFERRAL_CREDIT' 
					AND DATE(p.created_at) <= DATE(NOW())
					AND p.amount > 0
					LIMIT 1
				) IS NULL
		";
		
		if (!empty($billing_cycle)) {
			$conn = Member::connection();
			$sql .= "AND m.billing_cycle = " . $conn->escape($billing_cycle);
		}
		
		$query_result = Member::find_by_sql($sql);
		$result = !empty($query_result) ? $query_result[0]->total : 0;
		
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	
	/**
	 * Get projected annual revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedAnnualRevenue($billing_cycle = null)
	{

		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = self::getProjectedAnnualRevenuePaidMember($billing_cycle) +
				self::getProjectedAnnualRevenueCcTrialMember($billing_cycle);
				
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				
		return $result;
	}
	
	/**
	 * Get projected annual paid member revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedAnnualRevenuePaidMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = self::getProjectedMonthlyAverageRevenuePaidMember($billing_cycle) * 12;
				
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				
		return $result;
	}
	
	/**
	 * Get projected annual cc trial member revenue
	 * 
	 * @param string $billing_cycle
	 * @return float
	 */
	public static function getProjectedAnnualRevenueCcTrialMember($billing_cycle = null)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$result = self::getProjectedMonthlyAverageRevenueCcTrialMember($billing_cycle) * 12;
				
		Logger::log($result, Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
				
		return $result;
	}
}