<?php
class Payable extends ActiveRecord\Model
{
	static $table = 'payable';
	//static $pk    = 'track_id';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('affiliate', 'class' => 'Affiliate', 'foreign_key' => 'affiliate_id'),
		array('payment', 'class' => 'Payment', 'foreign_key' => 'member_payment_id')
	);

	public static function factory($member, $payment) 
	{
	
		if (($member->referring_affiliate instanceOf Affiliate) === false) return false;

		$previous_payments = Payment::count(array("conditions" => array("member_id = ? and id != ? and payment_type='clientfinder' and amount > 0", $member->id, $payment->id)));
		
		$payout = 0;
		
		if ($previous_payments == 0 && $member->referring_affiliate->first_payout_percent > 0) {
			$payout = $member->getFirstPayout();
		} elseif ($member->referring_affiliate->recurring_payout_percent > 0) {
			$payout = $member->getRecurringPayout();
		};
		
		if ($payout == 0) return false;
		
		$payable = new Payable();
		
		$payable->member_id			= $member->id;
		$payable->affiliate_id 		= $member->referring_affiliate->id;
		$payable->amount 			= $payout;
		$payable->member_payment_id = $payment->id;
		
		if (($override_affiliate = $member->referring_affiliate->override_affiliate) && $override_affiliate->override_payout_percent) {
			$override = new Payable();
			$override->member_id		 = $member->id;
			$override->affiliate_id 	 = $member->referring_affiliate->override_affiliate->id;
			$override->member_payment_id = $payment->id;
			$override->amount 			 = round($payment->amount * $override_affiliate->override_payout_percent / 100, 2);
			$override->save();
			$payable->override_id	  	 = $override->id;
		}

		return $payable;
	}
}