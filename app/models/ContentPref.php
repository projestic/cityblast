<?php

class ContentPref extends ActiveRecord\Model
{
	static $table = 'content_pref';
	
	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('tag', 'class' => 'Tag', 'foreign_key' => 'tag_id')
	);
	
	/**
	 * Get member content preferences as Tag
	 * 
	 * @param int $member_id
	 * @return array An array of Tag object with a 'pref' attribute
	 */
	public static function findAllForMemberAsTag($member_id)
	{
		$sql = 
			"SELECT " . 
				"t.id, " . 
				"t.ref_code, " . 
				"t.name, t.tag_group_id, " . 
				"t.publishable, " .
				"tmts.default_value, " . 
				"cp.pref, " . 
				"(cp.member_id IS NOT NULL) AS pref_set " . 
			"FROM tag t ". 
			"INNER JOIN tag_member_type_setting tmts ON tmts.tag_id = t.id " . 
				"AND tmts.member_type_id = (SELECT type_id FROM member WHERE id = ?) ".
				"AND tmts.available = 1 ".
			"LEFT JOIN content_pref cp ON t.id = cp.tag_id AND cp.member_id = ? " . 
			"GROUP BY t.id";


		$tags = Tag::find_by_sql($sql, array($member_id, $member_id));
		
		if (!empty($tags)) {
			foreach ($tags as $tag) {
				if (!$tag->pref_set) {
					$tag->pref = $tag->default_value;
				}
			}
		}
		
		return $tags;
	}
	
	/**
	 * Get member content preference as Tag by member id and tag reference code
	 * 
	 * @param int $member_id
	 * @param string $tag_ref_code
	 * @return Tag Tag object with a 'pref' attribute
	 */
	public static function findForMemberAsTagByTagRefCode($member_id, $tag_ref_code)
	{
		$sql = 
			"SELECT " . 
				"t.id, " . 
				"t.ref_code, " . 
				"t.name, t.tag_group_id, " . 
				"t.publishable, " .
				"tmts.default_value, " . 
				"cp.pref, " . 
				"(cp.member_id IS NOT NULL) AS pref_set " . 
			"FROM tag t ". 
			"INNER JOIN tag_member_type_setting tmts ON tmts.tag_id = t.id " . 
				"AND tmts.member_type_id = (SELECT type_id FROM member WHERE id = ?) ".
				"AND tmts.available = 1 ".
			"LEFT JOIN content_pref cp ON t.id = cp.tag_id AND cp.member_id = ? " . 
			"WHERE t.ref_code = ? " .
			"GROUP BY t.id";
		
		$tags = Tag::find_by_sql($sql, array($member_id, $member_id, $tag_ref_code));
		
		$tag = null;
		if (!empty($tags)) {
			$tag = $tags[0];
			if (!$tag->pref_set) {
				$tag->pref = $tag->default_value;
			}
		}
		
		return $tag;
	}
}
