<?php
class CashTrans extends ActiveRecord\Model
{
	const TYPE_CREDIT = 'CREDIT';
	const TYPE_DEBIT = 'DEBIT';
	
	static $table = 'cash_trans';
	
	public function getCreditAmount()
	{
		return ($this->type == self::TYPE_CREDIT) ? $this->amount : 0;
	}
	
	public function getDebitAmount()
	{
		return ($this->type == self::TYPE_DEBIT) ? $this->amount : 0;
	}
	
	public function isCredit()
	{
		return ($this->type == self::TYPE_CREDIT);
	}
	
	public function isDebit()
	{
		return ($this->type == self::TYPE_DEBIT);
	}
	
	/**
	 * Get the balance as it was before this transaction
	 */
	public function getPreviousBalance()
	{
		$sql = "SELECT balance FROM cash_trans WHERE member_id = '" . addslashes($this->member_id) . "' AND id < '" . $this->id . "' ORDER BY id DESC LIMIT 1";
		$previous = CashTrans::find_by_sql($sql);
		$previous = $previous ? array_shift($previous) : null;
		return ($previous) ? $previous->balance : 0;
	}
	
	/**
	 * Recalculate and set balance to prevent issues from race conditions
	 */
	public function resetBalance()
	{
		switch ($this->type) {
			case self::TYPE_CREDIT:
				$this->balance = $this->getPreviousBalance() + $this->amount;
				$this->save();
			break;
			case self::TYPE_DEBIT:
				$this->balance = $this->getPreviousBalance() - $this->amount;
				$this->save();
			break;
		}
	}
	
	public static function getMemberBalance($member_id)
	{
		$sql = "SELECT balance FROM cash_trans WHERE member_id = '" . addslashes($member_id) . "' ORDER BY id DESC LIMIT 1";
		$previous = CashTrans::find_by_sql($sql);
		$previous = $previous ? array_shift($previous) : null;
		return ($previous) ? $previous->balance : 0;
	}

	/**
	 * Credit customer.
	 *
	 * @param $member_id
	 * @param $amount
	 * @param $note
	 * @param null $created_by
	 * @return CashTrans
	 * @throws Exception
	 */
	public static function newCredit($member_id, $amount, $note, $created_by = null)
	{
		if (empty($amount)) {
			throw new Exception('You must specify a credit amount');
		}
		
		if ($amount < 0) {
			throw new Exception('Credit amount must not be negative');
		}
		
		if (empty($note)) {
			throw new Exception('You must provide a note');
		}
		
		$created_by = empty($created_by) && !empty($_SESSION['member']) ? $_SESSION['member']->id : $created_by; 
		
		$balance = CashTrans::getMemberBalance($member_id) + $amount;
		
		$cash_trans = new CashTrans();
		$cash_trans->type = CashTrans::TYPE_CREDIT;
		$cash_trans->member_id = $member_id;
		$cash_trans->payment_id = 0;
		$cash_trans->created_by = ($created_by) ? $created_by : 0;
		$cash_trans->amount = $amount;
		$cash_trans->balance = $balance;
		$cash_trans->note = $note;
		$cash_trans->created_at = date('Y-m-d H:i:s');
		$cash_trans->save();
		
		$cash_trans->resetBalance();
		
		return $cash_trans;
	}
	
	public static function newDebit($member_id, $amount, $note, $created_by = null, $payment_id = null)
	{
		if (empty($amount)) {
			throw new Exception('You must specify a debit amount');
		}
		
		if ($amount < 0) {
			throw new Exception('Debit amount must not be negative');
		}
		
		if (empty($note)) {
			throw new Exception('You must provide a note');
		}
		
		$created_by = empty($created_by) && !empty($_SESSION['member']) ? $_SESSION['member']->id : $created_by; 
		
		$blanace = CashTrans::getMemberBalance($member_id) - $amount;
		
		$cash_trans = new CashTrans();
		$cash_trans->type = CashTrans::TYPE_DEBIT;
		$cash_trans->member_id = $member_id;
		$cash_trans->payment_id = ($payment_id) ? $payment_id : 0;
		$cash_trans->created_by = ($created_by) ? $created_by : 0;
		$cash_trans->amount = $amount;
		$cash_trans->balance = $blanace;
		$cash_trans->note = $note;
		$cash_trans->created_at = date('Y-m-d H:i:s');
		$cash_trans->save();
		
		$cash_trans->resetBalance();
		
		return $cash_trans;
	}
}