<?php

class ListingTopByWeek extends ActiveRecord\Model
{
	static $table = 'listing_top_by_week';
	
	public static function findContent($date = null)
	{
		$timestamp = static::timeFirstOfWeek($date);
		$date = date('Y-m-d', $timestamp);
		$year = date('Y', $timestamp);
		$week = date('W', $timestamp);
			
		$contents = EngageByWeekListing::find('all', array(
			'select' => "
				engage_by_week_listing.date,
				engage_by_week_listing.year,
				engage_by_week_listing.week,
				engage_by_week_listing.listing_id,
				engage_by_week_listing.score_perpost
			",
			'joins' => "
				INNER JOIN listing ON  listing.id = engage_by_week_listing.listing_id AND status = 'active'
				INNER JOIN publishing_queue ON publishing_queue.listing_id = engage_by_week_listing.listing_id
				LEFT JOIN listing_top_by_week ON listing_top_by_week.listing_id = engage_by_week_listing.listing_id
			",
			'conditions' => array("
				listing.status = 'active' AND
				# hasnt previously been selected
				listing_top_by_week.listing_id IS NULL AND
				engage_by_week_listing.year = ? AND 
				engage_by_week_listing.week = ? AND 
				listing.blast_type = ? AND
				publishing_queue.hopper_id <= 3
				",
				$year,
				$week,
				Listing::TYPE_CONTENT
			),
			'order' => "engage_by_week_listing.score_perpost DESC, listing.created_at ASC",
			'group' => 'engage_by_week_listing.listing_id',
			'limit' => 3
		));
		
		return $contents;
	}
	
	public static function getContent($date = null)
	{
		$timestamp = static::timeFirstOfWeek($date);
		$date = date('Y-m-d', $timestamp);
		
		$contents = static::find_all_by_date($date, array('order' => 'score_perpost DESC'));
		
		if (empty($contents)) {
			$contents = static::populateContent($date);
		}
		
		return $contents;
	}
	
	public static function populateContent($date = null)
	{
		$timestamp = static::timeFirstOfWeek($date);
		$date = date('Y-m-d', $timestamp);
		$year = date('Y', $timestamp);
		$week = date('W', $timestamp);
		
		$contents = static::findContent($date);
		if (!empty($contents)) {
			foreach ($contents as $content) {
				$top = new ListingTopByWeek();
				$top->date = $date;
				$top->listing_id = $content->listing_id;
				$top->year = $year;
				$top->week = $week;
				$top->score_perpost = $content->score_perpost;
				$top->save();
			}
		}
		return $contents;
	}
	
	private static function timeFirstOfWeek($date = null)
	{
		$date = ($date instanceof DateTime) ? $date->format('Y-m-d') : $date;
		$time = !empty($date) ? strtotime($date) : time();
		$time = (date('N', $time) != 1) ? strtotime('last monday', $time) : $time;
		return $time;
	}
}