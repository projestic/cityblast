<?php


class PaymentGateway extends ActiveRecord\Model
{
	static $table = 'payment_gateway';
	
	public function processRecurringPayment(Member $member, $automated = true)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		if (!$this->local_processing) {
			Logger::log('Payments of type "' . $this->code . '" are not processed  locally');
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return true;
		}
		
		if ($member->next_payment_date > new DateTime()) {
			Logger::log('Payment not due until: ' . $member->next_payment_date->format('Y-m-d'));
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return false;
		}
		
		Logger::log('Loading previous payment details', Logger::LOG_TYPE_DEBUG);
		$last_payment   =	Payment::find(array('conditions' => 'member_id='.$member->id.' and payment_type="clientfinder"', 'limit' => 1, 'order'=>'created_at desc, id desc'));
		$params = array();
		
		if ($last_payment)
		{
			Logger::log('Found previous payment details', Logger::LOG_TYPE_DEBUG);
			Logger::log('Payment id: ' . $last_payment->id, Logger::LOG_TYPE_DEBUG);
			Logger::log('Payment created: ' . $last_payment->created_at->format('Y-m-d H:i:s'), Logger::LOG_TYPE_DEBUG);
			$params = array(
				'fname'		=> $last_payment->first_name,
				'lname'		=> $last_payment->last_name,
				'address'	=> $last_payment->address1,
				'city'		=> $last_payment->city,
				'state'		=> $last_payment->state,
				'cc_country'=> $last_payment->country,
				'zip'		=> $last_payment->zip,
				'phone'		=> $last_payment->phone,
			);
		} else {
			Logger::log('Failed to find previous payment details', Logger::LOG_TYPE_DEBUG);
		}
		
		Logger::log('Processing payment', Logger::LOG_TYPE_DEBUG);
		if ($this->processPayment($member, $params))
		{
			$member->update_account_status();
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return true;
		}
		else
		{	
			Logger::log('Processing payment failed', Logger::LOG_TYPE_DEBUG, true);
			if ($automated) {
				// We only handle payment failures of automated payments
				// - manually triggered payments (by an admin) should not result in an email notification
				// - and should not register as an actual payment failure
				$member->billing_failures++;
				$member->billing_attempt_fail	= date("Y-m-d H:i:s");
				$member->payment_status 		= "payment_failed";
				
				if ($member->billing_failures == 3) {
					$note = new Note();
					$note->note = 'Automatically moved to Cancelled status after 3 failed payment attempts';
					$note->member_id = $this->id;
					$note->admin_id = 0;
					$note->save();
					
					$member->payment_status = 'cancelled';
				}			
				$member->save();
				$this->notifyPaymentFailed($member);
			}
			Logger::endSection(Logger::LOG_TYPE_DEBUG);
			return false;
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
	public function makePayment(Member $member, $params)
	{
		$result = false;
		switch ($this->code) {
			case 'STRIPE':
				$result = $this->makePaymentStripe($member, $params);
			break;
			case 'PAYPAL':
				throw new Exception('Paypal payments are no longer supported');
			break;
		}
		
		return $result;
	}
	
	public function makePaymentStripe(Member $member, $params)
	{
		require_once(APPLICATION_PATH . '/../lib/stripe_lib/Stripe.php'); 
		Stripe::setApiKey(STRIPE_SECRET);
		
		//Create the CUSTOMER ID
		if ($this->newStripeCard($member, $params))
		{
			$today_unixtime = strtotime(date('Y-m-d'));
			$trial_end_unixtime = strtotime($member->created_at->format('Y-m-d') . ' +14day');
			$next_payment_date = ($member->next_payment_date) ? $member->next_payment_date->format('Y-m-d') : null;
			$next_payment_date_unixtime = strtotime($next_payment_date);
	
			$process_payment = ($today_unixtime >= $trial_end_unixtime) && 
							   ($next_payment_date == null || $today_unixtime >= $next_payment_date_unixtime);
							   
			if ($process_payment)
			{
				$payment = $this->processPayment($member, $params);
			}
			else
			{
				$payment = $this->saveAuthorization($member, $params);
			}
	
			if ($payment)
			{	
				$member->update_account_status($process_payment);
				
				if (isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1183) mail("alen@alpha-male.tv","FB Click - Payment Save - SUCCESS!","");
				if (isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1184) mail("alen@alpha-male.tv","Google Click - Payment Save - SUCCESS!","");   
				
				return (true);
			}
		}
		
		return(false);
	}
	
	public function newStripeCard(Member $member, $params)
	{
		require_once(APPLICATION_PATH . '/../lib/stripe_lib/Stripe.php'); 
		Stripe::setApiKey(STRIPE_SECRET);
					
		try
		{
			if (empty($params['stripeToken'])) {
				throw new Exception('Stripe token not set.');
			}			
			$response = Stripe_Customer::create(array(
				"email" => $member->email, 
				"description" => "member_id " . $member->id, 
				"card" => $params['stripeToken']
			));	
		}
		catch(Exception  $e)
		{		
			info( "WE FAILED TO PROCESS THIS CC INFORMATION - COULD NOT CREATE A CUSTOMER:" );
			info( $e->getMessage() );
			info( print_r ( $_REQUEST, true ) );
		
					
			mail("alen@alpha-male.tv", strtoupper(COMPANY_NAME) . " ERROR! STRIPE: " . $e->getMessage(),
				"There was a problem on " . APP_URL . " in Member::new_stripe_card() with member id " . $member->id . ", ERROR: " .  $e->getMessage() . "\n\n" .
				'$_REQUEST = ' . "\n" . print_r($_REQUEST, true) . "\n\n" .
				'$_SERVER = ' . "\n" . print_r($_SERVER, true)
			);
		
			$member->payment_error = $e->getMessage();
			return (false);
		}
		
		$member->stripe_customer_id = $response->id;
		$member->save();

		return(true);
	}
	
	public function processPayment(Member $member, $params)
	{
		$result = false;
		
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
	
		$price_net = $member->price;
		Logger::log('Net price: ' . $price_net, Logger::LOG_TYPE_DEBUG);
		
		$balance = CashTrans::getMemberBalance($member->id);
		Logger::log('Member balance: ' . $balance, Logger::LOG_TYPE_DEBUG);
		
		$debit_amount = 0;
		if ($balance >= $price_net) {
			$debit_amount = $price_net;
			$price_net = 0;
		} elseif ($balance > 0) {
			$debit_amount = $balance;
			$price_net = $price_net - $balance;
		}
		
		$tax_data = $member->getTaxData($params);
		// Update country and state in $params as getTaxData() may have found a different tax state
		$params['country'] = $tax_data['country'];
		$params['state'] = $tax_data['state'];
		
		$tax = round($price_net * $tax_data['rate'], 2);
		$price_gross = $price_net + $tax;
		
		Logger::log('Debit amount: ' . $debit_amount, Logger::LOG_TYPE_DEBUG);
		Logger::log('Charge amount: ' . $price_gross, Logger::LOG_TYPE_DEBUG);
		
		
		$params['amount_net'] = $price_net;
		$params['taxes'] = $tax;
		
		$payment = null;
		$payment_error_message = '';
		switch ($this->code) {
			case 'STRIPE':
				$payment = $this->processStripe($member, $params, $payment_error_message);
			break;
			case 'PAYPAL':
				$payment = $this->processPaypal($member, $params, $payment_error_message);
			break;
		}
		
		if ($payment) {
			if ($debit_amount > 0) {
				Logger::log('Debiting cash balance', Logger::LOG_TYPE_DEBUG);
				$cash_trans = CashTrans::newDebit($member->id, $debit_amount, 'Payment id ' . $payment->id, null, $payment->id);
				$payment->debit_cash_trans_id = $cash_trans->id;
				$payment->save();
				Logger::log('Debited: ' . $debit_amount, Logger::LOG_TYPE_DEBUG, true);
			}
			try {
				$member->creditAffiliate();
			} catch (Zend_Exception $e) {
				// Either no affiliate related or already credited
			}
			$member->doReferralPayout($payment);
			$result = $payment;

			$previous_payments = Payment::count(array("conditions" => array("member_id = ? and id != ? and payment_type='clientfinder'", $member->id, $payment->id)));

			// Notify that a customer made a first-time payment

			if ($previous_payments == 0) {			
				
				$subject = "SUCCESS - NEW PAYMENT " . COMPANY_NAME. " -- " . $member->id . " -- " . $member->email . " -- " . $member->created_at->format("Y-M-d").  " -- " . $member->price;
				$message = APP_URL . "/admin/member/" . $member->id;
				
				$alert_emailer = Zend_Registry::get('alertEmailer');
				$alert_emailer->send(
					$subject,
					$message,
					'new-payment',
					true
				);
			}

		} else {
			// payment failed
			$msg = $payment_error_message . "\n";
			$msg .= "Billing attempts: " . $member->billing_failures . "\n";	
			$msg .= "Member: ".APP_URL."/admin/member/".$member->id . "\n";	
			$msg .= "User FName: ".( empty($params['fname']) ? $member->first_name : $params['fname'] ) . "\n";
			$msg .= "User LName: ".( empty($params['lname']) ? $member->last_name : $params['lname'] ) . "\n";
			$msg .= "User Phone: ".( empty($params['phone']) ? $member->phone : $params['phone'] ) . "\n";
						
			$alert_emailer = Zend_Registry::get('alertEmailer');
			$alert_emailer->send(
				'Payment processing error - member_id ' . $member->id,
				$msg,
				'error-payment',
				true
			);
			EventLog::create(
				EventLog::ENTITY_TYPE_MEMBER,
				'payment_fail',
				$payment_error_message,
				$member->id,
				$member->id
			);
			$result = false;
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		return $result;
	}
	
	protected function processStripe(Member $member, $params, &$error_message)
	{
		require_once(APPLICATION_PATH . '/../lib/stripe_lib/Stripe.php'); 
		Stripe::setApiKey(STRIPE_SECRET);
		
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$amount_net = !empty($params['amount_net']) ? $params['amount_net'] : 0;
		$taxes = !empty($params['taxes']) ? $params['taxes'] : 0;
		$amount_gross = ($amount_net + $taxes);
		
		$charge = null;
		if ($amount_net > 0) 
		{
			try
			{
				if ( empty( $member->stripe_customer_id) ) 
				{
					Logger::log('Stripe customer id not set. Registering new Stripe card', Logger::LOG_TYPE_DEBUG);
					if (!$new_stripe_card =	$member->new_stripe_card($params)) 
					{
						throw new Exception($member->payment_error);
					}
				}
				
				$cents = $amount_gross * 100;
				
				// create the charge on Stripe's servers - this will charge the user's card
				$charge = Stripe_Charge::create(array(
							  "amount" => $cents, // amount in cents, again
							  "currency" => "cad",
							  "customer" => $member->stripe_customer_id,
							  "description" => COMPANY_NAME . " Payment for Member: " . $member->id)
							);
			}
			catch(Exception $e)
			{
				Logger::log('Stripe payment failed with an exception', Logger::LOG_TYPE_DEBUG);
				Logger::startSection(Logger::LOG_TYPE_DEBUG);
				Logger::log('Exception message: ' . $e->getMessage(), Logger::LOG_TYPE_DEBUG);
				Logger::log('Request data', Logger::LOG_TYPE_DEBUG);
				Logger::log(print_r($_REQUEST, true), Logger::LOG_TYPE_DEBUG, true);
				Logger::log('Server data', Logger::LOG_TYPE_DEBUG);
				Logger::log(print_r($_SERVER, true), Logger::LOG_TYPE_DEBUG, true);
				Logger::log('Session data', Logger::LOG_TYPE_DEBUG);
				Logger::log(print_r($_SESSION, true), Logger::LOG_TYPE_DEBUG, true);
				
				info( "WE FAILED TO PROCESS THIS CC INFORMATION - ACTUAL PAYMENT:" );
				info( $e->getMessage() );
				info( print_r ( $_REQUEST, true ) );
				$error_message = $e->getMessage();
				
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				
				return false;
			}
		}
		
		$payment = new Payment();
		
		$payment->member_id = $member->id;
		$payment->stripe_payment_id = !empty($charge) ? $charge->id : '';
		$payment->payment_type = "clientfinder";
		$payment->payment_gateway_id = $this->id;
		$customer = Stripe_Customer::retrieve($member->stripe_customer_id);
		if (isset($customer->active_card)) 
		{
			$member_updated = false;
			if (empty($member->country) || empty($member->state) && 
				!empty($customer->active_card->address_country) &&
				!empty($customer->active_card->address_state) 
			) 
			{
				$member->address = $customer->active_card->address_line1;
				$member->address2 = $customer->active_card->address_line2;
				$member->city = $customer->active_card->address_city;
				$member->state = $customer->active_card->address_state;
				$member->country = $customer->active_card->address_country;
				$member->zip = $customer->active_card->address_zip;
				
				$member_updated = true;
			}
				
			if (!empty($customer->active_card->exp_month)) {
				$payment->cc_expire_date = str_pad($customer->active_card->exp_month,2,"0",STR_PAD_LEFT).$customer->active_card->exp_year;
				$member_updated = true;
			}			if (!empty($customer->active_card->last4)) {
				$payment->last_four	= $customer->active_card->last4;
				$member_updated = true;
			}			
			if ($member_updated) {
				$member->save();
			}
		}
		
		$payment->first_name = ( empty($params['fname']) ? $member->first_name : $params['fname'] );
		$payment->last_name = ( empty($params['lname']) ? $member->last_name : $params['lname'] );
		
		$payment->address1 	= ( empty($params['address']) ? $member->address : $params['address'] );
		$payment->city = ( empty($params['city']) ? $member->city : $params['city'] );
		$payment->state = ( empty($params['state']) ? $member->state : $params['state'] );
		$payment->country = ( empty($params['country']) ? $member->country : $params['country'] );
		$payment->zip = ( empty($params['zip']) ? $member->zip : $params['zip'] );
		$payment->phone = ( empty($params['phone']) ? $member->phone : $params['phone'] );
		$payment->amount = $amount_net;	
		$payment->taxes = $taxes;
		
		$payment->save();
		
		$logPaymentData = array(
			'id' => $payment->id,
			'stripe_payment_id' => !empty($charge) ? $charge->id : '',
			'last_four' => $payment->last_four,
			'amount' => $payment->amount,
			'taxes' => $payment->taxes
		);
		Logger::log('Payment data', Logger::LOG_TYPE_DEBUG);
		Logger::log(print_r($logPaymentData, true), Logger::LOG_TYPE_DEBUG, true);
		Logger::log('Payment processed successfully', Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $payment;
	}
	
	protected function processPaypal(Member $member, $params, &$error_message)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$last_payment = Payment::find(array(
			'conditions' => 'member_id='.$member->id.' and payment_type="clientfinder" AND paypal_confirmation_number != "Free blast" AND paypal_confirmation_number IS NOT NULL', 
			'limit' => 1, 
			'order'=>'created_at desc, id desc'
		));
		if (!$last_payment)
		{
			$error_message = "MEMBER ID:".$member->id . " ".$member->first_name . " " . $member->last_name . " ".$member->email . " NO PAYPAL REFERENTIAL PAYMENT ON RECORD!";
			return false;
		}
		
		$amount_net = !empty($params['amount_net']) ? $params['amount_net'] : 0;
		$taxes = !empty($params['taxes']) ? $params['taxes'] : 0;
		$amount_gross = ($amount_net + $taxes);
		
		$paypal_response = null;
		if ($amount_net > 0) 
		{
			$paypal = new PaypalRequest();
			$paypal_response = $paypal->DoReferenceTransaction(
							$last_payment,
							$amount_gross,
							"CAD",
							COMPANY_NAME . " ClientFinder"
					);
			if ("SUCCESS" != strtoupper($paypal_response["ACK"]) && "SUCCESSWITHWARNING" != strtoupper($paypal_response["ACK"]))
			{			
				Logger::log('Paypal payment failed', Logger::LOG_TYPE_DEBUG);
				Logger::startSection(Logger::LOG_TYPE_DEBUG);
				Logger::log('Request data', Logger::LOG_TYPE_DEBUG);
				Logger::log(print_r($_REQUEST, true), Logger::LOG_TYPE_DEBUG, true);
				Logger::log('Server data', Logger::LOG_TYPE_DEBUG);
				Logger::log(print_r($_SERVER, true), Logger::LOG_TYPE_DEBUG, true);
				Logger::log('Session data', Logger::LOG_TYPE_DEBUG);
				Logger::log(print_r($_SESSION, true), Logger::LOG_TYPE_DEBUG, true);
				
				info("WE FAILED TO SEND PROCESS THIS CC INFORMATION:");
				info(print_r($paypal_response, true));
				info(print_r($_REQUEST, true));
				$error_message = "MEMBER ID:".$member->id . " ".$member->first_name . " " . $member->last_name . " ".$member->email
						 . " ERROR CODE: " . $paypal_response['L_ERRORCODE0'] . " SORTY MESSAGE: " . $paypal_response['L_SHORTMESSAGE0'] 
						 . " LONG MESSAGE: " . $paypal_response['L_LONGMESSAGE0'] . "\n\n\n" 
						 . " https://www.x.com/developers/paypal/documentation-tools/api/errorcodes";
						 
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
				Logger::endSection(Logger::LOG_TYPE_DEBUG);
						 
				return false;
			}
		}
		
		$payment = new Payment();
		if (!empty($paypal_response)) {
			$payment->paypal_confirmation_number = $paypal_response['TRANSACTIONID'];
			$payment->paypal_transaction_expire_date =  date("Y-m-d H:i:s", strtotime("+1 year"));
		}
		
		$payment->payment_gateway_id = $this->id;
		
		$payment->cc_expire_date = $last_payment->cc_expire_date;
		$payment->last_four = $last_payment->last_four;
		$payment->first_name = $last_payment->first_name;
		$payment->last_name = $last_payment->last_name;
		
		$payment->payment_type = 'clientfinder';
		
		$payment->member_id = $member->id;
		$payment->address1 = $last_payment->address1;
		$payment->city = $last_payment->city;
		$payment->state = $last_payment->state;
		$payment->country = $last_payment->country;
		$payment->zip = $last_payment->zip;
		$payment->amount = $amount_net;
		$payment->taxes = $taxes;
		$payment->save();
		
		$logPaymentData = array(
			'id' => $payment->id,
			'paypal_confirmation_number' => !empty($paypal_response) ? $paypal_response['TRANSACTIONID'] : '',
			'last_four' => $payment->last_four,
			'amount' => $payment->amount,
			'taxes' => $payment->taxes
		);
		Logger::log('Payment data', Logger::LOG_TYPE_DEBUG);
		Logger::log(print_r($logPaymentData, true), Logger::LOG_TYPE_DEBUG, true);
		Logger::log('Payment processed successfully', Logger::LOG_TYPE_DEBUG);
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $payment;
	}
	
	public function saveAuthorization(Member $member, $params)
	{
		$payment = null;
		switch ($this->code) {
			case 'STRIPE':
				$payment = $this->saveAuthorizationStripe($member, $params);
			break;
			case 'PAYPAL':
				throw new Exception('Invalid operation - cannot save payment authorisation to Paypal');
			break;
		}
		
		return $payment;
	}
	
	protected function saveAuthorizationStripe(Member $member, $params)
	{
		require_once(APPLICATION_PATH . '/../lib/stripe_lib/Stripe.php'); 
		Stripe::setApiKey(STRIPE_SECRET);
	
		$payment = new Payment();
		$payment->member_id 		= $member->id;
		
		$payment->first_name 		= !empty($params['fname']) ? $params['fname'] : $member->first_name;
		$payment->last_name 		= !empty($params['lname']) ? $params['lname'] : $member->last_name;
		$payment->payment_type 		= "clientfinder";
		$payment->payment_gateway_id 	= $this->id;
		
		$payment->address1 			= !empty($params['address']) ? $params['address'] : $member->address;
		$payment->city 			= !empty($params['city']) ? $params['city'] : $member->city;
		$payment->state 			= !empty($params['state']) ? $params['state'] : $member->state;
		$payment->country 			= !empty($params['country']) ? $params['country'] : $member->country;
		$payment->zip 				= !empty($params['zip']) ? $params['zip'] : $member->zip;
		$payment->phone 			= !empty($params['phone']) ? $params['phone'] : $member->phone;
		$payment->amount 			=  0;
		$payment->taxes  			=  0;
			
		try
		{
			
			// Need to load card expire date
			$customer = Stripe_Customer::retrieve($member->stripe_customer_id);
			
			if (isset($customer->active_card) && !empty($customer->active_card->exp_month))
				$payment->cc_expire_date	=	str_pad($customer->active_card->exp_month,2,"0",STR_PAD_LEFT).$customer->active_card->exp_year;
			
			if (isset($customer->active_card) && !empty($customer->active_card->last4))
			$payment->last_four	=	$customer->active_card->last4;
			$payment->save();
		
		} catch (Exception $e) {
		
			mail("alen@alpha-male.tv","ERROR !!!!!!! - PRE-AUTH - FAIL -- READ ME !!!!", $e->getMessage() . "\n\n" .  get_class($e) . "\n\n" . print_r($customer, true) . "\n\n" . print_r($payment, true) );

			return(false);			
		}
		
		$member->billing_failures = 0;
		
		if (isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1183) mail("alen@alpha-male.tv","FB Click - Payment Save - PRE-AUTH - SUCCESS!","");
		if (isset($_COOKIE['cityblast_aff_id']) && $_COOKIE['cityblast_aff_id'] == 1184) mail("alen@alpha-male.tv","Google Click - Payment Save - PRE-AUTH - SUCCESS!","");   			
		
		return($payment);
	}
	
	protected function notifyPaymentFailed(Member $member)
	{
		$DESCRIPTION = "Monthly ".COMPANY_NAME." Invoice";
		
		$view = Zend_Registry::get('view');
		$fromEmail = defined('HELP_EMAIL') ? HELP_EMAIL : 'noreply@'.APPLICATION_ENV.'.com';
		$fromName = defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
		
		$taxes = "0";
		if (strtoupper($member->default_city->state) == "Ontario") {
			$taxes = "0.13";
		}
		
		$params =	array(
			'member' => $member,
			'amount' => $member->price,
			'taxes' => $taxes,
			'title' => $DESCRIPTION
		);
		
		$subject = 'Oh dear, there was a problem. We failed to process your ' . COMPANY_NAME .' payment. Immediate attention required';
		$htmlBody = $view->partial('email/failed_payment.html.php', $params);
		if ($member->email) {		
		    $member_email = !empty($member->email_override) ? $member->email_override : $member->email;
			$mandrill = new Mandrill(array('FAILED_PAYMENT'));
			$mandrill->setFrom($fromName, $fromEmail);
			$mandrill->addTo($member->first_name . " " . $member->last_name, $member_email);
			if ($member_email != $member->email) {
				$mandrill->addTo("", $member->email);
			}
			$mandrill->send($subject, $htmlBody);

			//Keep track of the Email in our local LOG
			$mail = new ZendMailLogger('FAILED_PAYMENT', $member->id);
			$mail->setBodyHtml($htmlBody, 'utf-8', 'utf-8');
			$mail->setFrom($fromEmail, $fromName);
			$mail->addTo($member_email, $member_email);
			$mail->setSubject($subject);
			$mail->log();
		}
	}
	
	/*
	protected function paymentNotification(Member $member, $params)
	{
		$DESCRIPTION = "Monthly ".COMPANY_NAME." Invoice";
		
		$member  		 = $params['member'];
		$invoice_number = $params['invoice_number'];
		$taxes  		 = $params['taxes'];
		$amount  		 = $params['amount'];
		
		// $view = new Zend_View();
		// $view->setBasePath( dirname(__FILE__)."/../views/common/");
		// $view->setBasePath( dirname(__FILE__)."/../views/".APPLICATION_ENV."/");
		
		$fromEmail	=   defined('HELP_EMAIL') ? HELP_EMAIL : 'noreply@'.APPLICATION_ENV.'.com';
		$fromName		=   defined('APP_EMAIL_FROM_NAME') ? APP_EMAIL_FROM_NAME : COMPANY_NAME;
		
		$view = Zend_Registry::get('view');
		
		$subject  = COMPANY_NAME . 'Invoice Number: ' . $invoice_number;
		$htmlBody = $view->partial('email/clientfinder-invoice.html.php', $params);
		$mandrill = new Mandrill(array('INVOICE'));
		$mandrill->setFrom($fromName, $fromEmail);
		$mandrill->addTo($member->first_name . " " . $member->last_name, $params['email']);
		$mandrill->send($subject, $htmlBody);

		//Keep track of the Email in our local LOG
		$mail = new ZendMailLogger('INVOICE', $this->id);
		$mail->setBodyHtml ( $htmlBody, 'utf-8', 'utf-8' );
		$mail->setFrom ( $fromEmail, $fromName );
		$mail->addTo ( $email, $email );
		$mail->setSubject ( $subject );
		$mail->log();
	}
	*/
}