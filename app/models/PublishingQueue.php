<?php
class PublishingQueue extends ActiveRecord\Model
{
	static $table = 'publishing_queue';

	static $belongs_to = array(
		array('publish_city', 'class' => 'City', 'foreign_key' => 'city_id'),
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id')
	);
	
	/**
	 * Find unpublished publishable content for member
	 * 
	 * This method uses paginated DB query to minimise memory usuage.
	 * 
	 * @param Member $member
	 * @param array $options
	 * @return PublishingQueue
	 */
	public static function getUnpublishedPublishable($member, $options = array())
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$get_batch = function($member, $options, $page = 1, $records_per_page = 60) 
		{
			$options = is_array($options) ? $options : array();
			$opts = array();
			$opts['city_id'] = !empty($options['city_id']) ? $options['city_id'] : null;
			$opts['blast_type'] = !empty($options['blast_type']) ? $options['blast_type'] : null;
			$opts['hopper_id'] = !empty($options['hopper_id']) ? $options['hopper_id'] : null;
			$opts['inc_tag_id'] = !empty($options['inc_tag_id']) ? $options['inc_tag_id'] : null;
			$opts['ex_tag_id'] = !empty($options['ex_tag_id']) ? $options['ex_tag_id'] : null;
			$opts['franchise_id'] = !empty($options['franchise_id']) ? $options['franchise_id'] : null;
			$opts['target_network'] = !empty($options['target_network']) ? $options['target_network'] : null;
			
			$page = !empty($page) ? $page : 1;
			$records_per_page = !empty($records_per_page) ? $records_per_page : 60;
			$offset = (($page - 1) * $records_per_page);
			
			Logger::log('PublishQueue Query ' . $page, Logger::LOG_TYPE_DEBUG);
			
			$where_clauses = array();
			
			$conn = PublishingQueue::connection();
			$sql = "SELECT pq.* FROM publishing_queue pq ";
			
			// Ensure has not yet been posted by member
			$sql .= "LEFT JOIN post p ON pq.listing_id = p.listing_id AND p.member_id = " . $conn->escape($member->id) . " AND p.result = 1 ";
			$where_clauses[] ="p.id IS NULL";
			
			if ($opts['hopper_id']) {
				$where_clauses[] ="pq.hopper_id = " . $conn->escape($opts['hopper_id']);
			}
			
			$sql .= "INNER JOIN listing l ON pq.listing_id = l.id ";
			$where_clauses[] = "(l.expiry_date = 0 OR l.expiry_date IS NULL OR l.expiry_date > NOW()) ";
			if ($opts['blast_type']) {
				$where_clauses[] ="l.blast_type = " . $conn->escape($opts['blast_type']);
			}
			if ($opts['franchise_id']) {
				$sql .= "INNER JOIN franchise f ON l.franchise_id = f.id AND f.is_enabled = 1 ";
				$where_clauses[] = "l.franchise_id = " . $conn->escape($opts['franchise_id']);
			}
			
			
			if ($opts['inc_tag_id']) {
				$sql .= "INNER JOIN tag_cloud tc ON pq.listing_id = tc.listing_id AND tc.tag_id = " . $conn->escape($opts['inc_tag_id']) . " ";
			}
			
			if ($opts['ex_tag_id']) {
				$sql .= "LEFT JOIN tag_cloud extc ON pq.listing_id = tc.listing_id AND tc.tag_id = " . $conn->escape($opts['ex_tag_id']) . " ";
				$where_clauses[] ="extc.listing_id IS NULL ";
			}
			
			if ($opts['city_id']) {
				$where_clauses[] = "pq.city_id = " . $conn->escape($opts['city_id']);
			}
			
			switch ($opts['target_network']) {
				case 'facebook':
				break;
				case 'twitter':
				break;
				case 'linkedin':
					// Linked in content listings must have a link
					$where_clauses[] = "(
						l.blast_type != " . $conn->escape(Listing::TYPE_CONTENT) . " 
						OR (l.content_link IS NOT NULL AND l.content_link != '')
					)";
				break;
			}
			
			if (!empty($where_clauses)) {
				$sql .=  "WHERE " . implode(" AND ", $where_clauses) . " ";
			}
			
			$sql .= "ORDER BY pq.listing_id ASC LIMIT " . (int)$offset . ", " . (int)$records_per_page;
			
			$result = PublishingQueue::find_by_sql($sql);
			
			Logger::log('Found ' . count($result) . ' queue items', Logger::LOG_TYPE_DEBUG);
			
			if (!empty($result)) {
				shuffle($result);
				shuffle($result);
			}
			
			return $result;
		};

		$result = null;
		$page = 1;
		while ($items = $get_batch($member, $options,  $page)) {
			if (!empty($items)) {
				foreach ($items as $item) {
					if ($result = $member->testQueueItem($item)) {
						break 2;
					}
				}
			}
			if ($page == 1000) {
				// give up
				break;
			}
			$page++;
		}
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
		
		return $result;
	}
	
	
	/**
	 *
	 * @param array $params associative array of search form values
	 * @param integer $per_page integer
	 * @param integer $current_page integer
	 * @return Zend_Paginator
	 */
	static function search(array $params, $per_page = 10, $current_page = 1)
	{
		$conn = PublishingQueue::connection();
		
		$joins = array("LEFT JOIN listing ON publishing_queue.listing_id = listing.id");

		$conditions	= array();

		if (!empty($params['id']))
		{
			$conditions['id'] = "publishing_queue.id = " . $conn->escape($params['id']);
		}
		if (!empty($params['listing_id']))
		{
			$conditions['listing_id'] = "listing.id = " . $conn->escape($params['listing_id']);
		}
		if (!empty($params['hopper_id']))
		{
			$conditions['hopper_id'] = "publishing_queue.hopper_id = " . $conn->escape($params['hopper_id']);
		}
		if (!empty($params['member_id']))
		{
			$conditions['member_id'] = "listing.member_id = " . $conn->escape($params['member_id']);
		}
		if (!empty($params['message']))
		{
			$conditions['message'] = "listing.message LIKE " . $conn->escape('%' . $params['message'] . '%');
		}
		if (!empty($params['city_id']))
		{
			$conditions['city'] = "publishing_queue.city_id = " . $conn->escape($params['city_id']);
		}

		if (!empty($params['price_range_low']))
		{
			$conditions['priceL'] = "listing.price > " . (intval($params['price_range_low']) - 1);
		}

		if (!empty($params['price_range_high']))
		{
			$conditions['priceH'] = "listing.price < " . (intval($params['price_range_high']) + 1);
		}

		if (!empty($params['property_type_id']))
		{
			$conditions['type'] = "listing.property_type_id = " . $conn->escape($params['property_type_id']);
		}

		if (!empty($params['number_of_bedrooms']))
		{
			$conditions['bedroom'] = "listing.number_of_bedrooms = " . $conn->escape($params['number_of_bedrooms']);
		}

		if (!empty($params['number_of_bathrooms']))
		{
			$conditions['bathroom'] = "listing.number_of_bathrooms = " . $conn->escape($params['number_of_bathrooms']);
		}

		if (!empty($params['number_of_parking']))
		{
			$conditions['parking'] = "listing.number_of_parking = " . $conn->escape($params['number_of_parking']);
		}

		if (!empty($params['street']))
		{
			$conditions['street'] = "listing.street LIKE ". $conn->escape('%' . $params['street'] . '%');
		}
		
		if (!empty($params['content_link']))
		{
			$conditions['content_link'] = 
				"(content_link LIKE " . $conn->escape('%' . $params['content_link'] . '%') . " " . 
				"OR url_mapping.url LIKE " . $conn->escape('%' . $params['content_link'] . '%') .")";
			$joins[] = "LEFT JOIN url_mapping ON url_mapping.listing_id = listing.id";
		}

		if (!empty($params['postal']))
		{
			$conditions['postal'] = "listing.postal_code = " . $conn->escape($params['postal']);
		}

		if (!empty($params['mls_number']))
		{
			$conditions['mls'] = "listing.mls = " . $conn->escape($params['mls_number']);
		}

		if (
			! empty($params['listing_type']) 
			&& in_array(
				$params['listing_type'], 
				array(
					Listing::TYPE_CONTENT, 
					Listing::TYPE_LISTING, 
					Listing::TYPE_IDX,
				)
			)
		)
		{
			$conditions['listing_type'] = "listing.blast_type = ". $conn->escape($params['listing_type']);
		}
		
		if (!empty($params['tags']) && is_array($params['tags'])) 
		{
			foreach ($params['tags'] as $tag_id) 
			{
				$tag_exists[] = "tag_id = " . $conn->escape($tag_id);
			}
			$conditions[] = "EXISTS (SELECT listing_id FROM tag_cloud WHERE (" . implode(' OR ', $tag_exists) . ") AND tag_cloud.listing_id = publishing_queue.listing_id)";
		}

		if (count($conditions))
		{
			$conditions	= implode(" AND ", $conditions);
		}
		else
		{
			$conditions	= ' 1 ';
		}

		$options	= array(
			'conditions' => $conditions,
			'joins' => $joins,
			'order' => 'listing.id DESC',
		);

		$paginator = new Zend_Paginator(new ARPaginator('PublishingQueue', $options));
		$paginator->setCurrentPageNumber($current_page);
		$paginator->setItemCountPerPage($per_page);

		return $paginator;
	}
}
