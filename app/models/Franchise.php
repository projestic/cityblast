<?php
class Franchise extends ActiveRecord\Model
{
	static $table = 'franchise';

	static $belongs_to = array(
		array('owner', 'class' => 'Member', 'foreign_key' => 'member_id')
	);

	static $has_many = array(
		array('agents', 'class' => 'Member', 'foreign_key' => 'franchise_id')
	);

	public function hasAgents() 
	{
		return (bool) $this->countAgents();
	}
	
	public function countAgents() 
	{
		return Member::count( array('conditions' => array('franchise_id' => $this->id)) );
	}

	
	/************************************
	public function ingestPhoto($original) 
	{
		$imageDest  = '/images/franchise/' . basename($original);
		$imageDestPath = CDN_ORIGIN_STORAGE . $imageDest;

		//echo $imageDest . "<BR>";
		//echo $imageDestPath . "<BR>";
		echo "SRC:". $original . "<BR>";
		
		
		echo "DEST:".dirname($imageDestPath) . '/' . basename($original) . "<br>";
		
		exit();

		if(!copy($original, dirname($imageDestPath) . '/' . basename($original)))
		{
			echo "There was a problem copying the image!<BR>";
			exit();
		}	


		$this->logo = $imageDest;
		$this->save();
	
	}	 *******************************/
}