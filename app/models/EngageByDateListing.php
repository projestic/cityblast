<?php

class EngageByDateListing extends ActiveRecord\Model
{
	static $table = 'engage_by_date_listing';
	
	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id')
	);
	
	public function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$start_date = date('Y-m-d', $start_time);
		$end_date = date('Y-m-d', $end_time);
		
		Logger::log(
			'Compiling engagement by date and listing between ' . $start_date . ' and ' . $end_date, 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT 
				ctbdl.date, 
				ctbdl.listing_id,
				ctbdl.count,
				(SELECT likes FROM facebook_stat_by_date_listing WHERE date = ctbdl.date AND listing_id = ctbdl.listing_id),
				(SELECT comments FROM facebook_stat_by_date_listing WHERE date = ctbdl.date AND listing_id = ctbdl.listing_id),
				(SELECT shares FROM facebook_stat_by_date_listing WHERE date = ctbdl.date AND listing_id = ctbdl.listing_id),
				0,
				(
					SELECT 
						count(id) 
					FROM post 
					WHERE 
						listing_id = ctbdl.listing_id AND 
						DATE(created_at) <= ctbdl.`date` AND
						result = 1 
				) AS posts,
				0,
				0,
				0,
				0,
				0
			FROM `click_stat_by_date_listing` AS ctbdl
			WHERE date >= " . $db_con->escape($start_date) . "  
			AND date <= " . $db_con->escape($end_date) . "  
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		$sql = "
			SELECT 
				fsbdl.date, 
				fsbdl.listing_id,
				(SELECT count FROM click_stat_by_date_listing WHERE date = fsbdl.date AND listing_id = fsbdl.listing_id),
				fsbdl.likes,
				fsbdl.comments,
				fsbdl.shares,
				0,
				(
					SELECT 
						count(id) 
					FROM post 
					WHERE 
						listing_id = fsbdl.listing_id AND 
						DATE(created_at) <= fsbdl.`date` AND
						result = 1 
				) AS posts,
				0,
				0,
				0,
				0,
				0
			FROM `facebook_stat_by_date_listing` fsbdl
			WHERE date >= " . $db_con->escape($start_date) . "  
			AND date <= " . $db_con->escape($end_date) . "  
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		$sql = "
			UPDATE `engage_by_date_listing` 
			SET 
				score = (clicks + (likes * 10) + (comments * 15) + (shares * 25))
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		$sql = "
			UPDATE `engage_by_date_listing` 
			SET 
				clicks_perpost = ROUND(clicks / posts, 3),
				likes_perpost = ROUND(likes / posts, 3),
				comments_perpost = ROUND(comments / posts, 3),
				shares_perpost = ROUND(shares / posts, 3),
				score_perpost = ROUND(score / posts, 3)
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}