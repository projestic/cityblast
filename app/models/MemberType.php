<?php

class MemberType extends ActiveRecord\Model
{
	static $table = 'member_type';

    static $belongs_to = array(
		array('inherits', 'class' => 'MemberType', 'foreign_key' => 'inherits_from_type_id')
	);

	public function getPriceFor($member) 
	{
		switch ($member->billing_cycle) {
			case 'month':
				return $this->price_month;
			break;
			case 'biannual':
				return $this->price_biannual;
			break;
			case 'annual':
				return $this->price_annual;
			break;
		}
	}
	
	/**
	 * Get number of months in billing cycle
	 * 
	 * @var string $billing_cycle 
	 * @return int
	 */
	public static function getBillingCycleMonths($billing_cycle = null)
	{
		$billing_cycle = !empty($billing_cycle) ? $billing_cycle : null;
		$result = 1;
		switch ($billing_cycle){
			case 'biannual':
				$result = 6;
			break;
			case 'annual':
				$result = 12;
			break;
		}
		return $result;
	}
	
	/**
	 * Get Price
	 * 
	 * @var string $billing_cycle Returned price assumes member has given billing cycle
	 * @return float
	 */
	public function getPrice($billing_cycle) 
	{
		switch ($billing_cycle) {
			case 'month':
				return $this->price_month;
			break;
			case 'biannual':
				return $this->price_biannual;
			break;
			case 'annual':
				return $this->price_annual;
			break;
		}
	}
	
	
	/**
	 * Get price monthly
	 * 
	 * @var string $billing_cycle Returned price assumes member has given billing cycle
	 * @return float
	 */
	public function getPriceMonthly($billing_cycle = null)
	{
		$result = $this->getPrice($billing_cycle) / static::getBillingCycleMonths($billing_cycle);
		return $result;
	}

}