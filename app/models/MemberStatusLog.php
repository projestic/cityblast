<?php

class MemberStatusLog extends ActiveRecord\Model
{
	static $table = 'member_status_log';

    static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);
	
	public static function getMemberStatusLog($member_id, $date = null)
	{
		$date = ($date instanceof DateTime) ? $date->format('Y-m-d') : $date;
		$date = !empty($date) ? $date : date('Y-m-d');
		
		$status = MemberStatusLog::find(array(
			'conditions' => array("DATE(created_at) <= ? AND member_id = ?", $date, $member_id),
			'order' => 'created_at DESC',
			'limit' => 1
		));
		
		return $status;
	}
}