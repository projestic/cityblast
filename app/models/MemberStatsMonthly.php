<?php

class MemberStatsMonthly extends ActiveRecord\Model
{
	static $table = 'member_stats_monthly';
	
	public static function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$start_date = date('Y-m-01', $start_time);
		$end_date = date('Y-m-t', $end_time);
		
		Logger::log(
			'Compiling member stats monthly between ' . $start_date . ' and ' . $end_date, 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT 
				DATE_FORMAT(date, '%Y-%m-01') AS month_date, 
				DATE_FORMAT(date, '%Y'),
				DATE_FORMAT(date, '%m'),
				(
					SELECT nocc_trial_total
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(LAST_DAY(msd.date - INTERVAL 1 MONTH), '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS nocc_trial_total_prev,
				SUM(msd.nocc_trial_new),
				SUM(msd.nocc_trial_failed_convert),
				(
					SELECT nocc_trial_failed_convert_total 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS nocc_trial_failed_convert_total,
				SUM(msd.nocc_trial_cancel),
				SUM(msd.nocc_trial_deleted),
				(
					SELECT nocc_trial_total 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS nocc_trial_total,
				SUM(msd.nocc_trial_converted),
				(
					SELECT cc_trial_total
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(LAST_DAY(msd.date - INTERVAL 1 MONTH), '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS cc_trial_total_prev,
				SUM(msd.cc_trial_new),
				SUM(msd.cc_trial_cancel),
				SUM(msd.cc_trial_deleted),
				(
					SELECT cc_trial_total 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS cc_trial_total,
				SUM(msd.cc_trial_converted),
				(
					SELECT cc_trial_alltime
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS cc_trial_alltime,
				(
					SELECT paid_total
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(LAST_DAY(msd.date - INTERVAL 1 MONTH), '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS paid_total_prev,
				SUM(msd.paid_new),
				SUM(msd.paid_cancel),
				SUM(msd.paid_deleted),
				(
					SELECT paid_total 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS paid_total,
				(
					SELECT paid_alltime 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS paid_alltime,
				(
					SELECT active_cc 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS active_cc,
				SUM(msd.free_new),
				(
					SELECT free 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS free,
				SUM(msd.signup_new),
				(
					SELECT signup_alltime
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS signup_alltime,
				(
					SELECT total 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS total,
				SUM(msd.cancelled_new),
				(
					SELECT total 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS cancelled_total,
				SUM(msd.deleted_new),
				(
					SELECT deleted_total 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS deleted_total,
				(
					SELECT avrg_lifetime_value 
					FROM member_stats_daily 
					WHERE DATE_FORMAT(date, '%Y-%m') = DATE_FORMAT(msd.date, '%Y-%m')
					ORDER BY date DESC
					LIMIT 1
				) AS avrg_lifetime_value
			FROM `member_stats_daily` msd
			WHERE date >= " . $db_con->escape($start_date) . "  
			AND date <= " . $db_con->escape($end_date) . "  
			GROUP BY month_date
			ORDER BY date DESC
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
	
}