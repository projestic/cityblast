<?php
class PublishSettings extends ActiveRecord\Model
{
	static $table = 'publish_settings';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
	);

}
?>
