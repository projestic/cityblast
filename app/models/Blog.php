<?php
/**
 * Blog. Holds a feed URL to be pulled later.
 *
 * @property int id
 * @property string name
 * @property string url
 * @property int tier
 * @property int|bool seek_images_in_external_article
 * @property string|DateTime created_at
 */
class Blog extends ActiveRecord\Model
{
	static $table = 'blog';


	/**
	 * @param int $per_page
	 * @param int $current_page
	 * @return Zend_Paginator
	 */
	public static function getPaginator($per_page, $current_page) {
		$options = array();
		$paginator = new Zend_Paginator(new ARPaginator('Blog', $options));
		$paginator->setCurrentPageNumber($current_page);
		$paginator->setItemCountPerPage($per_page);
		return $paginator;
	}

}

