<?php
class OAuthRefreshToken extends ActiveRecord\Model
{
	static $table = 'oauth_refresh_tokens';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('client', 'class' => 'OAuthClient', 'foreign_key' => 'client_id'),
	);
	
}
