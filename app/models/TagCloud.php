<?php
class TagCloud extends ActiveRecord\Model
{
	static $table = 'tag_cloud';

	static $has_one = array(
		array('tag', 'class' => 'tag', 'foreign_key' => 'tag_id'),
		array('listing', 'class' => 'listing', 'foreign_key' => 'listing_id'),
	);
	
	public static function setListingTags ($listingId, $tags)
	{
		// delete any previous tags
		$selectedTags = TagCloud::find('all', array('conditions' => "listing_id = '{$listingId}'"));
		foreach ($selectedTags as $s)
		{
			$s->delete();
		}

		// set the new ones
		foreach ($tags as $t)
		{
			$tagCloud = new TagCloud();
			$tagCloud->tag_id = $t;
			$tagCloud->listing_id = $listingId;
			$tagCloud->save();
			unset($tagCloud);
		}
	}
}
?>