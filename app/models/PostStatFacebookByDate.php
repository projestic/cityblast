<?php


class PostStatFacebookByDate extends ActiveRecord\Model
{
	static $table = 'post_stat_facebook_by_date';
	
	public static function setTotalOnDate($type, $date, $count, $post)
	{
		$post = is_object($post) ? $post : Post::find($post); 
		
		$stats_prev =  PostStatFacebookByDate::all(array(
			'conditions' => array('post_id = ? AND date < ?', $post->id, $date),
			'order' => 'date DESC',
			'limit' => 1
		));
		$stat_prev = !empty($stats_prev) ? $stats_prev[0] : null;
		
		$stat_property_total = $type . '_total';
		$count_for_date = ($stat_prev) ? $count - $stat_prev->$stat_property_total : $count;
		
		// count_for_date would be zero if there was no change since last time
		// we only want to create a new record if ther is positive value
		if ($count_for_date) {
			$stat_existing = PostStatFacebookByDate::find_by_post_id_and_date($post->id, $date);
			$stat = $stat_existing ? $stat_existing : new PostStatFacebookByDate();
			$stat->date = new DateTime($date);
			$stat->post_id = $post->id;
			$stat->$stat_property_total = $count;
			if (!$stat_existing) {
				// If this is a new stat record then we must initialise totals of the other types
				// Even if these types had zero hits on the given day the totals must be correct
				$stat->likes_total = $post->facebook_likes;
				$stat->comments_total = $post->facebook_comments;
				$stat->shares_total = $post->facebook_shares;
			}
			$stat->$type = $count_for_date;
			$stat->save();
		}
	}
}