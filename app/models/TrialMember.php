<?php
class TrialMember extends ActiveRecord\Model
{
	static $table = 'trial_member';


	public function beforeSave() 
	{

		if ($this->is_dirty('email')  )
		{			 

			try {	
				$this->addToConstantContact();
			} catch (Exception $e) {
				$alert_emailer = Zend_Registry::get('alertEmailer');
				$alert_emailer->send(
					'ConstantContact error ' . $e->getCode() . ' for member ' . $this->id,
					$e->getMessage(),
					'error-constantcontact',
					true
				);
				
			}
		}
	}	

	private function getCTCTList() 
	{
		$encoded_user = urlencode(CONSTANT_CONTACT_USERNAME);
		return 'http://api.constantcontact.com/ws/customers/' . $encoded_user . '/lists/3';
	}

	public function addToConstantContact() 
	{
	
		if (!defined('CONSTANT_CONTACT_API_KEY')) return false;
		$api = new ConstantContact_API('oauth2', CONSTANT_CONTACT_API_KEY, CONSTANT_CONTACT_USERNAME, CONSTANT_CONTACT_OAUTH2_ACCESS_TOKEN);
		$contact = $this->makeCTCTContact();
		if (!$contact->emailAddress || !filter_var($contact->emailAddress, FILTER_VALIDATE_EMAIL)) return false;
		try 
		{
			if ($contact->link) 
			{
				$contact->id = 'http://api.constantcontact.com' . $this->constant_contact_link;
				$contact->link = $this->constant_contact_link;
				$api->updateContact($contact);
			} else {
				$new = $api->addContact($contact);
				$this->constant_contact_link = $new->link;
			}
		} catch (Exception $e) {
			if ($e->getCode() == 0 || $e->getCode() == 409) return false;
			throw $e;
		}
	
	}

	private function makeCTCTContact() 
	{
		$contact = new Contact();
		$contact->emailAddress 	= $this->email;
		$contact->optInSource   = 'ACTION_BY_CUSTOMER';
		$contact->lists 		= array( $this->getCTCTList() );
		$contact->customField1  = 'Unpaid';
		$contact->customField2  = 'trial';
		$contact->customField3  = 'paid';
		
		if ($this->constant_contact_link) 
		{
			$contact->id = 'http://api.constantcontact.com' . $this->constant_contact_link;
			$contact->link = $this->constant_contact_link;
		}
		return $contact;
	}

	public function is_dirty($attr) 
	{
		return in_array($attr, array_keys($this->dirty_attributes()));
	}


}
?>