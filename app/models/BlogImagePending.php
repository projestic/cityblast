<?php
/**
 * A blog entry image.
 *
 * @property int id
 * @property int blog_entry_id
 * @property string image
 */
class BlogImagePending extends ActiveRecord\Model
{
	static $table = 'blog_image_pending';

	static $before_destroy = 'delete_cleanup';

	static $belongs_to = array(
		array('blog_pending', 'class' => 'BlogPending', 'foreign_key' => 'blog_entry_id')
	);
	
	public function delete_cleanup() {
		@unlink(CDN_ORIGIN_STORAGE . $this->image);
	}

}
