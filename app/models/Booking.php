<?php
class Booking extends ActiveRecord\Model
{
	static $table = 'booking';


	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id'),
		array('booking_agent', 'class' => 'Member', 'foreign_key' => 'member_id'),
		array('city', 'class' => 'City', 'foreign_key' => 'city_id')
	);

}
?>
