<?php

class ClickStatByDateListing extends ActiveRecord\Model
{
	static $table = 'click_stat_by_date_listing';
	
	public function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		$start_date = date('Y-m-d', $start_time);
		$end_date = date('Y-m-d', $end_time);
		
		Logger::log(
			'Compiling click stat by date and listing between ' . $start_date . ' and ' . $end_date, 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT 
				DATE(`date`) AS day, 
				listing_id,
				SUM(count) 
			FROM `click_stat` 
			WHERE DATE(`date`) >= " . $db_con->escape($start_date) . "  
			AND DATE(`date`) <= " . $db_con->escape($end_date) . "  
			GROUP BY day, listing_id";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}
