<?php
class BlastReachQueue extends ActiveRecord\Model
{
	static $table = 'blast_reach_queue';

	static $belongs_to = array(
		array('member', 'class' => 'Member', 'foreign_key' => 'member_id')
	);

	static $has_one = array(
		array('post', 'class' => 'Post', 'foreign_key' => 'post_id')
	);
}
?>