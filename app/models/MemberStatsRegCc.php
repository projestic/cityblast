<?php

class MemberStatsRegCc
{ 
	public function calc() 
	{
		$records = $this->findMembersWithCc();
		return $this->groupIntoBuckets($records);
	}
	
	/**
	 * Find records along with time difference.
	 * @return array
	 */
	protected function findMembersWithCc() 
	{
		$sql = "
			SELECT m.id, TIMESTAMPDIFF(DAY, m.created_at, p1.created_at) AS diff_days
			FROM member m
			INNER JOIN payment p1 ON p1.member_id = m.id
			# Find first payment record per user
			LEFT JOIN payment p2 ON (p2.member_id = m.id AND p2.id < p1.id)
			WHERE
				p2.id IS NULL
		";
		$query_result = Member::find_by_sql($sql);

		return $query_result;
	}
	
	/**
	 * Groups records into buckets.
	 * @param array $records
	 * @return array Grouped records
	 */
	public function groupIntoBuckets($records)
	{
		$resBuckets = $this->prepareBuckets();
		foreach($records as $record) {
			$bucketName = $this->whereFalls($record);
			if (!array_key_exists($bucketName, $resBuckets)) {
				$resBuckets[$bucketName] = 0;
			}
			$resBuckets[$bucketName]++;
		}
		return $resBuckets;
	}

	/**
	 * Order buckets
	 * @return array Buckets
	 */
	protected function prepareBuckets() 
	{
		$sortOrder = array('0', '1', '2', '3', '4', '5', '6', '7', '8-14', '15-31', '1-2 months', '2-3 months', '3 months+');
		$resBuckets = array();
		foreach($sortOrder as $item) {
			$resBuckets[$item] = 0;
		}
		return $resBuckets;
	}

	/**
	 * Find out bucket name
	 * @param array $record
	 * @return string Bucket name
	 */
	protected function whereFalls($record)
	{
		if ($record->diff_days >= 0 && $record->diff_days <= 7) {
			return $record->diff_days;
		}
		if ($record->diff_days >= 8 && $record->diff_days <= 14) {
			return '8-14';
		}
		if ($record->diff_days >= 15 && $record->diff_days <= 31) {
			return '15-31';
		}
		if ($record->diff_days >= 32 && $record->diff_days <= 60) {
			return '1-2 months';
		}
		if ($record->diff_days >= 61 && $record->diff_days <= 70) {
			return '2-3 months';
		}
		if ($record->diff_days >= 71) {
			return '3 months+';
		}

		return 'unclassified';
	}
}