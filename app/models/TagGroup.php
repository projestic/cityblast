<?php

class TagGroup extends ActiveRecord\Model
{
	static $table = 'tag_group';
	

	static $has_many = array(
			array('tags', 'class' => 'Tag', 'foreign_key' => 'tag_group_id')	
		);
	
}

?>