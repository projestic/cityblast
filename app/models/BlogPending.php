<?php
/**
 * A blog entry.
 *
 * @property int id
 * @property int blog_id
 * @property string|DateTime article_date
 * @property string created_at
 * @property string title
 * @property string description text
 * @property string message mediumtext
 * @property string raw_message mediumtext
 * @property string image
 * @property string content_link
 *
 * @property Blog blog
 */
class BlogPending extends ActiveRecord\Model
{
	static $table = 'blog_pending';
	
	static $belongs_to = array(
		array('blog', 'class' => 'Blog', 'foreign_key' => 'blog_id')
	);

	static $has_many = array(
		array('images', 'class' => 'BlogImagePending', 'foreign_key' => 'blog_entry_id')
	);

	/**
	 * @param int $per_page
	 * @param int $current_page
	 * @param array $options
	 * @return Zend_Paginator
	 */
	public static function getPaginator($per_page, $current_page, $options) {
//		$options	= array(
//			'conditions'	=> $conditions,
//			'joins' 		=> $joins,
//			'order'			=> 'id DESC',
//		);
		$paginator = new Zend_Paginator(new ARPaginator('BlogPending', $options));
		$paginator->setCurrentPageNumber($current_page);
		$paginator->setItemCountPerPage($per_page);
		return $paginator;
	}

}
