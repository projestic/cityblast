<?php
class FranchiseAlias extends ActiveRecord\Model
{
	static $table = 'franchise_alias';

	static $belongs_to = array(
		array('franchise', 'class' => 'Franchise', 'foreign_key' => 'franchise_id')
	);


}