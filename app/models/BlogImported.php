<?php
/**
 * Blog Imported 
 * 
 * Keeps track of content already processed
 */
class BlogImported extends ActiveRecord\Model
{
	static $table = 'blog_imported';
	
	static $belongs_to = array(
		array('blog', 'class' => 'Blog', 'foreign_key' => 'blog_id')
	);
	
	public static function previouslyImported($blog_id, $feed_item)
	{
		$result = BlogImported::find_by_description_sha1(sha1($feed_item->get_description()));
		$result = !empty($result);
		return $result;
	}
	
	public static function registerImport(BlogPending $blogPending)
	{
		$blog_imported = new BlogImported();
		$blog_imported->blog_id = $blogPending->blog_id;
		$blog_imported->article_date = $blogPending->article_date;
		$blog_imported->title = $blogPending->title;
		$blog_imported->content_link = $blogPending->content_link;
		$blog_imported->description_sha1 = sha1($blogPending->description);
		$blog_imported->save();
	}
}
