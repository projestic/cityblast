<?php

class EngageByWeekListing extends ActiveRecord\Model
{
	static $table = 'engage_by_week_listing';

	static $belongs_to = array(
		array('listing', 'class' => 'Listing', 'foreign_key' => 'listing_id')
	);
	
	public function populateDateRange($start, $end)
	{
		Logger::log(__METHOD__, Logger::LOG_TYPE_DEBUG);
		Logger::startSection(Logger::LOG_TYPE_DEBUG);
		
		$start_time = strtotime($start);
		$end_time = strtotime($end);
		
		if ($start_time > $end_time) {
			throw new Exception('Start date must be before the end date');
		}
		
		// Shift to start of week
		$start_time = (date('N', $start_time) != 1) ? strtotime('last monday', $start_time) : $start_time;
		$start_date = date('Y-m-d', $start_time);
		
		
		// Shift to end of week
		$end_time = (date('N', $end_time) != 7) ? strtotime('next sunday', $end_time) : $end_time;
		$end_date = date('Y-m-d', $end_time);
		
		Logger::log(
			'Compiling engagement by week and listing between ' . $start_date . ' and ' . $end_date, 
			Logger::LOG_TYPE_DEBUG
		);
		
		$db_con = static::connection();
		
		$sql = "
			SELECT 
				SUBDATE(ebdl.date, WEEKDAY(date)) AS week_date, 
				ebdl.listing_id,
				DATE_FORMAT(ebdl.date, '%Y'),
				DATE_FORMAT(ebdl.date, '%v'),
				SUM(ebdl.clicks),
				SUM(ebdl.likes),
				SUM(ebdl.comments),
				SUM(ebdl.shares),
				0,
				MAX(ebdl.posts),
				0,
				0,
				0,
				0,
				0
			FROM `engage_by_date_listing` AS ebdl
			WHERE date >= " . $db_con->escape($start_date) . "  
			AND date <= " . $db_con->escape($end_date) . "  
			GROUP BY week_date, ebdl.listing_id
		";
		Blastit_ActiveRecord_AggregationHelper::selectReplaceInto(static::table(), $sql);
		
		$sql = "
			UPDATE `engage_by_week_listing` 
			SET 
				score = (clicks + (likes * 10) + (comments * 15) + (shares * 25))
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		$sql = "
			UPDATE `engage_by_week_listing` 
			SET 
				year = DATE_FORMAT(date, '%Y'),
				week = DATE_FORMAT(date, '%v'),
				clicks_perpost = ROUND(clicks / posts, 3),
				likes_perpost = ROUND(likes / posts, 3),
				comments_perpost = ROUND(comments / posts, 3),
				shares_perpost = ROUND(shares / posts, 3),
				score_perpost = ROUND(score / posts, 3)
			WHERE
				`date` >= " . $db_con->escape($start_date) . " AND
				`date` <= " . $db_con->escape($end_date) . "
		";
		$res = $db_con->query($sql);
		
		Logger::endSection(Logger::LOG_TYPE_DEBUG);
	}
}