<?php
	$ctrl = Zend_Controller_Front::getInstance();
	$router = $ctrl->getRouter(); // returns a rewrite router by default

	$router->addRoute("listing_blast",
		new Zend_Controller_Router_Route("blast/index/:id/",
		array("controller" => "blast", "action" => "index")
		)
	);

	$router->addRoute("payment_type",
		new Zend_Controller_Router_Route("blast/login/:payment_type/",
		array("controller" => "blast", "action" => "login")
		)
	);

	$router->addRoute("clientfindsignup",
		new Zend_Controller_Router_Route("member/clientfind-signup/:publish_city",
		array("controller" => "member", "action" => "clientfindsignup")
		)
	);

	$router->addRoute("home_page_with_affiliate",
		new Zend_Controller_Router_Route("index/index/:aff_id",
		array("controller" => "index", "action" => "index")
		)
	);

	$router->addRoute("clientfindsignupis",
		new Zend_Controller_Router_Route("member/clientfind-signupis/:publish_city",
		array("controller" => "member", "action" => "clientfindsignupis")
		)
	);

	$router->addRoute("MemberType",
		new Zend_Controller_Router_Route("member/signup/:publish_city",
		array("controller" => "member", "action" => "signup")
		)
	);

	$router->addRoute("member_sunshine",
		new Zend_Controller_Router_Route("sunshine",
		array("controller" => "member", "action" => "sunshine")
		)
	);

	$router->addRoute("member_settings",
		new Zend_Controller_Router_Route("member/settings",
		array("controller" => "member", "action" => "settings")
		)
	);

	$router->addRoute("member_settings_thankyou",
		new Zend_Controller_Router_Route("member/settings/:thankyou",
		array("controller" => "member", "action" => "settings")
		)
	);

	$router->addRoute("referral_join",
		new Zend_Controller_Router_Route("member/join/:aff_id",
		array("controller" => "member", "action" => "join")
		)
	);

	$router->addRoute("cityRegister",
		new Zend_Controller_Router_Route("member/city-register",
		array("controller" => "member", "action" => "cityregister")
		)
	);

	$router->addRoute("what_is_cb",
		new Zend_Controller_Router_Route("member/what-is-cityblast",
		array("controller" => "member", "action" => "what_is_cityblast")
		)
	);

	$router->addRoute("facebook_how_it_works_legacy",
		new Zend_Controller_Router_Route("f/how-clientfinder-works",
		array("controller" => "member", "action" => "fb_how_cityblast_works")
		)
	);

	$router->addRoute("facebook_how_it_works_cid_legacy",
		new Zend_Controller_Router_Route("f/how-clientfinder-works/:cid",
		array("controller" => "member", "action" => "fb_how_cityblast_works")
		)
	);

	$router->addRoute("google_how_it_works_legacy",
		new Zend_Controller_Router_Route("g/how-clientfinder-works/",
		array("controller" => "member", "action" => "google_how_cityblast_works")
		)
	);

	$router->addRoute("google_how_it_works_cid_legacy",
		new Zend_Controller_Router_Route("g/how-clientfinder-works/:cid",
		array("controller" => "member", "action" => "google_how_cityblast_works")
		)
	);

	$router->addRoute("facebook_how_it_works",
		new Zend_Controller_Router_Route("f/how-cityblast-works",
		array("controller" => "member", "action" => "fb_how_cityblast_works")
		)
	);

	$router->addRoute("facebook_how_it_works_cid",
		new Zend_Controller_Router_Route("f/how-cityblast-works/:cid",
		array("controller" => "member", "action" => "fb_how_cityblast_works")
		)
	);

	$router->addRoute("google_how_it_works",
		new Zend_Controller_Router_Route("g/how-cityblast-works/",
		array("controller" => "member", "action" => "google_how_cityblast_works")
		)
	);

	$router->addRoute("google_how_it_works_cid",
		new Zend_Controller_Router_Route("g/how-cityblast-works/:cid",
		array("controller" => "member", "action" => "google_how_cityblast_works")
		)
	);

	$router->addRoute("member_how_it_works_legacy",
		new Zend_Controller_Router_Route("member/how-clientfinder-works/",
		array("controller" => "member", "action" => "how_cityblast_works")
		)
	);

	$router->addRoute("PublishOn",
		new Zend_Controller_Router_Route("member/turn_on/:publish_type/",
		array("controller" => "member", "action" => "turn_on")
		)
	);

	$router->addRoute("PublishOff",
		new Zend_Controller_Router_Route("member/turn_off/:publish_type/",
		array("controller" => "member", "action" => "turn_off")
		)
	);

	$router->addRoute("FanpagePublishOn",
		new Zend_Controller_Router_Route("fanpage/on/:fanpage_id",
		array("controller" => "fanpage", "action" => "on")
		)
	);

	$router->addRoute("FanpagePublishOff",
		new Zend_Controller_Router_Route("fanpage/off/:fanpage_id",
		array("controller" => "fanpage", "action" => "off")
		)
	);
	$router->addRoute("FanpageTestPublish",
		new Zend_Controller_Router_Route("fanpage/testblast/:member_id/:fanpage_id",
		array("controller" => "fanpage", "action" => "testblast")
		)
	);
	$router->addRoute("FanpageDelete",
		new Zend_Controller_Router_Route("fanpage/delete/:fanpage_id",
		array("controller" => "fanpage", "action" => "delete")
		)
	);

	$router->addRoute("member_listing",
		new Zend_Controller_Router_Route("member/blast/:id/:page/",
		array("controller" => "member", "action" => "blast","page"=>1)
		)
	);

	$router->addRoute("twshare",
		new Zend_Controller_Router_Route("member/twshare/:id",
		array("controller" => "member", "action" => "twshare")
		)
	);

	$router->addRoute("fbshare",
		new Zend_Controller_Router_Route("member/fbshare/:id",
		array("controller" => "member", "action" => "fbshare")
		)
	);

	$router->addRoute("engag_weekly",
		new Zend_Controller_Router_Route("admin/engagweekly/:week",
			array("controller" => "admin", "action" => "engagweekly")
		)
	);

	$router->addRoute("engag_monthly",
		new Zend_Controller_Router_Route("admin/engagmonthly/:month",
			array("controller" => "admin", "action" => "engagmonthly")
		)
	);

	$router->addRoute("member_update",
		new Zend_Controller_Router_Route("admin/member/:id",
		array("controller" => "admin", "action" => "member")
		)
	);

	$router->addRoute("admin_memberposts",
		new Zend_Controller_Router_Route("admin/memberposts/:member",
			array("controller" => "admin", "action" => "memberposts")
		)
	);

	$router->addRoute("member_update_save",
		new Zend_Controller_Router_Route("member/update/:id",
		array("controller" => "member", "action" => "update")
		)
	);

	$router->addRoute("member_fanpages",
		new Zend_Controller_Router_Route("member/fanpages/:id",
		array("controller" => "member", "action" => "fanpages")
		)
	);

	$router->addRoute("member_update_details",
		new Zend_Controller_Router_Route("admin/update_member_details/:id",
		array("controller" => "admin", "action" => "update_member_details")
		)
	);

	$router->addRoute("referral",
		new Zend_Controller_Router_Route("index/refer-an-agent/",
		array("controller" => "index", "action" => "refer_an_agent")
		)
	);

	$router->addRoute("create_message",
		new Zend_Controller_Router_Route("index/create-your-message/",
		array("controller" => "index", "action" => "create_your_message")
		)
	);

	$router->addRoute("blast_network",
		new Zend_Controller_Router_Route("index/blast-our-network/",
		array("controller" => "index", "action" => "blast_our_network")
		)
	);

	$router->addRoute("sell_fast",
		new Zend_Controller_Router_Route("index/sell-your-listing-fast/",
		array("controller" => "index", "action" => "sell_your_listing_fast")
		)
	);

	$router->addRoute("how_it_works",
		new Zend_Controller_Router_Route("index/how-blasting-works/",
		array("controller" => "index", "action" => "how_blasting_works")
		)
	);

	$router->addRoute("how_a_blast_works",
		new Zend_Controller_Router_Route("index/how-blasting-works/:id",
		array("controller" => "index", "action" => "how_blasting_works")
		)
	);

	$router->addRoute("contact_us",
		new Zend_Controller_Router_Route("index/contact-us",
		array("controller" => "index", "action" => "contact_us")
		)
	);

	$router->addRoute("our_story",
		new Zend_Controller_Router_Route("index/our-story",
		array("controller" => "index", "action" => "our_story")
		)
	);

	$router->addRoute("send_referral",
		new Zend_Controller_Router_Route("index/send-referral",
		array("controller" => "index", "action" => "send_referral")
		)
	);

	$router->addRoute("send_fb_referral",
		new Zend_Controller_Router_Route("index/send-fb-referral",
		array("controller" => "index", "action" => "send_fb_referral")
		)
	);

	$router->addRoute("feedback_submit",
		new Zend_Controller_Router_Route("index/feedback-submit",
		array("controller" => "index", "action" => "feedback_submit")
		)
	);

	$router->addRoute("paging_listing",
		new Zend_Controller_Router_Route("listing/index/page/:page/",
		array("controller" => "listing", "action" => "index")
		)
	);

	$router->addRoute("complimentary_listing",
		new Zend_Controller_Router_Route("listing/comp/:id/",
		array("controller" => "listing", "action" => "comp")
		)
	);

	$router->addRoute("republish",
		new Zend_Controller_Router_Route("listing/republish/:id/",
		array("controller" => "listing", "action" => "republish")
		)
	);

	$router->addRoute("listing_view1",
		new Zend_Controller_Router_Route("listing/view/:id",
		array("controller" => "listing", "action" => "view")
		)
	);


	$router->addRoute("listing_view2",
		new Zend_Controller_Router_Route("listing/view/:id/:mid",
		array("controller" => "listing", "action" => "view")
		)
	);

	$router->addRoute("listing_view3",
		new Zend_Controller_Router_Route("listing/view/:id/:mid/:page_type",
		array("controller" => "listing", "action" => "view")
		)
	);

	$router->addRoute("listing_delete",
		new Zend_Controller_Router_Route("listing/delete/:id/",
		array("controller" => "listing", "action" => "delete")
		)
	);

	$router->addRoute("blast_removefromqueue",
		new Zend_Controller_Router_Route("listing/removefromqueue/:id/",
		array("controller" => "listing", "action" => "removefromqueue")
		)
	);

	$router->addRoute("listing_update",
		new Zend_Controller_Router_Route("listing/update/:id",
		array("controller" => "listing", "action" => "update")
		)
	);

	$router->addRoute("listing_email",
		new Zend_Controller_Router_Route("listing/email/:listing_id",
		array("controller" => "listing", "action" => "email")
		)
	);

	$router->addRoute("listing_preapprove",
		new Zend_Controller_Router_Route("listing/approve/:id",
		array("controller" => "listing", "action" => "approve")
		)
	);

	$router->addRoute("listing_unapprove",
		new Zend_Controller_Router_Route("listing/unapprove/:id",
		array("controller" => "listing", "action" => "unapprove")
		)
	);

	$router->addRoute("listing_approve",
		new Zend_Controller_Router_Route("listing/approve/:id/:inventory_unit_id/:hopper_id",
		array("controller" => "listing", "action" => "approve")
		)
	);

	$router->addRoute("moveup",
		new Zend_Controller_Router_Route("listing/moveup/:id/:hoper_id/",
		array("controller" => "listing", "action" => "moveup")
		)
	);

	$router->addRoute("movedown",
		new Zend_Controller_Router_Route("listing/movedown/:id/:hoper_id/",
		array("controller" => "listing", "action" => "movedown")
		)
	);


	$router->addRoute("edit_content",
		new Zend_Controller_Router_Route("content/edit/:listing_id",
		array("controller" => "content", "action" => "edit")
		)
	);
	
	$router->addRoute("copy_content",
		new Zend_Controller_Router_Route("content/copy-to-mm/:id",
		array("controller" => "content", "action" => "copytomm")
		)
	);

	$router->addRoute("externalcontent_edit",
		new Zend_Controller_Router_Route("externalcontent/edit/:id",
		array("controller" => "externalcontent", "action" => "edit")
		)
	);

	$router->addRoute("externalcontent_blog_edit",
		new Zend_Controller_Router_Route("externalcontent/blogedit/:id",
			array("controller" => "externalcontent", "action" => "blogedit")
		)
	);

	$router->addRoute("edit_group",
		new Zend_Controller_Router_Route("admin/groupadd/:id/",
		array("controller" => "admin", "action" => "groupadd")
		)
	);

	$router->addRoute("apply_promo_code",
		new Zend_Controller_Router_Route("admin/applypromo/:id/",
		array("controller" => "admin", "action" => "applypromo")
		)
	);

	$router->addRoute("edit_franchise",
		new Zend_Controller_Router_Route("franchise/add/:id/",
		array("controller" => "franchise", "action" => "add")
		)
	);
	
	$router->addRoute("delete_franchise",
		new Zend_Controller_Router_Route("franchise/delete/:id/",
		array("controller" => "franchise", "action" => "delete")
		)
	);
	
	$router->addRoute("list_franchise_alias",
		new Zend_Controller_Router_Route("franchise/alias/franchise/:franchise_id/",
		array("controller" => "franchise", "action" => "alias")
		)
	);

	$router->addRoute("edit_franchise_alias",
		new Zend_Controller_Router_Route("franchise/addalias/:id/",
		array("controller" => "franchise", "action" => "addalias")
		)
	);
	$router->addRoute("delete_franchise_alias",
		new Zend_Controller_Router_Route("franchise/deletealias/:id/",
		array("controller" => "franchise", "action" => "deletealias")
		)
	);
	$router->addRoute("list_franchise_email",
		new Zend_Controller_Router_Route("franchise/email/franchise/:franchise_id/",
		array("controller" => "franchise", "action" => "email")
		)
	);

	$router->addRoute("edit_franchise_email",
		new Zend_Controller_Router_Route("franchise/addemail/:id/",
		array("controller" => "franchise", "action" => "addemail")
		)
	);
	
	$router->addRoute("delete_franchise_email",
		new Zend_Controller_Router_Route("franchise/deleteemail/:id/",
		array("controller" => "franchise", "action" => "deleteemail")
		)
	);
	
	$router->addRoute("edit_networkapp",
		new Zend_Controller_Router_Route("networkapp/edit/:id/",
		array("controller" => "networkapp", "action" => "edit")
		)
	);
	
	$router->addRoute("delete_franchise_unknown",
		new Zend_Controller_Router_Route("franchise/unknowndelete/:id/",
		array("controller" => "franchise", "action" => "unknowndelete")
		)
	);
	
	$router->addRoute("edit_city",
		new Zend_Controller_Router_Route("city/add/:id/",
		array("controller" => "city", "action" => "add")
		)
	);

	$router->addRoute("delete_city",
	new Zend_Controller_Router_Route("city/delete/:id/",
		array("controller" => "city", "action" => "delete")
		)
	);

	$router->addRoute("rss_feed",
		new Zend_Controller_Router_Route("rss/feed.xml",
		array("controller" => "feed", "action" => "index")
		)
	);

	$router->addRoute("posts_list_id_only",
		new Zend_Controller_Router_Route("admin/posts/:listing_id",
		array("controller" => "admin", "action" => "posts")
		)
	);

	$router->addRoute("posts",
		new Zend_Controller_Router_Route("admin/posts/:listing_id/:blast_id",
		array("controller" => "admin", "action" => "posts")
		)
	);

	$router->addRoute("book_showing_2",
		new Zend_Controller_Router_Route("booking/index/:listing_id/:mid/:request_type",
		array("controller" => "booking", "action" => "index")
		)
	);

	$router->addRoute("booking_save",
		new Zend_Controller_Router_Route("booking/save/:listing_id/:mid",
		array("controller" => "booking", "action" => "save")
		)
	);

	$router->addRoute("booking_savebox",
		new Zend_Controller_Router_Route("booking/savebox/:listing_id/:mid",
		array("controller" => "booking", "action" => "savebox")
		)
	);

	$router->addRoute("adminlistingsstatus",
		new Zend_Controller_Router_Route("admin/listingstatus/:status",
		array("controller" => "admin", "action" => "listingstatus")
		)
	);
	
	$router->addRoute("adminlistingstype",
		new Zend_Controller_Router_Route("admin/listingtype/:type",
		array("controller" => "admin", "action" => "listingtype")
		)
	);

	/************************************
	$router->addRoute("adminbase",
		new Zend_Controller_Router_Route("admin/:action/:hoper_id/",
		array("controller" => "admin", "action" => "index","hoper"=>"*")
		)
	);
 
	$router->addRoute("admin_set_inventory_id",
		new Zend_Controller_Router_Route("admin/index/:hoper_id",
		array("controller" => "admin", "action" => "index")
		)
	);
	************************************/

	$router->addRoute("admin_bookings",
		new Zend_Controller_Router_Route("admin/bookings/:city_id",
		array("controller" => "admin", "action" => "bookings")
		)
	);

	$router->addRoute("admin_setmemberstatus",
		new Zend_Controller_Router_Route("admin/setmemberstatus/:member_status",
		array("controller" => "admin", "action" => "setmemberstatus")
		)
	);


	$router->addRoute("member_sent_emails",
		new Zend_Controller_Router_Route("admin/emails/bymember/:memberid/*",
		array("controller" => "admin", "action" => "emails")
		)
	);

	$router->addRoute("sent_emails_by_type",
		new Zend_Controller_Router_Route("admin/emails/bytype/:type/*",
		array("controller" => "admin", "action" => "emails")
		)
	);

	$router->addRoute("view_payment",
		new Zend_Controller_Router_Route("admin/payment/:id",
		array("controller" => "admin", "action" => "payment")
		)
	);
	$router->addRoute("view_payable",
		new Zend_Controller_Router_Route("admin/payable/:id",
		array("controller" => "admin", "action" => "payable")
		)
	);
	$router->addRoute("inventory",
		new Zend_Controller_Router_Route("admin/inventory/:id",
		array("controller" => "admin", "action" => "inventory")
		)
	);

	$router->addRoute("hopper",
		new Zend_Controller_Router_Route("admin/hopper/:hopper_id",
		array("controller" => "admin", "action" => "hopper")
		)
	);

	$router->addRoute("frequency",
		new Zend_Controller_Router_Route("admin/frequency/:frequency_id",
		array("controller" => "admin", "action" => "frequency")
		)
	);

	$router->addRoute("rech_by_city",
		new Zend_Controller_Router_Route("admin/reach-by-city",
		array("controller" => "admin", "action" => "reach_by_city")
		)
	);


	$router->addRoute("update_publishing_queue",
		new Zend_Controller_Router_Route("publishingqueue/update/:id",
		array("controller" => "publishingqueue", "action" => "update")
		)
	);

	$router->addRoute("delete_publishing_queue",
		new Zend_Controller_Router_Route("publishingqueue/delete/:id",
		array("controller" => "publishingqueue", "action" => "delete")
		)
	);

	$router->addRoute("retire_publishing_queue",
		new Zend_Controller_Router_Route("publishingqueue/retire/:id",
		array("controller" => "publishingqueue", "action" => "retire")
		)
	);

	$router->addRoute("media_handler",
		new Zend_Controller_Router_Route("media/:image",
		array("controller" => "media", "action" => "index")
		)
	);

	$router->addRoute("demo_whitelabel_base",
		new Zend_Controller_Router_Route("demo/",
		array("controller" => "whitelabel", "action" => "index", "company" => "demo")
		)
	);

	$router->addRoute("demo_whitelabel",
		new Zend_Controller_Router_Route("demo/index",
		array("controller" => "whitelabel", "action" => "index", "company" => "demo")
		)
	);
	$router->addRoute("demo_whitelabel_join",
		new Zend_Controller_Router_Route("demo/join",
		array("controller" => "whitelabel", "action" => "join", "company" => "demo")
		)
	);

	$router->addRoute("remax_whitelabel_base",
		new Zend_Controller_Router_Route("remax/",
		array("controller" => "whitelabel", "action" => "index", "company" => "remax")
		)
	);
	$router->addRoute("remax_whitelabel",
		new Zend_Controller_Router_Route("remax/index",
		array("controller" => "whitelabel", "action" => "index", "company" => "remax")
		)
	);

	$router->addRoute("remax_whitelabel_join",
		new Zend_Controller_Router_Route("remax/join",
		array("controller" => "whitelabel", "action" => "join", "company" => "remax")
		)
	);

	$router->addRoute("test_expired",
		new Zend_Controller_Router_Route("test/expired/",
		array("controller" => "test", "action" => "expired")
		)
	);

	$router->addRoute("test_welcome",
		new Zend_Controller_Router_Route("test/welcome/",
		array("controller" => "test", "action" => "welcome")
		)
	);

	$router->addRoute("test_invoice",
		new Zend_Controller_Router_Route("test/invoice/",
		array("controller" => "test", "action" => "invoice")
		)
	);
	$router->addRoute("test_facebook",
		new Zend_Controller_Router_Route("test/facebook/:id",
		array("controller" => "test", "action" => "facebook")
		)
	);

	$router->addRoute("test_twitter",
		new Zend_Controller_Router_Route("test/twitter/:id",
		array("controller" => "test", "action" => "twitter")
		)
	);

	$router->addRoute("test_linkedin",
		new Zend_Controller_Router_Route("test/linkedin/:id",
		array("controller" => "test", "action" => "linkedin")
		)
	);

	$router->addRoute("pub_testblast",
		new Zend_Controller_Router_Route("publish/testblast/:member_id",
		array("controller" => "test", "action" => "testblast")
		)
	);

	$router->addRoute("pub_facebook",
		new Zend_Controller_Router_Route("publish/facebook/:listing_id/:member_id",
		array("controller" => "test", "action" => "facebook")
		)
	);
	$router->addRoute("pub_twitter",
		new Zend_Controller_Router_Route("publish/twitter/:listing_id/:member_id",
		array("controller" => "test", "action" => "twitter")
		)
	);
	$router->addRoute("pub_linkedin",
		new Zend_Controller_Router_Route("publish/linkedin/:listing_id/:member_id",
		array("controller" => "test", "action" => "linkedin")
		)
	);

	$router->addRoute("url_mapper",
		new Zend_Controller_Router_Route("v/i/:listing_id",
		array("controller" => "urlmapper", "action" => "index")
		)
	);

	$router->addRoute("url_mapper_long",
		new Zend_Controller_Router_Route("v/i/:listing_id/:mid",
		array("controller" => "urlmapper", "action" => "index")
		)
	);

	$router->addRoute("url_mapper_new",
		new Zend_Controller_Router_Route("n/i/:click_source/:listing_id",
		array("controller" => "urlmapper", "action" => "newindex")
		)
	);

	$router->addRoute("url_mapper_new_long",
		new Zend_Controller_Router_Route("n/i/:click_source/:listing_id/:mid",
		array("controller" => "urlmapper", "action" => "newindex")
		)
	);


	$router->addRoute("reset_publish_type",
		new Zend_Controller_Router_Route("member/reset/:publish_type",
		array("controller" => "member", "action" => "reset")
		)
	);


	$router->addRoute("agent_share",
		new Zend_Controller_Router_Route("listing/agentemail/:id",
		array("controller" => "listing", "action" => "agentemail")
		)
	);

	$router->addRoute("delete_comment",
		new Zend_Controller_Router_Route("comments/delete/:id",
		array("controller" => "comments", "action" => "delete")
		)
	);

	$router->addRoute("fanpage_update",
		new Zend_Controller_Router_Route("fanpage/update/:member_id/:id",
		array("controller" => "fanpage", "action" => "update")
		)
	);

	$router->addRoute("fanpage_update1",
		new Zend_Controller_Router_Route("fanpage/update/:member_id",
		array("controller" => "fanpage", "action" => "update")
		)
	);

	$router->addRoute("interaction_fb1",
		new Zend_Controller_Router_Route("i/facebook/:listing_id",
		array("controller" => "interaction", "action" => "facebook")
		)
	);
	$router->addRoute("interaction_fb",
		new Zend_Controller_Router_Route("i/facebook/:listing_id/:booking_agent_id",
		array("controller" => "interaction", "action" => "facebook")
		)
	);

	$router->addRoute("interaction_tw1",
		new Zend_Controller_Router_Route("i/twitter/:listing_id",
		array("controller" => "interaction", "action" => "twitter")
		)
	);
	$router->addRoute("interaction_tw",
		new Zend_Controller_Router_Route("i/twitter/:listing_id/:booking_agent_id",
		array("controller" => "interaction", "action" => "twitter")
		)
	);

	$router->addRoute("interaction_email1",
		new Zend_Controller_Router_Route("i/email/:listing_id",
		array("controller" => "interaction", "action" => "email")
		)
	);
	$router->addRoute("interaction_email",
		new Zend_Controller_Router_Route("i/email/:listing_id/:booking_agent_id",
		array("controller" => "interaction", "action" => "email")
		)
	);

	$router->addRoute("members_only",
		new Zend_Controller_Router_Route("blast/members-only/",
		array("controller" => "blast", "action" => "membersonly")
		)
	);

	$router->addRoute("member_renewal",
		new Zend_Controller_Router_Route("member/payment-renewal",
		array("controller" => "member", "action" => "paymentrenewal", "rebill" => 1)
		)
	);

	$router->addRoute("member_renewal2",
		new Zend_Controller_Router_Route("member/paymentrenewal/",
		array("controller" => "member", "action" => "paymentrenewal", "rebill" => 1)
		)
	);

	$router->addRoute("cancel_account",
		new Zend_Controller_Router_Route("member/cancelaccount/:id",
		array("controller" => "member", "action" => "cancelaccount")
		)
	);

	$router->addRoute("admin_cancel_account",
		new Zend_Controller_Router_Route("admin/cancelaccount/:id",
		array("controller" => "admin", "action" => "cancelaccount")
		)
	);
	$router->addRoute("admin_payment",
		new Zend_Controller_Router_Route("admin/newcreditcard/:id",
		array("controller" => "admin", "action" => "newcreditcard")
		)
	);
	$router->addRoute("admin_payable",
		new Zend_Controller_Router_Route("payable/index",
		array("controller" => "payable", "action" => "index")
		)
	);
	$router->addRoute("sales_facebook",
		new Zend_Controller_Router_Route("f/index",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1183)
		)
	);

	$router->addRoute("sales_facebook_root",
		new Zend_Controller_Router_Route("f",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1183)
		)
	);

	$router->addRoute("sales_leila",
		new Zend_Controller_Router_Route("leila",
		array("controller" => "member", "action" => "join", "aff_id"=>55, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_leila_index",
		new Zend_Controller_Router_Route("leila/index",
		array("controller" => "member", "action" => "join", "aff_id"=>55, "is_royal"=>0)
		)
	);  

	$router->addRoute("sales_luke",
		new Zend_Controller_Router_Route("luke",
		array("controller" => "member", "action" => "join", "aff_id"=>75, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_luke_index",
		new Zend_Controller_Router_Route("luke/index",
		array("controller" => "member", "action" => "join", "aff_id"=>75, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_tiffany",
		new Zend_Controller_Router_Route("tiffany",
		array("controller" => "member", "action" => "join", "aff_id"=>343, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_tiffany_index",
		new Zend_Controller_Router_Route("tiffany/index",
		array("controller" => "member", "action" => "join", "aff_id"=>343, "is_royal"=>0)
		)
	);  

	$router->addRoute("referral_join",
		new Zend_Controller_Router_Route("member/videojoin/:aff_id",
		array("controller" => "member", "action" => "videojoin")
		)
	);

	$router->addRoute("sales_brixworks",
		new Zend_Controller_Router_Route("brixwork",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1367, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_brixworks_index",
		new Zend_Controller_Router_Route("brixwork/index",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1367, "is_royal"=>0)
		)
	);  

	$router->addRoute("sales_shelley",
		new Zend_Controller_Router_Route("shelley",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1442, "is_royal"=>0)
		)
	);
		
	$router->addRoute("sales_shelley_index",
		new Zend_Controller_Router_Route("shelley/index",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1442, "is_royal"=>0)
		)
	); 

	$router->addRoute("sales_david",
		new Zend_Controller_Router_Route("david",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1476, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_david_index",
		new Zend_Controller_Router_Route("david/index",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1476, "is_royal"=>0)
		)
	); 

	$router->addRoute("sales_natasha",
		new Zend_Controller_Router_Route("natasha",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1449, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_natasha_index",
		new Zend_Controller_Router_Route("natasha/index",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1449, "is_royal"=>0)
		)
	);  

	$router->addRoute("sales_seevirtual ",
		new Zend_Controller_Router_Route("seevirtual",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1344, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_seevirtual _index",
		new Zend_Controller_Router_Route("seevirtual/index",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1344, "is_royal"=>0)
		)
	); 

	$router->addRoute("sales_grace",
		new Zend_Controller_Router_Route("grace",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>581, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_grace_index",
		new Zend_Controller_Router_Route("grace/index",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>581, "is_royal"=>0)
		)
	);  

	$router->addRoute("sales_mike",
		new Zend_Controller_Router_Route("mike",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1281, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_mike_index",
		new Zend_Controller_Router_Route("mike/index",
		array("controller" => "member", "action" => "videojoin", "aff_id"=>1281, "is_royal"=>0)
		)
	);

	$router->addRoute("sales_james_kupka",
		new Zend_Controller_Router_Route("join",
		array("controller" => "join", "action" => "index", "aff_id"=>1000452, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_james_kupka_index",
		new Zend_Controller_Router_Route("join/index",
		array("controller" => "join", "action" => "index", "aff_id"=>1000452, "is_royal"=>0)
		)
	);

	$router->addRoute("sales_rlp",
		new Zend_Controller_Router_Route("rlp",
		array("controller" => "affiliate", "action" => "royal", "aff_id"=>1182, "is_royal"=>1)
		)
	);
	$router->addRoute("sales_rlp_index",
		new Zend_Controller_Router_Route("rlp/index",
		array("controller" => "affiliate", "action" => "royal", "aff_id"=>1182, "is_royal"=>1)
		)
	);   

	$router->addRoute("sales_royal",
		new Zend_Controller_Router_Route("royal",
		array("controller" => "affiliate", "action" => "royal", "aff_id"=>1182, "is_royal"=>1)
		)
	);
	$router->addRoute("sales_royal_index",
		new Zend_Controller_Router_Route("royal/index",
		array("controller" => "affiliate", "action" => "royal", "aff_id"=>1182, "is_royal"=>1)
		)
	);  

	$router->addRoute("sales_royal_lepage",
		new Zend_Controller_Router_Route("royallepage",
		array("controller" => "affiliate", "action" => "royal", "aff_id"=>1182, "is_royal"=>1)
		)
	);
	$router->addRoute("sales_royal_lepage_index",
		new Zend_Controller_Router_Route("royallepage/index",
		array("controller" => "affiliate", "action" => "royal", "aff_id"=>1182, "is_royal"=>1)
		)
	);  

	$router->addRoute("sales_geraldlucas",
		new Zend_Controller_Router_Route("property",
		array("controller" => "affiliate", "action" => "property", "aff_id"=>1003549, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_geraldlucas_index",
		new Zend_Controller_Router_Route("property/index",
		array("controller" => "affiliate", "action" => "property", "aff_id"=>1003549, "is_royal"=>0)
		)
	);  

	$router->addRoute("sales_estatevue",
		new Zend_Controller_Router_Route("estatevue",
		array("controller" => "affiliate", "action" => "estatevue", "aff_id"=>9999, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_estatevue_index",
		new Zend_Controller_Router_Route("estatevue/index",
		array("controller" => "affiliate", "action" => "estatevue", "aff_id"=>9999, "is_royal"=>0)
		)
	);  

	$router->addRoute("sales_boost",
		new Zend_Controller_Router_Route("boost",
		array("controller" => "affiliate", "action" => "boost", "aff_id"=>798, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_boost_index",
		new Zend_Controller_Router_Route("boost/index",
		array("controller" => "affiliate", "action" => "boost", "aff_id"=>798, "is_royal"=>0)
		)
	); 

	$router->addRoute("sales_seevirtual",
		new Zend_Controller_Router_Route("seevirtual",
		array("controller" => "affiliate", "action" => "seevirtual", "aff_id"=>1344, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_seevirtual_index",
		new Zend_Controller_Router_Route("seevirtual/index",
		array("controller" => "affiliate", "action" => "seevirtual", "aff_id"=>1344, "is_royal"=>0)
		)
	);  

	$router->addRoute("sales_retechulous",
		new Zend_Controller_Router_Route("retechulous",
		array("controller" => "affiliate", "action" => "retechulous", "aff_id"=>1000018, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_retechulous_index",
		new Zend_Controller_Router_Route("retechulous/index",
		array("controller" => "affiliate", "action" => "retechulous", "aff_id"=>1000018, "is_royal"=>0)
		)
	);

	$router->addRoute("sales_crew",
		new Zend_Controller_Router_Route("crew",
		array("controller" => "affiliate", "action" => "crew", "aff_id"=>1002554, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_agentpartner",
		new Zend_Controller_Router_Route("agentpartner",
		array("controller" => "affiliate", "action" => "agentpartner", "aff_id"=>1002916, "is_royal"=>0)
		)
	); 

	$router->addRoute("sales_hoss",
		new Zend_Controller_Router_Route("listingboss",
		array("controller" => "affiliate", "action" => "listingboss", "aff_id"=>1002106, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_hoss2",
		new Zend_Controller_Router_Route("hoss",
		array("controller" => "affiliate", "action" => "listingboss", "aff_id"=>1002106, "is_royal"=>0)
		)
	);

	$router->addRoute("sales_lonewolf",
		new Zend_Controller_Router_Route("lonewolf",
		array("controller" => "affiliate", "action" => "lonewolf", "aff_id"=>999999, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_lonewolf2",
		new Zend_Controller_Router_Route("lwolf",
		array("controller" => "affiliate", "action" => "lonewolf", "aff_id"=>999999, "is_royal"=>0)
		)
	);

	$router->addRoute("sales_lonewolf_index",
		new Zend_Controller_Router_Route("lonewolf/index",
		array("controller" => "affiliate", "action" => "lonewolf", "aff_id"=>999999, "is_royal"=>0)
		)
	);  

	$router->addRoute("sales_ouellette",
		new Zend_Controller_Router_Route("coach",
		array("controller" => "affiliate", "action" => "coach", "aff_id"=>1002741, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_ouellette_index",
		new Zend_Controller_Router_Route("coach/index",
		array("controller" => "affiliate", "action" => "coach", "aff_id"=>1002741, "is_royal"=>0)
		)
	);

	$router->addRoute("sales_realbies",
		new Zend_Controller_Router_Route("realbies",
		array("controller" => "affiliate", "action" => "realbies", "aff_id"=>1000531, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_realbies_index",
		new Zend_Controller_Router_Route("realbies/index",
		array("controller" => "affiliate", "action" => "realbies", "aff_id"=>1000531, "is_royal"=>0)
		)
	);

	$router->addRoute("webinar",
		new Zend_Controller_Router_Route("webinar",
		array("controller" => "webinar", "action" => "index", "is_royal"=>0)
		)
	);
	$router->addRoute("webinar_index",
		new Zend_Controller_Router_Route("webinar/index",
		array("controller" => "webinar", "action" => "index", "is_royal"=>0)
		)
	);

	$router->addRoute("webinar_insider",
		new Zend_Controller_Router_Route("free-webinar-insider-secrets",
		array("controller" => "webinar", "action" => "index", "is_royal"=>0)
		)
	);
	$router->addRoute("webinar_insider_index",
		new Zend_Controller_Router_Route("free-webinar-insider-secrets/index",
		array("controller" => "webinar", "action" => "index", "is_royal"=>0)
		)
	);

	$router->addRoute("webinar_watch",
		new Zend_Controller_Router_Route("webinar/watch",
		array("controller" => "webinar", "action" => "watch", "is_royal"=>0)
		)
	);
	$router->addRoute("webinar_watch",
		new Zend_Controller_Router_Route("webinar/watch",
		array("controller" => "webinar", "action" => "watch", "is_royal"=>0)
		)
	);

	$router->addRoute("webinar_insider_watch",
		new Zend_Controller_Router_Route("free-webinar-insider-secrets/watch",
		array("controller" => "webinar", "action" => "watch", "is_royal"=>0)
		)
	);
	$router->addRoute("webinar_insider_watch",
		new Zend_Controller_Router_Route("free-webinar-insider-secrets/watch",
		array("controller" => "webinar", "action" => "watch", "is_royal"=>0)
		)
	);

	/****
	$router->addRoute("sales_webinar_james_kupka",
		new Zend_Controller_Router_Route("webinar",
		array("controller" => "affiliate", "action" => "webinar", "aff_id"=>1000452, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_webinar_james_kupka_index",
		new Zend_Controller_Router_Route("webinar/index",
		array("controller" => "affiliate", "action" => "webinar", "aff_id"=>1000452, "is_royal"=>0)
		)
	);
	***/

	$router->addRoute("sales_special_david_gutierrez",
		new Zend_Controller_Router_Route("special",
		array("controller" => "affiliate", "action" => "special", "aff_id"=>1002979, "is_royal"=>0)
		)
	); 

	/*
	$router->addRoute("sales_facebook_index",
		new Zend_Controller_Router_Route("f/index",
		array("controller" => "member", "action" => "join", "aff_id"=>1183)
		)
	);   
 

	$router->addRoute("sales_google_index",
		new Zend_Controller_Router_Route("g/index",
		array("controller" => "member", "action" => "join", "aff_id"=>1184)
		)
	);		*/


	$router->addRoute("sales_vision",
		new Zend_Controller_Router_Route("vision",
		array("controller" => "affiliate", "action" => "vision", "aff_id"=>1001027, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_vision_index",
		new Zend_Controller_Router_Route("vision/index",
		array("controller" => "affiliate", "action" => "vision", "aff_id"=>1001027, "is_royal"=>0)
		)
	);   

	$router->addRoute("sales_realestate_education_center",
		new Zend_Controller_Router_Route("rec",
		array("controller" => "affiliate", "action" => "recvideojoin", "aff_id"=>191, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_realestate_education_center_index",
		new Zend_Controller_Router_Route("rec/index",
		array("controller" => "affiliate", "action" => "recvideojoin", "aff_id"=>191, "is_royal"=>0)

		)
	);

	$router->addRoute("chris_leader",
		new Zend_Controller_Router_Route("leader",
		array("controller" => "affiliate", "action" => "leader", "aff_id"=>1742, "is_royal"=>0)
		)
	);
	$router->addRoute("chris_leader_index",
		new Zend_Controller_Router_Route("leader/index",
		array("controller" => "affiliate", "action" => "leader", "aff_id"=>1742, "is_royal"=>0)

		)
	);   

	$router->addRoute("garrett",
		new Zend_Controller_Router_Route("garrett",
		array("controller" => "affiliate", "action" => "garrett", "aff_id"=>1003200, "is_royal"=>0)
		)
	);
	$router->addRoute("garrett_index",
		new Zend_Controller_Router_Route("garrett/index",
		array("controller" => "affiliate", "action" => "garrett", "aff_id"=>1003200, "is_royal"=>0)

		)
	);

	$router->addRoute("william_ripley",
		new Zend_Controller_Router_Route("takeaction",
		array("controller" => "affiliate", "action" => "ripley", "aff_id"=>1003098, "is_royal"=>0)
		)
	);
	$router->addRoute("william_ripley_index",
		new Zend_Controller_Router_Route("takeaction/index",
		array("controller" => "affiliate", "action" => "ripley", "aff_id"=>1003098, "is_royal"=>0)

		)
	);

	$router->addRoute("sales_realestatetalkshow_center",
		new Zend_Controller_Router_Route("realestatetalkshow",
		array("controller" => "affiliate", "action" => "recvideojoin", "aff_id"=>191, "is_royal"=>0)
		)
	);
	$router->addRoute("sales_realestatetalkshow_index",
		new Zend_Controller_Router_Route("realestatetalkshow/index",
		array("controller" => "affiliate", "action" => "recvideojoin", "aff_id"=>191, "is_royal"=>0)
		)
	);   

	$router->addRoute("admin_member_payments",
		new Zend_Controller_Router_Route("admin/mpayments/:id",
		array("controller" => "admin", "action" => "mpayments")
		)
	);

	$router->addRoute("publish_settings",
		new Zend_Controller_Router_Route("publishsettings/index",
		array("controller" => "publishsettings", "action" => "index")
		)
	);

	$router->addRoute("publish_settings_member",
		new Zend_Controller_Router_Route("publishsettings/member/:id",
		array("controller" => "publishsettings", "action" => "member")
		)
	); 

	$router->addRoute("member_emails",
		new Zend_Controller_Router_Route("admin/memberemails/:id",
		array("controller" => "admin", "action" => "memberemails")
		)
	);
	$router->addRoute("admin_hopper_select",
		new Zend_Controller_Router_Route("admin/gethopper/:listing_id",
		array("controller" => "admin", "action" => "gethopper")
		)
	);

	$router->addRoute("admin_pubassignment",
		new Zend_Controller_Router_Route("admin/pubassignment/:listing_id",
		array("controller" => "admin", "action" => "pubassignment")
		)
	);

	$router->addRoute("admin_publish",
		new Zend_Controller_Router_Route("admin/publish/:listing_id",
		array("controller" => "admin", "action" => "publish")
		)
	);

	$router->addRoute("force_member_payment",
		new Zend_Controller_Router_Route("admin/memberpayment/:id",
		array("controller" => "admin", "action" => "memberpayment")
		)
	);

	$router->addRoute("lp_the_real_estate_talk_show",
		new Zend_Controller_Router_Route("lp/the-real-estate-talk-show/",
		array("controller" => "lp", "action" => "the_real_estate_talk_show")
		)
	);

	$router->addRoute("tab_settings",
		new Zend_Controller_Router_Route("member/tab_settings",
		array("controller" => "member", "action" => "tab_settings")
		)
	);

	$router->addRoute("tab_contact",
		new Zend_Controller_Router_Route("member/tab_contact",
		array("controller" => "member", "action" => "tab_contact")
		)
	);
	$router->addRoute("tab_bookings",
		new Zend_Controller_Router_Route("member/tab_bookings",
		array("controller" => "member", "action" => "tab_bookings")
		)
	);
	$router->addRoute("tab_invoices",
		new Zend_Controller_Router_Route("member/tab_invoices",
		array("controller" => "member", "action" => "tab_invoices")
		)
	);
	$router->addRoute("tab_listings",
		new Zend_Controller_Router_Route("member/tab_listings/:page",
		array("controller" => "member", "action" => "tab_listings")
		)
	);
	$router->addRoute("tab_dashboard",
		new Zend_Controller_Router_Route("member/tab_dashboard",
		array("controller" => "member", "action" => "tab_dashboard")
		)
	);
	$router->addRoute("tab_referagent",
		new Zend_Controller_Router_Route("member/tab_referagent",
		array("controller" => "member", "action" => "tab_referagent")
		)
	);
	$router->addRoute("affiliate_link",
		new Zend_Controller_Router_Route("a/i/:aff_id",
		array("controller" => "affiliate", "action" => "index")
		)
	);

	$router->addRoute("affiliate_link2",
		new Zend_Controller_Router_Route("a/i",
		array("controller" => "affiliate", "action" => "index")
		)
	);

	$router->addRoute("MemberType",
		new Zend_Controller_Router_Route("member/cancelconfirm/:reason",
		array("controller" => "member", "action" => "cancelconfirm")
		)
	);

	$router->addRoute("facebook_videoland",
		new Zend_Controller_Router_Route("f/join/:price_id",
		array("controller" => "member", "action" => "fb_video_lander")
		)
	);

	$router->addRoute("facebook_promo",
		new Zend_Controller_Router_Route("f/promo/:promo_id",
		array("controller" => "member", "action" => "fb_promo")
		)
	);

	$router->addRoute("promo_update",
		new Zend_Controller_Router_Route("promo/update/:id",
		array("controller" => "promo", "action" => "update")
		)
	);

	$router->addRoute("promo_delete",
		new Zend_Controller_Router_Route("promo/delete/:id",
		array("controller" => "promo", "action" => "delete")
		)
	);


	$router->addRoute("landing_page_group",
		new Zend_Controller_Router_Route("lp/:group_name/:page_name",
			array("controller" => "lp", "action" => "grouppage")
		)
	);


	$router->addRoute("landing_page_group_join",
		new Zend_Controller_Router_Route("lp/:group_name/:page_name/join",
			array("controller" => "member", "action" => "join", "wizard_responsive" => "0")
		)
	);

	$router->addRoute("landing_page_group_member_info",
		new Zend_Controller_Router_Route("lp/:group_name/:page_name/info",
			array("controller" => "member", "action" => "info", "wizard_responsive" => "0")
		)
	);

	$router->addRoute("landing_page_group_payment",
		new Zend_Controller_Router_Route("lp/:group_name/:page_name/payment",
			array("controller" => "member", "action" => "payment")
		)
	);

	$router->addRoute("landing_page_group_thankyou",
		new Zend_Controller_Router_Route("lp/:group_name/:page_name/thankyou",
			array("controller" => "member", "action" => "thankyou", 'thankyou' => '1')
		)
	);
	
	$router->addRoute("temp_lp_join_fix",
		new Zend_Controller_Router_Route("lp/ join/:page_name",
			array(
				"controller" => "lp", 
				"action" => "grouppage",
				"group_name" => "join"
			)
		)
	);
	


	$router->addRoute("sample_popup",
		new Zend_Controller_Router_Route("content/sample/:tag_id",
			array("controller" => "listing", "action" => "sample")
		)
	);
	
	$router->addRoute("promo_code",
		new Zend_Controller_Router_Route("promo/code/:promo_code",
			array("controller" => "promo", "action" => "code")
		)
	);
	
	$router->addRoute("tmp_store_settings",
		new Zend_Controller_Router_Route("member/tmp_store_settings",
			array("controller" => "member", "action" => "tmpstoresettings")
		)
	);
	
	$router->addRoute("lonewolf_sync",
		new Zend_Controller_Router_Route("lwolf/sync/:lw_id",
			array("controller" => "lwolf", "action" => "sync")
		)
	);
	
	$router->addRoute("help",
		new Zend_Controller_Router_Route("help/:section",
			array("controller" => "help", "action" => "view")
		)
	);

	$router->addRoute("sso1",
		new Zend_Controller_Router_Route("sso/:idp",
			array("controller" => "sso", 'action' => 'index')
		)
	);

	$router->addRoute("sso2",
		new Zend_Controller_Router_Route("sso/:action/:idp",
			array("controller" => "sso")
		)
	);

	$router->addRoute("fanpage_contentfill",
		new Zend_Controller_Router_Route("fanpage/contentfill/:member_id/:fanpage_id",
			array("controller" => "fanpage", 'action' => 'contentfill')
		)
	);

	$router->addRoute("personal_contentfill",
		new Zend_Controller_Router_Route("admin/contentfill/:member_id",
			array("controller" => "admin", 'action' => 'contentfill')
		)
	);	
	
	$router->addRoute("listing_showcase",
		new Zend_Controller_Router_Route("admin/listingshowcase/:listing_id",
			array("controller" => "admin", 'action' => 'listingshowcase')
		)
	);	

	$router->addRoute("facebook_direct_publish_test",
		new Zend_Controller_Router_Route("test/facebook_direct/:member_id/:listing_id",
			array("controller" => "test", 'action' => 'facebook_direct')
		)
	);	

	$router->addRoute("fanpage_direct_publish_test",
		new Zend_Controller_Router_Route("test/fanpage_direct/:member_id/:listing_id",
			array("controller" => "test", 'action' => 'fanpage_direct')
		)
	);

	$router->addRoute("100_greatest",
		new Zend_Controller_Router_Route("100greatest",
			array("controller" => "greatest", 'action' => 'index')
		)
	);

	$router->addRoute("100_greatest_thanks",
		new Zend_Controller_Router_Route("100greatest/thanks",
			array("controller" => "greatest", 'action' => 'thanks')
		)
	);	

