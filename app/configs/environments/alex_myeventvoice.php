<?php
	
	// Service constants
	define('COMPANY_NAME', "MyEventVoice"); // company name without .com
	define('COMPANY_WEBSITE', "MyEventVoice.com"); // stylized website for footers and such
	define('COMPANY_ADDRESS', "642 King Street West, Unit 104");
	define('COMPANY_CITY', "Toronto");
	define('COMPANY_PROVINCE', "Ontario");
	define('COMPANY_POSTAL', "M5V 1M7");
	define('COMPANY_COUNTRY', "Canada");
	define('COMPANY_TAX_TYPE', "HST");
	define('COMPANY_TAX_ID', "809268808");

	define('DOMAIN', 'alex.myeventvoice.com');
	define('APP_URL', 'http://alex.myeventvoice.com'); 			
	define('IMG_URL', 'http://alex.myeventvoice.com/');
	define('APP_DOMAIN', 'alex.myeventvoice.com'); 		
	define('ADMIN_EMAIL', 'alex@cityblast.com');  
	define('COOKIE_DOMAIN', 'alex.myeventvoice.com');  
	
	define('GOOGLE_MAPS_API','ABQIAAAA7yqQ_SgKt3uPB4KiIEWychTV3vwQmz4vrIpfugnJ0BE_ijwvWRRLCPBUh5AgLngWk9Fr4POt4OyrFg');

	define('NUMBER_OF_HOPPERS', 3);	

	define('MAILCHIMP_API_KEY', '');
	define('MAILCHIMP_LIST_ID', '');	

	//Google Analytics Tracking - CITYBLAST
	define('GOOGLE_ANALYTICS', '');
	define('GOOGLE_CONVERSION_ID', '');

	define('TOLL_FREE', '1-888-712-7888');
	define('HELP_EMAIL', 'info@myeventvoice.com');

	//Stripe - TESTING
	define('STRIPE_SECRET', 'bWdSiy7Pw5SfqQcC7NkObAhHxZXJl8bR');
	define('STRIPE_PUBLISH', 'pk_FUFPUPA1lxWdLDZdcYsMb9nMDXV8a');
	
	define('AMAZON_AWS_API_KEY', 'FAKE');
	define('AMAZON_AWS_API_SECRET', 'FAKE');
	
	define('MANDRILL_API_KEY','846813ae-a336-4672-b9fd-fa38826fef70');
	
	define('HST', '809268808');	
	
	define('LIKE_WEIGHT', 30);
	define('COMMENT_WEIGHT', 50);
	define('SHARE_WEIGHT', 75);
	define('BUZZ_WEIGHT', 1000);	
			
	define('TYPEKIT','nps1ord');