<?php
	# Facebook Constants
	
	// Service constants
	define('COMPANY_NAME', "CityBlast"); // company name without .com
	define('COMPANY_WEBSITE', "CityBlast.com"); // stylized website for footers and such
	define('COMPANY_ADDRESS', "326 Adelaide St W., Suite 400");
	define('COMPANY_CITY', "Toronto");
	define('COMPANY_PROVINCE', "Ontario");
	define('COMPANY_POSTAL', "M5V 1R3");
	define('COMPANY_COUNTRY', "Canada");
	define('COMPANY_TAX_TYPE', "HST");
	define('COMPANY_TAX_ID', "809268808");

	define('DOMAIN', 'alex.mortgagemingler.com');
	define('APP_URL', 'http://alex.mortgagemingler.com'); 			
	define('IMG_URL', 'http://alex.mortgagemingler.com/');
	define('APP_DOMAIN', 'alex.mortgagemingler.com'); 		
	define('ADMIN_EMAIL', 'alex@cityblast.com');  
	define('COOKIE_DOMAIN', 'alex.mortgagemingler.com');  
	
	define('GOOGLE_MAPS_API','ABQIAAAA7yqQ_SgKt3uPB4KiIEWychTV3vwQmz4vrIpfugnJ0BE_ijwvWRRLCPBUh5AgLngWk9Fr4POt4OyrFg');

	define('NUMBER_OF_HOPPERS', 3);	
	
	//Google Analytics Tracking - CITYBLAST
	define('GOOGLE_ANALYTICS', 'UA-37894320-1');
	define('GOOGLE_CONVERSION_ID', '956260748');
	
	define('TOLL_FREE', '1-888-712-7888');
	define('HELP_EMAIL', 'info@cityblast.com');

	//Stripe - TESTING
	define('STRIPE_SECRET', 'bWdSiy7Pw5SfqQcC7NkObAhHxZXJl8bR');
	define('STRIPE_PUBLISH', 'pk_FUFPUPA1lxWdLDZdcYsMb9nMDXV8a');
	
	define('AMAZON_AWS_API_KEY', 'FAKE');
	define('AMAZON_AWS_API_SECRET', 'FAKE');
	
	define('HST', '809268808');	
		
	define('LIKE_WEIGHT', 30);
	define('COMMENT_WEIGHT', 50);
	define('SHARE_WEIGHT', 75);
	define('BUZZ_WEIGHT', 1000);	
			
?>