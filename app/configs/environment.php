<?php

#---------------------------------------------------------------------------
# Defines
#---------------------------------------------------------------------------



// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/..'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'alen'));


//echo APPLICATION_ENV . "<BR><BR>";
//exit();



#---------------------------------------------------------------------------
# Paths
#---------------------------------------------------------------------------

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../lib'),
    realpath(APPLICATION_PATH . '/../lib/GoogleAnalytics'),
    realpath(APPLICATION_PATH . '/../vendor'),
    realpath(APPLICATION_PATH . '/../vendor/facebook'),
    realpath(APPLICATION_PATH . '/models'), # Need to put this here because the Active Record Autoloader sucks, and suppresses errors
    get_include_path(),
)));


require_once 'Logger.php';
Logger::setHtml(true);
Logger::setEcho(false);
Logger::log(__FILE__);


#---------------------------------------------------------------------------
# Requires
#---------------------------------------------------------------------------


require_once 'Zend/Application.php';


#
# Need the application so we have the autoloader available for the
# remainder of the requires
#

$config_file = APPLICATION_PATH . '/configs/application.ini';
$application = new Zend_Application(
    APPLICATION_ENV,
    $config_file
);

// Define path to application directory
$options =   $application->getOptions();
defined('APPLICATION_DOCROOT')
    || define('APPLICATION_DOCROOT', $options['common']['doc_root']);

require_once 'active_record/ActiveRecord.php';

require_once APPLICATION_PATH . '/configs/routes.php';

require_once APPLICATION_PATH . '/controllers/ApplicationController.php';

// Only grab our utils if we are dev'ing
require_once APPLICATION_PATH . '/../lib/application_utils.php';



#---------------------------------------------------------------------------
# Extensions
#---------------------------------------------------------------------------

require_once APPLICATION_PATH . '/configs/constants.php';

require_once APPLICATION_PATH . '/configs/environments/' . APPLICATION_ENV . '.php';

// application/Bootstrap.php
require_once 'extensions/Zend/Zend_View_Extended.php';

// active record paginator
require_once 'extensions/Zend/Zend_Paginator_Adapter_Active_Record.php';

require_once 'extensions/Zend/Zend_Controller_Dispatcher_Extended.php';





#---------------------------------------------------------------------------
# Setup
#---------------------------------------------------------------------------

// Create application, bootstrap, and run

Zend_Registry::set('application', $application); // so web app can run it, console app will not


$application->bootstrap();

?>