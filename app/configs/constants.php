<?php
	# APPLICATION_PATH   defined in environment.php
	# APPLICATION_ENV    defined in environment.php  development, staging, production etc.
	
	define('VIEW_SUFFIX', 'html.php');
	
	# Set this so Active Record can use Zend Log
	define('PEAR_LOG_INFO', Zend_Log::INFO);
	
	# Change this to true for seeing all the nasty sql calls per request you are doing
	define('LOG_SQL', false);
   
    
	//Facebook Permissions
//	$permissions[] = "offline_access";
	$permissions[] = "email";
	
	$permissions[] = "user_activities";
	//$permissions[] = "user_checkins";
	$permissions[] = "user_education_history";
	$permissions[] = "user_events";
	$permissions[] = "user_groups";
	//$permissions[] = "user_status";
	$permissions[] = "user_hometown";

	$permissions[] = "manage_pages";
	
	$permissions[] = "user_interests";
	$permissions[] = "user_photos";	
	$permissions[] = "user_likes";
	$permissions[] = "user_birthday";
	//$permissions[] = "user_relationships";
	//$permissions[] = "user_relationship_details";
	//$permissions[] = "read_friendlists";
	//$permissions[] = "read_insights";
	//$permissions[] = "user_religion_politics";
	$permissions[] = "user_about_me";
	$permissions[] = "user_hometown";
	$permissions[] = "user_location";
	$permissions[] = "publish_actions"; // Needed to publish status
	$permissions[] = "read_stream"; // Needed to count shares

	$permissions[] = "friends_activities";
	//$permissions[] = "friends_about_me";
	$permissions[] = "friends_birthday";
	//$permissions[] = "friends_checkins";
	$permissions[] = "friends_education_history";
	//$permissions[] = "friends_events";
	$permissions[] = "friends_groups";
	$permissions[] = "friends_hometown";
	$permissions[] = "friends_interests";
	$permissions[] = "friends_likes";
	//$permissions[] = "friends_location";
	$permissions[] = "friends_relationships";
	//$permissions[] = "friends_relationship_details";
	//$permissions[] = "friends_religion_politics";

	
	define("FB_SCOPE", implode(",", $permissions));
?>