<?php

require realpath(dirname(__FILE__) . '/../env.php');
require realpath(dirname(__FILE__) . '/../../app/configs/environment.php');

if (!extension_loaded('soap')) {
	throw new Exception('The "soap" extension is not enabled');
}

ini_set("soap.wsdl_cache_enabled", "0");

$wsdl = dirname(__FILE__) . '/ProvisionServer.wsdl.orig';
$server = new Zend_Soap_Server($wsdl);
$server->setClassMap(Blastit_Clareity_Service_Provision::$classmap);
$server->setClass('Blastit_Clareity_Service_Provision', $server);

$postdata = file_get_contents('php://input');
$parser = xml_parser_create('UTF-8'); 
if (!xml_parse($parser,$postdata,true)) { 
	$server->fault('500', 'Cannot parse XML: '. 
		xml_error_string(xml_get_error_code($parser)). 
		' at line: ' . xml_get_current_line_number($parser). 
		', column: ' . xml_get_current_column_number($parser));
}

$server->handle();
$response = $server->getLastResponse();
$request = $server->getLastRequest();


$log_data = array(
	'time' => date('Y-m-d H:i:S'),
	'gmtime' => gmdate('Y-m-d H:i:S'),
	'client' => !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'unknown',
	'request' => $request,
	'response' => $response
);
file_put_contents(APPLICATION_PATH . '/../log/clareity_provision.log', print_r($log_data, true), FILE_APPEND);