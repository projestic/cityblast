<?php

if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();

if( file_exists( dirname(__FILE__)."/env.php") ) 
{	
	include_once dirname(__FILE__)."/env.php";
}

require '../app/configs/environment.php';

Zend_Registry::get('application')->run();

