(function(global) {
	// Globals
	if (!global.CBListings) { global.CBListings = {}; };
	var CBListings = global.CBListings;

	// To keep track of which embeds we have already processed
	if (!CBListings.foundEls) CBListings.foundEls = [];
	var foundEls = CBListings.foundEls;

	var els = document.getElementsByTagName('script');
	var re = /.*cityblast\.listing\.js/;

	for (var i = 0; i < els.length; i++) {
		var el = els[i];
		if (el.src.match(re) && foundEls.indexOf(el) < 0) {
			foundEls.push(el);
			var listing_id = getParameterByName(el.src, 'id');
			var member_id = getParameterByName(el.src, 'mid');
			createEmbeddedBlock(el, listing_id, member_id);
		}
	}

	function getParameterByName(url, name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		    results = regex.exec(url);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	function createEmbeddedBlock(el, listing_id, member_id) {
		var i = document.createElement('iframe');
		var l = getLocation(el.src);
		var unique_id = stringGen(10);
		i.src = l.protocol + '//' + l.host + '/listing/embedded_block/id/' + listing_id + '/member_id/' + member_id + '/block_id/' + unique_id;
		i.scrolling = 'no';
		i.id = unique_id;
		i.className = 'cb-widget';
		var styles = 'width:' + el.dataset.width + 'px;border:1px solid #e8e9ec;box-shadow:0 0 3px 0 rgba(216,216,216,.75)';
		if (i.getAttribute('style')) i.style.cssText = styles;
		else i.setAttribute('style', styles);
		insertAfter(i, el);
	}

	function insertAfter(elem, refElem) {
		var parent = refElem.parentNode;
		var next = refElem.nextSibling;
		if (next) {
			return parent.insertBefore(elem, next);
		} else {
			return parent.appendChild(elem);
		}
	}

	function getLocation(href) {
    	var l = document.createElement("a");
    	l.href = href;
    	return l;
	}

	function stringGen(len) {
    	var text = '';
    	var charset = 'abcdefghijklmnopqrstuvwxyz0123456789';
    	for (var i = 0; i < len; i++) {
    		text += charset.charAt(Math.floor(Math.random() * charset.length));
    	}
    	return text;
	}

	window.addEventListener("message", receiveMessage, false);
	function receiveMessage(event) {
		var block_id = '';
		var height = 200;
		var res = event.data.split('|');
		if (res.length == 3 && res[0] == 'cityblast_listing_message') {
			block_id = res[1];
			height = res[2];
			var block = document.getElementById(block_id);
			block.style.height = height + 'px';
		}
	}
})(this);