function validate_entry(){
	var valid = true;
	var errors = "";
	$('#errors').html(errors);
	$('#errors_wrapper').hide();
	$('.success').hide();
	 
    
    if(!$('#company_name').val()){
		$('#company_name').addClass('errors');
		errors += "<div>Please enter valid company name.</div>";
		valid = false;					
	}
	else{
		$('#company_name').removeClass('errors');
	}
	if(!$('#phone').val()){
		$('#phone').addClass('errors');
		errors += "<div>Please enter valid phone number.</div>";
		valid = false;					
	}
	else{
        var validNum =/^[\(]?(\d{0,3})[\)]?[\s]?[\-]?(\d{3})[\s]?[\-]?(\d{4})[\s]?[x]?(\d*)$/;
        if (validNum.test($('#phone').val()) == false)
        {
			$('#phone').addClass('errors');
        	errors += "<div>Please enter valid phone number.</div>";
        	valid = false;					

        }
        else
		    $('#phone').removeClass('errors');
	}
	if(!$('#address1').val()){
		$('#address1').addClass('errors');
		errors += "<div>Please enter valid address1.</div>";
		valid = false;					
	}
	else{
		$('#address1').removeClass('errors');
	}
   	

	if(!$('#city').val()){
		$('#city').addClass('errors');
		errors += "<div>Please enter valid city.</div>";
		valid = false;					
	}
	else{
		$('#city').removeClass('errors');
	}
	if(!$('#state').val())
	{
		$('#state').addClass('errors');
		errors += "<div>Please enter your state.</div>";
		valid = false;					
	}
	else
	{
		$('#state').removeClass('errors');
	}

    if(!$('#referral_notes').val())
	{
		$('#referral_notes').addClass('errors');
		errors += "<div>Please enter 'How did you hear about us?'.</div>";
		valid = false;					
	}
	else
	{
		$('#referral_notes').removeClass('errors');
	}

    if(!$('#country').val())
	{
		$('#country').addClass('errors');
		errors += "<div>Please select valid country.</div>";
		valid = false;					
	}
	else
	{
		$('#country').removeClass('errors');
	}
	if(showSiteName()==""){
           errors +="<div>Please enter valid site name.</div>";
	       valid = false;
		
	}else{
		if(!checkSiteName()){
		   errors +="<div>This site name is already taken</div>";
	       valid = false;
		}
	}

	 if(!$('#card_type').val())
	{
		$('#card_type').addClass('errors');
		errors += "<div>Please select valid card type.</div>";
		valid = false;					
	}
	else
	{
		$('#card_type').removeClass('errors');
	}

  	 if(!$('#card_number').val())
	{
		$('#card_number').addClass('errors');
		errors += "<div>Please enter card number.</div>";
		valid = false;					
	}
	else
	{
		if(!isValidCard($('#card_number').val(), $('#card_type').val())){
			$('#card_number').addClass('errors');
			errors += "<div>Please enter valid card number.</div>";
			valid = false;					

		}else{
			$('#card_number').removeClass('errors');
		}
	}


	if(!$('#card_exp_month').val())
	{
		$('#card_exp_month').addClass('errors');
		errors += "<div>Please enter card exp. month.</div>";
		valid = false;					
	}
	else
	{
		$('#card_exp_month').removeClass('errors');
	}

	if(!$('#card_exp_year').val())
	{
		$('#card_exp_year').addClass('errors');
		errors += "<div>Please enter card exp. year.</div>";
		valid = false;					
	}
	else
	{
		$('#card_exp_year').removeClass('errors');
	}


	 if(!$('#card_cvc').val())
	{
		$('#card_cvc').addClass('errors');
		errors += "<div>Please enter CSC code.</div>";
		valid = false;					
	}
	else
	{
		var cvc = $('#card_cvc').val()
	     var ccCheckRegExp = /[^\d ]/;
		 isValid = !ccCheckRegExp.test(cvc);
		 if(isValid){
			if(cvc.length !=3 && cvc.length !=4 ){
				isValid= false;
			}
		 }
		
		if(!isValid){
				$('#card_cvc').addClass('errors');
		        errors += "<div>Please enter valid CSC code.</div>";
		        valid = false;
		}else{
			$('#card_cvc').removeClass('errors');
		}
	}
	
	if(valid)
	{
		$('#errors_wrapper').hide();
	}
	else
	{
		window.location = "#sub_nav";
		$('#errors_wrapper').fadeIn(2000);
		$('#errors').html(errors);
	}
return valid;

}

var isValidSiteName=false;
function showSiteName(){
    siteName =$("#company_name").val();
    siteName = siteName.toLowerCase()
    siteName= siteName.replace(/ /gi,'-');
    siteName=siteName.replace(/[^\w\-]/gi, '')
    $("#sitename").html(siteName);
    return siteName;
}

function checkSiteName(){
    siteName= showSiteName();
	if(siteName == ""){
  	 $("#sitename_error").removeClass().addClass("error").html("Please enter valid site name.");
	 return ;
	}
    $.ajax({
        url: "/customer/checksitename",
        data: {'sitename':siteName},
        async : false,
        dataType:'json',
        success: function(returnData){
            if(returnData.success){
                $("#sitename_error").removeClass().addClass("valid").html("You can go with this site name.");
                $("#site").val(showSiteName());
                isValidSiteName = true
            }else{
                $("#sitename_error").removeClass().addClass("error").html("This site name is already taken.");
                 isValidSiteName = false;
            }
    }});
return isValidSiteName;
}

/*  ---  isValidCard start---      */

function isValidCard(cardNumber, cardType)
{
  var isValid = false;
  var ccCheckRegExp = /[^\d ]/;
  isValid = !ccCheckRegExp.test(cardNumber);

  if (isValid)
  {
    var cardNumbersOnly = cardNumber.replace(/ /g,"");
    var cardNumberLength = cardNumbersOnly.length;
    var lengthIsValid = false;
    var prefixIsValid = false;
    var prefixRegExp;

    switch(cardType)
    {
      case "master":
        lengthIsValid = (cardNumberLength == 16);
        prefixRegExp = /^5[1-5]/;
        break;

      case "visa":
        lengthIsValid = (cardNumberLength == 16 || cardNumberLength == 13);
        prefixRegExp = /^4/;
        break;

      case "amex":
        lengthIsValid = (cardNumberLength == 15);
        prefixRegExp = /^3(4|7)/;
        break;

      default:
        prefixRegExp = /^$/;
      
    }

    prefixIsValid = prefixRegExp.test(cardNumbersOnly);
    isValid = prefixIsValid && lengthIsValid;
  }
  return isValid;
}


/*  --- isValidCard end --- */