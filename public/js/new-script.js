$(function(){
	function setNavigationDownArrowPosition()
	{
		$('.navigation .nav-pills .downArrow').each(function(){
			var $this = $(this);
			$this.css('left', ($this.parent('li').width() - 14)/2);
		});
	}

	$('.mobileNavigation .toggleMenu').click(function(e){
		$('.mobileNavigation ul').slideToggle('slow');
	});

	// Find all YouTube videos
	var $allVideos = $(".videoPlayer"),

		// The element that is fluid width
		$fluidEl = $(".videoContainer");

	// Figure out and save aspect ratio for each video
	$allVideos.each(function() {

		$(this)
			.data('aspectRatio', this.height / this.width)
			
			// and remove the hard coded width/height
			.removeAttr('height')
			.removeAttr('width');

	});

	// When the window is resized
	// (You'll probably want to debounce this)
	$(window).resize(function() {

		var newWidth = $fluidEl.width();

		// Resize all videos according to their own aspect ratio
		$allVideos.each(function() {

			var $el = $(this);
			$el
				.width(newWidth)
				.height(newWidth * $el.data('aspectRatio'));

		});

		setNavigationDownArrowPosition();

	// Kick off one resize to fix all videos on page load
	}).resize();

	//FAQ and HELP page toggle questions

	$('.questionWrapper .question').click(function(){
		var $Parent = $(this).parent();
		var $Answer = $Parent.find(".answer");

		if($Parent.hasClass("expand")){
			$Answer.stop(true).slideUp();
		}
		else{
			$Answer.slideDown();
		}
		$Parent.toggleClass('expand');
	});

	//facebook embedded posts
	if ( $(window).width() <= 600 ) {
		$('.fbEmbedPost').css('width', '350');
		$('.fb-post').attr('data-width', '350');
	}

	//logos carousel
	if(jQuery().bxSlider) {
		$('.logo_span').bxSlider({
			slideWidth: 205,
			minSlides: 5,
			maxSlides: 5,
			slideMargin: 10,
			auto: true,
			autoHover: true,
			pager: false,
			controls: false
		});
	}
});