$(document).ready(function() {
	
	// Expand Panel
	$("#open").click(function()
	{
		$("#open_login").hide();
		$("#followers").hide();
		$("#close_login").show();
		
		$("div#panel").slideDown("slow");
	
	});	
	
	// Collapse Panel
	$("#close").click(function()
	{
		$("#open_login").show();
		$("#followers").show();
		$("#close_login").hide();
				
		$("div#panel").slideUp("slow");	
	});		
	
	// Switch buttons from "Log In | Register" to "Close Panel" on click
	$("#toggle a").click(function () {
		$("#toggle a").toggle();
	});		
		
});