$(document).ready(function(){
	
	//$('form').each(function(i, el) {

$(window).resize(function() 
{
 	$('.row-fluid.listing > div').css('height', '');
 	if($(window).innerWidth() > 767){
 		$('.row-fluid.listing > div').sameHeights();
 	};
});

		
	// ADD EVENTS TO ALL FIELDS
	$('.uiInput, .uiTextarea').each(function(i, el) {
		
		$(el).blur(function(){
			$(el).removeClass('active');
		});
		
		$(el).focus(function(){
			$(el).addClass('active');
			$(el).removeClass('error');
		});
		
	});
	
	$('.uiSelect').each(function(i, el) {
		
		$(el).blur(function(){
			$(el).removeClass('active');
			$(el).closest('.uiSelectWrapper').removeClass('active');
		});
		
		$(el).focus(function(){
			$(el).addClass('active');
			$(el).closest('.uiSelectWrapper').addClass('active');
			$(el).removeClass('error');
		});
		
	});
	
	/**
	 * same code which is used for uiSelect, uiInput and uiTextarea above, but here it is for new layouts input, select and textarea.
	 */
	$('input, textarea').each(function(i, el){
		$(el).blur(function(){
			$(el).removeClass('active');
		});
		
		$(el).focus(function(){
			$(el).addClass('active');
			$(el).removeClass('error');
		});
	})
	
	$('select').each(function(i, el){
		$(el).blur(function(){
			$(el).removeClass('active');
			$(el).closest('.selectWrapper').removeClass('active');
		});
		
		$(el).focus(function(){
			$(el).addClass('active');
			$(el).closest('.selectWrapper').addClass('active');
			$(el).removeClass('error');
		});
	})

	// Zebra the tables
	$('.zebrad').each(function(){ $(this).zebraTable(); });

	$("#explain_video").colorbox({
		iframe: true, 
		innerWidth: 960, 
		innerHeight: 540, 
		maxWidth: '80%', 
		maxHeight: '80%'
	});
	
	if ($('#flash_colorbox').length && $('#flash_colorbox').html().trim().length) {
		$.colorbox({width:"720px", height: "200px", inline:true, href: '#flash_colorbox'});
	}
	
	bindPaginationAjax();

});

function bindPaginationAjax() {
	$('.pagination-ajax a').click( function (e) {
		$('.dashboardContent').html('<img src="/images/dashboard_loading.gif" height="250" width="250" style="margin: 80px 230px 0;" />');
		$.get(this.href, function (data) {
			$('.dashboardContent').html(data);
			bindPaginationAjax();
		});
		e.preventDefault();
	});
}

function scrollToId(id){
	$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');
}

/* Plugin to make variable height divs equal heights */
$.fn.sameHeights = function() {
	
	var tallest = 0;
	
	$(this).each(function(){
		if (tallest < $(this).height()) { tallest = $(this).height(); }
	});
	
	$(this).each(function(){
		$(this).css({'height': tallest});
	});
	
	return this;
	
}

/*---------------------------------------------------------------*/
/* MAKE ZEBRA TABLES									*/
/*---------------------------------------------------------------*/
$.fn.zebraTable = function() {
	
	var rows = $(this).find('tr');
	
	$(rows).each(function(i){
		if($(this).hasClass('tr-odd'))
			$(this).removeClass('tr-odd')
		if(i>0 && i%2==1)
			$(this).addClass('tr-odd');
	});
	
	return this;
	
}
