<?php
/**
 * SAML 2.0 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://rnd.feide.no/content/idp-remote-metadata-reference
 */


// Below metadata was generated using www/admin/metadata-converter.php, submitting XML available at the IdP URL

$metadata['https://idp.clareitystore.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.clareitystore.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.clareitystore.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.clareitystore.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.clareitystore.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.clareitystore.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.clareitystore.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => '
MIIDQDCCAiigAwIBAgIVAPwiCw3nBiXgNssoNkZh3XXLo4BOMA0GCSqGSIb3DQEB BQUAMCAxHjAcBgNVBAMTFWlkcC5jbGFyZWl0eXN0b3JlLm5ldDAeFw0xMzA0MTUx OTEwMDdaFw0zMzA0MTUxOTEwMDdaMCAxHjAcBgNVBAMTFWlkcC5jbGFyZWl0eXN0 b3JlLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKUqPoEwn1tT +wCfTlvUcfZwkYws4lPxfUoUGpgpCXQbkLQhzDmm/tAA+dChIoaCOxmYPpMYhfrr Dlrjzl7WjwUl03/yB6ZZqc4qmriv3VGMzRg6dsy5WAzLcEdixlknL28CqZxjuMj9 6nXhllM67loYuuQamCrSXtEbRETx4hWHZAOJjRzhkLv7D8y4Ftt0j0cQOhCuYM16 rnum68KRvOG7f4PAu6QmROndvwUf7qMG4QBvhcWutd3afjfbsfI5Jg7nJVNKCRjh xhYRgLbyq2AmTjAua/v1sbuFsDI92foxTq1wV98yC0kAwgDclgvpFnIrgM1zaVcX Wh9CgnKerwMCAwEAAaNxMG8wTgYDVR0RBEcwRYIVaWRwLmNsYXJlaXR5c3RvcmUu bmV0hixodHRwczovL2lkcC5jbGFyZWl0eXN0b3JlLm5ldC9pZHAvc2hpYmJvbGV0 aDAdBgNVHQ4EFgQUjW1xli4GNB53mZmypXw7LhnRH2IwDQYJKoZIhvcNAQEFBQAD ggEBAGPZrf8prSyR9A4NmENKQ9dCtTxWDz8r5pYjB6ZNv616X2T4jwSrVjToVi3v GA4uXrus/ddRirQHPKQCUCVfW+aNn2Ruix3TpY6mIkLyM3ExlX+QHargTzhKgRDt zUzr+e+1FAcUCF9AWchhB1YmPD16vYzXdhazffHr6XR5vdo5rURx1I7Rm/+Spq5A yiK19MlQrKAwyN/iHaPhjjrVKtvxnoNwRJBEMKcpWO+J4Lc5NWuPmTkB2HD4sCPJ C9IBfjz4pgb3YWhA+9kf4Jnsf70dE/Ijxfk20BhFPCwut7zaNlvKU2USdazY9kiN rwN0dIdbNBJwJEd8Qq43wA9wHyk=
',
    ),
  ),
  'scope' => 
  array (
    0 => 'clareitystore.net',
  ),
);

$metadata['https://idp.tulsa.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.tulsa.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.tulsa.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.tulsa.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.tulsa.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.tulsa.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.tulsa.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
    5 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'http://idp.tulsa.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    6 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'http://idp.tulsa.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    7 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'http://idp.tulsa.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    8 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://idp.tulsa.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    9 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'http://idp.tulsa.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDPzCCAiegAwIBAgIUI3qCmlHim+FKoEwbxvoV2aexh/AwDQYJKoZIhvcNAQEF
BQAwIDEeMBwGA1UEAxMVaWRwLnR1bHNhLnNhZmVtbHMubmV0MB4XDTEwMDkyOTEz
MTc0MFoXDTMwMDkyOTEzMTc0MFowIDEeMBwGA1UEAxMVaWRwLnR1bHNhLnNhZmVt
bHMubmV0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0xaZtp7zZTe0
/jnh3zym+ELELuHwKus92XWPijjaCCHITjrt7HS1X5WmPKls7wP6ACAX+Pb6CC9U
mLuFmcwKRinA5Q/wkiWZFqouMQyVaE3DPAzQQ+kz3br5xwePwKdpkIlj+29wlrzo
kyGYdfOFjIKdETxl+wGluK+1QJsj5/QI03VoyNjOIXvEIxECfoxOJAjJ4Zb57Ox3
5TseF4ZeYDsMeLNtihN0rXU7UUmupOU/Gtig3Saw0UwmLmaJTVbHkd17ULMjygzG
cq30DxNe5WwpHozknazurws0vjr5iLB4x16HztXBr52UgN97ZAAaM1WaYsfHTvdY
MjEmn1hadQIDAQABo3EwbzBOBgNVHREERzBFghVpZHAudHVsc2Euc2FmZW1scy5u
ZXSGLGh0dHBzOi8vaWRwLnR1bHNhLnNhZmVtbHMubmV0L2lkcC9zaGliYm9sZXRo
MB0GA1UdDgQWBBRr92YZoeKNRiiXohy2CbJ/lOkklTANBgkqhkiG9w0BAQUFAAOC
AQEAfj62fvOjhOLnjZFqNwVowzSgzZNLAB1Vff/fm9kJx+YPHu6zRTHWD9G+1jO5
5l5xblpFI5pb7vWvqGDKc3hs8njw0dtvbK9AgPyTLKXs7Dh78VNin6miRY+NA5Yh
vvxGf9FuF0FPF/38HFPEofYhAz6Dblcy2PnLpvPrgfIeIGTwm1i1JX7VvcwvmoV5
2X0/+faUjjqxRNxBcy8/Xp6yImshk09UrshKRxLU99mo/ue00wDi0HRy5wmrzeHK
QCZy9jqeEosse7YMmZQyjcvOt+J+jkseQVZ2CK3pMWMK/gr/5Y+7fNc8rfuaD4cU
gnuFWYXX4JuQWdy7JkAaAYMHQQ==',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.maxebrd.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.maxebrd.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.maxebrd.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.maxebrd.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.maxebrd.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.maxebrd.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.maxebrd.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.maxebrd.safemls.net/idp/profile/SAML2/Redirect/SLO',
      'ResponseLocation' => 'https://idp.maxebrd.safemls.net/idp/profile/SAML2/Redirect/SLO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.maxebrd.safemls.net/idp/profile/SAML2/POST/SLO',
      'ResponseLocation' => 'https://idp.maxebrd.safemls.net/idp/profile/SAML2/POST/SLO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.maxebrd.safemls.net/idp/profile/SAML2/SOAP/SLO',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDSDCCAjCgAwIBAgIVALrvD9tOwg8NIg+BlRAv6OJoK/NnMA0GCSqGSIb3DQEB
BQUAMCIxIDAeBgNVBAMTF2lkcC5tYXhlYnJkLnNhZmVtbHMubmV0MB4XDTA5MDkw
MzE3MDUzMVoXDTI5MDkwMzE3MDUzMVowIjEgMB4GA1UEAxMXaWRwLm1heGVicmQu
c2FmZW1scy5uZXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCHPssA
OcS1M1VAIhV5v9MZ2KuTsqWwJwpqb+I58h0vzUY9NC22ssDMwbjFNKdtprNmhgr8
D9aqkXBPWL/l/lhvYu7dgGvw0HCAVjSfuqJhTZfAsUieY+9QozksuAcSSiln9xWo
GEr0mnDb4GMnmvTMSl9BZ6bZd8z8dzuv8fga//hmsIHS5HDuwSYsYqVaElguw/xc
ks8wYum2A8wxtEh5/qxF7sHuCGkHC3xLKV1q4SMCjmL8TctzE2h5wIUMDjnWhUka
FG/bfu6mXJDK1hufho6wP47o1dLx3gAE4aPUtWMBRs8C7jEQyeoLj6nBtv8MyV3/
hASKA0tvpJM0lBN5AgMBAAGjdTBzMFIGA1UdEQRLMEmCF2lkcC5tYXhlYnJkLnNh
ZmVtbHMubmV0hi5odHRwczovL2lkcC5tYXhlYnJkLnNhZmVtbHMubmV0L2lkcC9z
aGliYm9sZXRoMB0GA1UdDgQWBBSiveLwDffZ75dRE08fNMQ0lN+irTANBgkqhkiG
9w0BAQUFAAOCAQEAEqof75xQEs9on32uAo6j51Dx6vjX93NXEkefpmuK00mQhvW4
BnLIBBwwBOGKdn/gWPKjDA9J9nJ3OhccZQcZTpjQDmj9s3k/6EnxbOcAIKydRcH6
yP21JMGVpN5AZXBzY1jsr4zap2siEgnWODkw5Aq9OcfAjhNF2ZXsxLRFzxsH+dv1
Cl0KXuU4cLWY5p5lICUs0AHSCt1JxgCDz2ZhbSS7e1QHAfmh1PYdf07mmlQHPmzh
ycDQx70OsrqLJH0iOp6+fpn5kVzj3mRGvTqcFTV/CnW3+RLz/i3HHj31LHJGcP0o
0lyPRCtE6zs+jEsDIbO9ptzX3UJlRb7DzRhcuw==',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.glvar.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.glvar.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.glvar.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.glvar.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.glvar.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.glvar.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.glvar.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.glvar.safemls.net/idp/profile/SAML2/Redirect/SLO',
      'ResponseLocation' => 'https://idp.glvar.safemls.net/idp/profile/SAML2/Redirect/SLO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.glvar.safemls.net/idp/profile/SAML2/POST/SLO',
      'ResponseLocation' => 'https://idp.glvar.safemls.net/idp/profile/SAML2/POST/SLO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.glvar.safemls.net/idp/profile/SAML2/SOAP/SLO',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDPzCCAiegAwIBAgIUVTobeoHw4nNZ8dBHhxWlHoaOYHYwDQYJKoZIhvcNAQEF
BQAwIDEeMBwGA1UEAxMVaWRwLmdsdmFyLnNhZmVtbHMubmV0MB4XDTEzMDIwNzE3
MzM1OVoXDTMzMDIwNzE3MzM1OVowIDEeMBwGA1UEAxMVaWRwLmdsdmFyLnNhZmVt
bHMubmV0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArZHMtLrmyHCB
PPSjWxHA9lm6KSSQ/5QDIp3qOZaImiS3m2e2Wlh83vwM6mpS3y9Ii8PKh4jJAZ4R
w9LqpzgPbOLaXRu3zik36u0HPjL/52v9DnoASDI2YicmE7wBM3OYGwJ9QffHd8TP
YfqYTb0hf67+8fXzi5QGYVvM99KfCaKedJ9d/peSX3wrlpeQriwdQm98fWH0Opgs
IKYi9pUhcWoj9jHZehNJK/1eBO4RHkjf839DG6JwllFF8deQirBUhn+Kjoto7nLZ
BBGAHWvhpgzO8pn8u143Sx/mNNw7k16KOQaKEutKmdDGBQ3RooeDrDzVBoNk6eHQ
FADmibMD0wIDAQABo3EwbzBOBgNVHREERzBFghVpZHAuZ2x2YXIuc2FmZW1scy5u
ZXSGLGh0dHBzOi8vaWRwLmdsdmFyLnNhZmVtbHMubmV0L2lkcC9zaGliYm9sZXRo
MB0GA1UdDgQWBBQ29swgUFadMk1rka6LVEKO1MczfTANBgkqhkiG9w0BAQUFAAOC
AQEASiQBTl5X89lABNuNdF+bTX2Gy/BEF6Bkzxcz5viK1EpDAJDjS5Y54mrJnTZi
fOLCS/zKAniPAeXjv3S0BITPJDIdblORwJYg5djrcJHbFh0iDeP0qwRYRshpXLWu
I/mSZYqEu5YuH5kzTRvSlnAR+aP/v3J/YRO8+pBLqsLdjZPCnbH6uXhXvxQVHQL+
jPBS79u27pOKp1PFUn6XNsYIBFWRNHsJUiz/abWC45SjJh6YPAxsCONu4NGYg4cL
mDmQ5b/yMMavMSt06i5zJHKbwAcqKvNEbNrGj3LBdapyJsFpbFMRSqAHvGkYMwG1
n7YCNLC3ECMJzhCPNuzcMNDbfw==',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);

$metadata['https://idp.bamls.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.bamls.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.bamls.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.bamls.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.bamls.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.bamls.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.bamls.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDQDCCAiigAwIBAgIVALfaA7Ip09ibusnpHUi0t/Ck9QzTMA0GCSqGSIb3DQEB
BQUAMCAxHjAcBgNVBAMTFWlkcC5iYW1scy5zYWZlbWxzLm5ldDAeFw0xMjEyMTky
MTM4NTBaFw0zMjEyMTkyMTM4NTBaMCAxHjAcBgNVBAMTFWlkcC5iYW1scy5zYWZl
bWxzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKizuap/p3I5
RqtRsxO0Hlw+gooHudoc7q2mQaPJTQuoi3vdgTt5ZSQpO7FMVC78+b17PKkXfbFS
SzZlmJBJL6nAleyZByyC4/C4pLFnBpHg0Q+xcp6XZsMUkc+i30Y/j2TM2Thh7yVM
nfzMB3QUnVklBxRPx83OlTZuut+i+5hjjGeqViQjeM+4mUYoD02/cidFFzAbNUzz
pA7ZQVZLE62ZfVAjj6rgKVITPRxIR+r23V/rYTOOKxCpJEf81W2V0Yu2y+tYsxl9
/QiBk9QmwDK4t3AKUwSSXAMZGzgKtBo29flDJ5rDVKVcnwVRrmtUcJC1/rhZ68Qt
0qe2TLMMGjMCAwEAAaNxMG8wTgYDVR0RBEcwRYIVaWRwLmJhbWxzLnNhZmVtbHMu
bmV0hixodHRwczovL2lkcC5iYW1scy5zYWZlbWxzLm5ldC9pZHAvc2hpYmJvbGV0
aDAdBgNVHQ4EFgQUN2jKQ3EqoBCz4IdXy2lH2i814SEwDQYJKoZIhvcNAQEFBQAD
ggEBAJ6dPNOpYvXEj3+/Jpu6esfl8AMzrzPO0ML9xqF+d7xMwA0ec3CmNaq0gpzM
OKfbWAx0jkpZqW2iyqH0dsXW9evDrVpBqKEj6EHnhdvQ1wkbGHtNKPIX2uCf59Gi
70xerC/mZELCXn4+vRHA3nh5SdzFUEPuwrjmY1LWwTQ3fxLjMNFoOxdOT8Nu2K8G
4JrX9cdI6cvcKQgIFoTBt+D1CU5JrvivRGwsDZL/muoVo5HBrBjA7Hzs6Hf5WdKh
ZhSuR6TWlJ/HBmWdTyp1a5yixteENMweiq9oNzCwvytT3regHIjPnR6Whqs/+nuc
6niyt6RGyxM0rO1FjpjXyEZpumI=',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.ncmmls.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.ncmmls.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.ncmmls.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.ncmmls.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.ncmmls.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.ncmmls.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.ncmmls.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => ' MIIDQzCCAiugAwIBAgIUQAhBf6wlREkH8Gy5KhmhfIYv1jcwDQYJKoZIhvcNAQEF
BQAwITEfMB0GA1UEAxMWaWRwLm5uY21scy5zYWZlbWxzLm5ldDAeFw0xMjAyMDEw
NDM1MzhaFw0zMjAyMDEwNDM1MzhaMCExHzAdBgNVBAMTFmlkcC5ubmNtbHMuc2Fm
ZW1scy5uZXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDBV6X6crrw
DzPrXB/e4pCQt1NmYk9Sc67CEIt6gS3Bgky1cQuzZmr+HskfPeBj/cbpGfg7lmwb
PQK1kypwYPMkDqZyUgDOJ13/qOEUJz3JQ6uqkJE6ekTge0i8gmt6a/piGmHU8P2p
yPE+WZ70hIOj93sJ0pgBRq4sk6QEPrJA0eH34hP6CcNOkZmfNb05KEm/ttchMEpV
p+NkCd1UHluZYH/nI0SVLvsBSsM5/M0uKsDdKoJ+Q+nu1i9OjV0Cd//EFoCnHQZ/
le+P4JHPa0UIMd0fy24s769OhT40hYHK/9YC+AOag0Fz2i53NlNdS8ncTHxwy79H
T26cgkkRAo0LAgMBAAGjczBxMFAGA1UdEQRJMEeCFmlkcC5ubmNtbHMuc2FmZW1s
cy5uZXSGLWh0dHBzOi8vaWRwLm5uY21scy5zYWZlbWxzLm5ldC9pZHAvc2hpYmJv
bGV0aDAdBgNVHQ4EFgQU2GvSa75nUcnSJiwsVhR5gEe8fAcwDQYJKoZIhvcNAQEF
BQADggEBACgF763ILtACCPCMQg2qRbHTUS2LEW7mBTglXmmzuhEF6WjquELD6eaL
xd5jmkGwjJYfDP4TIoDbBYWPap5GK2N7pB8om9SB7TIwvwyVXzEXGbMAjKbny7q/
YkwjJdEjc7YbeLu0TzqHplBVfhm3dAwvQs/10908wCW8qq7p4lsxm01A+9n6m3Km
Rh/wamIOtGP3iHnCImW08oaHqqiMK9lweprsR3mHi8gAMtAR0xfpBIhTbYg8BYM0
qa94iOBM1xbidP2bPBGJ7CcpZkXEojFgEaI0sN5oRi2nwEyagZrzd2oU7tf1h0BH
rmU718k3Oc9BEjx077OasdVMcVBt3jE=
 ',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.stwmls.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.stwmls.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.stwmls.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.stwmls.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.stwmls.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.stwmls.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.stwmls.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDRDCCAiygAwIBAgIVAMDzIKP+HW5n6empliaRmI2cA8oiMA0GCSqGSIb3DQEB
BQUAMCExHzAdBgNVBAMTFmlkcC5zdHdtbHMuc2FmZW1scy5uZXQwHhcNMTIxMjI4
MDYwMjA3WhcNMzIxMjI4MDYwMjA3WjAhMR8wHQYDVQQDExZpZHAuc3R3bWxzLnNh
ZmVtbHMubmV0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAniJFxRYh
cWrvplH1+cjMsuMzu3T3++JJj9gDN8U3sfphuTLccpx6wpy/uWuLwpRB6hKpa5hJ
Vt9myzYAjcpGUK313ug8sjNPBSRS7tw9iIPxIOEK1EbyX/HbVf+uBHw4KbxTf2MX
dTDBtbnaSg9AdFJUgoPBeME6fxjKvQeuEsvdiDpiNNFTKtD4z/1yNmEC9k0PWw0r
IrLU692y0cTIYnA/4hqMAmahFCySk4Av20T+6FZxY0+rP9kqrnRNn5yybXHwhHZZ
cTZswnyVLWbMNn/q5EtM1IHYOoxZjcQxQrjw9eV/yukBkDj6RDU1gVTO7cn/14zO
tSqVbKa6OWkzNQIDAQABo3MwcTBQBgNVHREESTBHghZpZHAuc3R3bWxzLnNhZmVt
bHMubmV0hi1odHRwczovL2lkcC5zdHdtbHMuc2FmZW1scy5uZXQvaWRwL3NoaWJi
b2xldGgwHQYDVR0OBBYEFE2EIV7T2gxnZA1h/Iw3pH5BFwrnMA0GCSqGSIb3DQEB
BQUAA4IBAQCGPxj8x+hDq44wyx3YE9lFoGxhuNXejL+6j4GwVCEcGsFSXVMMn9hY
ITW7/UR83svh66dVexgyvSDlI/Z1+i6IXbvG9rJFHD0Qt0JuPWZwM5+r7pOOgCZK
jTbU7MIRFxUHe4Yye1QvIQ7uTp367+tO8LCUPnQn/5jvWMXCYX4SEDc/yBY3mp8E
1tP2Sf9VRkAG1BgJp6eZc9L9Zq/h+b6IzLCpJ8FMRRqMtuUEqQgylWGvtM/GytsK
2TtyT1jmdrC3vwTQAB5ubP66XVjpx99PCnA8XdxLRD+7LSDtsn5iucRpcxqvzrkt
pGKOICJtoRrUh8yX1ZKtsbokM+L44+cX',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.ctmls.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.ctmls.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.ctmls.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.ctmls.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.ctmls.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.ctmls.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.ctmls.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDPzCCAiegAwIBAgIUb5Icv2MPPWnIefpT53h0U/+HrMwwDQYJKoZIhvcNAQEF
BQAwIDEeMBwGA1UEAxMVaWRwLmN0bWxzLnNhZmVtbHMubmV0MB4XDTEzMDQxOTE2
MTAyNloXDTMzMDQxOTE2MTAyNlowIDEeMBwGA1UEAxMVaWRwLmN0bWxzLnNhZmVt
bHMubmV0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2T0LjSLxw34z
XjoL7sIza3FzBPDMiqb1q1N7dLIC8bPwzW100fprfxG97dquuH+r2VHG9FwP/IRJ
jHnZm/C1V/0/bEP+ZHi2fPiSU5FPxvDjzk9yMAumEJOtsrD0Gxd9L90SsZ7wCQuw
N1y46GxcJot/HlYljFWDXXWiiQD8Lg0q83hKNmD+s+rRIQ0yIrfMO2rnOndvUcjJ
UgGMqm2sKbzFRkJSB3NYIh4EU3UaxPe/wL6voNgLJdwxFbk8hrhjldQxviibcc4w
mno/NHO4s0rs5+5POQ1F+KnIEYUp0Hr6S6urELHTgdOdXYj6fbct4CYhpsWhRfdU
1JrXXLWv6wIDAQABo3EwbzBOBgNVHREERzBFghVpZHAuY3RtbHMuc2FmZW1scy5u
ZXSGLGh0dHBzOi8vaWRwLmN0bWxzLnNhZmVtbHMubmV0L2lkcC9zaGliYm9sZXRo
MB0GA1UdDgQWBBRRyibbt4mjGkdrOMRCcaX4tu8EezANBgkqhkiG9w0BAQUFAAOC
AQEAVOJCLzGDXx/O4mpgpKqXq035fKm1p9Gv+VsFTG7x9P9TdtbpK84P5xVj3ymy
p7DaJSkGyQtdYYUgeyT9rVB699KmDVoxDTQ3iBGmpnraoWi1WGcw64owHNV2Zw4m
Y0uaoT9QpvEIzpIr/JB09380O5y4at3l4UtOG4BWmxR73oxm2BV4FRs6bs6sLdRj
FCgaByfCoE7zgDwoHaw7KaoyHAzO86F/qnCOE3OfxPGPy8vGBYCzOZqJXOviINPf
OfvvGAzqS+Uiet59kfIezmBSU2qbMdWwswFuXe3pshrok4vsSrMFYO5sP0fUqbL5
+aDUD8NmhTY3eTHPBfFdI8ETgA==',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.spokane.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.spokane.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.spokane.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.spokane.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.spokane.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.spokane.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.spokane.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDRzCCAi+gAwIBAgIUQ+vqsJVJ5GTAL5GzFPQndi8L84owDQYJKoZIhvcNAQEF
BQAwIjEgMB4GA1UEAxMXaWRwLnNwb2thbmUuc2FmZW1scy5uZXQwHhcNMTAwNTI1
MTYyNjMyWhcNMzAwNTI1MTYyNjMyWjAiMSAwHgYDVQQDExdpZHAuc3Bva2FuZS5z
YWZlbWxzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJZjs6wZ
HT3Yrn9jBOpfySO8mouHtkhK00o9J0+PEAlPBEaPIDIE+S/7XgsAL9zLL29Z8UVy
oBCJSpXEG3zmbmwn38Z0lZ0H727/mDoKtPBL4uXRn2Xkvjx49TEs0og5lB5TAAS2
NIdZXI3y0EUasblfL5Rj78la29ignmD4Pro9JF/1Q5bqRMBUaOgWsrhMVfTpcjh2
iGBumQOe+vIKONvS5NfCWHqm9HMld1SVMS4dD1zHvEpodaAkXxlqRCPLVHYj+EXF
WIMfDBIlnJvioAf2r+GG8bcavgkWEmQL58bAkA2K7czNOXv3kaAl20Lnyx1hU6Mg
SyhhWmimV5P0jl0CAwEAAaN1MHMwUgYDVR0RBEswSYIXaWRwLnNwb2thbmUuc2Fm
ZW1scy5uZXSGLmh0dHBzOi8vaWRwLnNwb2thbmUuc2FmZW1scy5uZXQvaWRwL3No
aWJib2xldGgwHQYDVR0OBBYEFOHBUmrZvI9tZOR5iXJPa6LzYTVtMA0GCSqGSIb3
DQEBBQUAA4IBAQAwBU+0FcN2IZ6AB40wjqY89ak3SpRsCvThnAIHcBn6Kp8SAOhF
GD0CyYvSqHaU9QqNBGGIzXz8Wnay3RpnpdSfE9T9saeY6nPnIiaqGCCuBpvN+J5Y
irOXfCq/IJr4+4UQOnoyMm/KVGmXngENyoTdueMYVujEOOrWjpvTDtctvbaHIKNX
wo/3DPayhCd27IbBBgucmtHxY4KFHKqIv6vnLKRwtkeW8BCuu33SQBpNSAC2IY+O
m/vpY2iGroWCCx2e4v3DWnYGmkdKN4PgZO3cw07E1wk7niOqLbLntIt8SB2hIzvc
9o46zE9gSy6KTN0S56w3ylLGp1Ol9nEHk4oA',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.imls.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.imls.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.imls.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.imls.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.imls.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.imls.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDOzCCAiOgAwIBAgIUWv+VpEmLUA/oxXuie/Ph6sOz2TIwDQYJKoZIhvcNAQEF
BQAwHzEdMBsGA1UEAxMUaWRwLmltbHMuc2FmZW1scy5uZXQwHhcNMTExMDExMDQw
OTE0WhcNMzExMDExMDQwOTE0WjAfMR0wGwYDVQQDExRpZHAuaW1scy5zYWZlbWxz
Lm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL0H6vZS/7amjdsO
mbVAo/7Qb6S5MFp1rwDXnOanvJVV9Nrs7L16xSYYaWxNatmrCcR/jG0GSKAPpG81
SeGkilJuadzdr/J1yogUNPvYrPacdQHECaKf+NUw2WwG9bLQ6RnBPPO/HuBc6/rX
Nk6b/aaRBkd5F4rKRiQsu4HlhJsY7+XkNznknSM0lXouDRe3Hwe4hFldk6Ys7LUM
jgbfQKzZWZAShdH/Nv6amZ4WSG1kNa/kGuazGS7NQr4eVbFhj6xWNx566wqBH/P+
of8R1UDhch8rzf+4F1QsN3P5MwO7Jj8NHPZeND0u6Cwz12sdTEpqZmVG7Xz7XP3b
jUjJ5l0CAwEAAaNvMG0wTAYDVR0RBEUwQ4IUaWRwLmltbHMuc2FmZW1scy5uZXSG
K2h0dHBzOi8vaWRwLmltbHMuc2FmZW1scy5uZXQvaWRwL3NoaWJib2xldGgwHQYD
VR0OBBYEFCkOjEDwDikcxuDMea1E4WftifsFMA0GCSqGSIb3DQEBBQUAA4IBAQAC
WNA2bAv1wyP04e1P4xvfpD8oL143aHWIBtskznG3vV2ls6X+FvCpHIYhN1mVctZq
5Pakf/aVpcbPvgF2ZkIml1VzfvIOe7ruAVUXWgSkFPZQ4tjxltE4F9JvPODnD9nx
yoZZLXecJ30FP1MsM0+1Z3p9YqhMkRDC1r/8MkXfpCNpzi8uzRisdr5KpJdBseO8
/oqjYZVvfrw9T3aP4sKowNa7cMkWLTXPSHmqPIte8Ex5PRN6pRbGPpqg1kQP5mWM
rLUD9sTSWPeyuSsf7o29uQBjgC1m7CRLLR2q40/GiiC2ryQzdRDFdrXPbLSaoQh5
+yq0UXZwhof9kr3RxvBF',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.hhi.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.hhi.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.hhi.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.hhi.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.hhi.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.hhi.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.hhi.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDNzCCAh+gAwIBAgIUaa7z0N8AoFkRHWytocyDqRf/WRIwDQYJKoZIhvcNAQEF
BQAwHjEcMBoGA1UEAxMTaWRwLmhoaS5zYWZlbWxzLm5ldDAeFw0xMzEwMTAxOTA3
MzFaFw0zMzEwMTAxOTA3MzFaMB4xHDAaBgNVBAMTE2lkcC5oaGkuc2FmZW1scy5u
ZXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQD1JMMPdbDjWGd7zEkP
HCcmHC/TYC38LW8+QfBJazPV8jzgSupUrOwsBpOOw+McioE19BNbRq/7p/UZIfK0
aMctAgQVrxeKDsAV742xEU7VKs4b0/ahpzDsqCiYQsWzPg2eMlK+5OVHSasYy6+K
vLmAcOdldLgWnDWOyRktSNW8OQk39x4u3EttJ9defBV/xqPhtKCCEaGSUzmJYKgK
9GXVkA75rd+bl/TBfzz4J7D0HjH7FWs379KVD9u3Ar/0WIKnivyhvlXRcj+uhSdV
p7oU0xkcT+DNaGhjuCJ3UWWGLY7z2dcWc5h4kPpCm+yXxNuAoIFst2LxjR2PcQyU
dsRPAgMBAAGjbTBrMEoGA1UdEQRDMEGCE2lkcC5oaGkuc2FmZW1scy5uZXSGKmh0
dHBzOi8vaWRwLmhoaS5zYWZlbWxzLm5ldC9pZHAvc2hpYmJvbGV0aDAdBgNVHQ4E
FgQUqvaaLU2N5EqSoJTT8V/Kh21RiFEwDQYJKoZIhvcNAQEFBQADggEBAFxgr7LS
gv9jJSXOls5l7sJdy1B3i3Y6MTR4Zc9fHrNTFARJvAY9brqmcxSh4RJaKAxtUaKp
ryQkPZX/OpjTPckbVzyNT1DDBxWzzNjrOIz5X56iZVWcBNWBeu6ouD1uSTKiD7Om
VMPC7itQkBxvyiCgH1xHDOsPLJuAwfQqGx8eYwLuuZikRwXzcpR9LxIQK7nG5f+t
snmj0mK+KASKLuV9qVAUQnHk/P5MgdDWiHq//WhU20csEXTZfbwNgtRoOLnjuTKA
n2FZFQh7ukQ4yVn7XLLhDcmX+xCYj/lPNmYs6Pcni8/RfTehEiHEc2ecJniuUmfV
0uFPtkr1ezsdGyg=',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.tucson.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.tucson.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.tucson.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.tucson.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.tucson.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.tucson.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.tucson.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDQzCCAiugAwIBAgIUQuM5WXwr/lPJsRvg0rP9Q9EQZe8wDQYJKoZIhvcNAQEF
BQAwITEfMB0GA1UEAxMWaWRwLnR1c2Nvbi5zYWZlbWxzLm5ldDAeFw0xMTAyMDIx
NjQ4MDZaFw0zMTAyMDIxNjQ4MDZaMCExHzAdBgNVBAMTFmlkcC50dXNjb24uc2Fm
ZW1scy5uZXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC45fGgchaa
P/eMYyt1DYW4CVmFWfjTo2pal5IV+taBx8Ll+1HPDm/ePIg4XdvKtghBIIMvf38P
cmSDgxK4XxAPgnuKb2BlyO5Gr/eEBWBnDeD2UPqY3gMmS8x5quR/C7G3SmJMIEGz
cNcCuJhr6wcq5laBFLvLY3wkWN/QrUvO5snqwxpmVnWAZ+Z4M+7HVH4JAoZ1m2+w
5sacAJ/gEkzWDLSEWqYnbyVQWn1La+k8v7uo8KfId4In8wLk+h64mppitqzf4NZo
jAU4uKlO4KjZUll2gV7mr4zdEbs1yYSeOojrXAeDcYBf5nLOStnjmQe/IZKZHtOi
rBVIgiT0yKbpAgMBAAGjczBxMFAGA1UdEQRJMEeCFmlkcC50dXNjb24uc2FmZW1s
cy5uZXSGLWh0dHBzOi8vaWRwLnR1c2Nvbi5zYWZlbWxzLm5ldC9pZHAvc2hpYmJv
bGV0aDAdBgNVHQ4EFgQUG+gjpDJGdIQzG28ES2R9d1ayPl4wDQYJKoZIhvcNAQEF
BQADggEBAAcc2ejkzZFplFm64VTHsWpSn44lmHrvhwdyekxNq6d5pUxU5T7xaYMT
puZEtZtMOPJhmp87mO9qVlvIGNn9KSWIiflXo3gVXu3XE9Pv/DNGq3xzFl54TGrn
hxgLufnXNF3S/UJD4OnC3NDS9mV9z251aEkhI4qMDCcR8beO8uVVU8fHXA/JWwQ6
mpDahUoUnk3AgUscd9N0NnJqYAXrUiImuTws6cvJHYCzpvX3BPOPRmh2QGg+iG4e
EECNMLuksKyUNnkH7yEvCh5nGwLLoDFzAgs6dOcpybUj2L2jvu7DbCq/xiWth8LW
Rz5qDV8FGKYy9GL0vTWh18JkkMPDeD0=',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);


$metadata['https://idp.hicmls.safemls.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.hicmls.safemls.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'http://idp.hicmls.safemls.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'http://idp.hicmls.safemls.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'http://idp.hicmls.safemls.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://idp.hicmls.safemls.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'http://idp.hicmls.safemls.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDQzCCAiugAwIBAgIUZxEJJcManx/kZigcUsXEhjmxmywwDQYJKoZIhvcNAQEF
BQAwITEfMB0GA1UEAxMWaWRwLmhpY21scy5zYWZlbWxzLm5ldDAeFw0xMzAyMDcy
MTA3MzVaFw0zMzAyMDcyMTA3MzVaMCExHzAdBgNVBAMTFmlkcC5oaWNtbHMuc2Fm
ZW1scy5uZXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCDVzrLqIuc
TF31fgAkuywUm9ukp/sD0aqELZ3csDCv+Ge1n2/XGNozFnFbIDGNWMaLw8kHtK+c
cFU+XgQQi1YpwPRmKzpP7kMMRJIKEPAoYF1GtVaRBmPOUOsDftCNDD2usDEYpvhq
ZwqmCL7/VvT7cX1JRGReQnfU79MDndG9NE0URTr7+jNyN3mwqxa8XnlVJG99N9RZ
P5PfOBcY4aGrSV43U4ZoBo1Mlm7XxSvIkTEfwhKqCZl0H1Jej+KrsiqeJbNKZPr1
UcYeRx1vBSRb5ns4IQgCjxWHWR2ldYdEBJ67V/bWBovhgpnzMbniFhDZ9qSJsFIb
gRC2DmAVKWJpAgMBAAGjczBxMFAGA1UdEQRJMEeCFmlkcC5oaWNtbHMuc2FmZW1s
cy5uZXSGLWh0dHBzOi8vaWRwLmhpY21scy5zYWZlbWxzLm5ldC9pZHAvc2hpYmJv
bGV0aDAdBgNVHQ4EFgQU4rqWgTwA+VBMLj2TdDTqXR2nJ4AwDQYJKoZIhvcNAQEF
BQADggEBABXpQtd/MWQcTmdQesFVjTu9ycvdZU9dIwFifobW4vFq7JgAvRr5IJ2l
xW8v73Uo30ogU9/UNWZWgwHHgK2qM3uFuEXMeOAveBgSdQZ2VpCiXFRwsIzuDfgg
zB8MTsDpu/XxokcVgH86G5SDxTQUDP1+7e5vwq25DBx8B7fJCaNDjvS9ZkC721WH
o+gTlGTgY+tX8s4cvtR5xu7mQN+KFy3qFjlQtMliLD6IhaH88yKS4czG9iMJ/Xo6
d7aYCHaGJ7SdcnEfos4BGTFXN70H9WtnWfgGlH4SIs2tmmWVoSHArdh98SpNQX7d
vQAgfPIIzFiS3mDcv8z/ook01cK8XU8=',
    ),
  ),
  'scope' => 
  array (
    0 => 'safemls.net',
  ),
);
/*
 * Guest IdP. allows users to sign up and register. Great for testing!
 */

/* $metadata['https://openidp.feide.no'] = array(
	'name' => array(
		'en' => 'Feide OpenIdP - guest users',
		'no' => 'Feide Gjestebrukere',
	),
	'description'          => 'Here you can login with your account on Feide RnD OpenID. If you do not already have an account on this identity provider, you can create a new one by following the create new account link and follow the instructions.',

	'SingleSignOnService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => 'c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb'
); */