<?php
/**
 * SAML 1.1 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://rnd.feide.no/content/idp-remote-metadata-reference
 */

/*
$metadata['theproviderid-of-the-idp'] = array(
	'SingleSignOnService'  => 'https://idp.example.org/shibboleth-idp/SSO',
	'certFingerprint'      => 'c7279a9f28f11380509e072441e3dc55fb9ab864',
);
*/

// Below metadata was generated using www/admin/metadata-converter.php, submitting XML available at the IdP URL

$metadata['https://idp.clareitystore.net/idp/shibboleth'] = array (
  'entityid' => 'https://idp.clareitystore.net/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'shib13-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://idp.clareitystore.net/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp.clareitystore.net/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://idp.clareitystore.net/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp.clareitystore.net/idp/profile/SAML2/Redirect/SSO',
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:namees:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.clareitystore.net/idp/profile/SAML2/SOAP/ECP',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => '
MIIDQDCCAiigAwIBAgIVAPwiCw3nBiXgNssoNkZh3XXLo4BOMA0GCSqGSIb3DQEB BQUAMCAxHjAcBgNVBAMTFWlkcC5jbGFyZWl0eXN0b3JlLm5ldDAeFw0xMzA0MTUx OTEwMDdaFw0zMzA0MTUxOTEwMDdaMCAxHjAcBgNVBAMTFWlkcC5jbGFyZWl0eXN0 b3JlLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKUqPoEwn1tT +wCfTlvUcfZwkYws4lPxfUoUGpgpCXQbkLQhzDmm/tAA+dChIoaCOxmYPpMYhfrr Dlrjzl7WjwUl03/yB6ZZqc4qmriv3VGMzRg6dsy5WAzLcEdixlknL28CqZxjuMj9 6nXhllM67loYuuQamCrSXtEbRETx4hWHZAOJjRzhkLv7D8y4Ftt0j0cQOhCuYM16 rnum68KRvOG7f4PAu6QmROndvwUf7qMG4QBvhcWutd3afjfbsfI5Jg7nJVNKCRjh xhYRgLbyq2AmTjAua/v1sbuFsDI92foxTq1wV98yC0kAwgDclgvpFnIrgM1zaVcX Wh9CgnKerwMCAwEAAaNxMG8wTgYDVR0RBEcwRYIVaWRwLmNsYXJlaXR5c3RvcmUu bmV0hixodHRwczovL2lkcC5jbGFyZWl0eXN0b3JlLm5ldC9pZHAvc2hpYmJvbGV0 aDAdBgNVHQ4EFgQUjW1xli4GNB53mZmypXw7LhnRH2IwDQYJKoZIhvcNAQEFBQAD ggEBAGPZrf8prSyR9A4NmENKQ9dCtTxWDz8r5pYjB6ZNv616X2T4jwSrVjToVi3v GA4uXrus/ddRirQHPKQCUCVfW+aNn2Ruix3TpY6mIkLyM3ExlX+QHargTzhKgRDt zUzr+e+1FAcUCF9AWchhB1YmPD16vYzXdhazffHr6XR5vdo5rURx1I7Rm/+Spq5A yiK19MlQrKAwyN/iHaPhjjrVKtvxnoNwRJBEMKcpWO+J4Lc5NWuPmTkB2HD4sCPJ C9IBfjz4pgb3YWhA+9kf4Jnsf70dE/Ijxfk20BhFPCwut7zaNlvKU2USdazY9kiN rwN0dIdbNBJwJEd8Qq43wA9wHyk=
',
    ),
  ),
  'scope' => 
  array (
    0 => 'clareitystore.net',
  ),
);
