-- MySQL dump 10.11
--
-- Host: localhost    Database: fullforce_alen
-- ------------------------------------------------------
-- Server version	5.0.51a-24+lenny4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `administrator` (
  `id` int(11) NOT NULL auto_increment,
  `member_id` int(30) NOT NULL,
  `created_at` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount_code`
--

DROP TABLE IF EXISTS `discount_code`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `discount_code` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `discount_code`
--

LOCK TABLES `discount_code` WRITE;
/*!40000 ALTER TABLE `discount_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facebook_application`
--

DROP TABLE IF EXISTS `facebook_application`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facebook_application` (
  `id` int(11) NOT NULL auto_increment,
  `facebook_app_id` varchar(256) default NULL,
  `facebook_api_key` varchar(256) default NULL,
  `facebook_app_secret` varchar(256) default NULL,
  `created_at` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facebook_application`
--

LOCK TABLES `facebook_application` WRITE;
/*!40000 ALTER TABLE `facebook_application` DISABLE KEYS */;
/*!40000 ALTER TABLE `facebook_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listing`
--

DROP TABLE IF EXISTS `listing`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `listing` (
  `id` int(11) NOT NULL auto_increment,
  `member_id` int(11) NOT NULL,
  `list_price` varchar(75) default NULL,
  `unit_type` int(11) default NULL,
  `bedrooms` int(5) default NULL,
  `bathrooms` int(5) default NULL,
  `address` varchar(256) default NULL,
  `address2` varchar(256) default NULL,
  `city` varchar(75) default NULL,
  `state` varchar(75) default NULL,
  `country` varchar(75) default NULL,
  `zip` varchar(25) default NULL,
  `mls_number` varchar(50) default NULL,
  `link` varchar(256) default NULL,
  `image` varchar(256) default NULL,
  `published` int(3) default '0',
  `paypal_transaction_id` varchar(255) default NULL,
  `created_at` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `listing`
--

LOCK TABLES `listing` WRITE;
/*!40000 ALTER TABLE `listing` DISABLE KEYS */;
INSERT INTO `listing` VALUES (25,1,'4444444',3,3,5,'11 apple tree lane','post office box 4','Toronto','Ontario','CA','',NULL,NULL,NULL,1,'2G9040899J357445E','2011-05-01 17:51:47');
/*!40000 ALTER TABLE `listing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `member` (
  `id` int(11) NOT NULL auto_increment,
  `application_id` int(11) NOT NULL,
  `uid` varchar(30) NOT NULL,
  `access_token` text,
  `email` varchar(256) NOT NULL,
  `last_name` varchar(100) default NULL,
  `first_name` varchar(100) default NULL,
  `discount_code_id` int(11) default NULL,
  `company_name` varchar(256) default NULL,
  `company_link` varchar(256) default NULL,
  `address` varchar(256) default NULL,
  `address2` varchar(256) default NULL,
  `city` varchar(75) default NULL,
  `state` varchar(75) default NULL,
  `country` varchar(75) default NULL,
  `zip` varchar(25) default NULL,
  `phone` varchar(75) default NULL,
  `business_email` varchar(256) default NULL,
  `birthday` varchar(150) default NULL,
  `current_city` varchar(75) default NULL,
  `current_state` varchar(50) default NULL,
  `current_country` varchar(50) default NULL,
  `hometown_city` varchar(75) default NULL,
  `hometown_state` varchar(50) default NULL,
  `hometown_country` varchar(50) default NULL,
  `facebook_timezone` varchar(5) default NULL,
  `facebook_locale` varchar(10) default NULL,
  `facebook_friend_count` int(11) default NULL,
  `twitter_id` int(11) default NULL,
  `twitter_screen_name` varchar(50) default NULL,
  `twitter_name` varchar(50) default NULL,
  `twitter_followers` int(11) default NULL,
  `gender` varchar(10) default NULL,
  `created_at` datetime default NULL,
  `twitter_request_token` text,
  `twitter_access_token` text,
  `facebook_publish_flag` char(1) default '0',
  `twitter_publish_flag` char(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,0,'734450163','59807936315|9733b4019c0d1b3c2d084ec6.1-734450163|sOEshFaYfUifC3ggmua-tA2OOfo','alen@alpha-male.tv','Bubich','Alen',NULL,'Alpha Male Productions','www.realtyco.co.uk',NULL,NULL,NULL,NULL,NULL,NULL,'416-555-5541',NULL,'12/23/1974','Toronto','Ontario',NULL,'Thorold','Ontario',NULL,NULL,NULL,1494,NULL,NULL,NULL,NULL,NULL,'2011-04-25 19:18:29',NULL,NULL,'0','0'),(2,0,'100002054938172','59807936315|6832d801f743eab6a1f2e276.1-100002054938172|WPhkP4vZGpw7cgeBCtN65tqtvy0','collective@alpha-male.tv','Buenovista','Jen',NULL,'JenCo','www.jenco.com','454 somwhere','','Toronto','Ontario','CA','mmm5545','555-555-5555',NULL,'12/23/1985','Toronto','Ontario',NULL,'Toronto','Ontario',NULL,NULL,NULL,154,NULL,NULL,NULL,NULL,NULL,'2011-04-27 08:44:29',NULL,NULL,'0','0'),(3,0,'100000080226660','207684265933093|366dc37dac3fc0f0d50da9b5.1-100000080226660|twHa8dCcLpktdx7J8PnM8bJ2lGE','t.rainbolt@rogers.com','Rainbolt','Tomas',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'01/22/1974','Thornhill','Ontario',NULL,'Newcastle','Wyoming',NULL,NULL,NULL,95,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','0'),(4,0,'783264881','59807936315|10a554c6adc87885722ea74a.1-783264881|0VgLccHjzqKofA68PZ_oNWB_2lA','dara.daniyal@gmail.com','Nawaz','Daniyal',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'04/17/1988','Lahore','Pakistan',NULL,NULL,NULL,NULL,NULL,NULL,21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','0'),(5,0,'','{\"error\":{\"type\":\"OAuthException\",\"message\":\"Error validating client secret.\"}}','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','0'),(6,0,'','{\"error\":{\"type\":\"OAuthException\",\"message\":\"Error validating client secret.\"}}','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','0'),(7,0,'797575183','207684265933093|a55054b85df493ec24de36c3.1-797575183|y0mHF7ZX1O-B1gd3xfU2p37l1wY','james.kupka@gmail.com','Kupka','James',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'10/02/1977',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,351,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','0');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `payment` (
  `id` int(11) NOT NULL auto_increment,
  `member_id` int(11) NOT NULL,
  `paypal_confirmation_number` varchar(256) NOT NULL,
  `payment_value` varchar(256) NOT NULL,
  `created_at` datetime default NULL,
  `first_name` varchar(150) default NULL,
  `last_name` varchar(150) default NULL,
  `address1` varchar(250) default NULL,
  `address2` varchar(250) default NULL,
  `city` varchar(250) default NULL,
  `state` varchar(50) default NULL,
  `country` varchar(250) default NULL,
  `zip` varchar(50) default NULL,
  `phone` varchar(50) default NULL,
  `amount` varchar(75) default NULL,
  PRIMARY KEY  (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `post` (
  `id` int(11) NOT NULL auto_increment,
  `member_id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL,
  `fb_post_id` varchar(256) NOT NULL,
  `message` text,
  `created_at` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `listing_id` (`listing_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,1,25,'','734450163_10150586409180164',NULL),(2,2,25,'','100002054938172_126839877394509',NULL),(3,1,25,'','734450163_10150586417200164',NULL),(4,2,25,'','100002054938172_126840607394436',NULL),(5,1,25,'','734450163_10150586421740164',NULL),(6,2,25,'','100002054938172_126841094061054',NULL);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `unit` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(75) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT INTO `unit` VALUES (1,'Condo'),(2,'Apartment'),(3,'Townhouse'),(4,'Bungalow'),(5,'Bi-Level'),(6,'Tri-Level');
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-05-22 14:51:26
