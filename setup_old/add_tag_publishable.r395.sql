ALTER TABLE `tag` ADD COLUMN `publishable` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'Is this just a placeholder tag or will be actually select tagged content based on it.\n'  AFTER `content_pref_default` ;
UPDATE `tag` SET `publishable`='0' WHERE `ref_code`='CONTENT_LOCAL';
UPDATE `tag` SET `publishable`='0' WHERE `ref_code`='LISTINGS';
