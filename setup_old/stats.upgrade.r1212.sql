DROP TABLE IF EXISTS `click_stats_aggregated_by_date`;
CREATE TABLE IF NOT EXISTS `click_stats_aggregated_by_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dateindex` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Click stats per date' AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `click_stats_aggregated_by_listing`;
CREATE TABLE IF NOT EXISTS `click_stats_aggregated_by_listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `listing_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date_listid` (`date`,`listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Click stats per listing' AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `click_stats_aggregated_by_member`;
CREATE TABLE IF NOT EXISTS `click_stats_aggregated_by_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `member_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date_memberid` (`date`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Click stats per member' AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `click_stats_aggregated_by_listing_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listing_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `listing_memberid` (`listing_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Click stats per member' AUTO_INCREMENT=1 ;
