ALTER TABLE `tag` ADD COLUMN `tag_group_id` INT(11) UNSIGNED NOT NULL DEFAULT 0  AFTER `name` ;

CREATE TABLE `tag_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(62) NOT NULL,
  `slogan` varchar(127) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `tag_group` (`name`, `slogan`) VALUES ('Build Trust', 'Let them know that you mean business.');
INSERT INTO `tag_group` (`name`, `slogan`) VALUES ('Get Noticed', 'Generate buzz; become a focal point; engage your network.');
INSERT INTO `tag_group` (`name`, `slogan`) VALUES ('Be Inspiring', 'Let them know that your\'re more than just a business person.');


