CREATE  TABLE `state` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `country_code` VARCHAR(2) NULL ,
  `code` VARCHAR(2) NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `country_code_code` (`country_code` ASC, `code` ASC) )
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'ON', 'Ontario');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'QC', 'Quebec');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'NS', 'Nova Scotia');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'NB', 'New Brunswick');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'MB', 'Manitoba');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'BC', 'British Columbia');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'PE', 'Prince Edward Island');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'SK', 'Saskatchewan');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'AB', 'Alberta');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'NL', 'Newfoundland and Labrador');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'NT', 'Northwest Territories');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'YT', 'Yukon');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('CA', 'NU', 'Nunavut');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'AL', 'Alabama');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'AK', 'Alaska');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'AZ', 'Arizona');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'AR', 'Arkansas');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'CA', 'California');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'CO', 'Colorado');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'CT', 'Connecticut');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'DE', 'Delaware');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'FL', 'Florida');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'GA', 'Georgia');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'HI', 'Hawaii');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'ID', 'Idaho');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'IL', 'Illinois');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'IN', 'Indiana');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'IA', 'Iowa');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'KS', 'Kansas');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'KY', 'Kentucky');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'LA', 'Louisiana');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'ME', 'Maine');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'MD', 'Maryland');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'MA', 'Massachusetts');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'MI', 'Michigan');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'MN', 'Minnesota');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'MS', 'Mississippi');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'MO', 'Missouri');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'MT', 'Montana');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'NE', 'Nebraska');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'NV', 'Nevada');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'NH', 'New Hampshire');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'NJ', 'New Jersey');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'NM', 'New Mexico');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'NY', 'New York');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'NC', 'North Carolina');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'ND', 'North Dakota');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'OH', 'Ohio');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'OK', 'Oklahoma');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'OR', 'Oregon');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'PA', 'Pennsylvania');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'RI', 'Rhode Island');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'SC', 'South Carolina');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'SD', 'South Dakota');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'TN', 'Tennessee');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'TX', 'Texas');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'UT', 'Utah');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'VT', 'Vermont');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'VA', 'Virginia');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'WA', 'Washington');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'WV', 'West Virginia');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'WI', 'Wisconsin');
INSERT INTO `state` (`country_code`, `code`, `name`) VALUES ('US', 'WY', 'Wyoming');


