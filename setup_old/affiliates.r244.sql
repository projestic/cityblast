CREATE TABLE `affiliate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned DEFAULT NULL,
  `first_payout` double DEFAULT NULL,
  `recurring_payout` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `public_referrer_id` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `payable` CHANGE `amount` `amount` DOUBLE UNSIGNED NOT NULL;

ALTER TABLE `member` CHANGE `affiliate_first_payout` `affiliate_first_payout` VARCHAR(10)  CHARACTER SET utf8  NULL  DEFAULT NULL;
ALTER TABLE `member` CHANGE `affiliate_recurring_payout` `affiliate_recurring_payout` VARCHAR(10)  CHARACTER SET utf8  NULL  DEFAULT NULL;
ALTER TABLE `member` CHANGE `affiliate_id` `referring_affiliate_id` VARCHAR(11)  CHARACTER SET utf8  NULL  DEFAULT NULL;
ALTER TABLE `member` ADD `referring_member_id` VARCHAR(11) CHARACTER SET utf8  NULL DEFAULT NULL AFTER is_affiliate_member ;
ALTER TABLE `member` MODIFY COLUMN `referring_member_id` VARCHAR(11) CHARACTER SET utf8 DEFAULT NULL AFTER `referring_affiliate_id`;

INSERT INTO affiliate (member_id, public_referrer_id, first_payout, recurring_payout, price) SELECT id, id, 31.99, 7.99, 39.99 FROM member WHERE is_affiliate_member = 'YES';
INSERT INTO affiliate (member_id, public_referrer_id, first_payout, recurring_payout, price) SELECT DISTINCT affiliate_id, affiliate_id, 31.99, 7.99, 39.99 FROM promo LEFT JOIN affiliate ON affiliate.member_id = promo.affiliate_id WHERE affiliate.id IS NULL;

UPDATE promo SET affiliate_id = (SELECT id FROM affiliate WHERE member_id = promo.affiliate_id);
UPDATE payable SET affiliate_id = (SELECT id FROM affiliate WHERE member_id = payable.affiliate_id);

UPDATE member SET referring_member_id = referring_affiliate_id, referring_affiliate_id = NULL;
UPDATE member SET referring_affiliate_id = (SELECT id FROM affiliate WHERE member_id = referring_member_id);
UPDATE member SET referring_member_id = NULL where referring_affiliate_id IS NOT NULL;

UPDATE affiliate SET first_payout = 11.99, recurring_payout = 3, price = 14.99 WHERE member_id = 1182;
UPDATE affiliate SET first_payout = 0, recurring_payout = 0 WHERE member_id IN (35, 40, 75, 581, 1134, 1183, 1184);

