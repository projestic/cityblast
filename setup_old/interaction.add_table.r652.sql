CREATE TABLE `interaction` (
`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
`type` ENUM( 'facebook_share', 'twitter_share', 'google_plus_share', 'share_listing_email' ) NOT NULL ,
`referring_agent_id` INT( 11 ) UNSIGNED NOT NULL ,
`listing_id` INT( 11 ) UNSIGNED NOT NULL ,
`created_at` DATETIME NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_general_ci;