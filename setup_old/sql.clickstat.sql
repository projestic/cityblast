CREATE TABLE  `click_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `listing_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `source` varchar (100),
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `click_stat` ADD COLUMN `source` varchar(100)  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `count`;

ALTER TABLE `click_stat` MODIFY COLUMN `date` DATE  NOT NULL;

CREATE TABLE  `click_stat_hour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
