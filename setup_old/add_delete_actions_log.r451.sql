CREATE TABLE `delete_actions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `administrator_id` int(11) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `reason` text NOT NULL,
  `post_id` int(11) unsigned DEFAULT NULL,
  `listing_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `administrator_id` (`administrator_id`),
  KEY `post_id` (`post_id`),
  KEY `listing_id` (`listing_id`),
  CONSTRAINT `delete_actions_ibfk_4` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE,
  CONSTRAINT `delete_actions_ibfk_1` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`id`) ON DELETE CASCADE,
  CONSTRAINT `delete_actions_ibfk_3` FOREIGN KEY (`listing_id`) REFERENCES `listing` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



