
DROP TABLE IF EXISTS `dashboard_stats`;
CREATE TABLE IF NOT EXISTS `dashboard_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avg_clicks` float(9,2) NULL,
  `avg_likes`  float(9,2) NULL,
  `avg_comments` float(9,2) NULL,
  `avg_shares`  float(9,2) NULL,
  `avg_buzz`  float(9,2) NULL, 
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;