TRUNCATE content_pref;

INSERT INTO content_pref (member_id, tag_id, pref, created_by, created_at)
	SELECT 
		m.id AS member_id,
		t.id AS tag_id,
		1 AS pref,
		m.id AS created_by,
		NOW() AS created_at
	FROM tag t
	INNER JOIN member m ON (m.content_type = 'both' || m.content_type = 'content_only')
	WHERE t.ref_code = 'CONTENT_LOCAL';
	
INSERT INTO content_pref (member_id, tag_id, pref, created_by, created_at)
	SELECT 
		m.id AS member_id,
		t.id AS tag_id,
		0 AS pref,
		m.id AS created_by,
		NOW() AS created_at
	FROM tag t
	INNER JOIN member m ON m.content_type = 'listings_only'
	WHERE t.ref_code = 'CONTENT_LOCAL';
	
INSERT INTO content_pref (member_id, tag_id, pref, created_by, created_at)
	SELECT 
		m.id AS member_id,
		t.id AS tag_id,
		1 AS pref,
		m.id AS created_by,
		NOW() AS created_at
	FROM tag t
	INNER JOIN member m ON (m.content_type = 'both' || m.content_type = 'listings_only')
	WHERE t.ref_code = 'LISTINGS';
	
INSERT INTO content_pref (member_id, tag_id, pref, created_by, created_at)
	SELECT 
		m.id AS member_id,
		t.id AS tag_id,
		0 AS pref,
		m.id AS created_by,
		NOW() AS created_at
	FROM tag t
	INNER JOIN member m ON m.content_type = 'content_only'
	WHERE t.ref_code = 'LISTINGS';
	
	
INSERT INTO content_pref (member_id, tag_id, pref, created_by, created_at)
	SELECT 
		m.id AS member_id,
		t.id AS tag_id,
		1 AS pref,
		m.id AS created_by,
		NOW() AS created_at
	FROM tag t
	INNER JOIN member m ON m.id > 0
	WHERE t.id IN (2, 3, 5, 6, 12, 13, 14);
	
INSERT INTO content_pref (member_id, tag_id, pref, created_by, created_at)
	SELECT 
		m.id AS member_id,
		t.id AS tag_id,
		0 AS pref,
		m.id AS created_by,
		NOW() AS created_at
	FROM tag t
	INNER JOIN member m ON m.id > 0
	WHERE t.id NOT IN (2, 3, 5, 6, 12, 13, 14, 1, 4);
	
