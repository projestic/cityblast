
CREATE TABLE IF NOT EXISTS `click_stats_aggregated_by_listing_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listing_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `listing_memberid` (`listing_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Click stats per member' AUTO_INCREMENT=1 ;
