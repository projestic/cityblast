DROP TABLE  `referral_credit`;
CREATE TABLE IF NOT EXISTS `referral_credit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `redeemed` int(1) default 0,  
  `redemption_date` datetime,
  `status` enum ('sent','approved') default 'signup',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
