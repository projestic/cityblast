ALTER TABLE `administrator` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
OPTIMIZE TABLE `administrator`;

ALTER TABLE `blast_reach_queue` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
OPTIMIZE TABLE `blast_reach_queue`;

ALTER TABLE `comments` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `comments` CHANGE `fb_comment_id` `fb_comment_id` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
CHANGE `fb_name` `fb_name` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
CHANGE `text` `text` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
OPTIMIZE TABLE `comments`;

ALTER TABLE `discount_code` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `discount_code` CHANGE `code` `code` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
OPTIMIZE TABLE `discount_code`;

ALTER TABLE `email_sent` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `email_sent` CHANGE `type` `type` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
CHANGE `email` `email` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
CHANGE `subject` `subject` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
OPTIMIZE TABLE `email_sent`;

ALTER TABLE `facebook_application` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `facebook_application` CHANGE `facebook_app_id` `facebook_app_id` VARCHAR( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
CHANGE `facebook_api_key` `facebook_api_key` VARCHAR( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
CHANGE `facebook_app_secret` `facebook_app_secret` VARCHAR( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
REPAIR TABLE `facebook_application`;

ALTER TABLE `friends` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `friends` CHANGE `social_profile_id` `social_profile_id` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CHANGE `name` `name` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CHANGE `channel` `channel` ENUM( 'facebook', 'twitter', 'linkedin' ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'facebook';
REPAIR TABLE `friends`;

ALTER TABLE `hopers_queue` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
OPTIMIZE TABLE `hopers_queue`;

ALTER TABLE `listing` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `listing` CHANGE `message` `message` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `approval` `approval` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
CHANGE `street` `street` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `price` `price` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `city` `city` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `area` `area` VARCHAR(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CHANGE `community` `community` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CHANGE `postal_code` `postal_code` VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `appartment` `appartment` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `image` `image` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `content_link` `content_link` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `blast_type` `blast_type` ENUM( 'BLAST', 'MANUAL', 'SMLS' ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `latitude` `latitude` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `longitude` `longitude` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `mls` `mls` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `publish` `publish` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
CHANGE `status` `status` ENUM( 'active', 'deleted' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'active',
CHANGE `prop_type` `prop_type` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CHANGE `alt_image_1` `alt_image_1` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `alt_image_2` `alt_image_2` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `alt_image_3` `alt_image_3` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `alt_image_4` `alt_image_4` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `broker_of_record` `broker_of_record` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CHANGE `list_agent` `list_agent` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `list_agent_phone` `list_agent_phone` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
OPTIMIZE TABLE `listing`;

ALTER TABLE `payment` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `payment` CHANGE `paypal_confirmation_number` `paypal_confirmation_number` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CHANGE `first_name` `first_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `last_name` `last_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `address1` `address1` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `address2` `address2` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `city` `city` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `state` `state` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `country` `country` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `zip` `zip` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `phone` `phone` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `amount` `amount` VARCHAR(75) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `cc_expire_date` `cc_expire_date` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CHANGE `last_four` `last_four` VARCHAR(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
OPTIMIZE TABLE `payment`;

ALTER TABLE `post` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `post` CHANGE `fb_post_id` `fb_post_id` VARCHAR( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
CHANGE `message` `message` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
CHANGE `twitter_post_id` `twitter_post_id` VARCHAR( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
CHANGE `type` `type` ENUM( 'FACEBOOK', 'TWITTER', 'LINKEDIN' ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
OPTIMIZE TABLE `post`;

ALTER TABLE `publishing_queue` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
OPTIMIZE TABLE `publishing_queue`;



