--Manual Stop Publishing--

CREATE TABLE `publish_settings` (
  `id` int  NOT NULL AUTO_INCREMENT,
  `member_id` int  NOT NULL,
  `publish_type` enum('FACEBOOK','TWITTER','LINKEDIN')  NOT NULL,
  `changed_to` enum('ON','OFF')  NOT NULL,
  `change_date` date  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM;

