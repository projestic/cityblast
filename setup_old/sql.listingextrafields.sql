--"Extra" fields--

ALTER TABLE `listing` ADD COLUMN `number_of_bedrooms` INT(1)  NOT NULL AFTER `city_id`,
 ADD COLUMN `number_of_bathrooms` int(1)  NOT NULL AFTER `number_of_bedrooms`,
 ADD COLUMN `number_of_parking` int(1)  NOT NULL AFTER `number_of_bathrooms`,
 ADD COLUMN `alt_image_1` varchar(265)  NOT NULL AFTER `number_of_parking`,
 ADD COLUMN `alt_image_2` varchar(265)  NOT NULL AFTER `alt_image_1`,
 ADD COLUMN `alt_image_3` varchar(265)  NOT NULL AFTER `alt_image_2`,
 ADD COLUMN `alt_image_4` varchar(265)  NOT NULL AFTER `alt_image_3`;

