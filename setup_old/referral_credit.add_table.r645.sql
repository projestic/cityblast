CREATE TABLE `referral_credit` (
`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
`member_id` INT( 11 ) UNSIGNED NOT NULL ,
`referred_member_id` INT( 11 ) UNSIGNED NOT NULL ,
`created_at` DATETIME NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_general_ci;