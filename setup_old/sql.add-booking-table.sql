
CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL,
  `email` varchar(256) DEFAULT NULL,
  `phone` varchar(75) DEFAULT NULL,
  `message` text,
  `created_at` datetime DEFAULT NULL,
  `contact_by` enum('EMAIL','PHONE') DEFAULT NULL,
  `best_time` varchar(265) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `listing_id` (`listing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;