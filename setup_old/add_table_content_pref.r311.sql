CREATE TABLE `content_pref` (
  `member_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  `pref` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`member_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `tag` ADD COLUMN `content_pref_default` TINYINT UNSIGNED NOT NULL DEFAULT 0  AFTER `tag_group_id` ;

