CREATE TABLE IF NOT EXISTS `hopers_queue` (
  `hoper_code` varchar(5) NOT NULL,
  `listing_id` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) DEFAULT NULL,
  `counter` int(11) NOT NULL,
  PRIMARY KEY (`hoper_code`,`listing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hopers_queue`
--

INSERT INTO `hopers_queue` (`hoper_code`, `listing_id`, `priority`, `counter`) VALUES
('A', 0, NULL, 0),
('B', 0, NULL, 0),
('C', 0, NULL, 0),
('D', 0, NULL, 0),
('E', 0, NULL, 0),
('F', 0, NULL, 0);

ALTER TABLE  `member` ADD  `current_hoper` VARCHAR( 2 ) NULL;
ALTER TABLE  `member` ADD  `current_hoper_status` ENUM(  'pending',  'completed' ) NOT NULL default 'pending';
