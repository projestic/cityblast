CREATE TABLE `member_landing_page` (
  `member_id` int(10) unsigned NOT NULL,
  `group_name` varchar(63) NOT NULL,
  `page_name` varchar(63) NOT NULL,
  `url` varchar(127) NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

