ALTER TABLE `promo` CHANGE `price` `amount` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `promo` ADD `type` ENUM( 'FIXED_AMOUNT', 'PERCENT' ) NOT NULL;

ALTER TABLE `listing` ADD `big_image` INT( 1 ) NOT NULL DEFAULT '0';

ALTER TABLE `listing` ADD `email_passthru` INT( 1 ) NOT NULL DEFAULT '0';

/*

ALTER TABLE `member` ADD `promo_redeemed` INT( 1 ) NOT NULL DEFAULT '0';

*/