CREATE TABLE `tag` (
 id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(75) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `tag_cloud` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `tag_id` int(11) NOT NULL,
 `listing_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `tag_id` (`tag_id`),
 KEY `listing_id` (`listing_id`)
) ENGINE=MyISAM DEFAULT;

INSERT INTO `tag` (`name`) VALUES ('lifestyle'), ('informational'), ('inspirational quotes');

ALTER TABLE `listing` DROP `classification`;