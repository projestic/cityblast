ALTER TABLE `listing` CHANGE `status` `status` ENUM( 'active', 'deleted', 'expired' ) NOT NULL DEFAULT 'active'

ALTER TABLE `listing` CHANGE `bedrooms` `number_of_bedrooms` INT( 5 ) UNSIGNED NULL DEFAULT NULL
ALTER TABLE `listing` CHANGE `published` `published_on` DATETIME NULL 

ALTER TABLE `listing` ADD `number_of_bathrooms` INT( 5 ) UNSIGNED NOT NULL AFTER `number_of_bedrooms` ,
ADD `number_of_parking` INT( 9 ) UNSIGNED NOT NULL AFTER `number_of_bathrooms` ,
ADD `prop_type` VARCHAR( 200 ) NOT NULL AFTER `number_of_parking` ,
ADD `maintenance_fee` DOUBLE( 12, 2 ) NOT NULL AFTER `prop_type` ,
ADD `taxes` DOUBLE( 12, 2 ) NOT NULL AFTER `maintenance_fee` ,
ADD `broker_of_record` TEXT NOT NULL AFTER `taxes`,
ADD `price` DOUBLE( 12, 2 ) NOT NULL AFTER `broker_of_record` ,
ADD `street` VARCHAR( 255 ) NOT NULL AFTER `price` ,
ADD `postal_code` VARCHAR( 12 ) NOT NULL AFTER `street` ,
ADD `image` TEXT NOT NULL AFTER `postal_code` ,
ADD `city` VARCHAR( 55 ) NOT NULL AFTER `image` ,
ADD `area` VARCHAR( 55 ) NOT NULL AFTER `city` ,
ADD `community` VARCHAR( 100 ) NOT NULL AFTER `area`,
ADD `publish` SMALLINT UNSIGNED NOT NULL DEFAULT '1' AFTER `community` ,
ADD `blast_type` VARCHAR( 55 ) NOT NULL DEFAULT 'SMLS' AFTER `publish` ,
ADD `city_id` SMALLINT UNSIGNED NOT NULL DEFAULT '1' AFTER `blast_type`,
ADD `appartment` VARCHAR( 255 ) NOT NULL AFTER `street_sfx`



