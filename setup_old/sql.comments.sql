CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(255) NOT NULL,
  `fb_comment_id` varchar(100) NOT NULL,
  `fb_uid` int(13) NOT NULL,
  `fb_name` varchar(255) NOT NULL,
  `fb_admin_member_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `mid` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;
