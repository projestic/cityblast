CREATE TABLE `member_payment_status_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) unsigned NOT NULL,
  `payment_status` enum('signup','signup_expired','paid','free','payment_failed','cancelled','extended_trial','refund_issued') NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO member_payment_status_log (member_id, payment_status, created_at)
	SELECT 
		m.id AS member_id,
		m.payment_status AS payment_status,
		NOW() AS created_at
	FROM member m;

