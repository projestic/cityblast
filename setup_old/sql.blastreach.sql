
DROP TABLE IF EXISTS `blast_reach`;
CREATE TABLE IF NOT EXISTS `blast_reach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL,
  `channel` enum('facebook','linkedin','twitter') NOT NULL,
  `friend_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `blast_reach_queue`;
CREATE TABLE IF NOT EXISTS `blast_reach_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `channel` enum('facebook','linkedin','twitter') NOT NULL,
  `status` enum('pending','completed') NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `friends`;
CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(30) NOT NULL,
  `social_profile_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `channel` enum('facebook','twitter','linkedin') CHARACTER SET latin1 DEFAULT 'facebook',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_id_2` (`member_id`,`social_profile_id`,`channel`),
  KEY `member_id` (`member_id`),
  KEY `social_profile_id` (`social_profile_id`),
  KEY `channel` (`channel`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;