-- phpMyAdmin SQL Dump
-- version 3.3.10
-- http://www.phpmyadmin.net
--
-- Host: mysql.city-blast.com
-- Generation Time: Jul 04, 2011 at 03:25 PM
-- Server version: 5.1.53
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `cityblast_production`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
CREATE TABLE IF NOT EXISTS `administrator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(30) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `discount_code`
--

DROP TABLE IF EXISTS `discount_code`;
CREATE TABLE IF NOT EXISTS `discount_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `facebook_application`
--

DROP TABLE IF EXISTS `facebook_application`;
CREATE TABLE IF NOT EXISTS `facebook_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook_app_id` varchar(256) DEFAULT NULL,
  `facebook_api_key` varchar(256) DEFAULT NULL,
  `facebook_app_secret` varchar(256) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `listing`
--

DROP TABLE IF EXISTS `listing`;
CREATE TABLE IF NOT EXISTS `listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `bedrooms` int(5) DEFAULT NULL,
  `published` int(3) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `message` text,
  `payment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `uid` varchar(30) NOT NULL,
  `access_token` text,
  `email` varchar(256) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `discount_code_id` int(11) DEFAULT NULL,
  `company_name` varchar(256) DEFAULT NULL,
  `company_link` varchar(256) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `address2` varchar(256) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(75) DEFAULT NULL,
  `country` varchar(75) DEFAULT NULL,
  `zip` varchar(25) DEFAULT NULL,
  `phone` varchar(75) DEFAULT NULL,
  `business_email` varchar(256) DEFAULT NULL,
  `birthday` varchar(150) DEFAULT NULL,
  `current_city` varchar(75) DEFAULT NULL,
  `current_state` varchar(50) DEFAULT NULL,
  `current_country` varchar(50) DEFAULT NULL,
  `hometown_city` varchar(75) DEFAULT NULL,
  `hometown_state` varchar(50) DEFAULT NULL,
  `hometown_country` varchar(50) DEFAULT NULL,
  `facebook_timezone` varchar(5) DEFAULT NULL,
  `facebook_locale` varchar(10) DEFAULT NULL,
  `facebook_friend_count` int(11) DEFAULT NULL,
  `twitter_id` int(11) DEFAULT NULL,
  `twitter_screen_name` varchar(50) DEFAULT NULL,
  `twitter_name` varchar(50) DEFAULT NULL,
  `twitter_followers` int(11) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `twitter_request_token` text,
  `twitter_access_token` text,
  `facebook_publish_flag` char(1) DEFAULT '0',
  `twitter_publish_flag` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `paypal_confirmation_number` varchar(256) NOT NULL,
  `payment_value` varchar(256) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `address1` varchar(250) DEFAULT NULL,
  `address2` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `amount` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL,
  `fb_post_id` varchar(256) NOT NULL,
  `message` text,
  `created_at` datetime DEFAULT NULL,
  `twitter_post_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `listing_id` (`listing_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
