
TRUNCATE tag;
TRUNCATE tag_cloud;

INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (1,'CONTENT_LOCAL','Local Market Reports',1,1);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (2,NULL,'Real Estate News',1,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (3,NULL,'Tips and Guides for Clients',1,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (4,'LISTINGS','Local Listing Showcase',1,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (5,NULL,'Real Estate and Technology',1,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (6,NULL,'Top-10 Lists',2,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (7,NULL,'Polls and Quizzes',2,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (8,NULL,'News Around the Web',2,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (9,NULL,'Bold and Buzzworthy',2,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (10,NULL,'Humour',2,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (11,NULL,'Inspirational Quotes',3,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (12,NULL,'Beautiful Homes & Gardens',3,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (13,NULL,'Architecture and Design',3,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (14,NULL,'Tips for Living Well',3,0);
INSERT INTO `tag` (`id`,`ref_code`,`name`,`tag_group_id`,`content_pref_default`) VALUES (15,NULL,'Habitat for Humanity',3,0);
