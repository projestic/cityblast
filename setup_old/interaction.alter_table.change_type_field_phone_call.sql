ALTER TABLE `interaction`
	CHANGE COLUMN `type`
	`type` ENUM(
		'facebook_share'
		, 'twitter_share'
		, 'google_plus_share'
		, 'share_listing_email'
		, 'phone_call'
	) NOT NULL AFTER `id`;
