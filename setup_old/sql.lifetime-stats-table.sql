
DROP TABLE IF EXISTS `lifetime_stats`;
CREATE TABLE IF NOT EXISTS `lifetime_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_clicks` int(40) NULL,
  `total_likes`  int(20) NULL,
  `total_comments` int(11) NULL,
  `total_shares`  int(11) NULL,
  `total_posts`  int(30) NULL, 
  `total_interactions`  int(11) NULL,   
  `total_bookings`  int(11) NULL,   
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;