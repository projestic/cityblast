alter table payment add amount_float float(10,2) after amount;
update payment set amount_float = amount;
alter table payment change amount amount_old varchar(75);
alter table payment change amount_float amount float(10,2);
