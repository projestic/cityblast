CREATE TABLE `event_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_member_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Member who owns the entity. Zero indicates sytem owned entity.',
  `actor_member_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Member who performed the action. Zero incates system action.\n',
  `entity_type` varchar(32) NOT NULL COMMENT 'String identifer for the entity being manipulated.',
  `entity_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Id of the specific entity being manipulated.',
  `event_type` varchar(32) NOT NULL COMMENT 'Common values: create, update, delete',
  `description` varchar(256) NOT NULL DEFAULT '',
  `value_old` text COMMENT 'Optional JSON encoded string or array.',
  `value_new` text COMMENT 'Optional JSON encoded string or array.',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_member_id` (`entity_member_id`),
  KEY `actor_member_id` (`actor_member_id`),
  KEY `entity_type_entity_member` (`entity_member_id`,`entity_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


