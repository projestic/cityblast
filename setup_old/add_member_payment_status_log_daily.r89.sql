CREATE TABLE `member_payment_status_log_daily` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `payment_status` enum('signup','signup_expired','paid','free','payment_failed','cancelled','extended_trial','refund_issued') NOT NULL,
  `member_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `member_date` (`date`,`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

