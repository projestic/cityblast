ALTER TABLE `cash_credit` 
DROP COLUMN `redeemed_at` , 
ADD COLUMN `type` ENUM('CREDIT','DEBIT') NOT NULL  AFTER `member_id` ,
CHANGE COLUMN `note` `note` TEXT NULL  AFTER `type` , 
CHANGE COLUMN `amount` `amount` DECIMAL(10,2) unsigned NOT NULL  , 
CHANGE COLUMN `admin_id` `admin_id` INT(11) NOT NULL DEFAULT 0  , 
ADD COLUMN `balance` DECIMAL(10,2) NOT NULL  AFTER `amount` , 
RENAME TO `cash_trans` ;