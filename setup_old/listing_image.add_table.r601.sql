CREATE TABLE `cityblast_alen`.`listing_image` (
`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
`listing_id` INT( 11 ) UNSIGNED NOT NULL ,
`image` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_general_ci;