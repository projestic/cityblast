CREATE TABLE `note` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `member_id` int(11) NOT NULL,
 `note` text,
 `created_at` datetime NOT NULL,
 `admin_id` int(11) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`),
 KEY `member_id` (`member_id`),
 KEY `created_at` (`created_at`)
) ENGINE=MyISAM;

CREATE TABLE `refund` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `member_id` int(11) NOT NULL,
 `amount` float(10,2) NOT NULL,
 `note` text,
 `created_at` datetime NOT NULL,
 `redeemed_at` datetime DEFAULT NULL,
 `admin_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `member_id` (`member_id`),
 KEY `created_at` (`created_at`)
) ENGINE=MyISAM;