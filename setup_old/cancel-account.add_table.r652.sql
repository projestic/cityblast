drop table cancel_account;

CREATE TABLE `cancel_account` (
`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
`status` ENUM( 'new', 'complete' ) NOT NULL default "new",

`member_id` INT( 11 ) UNSIGNED NOT NULL ,
`admin_id` INT( 11 ) UNSIGNED NULL ,

`created_at` DATETIME NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_general_ci;