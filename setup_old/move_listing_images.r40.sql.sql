ALTER TABLE  `listing` CHANGE  `alt_image_1`  `thumbnail` VARCHAR( 265 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL

INSERT INTO listing_image (listing_id, image)
SELECT id, alt_image_2 FROM listing
WHERE alt_image_2 IS NOT NULL;

INSERT INTO listing_image (listing_id, image)
SELECT id, alt_image_3 FROM listing
WHERE alt_image_3 IS NOT NULL;

INSERT INTO listing_image (listing_id, image)
SELECT id, alt_image_4 FROM listing
WHERE alt_image_4 IS NOT NULL;

ALTER TABLE  `listing` drop column alt_image_2;
ALTER TABLE  `listing` drop column alt_image_3;
ALTER TABLE  `listing` drop column alt_image_4;