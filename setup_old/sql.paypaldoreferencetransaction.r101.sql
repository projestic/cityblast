ALTER TABLE payment add column paypal_transaction_expire_date datetime not null after paypal_confirmation_number;
ALTER TABLE payment add column cc_expire_date varchar(10) not null after  paypal_transaction_expire_date;
