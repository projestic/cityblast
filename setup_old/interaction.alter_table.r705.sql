-- up
ALTER TABLE `interaction` ADD COLUMN `http_user_agent` VARCHAR(128) NULL AFTER `listing_id`;
ALTER TABLE `interaction` ADD COLUMN `remote_addr` VARCHAR(64) NULL AFTER `http_user_agent`;
ALTER TABLE `interaction` ADD COLUMN `remote_host` VARCHAR(128) NULL AFTER `remote_addr`;

-- down
-- ALTER TABLE `interaction` DROP COLUMN `http_user_agent`;
-- ALTER TABLE `interaction` DROP COLUMN `remote_addr`;
-- ALTER TABLE `interaction` DROP COLUMN `remote_host`;