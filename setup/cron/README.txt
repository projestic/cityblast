When deploying to a specific environment, the .cron file in this directory corresponding to that environment is also loaded into the server's crontab. 

Note: removing a file from this directory does not remove it from the crontab on the web server(s)! It only stops the crontab from being updated in the future.