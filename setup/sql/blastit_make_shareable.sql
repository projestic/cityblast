UPDATE member
SET
	access_token = 'x',
	email = 'test@test.com',
	phone = '555-5555-5555',
	twitter_request_token = null,
	twitter_access_token = null,
	linkedin_access_token = null,
	linkedin_request_token = null,
	email_override = null,
	stripe_customer_id = null
WHERE type_id != 5;

TRUNCATE network_app;
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`,`username`,`user_id`) VALUES (1,'FACEBOOK','Facebook App','x','x',NULL,1,'0000-00-00 00:00:00',NULL,NULL);
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`,`username`,`user_id`) VALUES (2,'TWITTER','Twitter App','x','x',NULL,1,'0000-00-00 00:00:00',NULL,NULL);
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`,`username`,`user_id`) VALUES (3,'LINKEDIN','Linkedin App','x','x',NULL,1,'0000-00-00 00:00:00',NULL,NULL);

DELETE FROM post WHERE created_at < '2013-09-01';
DELETE FROM member_status_log WHERE created_at < '2013-09-01';