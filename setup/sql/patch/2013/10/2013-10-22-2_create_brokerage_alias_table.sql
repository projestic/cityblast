CREATE TABLE `brokerage_alias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `brokerage_id` int(11) unsigned NOT NULL,   
  `name` varchar(250) NOT NULL,
  `created_at` datetime,
  PRIMARY KEY (`id`),
  KEY(`brokerage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE brokerage_alias ADD FOREIGN KEY (brokerage_id) REFERENCES brokerage (id);