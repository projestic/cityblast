# If the brokerage is unknown then we dont need a brokerage_id field
ALTER TABLE `brokerage_unknown_member` DROP FOREIGN KEY `brokerage_unknown_member_ibfk_1` ;
ALTER TABLE `brokerage_unknown_member` DROP COLUMN `brokerage_id` 
, DROP INDEX `brokerage_id` ;
