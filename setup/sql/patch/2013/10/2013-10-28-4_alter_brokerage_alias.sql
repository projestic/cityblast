ALTER TABLE `brokerage_alias` DROP FOREIGN KEY `brokerage_alias_ibfk_1` ;
ALTER TABLE `brokerage_alias` 
  ADD CONSTRAINT `brokerage_alias_ibfk_1`
  FOREIGN KEY (`brokerage_id` )
  REFERENCES `brokerage` (`id` )
  ON DELETE CASCADE
  ON UPDATE RESTRICT;