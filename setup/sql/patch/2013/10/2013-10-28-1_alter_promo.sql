ALTER TABLE `promo` ADD COLUMN `brokerage_id` int(11) UNSIGNED; 
ALTER TABLE `promo` ADD INDEX `brokerage_id` (`brokerage_id`);
ALTER TABLE promo ADD FOREIGN KEY (brokerage_id) REFERENCES brokerage (id);