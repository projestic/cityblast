ALTER TABLE `brokerage` RENAME TO  `franchise` ;

ALTER TABLE `brokerage_alias` DROP FOREIGN KEY `brokerage_alias_ibfk_1` ;
ALTER TABLE `brokerage_alias` CHANGE COLUMN `brokerage_id` `franchise_id` INT(11) UNSIGNED NOT NULL  , 
  ADD CONSTRAINT `franchise_alias_ibfk_1`
  FOREIGN KEY (`franchise_id` )
  REFERENCES `franchise` (`id` )
  ON DELETE CASCADE,
RENAME TO `franchise_alias` ;

ALTER TABLE `brokerage_email_root` DROP FOREIGN KEY `brokerage_email_root_ibfk_1` ;
ALTER TABLE `brokerage_email_root` CHANGE COLUMN `brokerage_id` `franchise_id` INT(11) UNSIGNED NOT NULL  , 
  ADD CONSTRAINT `franchise_email_root_ibfk_1`
  FOREIGN KEY (`franchise_id` )
  REFERENCES `franchise` (`id` )
  ON DELETE CASCADE, 
RENAME TO  `franchise_email_root` ;


ALTER TABLE `brokerage_unknown_member` RENAME TO  `franchise_unknown_member` ;


ALTER TABLE `promo` CHANGE COLUMN `brokerage_id` `franchise_id` INT(11) NULL DEFAULT NULL  ;


ALTER TABLE `member` DROP FOREIGN KEY `member_ibfk_2` ;
ALTER TABLE `member` CHANGE COLUMN `brokerage_id` `franchise_id` INT(11) UNSIGNED NULL DEFAULT NULL  , 
  ADD CONSTRAINT `member_ibfk_2`
  FOREIGN KEY (`franchise_id` )
  REFERENCES `franchise` (`id` );



