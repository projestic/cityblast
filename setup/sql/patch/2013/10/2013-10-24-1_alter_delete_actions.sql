ALTER TABLE `delete_actions` DROP FOREIGN KEY `delete_actions_ibfk_1`;
ALTER TABLE `delete_actions` CHANGE `administrator_id` `member_id` INT(11)  UNSIGNED  NULL  DEFAULT NULL;
UPDATE delete_actions SET member_id = (SELECT member_id FROM administrator WHERE delete_actions.member_id = administrator.id);
ALTER TABLE `delete_actions` CHANGE `member_id` `member_id` INT(11) UNSIGNED  NOT NULL;
ALTER TABLE `delete_actions` ADD FOREIGN KEY (`member_id`) REFERENCES `member` (`id`) ON DELETE CASCADE;
