ALTER TABLE `member` ADD COLUMN `brokerage_id` INT(11) NULL;

ALTER TABLE `member` CHANGE COLUMN `brokerage_id` `brokerage_id` INT(11) UNSIGNED NULL  
, ADD INDEX `brokerage_id` (`brokerage_id` ASC) ;

ALTER TABLE member ADD FOREIGN KEY (brokerage_id) REFERENCES brokerage (id);