ALTER TABLE `listing` ADD `pre_approval` TINYINT(1) NOT NULL DEFAULT '0' AFTER `raw_message`;
UPDATE listing SET pre_approval = 1;