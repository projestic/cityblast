ALTER TABLE `brokerage_email_root` DROP FOREIGN KEY `brokerage_email_root_ibfk_1` ;
ALTER TABLE `brokerage_email_root` 
  ADD CONSTRAINT `brokerage_email_root_ibfk_1`
  FOREIGN KEY (`brokerage_id` )
  REFERENCES `brokerage` (`id` )
  ON DELETE CASCADE
  ON UPDATE RESTRICT;
