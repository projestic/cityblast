ALTER TABLE `member_type` ADD `is_customer` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `promo_codes_allowed`;
ALTER TABLE `member_type` ADD `is_admin` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `is_customer`;

ALTER TABLE `member_type` ADD `inherits_from_type_id` INT(11)  UNSIGNED  NULL  DEFAULT NULL  AFTER `is_customer`;
ALTER TABLE `member_type` ADD FOREIGN KEY (`inherits_from_type_id`) REFERENCES `member_type` (`id`);

UPDATE `member_type` SET `is_customer` = '1' WHERE `id` = '1';
UPDATE `member_type` SET `is_customer` = '1' WHERE `id` = '2';
UPDATE `member_type` SET `inherits_from_type_id` = '1' WHERE `id` = '2';

INSERT INTO `member_type` (`id`, `code`, `name`, `is_default`, `price_month`, `price_biannual`, `price_annual`, `promo_codes_allowed`, `is_customer`, `inherits_from_type_id`) VALUES (NULL, 'SALESADMIN', 'Sales Admin', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO `member_type` (`id`, `code`, `name`, `is_default`, `price_month`, `price_biannual`, `price_annual`, `promo_codes_allowed`, `is_customer`, `inherits_from_type_id`) VALUES (NULL, 'ODESK', 'ODesk', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO `member_type` (`id`, `code`, `name`, `is_default`, `price_month`, `price_biannual`, `price_annual`, `promo_codes_allowed`, `is_customer`, `inherits_from_type_id`) VALUES (NULL, 'SUPERADMIN', 'Super Admin', '0', '0', '0', '0', '0', '0', '3');
INSERT INTO `member_type` (`id`, `code`, `name`, `is_default`, `price_month`, `price_biannual`, `price_annual`, `promo_codes_allowed`, `is_customer`, `inherits_from_type_id`) VALUES (NULL, 'AFFILIATE', 'Affiliate', '0', '0', '0', '0', '0', '0', NULL);

UPDATE `member_type` SET `is_admin` = '1' WHERE `id` = '3';
UPDATE `member_type` SET `is_admin` = '1' WHERE `id` = '4';
UPDATE `member_type` SET `is_admin` = '1' WHERE `id` = '5';

ALTER TABLE `member_type` ADD `is_super_admin` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `is_admin`;
UPDATE `member_type` SET `is_super_admin` = '1', inherits_from_type_id = NULL WHERE `id` = '5';

