CREATE TABLE `brokerage_email_root` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `brokerage_id` int(11) unsigned NOT NULL,   
  `email` varchar(250) NOT NULL,
  `created_at` datetime,
  PRIMARY KEY (`id`),
  KEY(`brokerage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE brokerage_email_root ADD FOREIGN KEY (brokerage_id) REFERENCES brokerage (id);