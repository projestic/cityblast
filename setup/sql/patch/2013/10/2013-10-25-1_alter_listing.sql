ALTER TABLE `listing` ADD COLUMN `brokerage_id` int(11); 
ALTER TABLE `listing` ADD INDEX `brokerage_id` (`brokerage_id`);
ALTER TABLE brokerage_unknown_member ADD FOREIGN KEY (brokerage_id) REFERENCES brokerage (id);