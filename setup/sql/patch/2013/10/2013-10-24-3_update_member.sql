UPDATE member SET type_id = 3 WHERE id IN (SELECT DISTINCT member_id FROM administrator WHERE type = 'salesadmin');
UPDATE member SET type_id = 5 WHERE id IN (SELECT DISTINCT member_id FROM administrator WHERE type = 'superadmin');
