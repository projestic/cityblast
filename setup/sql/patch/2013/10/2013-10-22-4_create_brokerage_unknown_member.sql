CREATE TABLE `brokerage_unknown_member` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `brokerage_id` int(11) unsigned NOT NULL,   
  `member_id` int(11) unsigned NOT NULL,  
  `created_at` datetime,
  PRIMARY KEY (`id`),
  KEY(`brokerage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE brokerage_unknown_member ADD FOREIGN KEY (brokerage_id) REFERENCES brokerage (id);
ALTER TABLE brokerage_unknown_member ADD FOREIGN KEY (member_id) REFERENCES member (id);