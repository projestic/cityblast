UPDATE member SET type_id = 1 WHERE id IN (SELECT id FROM (SELECT member.id FROM member LEFT JOIN member_type ON member_type.id = type_id WHERE member_type.id IS NULL) AS tmp);
ALTER TABLE member CHANGE type_id type_id INT(11)  UNSIGNED  NOT NULL;
ALTER TABLE member ADD FOREIGN KEY (type_id) REFERENCES member_type (id);
