
# Store engagement score by week and by top 25 performing listings
CREATE TABLE `engag_top_by_week` (
  `id`         int unsigned NOT NULL AUTO_INCREMENT,
  `year`       smallint NOT NULL,
  `month`      tinyint NOT NULL,
  `week`       smallint NOT NULL,
  `clicks`     int NOT NULL DEFAULT 0,
  `likes`      int NOT NULL DEFAULT 0,
  `comments`   int NOT NULL DEFAULT 0,
  `shares`     int NOT NULL DEFAULT 0,
  `engagement_score` DECIMAL(8,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Engagement score. By week and by top 25 performing listings';
