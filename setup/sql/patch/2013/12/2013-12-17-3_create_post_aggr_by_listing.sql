# 2013dec17
# Story #176030449. Stats weekly cron

# --
# Aggregate posts by listing id and day
CREATE TABLE `post_aggr_by_listing` (
  `id`         int unsigned NOT NULL AUTO_INCREMENT,
  `date`       date NOT NULL,
  `listing_id` int unsigned NULL,
  `likes`      int NOT NULL DEFAULT 0,
  `comments`   int NOT NULL DEFAULT 0,
  `shares`     int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `listing_id` (`listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Posts. Aggr by listing and day';
