ALTER TABLE `network_app` ADD `username` VARCHAR(64)  NULL  DEFAULT NULL  AFTER `created_at`;
ALTER TABLE `network_app` ADD `user_id` VARCHAR(64)  NULL  DEFAULT NULL  AFTER `username`;
UPDATE network_app SET username = 'cityblastinfo', user_id = '400344503' WHERE network = 'TWITTER';