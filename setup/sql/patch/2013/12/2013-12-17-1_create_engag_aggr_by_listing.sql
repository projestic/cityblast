
# --
# Aggregate engagement score by listing and day
CREATE TABLE `engag_aggr_by_listing` (
  `id`         int unsigned NOT NULL AUTO_INCREMENT,
  `date`       date NOT NULL,
  `listing_id` int unsigned NULL,
  `clicks`     int NOT NULL DEFAULT 0,
  `likes`      int NOT NULL DEFAULT 0,
  `comments`   int NOT NULL DEFAULT 0,
  `shares`     int NOT NULL DEFAULT 0,
  `engagement_score` DECIMAL(8,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `listing_id` (`listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Engagement score. Aggr by listing and day';
