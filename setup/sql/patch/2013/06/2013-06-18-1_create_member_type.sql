  CREATE TABLE `member_type` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `code` char(16) NOT NULL,
    `name` varchar(127) NOT NULL,
    `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY `code_UNIQUE` (`code`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

