INSERT INTO member_status_log (member_id, payment_status, account_type, status, created_at)
	SELECT 
		m.id AS member_id,
		m.payment_status,
		m.account_type,
		m.status,
		NOW() AS created_at
	FROM member m;
