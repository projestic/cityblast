ALTER TABLE `member_payment_status_log_daily` 
CHANGE COLUMN `member_id` `member_id` INT(11) UNSIGNED NOT NULL  AFTER `id` , 
ADD COLUMN `status` ENUM('active', 'deleted') NOT NULL  AFTER `payment_status` , 
ADD COLUMN `account_type` ENUM('paid', 'free') NOT NULL  AFTER `status` , 
RENAME TO  `member_status_log_daily` ;
