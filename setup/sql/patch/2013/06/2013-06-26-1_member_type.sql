ALTER TABLE `member_type` 
ADD COLUMN `price_month` DECIMAL(8,2) UNSIGNED NOT NULL  AFTER `is_default` , 
ADD COLUMN `price_biannual` DECIMAL(8,2) UNSIGNED NOT NULL  AFTER `price_month` , 
ADD COLUMN `price_annual` DECIMAL(8,2) UNSIGNED NOT NULL  AFTER `price_biannual` ;

UPDATE `member_type` SET `price_month`='49.99', `price_biannual`='269.95', `price_annual`='479.90' WHERE `id`='1';
UPDATE `member_type` SET `price_month`='199.99', `price_biannual`='1079.95', `price_annual`='1919.90' WHERE `id`='2';

