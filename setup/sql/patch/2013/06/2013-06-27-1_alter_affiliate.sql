ALTER TABLE `affiliate` 
CHANGE COLUMN `member_id` `member_id` INT(10) UNSIGNED NOT NULL  , 
CHANGE COLUMN `first_payout` `first_payout` DECIMAL(8,2) NOT NULL  , 
CHANGE COLUMN `recurring_payout` `recurring_payout` DECIMAL(8,2) NOT NULL  , 
CHANGE COLUMN `price` `price_month` DECIMAL(8,2) NOT NULL  , 
ADD COLUMN `price_biannual` DECIMAL(8,2) NOT NULL  AFTER `price_month` , 
ADD COLUMN `price_annual` DECIMAL(8,2) NOT NULL  AFTER `price_biannual` ;