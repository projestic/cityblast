ALTER TABLE `member` 
ADD COLUMN `billing_cycle` ENUM('month', 'biannual', 'annual') NOT NULL DEFAULT 'month'  AFTER `account_type` ;
