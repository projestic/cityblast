ALTER TABLE `member_payment_status_log` 
ADD COLUMN `status` ENUM('active','deleted') NOT NULL  AFTER `payment_status` , 
ADD COLUMN `account_type` ENUM('paid','free') NOT NULL  AFTER `status` , 
RENAME TO  `member_status_log` ;
