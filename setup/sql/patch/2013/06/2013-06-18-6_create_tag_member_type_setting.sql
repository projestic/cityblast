CREATE  TABLE `tag_member_type_setting` (
  `tag_id` INT(11) UNSIGNED NOT NULL ,
  `member_type_id` TINYINT(3) UNSIGNED NOT NULL ,
  `available` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `default_value` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `visible` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `editable` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  UNIQUE INDEX `tag_id_member_type_id` (`tag_id` ASC, `member_type_id` ASC) );
