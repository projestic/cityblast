# Post stat counts should default to zero
ALTER TABLE `post` 
CHANGE COLUMN `facebook_likes` `facebook_likes` INT(7) NOT NULL DEFAULT 0  , 
CHANGE COLUMN `facebook_comments` `facebook_comments` INT(7) NOT NULL DEFAULT 0  , 
CHANGE COLUMN `facebook_shares` `facebook_shares` INT(7) NOT NULL DEFAULT 0  , 
CHANGE COLUMN `retweets` `retweets` INT(7) NOT NULL DEFAULT 0  ;
