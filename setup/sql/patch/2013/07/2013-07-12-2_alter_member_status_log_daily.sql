ALTER TABLE `member_status_log_daily` 
ADD COLUMN `price` DECIMAL(10,2) UNSIGNED NOT NULL  AFTER `account_type` , 
ADD COLUMN `billing_cycle` ENUM('month','biannual','annual') NOT NULL  AFTER `price` , 
ADD COLUMN `price_month` DECIMAL(10,2) UNSIGNED NOT NULL  AFTER `billing_cycle`;

UPDATE member_status_log_daily
SET 
	price = (SELECT price FROM member WHERE id = member_status_log_daily.member_id),
	billing_cycle = (SELECT billing_cycle FROM member WHERE id = member_status_log_daily.member_id),
	price_month = (SELECT price_month FROM member WHERE id = member_status_log_daily.member_id);