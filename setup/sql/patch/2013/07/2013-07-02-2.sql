ALTER TABLE `member_stats_daily` 
ADD COLUMN `nocc_trial_deleted` SMALLINT(5) UNSIGNED NOT NULL  AFTER `nocc_trial_cancel` , 
ADD COLUMN `cc_trial_deleted` SMALLINT(5) UNSIGNED NOT NULL  AFTER `cc_trial_cancel` , 
ADD COLUMN `paid_deleted` SMALLINT(5) UNSIGNED NOT NULL  AFTER `paid_cancel` , 
ADD COLUMN `free_new` SMALLINT(5) UNSIGNED NOT NULL  AFTER `active_cc` , 
ADD COLUMN `cancelled_new` SMALLINT(5) UNSIGNED NOT NULL  AFTER `total` , 
ADD COLUMN `cancelled_total` INT(11) UNSIGNED NOT NULL  AFTER `cancelled_new` , 
ADD COLUMN `deleted_new` SMALLINT(5) UNSIGNED NOT NULL  AFTER `cancelled_total` , 
ADD COLUMN `deleted_total` INT(11) UNSIGNED NOT NULL  AFTER `deleted_new` ;
