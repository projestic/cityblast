ALTER TABLE `member_type` ADD `promo_codes_allowed` TINYINT(3)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `price_annual`;
UPDATE member_type SET promo_codes_allowed = 1 WHERE id = 1;