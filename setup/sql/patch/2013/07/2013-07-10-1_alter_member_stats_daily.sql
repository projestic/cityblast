ALTER TABLE `member_stats_daily`
ADD COLUMN `nocc_trial_failed_convert` SMALLINT(5) UNSIGNED NOT NULL  AFTER `nocc_trial_new` ;

ALTER TABLE `member_stats_daily` 
ADD COLUMN `nocc_trial_failed_convert_total` INT(11) UNSIGNED NOT NULL  AFTER `nocc_trial_failed_convert` ;

