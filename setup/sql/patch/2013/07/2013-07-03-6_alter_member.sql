ALTER TABLE member DROP discount_code_id;
ALTER TABLE member DROP promotion_discount;

ALTER TABLE member DROP discount_code_id;
ALTER TABLE member DROP promotion_discount;
ALTER TABLE member DROP affiliate_first_payout;
ALTER TABLE member DROP affiliate_recurring_payout;

UPDATE member SET referring_member_id = NULL where referring_member_id = 0;
UPDATE member SET referring_member_id = NULL WHERE id IN (SELECT id FROM (SELECT member.id FROM member LEFT JOIN member AS referring_member ON member.referring_member_id = referring_member.id WHERE referring_member.id IS NULL AND member.referring_member_id IS NOT NULL) as tmptable);
UPDATE member SET referring_affiliate_id = NULL where referring_affiliate_id = 0;
UPDATE member SET referring_affiliate_id = NULL WHERE id IN (SELECT id FROM (SELECT member.id FROM member LEFT JOIN affiliate ON member.referring_affiliate_id = affiliate.id WHERE affiliate.id IS NULL AND member.referring_affiliate_id IS NOT NULL) as tmptable);

ALTER TABLE member CHANGE referring_affiliate_id referring_affiliate_id INT(11) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE member CHANGE referring_member_id referring_member_id INT(11) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE member ADD CONSTRAINT member_member_ibfk_1 FOREIGN KEY (referring_member_id) REFERENCES member (id);
ALTER TABLE member ADD CONSTRAINT member_affiliate_ibfk_1 FOREIGN KEY (referring_affiliate_id) REFERENCES affiliate (id);
