ALTER TABLE `member_status_log` 
ADD COLUMN `member_type_id` INT(11) UNSIGNED NOT NULL  AFTER `member_id` ;

UPDATE member_status_log 
SET member_type_id = (
	SELECT type_id 
	FROM member 
	WHERE member.id = member_status_log.member_id
);


ALTER TABLE `member_status_log_daily` 
ADD COLUMN `member_type_id` INT(11) UNSIGNED NOT NULL  AFTER `member_id` ;

UPDATE member_status_log_daily 
SET member_type_id = (
	SELECT type_id 
	FROM member 
	WHERE member.id = member_status_log_daily.member_id
);

ALTER TABLE `member_status_log_daily` 
DROP COLUMN `id` , 
CHANGE COLUMN `date` `date` DATE NOT NULL  FIRST 
, DROP INDEX `member_date`
, DROP PRIMARY KEY
, ADD PRIMARY KEY(`date`, `member_id`);