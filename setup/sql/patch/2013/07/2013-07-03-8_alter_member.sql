ALTER TABLE `member` ADD COLUMN `price_month` DECIMAL(10,2) UNSIGNED NULL DEFAULT NULL  AFTER `price` ;
UPDATE member SET price_month = price WHERE billing_cycle = 'month';
UPDATE member SET price_month = ROUND(price / 6, 2) WHERE billing_cycle = 'biannual';
UPDATE member SET price_month = ROUND(price / 12, 2) WHERE billing_cycle = 'annual';
