ALTER TABLE url_mapping ADD INDEX (listing_id);
ALTER TABLE url_mapping CHANGE listing_id listing_id INT(11)  UNSIGNED  NOT NULL;
ALTER TABLE url_mapping CHANGE id id INT(11)  UNSIGNED  NOT NULL  AUTO_INCREMENT;
DELETE FROM url_mapping WHERE id IN (SELECT id FROM (SELECT url_mapping.id FROM url_mapping LEFT JOIN listing ON listing.id = listing_id WHERE listing.id IS NULL) AS tmptable);
ALTER TABLE url_mapping ADD FOREIGN KEY (listing_id) REFERENCES listing (id) ON DELETE CASCADE;
