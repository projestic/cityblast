ALTER TABLE `affiliate` CHANGE `first_payout` `first_payout_percent` TINYINT(3) UNSIGNED NOT NULL;
ALTER TABLE `affiliate` CHANGE `recurring_payout` `recurring_payout_percent` TINYINT(3) UNSIGNED NOT NULL;

UPDATE affiliate SET first_payout_percent = (first_payout_percent/price_month) * 100 WHERE first_payout_percent > 0;
UPDATE affiliate SET recurring_payout_percent = (recurring_payout_percent/price_month) * 100 WHERE recurring_payout_percent > 0;

// check affiliate percentages

ALTER TABLE affiliate ADD FOREIGN KEY (member_id) REFERENCES member (id);
