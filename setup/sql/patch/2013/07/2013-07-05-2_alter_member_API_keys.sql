alter table member add column api_key varchar(10);
ALTER TABLE `member` ADD INDEX ( `api_key` );
alter table member add column api_secret varchar(30);
ALTER TABLE `member` ADD INDEX ( `api_secret` ); 
alter table member add column api_domain varchar(256);
ALTER TABLE `member` ADD INDEX ( `api_domain` ); 