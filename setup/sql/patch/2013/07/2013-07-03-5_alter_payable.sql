ALTER TABLE payable ADD member_payment_id INT UNSIGNED NULL DEFAULT NULL AFTER payment_date;

UPDATE payable SET member_payment_id = (SELECT payment.id FROM payment WHERE payable.payment_date = DATE(payment.created_at) and payable.member_id = payment.member_id);

ALTER TABLE payable DROP payment_date;
ALTER TABLE payable DROP amount_old;
ALTER TABLE payable ADD CONSTRAINT payable_ibfk_1 FOREIGN KEY (member_payment_id) REFERENCES payment (id);
ALTER TABLE payable ADD CONSTRAINT payable_ibfk_2 FOREIGN KEY (member_id) REFERENCES member (id);
ALTER TABLE payable ADD CONSTRAINT payable_ibfk_3 FOREIGN KEY (affiliate_id) REFERENCES affiliate (id);
