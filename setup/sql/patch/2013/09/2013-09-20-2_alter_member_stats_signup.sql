ALTER TABLE `member_stats_signup` 
ADD COLUMN `ad_spend` DECIMAL(10,2) UNSIGNED NULL DEFAULT 0  AFTER `revenue_actual` , 
ADD COLUMN `profit` DECIMAL(10,2) NULL DEFAULT 0  AFTER `ad_spend` ;
