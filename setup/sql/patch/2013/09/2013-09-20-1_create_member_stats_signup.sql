CREATE TABLE `member_stats_signup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `total` int(11) unsigned NOT NULL,
  `cc` int(11) unsigned NOT NULL,
  `nocc` int(11) unsigned NOT NULL,
  `cc_paid` int(11) unsigned DEFAULT NULL,
  `cc_cancel` int(11) unsigned DEFAULT NULL,
  `cc_failed_payment` int(11) unsigned DEFAULT NULL,
  `nocc_paid` int(11) unsigned DEFAULT NULL,
  `nocc_cancel` int(11) unsigned DEFAULT NULL,
  `nocc_failed_payment` int(11) unsigned DEFAULT NULL,
  `revenue_projected` decimal(10,2) unsigned NOT NULL,
  `revenue_actual` decimal(10,2) unsigned DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `date_UNIQUE` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;