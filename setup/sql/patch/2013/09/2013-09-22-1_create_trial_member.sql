CREATE TABLE `trial_member` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(265) NOT NULL,
  `cid` varchar(150) NOT NULL,
  `created_at` datetime,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;