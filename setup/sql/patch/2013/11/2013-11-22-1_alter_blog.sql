# Story 175348444. SimplePie content reader upgrades
#

# Capture the date that the article was published on. Rename column.
ALTER TABLE `blog_pending` 
  CHANGE COLUMN `published_on` `article_date` DATETIME NULL AFTER `blog_id`;

# Whenever to seek images inside external article by following the feed''s link
ALTER TABLE `blog` 
  ADD COLUMN `seek_images_in_external_article` tinyint(1) NOT NULL DEFAULT 0 AFTER `url`;