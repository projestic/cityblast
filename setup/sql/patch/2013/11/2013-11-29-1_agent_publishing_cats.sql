# Story 175977761. BUG: Head office news not showing up
#

# Let tag Head Office News to show up for agents (members of types Realtor and Broker)
# type_id 1 - Realtor
# type_id 2 - Broker
# tag 17 - Head Office News
UPDATE tag_member_type_setting SET available=1 WHERE tag_id=17 AND member_type_id IN (1, 2);