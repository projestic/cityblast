ALTER TABLE `member_stats_signup` 
ADD COLUMN `cc_avconv_rate` DECIMAL(3,2) UNSIGNED NOT NULL  AFTER `nocc_failed_payment` , 
ADD COLUMN `nocc_avconv_rate` DECIMAL(3,2) UNSIGNED NOT NULL  AFTER `cc_avconv_rate` , 
ADD COLUMN `cc_revenue_projected_max` DECIMAL(10,2) UNSIGNED NOT NULL  AFTER `nocc_avconv_rate` , 
ADD COLUMN `nocc_revenue_projected_max` DECIMAL(10,2) UNSIGNED NOT NULL  AFTER `cc_revenue_projected_max` ,
ADD COLUMN `revenue_projected_max` DECIMAL(10,2) UNSIGNED NOT NULL  AFTER `nocc_revenue_projected_max` ,
ADD COLUMN `cc_revenue_projected` DECIMAL(10,2) UNSIGNED NOT NULL  AFTER `nocc_revenue_projected_max` , 
ADD COLUMN `nocc_revenue_projected` DECIMAL(10,2) UNSIGNED NOT NULL  AFTER `cc_revenue_projected` ;
