# Story 175749146. More additions to: /externalcontent/index/
#

# need a way of prioritizing blogs and the articles associated with them
ALTER TABLE `blog`
  ADD COLUMN `tier` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'priority tier' AFTER `url`;