ALTER TABLE `group_city` 
CHANGE COLUMN `city_id` `city_id` INT(11) UNSIGNED NOT NULL;

DELETE FROM group_city WHERE id IN (
	SELECT id FROM (
		SELECT group_city.id FROM group_city
		LEFT JOIN city ON city.id = group_city.city_id
		WHERE city.id IS NULL
	) AS id
);

ALTER TABLE `group_city` 
ADD CONSTRAINT `city_id`
  FOREIGN KEY (`city_id`)
  REFERENCES `city` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

