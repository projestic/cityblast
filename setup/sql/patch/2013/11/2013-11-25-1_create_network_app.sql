CREATE TABLE `network_app` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `network` enum('FACEBOOK','TWITTER','LINKEDIN') NOT NULL,
  `app_name` varchar(64) NOT NULL,
  `consumer_id` varchar(64) NOT NULL,
  `consumer_secret` varchar(64) NOT NULL,
  `notes` text,
  `registration_enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
