INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`) VALUES (1,'FACEBOOK','City-Blast.com','199255106788609','562defa1c6bfddd0f58e40357beda2fa','Old city-blast.com domain',0,'2013-11-29 02:40:34');
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`) VALUES (2,'FACEBOOK','CityBlast.com','517988398241860','412ad344c01d2651b441d978e1cb3d41','cityblast.com domain',1,'2013-11-29 02:41:25');
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`) VALUES (3,'TWITTER','City-Blast.com','zfo6ZREOJYgapcxFCMivKQ','0MEbUWn32H9D5fgLznLxSAmnrSBDDE0qKO1grRg','Old city-blast.com domain',0,'2013-11-29 02:42:31');
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`) VALUES (4,'TWITTER','CityBlast.com','rAQayp4M7Ek64zHuY0TxkQ','qJD1uvwOsLtp6mCzemAgeywVTVfGy0P5NzWgCKaAI','cityblast.com domain',1,'2013-11-29 02:43:11');
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`) VALUES (5,'LINKEDIN','City-Blast.com','1h73iurszk9t','9Eq71NGLIwMVJKvo','Old city-blast.com domain',0,'2013-11-29 02:44:10');
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`) VALUES (6,'LINKEDIN','Cityblast.com','rav2pd8c9jpy','N5mZrHlhBdOMVRKE','cityblast.com domain',1,'2013-11-29 02:44:38');

UPDATE member SET application_id = 1 WHERE application_id = 0;
UPDATE member SET application_id = 2 WHERE application_id = 1;
UPDATE member SET tw_application_id = 3 WHERE tw_application_id = 0;
UPDATE member SET tw_application_id = 4 WHERE tw_application_id = 1;
UPDATE member SET ln_application_id = 5 WHERE ln_application_id = 0;
UPDATE member SET ln_application_id = 6 WHERE ln_application_id = 1;