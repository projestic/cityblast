# 2013nov09
# Story #174826456. SimplePie content reader

# --
# Blog
CREATE TABLE `blog` (
  `id`         int unsigned NOT NULL AUTO_INCREMENT,
  `name`       varchar(250) NOT NULL COMMENT 'Blog name if any. Ex: cnn.com',
  `url`        varchar(300) NOT NULL COMMENT 'Blog feed URL. Ex: cnn.com',
  `created_at` datetime,
  PRIMARY KEY    (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Blogs. Source of feeds';

# --
# Blog entries
CREATE TABLE `blog_pending` (
  `id`         int unsigned NOT NULL AUTO_INCREMENT,
  `blog_id`    int unsigned NOT NULL COMMENT 'Ref to blog table',
  #`member_id` int(11) NOT NULL,
  #`published` int(3) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `description` text,
  `message` mediumtext,
  `raw_message` mediumtext,
  #`pre_approval` tinyint(1) NOT NULL DEFAULT '0',
  #`approval` char(1) DEFAULT '0',
  #`approved_by_id` int(11) DEFAULT NULL,
  #`approved_at` datetime DEFAULT NULL,
  #`street` varchar(200) DEFAULT NULL,
  #`price` varchar(50) DEFAULT NULL,
  #`old_price` varchar(50) NOT NULL,
  #`city` varchar(50) DEFAULT NULL,
  #`postal_code` varchar(15) DEFAULT NULL,
  #`appartment` varchar(25) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  #`priority` int(11) NOT NULL,
  `content_link` varchar(256) DEFAULT NULL,
  #`blast_type` enum('LISTING','CONTENT','IDX') DEFAULT NULL,
  #`latitude` varchar(25) DEFAULT NULL,
  #`longitude` varchar(25) DEFAULT NULL,
  #`mls` varchar(30) DEFAULT NULL,
  #`publish` char(1) DEFAULT '0',
  `published_on` datetime DEFAULT NULL,
  #`status` enum('active','deleted') NOT NULL DEFAULT 'active',
  #`city_id` int(11) DEFAULT NULL,
  #`number_of_bedrooms` int(1) DEFAULT NULL,
  #`number_of_extra_bedrooms` int(1) DEFAULT NULL,
  #`number_of_bathrooms` int(1) DEFAULT NULL,
  #`number_of_parking` int(1) DEFAULT NULL,
  #`thumbnail` varchar(265) DEFAULT NULL,
  #`alt_image_2` varchar(265) DEFAULT NULL,
  #`alt_image_3` varchar(265) DEFAULT NULL,
  #`alt_image_4` varchar(265) DEFAULT NULL,
  #`maintenance_fee` float(10,2) DEFAULT NULL,
  #`property_type_id` int(5) DEFAULT NULL,
  #`taxes` float(10,2) DEFAULT NULL,
  #`list_agent` varchar(150) DEFAULT NULL,
  #`list_agent_phone` varchar(25) DEFAULT NULL,
  #`dupliate_type` enum('INTERNATIONAL','NATIONAL','NONE') DEFAULT NULL,
  #`broker_of_record` varchar(250) DEFAULT NULL,
  #`broker_address` varchar(256) NOT NULL,
  #`area` varchar(250) DEFAULT NULL,
  #`community` varchar(250) DEFAULT NULL,
  #`expiry_date` datetime DEFAULT NULL,
  #`contacted` enum('yes','no') NOT NULL DEFAULT 'no',
  #`property_status` enum('NEW','SOLD','EXPIRED','PRICE_CHANGE') DEFAULT NULL,
  #`square_footage` varchar(50) DEFAULT NULL,
  #`reblast_id` int(11) unsigned DEFAULT NULL,
  #`classification_delete_me_r207` enum('LIFESTYLE','INFORMATIONAL') DEFAULT NULL,
  #`big_image` int(1) NOT NULL DEFAULT '0',
  #`email_passthru` int(1) NOT NULL DEFAULT '0',
  #`franchise_id` int(11) DEFAULT NULL,
  PRIMARY KEY    (`id`),
  KEY `listing_id` (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Blog entries. Awaiting for approval';

# --
# Blog description
CREATE TABLE `blog_description_pending` (
  `id`            int unsigned NOT NULL AUTO_INCREMENT,
  `blog_entry_id` int unsigned NOT NULL COMMENT 'Ref to blog_pending table',
  `description`   text NOT NULL,
  PRIMARY KEY    (`id`),
  KEY `listing_id` (`blog_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Blog description. Awaiting for approval';

# --
# Blog images
CREATE TABLE `blog_image_pending` (
  `id`         int unsigned NOT NULL AUTO_INCREMENT,
  `blog_entry_id` int(11) unsigned NOT NULL COMMENT 'Ref to blog_pending table',
  `image` varchar(255) DEFAULT '',
  PRIMARY KEY    (`id`),
  KEY `listing_id` (`blog_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Blog images. Awaiting for approval';