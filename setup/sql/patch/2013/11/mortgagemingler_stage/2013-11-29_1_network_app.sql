INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`) VALUES (1,'FACEBOOK','MortgageMingler Stage','338464186274504','2b3b973cbaea5e4f93dd70db0bc41eee','',1,'2013-11-29 02:40:34');
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`) VALUES (2,'TWITTER','MortgageMingler Stage','ofZ7oBweC1JyKPMUQGyolA','K1VumVCARWUClzmYefMlvoFpI7UfZKcS2208bwB3ZEc','',1,'2013-11-29 02:42:31');
INSERT INTO `network_app` (`id`,`network`,`app_name`,`consumer_id`,`consumer_secret`,`notes`,`registration_enabled`,`created_at`) VALUES (3,'LINKEDIN','MortgageMingler Stage','k2j96wl0izun','h986Tir17r7XqWWG','',1,'2013-11-29 02:44:10');

UPDATE member SET application_id = 1 WHERE application_id IS NOT NULL;
UPDATE member SET tw_application_id = 2 WHERE tw_application_id IS NOT NULL;
UPDATE member SET ln_application_id = 3 WHERE ln_application_id IS NOT NULL;