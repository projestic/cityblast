ALTER table `listing` CHANGE `blast_type` `blast_type` ENUM('BLAST','MANUAL','SMLS','LISTING','CONTENT','IDX');
UPDATE `listing` SET `blast_type` = 'LISTING' WHERE `blast_type` = 'BLAST';
UPDATE `listing` SET `blast_type` = 'CONTENT' WHERE `blast_type` = 'MANUAL';
UPDATE `listing` SET `blast_type` = 'IDX' WHERE `blast_type` = 'SMLS';
UPDATE `listing` SET `blast_type` = NULL where `blast_type` = '';
ALTER table `listing` CHANGE `blast_type` `blast_type` ENUM('LISTING','CONTENT','IDX') DEFAULT NULL;

