ALTER TABLE `payment` 
DROP FOREIGN KEY `payment_payment_gateway_ibfk_1`;
ALTER TABLE `payment` 
CHANGE COLUMN `payment_gateway_id` `payment_gateway_id` INT(11) UNSIGNED NOT NULL DEFAULT 0 ;
ALTER TABLE `payment` 
ADD CONSTRAINT `payment_payment_gateway_ibfk_1`
  FOREIGN KEY (`payment_gateway_id`)
  REFERENCES `payment_gateway` (`id`);
