ALTER TABLE `blog_imported` 
CHANGE COLUMN `description_sha1` `description_sha1` CHAR(40) NOT NULL FIRST,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`description_sha1`, `blog_id`);
