CREATE TABLE `blog_imported` (
  `blog_id` int(10) unsigned NOT NULL,
  `description_sha1` char(40) NOT NULL,
  `article_date` datetime DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `content_link` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`blog_id`,`description_sha1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

REPLACE LOW_PRIORITY 
    INTO blog_imported (blog_id, description_sha1, article_date, title, content_link)
    SELECT blog_id, SHA1(description), article_date, title, content_link FROM blog_pending;
