INSERT INTO `payment_gateway` (`id`,`code`,`name`,`local_processing`,`is_default`) VALUES (0,'INTERNAL','Internal',0,0);
UPDATE payment_gateway SET id = 0 WHERE id = LAST_INSERT_ID();
ALTER TABLE `payment_gateway` AUTO_INCREMENT = 4;