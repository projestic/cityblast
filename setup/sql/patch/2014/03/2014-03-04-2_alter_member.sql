ALTER TABLE `member` 
CHANGE COLUMN `payment_gateway` `payment_gateway` ENUM('PAYPAL','STRIPES','CLAREITY') NOT NULL,
DROP COLUMN `payment_via`;
