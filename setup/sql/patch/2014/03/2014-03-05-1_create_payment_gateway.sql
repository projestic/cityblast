CREATE TABLE `payment_gateway` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(127) DEFAULT NULL,
  `local_processing` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


INSERT INTO `payment_gateway` (`id`,`code`,`name`,`local_processing`,`is_default`) VALUES (1,'PAYPAL','PayPal',1,0);
INSERT INTO `payment_gateway` (`id`,`code`,`name`,`local_processing`,`is_default`) VALUES (2,'STRIPE','Stripe',1,1);
INSERT INTO `payment_gateway` (`id`,`code`,`name`,`local_processing`,`is_default`) VALUES (3,'CLAREITY','Clareity',0,0);

ALTER TABLE `member` 
ADD COLUMN `payment_gateway_id` INT(11) UNSIGNED DEFAULT NULL AFTER `payment_gateway`,
ADD INDEX `payment_gateway_id` (`payment_gateway_id` ASC);

UPDATE member SET payment_gateway_id = 1 WHERE payment_gateway IN ('PAYPAL');
UPDATE member SET payment_gateway_id = 2 WHERE payment_gateway IN ('STRIPE', 'STRIPES', '', NULL);
UPDATE member SET payment_gateway_id = 3 WHERE payment_gateway IN ('CLAREITY');

ALTER TABLE `member` 
ADD CONSTRAINT `member_payment_gateway_ibfk_1`
  FOREIGN KEY (`payment_gateway_id`)
  REFERENCES `payment_gateway` (`id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `member` DROP COLUMN `payment_gateway`;
  
  
ALTER TABLE `payment` 
ADD COLUMN `payment_gateway_id` INT(11) UNSIGNED NOT NULL AFTER `payment_type`,
ADD INDEX `payment_gateway_id` (`payment_gateway_id` ASC);

UPDATE payment SET payment_gateway_id = 1 WHERE payment_gateway IN ('PAYPAL');
UPDATE payment SET payment_gateway_id = 2 WHERE payment_gateway IN ('STRIPE', 'STRIPES', '', NULL);
UPDATE payment SET payment_gateway_id = 3 WHERE payment_gateway IN ('CLAREITY');

ALTER TABLE `payment` 
ADD CONSTRAINT `payment_payment_gateway_ibfk_1`
  FOREIGN KEY (`payment_gateway_id`)
  REFERENCES `payment_gateway` (`id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `payment` DROP COLUMN `payment_gateway`;



