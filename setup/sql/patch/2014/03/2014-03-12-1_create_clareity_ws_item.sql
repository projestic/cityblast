CREATE TABLE `clareity_ws_item` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `store` VARCHAR(127) NOT NULL,
  `name` VARCHAR(127) NOT NULL,
  `member_type_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `store_name` (`store` ASC, `name` ASC),
  INDEX `member_type_id` (`member_type_id` ASC),
  CONSTRAINT `member_type_id`
    FOREIGN KEY (`member_type_id`)
    REFERENCES `member_type` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);
    
INSERT INTO `clareity_ws_item` (`store`, `name`, `member_type_id`) VALUES ('clareity-qa', 'cityblast', '1');

