CREATE TABLE `text_fix_history` (
  `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(127) NOT NULL,
  `row_id` int(11) unsigned NOT NULL,
  `field_name` varchar(127) NOT NULL,
  `original` mediumtext NOT NULL,
  `updated` mediumtext NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_at` varchar(127) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `field` (`table_name`,`row_id`,`field_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
