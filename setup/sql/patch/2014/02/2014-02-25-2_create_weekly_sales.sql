CREATE TABLE `weekly_sales` (
  `year` mediumint(9) unsigned NOT NULL,
  `week_number` tinyint unsigned NOT NULL,
  `member_id` int(11) unsigned NOT NULL,
  `volume` decimal(10,2) unsigned NOT NULL DEFAULT '0.000',
  `commission` decimal(10,2) unsigned NOT NULL DEFAULT '0.000',
  `number_of_sales` mediumint(9) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`year`, `week_number`, `member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;