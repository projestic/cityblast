CREATE TABLE `clareity_ws_provision_trans` (
  `transaction_id` varchar(80) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `store_name` varchar(127) DEFAULT NULL,
  `item_name` varchar(127) DEFAULT NULL,
  `item_quantity` int(11) DEFAULT NULL,
  `user_name` varchar(127) DEFAULT NULL,
  `nrds_id` varchar(127) DEFAULT NULL,
  `association_id` varchar(127) DEFAULT NULL,
  `office_id` varchar(127) DEFAULT NULL,
  `broker_id` varchar(127) DEFAULT NULL,
  `email` varchar(127) DEFAULT NULL,
  `first_name` varchar(127) DEFAULT NULL,
  `last_name` varchar(127) DEFAULT NULL,
  `mobile_phone` varchar(127) DEFAULT NULL,
  `adrs_ship_street` varchar(127) DEFAULT NULL,
  `adrs_ship_city` varchar(127) DEFAULT NULL,
  `adrs_ship_state` varchar(127) DEFAULT NULL,
  `adrs_ship_zip` varchar(127) DEFAULT NULL,
  `adrs_ship_country` varchar(127) DEFAULT NULL,
  `adrs_bill_street` varchar(127) DEFAULT NULL,
  `adrs_bill_city` varchar(127) DEFAULT NULL,
  `adrs_bill_state` varchar(127) DEFAULT NULL,
  `adrs_bill_zip` varchar(127) DEFAULT NULL,
  `adrs_bill_country` varchar(127) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `member_id` (`member_id`),
  KEY `store_name` (`store_name`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
