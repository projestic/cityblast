CREATE TABLE `engage_by_month_member` (
  `date` date NOT NULL,
  `member_id` int(11) unsigned NOT NULL,
  `year` mediumint(9) unsigned NOT NULL,
  `month` smallint(6) unsigned NOT NULL,
  `clicks` int(11) unsigned NOT NULL DEFAULT '0',
  `likes` int(11) unsigned NOT NULL DEFAULT '0',
  `comments` int(11) unsigned NOT NULL DEFAULT '0',
  `shares` int(11) unsigned NOT NULL DEFAULT '0',
  `score` int(11) unsigned NOT NULL DEFAULT '0',
  `posts` int(11) unsigned NOT NULL DEFAULT '0',
  `clicks_perpost` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  `likes_perpost` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  `comments_perpost` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  `shares_perpost` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  `score_perpost` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`date`,`member_id`),
  KEY `member_id` (`member_id`),
  KEY `date_score` (`date`,`score`),
  KEY `date_score_per_post` (`date`,`score_perpost`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;