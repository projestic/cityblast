CREATE TABLE `clareity_ws_apikey` (
  `apikey` CHAR(40) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NULL DEFAULT 1,
  `notes` TEXT NULL,
  PRIMARY KEY (`apikey`));
