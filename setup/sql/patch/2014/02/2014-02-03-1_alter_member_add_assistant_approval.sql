ALTER TABLE listing ADD assistant_approval TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER pre_approval;
ALTER TABLE listing CHANGE pre_approval pre_approval TINYINT(1) UNSIGNED  NULL DEFAULT NULL;
