CREATE TABLE `listing_top_by_week` (
  `date` date NOT NULL,
  `listing_id` int(11) unsigned NOT NULL,
  `year` mediumint(9) unsigned DEFAULT NULL,
  `week` smallint(6) unsigned DEFAULT NULL,
  `score_perpost` decimal(10,3) unsigned DEFAULT NULL,
  PRIMARY KEY (`date`,`listing_id`),
  KEY `year_month` (`year`,`week`),
  KEY `score_per_post` (`score_perpost`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

