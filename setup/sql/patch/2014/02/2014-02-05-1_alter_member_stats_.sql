ALTER TABLE `member_stats_daily` 
ADD COLUMN `avrg_lifetime_value` DECIMAL(12,2) UNSIGNED NOT NULL AFTER `deleted_total`;

ALTER TABLE `member_stats_monthly` 
ADD COLUMN `avrg_lifetime_value` DECIMAL(12,2) UNSIGNED NOT NULL AFTER `deleted_total`;
