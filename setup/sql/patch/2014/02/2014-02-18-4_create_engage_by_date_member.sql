CREATE TABLE `facebook_stat_by_date_member` (
  `date` date NOT NULL,
  `member_id` int(11) unsigned NOT NULL,
  `likes` int(11) unsigned NOT NULL DEFAULT '0',
  `comments` int(11) unsigned NOT NULL DEFAULT '0',
  `shares` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`date`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
