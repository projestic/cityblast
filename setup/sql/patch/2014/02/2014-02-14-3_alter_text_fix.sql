ALTER TABLE `text_fix` 
DROP COLUMN `auto2`,
ADD COLUMN `listing_id` INT(11) UNSIGNED NOT NULL AFTER `field_name`,
ADD INDEX `listing_id` (`listing_id` ASC);

ALTER TABLE `text_fix_history` 
ADD COLUMN `listing_id` INT(11) UNSIGNED NOT NULL AFTER `field_name`,
ADD INDEX `listing_id` (`listing_id` ASC);

