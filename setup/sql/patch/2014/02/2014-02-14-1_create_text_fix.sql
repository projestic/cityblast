CREATE TABLE `text_fix` (
  `table_name` varchar(127) NOT NULL,
  `row_id` int(11) unsigned NOT NULL,
  `field_name` varchar(127) NOT NULL,
  `fixed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `original` mediumtext,
  `auto1` mediumtext,
  `auto2` mediumtext,
  `manual` mediumtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`table_name`,`row_id`,`field_name`),
  KEY `fixed` (`fixed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
