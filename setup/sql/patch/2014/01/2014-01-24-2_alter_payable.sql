ALTER TABLE `payable` 
ADD COLUMN `override_id` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `affiliate_id`;
ALTER TABLE payable ADD FOREIGN KEY (override_id) REFERENCES payment (id);
