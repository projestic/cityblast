ALTER TABLE `member` ADD `auto_like` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1' AFTER `testimonial`;
UPDATE member SET auto_like = 0;