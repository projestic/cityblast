ALTER TABLE `member_stats_daily` 
ADD COLUMN `cc_trial_alltime` INT(11) UNSIGNED NOT NULL AFTER `cc_trial_converted`,
ADD COLUMN `paid_alltime` INT(11) UNSIGNED NOT NULL AFTER `paid_total`;

ALTER TABLE `member_stats_monthly` 
ADD COLUMN `cc_trial_alltime` INT(11) UNSIGNED NOT NULL AFTER `cc_trial_converted`,
ADD COLUMN `paid_alltime` INT(11) UNSIGNED NOT NULL AFTER `paid_total`;




