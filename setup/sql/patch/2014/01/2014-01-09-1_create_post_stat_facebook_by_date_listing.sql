CREATE TABLE `post_stat_facebook_by_date_listing` (
  `date` date NOT NULL,
  `listing_id` int(11) unsigned NOT NULL,
  `likes` int(11) unsigned NOT NULL DEFAULT '0',
  `comments` int(11) unsigned NOT NULL DEFAULT '0',
  `shares` int(11) unsigned NOT NULL DEFAULT '0',
  `posts` int(11) unsigned NOT NULL DEFAULT '0',
  `likes_perpost` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  `comments_perpost` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  `shares_perpost` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`date`,`listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
