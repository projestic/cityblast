UPDATE franchise SET is_enabled = 0 WHERE active = 'NO';
UPDATE franchise SET is_enabled = 1 WHERE active = 'YES';
ALTER TABLE `franchise` DROP COLUMN `active`;
