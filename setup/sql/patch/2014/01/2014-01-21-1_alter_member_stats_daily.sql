ALTER TABLE `member_stats_daily` 
DROP COLUMN `id`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`date`),
DROP INDEX `date`;
