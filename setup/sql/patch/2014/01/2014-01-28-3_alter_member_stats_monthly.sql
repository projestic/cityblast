ALTER TABLE `member_stats_monthly` 
ADD COLUMN `signup_new` INT(11) UNSIGNED NOT NULL AFTER `free`,
ADD COLUMN `signup_total` INT(11) UNSIGNED NOT NULL AFTER `signup_new`;
