DROP TABLE IF EXISTS
    `click_stats_aggregated_by_date`, 
    `click_stats_aggregated_by_listing`,
    `click_stats_aggregated_by_listing_member`,
    `click_stats_aggregated_by_member`,
    `post_aggr_by_listing`,
    `engag_top_by_week`,
    `engag_aggr_by_listing`;
    
