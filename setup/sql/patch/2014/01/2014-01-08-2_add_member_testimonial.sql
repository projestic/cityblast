ALTER TABLE `member` ADD `testimonial` TEXT  NULL  AFTER `franchise_id`;
UPDATE member JOIN affiliate ON affiliate.member_id = member.id SET member.testimonial = affiliate.testimonial;

