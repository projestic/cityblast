ALTER TABLE `affiliate` 
ADD COLUMN `override_affiliate_id` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `member_id`,
ADD COLUMN `override_payout_percent` TINYINT(3) UNSIGNED NOT NULL AFTER `recurring_payout_percent`;
ALTER TABLE affiliate ADD FOREIGN KEY (override_affiliate_id) REFERENCES affiliate (id);
