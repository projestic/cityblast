CREATE TABLE `post_stat_facebook_by_date` (
  `post_id` INT UNSIGNED NOT NULL,
  `date` DATE NOT NULL,
  `likes` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  `likes_total` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  `comments` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  `comments_total` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  `shares` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  `shares_total` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`post_id`, `date`),
  INDEX `date` (`date` ASC));
  
INSERT INTO post_stat_facebook_by_date (
	post_id, 
	`date`, 
	likes, 
	likes_total,
	comments,
	comments_total,
	shares,
	shares_total
) SELECT 
	id, 
	DATE(NOW()),
	facebook_likes,
	facebook_likes as likes_total,
	facebook_comments,
	facebook_comments as comments_total,
	facebook_shares,
	facebook_shares as shares_total
FROM post
WHERE 
	`type` = 'FACEBOOK' AND
	result = 1 AND
	(facebook_likes > 0 OR facebook_comments > 0 OR facebook_shares > 0);
