CREATE TABLE `click_stat_by_date_member` (
  `date` date NOT NULL,
  `member_id` int(11) unsigned NOT NULL,
  `count` int(11) unsigned NOT NULL,
  PRIMARY KEY (`date`,`member_id`),
  KEY `count` (`count`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
