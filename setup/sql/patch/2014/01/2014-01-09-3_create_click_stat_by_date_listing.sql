CREATE TABLE `click_stat_by_date_listing` (
  `date` date NOT NULL,
  `listing_id` int(11) unsigned NOT NULL,
  `count` int(11) unsigned NOT NULL,
  PRIMARY KEY (`date`,`listing_id`),
  KEY `count` (`count`),
  KEY `listing_id` (`listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
