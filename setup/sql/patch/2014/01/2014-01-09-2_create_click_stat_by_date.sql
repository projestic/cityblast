CREATE TABLE `click_stat_by_date` (
  `date` date NOT NULL,
  `count` int(11) unsigned NOT NULL,
  PRIMARY KEY (`date`),
  KEY `dateindex` (`date`),
  KEY `count` (`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
