CREATE TABLE `facebook_comment` (
  `id` varchar(128) NOT NULL,
  `post_id` int(11) NOT NULL,
  `from_uid` varchar(30) NOT NULL,
  `from_name` varchar(127) NOT NULL,
  `message` mediumtext NOT NULL,
  `likes` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
