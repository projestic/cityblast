CREATE TABLE `click_stat_by_member_listing` (
  `member_id` int(11) unsigned NOT NULL,
  `listing_id` int(11) unsigned NOT NULL,
  `count` int(11) unsigned NOT NULL,
  PRIMARY KEY (`member_id`,`listing_id`),
  KEY `count` (`count`),
  KEY `listing_id` (`listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
