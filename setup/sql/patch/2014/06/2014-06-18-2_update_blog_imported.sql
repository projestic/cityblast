
UPDATE IGNORE blog_imported SET blog_id = (  SELECT blog.id FROM blog JOIN (SELECT * FROM blog AS blog2 GROUP BY url HAVING count(id) > 1 ) dupe ON blog.url = dupe.url WHERE dupe.id = blog.id  AND blog_imported.blog_id = blog.id) WHERE blog_id IN (SELECT dupe1.id FROM (SELECT blog1.* FROM blog AS blog1 JOIN (SELECT * FROM blog AS blog1 GROUP BY url HAVING count(id) > 1) dupe ON blog1.url = dupe.url WHERE dupe.id < blog1.id) dupe1);

DELETE FROM blog_imported WHERE description_sha1 IN (SELECT description_sha1 FROM (SELECT * FROM blog_imported LEFT JOIN blog ON blog_id = blog.id WHERE blog.id IS NULL) AS tmp);

ALTER TABLE `blog_imported` ADD CONSTRAINT `blog_imported_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`) ON DELETE CASCADE;
