-- Add unique constraint to avoid adding duplicate entries
-- Column changed to latin1 to handle long indexes
ALTER TABLE `blog`
MODIFY COLUMN `url`  varchar(300) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL COMMENT 'Blog feed URL. Ex: cnn.com' AFTER `name`,
ADD UNIQUE INDEX `name_idx` (`name`) ,
ADD UNIQUE INDEX `url_idx` (`url`) ;
