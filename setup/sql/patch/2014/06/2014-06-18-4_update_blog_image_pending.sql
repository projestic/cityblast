DELETE FROM blog_image_pending WHERE id IN (SELECT id FROM (SELECT blog_image_pending.id FROM blog_image_pending LEFT JOIN blog_pending ON blog_entry_id = blog_pending.id WHERE blog_pending.id IS NULL) AS tmp);
ALTER TABLE `blog_image_pending` ADD CONSTRAINT `blog_image_pending_blog_entry_id` FOREIGN KEY (`blog_entry_id`) REFERENCES `blog_pending` (`id`) ON DELETE CASCADE;
