DELETE FROM blog_pending WHERE id IN (SELECT id FROM (SELECT blog_pending.id FROM blog_pending LEFT JOIN blog ON blog_id = blog.id WHERE blog.id IS NULL) AS tmp);
ALTER TABLE `blog_pending` ADD CONSTRAINT `blog_pending_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`) ON DELETE CASCADE;
