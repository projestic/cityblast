-- Add possibility to add listing without worrying about the old price field
alter table listing change old_price old_price varchar(255) not null default 0;
alter table listing change priority priority varchar(255) not null default 0;
alter table listing change broker_address broker_address varchar(255) not null default '';