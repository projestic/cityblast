DELETE FROM blog WHERE id IN (SELECT dupe1.id FROM (SELECT blog.* FROM blog JOIN (SELECT * FROM blog GROUP BY url HAVING count(id) > 1) dupe ON blog.url = dupe.url WHERE dupe.id < blog.id) dupe1);
