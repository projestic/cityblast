-- Change column type to boolean to flag approval status
ALTER TABLE `testimonial` MODIFY COLUMN `approved` tinyint(1) NULL AFTER `image`;

-- Set all testimonials to null to set all testimonials to no actioned
UPDATE testimonial SET approved = null WHERE approved = 0;