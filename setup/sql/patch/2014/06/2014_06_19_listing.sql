-- Convert member ID to a default ID
UPDATE listing SET member_id=146 WHERE member_id=0;

-- Make member_id unsigned
ALTER TABLE `listing` CHANGE `member_id` `member_id` INT(11)  UNSIGNED  NOT NULL;

-- Add foreign key to listing table
ALTER TABLE `listing` ADD CONSTRAINT `listing_fk` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;