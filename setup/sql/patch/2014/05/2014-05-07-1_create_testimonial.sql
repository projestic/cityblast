CREATE TABLE IF NOT EXISTS `testimonial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `testimonial` text,
  `image` varchar(256),
  `approved` char(1) NOT NULL DEFAULT '0',
  `link` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO testimonial( member_id, testimonial, link )
	SELECT id, testimonial, CONCAT( 'https://www.facebook.com/profile.php?id=', uid )
	FROM member
	WHERE testimonial IS NOT NULL AND testimonial != '';

ALTER TABLE `member`
  DROP `testimonial`;