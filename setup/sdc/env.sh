export SDC_URL=https://us-east-1.api.joyentcloud.com
export SDC_ACCOUNT=cityblast
export SDC_KEY_ID=$(ssh-keygen -l -f ~/.ssh/id_rsa.pub | awk '{print $2}' | tr -d '\n')